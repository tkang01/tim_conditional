(*** CS 51 Problem Set 6 ***)
(*** April 1, 2011 ***)
(*** Test Suite ***)

let check_fun f sol_f obj =
    f obj = sol_f obj
;;

(*>* Problem 2.1.a *>*)
(*>>* 2 *>>*)
let sol_headt (t: 'a tree) : 'a =
  let Stem(v, _, _) = t () in v
;;

let sol_ltail (t: 'a tree) : 'a tree =
  let Stem(_, t', _) = t () in t'
;;

let sol_rtail (t: 'a tree) : 'a tree =
  let Stem(_, _, t') = t () in t'
;;

let rec sol_mapt (f: 'a -> 'b) (t: 'a tree) : 'b tree =
  fun () -> let Stem(v, left, right) = t () in
    Stem(f v, sol_mapt f left, sol_mapt f right)
;;

let rec sol_zipt (f: 'a -> 'b -> 'c) (t1: 'a tree) (t2: 'b tree) : 'c tree =
  fun () -> Stem(f (sol_headt t1) (sol_headt t2), sol_zipt f (sol_ltail t1) (sol_ltail t2), sol_zipt f (sol_rtail t1) (sol_rtail t2))
;;

let rec sol_onest () =
  Stem(1, sol_onest, sol_onest)
;;

let rec sol_treenats () =
  let rec treenat x =
    fun () -> Stem(x, treenat (2*x), treenat (2*x+1)) in
    treenat 1 ()
;;

>>>
check_fun headt sol_headt sol_onest
>>>
check_fun headt sol_headt sol_treenats
>>>
check_fun headt sol_headt (sol_ltail sol_onest)
>>>
check_fun headt sol_headt (sol_rtail sol_treenats)
>>>
check_fun headt sol_headt (sol_ltail (sol_ltail sol_onest))
>>>
check_fun headt sol_headt (sol_rtail (sol_ltail sol_treenats))

(*>* Problem 2.1.b *>*)
(*>>* 2 *>>*)
let sol_headt (t: 'a tree) : 'a =
  let Stem(v, _, _) = t () in v
;;

let sol_ltail (t: 'a tree) : 'a tree =
  let Stem(_, t', _) = t () in t'
;;

let sol_rtail (t: 'a tree) : 'a tree =
  let Stem(_, _, t') = t () in t'
;;

let rec sol_mapt (f: 'a -> 'b) (t: 'a tree) : 'b tree =
  fun () -> let Stem(v, left, right) = t () in
    Stem(f v, sol_mapt f left, sol_mapt f right)
;;

let rec sol_zipt (f: 'a -> 'b -> 'c) (t1: 'a tree) (t2: 'b tree) : 'c tree =
  fun () -> Stem(f (sol_headt t1) (sol_headt t2), sol_zipt f (sol_ltail t1) (sol_ltail t2), sol_zipt f (sol_rtail t1) (sol_rtail t2))
;;

let rec sol_onest () =
  Stem(1, sol_onest, sol_onest)
;;

let rec sol_treenats () =
  let rec treenat x =
    fun () -> Stem(x, treenat (2*x), treenat (2*x+1)) in
    treenat 1 ()
;;

>>>
sol_headt (ltail sol_onest) = sol_headt (sol_ltail sol_onest)
>>>
sol_headt (rtail sol_onest) = sol_headt (sol_rtail sol_onest)
>>>
sol_headt (rtail (ltail sol_treenats)) = sol_headt (sol_rtail (sol_ltail sol_treenats))
>>>
sol_headt (ltail (rtail sol_treenats)) = sol_headt (sol_ltail (sol_rtail sol_treenats))

(*>* Problem 2.1.c *>*)
(*>>* 2 *>>*)
let sol_headt (t: 'a tree) : 'a =
  let Stem(v, _, _) = t () in v
;;

let sol_ltail (t: 'a tree) : 'a tree =
  let Stem(_, t', _) = t () in t'
;;

let sol_rtail (t: 'a tree) : 'a tree =
  let Stem(_, _, t') = t () in t'
;;

let rec sol_mapt (f: 'a -> 'b) (t: 'a tree) : 'b tree =
  fun () -> let Stem(v, left, right) = t () in
    Stem(f v, sol_mapt f left, sol_mapt f right)
;;

let rec sol_zipt (f: 'a -> 'b -> 'c) (t1: 'a tree) (t2: 'b tree) : 'c tree =
  fun () -> Stem(f (sol_headt t1) (sol_headt t2), sol_zipt f (sol_ltail t1) (sol_ltail t2), sol_zipt f (sol_rtail t1) (sol_rtail t2))
;;

let rec sol_onest () =
  Stem(1, sol_onest, sol_onest)
;;

let rec sol_treenats () =
  let rec treenat x =
    fun () -> Stem(x, treenat (2*x), treenat (2*x+1)) in
    treenat 1 ()
;;

let fifth t = sol_headt (sol_rtail (sol_ltail (sol_ltail (sol_rtail (sol_rtail t))))) ;;

>>>
fifth (mapt (fun x -> x + 1) sol_treenats) = fifth (sol_mapt (fun x -> x + 1) sol_treenats)
>>>
fifth (mapt (fun x -> 42) sol_onest) = fifth (sol_mapt (fun x -> 42) sol_onest)

(*>* Problem 2.1.d *>*)
(*>>* 2 *>>*)
let sol_headt (t: 'a tree) : 'a =
  let Stem(v, _, _) = t () in v
;;

let sol_ltail (t: 'a tree) : 'a tree =
  let Stem(_, t', _) = t () in t'
;;

let sol_rtail (t: 'a tree) : 'a tree =
  let Stem(_, _, t') = t () in t'
;;

let rec sol_mapt (f: 'a -> 'b) (t: 'a tree) : 'b tree =
  fun () -> let Stem(v, left, right) = t () in
    Stem(f v, sol_mapt f left, sol_mapt f right)
;;

let rec sol_zipt (f: 'a -> 'b -> 'c) (t1: 'a tree) (t2: 'b tree) : 'c tree =
  fun () -> Stem(f (sol_headt t1) (sol_headt t2), sol_zipt f (sol_ltail t1) (sol_ltail t2), sol_zipt f (sol_rtail t1) (sol_rtail t2))
;;

let rec sol_onest () =
  Stem(1, sol_onest, sol_onest)
;;

let rec sol_treenats () =
  let rec treenat x =
    fun () -> Stem(x, treenat (2*x), treenat (2*x+1)) in
    treenat 1 ()
;;

let fifth t = sol_headt (sol_rtail (sol_ltail (sol_ltail (sol_rtail (sol_rtail t))))) ;;

>>>
fifth (sol_zipt (fun x y -> x * y) sol_treenats sol_treenats) = fifth (zipt (fun x y-> x * y) sol_treenats sol_treenats)
>>>
fifth (sol_zipt (fun x y -> x + y) sol_onest sol_treenats) = fifth (zipt (fun x y -> x + y) sol_onest sol_treenats)

(*>* Problem 2.1.e *>*)
(*>>* 2 *>>*)
let sol_headt (t: 'a tree) : 'a =
  let Stem(v, _, _) = t () in v
;;

let sol_ltail (t: 'a tree) : 'a tree =
  let Stem(_, t', _) = t () in t'
;;

let sol_rtail (t: 'a tree) : 'a tree =
  let Stem(_, _, t') = t () in t'
;;

let rec sol_mapt (f: 'a -> 'b) (t: 'a tree) : 'b tree =
  fun () -> let Stem(v, left, right) = t () in
    Stem(f v, sol_mapt f left, sol_mapt f right)
;;

let rec sol_zipt (f: 'a -> 'b -> 'c) (t1: 'a tree) (t2: 'b tree) : 'c tree =
  fun () -> Stem(f (sol_headt t1) (sol_headt t2), sol_zipt f (sol_ltail t1) (sol_ltail t2), sol_zipt f (sol_rtail t1) (sol_rtail t2))
;;

let rec sol_onest () =
  Stem(1, sol_onest, sol_onest)
;;

let rec sol_treenats () =
  let rec treenat x =
    fun () -> Stem(x, treenat (2*x), treenat (2*x+1)) in
    treenat 1 ()
;;

let fifth t = sol_headt (sol_rtail (sol_ltail (sol_ltail (sol_rtail (sol_rtail t))))) ;;

>>>
fifth onest = fifth sol_onest
>>>
sol_headt onest = sol_headt sol_onest

(*>* Problem 2.1.f *>*)
(*>>* 2 *>>*)
let sol_headt (t: 'a tree) : 'a =
  let Stem(v, _, _) = t () in v
;;

let sol_ltail (t: 'a tree) : 'a tree =
  let Stem(_, t', _) = t () in t'
;;

let sol_rtail (t: 'a tree) : 'a tree =
  let Stem(_, _, t') = t () in t'
;;

let rec sol_mapt (f: 'a -> 'b) (t: 'a tree) : 'b tree =
  fun () -> let Stem(v, left, right) = t () in
    Stem(f v, sol_mapt f left, sol_mapt f right)
;;

let rec sol_zipt (f: 'a -> 'b -> 'c) (t1: 'a tree) (t2: 'b tree) : 'c tree =
  fun () -> Stem(f (sol_headt t1) (sol_headt t2), sol_zipt f (sol_ltail t1) (sol_ltail t2), sol_zipt f (sol_rtail t1) (sol_rtail t2))
;;

let rec sol_onest () =
  Stem(1, sol_onest, sol_onest)
;;

let rec sol_treenats () =
  let rec treenat x =
    fun () -> Stem(x, treenat (2*x), treenat (2*x+1)) in
    treenat 1 ()
;;

let fifth t = sol_headt (sol_rtail (sol_ltail (sol_ltail (sol_rtail (sol_rtail t))))) ;;

>>>
fifth treenats = fifth sol_treenats || fifth treenats + 1 = fifth sol_treenats
>>>
sol_headt treenats = sol_headt sol_treenats || sol_headt treenats + 1 = sol_headt sol_treenats 
>>>
sol_headt (sol_ltail (sol_ltail sol_treenats)) = sol_headt (sol_ltail (sol_ltail treenats))
 || 1 + sol_headt (sol_ltail (sol_ltail sol_treenats)) = sol_headt (sol_ltail (sol_ltail treenats))
>>>
sol_headt (sol_ltail (sol_rtail sol_treenats)) = sol_headt (sol_ltail (sol_rtail treenats))
 || 1 + sol_headt (sol_ltail (sol_rtail sol_treenats)) = sol_headt (sol_ltail (sol_rtail treenats))

(*>* Problem 2.2.a *>*)
(*>>* 2 *>>*)
open Lazy

let rec sol_ones = Cons(1, lazy(sol_ones));;

let sol_head (s:'a stream) : 'a =
  let Cons(v,_) = s in v
;;

let rec sol_map (f:'a -> 'b) (s:'a stream) : 'b stream =
  let Cons(v,t) = s in Cons(f v, lazy(sol_map f (Lazy.force t))) ;;

let rec sol_nats = Cons(0, lazy(sol_map ((+) 1) sol_nats)) ;;

let rec sol_nth (n:int) (s:'a stream) : 'a =
  if n = 0 then sol_head s else let Cons(_,t) = s in
    sol_nth (n-1) (Lazy.force t)
;;

let sol_merge (s1:int stream) (s2:int stream) : int stream =
  let rec helper (s1:int stream) (s2:int stream) (v:int) : int stream =
    let (Cons(v1,t1),Cons(v2,t2)) = (s1,s2) in
      if v1 < v2 then
	if v1 = v then helper (Lazy.force t1) s2 v
	else Cons(v1, lazy(helper (Lazy.force t1) s2 v1))
      else
	if v2 = v then helper s1 (Lazy.force t2) v
	else Cons(v2, lazy(helper s1 (Lazy.force t2) v2)) in
  let (Cons(v1,_),Cons(v2,_)) = (s1,s2) in
  let v = (min v1 v2) - 1 in
    helper s1 s2 v
;;

let sol_scale n = sol_map (( * ) n) ;;

let rec sol_selectivestream = Cons(1, lazy(sol_merge (sol_scale 3 sol_selectivestream) (sol_scale 5 sol_selectivestream))) ;;

>>>
head sol_ones = sol_head sol_ones
>>>
head sol_nats = sol_head sol_nats

(*>* Problem 2.2.b *>*)
(*>>* 2 *>>*)
open Lazy

let rec sol_ones = Cons(1, lazy(sol_ones));;

let sol_head (s:'a stream) : 'a =
  let Cons(v,_) = s in v
;;

let rec sol_map (f:'a -> 'b) (s:'a stream) : 'b stream =
  let Cons(v,t) = s in Cons(f v, lazy(sol_map f (Lazy.force t))) ;;

let rec sol_nats = Cons(0, lazy(sol_map ((+) 1) sol_nats)) ;;

let rec sol_nth (n:int) (s:'a stream) : 'a =
  if n = 0 then sol_head s else let Cons(_,t) = s in
    sol_nth (n-1) (Lazy.force t)
;;

let sol_merge (s1:int stream) (s2:int stream) : int stream =
  let rec helper (s1:int stream) (s2:int stream) (v:int) : int stream =
    let (Cons(v1,t1),Cons(v2,t2)) = (s1,s2) in
      if v1 < v2 then
	if v1 = v then helper (Lazy.force t1) s2 v
	else Cons(v1, lazy(helper (Lazy.force t1) s2 v1))
      else
	if v2 = v then helper s1 (Lazy.force t2) v
	else Cons(v2, lazy(helper s1 (Lazy.force t2) v2)) in
  let (Cons(v1,_),Cons(v2,_)) = (s1,s2) in
  let v = (min v1 v2) - 1 in
    helper s1 s2 v
;;

let sol_scale n = sol_map (( * ) n) ;;

let rec sol_selectivestream = Cons(1, lazy(sol_merge (sol_scale 3 sol_selectivestream) (sol_scale 5 sol_selectivestream))) ;;

>>>
sol_nth 5 (map (fun x -> x + 3) sol_nats) = sol_nth 5 (sol_map (fun x -> x + 3) sol_nats) 
>>>
sol_nth 5 (map (fun x -> x * 3) sol_nats) = sol_nth 5 (sol_map (fun x -> x * 3) sol_nats) 

(*>* Problem 2.2.c *>*)
(*>>* 2 *>>*)
open Lazy

let rec sol_ones = Cons(1, lazy(sol_ones));;

let sol_head (s:'a stream) : 'a =
  let Cons(v,_) = s in v
;;

let rec sol_map (f:'a -> 'b) (s:'a stream) : 'b stream =
  let Cons(v,t) = s in Cons(f v, lazy(sol_map f (Lazy.force t))) ;;

let rec sol_nats = Cons(0, lazy(sol_map ((+) 1) sol_nats)) ;;

let rec sol_nth (n:int) (s:'a stream) : 'a =
  if n = 0 then sol_head s else let Cons(_,t) = s in
    sol_nth (n-1) (Lazy.force t)
;;

let sol_merge (s1:int stream) (s2:int stream) : int stream =
  let rec helper (s1:int stream) (s2:int stream) (v:int) : int stream =
    let (Cons(v1,t1),Cons(v2,t2)) = (s1,s2) in
      if v1 < v2 then
	if v1 = v then helper (Lazy.force t1) s2 v
	else Cons(v1, lazy(helper (Lazy.force t1) s2 v1))
      else
	if v2 = v then helper s1 (Lazy.force t2) v
	else Cons(v2, lazy(helper s1 (Lazy.force t2) v2)) in
  let (Cons(v1,_),Cons(v2,_)) = (s1,s2) in
  let v = (min v1 v2) - 1 in
    helper s1 s2 v
;;

let sol_scale n = sol_map (( * ) n) ;;

let rec sol_selectivestream = Cons(1, lazy(sol_merge (sol_scale 3 sol_selectivestream) (sol_scale 5 sol_selectivestream))) ;;

>>>
sol_nth 0 nats = sol_nth 0 sol_nats || sol_nth 0 nats = sol_nth 0 sol_nats + 1
>>>
sol_nth 1 nats = sol_nth 1 sol_nats || sol_nth 1 nats = sol_nth 1 sol_nats + 1
>>>
sol_nth 2 nats = sol_nth 2 sol_nats || sol_nth 2 nats = sol_nth 2 sol_nats + 1
>>>
sol_nth 3 nats = sol_nth 3 sol_nats || sol_nth 3 nats = sol_nth 3 sol_nats + 1
>>>
sol_nth 4 nats = sol_nth 4 sol_nats || sol_nth 4 nats = sol_nth 4 sol_nats + 1
>>>
sol_nth 5 nats = sol_nth 5 sol_nats || sol_nth 5 nats = sol_nth 5 sol_nats + 1

(*>* Problem 2.2.d *>*)
(*>>* 2 *>>*)
open Lazy

let rec sol_ones = Cons(1, lazy(sol_ones));;

let sol_head (s:'a stream) : 'a =
  let Cons(v,_) = s in v
;;

let rec sol_map (f:'a -> 'b) (s:'a stream) : 'b stream =
  let Cons(v,t) = s in Cons(f v, lazy(sol_map f (Lazy.force t))) ;;

let rec sol_nats = Cons(0, lazy(sol_map ((+) 1) sol_nats)) ;;

let rec sol_nth (n:int) (s:'a stream) : 'a =
  if n = 0 then sol_head s else let Cons(_,t) = s in
    sol_nth (n-1) (Lazy.force t)
;;

let sol_merge (s1:int stream) (s2:int stream) : int stream =
  let rec helper (s1:int stream) (s2:int stream) (v:int) : int stream =
    let (Cons(v1,t1),Cons(v2,t2)) = (s1,s2) in
      if v1 < v2 then
	if v1 = v then helper (Lazy.force t1) s2 v
	else Cons(v1, lazy(helper (Lazy.force t1) s2 v1))
      else
	if v2 = v then helper s1 (Lazy.force t2) v
	else Cons(v2, lazy(helper s1 (Lazy.force t2) v2)) in
  let (Cons(v1,_),Cons(v2,_)) = (s1,s2) in
  let v = (min v1 v2) - 1 in
    helper s1 s2 v
;;

let sol_scale n = sol_map (( * ) n) ;;

let rec sol_selectivestream = Cons(1, lazy(sol_merge (sol_scale 3 sol_selectivestream) (sol_scale 5 sol_selectivestream))) ;;

>>>
nth 0 sol_nats = sol_nth 0 sol_nats
>>>
nth 1 sol_nats = sol_nth 1 sol_nats
>>>
nth 2 sol_nats = sol_nth 2 sol_nats
>>>
nth 3 sol_nats = sol_nth 3 sol_nats
>>>
nth 4 sol_nats = sol_nth 4 sol_nats
>>>
nth 5 sol_nats = sol_nth 5 sol_nats

(*>* Problem 2.2.e *>*)
(*>>* 2 *>>*)
open Lazy

let rec sol_ones = Cons(1, lazy(sol_ones));;

let sol_head (s:'a stream) : 'a =
  let Cons(v,_) = s in v
;;

let rec sol_map (f:'a -> 'b) (s:'a stream) : 'b stream =
  let Cons(v,t) = s in Cons(f v, lazy(sol_map f (Lazy.force t))) ;;

let rec sol_nats = Cons(0, lazy(sol_map ((+) 1) sol_nats)) ;;

let rec sol_nth (n:int) (s:'a stream) : 'a =
  if n = 0 then sol_head s else let Cons(_,t) = s in
    sol_nth (n-1) (Lazy.force t)
;;

let sol_merge (s1:int stream) (s2:int stream) : int stream =
  let rec helper (s1:int stream) (s2:int stream) (v:int) : int stream =
    let (Cons(v1,t1),Cons(v2,t2)) = (s1,s2) in
      if v1 < v2 then
	if v1 = v then helper (Lazy.force t1) s2 v
	else Cons(v1, lazy(helper (Lazy.force t1) s2 v1))
      else
	if v2 = v then helper s1 (Lazy.force t2) v
	else Cons(v2, lazy(helper s1 (Lazy.force t2) v2)) in
  let (Cons(v1,_),Cons(v2,_)) = (s1,s2) in
  let v = (min v1 v2) - 1 in
    helper s1 s2 v
;;

let sol_scale n = sol_map (( * ) n) ;;

let rec sol_selectivestream = Cons(1, lazy(sol_merge (sol_scale 3 sol_selectivestream) (sol_scale 5 sol_selectivestream))) ;;

>>>
nth 0 (merge sol_nats sol_nats) = 0
>>>
nth 5 (merge sol_nats sol_nats) = 5
>>>
nth 3 (merge (sol_scale 3 sol_nats) (sol_scale 5 sol_nats)) = 6
>>>
nth 4 (merge (sol_scale 2 sol_nats) (sol_scale 3 sol_nats)) = 6

(*>* Problem 2.2.f *>*)
(*>>* 2 *>>*)

(*>* Problem 2.2.g *>*)
(*>>* 2 *>>*)
open Lazy

let rec sol_ones = Cons(1, lazy(sol_ones));;

let sol_head (s:'a stream) : 'a =
  let Cons(v,_) = s in v
;;

let rec sol_map (f:'a -> 'b) (s:'a stream) : 'b stream =
  let Cons(v,t) = s in Cons(f v, lazy(sol_map f (Lazy.force t))) ;;

let rec sol_nats = Cons(0, lazy(sol_map ((+) 1) sol_nats)) ;;

let rec sol_nth (n:int) (s:'a stream) : 'a =
  if n = 0 then sol_head s else let Cons(_,t) = s in
    sol_nth (n-1) (Lazy.force t)
;;

let sol_merge (s1:int stream) (s2:int stream) : int stream =
  let rec helper (s1:int stream) (s2:int stream) (v:int) : int stream =
    let (Cons(v1,t1),Cons(v2,t2)) = (s1,s2) in
      if v1 < v2 then
	if v1 = v then helper (Lazy.force t1) s2 v
	else Cons(v1, lazy(helper (Lazy.force t1) s2 v1))
      else
	if v2 = v then helper s1 (Lazy.force t2) v
	else Cons(v2, lazy(helper s1 (Lazy.force t2) v2)) in
  let (Cons(v1,_),Cons(v2,_)) = (s1,s2) in
  let v = (min v1 v2) - 1 in
    helper s1 s2 v
;;

let sol_scale n = sol_map (( * ) n) ;;

let rec sol_selectivestream = Cons(1, lazy(sol_merge (sol_scale 3 sol_selectivestream) (sol_scale 5 sol_selectivestream))) ;;

>>>
sol_nth 0 (scale 4 sol_nats) = 0
>>>
sol_nth 1 (scale 4 sol_nats) = 4
>>>
sol_nth 2 (scale 4 sol_nats) = 8
>>>
sol_nth 3 (scale 4 sol_nats) = 12

(*>* Problem 2.2.h *>*)
(*>>* 2 *>>*)
open Lazy

let rec sol_ones = Cons(1, lazy(sol_ones));;

let sol_head (s:'a stream) : 'a =
  let Cons(v,_) = s in v
;;

let rec sol_map (f:'a -> 'b) (s:'a stream) : 'b stream =
  let Cons(v,t) = s in Cons(f v, lazy(sol_map f (Lazy.force t))) ;;

let rec sol_nats = Cons(0, lazy(sol_map ((+) 1) sol_nats)) ;;

let rec sol_nth (n:int) (s:'a stream) : 'a =
  if n = 0 then sol_head s else let Cons(_,t) = s in
    sol_nth (n-1) (Lazy.force t)
;;

let sol_merge (s1:int stream) (s2:int stream) : int stream =
  let rec helper (s1:int stream) (s2:int stream) (v:int) : int stream =
    let (Cons(v1,t1),Cons(v2,t2)) = (s1,s2) in
      if v1 < v2 then
	if v1 = v then helper (Lazy.force t1) s2 v
	else Cons(v1, lazy(helper (Lazy.force t1) s2 v1))
      else
	if v2 = v then helper s1 (Lazy.force t2) v
	else Cons(v2, lazy(helper s1 (Lazy.force t2) v2)) in
  let (Cons(v1,_),Cons(v2,_)) = (s1,s2) in
  let v = (min v1 v2) - 1 in
    helper s1 s2 v
;;

let sol_scale n = sol_map (( * ) n) ;;

let rec sol_selectivestream = Cons(1, lazy(sol_merge (sol_scale 3 sol_selectivestream) (sol_scale 5 sol_selectivestream))) ;;

>>>
sol_nth 3 sol_selectivestream = sol_nth 3 selectivestream
>>>
sol_nth 5 sol_selectivestream = sol_nth 5 selectivestream

(*>* Problem 2.3 *>*)
(*>>* 0 *>>*)
