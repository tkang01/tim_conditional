release is the version we will ship to students.

soln_old is the initial solution i built the intermediate solutions from.
soln_part6 is the official final solution and soln_old should be considered
deprecated.

check_static_files.hs checks that the files in static_files.txt are identical
between intermediate solution folders.

check_dynamic_files.hs prints out a diff between solution folders.

these are haskell scripts.  run them with 'runhaskell check_*.hs'.

soln_diff.txt is a snapshot of the solution diff for those which don't have
haskell isntalled.

the writeup is generated using markdown syntax in a text file.
ps7.html = markdown ps7.txt.  I use pandoc to generate html from markdown:
'pandoc -f markdown -t html ps7.txt'.
