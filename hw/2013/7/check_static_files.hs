{-# LANGUAGE TupleSections #-}

import Control.Monad
import Data.List
import System.Process
import Text.Printf
import qualified Data.ByteString.Char8 as BS

main = do
  files <- liftM (lines . BS.unpack) $ BS.readFile "static_files.txt"
  dirs <- liftM (lines . BS.unpack) $ BS.readFile "dirs.txt"
  let dirsCross = nub . map sort_pair . filter (not . uncurry (==)) $ cross dirs dirs
  allOk <- liftM (foldl' (&&) True . concat) $ 
    forM dirsCross $ \ (d1,d2) -> do
      forM files $ \ f -> do
        let p1 = printf "%s/%s" d1 f :: String
            p2 = printf "%s/%s" d2 f :: String
        (_,result,_) <- readProcessWithExitCode "diff" [p1,p2] ""
        if result /= "" then do
          putStrLn $ printf "%s and %s are different:" p1 p2
          putStrLn result
          return False
        else
          return True
  when allOk $ putStrLn "All OK"
  return ()
  where
    cross [] _ = []
    cross (x:xs) ys = map ((x,)) ys ++ cross xs ys

    sort_pair (x,y) | y < x = (y,x) | otherwise = (x,y)
