

---------- release v soln_part1 ----------


release/Bear.ml and soln_part1/Bear.ml are different:
42c42,47
<   (* ### TODO: Part 1 Basic ### *)
---
>   method get_name = "bear"
> 
>   method draw = 
>     self#draw_circle (Graphics.rgb 170 130 110) Graphics.black  ""
> 
>   method draw_z_axis = 40

release/Bee.ml and soln_part1/Bee.ml are different:
60c60,65
<   (* ### TODO: Part 1 Basic ### *)
---
>   method get_name = "bee"
> 
>   method draw = 
>     self#draw_circle Graphics.yellow Graphics.black ""
> 
>   method draw_z_axis = 20

release/Cave.ml and soln_part1/Cave.ml are different:
34c34,36
<   (* ### TODO: Part 1 Basic ### *)
---
>   method get_name = "cave"
> 
>   method draw = self#draw_circle Graphics.black Graphics.white "C"

release/Cow.ml and soln_part1/Cow.ml are different:
39c39,44
<   (* ### TODO: Part 1 Basic ### *)
---
>   method get_name = "cow"
> 
>   method draw = 
>     self#draw_circle (Graphics.rgb 180 140 255) Graphics.black ""
> 
>   method draw_z_axis = 30

release/Flower.ml and soln_part1/Flower.ml are different:
45c45,48
<   (* ### TODO: Part 1 Basic ### *)
---
>   method get_name = "flower"
> 
>   method draw =
>     self#draw_circle (Graphics.rgb 255 150 255) Graphics.black ""

release/Hive.ml and soln_part1/Hive.ml are different:
51c51,54
<   (* ### TODO: Part 1 Basic ### *)
---
>   method get_name = "hive"
> 
>   method draw =
>     self#draw_circle Graphics.cyan Graphics.black ""

release/Pasture.ml and soln_part1/Pasture.ml are different:
34c34,37
<   (* ### TODO Part 1 Basic ### *)
---
>   method get_name = "pasture"
> 
>   method draw =
>     self#draw_circle (Graphics.rgb 70 100 130) Graphics.white "P"

release/Pond.ml and soln_part1/Pond.ml are different:
12c12,16
<   (* ### TODO: Part 1 Basic ### *)
---
>   method get_name = "pond"
> 
>   method draw = self#draw_circle Graphics.blue Graphics.black ""
> 
>   method is_obstacle = true



---------- soln_part1 v soln_part2 ----------


soln_part1/Bear.ml and soln_part2/Bear.ml are different:
0a1,2
> open Direction
> open World
1a4,5
> open Movable
> open Hive
12c16
< class bear p : world_object_t =
---
> class bear p (hive:hive) : movable_t =
14c18
<   inherit world_object p as super
---
>   inherit movable p bear_inverse_speed as super
57c61,68
<   (* ### TODO: Part 2 Movement ### *)
---
>   method next_direction =
>     if World.rand (World.size/2) = 0 then 
>       (* occasionally move toward the hive where flowers have a high
>          probability of surviving. *)
>       World.direction_from_to self#get_pos hive#get_pos
>     else 
>       (* otherwise random walk. *)
>       Some (Direction.random World.rand)

soln_part1/Bee.ml and soln_part2/Bee.ml are different:
0a1,2
> open Direction
> open World
1a4
> open Movable
18c21
< class bee p : world_object_t =
---
> class bee p : movable_t =
20c23
<   inherit world_object p as super
---
>   inherit movable p bee_inverse_speed as super
79c82
<   (* ### TODO: Part 2 Movement ### *)
---
>   method next_direction = Some (Direction.random World.rand)

soln_part1/Cow.ml and soln_part2/Cow.ml are different:
0a1,2
> open Direction
> open World
1a4,5
> open Movable
> open Hive
11c15
< class cow p : world_object_t =
---
> class cow p (hive:hive) : movable_t =
13c17
<   inherit world_object p as super
---
>   inherit movable p cow_inverse_speed as super
52c56,63
<   (* ### TODO: Part 2 Movement ### *)
---
>   method next_direction =
>     if World.rand (World.size/2) = 0 then 
>       (* occasionally move toward the hive where flowers have a high
>          probability of surviving. *)
>       World.direction_from_to self#get_pos hive#get_pos
>     else 
>       (* otherwise random walk. *)
>       Some (Direction.random World.rand)

soln_part1/MainBasic.ml and soln_part2/MainBasic.ml are different:
18c18
<       ignore (new hive (2,2)) ;
---
>       let hive = new hive (2,2) in
21c21
<       ignore (new bear (5,5)) ;
---
>       ignore (new bear (5,5) hive) ;
23c23
<       ignore (new cow (7,7)) ;
---
>       ignore (new cow (7,7) hive) ;

soln_part1/MainMovement.ml and soln_part2/MainMovement.ml are different:
17c17
<       ignore (new hive (World.size/2,World.size/2)) ;
---
>       let hive = new hive (World.size/2,World.size/2) in
19,20c19,20
<       ignore (new bear (0,0)) ;
<       ignore (new cow (World.size-1,World.size-1)) ;
---
>       ignore (new bear (0,0) hive) ;
>       ignore (new cow (World.size-1,World.size-1) hive) ;

soln_part1/Movable.ml and soln_part2/Movable.ml are different:
2a3,4
> open Event
> open World
22c24,29
<   (* ### TODO: Part 2 Movement ### *)
---
>   initializer
>     match inv_speed with
>     | None -> ()
>     | Some s -> 
>         self#register_handler (Event.buffer s World.move_event)
>                               (fun _ -> self#do_move)
28c35
<   (* ### TODO: Part 2 Movement ### *)
---
>   method private do_move : unit = self#move self#next_direction
34,35c41
<   (* ### TODO: Part 2 Movement ### *)
<   method next_direction = failwith "unimplemented"
---
>   method next_direction : Direction.direction option = None



---------- soln_part2 v soln_part3 ----------


soln_part2/Bear.ml and soln_part3/Bear.ml are different:
1a2
> open WorldObjectI
24c25
<   (* ### TODO: Part 3 Actions ### *)
---
>   val mutable stolen_honey = 0
32c33,34
<   (* ### TODO: Part 3 Actions ### *)
---
>   initializer
>     self#register_handler World.action_event (fun _ -> self#do_action)
38c40,44
<   (* ### TODO: Part 3 Actions ### *)
---
>   method private do_action : unit =
>     if self#get_pos = hive#get_pos then begin
>       let amt = hive#forfeit_honey pollen_theft_amount (self :> world_object_i) in
>       stolen_honey <- stolen_honey + amt
>     end
49c55,56
<     self#draw_circle (Graphics.rgb 170 130 110) Graphics.black  ""
---
>     self#draw_circle (Graphics.rgb 170 130 110) Graphics.black  
>                      (Printf.sprintf "%d" stolen_honey)
53,54d59
<   (* ### TODO: Part 3 Actions ### *)
< 
62,70c67
<     if World.rand (World.size/2) = 0 then 
<       (* occasionally move toward the hive where flowers have a high
<          probability of surviving. *)
<       World.direction_from_to self#get_pos hive#get_pos
<     else 
<       (* otherwise random walk. *)
<       Some (Direction.random World.rand)
< 
<   (* ### TODO: Part 3 Actions ### *)
---
>     World.direction_from_to self#get_pos hive#get_pos

soln_part2/Bee.ml and soln_part3/Bee.ml are different:
1a2
> open WorldObjectI
29c30
<   (* ### TODO: Part 3 Actions ### *)
---
>   val mutable pollen = []
39c40,41
<   (* ### TODO: Part 3 Actions ### *)
---
>   initializer 
>     self#register_handler World.action_event (fun () -> self#do_action)
47c49,52
<   (* ### TODO: Part 3 Actions ### *)
---
>   method private do_action : unit = 
>     let neighbors = World.get self#get_pos in 
>     List.iter self#deposit_pollen neighbors ; 
>     List.iter self#extract_pollen neighbors
55c60,67
<   (* ### TODO: Part 3 Actions ### *)
---
>   method private deposit_pollen (o:world_object_i) : unit =
>     let pollen' = o#receive_pollen pollen in
>     pollen <- pollen'
> 
>   method private extract_pollen (o:world_object_i) : unit =
>     match o#forfeit_pollen with
>     | None -> ()
>     | Some i -> pollen <- i::pollen
66c78,79
<     self#draw_circle Graphics.yellow Graphics.black ""
---
>     self#draw_circle Graphics.yellow Graphics.black 
>                      (Printf.sprintf "%d" (List.length pollen))
70,71d82
<   (* ### TODO: Part 3 Actions ### *)
< 

soln_part2/Cow.ml and soln_part3/Cow.ml are different:
0a1
> open Helpers
23c24
<   (* ### TODO: Part 3 Actions ### *)
---
>   val mutable consumed_objects = 0
29c30,31
<   (* ### TODO: Part 3 Actions ### *)
---
>   initializer
>     self#register_handler World.action_event (fun () -> self#do_action)
35c37,45
<   (* ### TODO: Part 3 Actions ### *)
---
>   method private do_action =
>     let neighbors = World.get self#get_pos in 
>     List.iter begin fun o -> 
>        if Helpers.is_some o#smells_like_pollen then begin
>          print_string "*nom* " ; flush_all () ;
>          o#die ;
>          consumed_objects <- consumed_objects + 1
>        end
>     end neighbors ;
46c56,57
<     self#draw_circle (Graphics.rgb 180 140 255) Graphics.black ""
---
>     self#draw_circle (Graphics.rgb 180 140 255) Graphics.black
>                      (Printf.sprintf "%d" consumed_objects)
50,51d60
<   (* ### TODO: Part 3 Actions ### *)
< 

soln_part2/Flower.ml and soln_part3/Flower.ml are different:
0a1,2
> open Helpers
> open World
19c21
< class flower p : world_object_t =
---
> class flower p pollen_id : world_object_t =
27c29
<   (* ### TODO: Part 3 Actions ### *)
---
>   val mutable pollen = World.rand max_pollen
33c35,36
<   (* ### TODO: Part 3 Actions ### *)
---
>   initializer
>     self#register_handler World.action_event (fun () -> self#do_action)
39c42,50
<   (* ### TODO: Part 3 Actions ### *)
---
>   method private do_action =
>     Helpers.with_inv_probability World.rand produce_pollen_probability 
>       begin fun () ->
>         pollen <- min max_pollen (pollen + 1)
>       end ;
>     Helpers.with_inv_probability World.rand bloom_probability 
>       begin fun () ->
>         World.spawn 1 self#get_pos (fun p -> ignore (new flower p pollen_id))
>       end
48c59,60
<     self#draw_circle (Graphics.rgb 255 150 255) Graphics.black ""
---
>     self#draw_circle (Graphics.rgb 255 150 255) Graphics.black 
>                      (Printf.sprintf "%d" pollen)
50c62,72
<   (* ### TODO: Part 3 Actions ### *)
---
>   method smells_like_pollen = if pollen = 0 then None else Some pollen_id
> 
>   method forfeit_pollen = 
>     let result = ref None in
>     if pollen > 0 then 
>       Helpers.with_inv_probability World.rand forfeit_pollen_probability 
>         begin fun () -> 
>           pollen <- pollen - 1 ;
>           result := Some pollen_id
>         end ;
>     !result

soln_part2/Hive.ml and soln_part3/Hive.ml are different:
0a1,4
> open Helpers
> open Event
> open WorldObjectI
> open World
13c17,23
< class hive p : world_object_t =
---
> class hive p :
> object
>   inherit world_object_t
> 
>   method forfeit_honey : int -> world_object_i -> int
> 
> end =
21c31
<   (* ### TODO: Part 3 Actions ### *)
---
>   val mutable pollen = starting_pollen
29c39,40
<   (* ### TODO: Part 3 Actions ### *)
---
>   initializer
>     self#register_handler World.action_event (fun () -> self#do_action)
35c46,50
<   (* ### TODO: Part 3 Actions ### *)
---
>   method private do_action = 
>     Helpers.with_inv_probability World.rand pollen_probability 
>       begin fun () ->
>         pollen <- pollen + 1
>       end ;
54c69,70
<     self#draw_circle Graphics.cyan Graphics.black ""
---
>     self#draw_circle Graphics.cyan Graphics.black
>                      (Printf.sprintf "%d" pollen)
56c72,74
<   (* ### TODO: Part 3 Actions ### *)
---
>   method receive_pollen ps = 
>     pollen <- pollen + (min (List.length ps) max_pollen_deposit) ;
>     []
64c82,87
<   (* ### TODO: Part 3 Actions ### *)
---
>   method forfeit_honey n b = 
>     let stolen = min pollen n in
>     pollen <- pollen - stolen ;
>     self#danger b ;
>     stolen
>     

soln_part2/MainActions.ml and soln_part3/MainActions.ml are different:
26c26
<                    (fun p -> ignore (new flower p))
---
>                    (fun p -> ignore (new flower p !pollen_id))

soln_part2/MainBasic.ml and soln_part3/MainBasic.ml are different:
17c17
<       ignore (new flower (1,1)) ;
---
>       ignore (new flower (1,1) 0) ;



---------- soln_part3 v soln_part4 ----------


soln_part3/Ageable.ml and soln_part4/Ageable.ml are different:
0a1,2
> open Event
> open World
32c34,35
<   (* ### TODO: Part 4 Aging ### *)
---
>   initializer
>     self#register_handler World.age_event (fun () -> self#do_age)
38c41,43
<   (* ### TODO: Part 4 Aging ### *)
---
>   method private do_age : unit = 
>     lifetime <- lifetime - 1 ;
>     if lifetime <= 0 then self#die
60,62c65,67
<   (* ### TODO: Part 4 Aging ### *)
<   method draw_picture = failwith "unimplemented"
<   method reset_life = failwith "unimplemented"
---
>   method draw_picture : unit = super#draw
> 
>   method reset_life = lifetime <- max_lifetime

soln_part3/Bee.ml and soln_part4/Bee.ml are different:
4,5c4,5
< open WorldObject
< open Movable
---
> open CarbonBased
> open Ageable
22c22
< class bee p : movable_t =
---
> class bee p : ageable_t =
24c24
<   inherit movable p bee_inverse_speed as super
---
>   inherit carbon_based p bee_inverse_speed (World.rand bee_lifetime) bee_lifetime as super
77,80d76
<   method draw = 
<     self#draw_circle Graphics.yellow Graphics.black 
<                      (Printf.sprintf "%d" (List.length pollen))
< 
87c83,85
<   (* ### TODO: Part 4 Aging ### *)
---
>   method draw_picture = 
>     self#draw_circle Graphics.yellow Graphics.black 
>                      (Printf.sprintf "%d" (List.length pollen))

soln_part3/CarbonBased.ml and soln_part4/CarbonBased.ml are different:
1a2
> open Dust
12c13,16
<   (* ### TODO: Part 4 Aging *)
---
>   initializer
>     self#register_handler self#get_die_event begin fun () ->
>       ignore (new dust self#get_pos self#get_name)
>     end

soln_part3/Dust.ml and soln_part4/Dust.ml are different:
15c15,17
<   (* ### TODO: Part 4 Aging ### *)
---
>   method draw_picture =
>     self#draw_circle (Graphics.rgb 150 150 150) 
>                      Graphics.black (String.sub name 0 2)

soln_part3/Flower.ml and soln_part4/Flower.ml are different:
3c3,4
< open WorldObject
---
> open Ageable
> open CarbonBased
21c22
< class flower p pollen_id : world_object_t =
---
> class flower p pollen_id : ageable_t =
23c24
<   inherit world_object p as super
---
>   inherit carbon_based p None (World.rand flower_lifetime) flower_lifetime as super
58,61d58
<   method draw =
<     self#draw_circle (Graphics.rgb 255 150 255) Graphics.black 
<                      (Printf.sprintf "%d" pollen)
< 
74c71,73
<   (* ### TODO: Part 4 Aging ### *)
---
>   method receive_pollen ps =
>     if List.filter ((<>) pollen_id) ps <> [] then self#reset_life ;
>     ps
80c79,82
<   (* ### TODO: Part 4 Aging ### *)
---
>   method draw_picture =
>     self#draw_circle (Graphics.rgb 255 150 255) Graphics.black 
>                      (Printf.sprintf "%d" pollen)
> 

soln_part3/Hive.ml and soln_part4/Hive.ml are different:
5a6
> open Bee
51,52c52,57
< 
<   (* ### TODO: Part 4 Aging ### *)
---
>     if pollen >= cost_of_bee then
>       Helpers.with_inv_probability World.rand spawn_probability 
>         begin fun () -> 
>           pollen <- pollen - cost_of_bee ;
>           self#generate_bee self#get_pos
>         end
58c63,64
<   (* ### TODO: Part 4 Aging ### *)
---
>   method private generate_bee p =
>     ignore (new bee p)
87d92
<     



---------- soln_part4 v soln_part5 ----------


soln_part4/Bee.ml and soln_part5/Bee.ml are different:
0a1
> open Helpers
22c23,29
< class bee p : ageable_t =
---
> class type bee_t =
> object
>   inherit ageable_t
> 
>   method next_direction_default : Direction.direction option
> end
> class bee p (home:world_object_i) : bee_t =
32c39,41
<   (* ### TODO: Part 5 Smart Bees ### *)
---
>   val sensing_range = World.rand max_sensing_range
> 
>   val pollen_types = World.rand max_pollen_types + 1
69c78,89
<   (* ### TODO: Part 5 Smart Bees ### *)
---
>   method private magnet_flower : world_object_i option =
>     let os = World.objects_within_range self#get_pos sensing_range in
>     let smelly = List.filter begin fun o -> 
>       match o#smells_like_pollen with
>       | None -> false
>       | Some s -> not (List.mem s pollen) 
>     end os in
>     let ps = List.map (fun o -> (Direction.distance o#get_pos self#get_pos, o)) smelly in
>     let sorted = List.sort compare ps in
>     match sorted with
>     | [] -> None
>     | (_,o)::_ -> Some o
91,93c111,116
<   method next_direction = Some (Direction.random World.rand)
< 
<   (* ### TODO: Part 5 Smart Bees ### *)
---
>   method next_direction =
>     if List.length (Helpers.unique pollen) >= pollen_types then 
>       World.direction_from_to self#get_pos home#get_pos
>     else match self#magnet_flower with
>     | Some m -> World.direction_from_to self#get_pos m#get_pos
>     | None -> self#next_direction_default
101c124
<   (* ### TODO: Part 5 Smart Bees ### *)
---
>   method next_direction_default = None

soln_part4/BeeBouncy.ml and soln_part5/BeeBouncy.ml are different:
1c1,3
< open WorldObject
---
> open Direction
> open World
> open Bee
6c8,13
< class bee_bouncy p hive : world_object_t =
---
> class bee_bouncy p home :
> object
>   inherit bee_t
> 
>   method next_direction_default : Direction.direction option
> end =
8c15
<   inherit world_object p as super
---
>   inherit bee p home as super
14c21
<   (* ### TODO: Part 5 Smart Bees *)
---
>   val mutable dir = Direction.random World.rand
20c27
<   (* ### TODO: Part 5 Smart Bees *)
---
>   method get_name = "bee_bouncy"
26c33,39
<   (* ### TODO: Part 5 Smart Bees *)
---
>   method next_direction_default = 
>     let next_pos = Direction.move_point self#get_pos (Some dir) in
>     if World.can_move next_pos then Some dir 
>     else begin 
>       dir <- Direction.random World.rand ; 
>       self#next_direction_default
>     end

soln_part4/BeeRandom.ml and soln_part5/BeeRandom.ml are different:
1c1,3
< open WorldObject
---
> open Direction
> open World
> open Bee
4c6,11
< class bee_random p : world_object_t = 
---
> class bee_random p home :
> object
>   inherit bee_t
> 
>   method next_direction_default : Direction.direction option
> end = 
6c13
<   inherit world_object p as super
---
>   inherit bee p home as super
12c19
<   (* ### TODO: Part 5 Smart Bees *)
---
>   method get_name = "bee_random"
18c25
<   (* ### TODO: Part 5 Smart Bees *)
---
>   method next_direction_default = Some (Direction.random World.rand)

soln_part4/Hive.ml and soln_part5/Hive.ml are different:
6c6,7
< open Bee
---
> open BeeBouncy
> open BeeRandom
64,66c65,68
<     ignore (new bee p)
< 
<   (* ### TODO: Part 5 Smart Bees ### *)
---
>     Helpers.with_equal_probability World.rand [ 
>       (fun () -> ignore (new bee_random p (self :> world_object_i))) ;
>       fun () -> ignore (new bee_bouncy p (self :> world_object_i))
>     ]

soln_part4/MainActions.ml and soln_part5/MainActions.ml are different:
1a2
> open WorldObjectI
6c7
< open Bee
---
> open BeeRandom
39c40
<         ignore (new bee (World.size/2+1,World.size/2)) ;
---
>         ignore (new bee_random (World.size/2+1,World.size/2) (hive :> world_object_i)) ;

soln_part4/MainBasic.ml and soln_part5/MainBasic.ml are different:
0a1
> open WorldObjectI
5c6
< open Bee
---
> open BeeRandom
19c20
<       ignore (new bee (3,3)) ;
---
>       ignore (new bee_random (3,3) (hive :> world_object_i)) ;

soln_part4/MainFinal.ml and soln_part5/MainFinal.ml are different:
30,31c30,31
<       ignore (new cave (0,0) hive) ;
<       ignore (new pasture (World.size-1,World.size-1) hive) ;
---
>       ignore (new cave (0,0)) ;
>       ignore (new pasture (World.size-1,World.size-1)) ;

soln_part4/MainMovement.ml and soln_part5/MainMovement.ml are different:
1a2
> open WorldObjectI
6c7
< open Bee
---
> open BeeRandom
18c19
<       ignore (new bee (World.size/2+1,World.size/2)) ;
---
>       ignore (new bee_random (World.size/2+1,World.size/2) (hive :> world_object_i)) ;



---------- soln_part5 v soln_part6 ----------


soln_part5/Bear.ml and soln_part6/Bear.ml are different:
0a1
> open Helpers
17c18
< class bear p (hive:hive) : movable_t =
---
> class bear p (home:world_object_i) (hive:hive) : movable_t =
27c28
<   (* ### TODO: Part 6 Events ### *)
---
>   val mutable life = bear_starting_life
43a45,49
>     end ;
>     if self#get_pos = home#get_pos && stolen_honey > 0 then begin
>       stolen_honey <- 
>         List.length (home#receive_pollen (Helpers.replicate stolen_honey 0)) ;
>       if hive#get_pollen < (pollen_theft_amount/2) then self#die
46,47d51
<   (* ### TODO: Part 6 Custom Events ### *)
< 
60c64,66
<   (* ### TODO: Part 6 Custom Events ### *)
---
>   method receive_sting =
>     life <- life - 1 ;
>     if life = 0 then self#die
67,69c73,76
<     World.direction_from_to self#get_pos hive#get_pos
< 
<   (* ### TODO: Part 6 Custom Events ### *)
---
>     if stolen_honey = 0 then
>       World.direction_from_to self#get_pos hive#get_pos
>     else
>       World.direction_from_to self#get_pos home#get_pos

soln_part5/Bee.ml and soln_part6/Bee.ml are different:
2a3
> open Event
43c44
<   (* ### TODO: Part 6 Custom Events ### *)
---
>   val mutable danger_object = None
50,52c51,52
<     self#register_handler World.action_event (fun () -> self#do_action)
< 
<   (* ### TODO: Part 6 Custom Events ### *)
---
>     self#register_handler World.action_event (fun () -> self#do_action) ;
>     self#register_handler home#get_danger_event (fun o -> self#do_danger o)
61,63c61,71
<     List.iter self#extract_pollen neighbors
< 
<   (* ### TODO: Part 6 Custom Events ### *)
---
>     List.iter self#extract_pollen neighbors ;
>     match danger_object with
>     | None -> ()
>     | Some o -> if self#get_pos = o#get_pos then begin
>         o#receive_sting ;
>         self#die
>       end
> 
>   method private do_danger (o:world_object_i) : unit = 
>     danger_object <- Some o ;
>     ignore(Event.add_listener o#get_die_event (fun () -> danger_object <- None))
111,118c119,128
<   method next_direction =
<     if List.length (Helpers.unique pollen) >= pollen_types then 
<       World.direction_from_to self#get_pos home#get_pos
<     else match self#magnet_flower with
<     | Some m -> World.direction_from_to self#get_pos m#get_pos
<     | None -> self#next_direction_default
< 
<   (* ### TODO: Part 6 Custom Events ### *)
---
>   method next_direction = 
>     match danger_object with
>     | Some o -> 
>         World.direction_from_to self#get_pos o#get_pos
>     | None -> 
>         if List.length (Helpers.unique pollen) >= pollen_types then 
>           World.direction_from_to self#get_pos home#get_pos
>         else match self#magnet_flower with
>         | Some m -> World.direction_from_to self#get_pos m#get_pos
>         | None -> self#next_direction_default

soln_part5/Cave.ml and soln_part6/Cave.ml are different:
0a1,2
> open Event
> open WorldObjectI
1a4,5
> open Hive
> open Bear
8c12
< class cave p : world_object_t =
---
> class cave p (hive:hive) : world_object_t =
16c20
<   (* ### TODO: Part 6 Custom Events ### *)
---
>   val mutable bear_attacking = false
22c26,31
<   (* ### TODO: Part 6 Custom Events ### *)
---
>   initializer
>     let spawn_condition pollen = 
>       pollen >= spawn_bear_pollen && not bear_attacking
>     in
>     self#register_handler (Event.filter spawn_condition hive#get_pollen_event)
>                           (fun _ -> self#do_spawn_bear)
28c37,41
<   (* ### TODO: Part 6 Custom Events ### *)
---
>   method private do_spawn_bear =
>     print_string "omg bears! " ; flush_all () ;
>     let bear = new bear self#get_pos (self :> world_object_i) hive in
>     bear_attacking <- true ;
>     self#register_handler bear#get_die_event (fun () -> bear_attacking <- false)
38c51
<   (* ### TODO: Part 6 Custom Events *)
---
>   method receive_pollen _ = []

soln_part5/Cow.ml and soln_part6/Cow.ml are different:
2a3
> open WorldObjectI
16c17
< class cow p (hive:hive) : movable_t =
---
> class cow p (home:world_object_i) (hive:hive) : movable_t =
38,47c39,51
<     let neighbors = World.get self#get_pos in 
<     List.iter begin fun o -> 
<        if Helpers.is_some o#smells_like_pollen then begin
<          print_string "*nom* " ; flush_all () ;
<          o#die ;
<          consumed_objects <- consumed_objects + 1
<        end
<     end neighbors ;
< 
<   (* ### TODO: Part 6 Custom Events ### *)
---
>     if consumed_objects < max_consumed_objects then
>       (* if still hungry then om nom anything that smells like pollen. *)
>       let neighbors = World.get self#get_pos in 
>       List.iter begin fun o -> 
>          if Helpers.is_some o#smells_like_pollen then begin
>            print_string "*nom* " ; flush_all () ;
>            o#die ;
>            consumed_objects <- consumed_objects + 1
>          end
>       end neighbors ;
>     else if self#get_pos = home#get_pos then 
>       (* if no longer hungry and back at the pasture, then expire. *)
>       self#die
66c70,73
<     if World.rand (World.size/2) = 0 then 
---
>     if consumed_objects >= max_consumed_objects then
>       (* if no longer hungry then return home. *)
>       World.direction_from_to self#get_pos home#get_pos
>     else if World.rand (World.size/2) = 0 then 
74,75d80
<   (* ### TODO: Part 6 Custom Events ### *)
< 

soln_part5/Hive.ml and soln_part6/Hive.ml are different:
19c19
< class hive p :
---
> class hive p : 
22a23,26
>   method get_pollen_event : int Event.event
> 
>   method get_pollen : int
> 
35c39
<   (* ### TODO: Part 6 Custom Events ### *)
---
>   val pollen_event = Event.new_event ()
81a86
>     Event.fire_event pollen_event pollen ;
84,85d88
<   (* ### TODO: Part 6 Custom Events ### *)
< 
96c99,101
<   (* ### TODO: Part 6 Custom Events ### *)
---
>   method get_pollen_event : int Event.event = pollen_event
> 
>   method get_pollen = pollen

soln_part5/MainActions.ml and soln_part6/MainActions.ml are different:
34,35c34,35
<       ignore (new cave (0,0)) ;
<       ignore (new pasture (World.size-1,World.size-1)) ;
---
>       let cave = new cave (0,0) hive in
>       let pasture = new pasture (World.size-1,World.size-1) hive in
43,44c43,44
<       ignore (new bear (0,0) hive) ;
<       ignore (new cow (World.size-1,World.size-1) hive)
---
>       ignore (new bear (0,0) (cave :> world_object_i) hive) ;
>       ignore (new cow (World.size-1,World.size-1) (pasture :> world_object_i) hive)

soln_part5/MainBasic.ml and soln_part6/MainBasic.ml are different:
21,24c21,24
<       ignore (new cave (4,4)) ;
<       ignore (new bear (5,5) hive) ;
<       ignore (new pasture (6,6)) ;
<       ignore (new cow (7,7) hive) ;
---
>       let cave = new cave (4,4) hive in
>       ignore (new bear (5,5) (cave :> world_object_i) hive) ;
>       let pasture = new pasture (6,6) hive in
>       ignore (new cow (7,7) (cave :> world_object_i) hive) ;

soln_part5/MainFinal.ml and soln_part6/MainFinal.ml are different:
30,31c30,31
<       ignore (new cave (0,0)) ;
<       ignore (new pasture (World.size-1,World.size-1)) ;
---
>       ignore (new cave (0,0) hive) ;
>       ignore (new pasture (World.size-1,World.size-1) hive) ;

soln_part5/MainMovement.ml and soln_part6/MainMovement.ml are different:
20,21c20,23
<       ignore (new bear (0,0) hive) ;
<       ignore (new cow (World.size-1,World.size-1) hive) ;
---
>       let cave = new cave (0,0) hive in
>       ignore (new bear (0,0) (cave :> world_object_i) hive) ;
>       let pasture = new pasture (World.size-1,World.size-1) hive in
>       ignore (new cow (World.size-1,World.size-1) (pasture :> world_object_i) hive) ;

soln_part5/Pasture.ml and soln_part6/Pasture.ml are different:
0a1,3
> open Helpers
> open WorldObjectI
> open World
1a5,6
> open Hive
> open Cow
8c13
< class pasture p : world_object_t =
---
> class pasture p (hive:hive) : world_object_t =
16c21
<   (* ### TODO Part 6 Custom Events ### *)
---
>   val mutable cow_is_grazing = false
22c27,28
<   (* ### TODO Part 6 Custom Events ### *)
---
>   initializer
>     self#register_handler World.action_event (fun () -> self#do_action)
28c34,45
<   (* ### TODO Part 6 Custom Events ### *)
---
>   method private do_action =
>     let smelly_objects = 
>       World.fold begin fun o t -> 
>         t + Helpers.int_of_bool (Helpers.is_some o#smells_like_pollen)
>       end 0
>     in
>     if smelly_objects >= smelly_object_limit && not cow_is_grazing then begin
>       print_string "mooooooooo " ; flush_all () ;
>       let c = new cow self#get_pos (self :> world_object_i) hive in
>       cow_is_grazing <- true ;
>       self#register_handler c#get_die_event (fun () -> cow_is_grazing <- false)
>     end

