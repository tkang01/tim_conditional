#!/bin/bash

FILE=wikiinfo.txt
LINKS=$(cat ${FILE})
TEMP=temp

for l in $LINKS
do
    u="http://en.wikipedia.org$l"
    save=${l:6}
    echo $u
    curl $u > $save
    sed s,/wiki/,,g $save > $TEMP
    mv $TEMP $save
done
rm $TEMP