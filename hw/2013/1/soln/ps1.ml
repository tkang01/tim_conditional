(*** CS 51 Problem Set 1 Solutions ***)
(*** January 28, 2012 ***)
(*** YOUR NAME HERE ***)

(* Fill in types: *)
(*>* Problem 1a *>*)

let prob1a : string  = let greet y = "Hello " ^ y in greet "World!" ;;


(*>* Problem 1b *>*)

let prob1b : int option list = [Some 4; Some 2; None; Some 3] ;;


(*>* Problem 1c *>*)

let prob1c : ('a option * float option) * bool  = ((None, Some 42.0), true) ;;


(* Explain in comments why the following will not type check, and
 * provide a fix by either replacing the types or modifying the
 * expressions *)

(*>* Problem 1d *>*)
(* Why doesn't this type check? *)
(*

(* type should be list of tuples, not a tuple with int list element *)

let prob1d : string * int list = [("CS", 51); ("CS", 50)] ;;
*)

(*>* Problem 1e *>*)
(* Why doesn't this type check? *)
(*

(* cannot compare int and float directly *)

let prob1e : int =
  let compare(x,y) = x < y in
  if compare (4, 3.9) then 4 else 2;;
*)

(*>* Problem 1f *>*)
(* Why doesn't this type check? *)
(*

(* should be type (string * int option) list *)

let prob1f : (string * string) list =
  [("January", None); ("February", Some 1); ("March", None); ("April", None);
   ("May", None); ("June", Some 1); ("July", None); ("August", None);
   ("September", 3); ("October", 1); ("November", 2); ("December", 3)] ;;
*)


(* Problem 2 - Fill in expressions with these types: *)
(*>* Problem 2a *>*)

let prob2a : (int * (string * float) option list) list =
  [(42, [Some ("ducks", 3.0); Some ("geese", 7.0)]) ]
;;

(*>* Problem 2b *>*)

type student = {name: string; year: int option; house: string option} ;;

let prob2b : (student list option * int) list =
  let b = {name="bill";   year=Some 2012; house=Some "adams"} in
  let g = {name="greg";   year=None;      house=None} in
  let h = {name="halley"; year=Some 2013; house=Some "pfoho"} in
    [(Some [b;h], 22); (None, 23); (Some [g], 24)]
;;

(*>* Problem 2c *>*)

let prob2c : (float * float -> float) * (int -> int -> unit) * bool  =
  ((fun (x,y) -> x *. y), (fun x y -> ()), true)
;;

(* Fill in ??? with something that makes these typecheck: *)
(*>* Problem 2d *>*)

let prob2d =
  let rec foo bar =
    match bar with
    | (a, (b, c)) :: xs -> if a then (b - c + (foo xs)) else foo xs
    | _ -> 0
  in foo ([(true, (2, 3))] : (bool * (int * int)) list)
;;

(*>* Problem 2e *>*)

let prob2e =
  let v = (32.0, 28.0) in
  let square x = x *. x in
  let squared_norm (w: float * float) : float =
    let (f1,f2) = w in square f1 +. square f2 in
  let d = sqrt (squared_norm v) in
  int_of_float d
;;



(* Problem 3 - Write the following functions *)
(* For each subproblem, you must implement a given function and corresponding
 * unit tests.  You are provided a high level description as well as a
 * prototype of the function you must implement. *)

(*>* Problem 3a *>*)

(* reversed lst should return true if the items in the list are in
 * decreasing order. The empty list is considered to be reversed.*)

(* let _ = (reversed : int list -> bool) ;; *)

let rec reversed (l:int list) : bool =
  match l with
    | x::(y::tl as l') -> x >= y && reversed l'
    | _ -> true
;;

assert (reversed []);;
assert (reversed [2]);;
assert (reversed [2;2;2]);;
assert (not (reversed [2;1;2]));;
assert (reversed [2;2;1]);;
assert (reversed [9;8;7;6;5;5;5;5;4;-2]);;
assert (not (reversed [9;8;7;6;7;5;5;5;5;4;3]));;


(*>* Problem 3b *>*)

(* merge xs ys takes two lists sorted in increasing order, and returns a
single merged list in sorted order.  For example:
   # merge [1;3;5] [2;4;6];;
- : int list = [1; 2; 3; 4; 5; 6]
# merge [1;3;5] [2;4;6;12];;
- : int list = [1; 2; 3; 4; 5; 6; 12]
# merge [1;3;5;700;702] [2;4;6;12];;
- : int list = [1; 2; 3; 4; 5; 6; 12; 700; 702]
*)

(* let _ = (merge : int list -> int list -> int list) ;; *)

let rec merge (a:int list) (b:int list) : int list =
  match a, b with
  | (a1 :: a'), (b1 :: b') ->
      if a1 < b1 then (a1 :: merge a' b) else (b1 :: merge a b')
  | a, [] -> a
  | [], b -> b
;;

(* sample tests *)
assert ((merge [1;2;3] [4;5;6;7]) = [1;2;3;4;5;6;7]) ;;
assert ((merge [4;5;6;7] [1;2;3]) = [1;2;3;4;5;6;7]) ;;
assert ((merge [4;5;6;7] [1;2;3]) = [1;2;3;4;5;6;7]) ;;
assert ((merge [2;2;2;2] [1;2;3]) = [1;2;2;2;2;2;3]) ;;
assert ((merge [1;2] [1;2]) = [1;1;2;2]) ;;
assert ((merge [-1;2;3;100] [-1;5;1001]) = [-1;-1;2;3;5;100;1001]) ;;
assert ((merge [] []) = []) ;;
assert ((merge [1] []) = [1]) ;;
assert ((merge [] [-1]) = [-1]) ;;
assert ((merge [1] [-1]) = [-1;1]) ;;


(*>* Problem 3c *>*)
(* unzip should be a function which, given a list of pairs, returns a
   pair of lists, the first of which contains each first element of
   each pair, and the second of which contains each second element.
   The returned lists should have the elements in the order that they
   appeared in the input.  So, for instance,
   unzip [(1,2);(3,4);(5,6)] = ([1;3;5],[2;4;6])
*)

(* let _ = (unzip : (int * int) list -> int list * int list) ;; *)

let rec unzip (l:(int * int) list) : int list * int list =
  match l with
  | (a, b) :: ps -> let (l1, l2) = unzip ps in (a :: l1, b :: l2)
  | [] -> ([], [])
;;

assert((unzip [])=([],[]));;
assert((unzip [(1,2);(3,4)])=([1;3],[2;4]));;


(*>* Problem 3d *>*)

(* variance lst should be a function that returns None if lst has fewer than 2
elements, and Some of the variance of lst otherwise: 1/n * sum (x_i-m)^2, where
 a^2 means a squared, and m is the arithmetic mean of the list (sum of list /
length of list).  For example,
 - variance [1.0; 2.0; 3.0; 4.0; 5.0] = Some 2.5
 - variance [1.0] = None

Remember to use the floating point version of the arithmetic operators when
operating on floats (+. *., etc).  The "float" function can cast an int to a
float.
*)

(* let _ = (variance : float list -> float option) ;; *)

let rec sum (l:float list) : float =
  match l with
  | x :: xs -> x +. sum xs
  | [] -> 0.0

let variance (l:float list) : float option =
  let n = List.length l in
  if n < 2 then None else Some
    (let nf = float n in
     let mean = sum l /. nf in
     let square x = x *. x in
     (1.0 /. (nf -. 1.0)) *. sum (List.map (fun x -> square (x -. mean)) l))
;;

(* we'll need this to test float equality *)
let float_option_approx_eq (x1:float option) (x2:float option) : bool =
  match (x1, x2) with
  | (Some f1, Some f2) -> abs_float (f1 -. f2) < 1e-6
  | (None, None) -> true
  | (_, _) -> false
;;

assert (float_option_approx_eq (variance []) None);;
assert (float_option_approx_eq (variance [1.]) None);;

assert (float_option_approx_eq (variance [1.0;2.0;3.0;4.0;5.0])
  (Some 2.5));;
assert (float_option_approx_eq (variance [1.0;1.0]) (Some 0.0));;
assert (float_option_approx_eq (variance [3.0;2.0;1.0]) (Some 1.0));;


(*>* Problem 3e *>*)

(* few_divisors n m should return true if n has fewer than m divisors,
 * (including 1 and n) and false otherwise:
few_divisors 17 3;;
- : bool = true
# few_divisors 4 3;;
- : bool = false
# few_divisors 4 4;;
- : bool = true
 *)
(* let _ = (few_divisors : int -> int -> bool) ;; *)

let few_divisors (n:int) (m:int) : bool =
  let rec count_divs_from (start:int) : int =
    if start > n then 0 else
      (if n mod start = 0 then 1 else 0) + count_divs_from (start + 1)
  in count_divs_from 1 < m
;;

assert (few_divisors 17 3);;
assert (not (few_divisors 4 3));;
assert (not (few_divisors 13 2));;
assert (few_divisors 15 5);;
assert (not (few_divisors 30 6));;
assert (few_divisors 1 2);;
assert (not (few_divisors 0 1));;


(*>* Problem 3f *>*)

(* concat_list sep lst should take a list of strings and return one big
   string with all the string elements of lst concatenated together, but
   separated by the string sep.  Here are some example tests:
concat_list ", " ["Greg"; "Anna"; "David"] ;;
- : string = "Greg, Anna, David"
concat_list "..." ["Moo"; "Baaa"; "Quack"] ;;
- : string = "Moo...Baaa...Quack"
concat_list ", " [] ;;
- : string = "" ;;
concat_list ", " ["Moo"] ;;
- : string = "Moo"
*)
(* The type signature for concat_list is: *)
(* concat_list : string -> string list -> string *)

let rec concat_list (sep:string) (lst:string list) : string =
  match lst with
    | hd::[] -> hd
    | hd::tl -> hd ^ sep ^ (concat_list sep tl)
    | [] -> ""
;;

assert ((concat_list "" ["a"; "b"; "c"])="abc");;
assert ((concat_list ", " ["Greg"; "Anna"; "David"])="Greg, Anna, David");;
assert ((concat_list ", " ["Greg"])="Greg");;
assert ((concat_list ", " [])="");;

(*>* Problem 3g *>*)

(* One way to compress a list of characters is to use run-length encoding.
  The basic idea is that whenever we have repeated characters in a list
  such as ['a';'a';'a';'a';'a';'b';'b';'b';'c';'d';'d';'d';'d'] we can
  (sometimes) represent the same information more compactly as a list
  of pairs like [(5,'a');(3,'b');(1,'c');(4,'d')].  Here, the numbers
  represent how many times the character is repeated.  For example,
  the first character in the string is 'a' and it is repeated 5 times,
  followed by 3 occurrences of the character 'b', followed by one 'c',
  and finally 4 copies of 'd'.

  Write a function to_run_length that converts a list of characters into
  the run-length encoding, and then write a function from_run_length
  that converts back.  Writing both functions will make it easier to
  test that you've gotten them right.
*)
(* The type signatures for to_run_length and from_run_length are: *)
(* to_run_length : char list -> (int * char) list *)
(* from_run_length : (int * char) list -> char list *)

let rec to_run_length (lst:char list) : (int * char) list =
  match lst with
    | hd::tl -> (match (to_run_length tl) with
	| (i,c)::tl -> if hd = c then (i+1,c)::tl
	  else (1,hd)::(i,c)::tl
	| [] -> [1,hd]
    )
    | [] -> []
;;

assert ((to_run_length [])=[]);;
assert ((to_run_length ['a'])=[(1, 'a')]);;
assert ((to_run_length ['a';'a'])=[(2, 'a')]);;
assert ((to_run_length ['a';'a';'b';'a'])=[(2, 'a');(1, 'b');(1,'a')]);;


let rec from_run_length (lst:(int * char) list) : char list =
  match lst with
    | (i,c)::tl -> if i < 1 then from_run_length tl
      else if i = 1 then c::(from_run_length tl)
      else c::(from_run_length ((i-1, c)::tl))
    | [] -> []
;;

assert ((from_run_length [])=[]);;
assert ((from_run_length [(1,'a')])=['a']);;
assert ((from_run_length [(1,'a');(2, 'b')])=['a';'b';'b']);;
assert ((from_run_length [(1,'a');(2, 'a')])=['a';'a';'a']);;

assert ((from_run_length (to_run_length ['h';'e';'l';'l';'o']))=
  ['h';'e';'l';'l';'o']);;


(*>* Problem 4 *>*)

(* Challenge!

  permutations lst should return a list containing every
  permutation of lst.  For example, one correct answer to
  permutations [1;	2; 3] is
  [[1; 2; 3]; [2; 1; 3]; [2; 3; 1]; [1; 3; 2];	[3; 1; 2]; [3; 2; 1]].

  It doesn't matter what order the permutations appear in the returned list.
  Note that if the input list is of length n then the answer should be of
  length n!.

  Hint:
  One way to do this is to write an auxiliary function,
  interleave : int -> int list -> int list list,
  that yields all interleavings of its first argument into its second:
  interleave 1 [2;3] = [ [1;2;3]; [2;1;3]; [2;3;1] ].
  You may also find occasion for the library functions
  List.map and List.concat.
*)

(* let _ = (permutations : int list -> int list list) ;; *)

let rec interleave (n:int) (l:int list) : int list list =
  match l with
  | x :: xs -> (n :: x :: xs) :: List.map (fun l -> x :: l) (interleave n xs)
  | [] -> [[n]]

let rec permutations (l:int list) : int list list =
  match l with
  | x :: xs -> List.concat (List.map (interleave x) (permutations xs))
  | [] -> [[]]
;;

(* since permutations doesn't specify the order the elements
   are returned in, we need to sort them to test our results *)
let rec list_cmp (a:int list) (b:int list) =
  match a, b with
  | x :: xs, y :: ys ->
      if x < y then -1 else if x > y then 1 else list_cmp xs ys
  | [], y :: ys -> -1
  | x :: xs, [] -> 1
  | [], [] -> 0

let list_sort = List.sort list_cmp;;


assert ((permutations [])=[[]]);;
assert ((permutations [1])=[[1]]);;
assert ((list_sort (permutations [1;2]))=
  (list_sort [[1;2];[2;1]]));;
assert ((list_sort (permutations [1;2;3]))=
  (list_sort [[1;2;3];[1;3;2];[2;1;3];[2;3;1];[3;1;2];[3;2;1]]));;
