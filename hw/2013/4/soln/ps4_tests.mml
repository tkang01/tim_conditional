(*** CS 51 Problem Set 4 ***)
(*** February 27, 2012 ***)
(*** Test Suite ***)

(*>+* eat >+*)
module BinSTree(C : COMPARABLE_AND_GENABLE) : BINTREE with type elt = C.t =
struct
module BinaryHeap(C : COMPARABLE_AND_GENABLE) : PRIOQUEUE with type elt = C.t =
struct
(*>+* eat-replacewith >+*)
module C = IntCompare
(*>+* eat-end >+*)

(*>* Problem 2.0 *>*)
(*>>* 3 *>>*)

>>>
let x = C.generate () in
let t = insert x empty in
t = Branch(Leaf, [x], Leaf)

>>>
let x = C.generate () in
let t = insert x empty in
let t = insert x t in
t = Branch(Leaf, [x;x], Leaf)

>>>
let x = C.generate () in
let t = insert x empty in
let t = insert x t in
let y = C.generate_gt x () in
let t = insert y t in
t = Branch(Leaf, [x;x], Branch(Leaf, [y], Leaf))

>>>
let x = C.generate () in
let t = insert x empty in
let t = insert x t in
let y = C.generate_gt x () in
let t = insert y t in
let z = C.generate_lt x () in
let t = insert z t in
t = Branch( Branch(Leaf, [z], Leaf), [x;x], Branch(Leaf, [y], Leaf))


(*>* Problem 2.1 *>*)
(*>>* 2 *>>*)

>>>
let x = C.generate () in
let t = insert x empty in
search x t

>>>
let x = C.generate () in
let t = insert x empty in
let order = [ true; false; true; true; true; false; false] in
let full_tree, values_inserted =
  reduce
    begin fun current_order (tree_so_far, values_so_far) ->
      let prev_value =
        match values_so_far with
        | [] -> x
        | hd :: _ -> hd
      in
      let value =
        if current_order
        then C.generate_gt prev_value ()
        else C.generate_lt prev_value ()
      in
      insert value tree_so_far, value :: values_so_far
    end
    (t, []) order
in
List.fold_left
  (fun accum value -> accum && (search value full_tree)) true values_inserted



(*>* Problem 2.2 *>*)
(*>>* 1 *>>*)

>>>
let x = C.generate () in
let x2 = C.generate_gt x () in
let x3 = C.generate_gt x2 () in
let x4 = C.generate_gt x3 () in
getmin (insert x2 (insert x4 (insert x (insert x3 empty)))) = x


(*>* Problem 2.3 *>*)
(*>>* 1 *>>*)

>>>
let x = C.generate () in
let x2 = C.generate_lt x () in
let x3 = C.generate_lt x2 () in
let x4 = C.generate_lt x3 () in
getmax (insert x4 (insert x3 (insert x2 (insert x empty)))) = x


(*>* Problem 3.0 *>*)
(*>>* 0 *>>*)

(*>* Problem 3.1 *>*)
(*>>* 0 *>>*)


(*>* Problem 3.2 *>*)
(*>>* 0 *>>*)


(*>* Problem 3.3 *>*)
(*>>* 0 *>>*)


(*>* Problem 3.4 *>*)
(*>>* 0 *>>*)


(*>* Problem 3.5 *>*)
(*>>* 10 *>>*)

module IntListQueue = (ListQueue(IntCompare) : 
                        PRIOQUEUE with type elt = IntCompare.t)
module IntTreeQueue = (TreeQueue(IntCompare) : 
                        PRIOQUEUE with type elt = IntCompare.t)
let list_module = (module IntListQueue : PRIOQUEUE with type elt = IntCompare.t)
let tree_module = (module IntTreeQueue : PRIOQUEUE with type elt = IntCompare.t)

(* list_module tests *)
>>>
let module P = (val (list_module) : PRIOQUEUE with type elt = IntCompare.t) in
P.is_empty P.empty

>>>
let module P = (val (list_module) : PRIOQUEUE with type elt = IntCompare.t) in
let x = P.add 3 P.empty in
not (P.is_empty x)

>>>
let module P = (val (list_module) : PRIOQUEUE with type elt = IntCompare.t) in
let x = P.add 3 P.empty in
let (p, x') = P.take x in
p = 3 && P.is_empty x'

>>>
let module P = (val (list_module) : PRIOQUEUE with type elt = IntCompare.t) in
let x = P.add 3 P.empty in
let x' = P.add 4 x in 
let x'' = P.add 2 x' in
P.take x'' = (2, x')

>>>
let module P = (val (list_module) : PRIOQUEUE with type elt = IntCompare.t) in
let x = P.add 3 P.empty in
let x' = P.add 4 x in 
let x'' = P.add 2 x' in
let x' = snd (P.take x'') in
P.take x' = (3, (P.add 4 P.empty))

>>>
let module P = (val (list_module) : PRIOQUEUE with type elt = IntCompare.t) in
let x = P.add 3 P.empty in
let x' = P.add 4 x in 
let x'' = P.add 2 x' in
let x' = snd (P.take x'') in
let x = snd (P.take x') in
P.take x = (4, P.empty)

>>>
let module P = (val (list_module) : PRIOQUEUE with type elt = IntCompare.t) in
let x = P.empty in
let insert = [2;7;12;1;-1;4] in
let rec inserter lst queue =
  match lst with
  | [] -> queue
  | hd :: tl -> inserter tl (P.add hd queue) in
let x = inserter insert x in
let rec remover lst queue = 
  match lst with
  | [] -> P.is_empty queue
  | hd :: tl ->
    let (p, x') = P.take queue in
    p = hd && remover tl x'
in
remover (List.fast_sort compare insert) x


(* tree_module tests *)
>>>
let module P = (val (tree_module) : PRIOQUEUE with type elt = IntCompare.t) in
P.is_empty P.empty

>>>
let module P = (val (tree_module) : PRIOQUEUE with type elt = IntCompare.t) in
let x = P.add 3 P.empty in
not (P.is_empty x)

>>>
let module P = (val (tree_module) : PRIOQUEUE with type elt = IntCompare.t) in
let x = P.add 3 P.empty in
let (p, x') = P.take x in
p = 3 && P.is_empty x'

>>>
let module P = (val (tree_module) : PRIOQUEUE with type elt = IntCompare.t) in
let x = P.add 3 P.empty in
let x' = P.add 4 x in 
let x'' = P.add 2 x' in
P.take x'' = (2, x')

>>>
let module P = (val (tree_module) : PRIOQUEUE with type elt = IntCompare.t) in
let x = P.add 3 P.empty in
let x' = P.add 4 x in 
let x'' = P.add 2 x' in
let x' = snd (P.take x'') in
P.take x' = (3, (P.add 4 P.empty))

>>>
let module P = (val (tree_module) : PRIOQUEUE with type elt = IntCompare.t) in
let x = P.add 3 P.empty in
let x' = P.add 4 x in 
let x'' = P.add 2 x' in
let x' = snd (P.take x'') in
let x = snd (P.take x') in
P.take x = (4, P.empty)

>>>
let module P = (val (tree_module) : PRIOQUEUE with type elt = IntCompare.t) in
let x = P.empty in
let insert = [2;7;12;1;-1;4] in
let rec inserter lst queue =
  match lst with
  | [] -> queue
  | hd :: tl -> inserter tl (P.add hd queue) in
let x = inserter insert x in
let rec remover lst queue = 
  match lst with
  | [] -> P.is_empty queue
  | hd :: tl ->
    let (p, x') = P.take queue in
    p = hd && remover tl x'
in
remover (List.fast_sort compare insert) x


(*>* Problem 4.0 *>*)
(*>>* 25 *>>*)

(* get_top *)
>>>
let x = C.generate () in
let t = Leaf x in
get_top t = x

>>>
let x = C.generate () in
let y = C.generate_gt x () in
let t = OneBranch (x, y) in
get_top t = x

>>>
let x = C.generate () in
let y = C.generate_gt x () in
let z = C.generate_gt y () in 
let t = TwoBranch (Even, x, Leaf y, Leaf z) in
get_top t = x

>>>
let x = C.generate () in
let y = C.generate_gt x () in
let z = C.generate_gt y () in 
let w = C.generate_gt z () in
let t = TwoBranch (Odd, x, OneBranch (y, w), Leaf z) in
get_top t = x

>>>
let x = C.generate () in
let y = C.generate_gt x () in
let z = C.generate_gt y () in 
let w = C.generate_gt z () in
let q = C.generate_gt w () in
let t = TwoBranch (Even, x, OneBranch (y, w), OneBranch (z, q)) in
get_top t = x


(* get_last *)
>>>
let x = C.generate () in
let t = Leaf x in
get_last t = (x, Empty)

>>>
let x = C.generate () in
let y = C.generate_gt x () in
let t = OneBranch (x, y) in
get_last t = (y, Tree (Leaf x))

>>>
let x = C.generate () in
let y = C.generate_gt x () in
let z = C.generate_gt y () in 
let t = TwoBranch (Even, x, Leaf y, Leaf z) in
get_last t = (z, Tree (OneBranch (x, y)))


>>>
let x = C.generate () in
let y = C.generate_gt x () in
let z = C.generate_gt y () in 
let w = C.generate_gt z () in
let t = TwoBranch (Odd, x, OneBranch (y, w), Leaf z) in
get_last t = (w, Tree (TwoBranch (Even, x, Leaf y, Leaf z)))

>>>
let x = C.generate () in
let y = C.generate_gt x () in
let z = C.generate_gt y () in 
let w = C.generate_gt z () in
let q = C.generate_gt w () in
let t = TwoBranch (Even, x, OneBranch (y, w), OneBranch (z, q)) in
get_last t = (q, Tree (TwoBranch (Odd, x, OneBranch (y, w), Leaf z)))


(* fix *)
>>>
let x = C.generate () in
let t = Leaf x in
fix t = t

>>>
let x = C.generate () in
let y = C.generate_gt x () in
let t = OneBranch (y, x) in
fix t = OneBranch (x, y)

>>>
let x = C.generate () in
let y = C.generate_gt x () in
let t = OneBranch (x, y) in
fix t = OneBranch (x, y)

>>>
let x = C.generate () in
let y = C.generate_gt x () in
let z = C.generate_gt y () in 
let t = TwoBranch (Even, x, Leaf y, Leaf z) in
fix t = t

>>>
let x = C.generate () in
let y = C.generate_gt x () in
let z = C.generate_gt y () in 
let t = TwoBranch (Even, z, Leaf x, Leaf y) in
fix t = TwoBranch (Even, x, Leaf z, Leaf y)

>>>
let x = C.generate () in
let y = C.generate_gt x () in
let z = C.generate_gt y () in 
let t = TwoBranch (Even, y, Leaf z, Leaf x) in
fix t = TwoBranch (Even, x, Leaf z, Leaf y)

>>>
let x = C.generate () in
let y = C.generate_gt x () in
let z = C.generate_gt y () in 
let w = C.generate_gt z () in
let t = TwoBranch (Odd, x, OneBranch (y, w), Leaf z) in
fix t = t

>>>
let x = C.generate () in
let y = C.generate_gt x () in
let z = C.generate_gt y () in 
let w = C.generate_gt z () in
let t = TwoBranch (Odd, z, OneBranch (x, y), Leaf w) in
fix t = TwoBranch (Odd, x, OneBranch (y, z), Leaf w)

>>>
let x = C.generate () in
let y = C.generate_gt x () in
let z = C.generate_gt y () in 
let w = C.generate_gt z () in
let t = TwoBranch (Odd, w, OneBranch (y, z), Leaf x) in
fix t = TwoBranch (Odd, x, OneBranch (y, z), Leaf w)

>>>
let x = C.generate () in
let y = C.generate_gt x () in
let z = C.generate_gt y () in 
let w = C.generate_gt z () in
let t = TwoBranch (Odd, w, OneBranch (x, z), Leaf y) in
fix t = TwoBranch (Odd, x, OneBranch (z, w), Leaf y)

>>>
let x = C.generate () in
let y = C.generate_gt x () in
let z = C.generate_gt y () in 
let w = C.generate_gt z () in
let q = C.generate_gt w () in
let t = TwoBranch (Even, x, OneBranch (y, w), OneBranch (z, q)) in
fix t = t

>>>
let x = C.generate () in
let y = C.generate_gt x () in
let z = C.generate_gt y () in 
let w = C.generate_gt z () in
let q = C.generate_gt w () in
let t = TwoBranch (Even, y, OneBranch (x, w), OneBranch (z, q)) in
fix t = TwoBranch (Even, x, OneBranch (y, w), OneBranch (z, q))

>>>
let x = C.generate () in
let y = C.generate_gt x () in
let z = C.generate_gt y () in 
let w = C.generate_gt z () in
let q = C.generate_gt w () in
let t = TwoBranch (Even, w, OneBranch (x, y), OneBranch (z, q)) in
fix t = TwoBranch (Even, x, OneBranch (y, w), OneBranch (z, q))

>>>
let x = C.generate () in
let y = C.generate_gt x () in
let z = C.generate_gt y () in 
let w = C.generate_gt z () in
let q = C.generate_gt w () in
let t = TwoBranch (Even, w, OneBranch (z, q), OneBranch (x, y)) in
fix t = TwoBranch (Even, x, OneBranch (z, q), OneBranch (y, w))

>>>
let x = C.generate () in
let y = C.generate_gt x () in
let z = C.generate_gt y () in 
let w = C.generate_gt z () in
let q = C.generate_gt w () in
let t = TwoBranch (Even, w, OneBranch (x, q), OneBranch (y, z)) in
fix t = TwoBranch (Even, x, OneBranch (w, q), OneBranch (y, z))

>>>
let x = C.generate () in
let y = C.generate_gt x () in
let z = C.generate_gt y () in 
let w = C.generate_gt z () in
let q = C.generate_gt w () in
let t = TwoBranch (Even, q, OneBranch (y, z), OneBranch (x, w)) in
fix t = TwoBranch (Even, x, OneBranch (y, z), OneBranch (w, q))

(* take *)
>>>
let x = C.generate () in
let y = C.generate_gt x () in
let z = C.generate_gt y () in 
let w = C.generate_gt z () in
let t = Tree (TwoBranch (Odd, x, OneBranch (y, w), Leaf z)) in
take t = (x, Tree (TwoBranch (Even, y, Leaf w, Leaf z)))

(*>* Problem N.0 *>*)
(*>>* 0 *>>*)

(*>* Problem N.1 *>*)
(*>>* 0 *>>*)

(*>* Problem N.2 *>*)
(*>>* 0 *>>*)
