open Dict

(* An association list implementation of dictionaries.  *)
module GradingAssocListDict(D:DICT_ARG) : (DICT with type key = D.key
                                         with type value = D.value) = 
struct
  type key = D.key
  type value = D.value 
        
  type dict = 
    | Empty 
    | Branch of key * value * dict * dict
      
  let empty : dict = Empty
    
  let rec insert (d:dict) (k:key) (v:value) : dict = 
    match d with 
      | Empty -> Branch (k,v,Empty,Empty)
      | Branch (kk,vv,l,r) ->
	match D.compare k kk with
	  | Order.Less -> Branch(kk,vv,insert l k v,r)
	  | Order.Greater -> Branch(kk,vv,l,insert r k v)
	  | Order.Eq -> Branch (k,v,l,r)
      
  let rec lookup (d:dict) (k:key) : value option = 
    match d with 
      | Empty -> None
      | Branch (kk,vv,l,r) ->
	match D.compare k kk with
	  | Order.Less -> lookup l k
	  | Order.Greater -> lookup r k
	  | Order.Eq -> Some vv
	    
  let member (d:dict) (k:key) : bool = 
    lookup d k <> None
(*
  let rec remove_right k v l r : key * value * dict =
    match r with
      | Empty -> (k,v,l)
      | Branch (kk,vv,ll,rr) ->
	let (a,b,c) = remove_right kk vv ll rr in
	(a,b,Branch(k,v,l,c))

  let merge (l : dict) (r : dict) : dict =
    match l,r with 
      | Empty, _ -> r
      | _, Empty -> l
      | Branch(kk,vv,ll,rr),_ ->
	let (a,b,c) = remove_right kk vv ll rr in
	Branch(a,b,c,rr)
*)     
  let rec merge (l : dict) (r : dict) : dict =
    match l, r with
      | Empty, _ -> r
      | _, Empty -> l
      | Branch(k,v,ll,rr), _ -> Branch(k,v,ll,merge rr r)

  let rec remove (d:dict) (k:key) : dict = 
    match d with 
      | Empty -> Empty
      | Branch (kk,vv,l,r) ->
	match D.compare k kk with
	  | Order.Less -> Branch(kk,vv,remove l k, r)
	  | Order.Greater -> Branch(kk,vv,l,remove r k)
	  | Order.Eq -> merge l r
	      
  let choose (d:dict) : (key*value*dict) option = 
    match d with
      | Empty -> None
      | Branch (kk,vv,l,r) -> Some (kk,vv,merge l r)            

  let fold (f:key -> value -> 'a -> 'a) (u:'a) (d:dict) : 'a = 
    let rec recur d k a =
      match d with 
	| Empty -> k a
	| Branch (kk,vv,l,r) ->
	  recur l (recur r (f kk vv)) a
    in
    recur d (fun x -> x) u

  let string_of_key = D.string_of_key
  let string_of_value = D.string_of_value
    
  let list_of_tree t =
    fold (fun k v r -> (k,v)::r) [] t
      
  let string_of_dict (t: dict) : string = 
    let f = (fun y (k,v) -> y ^ "\n key: " ^ D.string_of_key k ^ 
               "; value: (" ^ D.string_of_value v ^ ")") in
      List.fold_left f "" (list_of_tree t)

end    

