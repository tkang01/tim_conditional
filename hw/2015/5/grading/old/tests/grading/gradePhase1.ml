open Lazy

module Tester =
struct
  let wrong = ref 0
  let right = ref 0

  let reset () = 
    wrong := 0 ; 
    right := 0

  let good () = right := !right + 1
  let bad () = wrong := !wrong + 1

  let total () = !wrong + !right

  let correct () = !right

  let scale n = 
    let c = float_of_int (correct ()) in
    let t = float_of_int (total ()) in
    int_of_float (floor (c /. t *. float_of_int n))

  let test (b : bool Lazy.t) (e : string) =
    try 
      if Lazy.force b then
	good ()
      else bad ()
    with
      | _ -> bad ()

end

module DictTest (A : Dict.DICT_ARG with type key = int with type value = int)
  (F : functor (D : Dict.DICT_ARG) ->
    (Dict.DICT with type key = D.key
               with type value = D.value)) =
struct
  let run_test name total test = 
    Tester.reset () ;
    print_string ("(** $$ " ^ name ^ " $$ **)\n") ;
    test () ;
    print_string ("(** $$ Total: " ^ string_of_int (Tester.scale total) ^ " / " ^ string_of_int total ^ " $$ **)")
  
  module DZ = F(A)
  module TZ = Testdict.GradingAssocListDict(A)

  let test_vectors = [[(1,1)]; 
		      [(1,2);(2,1)]; 
		      [(5,6);(8,12);(2,-3);(3,7);(1,2)];
		      [(1,2);(0,1);(11,15);(10,11);(7,8);(17,9);(11,17);(20,8)]]

  let equiv a b = 
    (DZ.fold (fun k v x -> x && TZ.lookup a k = Some v) true b) &&
    (TZ.fold (fun k v x -> x && DZ.lookup b k = Some v) true a)

  let test_empty () = 
    Tester.test (lazy (equiv TZ.empty DZ.empty)) "empty not empty" ;
    ()

  let test_insert () =
    let core_test_DZ tv = 
      let (_,_,x) = 
	List.fold_left 
	  (fun (a,b,good) (k,v) ->
	    let a' = TZ.insert a k v in
	    let b' = DZ.insert b k v in
	    (a',b',good && equiv a' b'))
	  (TZ.empty,DZ.empty,true) tv
      in Tester.test (lazy x) "Bad insert"
    in
    List.iter core_test_DZ test_vectors ;
    ()

  let test_remove () = 
    let core_test_DZ tv =
      let a = List.fold_left (fun d (k,v) -> TZ.insert d k v) TZ.empty tv in
      let b = List.fold_left (fun d (k,v) -> DZ.insert d k v) DZ.empty tv in
      let (_,_,x) = 
	List.fold_left 
	  (fun (a,b,good) (k,v) ->
	    let a' = TZ.remove a k in
	    let b' = DZ.remove b k in
	    (a',b',good && equiv a' b'))
	  (a,b,true) tv
      in Tester.test (lazy x) "Bad insert"
    in
    List.iter core_test_DZ test_vectors ;
    ()

  let test_choose () = 
    let core_test_DZ tv = 
      let a = List.fold_left (fun d (k,v) -> TZ.insert d k v) TZ.empty tv in
      let b = List.fold_left (fun d (k,v) -> DZ.insert d k v) DZ.empty tv in
      let rec choose_all a b = 
	match DZ.choose b with
	  | None -> TZ.choose a = None
	  | Some (kb,vb,b') ->
	    let a' = TZ.remove a kb in
	    TZ.lookup a kb = Some vb &&
	    DZ.lookup b kb = Some vb &&
	    DZ.lookup b' kb = None &&
	    equiv a' b' && choose_all a' b' 
      in
      Tester.test (lazy (choose_all a b)) "Bad choose"
    in
    List.iter core_test_DZ test_vectors ;
    ()

  let test_member () = 
    let core_test_DZ tv = 
      let (_,_,x) = 
	List.fold_left 
	  (fun (a,b,good) (k,v) ->
	    let a' = TZ.insert a k v in
	    let b' = DZ.insert b k v in
	    (a',b',good && DZ.member b' k && DZ.member b k = TZ.member a k))
	  (TZ.empty,DZ.empty,true) tv
      in Tester.test (lazy x) "Bad insert"
    in
    List.iter core_test_DZ test_vectors ;
    ()

  let test_all () =
    run_test "empty" 2 test_empty ;
    run_test "insert" 8 test_insert ;
    run_test "remove" 8 test_remove ;
    run_test "choose" 8 test_choose ;
    run_test "member" 4 test_member

end

  module Z = 
  struct 
    type key = int
    type value = int
    let compare l r =
      if l < r then Order.Less
      else if l > r then Order.Greater
      else Order.Eq
    let string_of_key _ = ""
    let string_of_value _ = ""
  end

  module Z10 = 
  struct
    type key = int
    type value = int
    let compare l r =
      let l = ((l mod 10) + 10) mod 10 in
      let r = ((r mod 10) + 10) mod 10 in
      if l < r then Order.Less
      else if l > r then Order.Greater
      else Order.Eq
    let string_of_key _ = ""
    let string_of_value _ = ""
  end


module Test_BTDict = DictTest(Z)(Dict.BTDict)
module Test_BTDict10 = DictTest(Z10)(Dict.BTDict)
module Test_MapDict = DictTest(Z)(Dict.MapDict)
module Test_MapDict10 = DictTest(Z10)(Dict.MapDict)

module SetTest (A : Myset.COMPARABLE with type t = int)
  (F : functor (C : Myset.COMPARABLE) ->
    (Myset.SET with type elt = C.t)) =
struct
  let run_test name total test = 
    Tester.reset () ;
    print_string ("(** $$ " ^ name ^ " $$ **)\n") ;
    test () ;
    print_string ("(** $$ Total: " ^ string_of_int (Tester.scale total) ^ " / " ^ string_of_int total ^ " $$ **)")

  module DZ = F(A)
  module TZ = Myset.ListSet(A)

  let test_vectors = [[1];[1;2;3];[0;-1;1;-2;2;3;-3;4;-4];
		      [0;-1;1;-2;2;3;-3;4;-4;10;-11;11;-12;12;13;-13;14;-14]]

  let equiv a b = 
    (DZ.fold (fun k x -> x && TZ.member a k) true b) &&
    (TZ.fold (fun k x -> x && DZ.member b k) true a)

  let test_empty () = 
    Tester.test (lazy (DZ.is_empty DZ.empty)) "empty not empty" ;
    Tester.test (lazy (DZ.is_empty (DZ.singleton 1) = false)) "not empty is empty" ;
    ()

  let test_singleton () =
    List.iter (fun i ->
      let _ = Tester.test (lazy (DZ.member (DZ.singleton i) i = TZ.member (TZ.singleton i) i)) "singleton member" in
      let _ = Tester.test (lazy (DZ.member (DZ.singleton i) (i+1) = TZ.member (TZ.singleton i) (i+1))) "singleton member" in
      ()
    ) [0;1;2;10;11;15]
  
  let test_insert () =
    let core_test_DZ tv = 
      let (_,_,x) = 
	List.fold_left 
	  (fun ((a,b,good) : TZ.set * DZ.set * bool) (k : int) ->
	    try
	      let a' = TZ.insert k a in
	      let b' = DZ.insert k b in
	      (a',b',good && equiv a' b')
	    with
	      | _ -> (a,b,false))
	  (TZ.empty, DZ.empty, true) tv
      in Tester.test (lazy x) "Bad insert"
    in
    List.iter core_test_DZ test_vectors ;
    ()

  let test_remove () = 
    let core_test_DZ tv =
      let a = List.fold_left (fun d k -> TZ.insert k d) TZ.empty tv in
      let b = List.fold_left (fun d k -> DZ.insert k d) DZ.empty tv in
      let (_,_,x) = 
	List.fold_left 
	  (fun (a,b,good) k ->
	    try
	      let a' = TZ.remove k a in
	      let b' = DZ.remove k b in
	      (a',b',good && equiv a' b')
	    with
	      | _ -> (a,b,false))
	  (a,b,true) tv
      in Tester.test (lazy x) "Bad insert"
    in
    List.iter core_test_DZ test_vectors ;
    ()

  let test_choose () = 
    let core_test_DZ tv = 
      let a = List.fold_left (fun d k -> TZ.insert k d) TZ.empty tv in
      let b = List.fold_left (fun d k -> DZ.insert k d) DZ.empty tv in
      let rec choose_all a b = 
	match DZ.choose b with
	  | None -> TZ.choose a = None
	  | Some (kb,b') ->
	    let a' = TZ.remove kb a in
	    TZ.member a kb = true &&
	    DZ.member b kb = true &&
	    DZ.member b' kb = false &&
	    equiv a' b' && choose_all a' b' 
      in
      Tester.test (lazy (choose_all a b)) "Bad choose"
    in
    List.iter core_test_DZ test_vectors ;
    ()

  let test_all () =
    run_test "empty/is_empty" 3 test_empty 
  
end

module CompareZ = 
struct 
  type t = int
  let compare l r =
    if l < r then Order.Less
    else if l > r then Order.Greater
    else Order.Eq
  let string_of_t _ = ""
end

module CompareZ10 = 
struct
  type t = int
  let compare l r =
    let l = ((l mod 10) + 10) mod 10 in
    let r = ((r mod 10) + 10) mod 10 in
    if l < r then Order.Less
    else if l > r then Order.Greater
    else Order.Eq
  let string_of_t _ = ""
end

module Test_MapSet = SetTest (CompareZ) (Myset.DictSet)

(*
let rec crawlJohn (n:int) (frontier: LinkSet.set)
    (visited : LinkSet.set) (d:WordDict.dict) : WordDict.dict = 
  if n = 0 then d else
    match LinkSet.choose frontier with
      | None -> d
      | Some (link,frontier) -> 
	let {url=_;links=lks;words=wds} = get_page link in
	let old_set w d = 
	  match WordDict.lookup d w with
	    | None -> LinkSet.empty
	    | Some s -> s in
	let d = List.fold_left 
	  (fun d w -> WordDict.insert d w (LinkSet.insert link (old_set w d))) d wds in
	let visited = LinkSet.insert link visited in
	let frontier = List.fold_left (fun f l -> if LinkSet.member visited l then f else LinkSet.insert l f) frontier lks in
	crawlJohn (n-1) frontier visited d 
;;

let testCrawl () = 
  let d1 = crawl num_pages_to_search (LinkSet.singleton initial_link) LinkSet.empty
    WordDict.empty 
  and d2 = crawlJohn num_pages_to_search (LinkSet.singleton initial_link) LinkSet.empty
    WordDict.empty in
  true
*)


let main () =
  print_string "(** $$ BTDict Tests $$ **)\n" ;
  Test_BTDict.test_all () ;
  Test_BTDict10.test_all () ;
  print_string "\n\n(** $$ MapDict Tests $$ **)\n" ;
  Test_MapDict.test_all () ;
  Test_MapDict10.test_all () ;
  ()
(*
  print_string "\n\n(** $$ MapSet Tests $$ **)\n" ;
  Test_MapSet.test_all () 
*)
;;

main ()

