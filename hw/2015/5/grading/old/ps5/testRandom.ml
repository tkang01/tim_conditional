open Graph
open Nodescore
open Pagerank
open HelpNS

module JohnRandomWalkRanker (GA: GRAPH) (NSA: NODE_SCORE with module N = GA.N) 
  (P : WALK_PARAMS) : 
  (RANKER with module G = GA with module NS = NSA) =
struct
  module G = GA
  module NS = NSA

  let random_pick l = 
    let n = List.length l in
    List.nth l (Random.int n) 

  let rec random_walk g ns n alpha count =
    if count = 0 then ns 
    else 
      let ns = NS.add_score ns n 1. in
      let neighbors = 
	match (G.neighbors g n) with 
	  | None -> failwith "Node does not exists"
	  | Some v -> v
      in
      if List.length neighbors = 0 || Random.float 1. < alpha 
      then
	random_walk g ns (random_pick (G.nodes g)) alpha (count - 1)
      else
	let n = random_pick neighbors in
        random_walk g ns n alpha (count - 1)
	
  let rank g =   
    let p = 
      match P.do_random_jumps with
	| None -> failwith "No probability..."
	| Some p -> p
    in
    let ns = random_walk g (NS.zero_node_score_map (G.nodes g)) 
      (random_pick (G.nodes g)) p P.num_steps in
    NS.normalize ns
      
end

module Rand = 
struct 
  let do_random_jumps = Some 0.3
  let num_steps = 50
  let debug = false
end
  
module Student = RandomWalkRanker (TestGraph) (TestScore) (Rand)
module Staff = JohnRandomWalkRanker (TestGraph) (TestScore) (Rand)

let gsimple =
  let g = add_nodes [0;1;2] in
  let g = add_edges 0 [1] g in
  let g = add_edges 1 [0] g in
  let g = add_edges 2 [1] g in
  g

let test e =
  let nsref = Staff.rank gsimple in
  Printf.fprintf stderr "*** RandomWalkRanker ***\n";
  Printf.fprintf stderr "res: Random ";
  if try 
       compare nsref (Student.rank gsimple) e
    with
      | _ -> false
  then
    Printf.fprintf stderr "16 / 16\n"
  else
    Printf.fprintf stderr "0 / 16\n"
;;

test 0.2
