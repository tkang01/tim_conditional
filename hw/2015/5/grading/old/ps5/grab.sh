#!/bin/bash

for x in submissions/*
do 
    # echo -n ${x#submissions/}
    # grep -e "res: InDegree" $x/moogle_phase2_grade.txt | awk -F ' ' '{ print " " $2 " " $3 }'
    # echo -n ${x#submissions/}
    # grep -e "res: Random" $x/moogle_phase2_grade.txt | awk -F ' ' '{ print " " $2 " " $3 }'
    # echo -n ${x#submissions/}
    # grep -e "res: Quantum" $x/moogle_phase2_grade.txt | awk -F ' ' '{ print " " $2 " " $3 }'

    ID=`grep -e "res: InDegree" $x/moogle_phase2_grade.txt | awk -F ' ' '{ print $3 }'`
    RA=`grep -e "res: Random" $x/moogle_phase2_grade.txt | awk -F ' ' '{ print $3 }'`
    QU=`grep -e "res: Quantum" $x/moogle_phase2_grade.txt | awk -F ' ' '{ print $3 }'`

    echo ${x#submissions/} $ID $RA $QU

done