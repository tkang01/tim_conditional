#!/bin/bash
# Moogle grading

V=`pwd`
D=shcherb

mkdir -p tmp/submissions
for x in submissions
do

    echo Grading... $x
    #if [ ${x#submissions/} == $D ]
    #then
    #    echo "skipping diverging student"
    #    continue
    #fi

    mkdir -p tmp/$x
    cp -R $x/* tmp/$x
    cp {testInDegree,helpNS,testRandom,testQuantum}.ml tmp/$x

    cp Makefile tmp/$x
    pushd tmp/$x > /dev/null

    for y in InDegree Random Quantum
    do
	#echo Building test$y
	rm -f test$y
	make test$y
    done

    rm -f moogle_phase2_grade.txt
    for y in InDegree Random Quantum
    do
	#echo Testing $y
	if [ -f test$y ]
	then
	    ./test$y 8080 42 $V/simple-html/index.html 2>> moogle_phase2_grade.txt > /dev/null
	else
	    if [ $y == InDegree ]
	    then
		echo res: $y 0 / 4 >> moogle_phase2_grade.txt
	    else
		if [ $y == Random ]
		then
		    echo res: $y 0 / 16 >> moogle_phase2_grade.txt
		else
		    if [ $y == Quantum ]
		    then
			echo res: $y 0 / 11 >> moogle_phase2_grade.txt
		    else
			echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
		    fi
		fi
	    fi
	fi
    done

    popd > /dev/null

    grep "res:" tmp/$x/moogle_phase2_grade.txt | awk -F ' ' '{ points += $3 ; total += $5 } END { print points " / " total }'
    echo -n ":total" ${x#submissions/} " " >> tmp/$x/moogle_phase2_grade.txt
    grep "res:" tmp/$x/moogle_phase2_grade.txt | awk -F ' ' '{ points += $3 ; total += $5 } END { print points " / " total }' >> tmp/$x/moogle_phase2_grade.txt

    cp tmp/$x/moogle_phase2_grade.txt ./$x/moogle_phase2_grade.txt
done
