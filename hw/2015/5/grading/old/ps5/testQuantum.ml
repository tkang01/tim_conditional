open Graph
open Nodescore
open Pagerank
open HelpNS

module JohnQuantumRanker (GA: GRAPH) (NSA: NODE_SCORE with module N = GA.N) 
  (P : QUANTUM_PARAMS) : 
  (RANKER with module G = GA with module NS = NSA) =
struct
  module G = GA
  module NS = NSA

  let propagate_weight n g new_ns old_ns =
    let pushy = 
      match G.neighbors g n with
	| None -> failwith "Propagating weight from a node that does not exists\n"
	| Some l -> n :: l
    in
    let p = 
      match NS.get_score old_ns n with
	| None -> failwith "The nodecore has not been initialized\n"
	| Some p -> p
    in
    let num = float_of_int (List.length pushy) in
    List.fold_left (fun new_ns node -> NS.add_score new_ns n (p /. num)) new_ns pushy 

  let compute_new_ns g ns =
    let nodes = G.nodes g in
    let fresh_ns = NS.normalize (NS.fixed_node_score_map nodes P.alpha) in
    List.fold_left (fun new_ns n -> propagate_weight n g new_ns ns) fresh_ns nodes

  let rec loop g ns n = 
    if n = 0 then ns 
    else loop g (compute_new_ns g ns)  (n - 1)

  let rank g = 
    let nodes = G.nodes g in
    loop g (NS.normalize (NS.fixed_node_score_map nodes 1.)) P.num_steps

  let _ = if P.alpha > 1.0 || P.alpha < 0.0 
  then raise (Failure "alpha must be in [0.0,1.0]") 
  else ()

end

module Quant = 
struct 
  let alpha = 0.01
  let num_steps = 1
  let debug = true
end

module Student = QuantumRanker (TestGraph) (TestScore) (Quant)
module Staff = JohnQuantumRanker (TestGraph) (TestScore) (Quant)

let test e =
  let nsref = Staff.rank gsimple in
  Printf.fprintf stderr "*** QuantumRanker ***\n";
  Printf.fprintf stderr "res: Quantum ";
  if try 
       compare nsref (Student.rank gsimple) e
    with
      | _ -> false
  then
    Printf.fprintf stderr "11 / 11\n"
  else
    Printf.fprintf stderr "0 / 11\n"
;;

test 0.2
