(*** CS 51 Problem Set 4 ***)
(*** March 24, 2013 ***)
(*** Test Suite ***)


(*>+* eat >+*)
module BTDict(D:DICT_ARG) : (DICT with type key = D.key
with type value = D.value) =
struct
(*>+* eat-replacewith >+*)
module D = IntStringDictArg
(*>+* eat-end >+*)


(*>* Problem 3.0 *>*)
(*>>* 25 *>>*)

(* Testing support code copied from dict.ml *)
(* adds a list of (key,value) pairs in left-to-right order *)
let insert_list (d: dict) (lst: (key * value) list) : dict =
List.fold_left ~f:(fun r (k,v) -> insert r k v) ~init:d lst

(* adds a list of (key,value) pairs in right-to-left order *)
let insert_list_reversed (d: dict) (lst: (key * value) list) : dict =
List.fold_right ~f:(fun (k,v) r -> insert r k v) lst ~init:d

(* generates a (key,value) list with n distinct keys in increasing order *)
let generate_pair_list (size: int) : (key * value) list =
let rec helper (size: int) (current: key) : (key * value) list =
  if size <= 0 then []
  else
    let new_current = D.gen_key_gt current () in
    (new_current, D.gen_value()) :: (helper (size - 1) new_current)
in
helper size (D.gen_key ())

(* generates a (key,value) list with keys in random order *)
let rec generate_random_list (size: int) : (key * value) list =
if size <= 0 then []
else
  (D.gen_key_random(), D.gen_value()) :: (generate_random_list (size - 1))

>>>
let d1 = Leaf in
balanced d1

>>>
let d2 = Two(Leaf,D.gen_pair(),Leaf) in
balanced d2

>>>
let d3 = Three(Leaf,D.gen_pair(),Leaf,D.gen_pair(),Leaf) in
balanced d3

>>>
let d4 = Three(Two(Two(Two(Leaf,D.gen_pair(),Leaf),D.gen_pair(),
                       Two(Leaf,D.gen_pair(),Leaf)),
                   D.gen_pair(),Two(Two(Leaf,D.gen_pair(),Leaf),
                                    D.gen_pair(),
                                    Two(Leaf,D.gen_pair(),Leaf))),
               D.gen_pair(),
               Two(Two(Two(Leaf,D.gen_pair(),Leaf),D.gen_pair(),
                       Two(Leaf,D.gen_pair(),Leaf)),D.gen_pair(),
                   Two(Two(Leaf,D.gen_pair(),Leaf),D.gen_pair(),
                       Two(Leaf,D.gen_pair(),Leaf))),D.gen_pair(),
               Two(Two(Two(Leaf,D.gen_pair(),Leaf),D.gen_pair(),
                       Two(Leaf,D.gen_pair(),Leaf)),D.gen_pair(),
                   Three(Two(Leaf,D.gen_pair(),Leaf),D.gen_pair(),
                         Two(Leaf,D.gen_pair(),Leaf),D.gen_pair(),
                         Three(Leaf,D.gen_pair(),Leaf,D.gen_pair(),Leaf))))
in
balanced d4

>>>
let d5 = Two(Leaf,D.gen_pair(),Two(Leaf,D.gen_pair(),Leaf)) in
    not (balanced d5)

>>>
let d6 = Three(Leaf,D.gen_pair(),Two(Leaf,D.gen_pair(),
	Leaf),D.gen_pair(),Leaf) in
    not (balanced d6)

>>>
let d7 = Three(Three(Leaf,D.gen_pair(),Leaf,D.gen_pair(),Leaf),
	D.gen_pair(),Leaf,D.gen_pair(),Two(Leaf,D.gen_pair(),Leaf)) in
    not (balanced d7)

>>>
let pairs1 = generate_pair_list 26 in
    let d1 = insert_list empty pairs1 in
    let foo = List.fold_right ~f:(fun (k,v) sofar ->
	sofar &&(lookup d1 k = Some v)) pairs1 ~init:true in
    foo && (balanced d1)

>>>
let pairs2 = generate_pair_list 13 in
    let d2 = insert_list empty pairs2 in
    let foo = List.fold_right ~f:(fun (k,v) sofar ->
	sofar && (lookup d2 k = Some v)) pairs2 ~init:true in
    foo && (balanced d2)

>>>
let pairs3 = [] in
    let d3 = insert_list empty pairs3 in
    let foo = List.fold_right ~f:(fun (k,v) sofar ->
	sofar &&(lookup d3 k = Some v)) pairs3 ~init:true in
    foo && (balanced d3)

>>>
let pairs6 = generate_pair_list 26 in
    let d6 = insert_list_reversed empty pairs6 in
    let foo = List.fold_right ~f:(fun (k,v) sofar ->
	sofar && (lookup d6 k = Some v)) pairs6 ~init:true in
    foo && (balanced d6)

>>>
let pairs7 = generate_pair_list 13 in
    let d7 = insert_list_reversed empty pairs7 in
    let foo = List.fold_right ~f:(fun (k,v) sofar ->
	sofar && (lookup d7 k = Some v)) pairs7 ~init:true in
    foo && (balanced d7)

>>>
let pairs4 = generate_pair_list 26 in
let update_key = D.gen_key_gt (D.gen_key_gt (D.gen_key ()) ()) () in
let update_value = D.gen_value () in
let new_pairs = pairs4 @ [(update_key,update_value)] in
let d4 = insert_list empty new_pairs in
    let foo = List.fold_right
      ~f:(fun (k,v) sofar ->
        if k = update_key then sofar && (lookup d4 k = Some update_value)
        else sofar && (lookup d4 k = Some v)
      ) new_pairs ~init:true in
	foo && (balanced d4)

>>>
let pairs5 = generate_random_list 100 in
    let d5 = insert_list empty pairs5 in
    let foo = List.fold_right ~f:(fun (k,v) sofar ->
	sofar && (member d5 k)) pairs5 ~init:true in
    foo && (balanced d5)

>>>
let pairs1 = generate_pair_list 26 in
    let d1 = insert_list empty pairs1 in
    let r2 = remove d1 (D.gen_key_lt (D.gen_key()) ()) in
    let foo = List.fold_right ~f:(fun (k,v) sofar ->
	sofar && (lookup r2 k = Some v)) pairs1 ~init:true in
    foo && (balanced r2)

>>>
let d1 = empty in
    let r1 = remove d1 (D.gen_key()) in
   (r1 = empty) && (balanced r1)

>>>
let pairs1 = generate_pair_list 26 in
    let d1 = insert_list empty pairs1 in
    List.iter
      ~f:(fun (k,v) ->
        let r = remove d1 k in
        List.iter
          ~f:(fun (k2,v2) ->
            if k = k2 then assert(lookup r k2 = None)
            else assert(lookup r k2 = Some v2)
          ) pairs1;
        assert(balanced r)
      ) pairs1 ;
      true

>>>
    let pairs1 = generate_pair_list 26 in
    let d1 = insert_list_reversed empty pairs1 in
    List.iter
      ~f:(fun (k,v) ->
        let r = remove d1 k in
        let _ = List.iter
          ~f:(fun (k2,v2) ->
            if k = k2 then assert(lookup r k2 = None)
            else assert(lookup r k2 = Some v2)
          ) pairs1 in
        assert(balanced r)
      ) pairs1 ;
      true

>>>
    let pairs5 = generate_random_list 100 in
    let d5 = insert_list empty pairs5 in
    let r5 = List.fold_right ~f:(fun (k,_) d -> remove d k) pairs5 ~init:d5 in
    let foo = List.fold_right ~f:(fun (k,_) sofar ->
	sofar && (not (member r5 k))) pairs5 ~init:true in
    foo && (r5 = empty) && (balanced r5)

>>>
let pairs1 = generate_pair_list 26 in
    let d1 = insert_list_reversed empty pairs1 in
    let rec choose_n (n: int) (d: dict) : bool =
      if n = 0 then true
      else
        match choose d with
          | None -> false
          | Some (_,_,rest) -> choose_n (n-1) rest
    in
    choose_n 26 d1

>>>
    let pairs1 = generate_pair_list 26 in
    let d1 = insert_list_reversed empty pairs1 in
    let num_elements = fold (fun _ _ r -> r + 1) 0 d1 in
    num_elements = 26




