#!/usr/bin/python
# CS 51 Tools
# Automated grading for OCaml submissions.
#
# N.B.: Unless you're feeling dangerous, best run within a VM, to avoid
# injecting any evil student ML code.

import optparse
import os
import re
import shutil
import string
import subprocess
import sys
import time

MAX_LINE_LEN = 80
WRONG_ANSWER_ERROR_CODE = 17
# A random exit code that we hope is never used by OCaml to denote a crash.
MAX_TIME = 15

summary = []

def load(filename):
    """Load the contents of a file into a list of lines, stripping newlines."""
    f = open(filename)
    ls = []
    for l in f:
        # we want to strip trailing newlines -- this cuts off other
        # trailing spaces too, but that's probably okay
        ls.append(l.rstrip())
    f.close()
    return ls

def in_magic(l, n=1):  # Incoming magic is a line of the form: "(*>* label *>*)"
    """If the line l contains a magical comment, return the magic
    within. Look for a comment containing n angle brackets on each
    side."""
    l = l.strip()

    # if len(l) >= 10 and \
    #         (l[:4] == '(*>*' and l[-4:] == '*>*)' and '*' not in l[4:-4]):
    #     return l[4:-4].strip()
    # else:
    #     return None

    m = re.match(r'\(\*>{%d}\*(.*)\*>{%d}\*\)' % (n,n), l)
    if m:
        return m.group(1).strip()
    return None

def out_magic(s, n=1):  # Outgoing magic is a line of the form: "(*<* label *<*)"
    """
    Return a magical comment containing the input string.
    Input string should not contain newlines.
    """
    st = '<' * n
    return '(*%s* %s *%s*)' % (st, s, st)

def extract_entries(ls):
    """
    extract_entries (ls:line list) :
        (magic_string option * magic_line option * int option * line list) list

    Takes a series of lines (perhaps a whole file's lines) and splits
    it into sections demarcated by magical comment lines: each section
    is a triple (s, l, n, ls'), where s is the magic word heading the
    section (or None if the section is the top of the file), l is the
    entire magical comment line heading the section (containing s), n
    is the number of points the section is worth (marked by (*>>* n
    *<<*)) and ls' is the list of lines contained in the section
    (excluding the initial magical comment line).
    """
    entries = []
    curr_entry = [None, None, 3, []]
    for l in ls:
        s = in_magic(l)
        if s is None:
            curr_entry[3].append(l)
            s2 = in_magic(l, 2)
            if s2 is not None:
                try:
                    curr_entry[2] = int(s2)
                    pass
                except ValueError: #invalid string
                    print 'Warning! Point value "%s" is invalid!"' % s2
                    pass
        else:
            entries.append(curr_entry)
            curr_entry = [s, l, None, []]

    entries.append(curr_entry)
    return map(tuple, entries)

class SubmissionException(Exception):
    """For some reason, we don't like the submission we're grading."""
    def __init__(self, desc):
        self.desc = desc
    def __str__(self):
        return self.desc

def screen_for_evil(ls):
    # In which we pray that if people are evil they are also stupid.
    # TODO: figure out how we actually want to sandbox, if we do at all
    """
    Run simple heuristics over code (a list of lines) to determine whether
    it might be evil. If so, raise a SubmissionException.
    """
    flags = []
    for (i, l) in enumerate(ls):
        s = l.strip()
        # XXX: allowing Sys, because people used Sys.time
        #if 'Sys.' in s or 'open Sys' in s or \
        #if 'Unix.' in s or 'open Unix' in s: # or (s != '' and s[0] == '#'):
        #    raise SubmissionException(
        #        'Code processed may be evil: line %d reads "%s"' % (i, l))

def write_line(f, l):
    f.write('%s\n' % l)

def pedantic_write_line(f, l):
    """Write line l to file (handle) f, complaining if the line is too long."""
    write_line(f, l)
    n = len(l)
    if n > MAX_LINE_LEN:
        write_line(f, out_magic('Preceding line is %d (> %d) characters long.'
                                % (n, MAX_LINE_LEN)))

def parse_testing(problem_body_lines):
    """
    parse_testing : line list -> line list * line list list

    Takes a chunk of testing code and splits it into
    general support code and individual tests.
    For instance:

    parse_testing(
      ["foo bar",
       "",
       ">>>",
       "baz",
       "qux",
       "",
       "frob"])
    =
    (["foo bar", "", "frob"], [["baz", "qux"]])
    """
    testing_support = []
    tests = []
    curr_test = None
    for l in problem_body_lines:
        if l.strip() == '>>>':  # Triple right angle brackets begin tests.
            curr_test = []
            tests.append(curr_test)
        elif l == '' and curr_test is not None:
            # Blank lines terminate tests.
            curr_test = None
        elif curr_test is None:
            testing_support.append(l)
        else:
            curr_test.append(l)
    return (testing_support, tests)

def sanitize(s):
    """
    sanitize : string -> string
    Replaces potentially-un-filename-safe characters with underscores.
    """
    return ''.join(
        map(lambda c: c if c in (string.letters + string.digits) else '_', s))

def calc_points(correct, total, pts):
    """Compute the number of points (out of pts) to assign, if
    correct/total tests are correct."""
    return correct * pts / total

def grade(fname_in, fname_tests, fname_out, fname_out_run_temp, fname_driver_temp_prefix):
    """
    Grades the file fname_in,
    using fname_tests for test cases
    (and testing support code: see parse_testing, above),
    imperatively writing the output (graded file) to fname_out,
    and using temporarily files:
    fname_out_run_temp, fname_driver_binary_temp,
    and multiple files starting with fname_driver_temp_prefix,
    one for each section
    (demarcated by magic comment lines, which usually denote problem names).

    In a nutshell, this works by matching the magic comment line names
    between fname_tests and fname_in (raising SubmissionException if they
    don't match exactly);
    maintaining an OCaml file f_out_run_temp, which is gradually
    filled in with the input fname_in's contents, section by section;
    parsing the testing code for each section using parse_testing, above;
    and for each section, copying f_out_run_temp into a scratch file
    beginning with the prefix fname_driver_temp_prefix,
    appending the relevant testing code, and calling out to ocamlc
    to get the output (or error).
    Results are progressively written (interspersed with a pedantic
    copy of the initial code from fname_in) in "outgoing magic" lines
    (see above) into fname_out.
    """
    compiles = True
    if not os.path.exists(fname_in):
        raise SubmissionException('File to be graded does not exist: %s' % fname_in)
    ls = load(fname_in)
    screen_for_evil(ls)
    ls_entries = extract_entries(ls)
    test_ls = load(fname_tests)
    test_ls_entries = extract_entries(test_ls)
    if len(ls_entries) != len(test_ls_entries):
        raise SubmissionException('Wrong number of magic lines.  Expected %d '
                                  % len(test_ls_entries) + 'lines, found %d.'
                                  %len(ls_entries))

    f_out = open(fname_out, 'w')
    f_out_run_temp = open(fname_out_run_temp, 'w')
    run_temp_lines = []
    reached_end_of_module = False
    eaten = False
    for ((name, name_line, pts, body_lines), (t_name, t_name_line, t_pts, t_body_lines)) \
            in zip(ls_entries, test_ls_entries):
        # For each section (demarcated with magic comment lines):
        if name is None:
            # If it is the initial section (before the first magic
            # comment line), just copy it over and don't do any testing.

            to_eat = None
            to_replace_with = None
            recording_to_eat = False
            recording_to_replace_with = False
            saved_to_replace_with = None
            for l in t_body_lines:
                if l == "(*>+* eat >+*)":
                    recording_to_eat = True
                    to_eat = []
                elif l == "(*>+* eat-replacewith >+*)":
                    recording_to_eat = False
                    recording_to_replace_with = True
                    to_replace_with = []
                elif l == "(*>+* eat-end >+*)":
                    recording_to_eat = False
                    recording_to_replace_with = False
                elif recording_to_eat:
                    to_eat += [l]
                elif recording_to_replace_with:
                    to_replace_with += [l]
                else:
                    write_line(f_out_run_temp, l)
            write_line(f_out_run_temp, 'let _ = () ;;')
            for l in body_lines:
                # XXX: DRY
                if to_eat is not None and len(to_eat) > 0 and l == to_eat[0]:
                    to_eat.pop(0)
                    eaten = True
                    if to_replace_with is not None:
                        # Output just to f_out_run_temp, not to f_out
                        # (This is only for running testing, not for
                        # printing in the student's graded file)
                        for rl in to_replace_with:
                            write_line(f_out_run_temp, rl)
                        saved_to_replace_with = to_replace_with
                        to_replace_with = None
                # If we've eaten a module definition, then the next time
                # we see 'end', we want to clean up.
                elif eaten and l == "end":
                    reached_end_of_module = True
                    eaten = False
                elif not reached_end_of_module:
                    write_line(f_out_run_temp, l)

                run_temp_lines += [l]
                pedantic_write_line(f_out, l)
            write_line(f_out_run_temp, 'let _ = () ;;')
            continue
        if name != t_name:
            raise SubmissionException('Expected magic line "%s", found "%s"' %
                                      (t_name, name))
        pedantic_write_line(f_out, name_line)
        write_line(f_out_run_temp, name_line)
        run_temp_lines += [name_line]


        if reached_end_of_module:
            f_out_run_temp.close()
            f_out_run_temp = open(fname_out_run_temp, 'w')
            for line in run_temp_lines:
                write_line(f_out_run_temp, line)

        reached_end_of_module = False
        for l in body_lines:
            # XXX: DRY
            if to_eat is not None and len(to_eat) > 0 and l == to_eat[0]:
                to_eat.pop(0)
                eaten = True
                if to_replace_with is not None:
                    # Output just to f_out_run_temp, not to f_out
                    # (This is only for running testing, not for
                    # printing in the student's graded file)
                    for rl in to_replace_with:
                        write_line(f_out_run_temp, rl)
                    saved_to_replace_with = to_replace_with
                    to_replace_with = None
            # If we've eaten a module definition, then the next time
            # we see 'end', we want to clean up.
            elif eaten and l == "end":
                reached_end_of_module = True
                eaten = False
                if saved_to_replace_with is not None:
                    to_replace_with = saved_to_replace_with
            elif not reached_end_of_module:
                write_line(f_out_run_temp, l)

            run_temp_lines += [l]

        f_out_run_temp.flush()
        (testing_support, tests) = parse_testing(t_body_lines)
        failures = []
        for test_lines in tests:
            # Reset compiles flag each time so people aren't punished if
            # one problem fails (e.g. if they left it out.) Kind of a hack.
            compiles = True
            # Run each test, keeping track of the failures
            # (so we can write them all out, in fname_out,
            # just below the section's magic comment line).
            test = '\n'.join(test_lines)
            fname_driver_temp = '%s_%s.ml' % (fname_driver_temp_prefix,
                                              sanitize(name))
            fname_driver_binary_temp = '%s_%s.byte' % (fname_driver_temp_prefix,
                                              sanitize(name))
            shutil.copy(fname_out_run_temp, fname_driver_temp)
            f_driver_temp = open(fname_driver_temp, 'a')
            # Just in case the end of the submission doesn't satisfy
            # OCaml's picky parser, make sure we add no-op code with a ';;'.
            write_line(f_driver_temp, 'let _ = () ;;')
            for l in testing_support:
                f_driver_temp.write('%s\n' % l)
            write_line(f_driver_temp, 'let _ = () ;;')
            write_line(f_driver_temp,
                       (('let success = (%s) in\n'
                         'exit (if success then 0 else %s)') %
                        (test, WRONG_ANSWER_ERROR_CODE)))
            f_driver_temp.close()

            slash = fname_driver_binary_temp.rindex('/')
            directory = fname_driver_binary_temp[:slash]
            programname = fname_driver_binary_temp[slash+1:]
            curdir = os.getcwd();

            os.chdir(directory)
            driver_proc = subprocess.Popen(
                # SKM 2/16/12 don't check asserts so student asserts
                # don't break code.
                ['corebuild', '-quiet', '-cflag', '-noassert', programname], #stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                stdout=sys.stdout, stderr=sys.stderr)
            os.chdir(curdir)

            time_taken = 0
            compile_ret = None
            while (compile_ret is None):
                if time_taken > MAX_TIME:
                    driver_proc.kill()
                    break
                time.sleep(0.01)
                time_taken += 0.01
                driver_proc.poll()
                compile_ret = driver_proc.returncode
            if compile_ret is None:
                compile_ret = 1
            if compile_ret != 0:
                failures.append(('compile error', test))
                compiles = False
            else:
                driver_proc = subprocess.Popen([fname_driver_binary_temp],
                                          stdout=subprocess.PIPE,
                                          stderr=subprocess.PIPE)
                run_ret = None
                while (run_ret is None):
                    if time_taken > MAX_TIME:
                        print >> sys.stderr, "Giving up on test. Possible infinite loop."
                        driver_proc.kill()
                        break
                    time.sleep(0.05)
                    time_taken += 0.05
                    driver_proc.poll()
                    run_ret = driver_proc.returncode
                if run_ret is None:
                    failures.append(('possible infinite loop', test))
                elif run_ret == WRONG_ANSWER_ERROR_CODE:
                    failures.append(('wrong answer', test))
                elif run_ret != 0:
                    # TODO: does OCaml always exit 2 on exception? It seems to.
                    # If so, we can raise a red flag here if run_ret != 2.
                    # (Actually, this is already a red flag.
                    # But we could raise a crimson flag.)
                    failures.append(('crashed', test))
        n_tests = len(tests)
        if n_tests != 0:
            passed = n_tests - len(failures)
            pts = calc_points(passed, n_tests, t_pts)
            result = '%s Passed: %d/%d Points: %d/%d' % (name, passed, n_tests,
                                                         pts, t_pts)
            summary.append(result)
            print result
            write_line(f_out, out_magic('Passed %d of %d test(s).' %
                                        (passed, n_tests)))
            write_line(f_out, out_magic('Got %d of %d points.' %
                                        (pts, t_pts), 2))
        for (reason, orig_test) in failures:
            write_line(f_out, out_magic('Failed (%s):' % reason))
            write_line(f_out, out_magic(orig_test))
        for l in body_lines:
            pedantic_write_line(f_out, l)
    f_out.close()
    if (compiles):
        return None
    else:
        return "compile error"

def grade_all(asst_name, submissions_dir, tests_full_path, fname_fail_log,
              uids, debug=False):
    """
    Grades all the submissions in submissions_dir
    (or only those matching the user ids in the uids list, if nonempty),
    given the name of the submitted ML file (ml_name),
    the full path to the file containing test cases and testing support code
    in the format described above (tests_full_path),
    and a path to store output of any failures (fname_fail_log).
    Progress updates will be written to stdout.
    It will expect submissions_dir to have the following structure:

    .
    `-- submissions_dir
        |-- jhacker1
        |   `-- ml_name.ml
        `-- jscheme
            `-- ml_name.ml

    and will interpret each non-hidden subdirectory as a student's uid.
    Suppose that jhacker1's submission was evil, and our script detected it.
    Then (assuming fname_fail_log is a path in the current directory)
    the resulting structure would be:

    .
    |-- fname_fail_log
    `-- submissions
        |-- jhacker1
        |   `-- ml_name.ml
        `-- jscheme
            |-- ml_name.ml
            `-- ml_name_graded.ml

    where fname_fail_log recounts the evil found in jhacker's solution.
    If the debug flag is set, then temporary files used in the process
    will be retained; otherwise, they will be nuked after grading.
    """
    f_fail_log = None
    bad_submissions = []
    if os.path.exists(fname_fail_log):
        os.remove(fname_fail_log)
    for username in os.listdir(submissions_dir):
        if username.startswith('.'):
            continue  # Ignore hidden directories, such as .svn.
        if uids != [] and username not in uids:
            continue
        base_dir = os.path.join(submissions_dir, username)
        if os.path.isdir(base_dir):
            temp_dir = os.path.join(base_dir, 'temp')
            if not os.path.isdir(temp_dir):
                os.mkdir(temp_dir)
            os.chdir(temp_dir)
            grading_output = os.path.join(base_dir, '%s_graded.ml' % asst_name)
            try:
                print >> sys.stderr, 'Starting %s' % username
                compiles = grade(os.path.join(base_dir, '%s.ml' % asst_name),
                      tests_full_path,
                      grading_output,
                      os.path.join(temp_dir, 'run_temp.ml'),
                      os.path.join(temp_dir, 'driver_temp'))
                if compiles is not None:
                    print >> sys.stderr, '%s: %s' % (username, compiles)
                    bad_submissions.append(username + ': ' + compiles)
                print >> sys.stderr, 'Graded %s' % username

            except SubmissionException, e:
                if os.path.exists(grading_output):
                    os.remove(grading_output)
                error_str = '%s: %s' % (username, e.desc)
                print >> sys.stderr, 'FAIL -- %s' % error_str
                if f_fail_log is None:
                    f_fail_log = open(fname_fail_log, 'w')
                write_line(f_fail_log, error_str)
                bad_submissions.append(username + ': ' + error_str)
            if not debug:
                shutil.rmtree(temp_dir)
    if f_fail_log is not None:
        f_fail_log.close()
    return bad_submissions

def usage():
    print 'Usage: grade.py [-d|--debug]'
    sys.exit(255)

if __name__ == '__main__':
    my_path = os.path.dirname(os.path.realpath(__file__))
    parser = optparse.OptionParser()
    parser.add_option('-d', '--debug', action='store_true',
                      dest='debug', default=False,
                      help='Leave temp files around for debugging')
    parser.add_option('-u', '--uid', action='append',
                      dest='uids', default=[],
                      help='Grade submissions for specific uid(s)')
    opts, args = parser.parse_args(sys.argv[1:])
    opts.uids = [] if opts.uids == [] else opts.uids[0].split(',')
    ps_number = '5'
    ps_grade_path = os.path.join(my_path, '..')
    ps_soln_path = os.path.join(my_path, ps_number)
    parts = ['dict', 'myset']
    bad_submissions = {}
    for part in parts:
        print >> sys.stderr, '*** GRADING PART: %s' % part
        bad = grade_all(part,
                  ps_grade_path,
                  os.path.join(ps_soln_path, '%s_tests.mml' % part),
                  os.path.join(ps_grade_path, '%s_fail_log.txt' % part),
                  uids=opts.uids, debug=opts.debug)
        bad_submissions[part] = bad
    print >> sys.stderr, '*** BAD SUBMISSIONS ***'
    for part in parts:
        print >> sys.stderr, '* Part: %s *' % part
        for badsub in bad_submissions[part]:
            print >> sys.stderr, ' %s' % badsub
    print "SUMMARY:"
    for res in summary:
        print res
