#!/bin/bash
# need to be in upper level pset directory
#SUBMISSIONS=$(ls 5)
SUBMISSIONS="5"
ORDERMODULE="(* APPENDED HERE FOR GRADING *)\n
module Order = struct\n
  open Core.Std\n\n
  let string_compare x y =\n
    let i = String.compare x y in\n
    if i = 0 then Equal else if i < 0 then Less else Greater\n
\n
  let int_compare x y =\n
    let i = x - y in\n
    if i = 0 then Equal else if i < 0 then Less else Greater\n
end"

for submission in $SUBMISSIONS
do
    #DIR=5 /$submission
    DIR=5
    if [ ! -f $submission/myset.ml ] || [ ! -f $submission/dict.ml ]; then
        continue
    fi
    echo "performing... $submission"
    # Append Order the top, and add the magic markers to dict and myset
    mv $DIR/dict.ml $DIR/dict_orig.ml
    echo -e $ORDERMODULE > $DIR/dict.ml
    cat $DIR/dict_orig.ml >> $DIR/dict.ml
    sed -i.bak s/"(\*       TESTS                                                      \*)"/"(\*>\* Problem 3.0 \*>\*)"/g $DIR/dict.ml
    sed -i.bak s/"(\* You must write more comprehensive tests, using our remove tests  \*)"/"(\*>>\* 25 \*>>\*)"/g $DIR/dict.ml

    mv $DIR/myset.ml $DIR/myset_orig.ml
    echo -e $ORDERMODULE > $DIR/myset.ml
    echo -e "module Dict = struct" >> $DIR/myset.ml
    cat $DIR/dict_orig.ml >> $DIR/myset.ml
    echo -e "end" >> $DIR/myset.ml
    cat $DIR/myset_orig.ml >> $DIR/myset.ml
    sed -i.bak s/"(\* Tests for our DictSet functor                                \*)"/"(\*>\* Problem 2.0 \*>\*)"/g $DIR/myset.ml
    sed -i.bak s/"(\* Use the tests from the ListSet functor to see how you should \*)"/"(\*>>\* 5 \*>>\*)"/g $DIR/myset.ml

    rm $DIR/*.bak
done

