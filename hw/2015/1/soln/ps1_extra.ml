(* 2a: Reversed *)

(* Solution 1
 * This solution needs improvement.
 * Pros: Correctness
 * Cons: Unnecessary second match statement, shadowing xs, if/then/else styling
 *)
let rec reversed (xs : int list) : bool =
  match xs with
  | [ ] -> true
  | x :: xs -> (
     match xs with
     | [ ] -> true
     | x' :: xs' ->
    if x >= x'
    then reversed xs
    else false
  )
;;

(* Solution 2
 * This is the most likely solution for a student in CS51.
 * Pros: Readability, tail recursive
 * Cons: Could combine first two match cases, could use 'as' keyword
 *)
let rec reversed (lst : int list) : bool =
  match lst with
  | [] -> true
  | [_] -> true
  | hd :: (e1 :: tail) ->
    if hd >= e1 then reversed (e1 :: tail) else false
;;

(* Solution 3
 * This is the staff solution as of 2015.
 * Pros: Uses 'as' and _ effectively, clever, good style on first two match
 * cases
 * Cons: Not tail recursive, likely less intuitve than solution 2
 *)
let rec reversed (lst : int list) : bool =
  match lst with
  | []
  | [_] -> true
  | hd1 :: (hd2 :: _ as tl) -> hd1 >= hd2 && reversed tl
;;

(* 2b: Merge *)

(* Solution 1
 * This solution has poor style.
 * Pros: Correctness
 * Cons: Second and third match cases should use underscores and lst1/lst2
 * instead of x :: y
 *)
let rec merge (lst1 : int list) (lst2 : int list) : int list =
  match lst1, lst2 with
  | [], [] -> []
  | hd :: tl, [] -> hd :: tl
  | [], hd2 :: tl2 -> hd2 :: tl2
  | hd :: tl, hd2 :: tl2 ->
     if hd < hd2
       then hd :: merge (hd2 :: tl2) tl
       else hd2 :: merge (hd :: tl) tl2
;;

(* Solution 2
 * This is the staff solution as of 2015.
 * Most students will have a very similar solution for the recursive case,
 * though the function overall may not be as clean.
 *)
let rec merge (a : int list) (b : int list) : int list =
  match (a, b) with
  | (_, []) -> a
  | ([], _) -> b
  | (hd1 :: tl1, hd2 :: tl2) ->
          if hd1 < hd2 then hd1 :: merge tl1 b else hd2 :: merge a tl2
;;

(* 2c: Unzip *)

(* Unzip is an entirely different way of thinking about recursion from
 * that which is required for the other problems students have seen. The
 * solutions end up being very similar, but it can be challenging to guide
 * students to the solution. *)

(* Solution 1
 * This solution exhibits poor style.
 * Pros: Correctness
 * Cons: Uses 'match' where 'let' is preferable
 *)
let rec unzip (pair_of_lst : (int * int) list) : int list * int list =
  match pair_of_lst with
  | [] -> ([],[])
  | (x, y) :: tl ->
    (match unzip tl with
     |(xlist, ylist) -> (x::xlist, y::ylist))
;;

(* Solution 2
 * This solution exhibits even poorer style.
 * Pros: Correctness
 * Cons: Uses 'match' where 'let' is preferable, uses 'match' on 'x_head'
 * instead of splitting tuple in original pattern matching
 *)
let rec unzip (xs : (int * int) list) : (int list) * (int list) =
  match xs with
  | [] -> ([], [])
  | x_head :: x_tail ->
     match unzip x_tail with
     | (yt, zt) ->
    match x_head with
    | (first, second) -> (first :: yt, second :: zt);;

(* Solution 3
 * This is the staff solution as of 2015. Most students will end up
 * with a very similar solution, though getting there is not an easy journey.
 *)
let rec unzip (lst : (int * int) list) : int list * int list =
  match lst with
  | [] -> ([], [])
  | (l, r) :: tl ->
    let (left, right) = unzip tl in (l :: left, r :: right)

(* 2d: Variance *)

(* Students should write helper functions in their solutions to variance.
 * Doing so makes the solution relatively straightforward, as it follows
 * from the mathematical formula. One thing to keep in mind is the number
 * of passes the student makes over the list. *)

(* Solution 1
 * This solution (helper functions not included) is very clear, but
 * makes more passes over the list than are necessary. See the staff
 * solution for a more efficient approach.
 * Pros: Readability
 * Cons: Efficiency
 *)
let variance (lst : float list) : float option =
  let len = length lst in
  if len < 2 then
    None
  else
    let m = mean lst len in
    Some (sum (square (subtract lst m)) /. float (len - 1))

(* 2e: Few Divisors *)

(* Solution 1
 * This solution looks for divisors by checking integers from n to 1.
 * Its efficiency could be improved by stopping at n/2, as in the staff
 * solution.
 *
 * Per the 2015 CS51 Style Guide, this is also a place where if/then is
 * preferable to a match statement.
 *
 * count_divisors would also be better defined inside few_divisors
 *)
let rec count_divisors (n : int) (i : int) =
  match i with
  | 1 -> 1
  | _ -> (if (n mod i) = 0 then 1 else 0) + count_divisors n (i - 1)

let few_divisors (n : int) (m : int) : bool =
  count_divisors n n < m

(* Solution 2
 * This is the staff solution as of 2015. It is relatively readable and
 * efficient.
 *)
let few_divisors (n:int) (m:int) : bool =
  let rec count_divs (div : int) : int =
    if div > n/2 then 1 else
      (if n mod div = 0 then 1 else 0) + count_divs (div + 1) in
  count_divs 1 < m

(* 2f: Concat List *)

(* Solution 1
 * This problem is likely the easiest on the homework, and could be a
 * good place for students to start if they need to gain some confidence.
 *
 * The most common mistakes are stylistic, such as the one commented
 * out in the solution below.
 *)
let rec concat_list (sep:string) (lst:string list) : string =
  match lst with
  | [] -> ""
  (* | hd :: [] -> hd *)
  | [hd] -> hd
  | hd :: tl -> hd ^ sep ^ (concat_list sep tl)

(* 2g: To Run Length *)

(* Solution 1
 * This is the staff solution as of 2015. Most students arrive at a similar
 * solution.
 *)
let rec to_run_length (lst : char list) : (int * char) list =
  match lst with
  | [] -> []
  | hd :: tl ->
    match to_run_length tl with
    | [] -> [(1,hd)]
    | (n, c) :: tl as tl' -> if hd = c then (n+1, c)::tl else (1, hd)::tl'

(* 2h: From Run Length *)

(* Solution 1
 * This solution uses a match with only two cases, and an if statement.
 * This is not as clean as the three case match in Solution 2.
 *)
let rec from_run_length (runlength : (int * char) list) : char list =
  match runlength with
  | [] -> []
  | (num, char) :: xs -> if num = 1 then char :: from_run_length xs
    else char :: from_run_length ((num-1, char) :: xs)
;;

(* Solution 2
 * This is the staff solution as of 2015. It's nice.
 *)
let rec from_run_length (lst : (int * char) list) : char list =
  match lst with
  | [] -> []
  | (0, _) :: tl -> from_run_length tl
  | (i, c) :: tl -> c :: from_run_length ((i-1, c) :: tl)

(* 3: Permutations *)

(* This is one of the first problems in this problem set to have a variety of
 * solutions, in part depending on the student's familiarity with higher order
 * functions.
 *)

(* Solution 1
 * This is the 2015 staff solution, and is representative of the solutions
 * submitted by students that are comfortable with map.
 *)
let rec interleave (n : int) (lst : int list) : int list list =
  match lst with
  | [] -> [[n]]
  | x :: xs -> (n :: x :: xs) :: List.map ~f:(fun l -> x :: l) (interleave n xs)

let rec permutations (lst : int list) : int list list =
  match lst with
  | [] -> [[]]
  | x :: xs -> List.concat (List.map ~f:(interleave x) (permutations xs))
;;

(* Solution 2
 * This solution uses an additional helper function instead of map.
 *)
let rec interleave (x : int) (hd : int list) (tl : int list) : int list list =
  match tl with
  | [] -> [List.rev_append hd [x]]
  | e :: tl' -> (List.rev_append hd (x :: tl)) :: (interleave x (e :: hd) tl')

let rec interleave_all (x : int) (ps : int list list) : int list list =
  match ps with
  | [] -> []
  | p :: ps' -> (interleave x [] p) @ (interleave_all x ps')

let rec permutations (xs : int list) : int list list =
  match xs with
  | [] -> []
  | x :: [] -> [[x]]
  | x :: xs' -> interleave_all x (permutations xs')
