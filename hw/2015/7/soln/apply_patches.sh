#!/bin/bash
for file in *.patch; do
	mlfile=${file/.patch/.ml}
	cp ../release/$mlfile $mlfile
	patch $mlfile $file
done
