open WorldObject
open WorldObjectI

(** Bouncy bees will travel in a straight line in a random direction until an
    obstacle or edge of the world is reached, at which point a new random
    direction will be chosen. *)
class bee_bouncy p hive : Bee.bee_t =
object (self)
  inherit Bee.bee p hive as super

  (******************************)
  (***** Instance Variables *****)
  (******************************)

  (* ### TODO: Part 5 Smart Bees *)
  val mutable dir = Direction.random World.rand

  (********************************)
  (***** WorldObjectI Methods *****)
  (********************************)

  (* ### TODO: Part 5 Smart Bees *)
  method get_name = "bee_bouncy"

  (***********************)
  (***** Bee Methods *****)
  (***********************)

  (* ### TODO: Part 5 Smart Bees *)
  method private next_direction_default =
    let next_pos = Direction.move_point self#get_pos (Some dir) in
    if World.can_move next_pos then Some dir
    else begin
      dir <- Direction.random World.rand ;
      self#next_direction_default
    end

end


