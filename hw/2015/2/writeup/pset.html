<html>
<head>
    <link rel="stylesheet" type="text/css" href="css/cs51.css" />
    <link rel="stylesheet" type="text/css" href="css/sh_style.css" />
    <script type="text/javascript" src="js/sh_main.min.js"></script>
    <script type="text/javascript" src="js/sh_caml.min.js"></script>
    <title>Problem Set 2</title>
</head>

<body>
<div class=column>

    <div id="rightframe">
        <div class="header">
	    <h1>Moothematics and Higher Order Functions</h1>
            <h2>CS51 Problem Set 2</h2>
        </div>
            <h3>Due Friday, February 20 at 5pm (Sunday, February 22 at 11:59pm for Extension).</h3>
        <hr />

<h3><span lang="en-us">Introduction</span></h3>
<p class="MsoNormal"><span lang="en-us">In this assignment, you will be
given the opportunity to use higher order functions in Ocaml
including map, fold, and filter. You will also write your own language for symbolic
differentiation. In doing this, you will exercise
important features of functional languages, such as recursion,
pattern matching, and list processing, and you will gain the
ability to design your own mini-language.</span></p>

<h3>Downloading</h3>
<p>To download Problem Set 2, navigate to the folder you created in
PS0 (the folder that contains the folders "0" and "1"). Once
there, execute:
    <pre><code>git pull git@code.seas.harvard.edu:cs51-2015/psets.git master</code></pre>
This will download PS2 in a folder named "2".</p>

<p>For this assignment, you will be working in two different files:
<ul>
<li><code>mapfold.ml</code>: a self-contained series of exercises involving higher order functions.
<li><code>expression.ml</code>: a number of functions that will allow you to perform symbolic differentiation of algebraic expressions. Support code can be found in <tt>ast.ml</tt> and <tt>expressionLibrary.ml</tt>.
</ul>

<p>
  Before starting, make sure you have a correct .emacs file. This will
  allow you to run the toplevel directly from emacs (and prevent any errors
  with the line 'open Core.Std'). Please follow the
  instructions on Piazza <a href="https://piazza.com/class/i55hhqy3vmr1xk?cid=240">here</a>.
</p>

<h3>Reminders</h3>
<ul>
  <li> <strong>Testing is required.</strong>
As with last week, we ask that you explicitly add tests to your code. For this purpose, Ocaml provides the function "assert" which takes a bool, does nothing if the bool is true, and throws an error if the bool is false. We have included an example of assert in problem 1.2.a. Begin by uncommenting this example after you write negate_all. Then, we expect at least 1 test per code path (every match case/if else case) for every function that you write on this problem set. Please put the tests just below the function being tested.</li>
  <li><strong>Compilation</strong>
    <ul>
      <li><tt>make mapfold</tt> compiles only Part 1.</li>
      <li><tt>make expression</tt> compiles only Part 2.</li>
      <li><tt>make</tt> and <tt>make all</tt> compile everything.
	These two commands are exactly the same, so it doesn't matter
	which one you use.</li>
    </ul>
  </li>
  <li><strong>Part 2.</strong>
  Part 2 may prove quite challenging, so we encourage you to start early and
  ask questions if you get stuck.</li>
  <li><strong>Time Estimates.</strong>
  Please estimate how much time you spent on each part of the problem set
  by editing the line: <code>let minutes_spent_on_part_X : int = _ ;;</code> at the
  bottom of each file.</li>
  <li><strong>Style.</strong>
  Style is one of the most important aspects of this problem set. As such,
  <strong>style will be a significant portion of your grade.</strong> Please
  refer to the
  <a href="http://sites.fas.harvard.edu/~cs51/style_guide.html">CS51 Style Guide</a>
  to ensure the quality of your code.</li>
</ul>

<div class="info" style="text-align:left">
  As in the previous assignment, the following must be true for you to receive
  full credit and prevent delays in grading:
  <ul>
    <li>Your code compiles without warnings.</li>
    <li>Your code adheres to the CS51 Style Guide.</li>
    <li>You have written one test per code path per function.</li>
    <li>You have not changed any lines needed by our automated correctness
    grader.</li>
    <li>Your functions have the correct names and number of arguments. Do not
    change the signature of a function that we have provided!</li>
  </ul>
</div>

<h3>Part 1: Higher Order Functions (<tt>mapfold.ml</tt>)</h3>
<p>Mapping, folding, and filtering are important techniques in functional languages that allow the programmer
to abstract out the details of traversing a list.  Each can be used to accomplish a great deal with very little code.
In this part, you will create a number of functions using the higher-order functions map and fold.
<ul>
  <li>map is implemented in Ocaml by the function <code>List.map</code>.</li>
  <li>filter is implemented in Ocaml by the function <code>List.filter</code>.</li>
  <li>fold is implemented in OCaml by the function <code>List.fold_right</code>.</li>
</ul>

<p>All instructions for Part 1 can be found in <code>mapfold.ml</code></p>

<a name="part2"></a>
<h3>Part 2: A Language for Symbolic Differentiation (<tt>expression.ml</tt>)</h3>

<p>In the summer of 1958, John McCarthy
(recipient of the Turing Award in 1971) made a major contribution to the
field of programming languages. With the objective of writing a program that
performed symbolic differentiation of algebraic expressions in a effective way,
he noticed that some features that would have helped him to accomplish this task
were absent in the programming languages of that time. This led him to the
invention of LISP (published in Communications of the ACM in 1960) and other
ideas, such as list processing (the name Lisp derives from "List Processing"),
recursion and garbage collection, which are essential to modern programming
languages, including Java. Nowadays, symbolic differentiation of algebraic
expressions is a task that can be conveniently accomplished on modern
mathematical packages, such as Mathematica and Maple.</p>

<p>In Part 2, your mission is to build a language that can
differentiate and evaluate symbolically represented mathematical expressions
that are functions of a single variable.
Symbolic expressions consist of numbers,
variables, and standard math functions (<i>i.e.,</i> plus, minus, times, divide, sin, cos, etc.).</p>

<strong>Conceptual Overview</strong>
<p>To get you started, we have provided
the datatype that defines the abstract syntax tree for such expressions in <code>ast.ml</code>.</p>

<pre>(* abstract syntax tree *)

(* Binary operators. *)
type binop = Add | Sub | Mul | Div | Pow ;;

(* Unary operators. *)
type unop = Sin | Cos | Ln | Neg ;;

type expression =
  | Num of float
  | Var
  | Binop of binop * expression * expression
  | Unop of unop * expression
;;</pre>
<p><span lang="en-us">
    <code>Var</code> represents an occurrence of the single variable &quot;x&quot;.<br>
    <code>Unop(Ln, Var)</code> represents the natural logarithm of <code>x</code>.<br>
    <code>Neg</code> is negation, and is denoted by the &quot;~&quot; symbol
    (&quot;-&quot; is only used for subtraction). The rest should be
    clear.
    Mathematical expressions can be constructed using the
    constructors in the above datatype definition. For example, the expression
    <code>&quot;x^2 + sin(~x)&quot;</code> can be
    represented as:
</span></p>
<pre> Binop(Add, Binop(Pow, Var, Num(2.0)), Unop(Sin, Unop(Neg, Var)))</pre>

<p>This represents a tree where nodes are the type constructors
and the children of each node are the specific operator to use and the arguments of that
constructor. Such a tree is called an abstract syntax tree (or AST for short).
</p>

<a name="part2compile"></a>
<h3>How to Compile and Test Part 2</h3>
<p>The code you will be editing is in <code>expression.ml</code>.</p>

<p>
  You can compile and test from the terminal. To use this method,
  navigate to the folder containing your code and
  run <tt>make expression</tt> (or just <tt>make</tt> if you don't mind
  compiling mapfold.ml as well). You can then run the compiled code with
  <tt>./expression.native</tt>. You should rely on <tt>assert</tt> to
  do your testing. As a helpful note, rather than testing that you get a particular
  float value, you may want to <tt>assert</tt> that it is within a small epsilon of the
  answer you expect. This is necessary to avoid small differences due to the imprecision of
  <tt>float</tt>, and can save you a headache down the road.
  You can also use printing functions such as <tt>print_endline</tt> (more
  <a href="http://caml.inria.fr/pub/docs/manual-ocaml/libref/Pervasives.html">here</a>)
  to help yourself debug code, but this does not count at testing for the
  purposes of this problem set.
</p>
<p>We have provided some functions to
  create and manipulate <tt>expression</tt>
  values. <code>checkexp</code> is contained
  in <tt>expression.ml</tt>. The others are contained in <tt>expressionLibrary.ml</tt>.
</p>
<ul>
  <li><code>parse</code> : translates a string in infix form (such as <code>&quot;x^2 + sin(~x)&quot;</code>)
    into an <code>expression</code> (treating &quot;x&quot; as the variable).
    The <code>parse</code> function parses according to the standard order of operations - so
    <code>&quot;5+x*8&quot;</code> will be read as
    <code>&quot;5+(x*8)&quot;</code>.
  </li>
  <li><code>to_string</code> : prints expressions in a readable form, using infix
    notation. This function adds parentheses around every binary operation so
    that the output is completely unambiguous.
  </li>
  <li><code>to_string_smart</code> : prints expressions in an even more readable
    form, only adding parentheses when there may be ambiguity.
  </li>
  <li><code>make_exp</code> : takes in a length l and returns a randomly generated
    expression of length at most 2<sup>l</sup></code>.
  </li>
  <li><code>rand_exp_str</code> : takes in a length l and returns a string
    representation of length at most 2<sup>l</sup></code>.
  </li>
  <li><code>checkexp</code> : takes in a string expression and an x value and
    prints the results of calling every function to be tested except find_zero.</li>
</ul>
<p></p>
The difference between <code>to_string</code> and <code>to_string_smart</code>:
<pre>let e = Binop(Add,Binop(Pow,Var,Num 2.0),Unop(Sin,Binop(Div,Var,Num 5.0)))
  to_string(e) = &quot;((x^2.)+(sin((x/5.))))&quot;
  to_string_smart(e) = &quot;x^2.+sin(x/5.)&quot;
</pre>

<h3>Problem Instructions</h3>
<p>Instructions for problems 2.1 and 2.2 are in <tt>expression.ml</tt>.</p>
<a name="part2_3"></a>
<strong>Problem 2.3: Derivatives</strong>
<p>Next, we want to develop a
<span lang="en-us">function that takes an expression
<code>e</code> as its argument and returns an expression
<code>e'</code> representing the derivative of the expression with
respect to x. This process is referred to as symbolic differentiation.</p>

<p><span lang="en-us">When implementing this function, recall the chain rule from your freshman Calculus course:</span></p>
<blockquote>
	<p><img src="ChainRule.png"></p>
</blockquote>
<p><span lang="en-us">And recall how, using that, we can write the derivatives for the other functions in our
language:</span></p>
<blockquote>
<br>
	<p><img src="DerivRules.png"></p>
</blockquote>

<p>Note that there two cases provided for calculating the derivative of <code>f(x) ^ g(x)</code>,
one for where <code>g(x) = h</code> does not contain any variables, and one for the general case.
The first is a special case of the second, but it is useful to treat them separately, because when the first case applies, the second case produces
unnecessarily complicated expressions.</code>

<p>Your task is to implement the <code>derivative</code> function.
The type of this function is <code>expression -> expression</code>.
The result of
your function must be correct, but need not be expressed in the simplest form.
Take advantage of this in order to keep the code in this part as short
as possible.</p>

<p>To make your task easier, we have provided an outline of the function
with many of the cases filled in. We have also provide a function, <code>checkexp</code>,
which checks parts 2.1-2.3 for a given input.  The portions of the function that
require your attention read <code>raise (Failure "Not implemented")</code>.
Do not attempt to run the function until you have replaced all of the
<code>raise</code> statements with valid code.</p>

<a name="part2_4"></a>
<strong>Problem 2.4: Zero Finding</strong>
  <p>One application of the derivative of a function is to find zeros of a function. One way to do so is <a href="http://en.wikipedia.org/wiki/Newton%27s_method">Newton's method</a>. The function should take an expression, a starting guess for the zero, a precision requirement, and a limit on the number of times to repeat the process. It should return <code>None</code> if no zero was found within the desired precision by the time the limit was reached, and <code>Some r</code> if a zero was found at <code>r</code> within the desired precision.</p>

  <p>Your task is to implement the <code>find_zero:expression -&gt;
  float -&gt; float -&gt; int -&gt; float option</code> function. Note that there are
  cases where Newton's method will fail to produce a zero, such as for
  the function <span class=m><i>x</i><sup>1/3</sup></span>. You are not
  responsible for finding a zero in those cases,
   but just for the correct implementation of Newton's method.</p>

<p><b>Clarification: </b> If the expression that find_zero
is operating on  is '<code>f(x)</code>' and the precision is
<code>epsilon</code>, we are asking you to find a value
<code>x</code>  such that <code>|f(x)| &lt; epsilon</code>.  That is, the
value that the expression evaluates to at <code>x</code> is "within
<code>epsilon</code>" of 0.
<br>(We are <b>not</b> requiring you to find an <code>x</code> such that <code>|x -
x<sub>0</sub>| &lt; epsilon</code> for some <code>x<sub>0</sub></code> for
which <code>f(x<sub>0</sub>) = 0</code>.)</p>

<a name="part2_5"></a>
<strong>Problem 2.5: Symbolic Zero-Finding (Extra Credit)</strong>
<p>We suggest you attempt this after finishing the rest of the problem set,
  but you should not feel discouraged if you cannot complete it.</p>

<p>The function you wrote above allows you to find the zero (or a zero) of most
functions that can be represented with our AST. This makes it quite
powerful. However, in addition to numeric solving like this,
Mathematica and many similar programs can perform symbolic
algebra. These programs can solve equations using techniques similar
to those you learned in middle and high school (as well as more
advanced techniques for more complex equations) to get exact, rather
  than approximate answers. For example, given the expression <i>3x-1</i>,
  your <code>find_zero</code> function might return something like
  0.33333, depending on your value of epsilon. The exact solution,
  however is 1/3, and this answer can be found by a program that
  solves equations symbolically.</p>

<p>Performing symbolic manipulation on complex expressions is quite
  difficult, and we do not expect you to do it (although it could make
  a good final project for this class). However, there is one type of
  expression for which this is not so difficult. These are expressions
  that can be simplified to the form <i>ax + b</i>. You likely learned
  how to solve equations of the form <i>ax + b=0</i> years ago, and
  can apply the same skills in writing a program to solve these.</p>

<p>Write a function, <code>find_zero_exact</code> which will exactly
  find the zero of degree one expressions (expressions like the ones
  described above, in which the highest exponent of a variable is
  1). Your function should, given a valid expression that has a zero,
return Some and an expression that
<ul>
<li> contains no variables
<li> evaluates to the zero of the given expression and
<li> is exact.
</ul>
If the expression is not degree one or has no zero, return None.
You need not return the
simplest expression, though it could be instructive to think about how
to simplify results. For example, <code>find_zero_exact (parse
  "3*x-1")</code> might return
<code>Binop (Div, Num 1., Num 3.)</code> or <code>Unop(Neg, Binop
  (Div, Num -1., Num 3.))</code> but should <b>not</b>
  return <code>Num 0.333333333</code> as this is not exact.</p>

<p>Note that degree one expressions need not be as simple as <i>ax +
    b</i>. Something like <i>5x - 3 + 2(x - 8)</i> is also a degree-one
    expression since it can be turned into <i>ax + b</i> by
    distributing and simplifying. You will need to think about how to
    handle these types of expressions. You will also need to think
    about how to determine whether an expression is degree one.</p>

<p><i>Hint:</i> You may want to start by writing a function which will
  crawl through an expression and distribute any multiplications or divisions,
  resulting in something of a form like <i>ax + b</i> (or maybe <i>ax
  + bx + cx + d + e + f</i> or similar).</p>

<h3>Submit!</h3>
      <p>Before submitting, it is <strong>very important</strong> that you compile
      your code by running the <code>make</code> command while in the directory with your expression.ml and
      mapfold.ml, and then run your compiled programs
      using <code>./mapfold.native</code>
      and <code>./expression.native</code><br>. There should be no output
      from running the program. If you do not see any output, your
      asserts passed and you are good to go.
      Evaluating definitions
      in the toplevel, while useful during development, <b>is not
      sufficient</b> for preparing your code for submission.</p>

      <p>To submit, commit all your changes and push them to code.seas by
      running <tt>git commit -am "commit message here"</tt> followed by
      <tt>git push</tt>. This will allow us to access your code from
      the grading server.</p>

</body></html>
