(*** CS 51 Problem Set 2 ***)
(*** February 12, 2010 ***)
(*** Test Suite ***)

module Ast = struct end
module ExpressionLibrary = struct end

open Core.Std ;;

(* ------ Type definitions for the abstract syntax tree ------- *)

(* Binary operators. *)
type binop = Add | Sub | Mul | Div | Pow

(* Unary operators. *)
type unop = Sin | Cos | Ln | Neg

type expression =
  | Num of float
  | Var
  | Binop of binop * expression * expression
  | Unop of unop * expression


(* ------------- Library code ------------- *)
(* You shouldn't have to do anything here.  *)

exception ParseError of string

type token =
  | NumT of float
  | VarT
  | BinopT of binop
  | UnopT of unop
  | LParen
  | RParen
  | LBrace
  | RBrace
  | EOF

let recognized_tokens = [|"x"; "sin"; "cos"; "ln"|] ;;

let token_expressions = [|VarT; UnopT Sin; UnopT Cos; UnopT Ln|] ;;

let string_to_char_list (s:string) : char list =
  let rec string_to_char_list' (s:string) (acc:char list) (i:int) =
    if i < 0 then acc else
      let c = String.get s i in
    string_to_char_list' s (c::acc) (i-1)
  in string_to_char_list' s [] (String.length s - 1)
;;

let is_digit (c:char) : bool =
  let i = int_of_char c in
    i >= 48 && i <= 57
;;

(* The precedence of a binary operator.  Used in the parse_string and
      to_string_smart functions. *)
let binop_precedence (b:binop) : int =
  match b with
    | Add -> 3
    | Sub -> 3
    | Mul -> 2
    | Div -> 2
    | Pow -> 1
;;

let unop_precedence (_:unop) : int = 4 ;;


(* A strict upper bound on the precedence of any operator. *)
let prec_bound : int = 5 ;;

let binop_is_associative (b:binop) : bool =
  match b with
    | Add | Mul -> true
    | Sub | Div | Pow -> false ;;

(* Pretty-printing functions for expressions *)

let binop_to_string (b:binop) : string =
  match b with
    | Add -> "+"
    | Sub -> "-"
    | Mul -> "*"
    | Div -> "/"
    | Pow -> "^"
;;

let unop_to_string (u:unop) : string =
  match u with
    | Sin -> "sin"
    | Cos -> "cos"
    | Ln -> "ln"
    | Neg -> "~"
;;

let token_to_string (t:token) : string =
  match t with
    | NumT n -> Float.to_string n
    | VarT -> "x"
    | BinopT b -> binop_to_string b
    | UnopT u -> unop_to_string u
    | LParen -> "("
    | RParen -> ")"
    | LBrace -> "{"
    | RBrace -> "}"
    | EOF -> "EOF"
;;

(* Only adds parentheses when needed to prevent ambiguity. *)
let to_string_smart (e:expression) : string =
  let rec to_string_smart' e parent_precedence parent_associative =
    match e with
      | Num n ->
      if n >= 0.0 then Float.to_string n
      else "~" ^ Float.to_string (Float.abs n)
      | Var -> "x"
      | Unop (u,e1) ->
      unop_to_string u ^ "(" ^
        to_string_smart' e1 (unop_precedence u) false ^ ")"
      | Binop (b,e1,e2) ->
      let prec = binop_precedence b in
          let e_str =
          (to_string_smart' e1 prec false ^
           binop_to_string b ^
           to_string_smart' e2 prec (binop_is_associative b)) in
            if prec > parent_precedence ||
                  (prec = parent_precedence && not parent_associative)
            then "(" ^ e_str ^ ")"
        else e_str
  in to_string_smart' e prec_bound false
;;

(* Always adds parentheses around all binary ops. Completely unambiguous;
       however, often very hard to read... *)
let rec to_string (e:expression) : string =
  match e with
    | Num n ->
    if n >= 0.0 then Float.to_string n
        else "~" ^ Float.to_string (Float.abs n)
    | Var -> "x"
    | Unop (u,e1) -> "(" ^ unop_to_string u ^ "(" ^ to_string e1 ^ "))"
    | Binop (b,e1,e2) ->
        "(" ^ to_string e1 ^ binop_to_string b ^ to_string e2 ^ ")"
;;

(* Lexing functions (producing tokens from char lists) *)

let rec match_while (p:char -> bool) (l:char list) : string * char list =
  match l with
    | [] -> ("", [])
    | c::cs ->
    if p c then
      let (s_cs, l_cs) = match_while p cs in (String.make 1 c ^ s_cs, l_cs)
    else ("", l) ;;

let lex_number_string = match_while (fun c -> is_digit c || c = '.')

let lex_number (l:char list) : (token * char list) option =
  let (s,l') = lex_number_string l in
    try Some (NumT (Float.of_string s), l')
    with Invalid_argument _ -> None ;;

let rec match_string (l:char list) (s:string) : char list option =
  if s = "" then Some l else
    match l with
      | [] -> None
      | h::t ->
      if h = String.get s 0 then
            match_string t (String.sub s ~pos:1 ~len:(String.length s - 1))
          else None ;;

let lex_multi_char_token (l:char list) : (token * char list) option  =
  let rec lex_multi_char_token' l i =
    if i >= Array.length recognized_tokens then None
    else
      match match_string l (Array.get recognized_tokens i) with
    | Some l' -> Some (Array.get token_expressions i, l')
    | None -> lex_multi_char_token' l (i+1)
  in lex_multi_char_token' l 0 ;;

let rec lex' (l:char list) : token list =
  match l with
    | [] -> []
    | ' '::cs -> lex' cs
    | c::cs ->
    let (token, l') =
      (match c with
       | '+' -> (BinopT Add, cs)
       | '-' -> (BinopT Sub, cs)
       | '*' -> (BinopT Mul, cs)
       | '/' -> (BinopT Div, cs)
       | '^' -> (BinopT Pow, cs)
       | '~' -> (UnopT Neg, cs)
       | '(' -> (LParen, cs)
       | ')' -> (RParen, cs)
       | '{' -> (LBrace, cs)
       | '}' -> (RBrace, cs)
       | _ ->
           (match lex_number l with
        | Some (t, l') -> (t, l')
        | None ->
            (match lex_multi_char_token l with
             | Some (t, l') -> (t, l')
             | None -> raise (ParseError "Unrecognized token"))))
    in token :: lex' l' ;;

let lex (s:string) : token list =
  lex' (string_to_char_list s) @ [EOF] ;;

let parse (s:string) : expression =
  let rec parse_toplevel_expression (l:token list) : expression =
    let (e,_,_) = parse_delimited_expression l EOF prec_bound in e

  and parse_expression (l:token list) : expression * token list =
    match l with
      | [] -> raise (ParseError "Unexpected end of string")
      | t::ts ->
          match t with
            | LParen ->
        let (e,l',_) =
          parse_delimited_expression ts RParen prec_bound in
          (e,l')
            | RParen -> raise (ParseError "Unexpected rparen")
            | LBrace ->
        let (e,l',_) =
          parse_delimited_expression ts RBrace prec_bound in
          (e,l')
            | RBrace -> raise (ParseError "Unexpected rbrace")
            | UnopT u -> parse_unop ts u
            | VarT -> (Var, ts)
            | EOF -> raise (ParseError "Unexpected EOF")
            | NumT n -> (Num n, ts)
            | BinopT _ ->
        raise (ParseError ("Unexpected Binop: " ^ token_to_string t))

  and parse_binop (l:token list) (delim:token) (current_prec:int) eq
      : expression * token list * bool =
    match l with
      | [] -> raise (ParseError "Unexpected end of string 2")
      | t::ts ->
          if t = delim then (eq,ts,true) else
            match t with
              | BinopT b ->
                  let prec = binop_precedence b in
                    if current_prec <= prec then (eq,l,false)
                    else
              let (eq2,l',d) =
                        parse_delimited_expression ts delim prec in
            if d then (Binop(b,eq,eq2),l',true)
            else parse_binop l' delim current_prec
                          (Binop(b,eq,eq2))
              | _ ->
          raise
            (ParseError
                       ("Expecting Binop, but found: " ^ token_to_string t))

  and parse_delimited_expression (l:token list) (delim:token)
      (current_prec:int) : expression * token list * bool =
    match l with
      | [] -> raise (ParseError "Unexpected end of string 3")
      | t::_ ->
          if t = delim then
            raise (ParseError ("Unexpected delim: " ^ token_to_string delim))
          else
        let (eq,l') = parse_expression l in
              parse_binop l' delim current_prec eq

  and parse_unop (tokens:token list) (u:unop) =
    let (e,t) = parse_expression tokens in (Unop(u,e),t)

  in parse_toplevel_expression (lex s)
;;

(* -------------------------------------------------------------- *)

let sol_unop_denote (u:unop) =
  match u with
    | Sin -> sin
    | Cos -> cos
    | Ln -> log
    | Neg -> fun x -> -. x
;;

let sol_binop_denote (b:binop) =
  match b with
    | Add -> ( +. )
    | Sub -> ( -. )
    | Mul -> ( *. )
    | Div -> ( /. )
    | Pow -> ( ** )
;;

let rec sol_evaluate (e:expression) (x:float) : float =
  match e with
    | Num n -> n
    | Var -> x
    | Unop (u,e1) -> sol_unop_denote u (sol_evaluate e1 x)
    | Binop (b,e1,e2) -> sol_binop_denote b (sol_evaluate e1 x) (sol_evaluate e2 x)
;;

let approx_eq (x1:float) (x2:float) : bool =
  Float.abs (x1 -. x2) < 1e-6 ||
    x1 = x2 || (Float.is_nan x1 && Float.is_nan x2) ;;

let rec ifold_left
    (a:'a -> 'b -> 'b) (f:int -> 'a) (z:'b) (lo:int) (hi:int) : 'b =
  if lo > hi then z else ifold_left a f (a (f lo) z) (lo+1) hi ;;

let check_at (e:expression) (e':expression) (x:float) =
  approx_eq (sol_evaluate e x) (sol_evaluate e' x) ;;

let check (e:expression) (e':expression) =
  ifold_left (&&) (fun x -> check_at e e' (float x)) true (-100) 100 ;;


(*>* Problem 2.1 *>*)
(*>>* 2 *>>*)

>>>
contains_var (parse "x")

>>>
contains_var (parse "sin(x)")

>>>
contains_var (parse "3^2+sin (cos x)")

>>>
contains_var (parse "5*(4+sin(3*x^5))")

>>>
not (contains_var(parse "10"))

>>>
not (contains_var (parse "sin ~3+ln (5+ln 7)"))


(*>* Problem 2.2 *>*)
(*>>* 4 *>>*)
>>>
evaluate (parse "5/2+4*2^4-1") 0. = 65.5

>>>
evaluate (parse "5/2+4*2^4-1") 5. = 65.5

>>>
evaluate (parse "x") 37. = 37.

>>>
approx_eq (evaluate (parse "sin(x)+(cos(x)*~4)/(x^2)") 5.) (-1.00431022)

>>>
evaluate (parse "x^(~2)") 2. = 0.25

>>>
evaluate (parse "x^.25") 256. = 4.

>>>
approx_eq (evaluate (parse "ln(x)+x") 10.) 12.30258509


(*>* Problem 2.3 *>*)
(*>>* 8 *>>*)

>>>
derivative (parse "x") = Num 1.

>>>
derivative (parse "7") = Num 0.

>>>
check (derivative (parse "x^2")) (parse "2*x")

>>>
check (derivative (parse "sin(x)")) (parse "cos(x)")

>>>
check (derivative (parse "1/x")) (parse "~1/x^2")

>>>
check (derivative (parse "ln(x*cos(x))+47"))
      (Binop (Div, Binop (Add,
	Binop (Mul, Var, Binop (Mul, Unop (Neg, Unop (Sin, Var)), Num 1.)),
	Binop (Mul, Num 1., Unop (Cos, Var))),
	Binop (Mul, Var, Unop (Cos, Var))))

>>>
check (derivative (parse "x^(2*x)"))
  (Binop (Mul, Binop (Pow, Var, Binop (Mul, Num 2., Var)),
   Binop (Add, Binop (Mul, Binop (Add, Binop (Mul, Num 2., Num 1.),
   Binop (Mul, Num 0., Var)), Unop (Ln, Var)),
   Binop (Div, Binop (Mul, Num 1., Binop (Mul, Num 2., Var)), Var))))

>>>
check (derivative (parse "sin(x)^3+x*x-1/(5*x)"))
  (Binop (Sub,
	  Binop (Add,
		 Binop (Mul, Num 3.,
   Binop (Mul, Binop (Mul, Unop (Cos, Var), Num 1.),
    Binop (Pow, Unop (Sin, Var), Binop (Sub, Num 3., Num 1.)))),
  Binop (Add, Binop (Mul, Var, Num 1.), Binop (Mul, Num 1., Var))),
 Binop (Div,
  Binop (Sub, Binop (Mul, Num 0., Binop (Mul, Num 5., Var)),
   Binop (Mul, Num 1.,
    Binop (Add, Binop (Mul, Num 5., Num 1.), Binop (Mul, Num 0., Var)))),
  Binop (Pow, Binop (Mul, Num 5., Var), Num 2.))))


(*>* Problem 2.4 *>*)
(*>>* 8 *>>*)

let check_zero (e:expression) (c:float) f =
  match (f e c 1e-4 100) with
    | Some x -> Float.abs (sol_evaluate e x) < 1e-4
    | None -> false ;;

let check_no_zero (e:expression) (c:float) f =
  match (f e c 1e-4 100) with
    | Some _ -> false
    | None -> true ;;

>>>
check_zero (parse "x") 0.1 find_zero

>>>
check_zero (parse "sin(x)") 3.141526 find_zero

>>>
check_zero (parse "x^2-x") 1. find_zero

>>>
check_zero (parse "sin(x)^2-cos(x)^2") 2.35619 find_zero

>>>
check_no_zero (parse "3") 0. find_zero

>>>
check_no_zero (parse "sin(x)+1.1") 1. find_zero


(*>* Problem 2.5 *>*)
(*>>* 3 *>>*)

let deopt opt =
  match opt with
  | None -> ""
  | Some a -> to_string_smart a
;;

>>>
find_zero_exact (parse "2.*1.*x^(2.-1.)") = None

>>>
find_zero_exact (parse "42") = None

>>>
find_zero_exact (parse "0*x") = None

>>>
deopt (find_zero_exact (parse "42*x")) = "(0./42.)"

>>>
deopt (find_zero_exact (parse "42*x-4")) = "(4./42.)"

>>>
deopt (find_zero_exact (parse "42*x+3-7")) = "(4./42.)"

>>>
deopt (find_zero_exact (parse "42*x+6*x-3*x+96")) = "~(96./45.)"


(*>* Problem 2.6 *>*)
(*>>* 0 *>>*)

>>>
let (_ : int) = minutes_spent_on_part_2 in true
