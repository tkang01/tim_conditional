(* Consider this mutable list type. *)
type 'a mlist = Nil | Cons of 'a * (('a mlist) ref)

let rec rmem a lst =
  match lst with
    | [] -> false
    | h::t -> (h==(!a)) || (rmem a t)

(*>* Problem 1.1 *>*)
(* Write a function has_cycle that returns whether a mutable list has a cycle. *)
let has_cycle (lst : 'a mlist) : bool =
  let rec cycle_rec lst seen =
    match lst with
      | Nil -> false
      | Cons (h, t) -> if rmem t seen then true else cycle_rec !t (lst::seen)
  in cycle_rec lst []

let list1a = Cons(2, ref Nil)
let list1b = Cons(2, ref list1a)
let list1 = Cons(1, ref list1b)

let reflist = ref (Cons(2, ref Nil))
let list2 = Cons(1, ref (Cons (2, ref (Cons (2, reflist)))))
let _ = reflist := list2

(*>* Problem 1.2 *>*)
(* Write a function flatten that flattens a list (removes its cycles if it
 * has any) destructively. *)
let flatten (lst : 'a mlist) : unit =
  let rec flatten_rec lst seen =
    match lst with
      | Nil -> ()
      | Cons(h, t) -> if rmem t seen then t := Nil
        else flatten_rec !t (lst::seen)
  in flatten_rec lst []

(* Flattened used to be problem 1.3, and is now a helper function used by mlength *)
let flattened (lst : 'a mlist) : 'a mlist =
  let rec flatten_rec lst seen =
    match lst with
      | Nil -> Nil
      | Cons(h, t) -> if rmem t seen then Cons(h, ref Nil)
        else Cons(h, ref (flatten_rec !t (lst::seen)))
  in flatten_rec lst []

(*>* Problem 1.3 *>*)
(* Use flatten2 to write mlength, which finds the length of a mutable list. *)
let mlength (lst : 'a mlist) : int =
  let rec mlength_rec lst =
    match lst with
      | Nil -> 0
      | Cons(_, t) -> 1 + mlength_rec !t
  in mlength_rec (flattened lst)

(*>* Problem 1.4 *>*)
