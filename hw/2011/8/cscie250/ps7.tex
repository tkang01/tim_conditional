\documentclass[12pt]{article}

\usepackage{alltt}
\usepackage{hyperref}

 \input{decls-common}

%% Page layout
\oddsidemargin 0pt
\evensidemargin 0pt
 \textheight 600pt 
\textwidth 469pt 
\setlength{\parindent}{0em} 
\setlength{\parskip}{1ex}

\begin{document}

\title{Problem Set 7: Red, Black and Trees. That's right, AIs for checkers!}

\author{}

\date{Due Sunday, April 11$^{th}$, 2010, 11:59 pm}

\maketitle

\section{Introduction}
In this assignment, you will implement classes that play checkers in Java. We've provided a complete implementation of the game logic for you, including an interface called Player. A class that implements Player should represent a player in the game. Player must have a method \verb=play= which plays a move (or, more specifically, returns an object of type Move) when called by the game engine. An example of a Player class is provided for you: DummyPlayer looks at the possible moves and chooses one at random. Your task will be to create two more Player classes which behave in more logical ways.

\begin{enumerate}
\item First, familiarize yourself with the rules of checkers if you haven't played in a while (or ever).
\\
\\
We followed the rules of Standard American Checkers in building out the game logic.   Your code will most likely be completely agnostic to the game logic itself, as the Board interface can always provide you with the moves that are possible, and the means of coming to a decision about what moves are favorable given the game logic.  See http://www.darkfish.com/checkers/rules.html for an explicit discussion of rules, if you're curious.  Here are a few key points that may not be obvious:
\begin{itemize}
\item A player ``wins" by capturing all of the other player's pieces or by putting the other player in a position in which movement is impossible.  We implement this by having player A win when player B has no legal moves to make (because if player B has no pieces left, it will have no legal moves to make).
\item Official rules are a bit inconclusive about what constitutes a ``draw" (tie game), so we have adopted the following metric: if in the last 50 moves, no piece has been captured and no non-king piece has moved forward, the game is a draw.  When this happens, the method winner() in the Board interface will return null.
\end{itemize}
\item Download the code tarball from the website.  If you're using Eclipse, you will need to put these files into a new Java project. Create a new project in the folder where you downloaded the assignment files. If you select the folder containing the .java files, Eclipse will give you the option to include them in the project. Once you have created the project, check if the files are in a package called cs51checkers (it should appear as a brown package symbol on the project navigator tree.) If not, right-click on your project name in the navigator, and select New, then Package to create a package called cs51checkers, and drag the .java files into it.
\\
\\ In general, you want to get the IDE that you're using to recognize that the code provided to you is all in a package cs51checkers.  \textbf{The bottom line} regardless of the IDE that you're using is that because the code provided to you is in the cs51checkers package, it must always remain in a folder called cs51checkers.  Whenever you are trying to run your code, you must be one level up from this folder.
\item In particular, the game code, as given to you, should compile and run. If you are using Linux or Mac OS, we have provided a shell script checkers.sh that compiles all of the code and runs Checkers.  In general, run the program by navigating one level up from the cs51checkers folder and providing command line arguments $<$ player class$>$ $<$ player class$>$ $<$ round delay (ms)$>$ $<$ enable time limit (t|f)$>$.  That is, type, for example  ``java cs51checkers.Checkers DummyPlayer DummyPlayer 1000 t". (If you are using an IDE and don't want to work with the command line, you will at least need to figure out how to get your IDE to pass command line arguments to the JVM.  How to do this can easily be found via Google search).
\\ 
\\ The ``player class" arguments should be the names of two classes of type Player, where the first given is the red player, and the second given is the black player.  The ``round delay (ms)" argument lets you specify the delay between printing two rounds (to slow down the game enough that you can watch it).  Finally, the ``enable time limit (t|f)", when given as 't', enables a 1 second time limit on moves to prevent computer players from running arbitrarily long (given enough time, a computer can ``solve" checkers completely!).  Thus, the example command above would start a game between two ``dumb" players, with a full second between rounds, and enabling the 1 second maximum (trust us, though, a DummyPlayer will not take more than a second to compute its ``decision").
\end{enumerate}

\section{A Smarter Player}
By now, you should realize that DummyPlayer is dumb. Your first task is to implement a better AI (possibly one that lives up to the second word in the term Artificial Intelligence). You will do this using the Minimax algorithm. We will be covering Minimax in section this week, but if you would like to do some research on your own, start here: \verb=http://en.wikipedia.org/wiki/Minimax=. The implementation will consist of an evaluator function that assigns a value to the state of the board for one of the players, and a main function that will find the legal move which maximizes the current player's future board position while minimizing that of the other player.
\begin{enumerate} 
\item Create a new file called ``$<$username$>$MMPlayer.java", where $<$username$>$ is your username on nice, with the first letter capitalized to conform to Java naming conventions.  Within this file, define a public class $<$username$>$MMPLayer that implements the Player interface.
\\
\\ Make sure that your file is declared to be in the cs51checkers package (see any other file for how to do this).

\item Define a private function \verb=evaluate(Board board, Side side)= in your class. \verb=evaluate= should return an integer evaluating the given Board from the perspective of the given Side. A higher integer should correspond to a better board. For example, if \verb=side= is RED, \verb=evaluate= should return a high integer if the board looks very good for the red player. We recommend that you start with a simple metric for evaluating a board. Our solution, if \verb=side= is RED, counts the number of red pieces on the board, counting kings twice, and subtracts the number of black pieces on the board, counting kings twice. If you finish the assignment and want to change your evaluation function to be smarter and more complex for karma points, you're welcome to do this. Thanks to modularity, the change shouldn't affect the rest of your code. (In fact, the best modular design would completely abstract the notion of evaluating board states from any individual implementation of Player.  That is, you could define an interface describing the behavior of classes that implement Board comparison, and then you could create multiple classes that implement this interface, and easily go back and forth between them in your Player.  Once the logic of board evaluation is separated from the logic of making a decision \textit{given} the relative value of boards, you could deftly create multiple evaluation metrics and multiple variants of AI logic, and try all the combinations between them to pick the most effective one.  You are welcome to build out that design, but we won't be requiring it for this problem set.)

\item Given your evaluation metric, you must implement the minimax logic itself.  We are intentionally avoiding giving you a template of code and thorough documentation about how this should be done, in light of the fact that you are about to start to have to implement a very large project completely from scratch.  That being said, here are some hints:
    \begin{itemize}
    \item To begin, look at the Player interface to see the one method that you necessarily need to implement, in order to satisfy the interface.  The body of this method will probably end up being quite short, as it will simply be a wrapper around a recursive procedure.
    \item Also, look at the DummyPlayer class to see an example of a concrete implementation of Player.  Note the way it interacts with the Board that has been passed to it.
    \item Finally, if you want a better sense of how the Board works, read the comments in Board.java.  Since Board is just an interface, there is no actual code to worry about, just documentation written for your benefit (the code is all in StandardBoard.java, which you do \textit{not} need to look at, unless you are curious).
    \item At the core of minimax is a recursive alternation between minimizing and maximizing.  Again, refer to what you learned in section and to Wikipedia for exactly what this means.  We recommend having a recursive function that takes a Board and a Side, and also takes some way of representing the number of rounds of minimax remaining, and finally whether this current step should be maximizing or minimizing.  The return value of your function might prove a bit tricky.  At the end of the day, your Player needs to be able to return a Move.  However, in the course of operation of minimax, you'll need a way of keeping track of a value associated with a given Move.  If this were Ocaml, you would simply make the return value of your recursive function be a pair of Move and value.  Java has no pairs, so you'll need to figure out a way to work around this.  You might try defining a dummy class that simply wraps pairs of Move and value.
    \item Last hint: you'll be needing some way of reasoning hypothetically about how the board \textit{would} be if a certain move were taken.  How could you do this?  Aren't we concerned that you're going to modify the Board being used to run the actual game?  We aren't, becuase we made sure that Board objects are immutable.  Learn all about this, and get intuition for how to use it your advantage, by reading the documentation in Board.java.
    \end{itemize}
\end{enumerate}

\section{An (Even Smarter?) Player}
Congratulations, you can now watch the computer play itself in checkers. However, this is likely not that interesting to you, and you may be wondering how you can get in on the game. In this part of the assignment, you will be implementing another Player class, ConsolePlayer, that asks the user to input a move and plays that move. This will allow two users to play each other, or a user to play the computer.
\begin{enumerate}
\item Add a \verb=toString()= method to the Move class. A Location object has two relevant instance variables, \verb=ROW= and \verb=COL=. Move has as an instance variable \verb=locSequence=, which is an ArrayList of Location objects. \verb=locSequence= represents the order in which a given move visits board locations, starting with the starting location. A simple move or a single jump will have 2 elements, a move involving multiple jumps may have more. Your \verb=toString()= method should output the sequence of squares in a user-friendly way, for example \verb=(2,2)->(3,1)=. Note that Location already has a \verb=toString()= method, which returns a longer string than simply (row, column). If you want, you can write a more concise \verb=toStringConcise()= method for Location and then call this from your Move \verb=toString()= method.

\item Fill in the template for ConsolePlayer in ConsolePlayer.java. We have given you a method, \verb=readInt(int low, int high)=, which reads an integer between \verb=low= and \verb=high= from the console and returns it. If the input is invalid or outside the given range, \verb=readInt()= will continue to prompt the user until the input is valid. Your job is to implement the play method. When it is ConsolePlayer's turn, you should ask for a starting row and column (one after the other is fine. If you want to do something fancy like asking for both separated by a comma and parse this string, go ahead, but this is not required). You should then check the moves that are possible from that starting location (check the Board interface for a function that might be useful). If no moves are possible, prompt the user again. If one move is possible, return it. If multiple moves are possible, present the user with a numbered list of them and ask them to choose one by number. You may find your Move.toString() method useful in assembling this list. 

\end{enumerate}

\section{Other Hints}
\begin{itemize}
\item Java has an alternate \verb=for= loop syntax you may find useful. If \verb=lst= is an array or ArrayList (or any ``Iterable") of Objs,
\verb=for (Obj o : lst)= will cause \verb=o= to iterate over all values in \verb=lst=.  That is, the body of this for loop will execute once for each value in \verb=lst=, and in each iteration \verb=o= will be the ``next'' value in \verb=lst=.
\end{itemize}

\section{Karma Galore}
There are numerous ways to extend this problem set and gain karma. In particular, if you see any way that doing more work on this codebase could help you learn things that would be relevant to your final project, have at it!  Some of the things that we've thought of are:
\begin{itemize}
\item Write a simple graphical GUI (Graphical User Interface) that will show the game in a way more visually pleasing than ASCII art.

\item Change the game rules in some non-trivial way (pieces that blow up? Pieces that fly?  The sky is the limit...).  You'll have to figure out our code. It'll take some work, but it's a really good exercise.

\item Add another AI Player with some cooler AI (look up alpha beta pruning).
\end{itemize}
%If you earn karma for yourself after completing and submitting the required portion of the problem set and want to sit down with a TF to review your extension, come to an office hours.  Again (if you have spare time) this could be a great way to dive into something that will help you with your project.

\end{document}
