package cs51checkers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
 * @author Jim Danz, Jeremy Hoon
 */


public class StandardBoard implements Board {
    // Index (0, 0) defines the bottom left of the board
    // Index (BOARD_SIZE - 1, BOARD_SIZE -1) defines the top right of the board.
    private Piece[][] squares;
    private int moveNumber;
    private int movesTowardDraw;
    private Side turnToMove;

    // We want to only compute the possible legal moves once, since they are used in multiple places:
    // - engine checks whether player is stuck
    // - player does its thing
    // - engine checks whether the returned move is legal
    private ArrayList<Move>[][] memoizedMoves;
    
    public StandardBoard() {
        moveNumber = 0;
        movesTowardDraw = 0;
	
        squares = new Piece[BOARD_SIZE][BOARD_SIZE];
        turnToMove = Side.RED;
	
        memoizedMoves = null;
	
        initSquares();
    }

    // Constructor for unit tests only
    public StandardBoard(String boardDesc) {
        moveNumber = 0;
        movesTowardDraw = 0;
        squares = new Piece[BOARD_SIZE][BOARD_SIZE];
        turnToMove = Side.RED;
        int r,c;
        if (boardDesc.length() > BOARD_SIZE*BOARD_SIZE) {
            throw new java.lang.IllegalArgumentException();
        }
        for (int i = 0; i < boardDesc.length(); i++) {
            r = BOARD_SIZE - (i/BOARD_SIZE) - 1;
            c = i%BOARD_SIZE;
            char curr = boardDesc.charAt(i);
            if (curr != '.') {
                if (r%2 != c%2) {
                    throw new java.lang.IllegalArgumentException();
                }
                squares[r][c] = Piece.getPiece(curr);		
            }
        }
    }
    
    private void initSquares() {
		for(int i = 0; i < 3; i++) {
		    for(int columnIndex = i % 2; columnIndex < BOARD_SIZE; columnIndex += 2) {
				squares[i][columnIndex] = Piece.RED_NORMAL;
				squares[(BOARD_SIZE - 1) - i][(columnIndex + 1)%BOARD_SIZE] = Piece.BLACK_NORMAL;
		    }
		}
    }
    
    public Board move(Move m) throws IllegalMoveException {
        if (!isLegal(m)) {
            throw new IllegalMoveException();
        }
	
        /* Create a new board to return whose values are based off
         * of this's values.  The interface dictates that Boards be
         * immutable, so this method must only modify values in the
         * board to be returned; it cannot touch values in "this" board. */
        StandardBoard ret = new StandardBoard();
        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                ret.squares[i][j] = squares[i][j];
            }
        }
        ret.moveNumber = moveNumber + 1;
        ret.movesTowardDraw = movesTowardDraw + 1;
        ret.turnToMove = Side.otherSide(turnToMove);


        Location[] locs = m.getLocations();
        Location first = locs[0];
        Location last = locs[locs.length - 1];
        Piece p = ret.squares[first.ROW][first.COL];

        // If a non-king piece has moved, we reset the counter toward a "draw"
        if(p.TYPE == PieceType.NORMAL) {
            ret.movesTowardDraw = 0;
        }

        // handle king-ing
        if (p == Piece.RED_NORMAL && last.ROW == BOARD_SIZE-1) {
            p = Piece.RED_KING;
        } else if (p == Piece.BLACK_NORMAL && last.ROW == 0) {
            p = Piece.BLACK_KING;
        }
        // move the actual piece
        ret.squares[first.ROW][first.COL] = null;
        ret.squares[last.ROW][last.COL] = p;
        // remove all taken pieces
        Taken t = m.getTaken();

        if(m.isCapture()) {
            for (int i = 0; i < BOARD_SIZE; i++) {
                for (int j = 0; j < BOARD_SIZE; j++) {
                    if (t.isTaken(i,j)) {
                        ret.squares[i][j] = null;
                    }
                }
            }
            
            ret.movesTowardDraw = 0;
        }


        return ret;
    }
    
    public boolean isLegal(Move m) {
    	if(m == null) {
    		return false;
    	}
    	
    	List<Move> legals = legalMoves(m.getStartLocation());
    	for (Move legal : legals) {
            if(legal.equals(m)) {
                return true;
            }
    	}
    	return false;
    }
    
    public boolean isOver() {
    	// 1. Red Player is Stuck
    	// 2. Black Player is Stuck
        // 3. It's a draw.
    	return isStuck() || isDraw();
    }
    
    public Side winner() throws GameNotOverException {
    	if (isStuck()) {
    		return Side.otherSide(turnToMove);
    	}
    	else if(isDraw()) {
    		return null; 
    	}
    	else {
    		throw new GameNotOverException();
    	}
    }
    
    private boolean isStuck() {
    	for(int i = 0; i < BOARD_SIZE; i++) {
    		for(int j = 0; j < BOARD_SIZE; j++) {
    			if(legalMoves(new Location(i, j)).size() != 0) {
    				return false;
    			}
    		}
    	}
    	return true;
    }

    private boolean isDraw() {
        return movesTowardDraw >= DRAW_MOVE_LIMIT; 
    }
    
    public Piece pieceAt(Location l) {
    	return pieceAt(l.ROW, l.COL);
    }
    
    public Piece pieceAt(int row, int col) {
    	return squares[row][col];
    }
    
    public List<Move> legalMoves(Location l) {
    	if(memoizedMoves == null) {
    		computeLegalMoves();
    	}
    	return Collections.unmodifiableList(memoizedMoves[l.ROW][l.COL]);
    }
    
    private void computeLegalMoves() {
    	memoizedMoves = new ArrayList[BOARD_SIZE][BOARD_SIZE];
        boolean capturePossible = false;
        
    	for(int i = 0; i < BOARD_SIZE; i++) {
    		for(int j = 0; j < BOARD_SIZE; j++) {
                // only look for non-jumps if we haven't found a jump yet.
    			memoizedMoves[i][j] = computeMovesAtLocation(new Location(i, j), !capturePossible);
                for(Move m : memoizedMoves[i][j]) {
                    if (m.isCapture()) {
                        capturePossible = true;
                    }
                }
    		}
    	}
    	
    	if(capturePossible) {
        	for(int i = 0; i < BOARD_SIZE; i++) {
        		for(int j = 0; j < BOARD_SIZE; j++) {
        			memoizedMoves[i][j] = filterInCaptureMoves(memoizedMoves[i][j]);
        		}
        	}
        }
    }
        
    // Return just the capture moves
    private ArrayList<Move> filterInCaptureMoves(ArrayList<Move> moves) {
    	ArrayList<Move> toRet = new ArrayList<Move>();
    	for(Move move : moves) {
    		if(move.isCapture()) {
    			toRet.add(move);
    		}
    	}
    	return toRet;
    }
    
    
    private ArrayList<Move> computeMovesAtLocation(Location l, boolean searchForNonJumps) {
        Piece piece = pieceAt(l);
        if(piece == null || piece.SIDE != turnToMove) {
            return new ArrayList<Move>();
        }
        else {
            // First, try jumps.
            ArrayList<Move> legals = possibleJumps(piece, null, l);
            // Iff jumping is impossible, try nonjumping moves.
            if(searchForNonJumps && legals.isEmpty()) {
                legals = possibleNonJumps(piece, l);
            }
            return legals;
        }
    }
    
    private ArrayList<Move> possibleJumps(Piece p, Move soFar, Location l) {
        boolean fresh = (soFar == null);
        if(soFar == null) {
    		soFar = new Move(l);
        }
    	
    	ArrayList<Move> possible = new ArrayList<Move>();
    	
    	Taken t = soFar.getTaken();
    	Move afterStep;
    	
    	Location proposedVictimLoc;
    	ArrayList<Location> jumpsAvail = filterAvailable(jumpsWithinRange(p, l), t);
    	for (Location nextStep : jumpsAvail) {
    		proposedVictimLoc = getJumpedLocation(p, t, l, nextStep);
    		if(proposedVictimLoc != null) {
    			// If and only if we're actually going to jump something,
    			// we clone our current Move before modifying it.
    			afterStep = (Move)soFar.clone();
    			afterStep.addLocation(nextStep);
    			afterStep.recordTaken(proposedVictimLoc);
    			possible.addAll(possibleJumps(p, afterStep, nextStep));
    		}
    	}
    	if(!fresh && possible.isEmpty()) {
    		if(soFar != null) {
    			ArrayList<Move> justSoFar = new ArrayList<Move>();
    			justSoFar.add(soFar);
    			return justSoFar;
    		}
    		else {
    			return new ArrayList<Move>();
    		}
    	}
    	else {
    		return possible;
    	}
    }
    
    private ArrayList<Move> possibleNonJumps(Piece p, Location l) {
    	Taken dummy = new BooleanTaken(BOARD_SIZE);
    	
    	ArrayList<Location> locs = filterAvailable(nonJumpsWithinRange(p, l), dummy);
    	
    	ArrayList<Move> moves = new ArrayList<Move>();
    	for(Location dest : locs) {
    		moves.add(new Move(l, dest));
    	}
    	return moves;
    }
    
    private ArrayList<Location> filterAvailable(ArrayList<Location> list,
                                                Taken t) {
    	ArrayList<Location> avail = new ArrayList<Location>();
    	for(Location poss : list) {
    		if(isAvailable(t, poss)) {
    			avail.add(poss);
    		}
    	}
    	return avail;
    }
    
    public int moveNumber() {
    	return moveNumber;
    }
    
    public int movesTowardDraw() {
    	return movesTowardDraw;
    }
    
    public Location[] getPieceLocations() {
        ArrayList<Location> locations = new ArrayList<Location>();
        for(int i = 0; i < BOARD_SIZE; i++) {
            for(int j = 0; j < BOARD_SIZE; j++) {
                if(squares[i][j] != null) {
                    locations.add(new Location(i, j));
                }
            }
        }
        return locations.toArray(new Location[locations.size()]);
    }
    
    // In the process of generating a move, we want to know if this location is
    // a location that we could potentially move to, only keeping track of this location
    // and what we've taken so far.  (If we're taken what was there, can move there).
    protected boolean isAvailable(Taken t, Location l) {
        return squares[l.ROW][l.COL] == null || t.isTaken(l);
    }

    // Considering having piece p jump from src to dest.  If possible, returns the location
    // of the "victim" being jumped.
    // Returns null if no jump is possible.
    protected Location getJumpedLocation(Piece p, Taken t, Location src, Location dest) {
    	Location poten = findPotentiallyJumped(t, src, dest);
    	
    	if(poten == null ||  squares[poten.ROW][poten.COL].SIDE == p.SIDE) {
    	    return null;
    	}
    	else{
    		return poten;
    	}
    }
    
    protected boolean isJump(Piece p, Taken t, Location src, Location dest) {
    	return findPotentiallyJumped(t,src,dest) != null;
    }
    
    protected Location findPotentiallyJumped(Taken t, Location src, Location dest) {
    	checkDiagonal(src,dest);
		int rowDiff = dest.ROW - src.ROW;
		int colDiff = dest.COL - src.COL;
		int rAddend = Math.abs(rowDiff)/rowDiff;
		int cAddend = Math.abs(colDiff)/colDiff;
		for (int r = src.ROW+rAddend, c = src.COL+cAddend;
		     r != dest.ROW;
		     r += rAddend, c += cAddend) {
		    Piece curr = squares[r][c];
            if (curr != null && !t.isTaken(r,c)) {
                return new Location(r,c);
		    }
		}
        return null;
    }
    
    public boolean isDiagonal(Location l1, Location l2) {
        int slopeNum = l1.COL - l2.COL;
        int slopeDenom = l1.ROW - l2.ROW;
        return Math.abs(slopeNum) == Math.abs(slopeDenom);
    }
    public void checkDiagonal(Location l1, Location l2) {
        if (!isDiagonal(l1,l2)) {
            throw new java.lang.IllegalArgumentException();
        }
    }
    
    protected ArrayList<Location> nonJumpsWithinRange(Piece p, Location l) {
    	return locsWithinN(p, l, 1);
    }
    
    protected ArrayList<Location> jumpsWithinRange(Piece p, Location l) {
    	return locsWithinN(p, l, 2);
    }
    
    protected ArrayList<Location> locsWithinN(Piece piece, Location l, int n) {
    	ArrayList<Location> poss = new ArrayList<Location>();
    	if(piece.SIDE == Side.RED || piece == Piece.BLACK_KING) {
    	    addIfInBounds(poss, l.ROW + n, l.COL - n);
    	    addIfInBounds(poss, l.ROW + n, l.COL + n);
    	}
    	if(piece.SIDE == Side.BLACK || piece == Piece.RED_KING) {
    	    addIfInBounds(poss, l.ROW - n, l.COL - n);
    	    addIfInBounds(poss, l.ROW - n, l.COL + n);
    	}
    	return poss;
    }
    
    protected void addIfInBounds(ArrayList<Location> poss, int r, int c) {
        Location l = new Location(r,c);
    	if(inBounds(l)) {
            poss.add(l);
    	}
    }
    
    protected boolean inBounds(Location l) {
    	return (l.ROW >= 0 && l.ROW < BOARD_SIZE
                && l.COL >= 0 && l.COL < BOARD_SIZE); 
    }
    
    // For debugging purposes
    public String toString() {
    	String dashes = " ----------------";
    	String toRet = dashes + "\n";
		for(int i = BOARD_SIZE -1; i >= 0; i--) {
			toRet += "|";
		    for(int j = 0; j < BOARD_SIZE; j++) {
		    	toRet += ((i+j)%2 == 0) ? pieceToString(squares[i][j]) : "  ";
		    }
		    toRet += "|\n";
		}
		toRet += dashes + "\n";
		toRet += "The next player to move is:" + turnToMove + "\n";
		toRet += dashes + "\n\n";
		
		return toRet;
    }
    public String pieceToString(Piece p) {
		String ret = null;
		if(p == Piece.BLACK_NORMAL) {
		    ret = "bn";
		} else if(p == Piece.RED_NORMAL) {
		    ret = "rn";
		} else if(p == Piece.BLACK_KING) {
		    ret = "BK";
		} else if(p == Piece.RED_KING) {
		    ret = "RK";
		} else {
			ret = "..";
		}
		return ret;
	}
    
    public String asBytes() {
        int n = BOARD_SIZE*BOARD_SIZE;
        char[] cs = new char[BOARD_SIZE*BOARD_SIZE];
        for (int i = 0; i < n; i++) {
            int r = BOARD_SIZE - (i/BOARD_SIZE) - 1;
            int c = i%BOARD_SIZE;
            Piece p = pieceAt(r,c);
            if (p != null) {
                cs[i] = p.toChar();
            } else {
                cs[i] = '.';
            }
        }
        return new String(cs);
    }
}
