package cs51checkers;

public class Location {
	public final int ROW;
	public final int COL;
	
	public Location(int row, int col) {
		this.ROW = row;
		this.COL = col;
	}

    public boolean equals(Location other) {
	    return this.ROW == other.ROW && this.COL == other.COL;
    }

    /* Note that the toString() method has already been written here,
     * but perhaps in a format that might not be useful for you if you
     * are trying to create a nice, concise toString method in Move. */
    public String toString() {
	    return "Location(" + ROW + "," + COL + ")";
    }
}
