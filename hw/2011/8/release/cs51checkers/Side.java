package cs51checkers;

public enum Side {
	RED, BLACK;
	
	public static Side otherSide(Side side) {
		return (side == BLACK) ? RED : BLACK;
	}
}