package cs51checkers;

// Immutable.
public interface Taken {
    public boolean isTaken(int r, int c);
    public boolean isTaken(Location l);
    public Taken take(Location l);
    public Taken unTake(Location l);
    public boolean equals(Taken other);
}