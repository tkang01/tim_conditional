package cs51checkers;

import java.util.ArrayList;

// Not immutable, but capable of returning a fresh copy of itself.
public class Move {
    /* locSequence is a list of  List of locations, starting with where
     * the piece making this move starts, ending with the destination,
     * including all intermediate jump points.
     * That is, if this move is not a jump/capture or if it only
     * jumps on piece, locSequence will have just 2 entries.  The size of
     * locSequence will grow beyond  that with the number of additional
     * pieces that are jumped in the course of completing this move.*/
    private ArrayList<Location> locSequence;

    /* taken is "bit array" including all locations where you took a piece
     * _AND_ the start location (this is a bit hack-ish).
     * 
     * You should *not* worry about taken when you are writing toString,
     * as it merely enables some book-keeping that is internal to 
     * the operation of Moves and Boards. */
    private Taken taken;
    private boolean captureMove;
    
    public Move(Location startLoc) {
        locSequence = new ArrayList<Location>();
        taken = new BooleanTaken(Board.BOARD_SIZE);
        locSequence.add(startLoc);
        recordTaken(startLoc);
        captureMove = false; // Yes, we have "taken" this location, but that
        // doesn't really mean that this is a capture move.
    }
    
    public Move(Location startLoc, Location endLoc) {
    	this(startLoc);
    	locSequence.add(endLoc);
    }
    
    private Move(ArrayList<Location> locSeq, Taken taken, boolean captureMove) {
        // safe whether or not clone is a deepy copy, because Location
        // objects are immutable.
        this.locSequence = (ArrayList<Location>)locSeq.clone();
        this.taken = taken; // safe because Taken objects are immutable
        this.captureMove = captureMove;
    }
    
    public void addLocation(Location loc) {
        locSequence.add(loc);
    }
    
    public Location[] getLocations() {
        Location[] ret = new Location[locSequence.size()];
        locSequence.toArray(ret);
        return ret;
    }
    
    public void recordTaken(Location loc) {
        taken = taken.take(loc); // because Takens are immutable.
        captureMove = true;
    }
    
    public Taken getTaken() {
        return taken;
    }
    
    public Location getStartLocation() {
    	return locSequence.get(0);
    }
    
    public boolean isCapture() {
    	return captureMove;
    }
    
    public Object clone() {
        return new Move(locSequence, taken, captureMove);
    }

    public boolean equals(Move other) {
        if (locSequence.size() != other.locSequence.size()) {
            return false;
        }
        for (int i = 0; i < locSequence.size(); i++) {
            if (!locSequence.get(i).equals(other.locSequence.get(i))) {
                return false;
            }
        }
        return captureMove == other.captureMove && taken.equals(other.taken);
    }


    /* Implement your toString method here. */
}
