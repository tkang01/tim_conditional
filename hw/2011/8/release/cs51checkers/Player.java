package cs51checkers;

public interface Player {
	public Move play(Board board, Side side)
	    throws InvalidLocationException;
}
