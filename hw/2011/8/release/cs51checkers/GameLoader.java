package cs51checkers;

import java.lang.ClassLoader;
import java.io.File;
import java.lang.ClassNotFoundException;
import java.util.HashMap;

public class GameLoader {
    public static Player load(String classname) throws ClassNotFoundException {
        String glName = GameLoader.class.getName();
        String pkgName = glName.substring(0,glName.lastIndexOf('.'));
        String fullName = pkgName + "." + classname;
        ClassLoader cl = ClassLoader.getSystemClassLoader();
        Class PlayerClass = cl.loadClass(fullName);
        Player cp;
        try {
            cp = (Player)PlayerClass.newInstance();
        } catch (Exception e) {
            System.out.println(e);
            throw new ClassNotFoundException(classname);
        }
        return cp;
    }
}
