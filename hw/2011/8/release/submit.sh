#!/bin/bash -e

if test -f /usr/local/bin/submit; then
    rm -f cs51checkers/*.class
	rm -rf ps8-submit
	mkdir ps8-submit
	cp -r cs51checkers ps8-submit
	/usr/local/bin/submit lib51 8 `pwd`/ps8-submit
	echo "Done!"
else
    echo "You must run this on one of the nice.fas machines"
fi
