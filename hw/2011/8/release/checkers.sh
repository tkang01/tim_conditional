#!/usr/bin/env bash

set -e


#CP=junit-4.8.1.jar:.
CP=.

#compile 
javac -cp $CP cs51checkers/Player.java cs51checkers/*.java

#run
java cs51checkers.Checkers $*
