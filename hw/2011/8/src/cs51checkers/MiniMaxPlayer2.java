package cs51checkers;

import java.util.ArrayList;
import java.util.Random;

interface ValMove {
	boolean hasMove();
	int getVal();
}

class EvaluatedMove2 implements ValMove {
	final private int val;
	final public Move m;
	public EvaluatedMove2(Move m, int val) {
		this.m = m;
		this.val = val;
	}
	public boolean hasMove() {return true;}
	public int getVal() {return val;}
}

class ValueOnly implements ValMove {
	final private int val;
	public ValueOnly(int val) {
		this.val = val;
	}
	public boolean hasMove() {return false;}
	public int getVal() {return val;}
}

public class MiniMaxPlayer2 implements Player {
    public static final int SEARCH_DEPTH = 4;

	public MiniMaxPlayer2() {
	}
	
	private int evaluate(Board board, Side side) {
		//Not too smart. Just return number of pieces
        //Not too smart. Just return number of pieces
        Location[] pieces = board.getPieceLocations();
        Piece p;
        int n=0;
        for (Location l : pieces) {
                p = board.pieceAt(l);
                if (p.SIDE == side) {
        n++;
        if (p.TYPE == PieceType.KING) {
            n++;
        }
    } else {
        n--;
        if (p.TYPE == PieceType.KING) {
            n--;
        }
    }
        }
return n;

	}
	
	private ValMove minimax(Board board, Side side, int depth) {
		if (depth<=0) return new ValueOnly(evaluate(board, side));
		ArrayList<Move> allPoss = new ArrayList<Move>();
		for(int i = 0; i < Board.BOARD_SIZE; i++) {
			for(int j = 0; j < Board.BOARD_SIZE; j++) {
				allPoss.addAll(board.legalMoves(new Location(i, j)));
			}
		}
		boolean checked=false;
		//Find the move that minimizes the other player's minimax
		//value.
		int bestMoveEval=0;
		Move bestMove = new Move(new Location(0,0));
		ValMove otherMoveEval;
		Board newBoard;
		for(Move m : allPoss) {
			try {
				otherMoveEval = minimax(board.move(m), Side.otherSide(side), depth-1);
				if((!checked)||(otherMoveEval.getVal() < bestMoveEval)) {
					bestMove = m;
					bestMoveEval = otherMoveEval.getVal();
				}
			} catch (IllegalMoveException e) {

			}

		}
		//Return the best move, together with the inverse of its
		//value for the other player.
		return new EvaluatedMove2(bestMove, 0 - bestMoveEval);
	}
	
	public Move play(Board board, Side side) {
		ValMove vm = minimax(board, side, 5); //Replace magic number with something
		if(vm.hasMove()) {
			EvaluatedMove2 em = (EvaluatedMove2)vm;
			return em.m;
		}
		else {
			//What to do here?
			return new Move(new Location(0,0));
		}
	}
}
