package cs51checkers;

import java.util.ArrayList;
import java.lang.Runnable;
import java.lang.Thread;
import java.lang.Throwable;

public class GameEngine {
    private class RoundRunnable implements Runnable {
        public RoundRunnable(Player p, Board b, Side s) {
            _player = p;
            _board = b;
            _side = s;
            _exn = null;
            _move = null;
        }
        public void run() {
            Move m;
            try {
                m = _player.play(_board,_side);
            } catch (InvalidLocationException ile) {
                _exn = ile;
                return;
            }
            if (m == null) {
                _exn = new IllegalMoveException();
                return;
            }
            _move = m;
            return;	    
        }
        public Throwable getExn() {
            return _exn;
        }
        public Move getMove() {
            return _move;
        }
        public boolean finished() {
            return !((_exn == null) && (_move == null));
        }
        private Player _player;
        private Board _board;
        private Side _side;
        private Throwable _exn;
        private Move _move;
	    
    }
    private Board board;
    private Player redPlayer;
    private Player blackPlayer;
    private Player currentPlayer;
    public static final int maxRoundTimeMillis = 1000;
    private boolean enableTimeLimit;

    private int roundDelayMs;

    public GameEngine(Player redPlayer, Player blackPlayer, int roundDelayMs, boolean timeLimit) {
        board = new StandardBoard();
        this.redPlayer = redPlayer;
        this.blackPlayer = blackPlayer;
        this.currentPlayer = redPlayer;
	    this.enableTimeLimit = timeLimit;
        this.roundDelayMs = roundDelayMs;
    }
    // Returns false if it is impossible to play another round.
    public boolean playRound() throws CheckersException,Throwable {
        // Keep track of whose turn it is, what AI to call, etc
        Side currside;
        Player nextPlayer;
        if (currentPlayer == redPlayer) {
            currside = Side.RED;
            nextPlayer = blackPlayer;
        } else {
            currside = Side.BLACK;
            nextPlayer = redPlayer;
        }
        RoundRunnable rr = new RoundRunnable(currentPlayer,
                                             getBoard(), currside);
        Thread tr = new Thread(rr, "round thread");
        tr.setDaemon(true);
        tr.start();

        int delay;
        if (enableTimeLimit) {
            delay = maxRoundTimeMillis;
        } else {
            delay = 0;
        }

        try {
            tr.join(delay);
        } catch (java.lang.InterruptedException ie) {
            // jhoon: I don't really think this is the right way to
            // handle this.
            throw new OutOfTimeException();
        }
        if (!rr.finished()) {
            tr.stop();
            throw new OutOfTimeException();
        } else if (rr.getExn() != null) {
            throw rr.getExn();
        }
        Move m = rr.getMove();
        if (!board.isLegal(m)) {
            throw new IllegalMoveException();
        }
        board = board.move(m);
        currentPlayer = nextPlayer;
        return !board.isOver();
    }
    
    public Side play() throws CheckersException,Throwable {
        System.out.println(board);
        while (playRound()) {
            try {
                Thread.sleep(roundDelayMs);
            }
            catch(InterruptedException e) {}
            System.out.println(board);
            System.out.println();
        }
        System.out.println(board);

        return board.winner();
    }
    
    public Board getBoard() {
        return board;
    }
}
