package cs51checkers;

import java.lang.ClassLoader;
import java.io.File;
import java.lang.ClassNotFoundException;
import java.util.HashMap;

public class GameLoader {
    public static Player load(String classname) throws ClassNotFoundException {
        if (!checkClassName(classname)) {
            System.out.println("ouch");
            throw new ClassNotFoundException("Invalid class name: "
                                             + classname);
        }
        String glName = GameLoader.class.getName();
        String pkgName = glName.substring(0,glName.lastIndexOf('.'));
        String fullName = pkgName + "." + classname;
        ClassLoader cl = ClassLoader.getSystemClassLoader();
        Class PlayerClass = cl.loadClass(fullName);
        Player cp;
        try {
            cp = (Player)PlayerClass.newInstance();
        } catch (Exception e) {
            System.out.println(e);
            throw new ClassNotFoundException(classname);
        }
        return cp;
    }
    private static boolean checkClassName(String classname) {
        return true;
//        return classname.matches("^\\w+Player$");
    }
    
}