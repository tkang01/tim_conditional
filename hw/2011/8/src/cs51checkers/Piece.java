package cs51checkers;

public class Piece {
	public final Side SIDE;
	public final PieceType TYPE;
	
	private Piece(Side side, PieceType type) {
		this.SIDE = side;
		this.TYPE = type;
	}

    public static final Piece getPiece(char c) {
	switch (c) {
	case 'r':
	    return RED_NORMAL;
	case 'R':
	    return RED_KING;
	case 'b':
	    return BLACK_NORMAL;
	case 'B':
	    return BLACK_KING;
	default:
	    throw new java.lang.IllegalArgumentException();
	}
    }
    public char toChar() {
	char c;
	if (SIDE == Side.RED) {
	    c = 'r';
	} else {
	    c = 'b';
	}
	if (TYPE == PieceType.KING) {
	    c = Character.toUpperCase(c);
	}
	return c;
    }
	
	public static final Piece RED_NORMAL = new Piece(Side.RED, PieceType.NORMAL);
	public static final Piece RED_KING = new Piece(Side.RED, PieceType.KING);
	public static final Piece BLACK_NORMAL = new Piece(Side.BLACK, PieceType.NORMAL);
	public static final Piece BLACK_KING = new Piece(Side.BLACK, PieceType.KING);
	
	public static Piece getPiece(Side side, PieceType type) {
		if(side == Side.RED) {
			return (type == PieceType.NORMAL) ? RED_NORMAL : RED_KING;
		}
		else {
			return (type == PieceType.NORMAL) ? BLACK_NORMAL : BLACK_KING;
		}
	}
}
