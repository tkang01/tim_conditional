package cs51checkers;

//import cs51checkers.*;

class Checkers {
    public static void main(String args[]) {
        String usage = "Usage: <program> <player class> <player class> <round delay (ms)> <enable time limit (t|f)>";
        if (args.length != 4) {
            System.out.println("Wrong number of arguments.");
            System.out.println(usage);
            return;
        }
        String redName = args[0];
        String blackName = args[1];
        int roundDelayMs = Integer.parseInt(args[2]);
    	boolean enableTimeLimit = args[3].toLowerCase().startsWith("t");

        Player red;
        Player black;
        try {
            red = GameLoader.load(redName);
            black = GameLoader.load(blackName);
        } catch (java.lang.ClassNotFoundException cnfe) {
            System.out.println(cnfe);
            return;
        }
        GameEngine engine = new GameEngine(red,black,roundDelayMs,enableTimeLimit);
        Side winner;
        try {
            winner = engine.play();
        } catch (Throwable t) {
            t.printStackTrace();
            return;
        }
        String msg;
        if (winner == Side.RED) {
            msg = "Red wins.";
        } else if (winner == Side.BLACK) {
            msg = "Black wins.";
        } else {
            msg = "Tie game.";
        }
        System.err.println(msg);
    }
}
