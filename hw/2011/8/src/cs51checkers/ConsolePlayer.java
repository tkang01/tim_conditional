package cs51checkers;

import java.io.IOException;
import java.util.Random;
import java.util.ArrayList;
import java.io.*;


public class ConsolePlayer implements Player {
	BufferedReader reader = new BufferedReader (new InputStreamReader(System.in));

	public ConsolePlayer() {
	}
	
	private int readInt(int low, int high) {
		int n=0;
		try {
			n = Integer.parseInt(reader.readLine());
		}
		catch (IOException e) {
			n=-1;
		}
		catch (NumberFormatException e2) {
			n=-1;
		}
		while ((n<low)||(n>high)) {
			System.out.println("Invalid Input. Input should be in the range " + Integer.toString(low) + "-" + Integer.toString (high) + ".");
			try {
				n = Integer.parseInt(reader.readLine());
			}
			catch (IOException e) {
				n=-1;
			}
			catch (NumberFormatException e2) {
				n=-1;
			}
		}
		return n;
	}
	
	public Move play(Board board, Side side) {
		ArrayList<Move> moves= new ArrayList<Move>();
		int col=-1;
		int row=-1;
		System.out.println("Choose a row(0-7)");
		row = readInt(0, 7);
		System.out.println("Choose a column(0-7)");
		col = readInt(0, 7);
		moves.addAll(board.legalMoves(new Location(row, col)));
		while(moves.size()==0) {
			row=-1;
			col=-1;
			System.out.println("No legal moves from that location.");
			System.out.println("Choose a row(0-7)");
			row = readInt(0, 7);
			System.out.println("Choose a column(0-7)");
			col = readInt(0, 7);
			moves.addAll(board.legalMoves(new Location(row, col)));			
		}
		int n=0;
		if(moves.size()>1) {
			System.out.println("Multiple moves possible. Choose a move.");
			int i=0;
			for (Move m : moves) {
				i++;
				System.out.println(Integer.toString(i) + "." + m.toString());
			}
			n = readInt(1, moves.size()) - 1;
		}
		return moves.get(n);
	}
}
