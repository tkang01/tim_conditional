package cs51checkers;

import java.util.ArrayList;
import java.util.Random;

class EvaluatedMove {
	public Move m;
	public int val;
	public EvaluatedMove(Move m, int val) {
		this.m = m;
		this.val = val;
	}
}

public class MiniMaxPlayer implements Player {
    public static final int SEARCH_DEPTH = 4;
    
	public MiniMaxPlayer() {
	}
	
	private int evaluate(Board board, Side side) {
		//Not too smart. Just return number of pieces
		Location[] pieces = board.getPieceLocations();
		Piece p;
		int n=0;
		for (Location l : pieces) {
			p = board.pieceAt(l);
			if (p.SIDE == side) {
                n++;
                if (p.TYPE == PieceType.KING) {
                    n++;
                }
            } else {
                n--;
                if (p.TYPE == PieceType.KING) {
                    n--;
                }
            }
		}
        return n;
	}
	
	private EvaluatedMove minimax(Board board, boolean maximize, Side side, int depth) {
		int boardEval = 0;
		if (depth <= 0) {
            return new EvaluatedMove(null, evaluate(board, side));
        }
		//Find all possible moves
		ArrayList<Move> allPoss = new ArrayList<Move>();
		for(int i = 0; i < Board.BOARD_SIZE; i++) {
			for(int j = 0; j < Board.BOARD_SIZE; j++) {
				allPoss.addAll(board.legalMoves(new Location(i, j)));
			}
		}
		//Find the move that minimizes the other player's minimax
		//value, if we're minimizing.
		//Maximize our value if we're maximizing.
        int bestVal =0;
        if (maximize) {
            bestVal = Integer.MIN_VALUE;
        } else {
            bestVal = Integer.MAX_VALUE;
        }
		Move bestMove = null;
		for(Move m : allPoss) {
            EvaluatedMove otherMove = null;

            try {
                otherMove = minimax(board.move(m), !maximize, Side.otherSide(side), depth - 1);
            } catch (IllegalMoveException e) {
                System.err.println("Oh noes!  Bad bad move.  Bug in engine");
                System.exit(1);
            }
            
			if((maximize && otherMove.val > bestVal) ||
               (!maximize && otherMove.val < bestVal))
            {
				bestMove = m;
				bestVal = otherMove.val;
			}
		}
		//Return the best move, together with the inverse of its
		//value for the other player.
        //System.out.println("Maximizing = " + maximize + ";  Side= " + side +
        //                   "; Best move score = " + bestVal);
		return new EvaluatedMove(bestMove, bestVal);
	}
	
	public Move play(Board board, Side side) {
        //System.out.println("Looking for best move for "+ side);
		EvaluatedMove em = minimax(board, true, side, SEARCH_DEPTH);
		return em.m;
	}
}
