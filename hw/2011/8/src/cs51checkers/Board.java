package cs51checkers;

import java.util.List;

/* Implementations of the Board class should be immutable.
 * The only class that currently implements Board is StandardBoard
 * So long as you are only working with implementations of the 
 * Player interface, though, you will never need to instantiate a
 * board, so you will never need to work directly with StandardBoard.
 */
  

public interface Board {
    public final int BOARD_SIZE = 8;
    public final int DRAW_MOVE_LIMIT = 50;
   

    /* Takes a move object and returns a new Board that has been
     * updated to reflect the fact that this move has occurred.
     * This includes updating "whose turn it is."
     * If the Move passed is not in the set of legal moves,
     * throws IllegalMoveException. */
    public Board move(Move m) throws IllegalMoveException;

    /* Returns true iff m is a legal move. */
    public boolean isLegal(Move m);

    /* Returns true if the game is over.  This should always be invoked
     * before calling winner(), because winner() throws a
     * GameNotOverException if the game is not over.  You should be getting
     * annoyed reading this comment, because it should feel like this
     * could have been done better.  Keep reading about what winner() does
     * to learn more. */
    public boolean isOver();

    /* winner() returns an object of the enum Side.
     * If red or black has won, this is Side.RED or Side.BLACK, respectively.
     * If the game has ended in a draw, winner() returns null.
     * If the game is not over, this throws a GameNotOverException.
     *
     * Back to our discussion of why this design isn't very good.
     * If this were ML, we'd have defined a new algebraic datatype
     * that looked something like
     *      type game_status = Incomplete | Draw | WonBy of side ;;
     * We don't have algebraic data types in Java, and when writing this
     * method we found ourselves faced with the problem of needing
     * 3 possible return types.  We picked one type, and chose "null"
     * to simulate the second (Draw).  Then what?  We considered
     * the game being over to be an exception, thereby labelling it a
     * failure state, and saying that you should only call winner()
     * if isOver() has returned true.  We chose this because it seemed
     * less ridiculous than the immediate alternative -- simulating
     * algebraic data types by creating an entirely new enum
     * just for game states.  Even then, we wouldn't have been able
     * to encode as cleanly the possibility that objects of the game state
     * enum might encode objects of the Side enum. */
    public Side winner() throws GameNotOverException;

    /* Returns the piece at the given location, or null if there is
     * no piece. */
    public Piece pieceAt(Location l);

    /* Same as above, except that it takes a row and column. */
    public Piece pieceAt(int row, int col);

    /* Returns all of the legal moves that can be made, given a location.
     * The return value of this method is based on whose turn it is to move
     * next, which is maintained as internal state of the board.
     * If l defines a location that either has no piece or has a piece
     * that belongs to the "wrong" Side, legalMoves returns an empty list.
     */
    public List<Move> legalMoves(Location l);


    /* Returns a counter of the total number of moves that have
     * been taken (by either player) so far in the game. */
    public int moveNumber();

    /* Returns a counter of the number of moves that have been
     * taken on track to a draw.  That is, winner() will return null
     * once the return value of movesTowardDraw is greater than or
     * equal to DRAW_MOVE_LIMIT.
     * As stated in the writeup, this counter of moves toward a draw
     * will be reset in the event that 1) a non-king piece moves,
     * or 2) a piece is captured. */
    public int movesTowardDraw();

    /* Returns an array of locations in the board that currently
     * have pieces.  That is, a Location l will be in the array
     * returned by getPieceLocations() iff pieceAt(l) would not
     * return null. */
    public Location[] getPieceLocations();
    
}
