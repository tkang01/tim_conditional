#!/usr/bin/env bash

#javac cs51checkers/*.java

rm $1_results

echo "PS7 Test results for $1" >> $1_results
echo "" >> $1_results
echo "Playing 20 games against DummyPlayer." >> $1_results
echo "$2Player is red, DummyPlayer is black." >> $1_results
echo "" >> $1_results


for (( i=0; i<20; i++ ))
do
  java cs51checkers.Checkers $2Player DummyPlayer 0 t 2>> $1_results > /dev/null
done

echo "" >> $1_results

echo "Playing 1 game against solution MiniMaxPlayer." >> $1_results
echo "$2Player is red, solution is black." >> $1_results
echo "" >> $1_results


java cs51checkers.Checkers $2Player MiniMaxPlayer 0 t 2>> $1_results > /dev/null

cat $1_results