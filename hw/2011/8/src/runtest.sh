#!/usr/bin/env bash

set -e
CP=`pwd`/junit-4.8.1.jar:.

javac -cp $CP Player.java *.java
cd ..
java -cp $CP org.junit.runner.JUnitCore cs51checkers.CheckersTests