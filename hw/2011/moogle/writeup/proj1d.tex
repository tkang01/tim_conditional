\documentclass[\MODE]{pset}

%%% Hey you!  Don't edit this file unless you know what you're doing.
%%% This is NOT plain LaTeX; it passes through a preprocessor.
%%% Lines like the next (non-blank) one are preprocessor directives:
%tag=


\usepackage{fullpage}
\usepackage{graphicx}
\usepackage{multicol}

\lstset{language=Scheme}
\hyphenpenalty=500
\def\due{Friday, 13 March 2009 at 11:59 PM}

\title[$10^{100-\epsilon}$]{CS51 Project 1d}
\date{Due: \due}

\setstyle{5}
\begin{document}

\maketitle

%> ;; CS51 Project 1
%> ;; Part d
%> ;;
%> ;; File:          proj1d.scm
%> ;; Author:        YOUR-NAME <YOUR-EMAIL@fas.harvard.edu>
%> ;; Collaborators: YOUR-PARTNER <YOUR-PARTNERS-EMAIL@fas.harvard.edu>
%> ;;
%> ;; Collaboration *is required* on this assignment.  By submitting
%> ;; this assignment, you certify that you have followed the CS51
%> ;; collaboration policy.  If you are unclear on the CS51 collaboration
%> ;; policy, please ask your TF or the head TF.
%>
%> 

%> #lang s-exp CS51
%> 
%> (require CS51/ps4/graph)
%> (require CS51/babbler/nodescore)
%> 
%> ; Takes a graph and a list of lists (from, to, [weight]), with an optional
%> ; edge weight.  Returns all the edges added to the graph as a new graph. 
%> ; The default edge weight is 1.
%> (define (graph-add-edges g edges) 
%>   (foldr (lambda (x r) 
%>            (graph-add-edge (car x) (cadr x) r 
%>                            ; Weight 1 by default...
%>                            (if (= (length x) 3) (caddr x) 1)))
%>          g edges))


\noindent This is the fourth and final part of Project 1.  Having
worked with ADTs, graphs and random processes, we will now put them
together to explore solutions to a compelling problem: finding
``important'' nodes in graphs like the Internet, such as the World
Wide Web.

The concept of assigning a measure of importance to nodes is very
useful in designing search algorithms, such as those that many popular
search engines rely on.  Early search engines often ranked the
relevance of pages based on the number of times that search terms
appeared in the pages. However, it was easy for spammers to game this
system by including popular search terms many times, propelling their
results to the top of the list.

When you enter a search query, you really want the important pages:
the ones with valuable information, a property often reflected in the
quantity and quality of other pages linking to them.  Better
algorithms were eventually developed that took into account the
relationships between web pages, as determined by links.
\footnote{For more about the history of search engines, you can check
out this page: http://en.wikipedia.org/wiki/Search\_engine.}  These
relationships can be represeted nicely by a graph structure, which is
what we'll be using here.

\section{NodeScore ADT}
Throughout the assignment, we'll want to maintain associations
of graph nodes to their importance, or ``NodeScore'':
a value between $0$ (completely unimportant) and
$1$ (the only important node in the graph). 

In order to assign NodeScores to the nodes in a graph, we've provided
a module with an implementation of an ADT, |nodescore|, to hold such
associations.  |nodescore| has the following interface:

\begin{scheme}
  [nodescore? (-> any/c boolean?)]
  [ns-new nodescore?]
  [ns-empty? (-> nodescore? boolean?)]
  [ns->string (-> nodescore? string?)]
  [ns-scale (-> number? nodescore? nodescore?)]
  [ns-normalize (-> nodescore? nodescore?)]
  [ns-nodes (-> nodescore? list?)]
  [ns-get-score (-> any/c (and/c nodescore? (not/c ns-empty?)) number?)]
  [ns-set-score (-> any/c number? nodescore? nodescore?)]
  [ns-add-score (-> any/c number? nodescore? nodescore?)]
\end{scheme}

The |nodescore| structure makes it easy to create, modify,
normalize (to sum to $1$), and display NodeScores.
More detailed documentation can be found in the implementation
in |nodescore.ss|.

\section{Testing}

Testing these algorithms is hard, because most of them involve a lot of
randomness. We aren't giving any explicit points for testing on this
problem set. However, you will doubtless want to test your code to make
sure that it works! For the deterministic algorithm
(|in-degree-nodescore|), you can do a small example by hand and then
check that your algorithm gives the correct result. However, for the
rest of them, you can't do much better than running it a bunch of times
for a large number of iterations and checking that it always converges
towards something sensible. The graph |tst-graph| in particular is good
for testing, since it's fairly clear what the nodescore should be.

We've defined many test graphs for you at the top of the .scm file (not
reproduced here). Feel free to use any of these for your testing, or to
define any other example graphs you'd like to use.

%>
%>;;;;;;;;;;;;;;;;;;;;;;;; BEGIN SAMPLE GRAPH DEFINITIONS ;;;;;;;;;;;;;;;;;
%>
%>(define tst-graph (graph-add-edges graph-new '((A A 0.5)
%>                                               (A B 0.5)
%>                                               (B A 0.6)
%>                                               (B B 0.4))))
%>
%>(define first-internet (graph-add-edges graph-new
%>                                        '((me you)
%>					  (him you)
%>					  (them you)
%>					  (everyone you)
%>					  (me him))))
%>(define mini-internet (graph-add-edges graph-new
%>                                       '((myspace.com myspace.com 0.4)
%>                                         (myspace.com facebook.com 0.5)
%>                                         (facebook.com myspace.com 0.75)
%>                                         (facebook.com facebook.com 0.25)
%>                                         (xanga.com facebook.com 0.9)
%>                                         (xanga.com xanga.com 0.1)
%>                                         (myspace.com eharmony.com 0.1))))
%>(define better-internet (graph-add-edges graph-new
%>                                  '((google.com del.icio.us 0.5)
%>                                    (google.com facebook.com 0.5)
%>                                    (del.icio.us google.com 0.3)
%>                                    (del.icio.us facebook.com 0.7)
%>                                    (facebook.com wordpress.com 1.0)
%>                                    (wordpress.com del.icio.us 0.8)
%>                                    (wordpress.com photosynth.net 0.1)
%>                                    (wordpress.com digg.com 0.1)
%>                                    (photosynth.net google.com 1.0)
%>                                    (digg.com facebook.com 0.9)
%>                                    (digg.com reddit.com 0.1)
%>                                    (reddit.com digg.com 0.9)
%>                                    (reddit.com xkcd.com 0.1)
%>                                    (xkcd.com cs51.seas.harvard.edu 1)
%>                                    (cs51.seas.harvard.edu xkcd.com 1))))
%>
%>(define harvard-graph (graph-add-edges graph-new
%>                                          '((Harvard HCL 0.15)
%>                                            (Harvard Academics 0.25)
%>                                            (Harvard UHS 0.1)
%>                                            (Harvard GoCrimson 0.3)
%>                                            (Harvard HUDS 0.2)
%>                                           (HUDS GoCrimson 1)
%>                                            (GoCrimson UHS 1)
%>                                            (UHS HCL 1)
%>                                            (Academics HCL 0.5)
%>                                            (Academics Schools 0.5)
%>                                            (Schools Harvard 0.92)
%>                                            (Schools SEAS 0.08)
%>                                            (SEAS Undergrad 0.1)
%>                                            (SEAS Harvard 0.9)
%>                                            (Undergrad Courses 0.1)
%>                                            (Undergrad Harvard 0.9)
%>                                            (Courses CS51 0.05)
%>                                            (Courses Harvard 0.95)
%>                                            (CS51 Harvard 1))))
%>
%>;; Inspired by xkcd's Online Communities: http://xkcd.com/256/
%>(define xkcd-online-coms (graph-add-edges graph-new
%>                  '((WoW yahoo-games 0.6)
%>                    (WoW sea-of-memes 0.4)
%>                    (digg straits-of-web-2-0 0.1)
%>                    (digg reddit 0.9)
%>                    (facebook flickr 0.1)
%>                    (facebook myspace 0.2)
%>                    (facebook sea-of-memes 0.5)
%>                    (facebook viral-straits 0.2)
%>                    (flickr friendster 0.6)
%>                    (flickr wikipedia 0.4)
%>                    (friendster ocean-of-subculture 0.9)
%>                    (friendster wikipedia 0.1)
%>                    (gulf-of-youtube deviant-art 0.4)
%>                    (gulf-of-youtube last.fm 0.6)
%>                    (last.fm flickr 0.1)
%>                    (last.fm ocean-of-subculture 0.9)
%>                    (myspace facebook 0.6)
%>                    (myspace livejournal 0.4)
%>                    (noob-sea myspace 1.0)
%>                    (ocean-of-subculture sea-of-memes 0.7)
%>                    (ocean-of-subculture the-icy-north 0.3)
%>                    (p2p-shoals gulf-of-youtube 1.0)
%>                    (reddit digg 0.3)
%>                    (reddit youtube 0.7)
%>                    (sea-of-memes the-blogipelago 1.0)
%>                    (straits-of-web-2-0 p2p-shoals 0.9)
%>                    (straits-of-web-2-0 noob-sea 0.1)
%>                    (the-blogipelago facebook 0.8)
%>                    (the-blogipelago livejournal 0.2)
%>                    (the-icy-north p2p-shoals 0.8)
%>                    (the-icy-north the-blogipelago 0.2)
%>                    (usenet noob-sea 0.2)
%>                    (usenet the-blogipelago 0.8)
%>                    (viral-straits sea-of-memes 1)
%>                    (wikipedia flickr 0.9)
%>                    (wikipedia WoW 0.1)
%>                    (yahoo-games ocean-of-subculture 1.0))))
%>
%>;;;;;;;;;;;;;;;;;;;;;;;;;; END SAMPLE GRAPH DEFINITIONS ;;;;;;;;;;
%>
\section{NodeScore Algorithms}

In this section, we'll write a series of NodeScore
algorithms: that is, functions that take a graph and return
a |nodescore| on it. As an example, we've implemented a
trivial NodeScore algorithm (|uniform-nodescore|)
that gives all nodes equal score
(normalized to sum to $1$, of course).

%> ; A trivial nodescore algorithm: every node has equal score
%> ;   (normalized to sum to 1).
%> (define (uniform-nodescore g)
%>   (ns-normalize
%>     (foldr (lambda (n r) (ns-set-score n 1 r))
%> 	   ns-new (graph-nodes g)))) 

\subsection{In-Degree NodeScore}

A somewhat less trivial algorithm is the following: a node's score is
proportional to the number of nodes linking to it (not including
itself). If we apply this to our understanding of the internet as a
graph, this means that a page becomes more relevant the more pages
that link to it. This is an improvement over the uniform-nodescore
algorithm because the nodescore of a webpage is determined by the
value that other webpages place on it by linking to it.

\begin{exercise} ?
\part {3}
%comment!
%fill=;
Implement |in-degree-nodescore|, which takes a graph and returns a nodescore
on the graph giving each node a score proportional to the number of other
nodes linking to it (not including itself). The nodescore should be
normalized, but may be the zero nodescore if the graph has no non-self edges.
You will likely want to follow the template of our example above; think
carefully about each of the arguments you pass to the function(s) you call.
%end!
%>;
%>; (define (in-degree-nodescore g)
%>;    YOUR-ANSWER-HERE)
%>
%>

\A*[
\begin{scheme}
(define (in-degree-nodescore g)
  (ns-normalize
    (foldr
      (lambda (e ns)
        (if (equal? (edge-source e) (edge-dest e)) ns
          (ns-add-weight (edge-dest e) 1 ns)))
      ns-new
      (graph-edges g))))
\end{scheme}
\A]

This approach works well for many graphs.  But suppose some scurvy
spammer sets out to subvert the system: all he has to do is set up a
bunch of dummy pages to link to the page that he wants to become
popular. If that page would typically be found by searching a
particularly competitive search term, such as "cars" or "hotels", now
the spammer's page, which has many other pages linking to it, would
suddenly seem extraordinarily relevant and shoot up in the rankings.

What we'd really like is a way to conclude that those dummy pages
really are dummy pages, and don't signify that a lot of people
value the spammer's page. That seems easy, since no one links
to the dummy pages. So what if we don't count an edge if its
source node itself is not linked to?

\part {1}
Explain how the spammer still wins. Arrr!
%>
%>; Explain how the spammer still wins, even if we only count edges from
%>; nodes that are linked to by some other node in the graph. Arrr!
%>;
%>; YOUR-ANSWER-HERE
%>;
%>

\subsection{Sisyphean NodeScore}
We need a better way of saying that nodes are popular or unpopular,
considering global properties of the graph, and not just edges
adjacent to or near the nodes in question. For example, there could be
an extremely relevant webpage that is several nodes removed from the
node we start at. That node would normally fare pretty low on our
ranking system, but perhaps it should be higher based on there being a
high probability that the node could be reached when browsing around
on the internet.

So consider Sisyphus, doomed to crawl the Web for eternity: or more
specifically, doomed to start at some arbitrary page, and follow links
randomly (choosing according to their weights). \footnote{We assume
the Web is strongly connected and that every page has an outgoing
link, unrealistic assumptions that we will return to address later.}
Let's say Sisyphus can take $k$ steps after starting from a random
node - how could we design a system to determine nodescores that is
based off how likely it is that he will reach a certain page?  Hint:
by asking, where will Sisyphus spend most of his time?

\part {8}
%comment!
Implement |sisyphus-nodescore|, which takes a graph and a number $k$,
and returns a normalized nodescore on the graph where the score for
each node is proportional to the number of times Sisyphus visits the
node in $k$ steps, starting from a random node. Your function may
error if some node has no outgoing edges.

You will find the code you wrote for your babbler to pick a random
outgoing edge from a node to be useful. You will have to modify it
slightly to work with edges with fractional weights. The |random|
function, which takes no parameters and returns a random number
in the range (0, 1), will be useful.  (Note that |(random n)| will
return a random {\em integer} between 0 and |n|).
%end!

%>
%>; (define (sisyphus-nodescore k g)
%>;   YOUR-ANSWER-HERE)
%>
%end!

\A*[
\begin{scheme}
; Walk the internets (graph) for n steps, starting at a random node, and 
; assuming that all nodes have outgoing edges.
(define (sisyphus-nodescore n graph )
  (ns-normalize (sisyphus-nodescore-inner n graph 
                                          (random-elt (graph-nodes graph))
                                          ns-new)))

(define (sisyphus-nodescore-inner n g node ns)
  (if (< n 1) ns ; done
      ; otherwise pick a random neighbor to go next.
      (let ([next (edge-dest (pick-random-edge node g))])
        ; and go there, adding 1 to its nodescore
        (sisyphus-nodescore-inner (- n 1) g next
                                  (ns-add-score next 1 ns)))))
\end{scheme}
\A]

\subsection{Some Improvements}

Our Sisyphean NodeScore algorithm does better at identifying important
nodes according to their global popularity, rather than being fooled
by local properties of the graph. But what about the error condition
we mentioned above: that a node might not have any outgoing edges? In
this case, Sisyphus has reached the end of the Internet... what's left
to do but go back to the beginning?

\part {5}
%comment!
Write |sisyphus-end-jump-nodescore|, which is like |sisyphus-nodescore|
except that instead of erroring at the end of the Internet, it has
Sisyphus jump back to the first node ever visited.
%end!
%>
%>; (define (sisyphus-end-jump-nodescore n g)
%>;   YOUR-ANSWER-HERE)
%>

\A*[
\begin{scheme}

; Walk the internets (graph) for n steps, starting at a random node, and 
; going back to that first node whenever we reach a node with no edges
(define (sisyphus-end-jump-nodescore n graph )
  (let ([start-node (random-elt (graph-nodes graph))])
    (ns-normalize (sisyphus-end-jump-nodescore-inner n graph 
                                                     start-node 
                                                     start-node  ; as current
                                                     ns-new))))


(define (sisyphus-get-next-jump-start graph node start)
  (if (= 0 (length (graph-outgoing-neighbors node graph)))
      ; no edges--jump to the start node
      start
      ; otherwise same as before
      (edge-dest (pick-random-edge node graph))))

(define (sisyphus-end-jump-nodescore-inner n g start current ns)
  (if (< n 1) ns ; done
      ; otherwise pick a random neighbor to go next.
      (let ([next (sisyphus-get-next-jump-start g current start)])
        ; and go there, adding 1 to its nodescore
        (sisyphus-end-jump-nodescore-inner (- n 1) g start next
                                  (ns-add-score next 1 ns)))))

\end{scheme}
\A]


Not crashing on a realistic Web graph is certainly an improvement.
But what if we have a graph like this:

\begin{center}
\includegraphics[trim=50 525 0 0]{harvard-graph.pdf}
\end{center}
In this case, we may never get to the CS51 page on the right, since
it's only accessible via those low probability links, and it's very
improbable that Sisyphus will traverse the whole length without
jumping back to the main part of the graph on the left.  So we might
give those nodes zero score, when in fact they should have some small
but nonzero score.  To fix this, instead of jumping back to the
beginning upon reaching a node with no outgoing edges, let's jump to a
random node.

\part {2}
%comment!
Write |sisyphus-end-rand-nodescore|, which is like |sisyphus-nodescore|
except that instead of erroring at the end of the Internet, it has
Sisyphus jump back to a random node.
%end!
%>
%>; (define (sisyphus-end-rand-nodescore n g)
%>;   YOUR-ANSWER-HERE)
%>

\A*[
\begin{scheme}

; Walk the internets (graph) for n steps, starting at a random node, and 
; jumping to a random node whenever we reach a node with no edges
(define (sisyphus-end-rand-nodescore n graph )
  (ns-normalize (sisyphus-end-rand-nodescore-inner
                 n graph 
                 (random-elt (graph-nodes graph))
                 ns-new)))

(define (sisyphus-get-next-rand graph node)
  (if (= 0 (length (graph-outgoing-neighbors node graph)))
      ; no edges--jump to a random node
      (random-elt (graph-nodes graph))
      ; otherwise same as before
      (edge-dest (pick-random-edge node graph))))

(define (sisyphus-end-rand-nodescore-inner n g current ns)
  (if (< n 1) ns ; done
      ; otherwise pick a random neighbor to go next.
      (let ([next (sisyphus-get-next-rand g current)])
        ; and go there, adding 1 to its nodescore
        (sisyphus-end-rand-nodescore-inner (- n 1) g next
                                           (ns-add-score next 1 ns)))))
\end{scheme}
\A]

This should work better. But what if there are very few pages that
have no outgoing edges -- or worse, what if there is a set of vertices
with no outgoing edges to the rest of the graph, but there are still
edges between vertices in the set? Sisyphus would be stuck there
forever, and that set would win the node popularity game hands down,
just by having no outside links.  What we'd really like to model is
the fact that Sisyphus doesn't have an unlimited attention span, and
starts to get jumpy every now and then...

\part {3}
%comment!
Write |sisyphus-jumpy-nodescore|, which is like
|sisyphus-end-rand-nodescore| except that with some small probability
|alpha|, Sisyphus will jump randomly to any node in the graph
uniformly, regardless of whether the current node has outgoing
edges. (Note that if the current node has no outgoing edges, Sisyphus
should still jump to a random node in the graph, as before.)
%end!
%>
%>; (define (sisyphus-jumpy-nodescore n alpha g)
%>;   YOUR-ANSWER-HERE)
%>

\A*[
\begin{scheme}

; Walk the internets (graph) for k steps, starting at a random node.
; Randomly jump somewhere new with probability alpha, or if there is
; no outgoing edge
(define (sisyphus-jumpy-nodescore n alpha graph)
  (ns-normalize (sisyphus-jumpy-nodescore-inner n alpha graph 
                                        (random-elt (graph-nodes graph))
                                       ns-new)))

; With probability alpha, and always if node has no outgoing neighbors, 
; return a random node in the graph. 
; Otherwise, pick a random neighbor of the node according to the 
; weights on the edges.
(define (jumpy-get-next graph node alpha)
  (if (or (< (random) alpha)
          (= 0 (length (graph-outgoing-neighbors node graph))))
      (random-elt (graph-nodes graph))
      (edge-dest (pick-random-edge node graph))))
  
(define (sisyphus-jumpy-nodescore-inner n alpha g node ns)
  (if (< n 1) ns ; done
      ; otherwise pick a random place to go next.
      (let ([next (jumpy-get-next g node alpha)])
        ; and go there, adding 1 to its nodescore
        (sisyphus-jumpy-nodescore-inner (- n 1) alpha g next
                                        (ns-add-score next 1 ns)))))

\end{scheme}
\A]

\end{exercise}

\section{SuperNodeScore}

Our algorithm so far works pretty well, but on a huge graph it would
take a long time to explore all of the small-probability
nodes.\footnote{Especially when new nodes are being added all the
  time...}

So we'll need to adjust our algorithm somewhat.  In particular, let's
suppose Sisyphus is bitten by a radioactive
eigenvalue,\footnote{Represented by $\lambda$. Coincidence?}  giving
him the power to subdivide himself arbitrarily and send parts of
himself off to multiple different nodes at once.  We have him start
evenly spread out among all the nodes. Then, from each of these nodes,
the pieces of Sisyphus that start there will propagate outwards along
the graph, dividing themselves among all outgoing edges of that node
according to the weight of each edge.

So, let's say that at the start of a step, we have some fraction q of
Sisyphus at a node, and that node has 3 outgoing edges, with weights
0.2, 0.3, and 0.5.  Then $0.2q$ of Sisyphus will propagate outwards
from that node along the edge of weight 0.2, $0.3q$ along the edge of
weight 0.3, and $q/2$ along the edge of weight 0.5.  This will mean
that nodes with a lot of value will make their neighbors significantly
more important at each timestep, and also that in order to be
important, a node must have a large number of incoming edges
continually feeding it importance.

Thus, our basic algorithm takes an existing graph and NodeScore, and
updates the NodeScore by propagating all of the value at each node to
all of its neighbors. However, there's one wrinkle: we want to
include some mechanism to simulate random jumping. The way that we do
this is to pass a parameter alpha, similarly to what we did at the end
of exercise 1. At each step, each node propagates a fraction (1-alpha)
of its value to its neighbors as described above, but also a fraction
alpha of its value to all nodes in the graph. This will ensure that
every node in the graph is always getting some small amount of value,
so that we never completely abandon nodes that are hard to reach.

We can model this fairly simply. If each node distributes alpha times
its value to all nodes at each timestep, then at each timestep each
node accrues (alpha/n) times the overall value in this manner. Thus,
we can model this effect by having the base NodeScore at each timestep
give (alpha/n) to every node.

For all complexity calculations on the remainder of this assignment,
you may make the (unrealistic) assumption that |graph-outgoing-edges|
takes constant time, or O(1).

\begin{exercise} ?

\part {2}
%comment!
Write a function |fixed-nodescore| that takes a graph and a value,
and returns a NodeScore where the value of each node in the graph
is the value given.
%end!

%>;
%>; (define (fixed-nodescore graph val)
%>;    YOUR-ANSWER-HERE)
%>
%>

\A[
  \begin{scheme}
; Set the score of every node in g to x--used to set the 
; initial addition of alpha/|V|
(define (fixed-nodescore g x)
  (foldr (lambda (n r) (ns-set-score n x r))
         ns-new (graph-nodes g)))
  \end{scheme}
\A]

Now we have our base case for each timestep of our quantized Sisyphus
algorithm. What else do we need to do at each timestep? Well, as
explained above, we need to distribute the value at each node to all
of its neighbors, based on the edge weights.

\part {6}
%comment!
Write a function |propagate-weight| that takes a node, a graph, a
value of alpha, a NodeScore that it's building up, and the NodeScore
from the previous timestep, and returns the new NodeScore resulting
from distributing the weight that the given node had in the
old NodeScore to all of its neighbors. You might find code from your
babbler useful in doing the appropriate weighting. Remember that only
(1-alpha) of
the NodeScore gets distributed among the neighbors (the other alpha
was distributed evenly to all nodes in the base case). Your function
should take time linear in the number of edges outgoing from the given
node in the given graph (making the assumption stated above, that
|graph-outgoing-edges| is constant-time).
%end!
%>;
%>; (define (propagate-weight node graph alpha new_ns old_ns)
%>;    YOUR-ANSWER-HERE)
%>
%>

\A[
  \begin{scheme}
; Given an initial nodescore, propagates (1-alpha) of the nodescore of node
; in old-pr to its outgoing neighbors, divided according to the ratios
; of the weights on the edges.  Assumes that the edge weights
; are always a valid probability distribution (>= 0, sum to 1)
(define (quantum-propagate-weight node graph alpha new-ns old-ns)
  (foldl (lambda (edge ns) 
           (let ([adding-w (* (- 1 alpha)
                              (ns-get-score node old-ns)
                              (edge-weight edge))])
             ; (begin (printf "Adding ~a from ~a to ~a\n" adding-w 
             ;      node (edge-dest edge))
             (ns-add-score (edge-dest edge) 
                           adding-w
                           ns)))
         new-ns
         (graph-outgoing-edges node graph)))
  \end{scheme}
\A]

\part {5}
%comment!
Now we have all the components we need. Write a function
|quantum-nodescore| that takes a graph, a number of timesteps to
simulate, and a value of alpha to use as the distributing parameter.
It should return the NodeScore resulting from applying the updating
procedures described above. Your function must run in O(k*e), where
k is the number of timesteps to run and e is the number of edges in
the input graph. (Hint: As explained above, your |propagate-weight|
function should be linear in the number of edges leaving the input
node. Thus, your |quantum-nodescore| function should probably do a
fold over what list, in order to make the overall complexity per
timestep linear in the total number of edges?)
%end!

%>;
%>; (define (quantum-nodescore graph k alpha)
%>;    YOUR-ANSWER-HERE)
%>
%>

\A[
; Simulate the divisible walker algorithm, starting with a uniform 
; distribution,and going through k iterations.  At each iteration,
; have a fraction alpha of the probability at the node split evenly among 
; all the nodes in the graph (including the current one).
(define (quantum-nodescore graph k alpha)
  (if (or (< alpha 0) (> alpha 1)) 
      (error 'quantum-nodescore "Alpha must be between 0 and 1")
      (quantum-nodescore-inner 
       graph k alpha 
       (uniform-nodescore graph))))

; Each round starts with a uniform distribution and then goes through 
; all nodes and adds the appropriate weight for each outgoing edge
(define (quantum-nodescore-inner g k alpha ns)
  (if (< k 1) ns ; done
      ; for each node, deal with its edges
      (quantum-nodescore-inner     
       g (- k 1) alpha 
       (foldl (lambda (node new-ns) 
                (quantum-propagate-weight node g alpha new-ns ns))
              ; At every round, will add alpha/n all nodes, so start with that
              (fixed-nodescore g (/ alpha (length (graph-nodes g))))
              (graph-nodes g)))))
\A]

\part {3}
%comment!
Prove that your |quantum-nodescore| function runs in O(k*e). You may
need to prove the Big-O runtime of your |propagate-weight| function
in order to do this.
%end!

%>;
%>; YOUR-PROOF-HERE (in a comment is fine)
%>;
%>

\end{exercise}

%>; ########################## Some functions for testing #################
%>; For testing: returns a list of differences between the nodes in two
%>; nodescores Errors if they have different nodes
%>(define (delta-nodescores a b)
%>  (map (lambda (x) (abs (- (ns-get-score x a) (ns-get-score x b))))
%>       (ns-nodes a)))
%>
%>(define (max-delta-ns a b)
%>  (apply max (delta-nodescores a b)))
%>
%>(define (average lst)
%>  (/ (foldr + 0 lst) (length lst)))
%>
%>(define (average-delta-ns a b)
%>  (average (delta-nodescores a b)))
%>
%>; ####################################################################

%> (define minutes-spent -1)

\end{document}
