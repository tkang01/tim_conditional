#!/bin/bash -e

if [ x$1 = x ] 
then
    echo "Need to specify assignment number"
    exit
fi

if test -f /usr/local/bin/submit; then
	rm -rf moogle-submit
	mkdir moogle-submit
	cp -p *.ml moogle-submit
	cp -p *.html moogle-submit
	/usr/local/bin/submit lib51 $1 `pwd`/moogle-submit
	echo "Done!"
else
    echo "You must run this on one of the nice.fas machines"
fi
