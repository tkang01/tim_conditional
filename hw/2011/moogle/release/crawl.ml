open Util ;;    
open CrawlerServices ;;
open Order ;;
open Pagerank ;;


(* Phase II: Change these as you implement smarter ranking algorithms *)
module MoogleRanker
  = DummyRanker (PageGraph) (PageScore) 
  (* = InDegreeRanker (PageGraph) (PageScore) *)

  (* = RandomWalkRanker (PageGraph) (PageScore) (struct 
       let do_random_jumps = None
       let num_steps = 1000
     end) *)

  (* = QuantumRanker (PageGraph) (PageScore) (struct 
       let alpha = 0.01
       let num_steps = 1
       let debug = true
     end)
  *)

(* Dictionaries mapping words (strings) to sets of crawler links *)
module WordDict = Dict.Make(
  struct 
    type key = string
    type value = LinkSet.set
    let compare = string_compare
    let string_of_key = (fun s -> s)
    let string_of_value = LinkSet.string_of_set
  end)

(* A query module that uses LinkSet and WordDict *)
module Q = Query.Query(
  struct
    module S = LinkSet
    module D = WordDict
  end)

let print s = 
  let _ = Printf.printf "%s\n" s in
  flush_all();;


(* TODO: Fill this in (part 1).  You may find it useful to write in pseudocode
 * for what you're trying to do before writing the actual code
 *)
let rec crawl (n:int) (frontier: LinkSet.set)
    (visited : LinkSet.set) (d:WordDict.dict) : WordDict.dict = 
  (* TODO, phase 1: replace this stub with real code :)  *)
  WordDict.empty
;;

let crawler () = 
  crawl num_pages_to_search (LinkSet.singleton initial_link) LinkSet.empty
    WordDict.empty
;;

(* Debugging note: if you set debug=true in moogle.ml, it will print out your
 * index after crawling. *)
