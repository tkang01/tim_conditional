(* Interfaces and implementations of dictionaries.  A dictionary
 * is used to associate a value with a key.  In our case, we will
 * be using a dictionary to build an index for the web, associating
 * a set of URLs with each word that we find as we crawl the web.
 *)
module type DICT = 
sig
  type key   
  type value 
  type dict
  val empty : dict 
  val insert : dict -> key -> value -> dict
  val lookup : dict -> key -> value option
  val remove : dict -> key -> dict
  val member : dict -> key -> bool

  (* Return an arbitrary key, value pair along with a new dict with that
   * pair removed.  Return None if the input dict is empty *)
  val choose : dict -> (key * value * dict) option
  val fold : (key -> value -> 'a -> 'a) -> 'a -> dict -> 'a
  val string_of_key: key -> string
  val string_of_value : value -> string
  val string_of_dict : dict -> string
end

(* Arguments to the AssocListDict functor *)
module type DICT_ARG = 
sig
  type key
  type value
  val compare : key -> key -> Order.order
  val string_of_key: key -> string
  val string_of_value : value -> string
end

(* An association list implementation of dictionaries.  *)
module AssocListDict(D:DICT_ARG) : (DICT with type key = D.key
with type value = D.value) = 
struct
  open Order
  type key = D.key
  type value = D.value
  (* invariant: sorted by key, no duplicates *)
  type dict = (key * value) list;;
  let empty = [] ;;
  let rec insert d k v = 
    match d with 
      | [] -> [(k,v)]
      | (k1,v1)::d1 -> (match D.compare k k1 with 
                          | Less -> (k,v)::d
                          | Eq -> (k,v)::d1
                          | Greater -> (k1,v1)::(insert d1 k v))

  let rec lookup d k = 
    match d with 
      | [] -> None
      | (k1,v1)::d1 -> (match D.compare k k1 with
                          | Eq -> Some v1
                          | Greater -> lookup d1 k 
                          | _ -> None)

  let member d k = match lookup d k with | None -> false | Some _ -> true
    
  let rec remove d k = 
    match d with 
      | [] -> []
      | (k1,v1)::d1 ->
	      (match D.compare k k1 with 
             | Eq -> d1
             | Greater -> (k1,v1)::(remove d1 k)
             | _ -> d)
	        
  let choose d = 
    match d with 
      | [] -> None
      | (k,v)::rest -> Some(k,v,rest)
          
  let fold f d = List.fold_left (fun a (k,v) -> f k v a) d 

  let string_of_key = D.string_of_key
  let string_of_value = D.string_of_value
  let string_of_dict (d: dict) : string = 
    let f = (fun y (k,v) -> y ^ "\n key: " ^ D.string_of_key k ^ 
               "; value: (" ^ D.string_of_value v ^ ")") in
      List.fold_left f "" d

end    

(* Testing example (not comrehensive!) *)
module TestAssocListDicts = 
struct
  module D = AssocListDict(
    struct 
      type key = int
      type value = int
      let compare = Order.int_compare
      let string_of_key =  string_of_int
      let string_of_value = string_of_int
    end)

  let d = List.fold_right (fun (a,b) d -> D.insert d a b) 
    [(1,2); (2,7)] D.empty;;

  assert (D.lookup d 1 = Some 2);;
  assert (D.lookup d 2 = Some 7);;
  assert (D.lookup d 3 = None);;

  let d2 = D.remove d 2;;
  assert (D.lookup d2 2 = None);;

  let d3 = D.remove d 3;;
  assert (D.lookup d3 2 = Some 7);;
end 

(************************* PART 2 *********************************)

(******************************************************************)
(* BTDict: a functor that implements our DICT signature           *)
(* using a binary search tree.                                    *)
(******************************************************************)

(*
module BTDict(D:DICT_ARG) : (DICT with type key = D.key
with type value = D.value) =
struct
        (* 
           TODO Part 1 -- uncomment, then fill this in
        *)
end
*)

(******************************************************************)
(* MapDict: a functor that implements our DICT signature          *)
(* using the Map module built into ocaml.                         *)
(* http://caml.inria.fr/pub/docs/manual-ocaml/libref/Map.html     *)
(******************************************************************)

(*
module MapDict(D:DICT_ARG) : (DICT with type key = D.key
with type value = D.value) = 
struct
  (* Note: the Map module in ocaml expects a comparison function
   * that returns -1, 0, and 1 for less than, equal, or greater than.
   *)

  (* TODO: Fill this in (Part 1) *)


  (* The ocaml Map module didn't have a choose method in versions 
   * before 3.12.0.  To make this work on nice.fas and be O(1), need
   * an ugly hack, so we're giving you this function:
   *)
  exception PairExc of (D.key * D.value)

  let choose d = 
    try 
      M.fold (fun k v _ -> raise (PairExc (k,v))) d ();
      None
    with
        (* Note that remove must be above this *)
      | PairExc(k,v) -> Some (k,v, remove d k)
          
  (***** End hack *****)

  
end
*)
  

module Make (D:DICT_ARG) : 
  (DICT with type key = D.key
with type value = D.value) = 
  (* Change this line to change the map implementation *)
  AssocListDict(D)
    (* BTDict (D) *)
    (* MapDict (D) *)



