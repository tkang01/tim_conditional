(* CS51 Spring 2010
 * PS 0 *)

(* Please define these variables with appropriate values *)

let name : (string * string) = ("FIRST", "LAST");;

type year = Freshman | Sophomore | Junior | Senior | Other of string;;

let class_year : year = Other "I haven't filled it in yet";;

let exciting : string = "I'm excited about ....!";;

(* **** You shouldn't change anything below this line, but you should
   read to the bottom of the file.
  **** *)

let print = Printf.printf;;

let print_survey = 
  let (first, last) = name in
  let string_year = 
    (match class_year with
       | Freshman -> "2014"
       | Sophomore -> "2013"
       | Junior -> "2012"
       | Senior -> "2011"
       | Other s -> "Other: " ^ s
    ) in
    (print "----------------------------------------\n";
     print "Name: %s %s\n\n" first last;
     print "Year: %s\n\n" string_year; 
     print "%s\n\n" exciting;
     print "----------------------------------------\n\n";);;


print_survey;;

(* type "make" to compile the file.
  type ./survey to run the program and print the output.  
  Make sure all the values look right.  If they do, submit and 
  you're done! *)
