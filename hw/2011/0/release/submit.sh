#!/bin/bash

if test -f /usr/local/bin/submit; then
	rm -rf ps0-submit
	mkdir ps0-submit
	cp -p ps0.ml ps0-submit
	/usr/local/bin/submit lib51 0 `pwd`/ps0-submit
	echo Done!
else
    echo "You must run this on one of the nice.fas machines"
fi
