/*
 * Problem set 6.
 * CS51, Spring 2010
 * 
 */

/*
 * Part 2: Expressions
 */

// Exception to handle unbound variables in your Expressions.
class UnboundVar extends Exception {
    final String badVar;
    UnboundVar(String var) { badVar = var; }
}

// Define the nodes of your linked-list environment here.
class VarValNode {
    // Any private variables go here
    VarValNode() {}; // Alter as necessary
}

// Define your linked list environment here. See the problem set spec.
class VarValList {
    // Any private variables go here
    VarValList() {}; // Alter as necessary
    /* Should define var to have the value v in this environment. */
    void addVar(String var, double v) {
        // Body goes here.
    }
    /* Should return the value of var in this environment, throwing an
     * UnboundVar exception if var is unbound. */
    double lookup(String var) throws UnboundVar {
        return -1;
        // Body goes here.
    }
}

// Expression class definition.
interface Expression {
    abstract boolean hasVar(String v);
    abstract String toString();
    abstract double eval(VarValList env) throws UnboundVar;
    abstract Expression derivative(String v);
}

/* Define your subclasses of Expression here. We've done Number for you. */

class Number implements Expression {
    final double num;
    Number(double n) { num = n; }
    public boolean hasVar(String v) { return false; }
    public String toString() { return "(" + Double.toString(num) + ")"; }
    public double eval(VarValList env) throws UnboundVar { return num; }
    public Expression derivative(String v) { return new Number(0); }
}

// OTHER CLASS DEFINITIONS GO HERE
