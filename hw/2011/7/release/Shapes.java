/*
 * Problem set 6.
 * CS51, Spring 2010
 */

/*
 * Part 1: Shapes
 */

// To be used internally in your class definitions
class Point {
    double x;
    double y;
    Point(double a, double b) { x = a; y = b; }
}

interface Shape {
    abstract double perimeter();
    abstract double area();
    abstract void scale(double scaleFactor);
    abstract void translate(double xTranslation, double yTranslation);
    abstract Shape boundingBox();
    //abstract void rotate(double radians);
}

/* YOUR CLASS DEFINITIONS GO HERE */
// Dummy definition as an illustration of syntax.
class Square implements Shape {
    Square() {};
    public double perimeter() { return 1; }
    public double area() { return 1; }
    public void scale(double s) {};
    public void translate (double xt, double yt) {};
    public Shape boundingBox() { return new Square(); }
}

// Put your other two classes here.