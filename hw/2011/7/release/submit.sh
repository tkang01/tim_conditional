#!/bin/bash -e

if test -f /usr/local/bin/submit; then
	rm -rf ps7-submit
	mkdir ps7-submit
	cp -p *.java writeup.txt ps7-submit
	/usr/local/bin/submit lib51 7 `pwd`/ps7-submit
	echo "Done!"
else
    echo "You must run this on one of the nice.fas machines"
fi
