\documentclass[12pt]{article}

\usepackage{alltt}
\usepackage{hyperref}

 \input{decls-common}

%% Page layout
\oddsidemargin 0pt
\evensidemargin 0pt
 \textheight 600pt 
\textwidth 469pt 
\setlength{\parindent}{0em} 
\setlength{\parskip}{1ex}

\begin{document}

\title{Problem Set 7: Time to get ``Class''y}

\author{}

\date{Due Friday, April 8$^{th}$, 2011, 5:00 pm}

\maketitle

\section{Introduction}

Welcome to Java, an imperative, object-oriented programming language that represents a major shift away from OCaml. In this problem set, you'll be warming up with the basics of the language and its syntax.

To begin:
\begin{enumerate}
\item (Unless you already have a Java development environment set up),
  follow the staff's installation instructions for getting
  started with Eclipse/Java:
  \\ \htmladdnormallink{http://www.seas.harvard.edu/courses/cs51/eclipse-java.html}
	{http://www.seas.harvard.edu/courses/cs51/eclipse-java.html}.
\item Download the assignment tarball from the assignments page
and extract it.
\end{enumerate}

\subsection{Classes}

The object-oriented part of Java results in pretty much everything being a class. When programming in Java, you will want to define the datatypes you'll be working with as classes; you'll define functions that create and operate over values of that type within those classes, and refer to these functions as methods; and you will grow used to encapsulating the state of your program within this kind of abstraction.

This problem set is in two parts. The first part is a set of basic exercises intended to get you comfortable with inheritance and its syntax. The second part reprises the second part of our OCaml PS2, in which we defined a set of datatypes and functions to describe mathematical expressions in terms of a single variable, evaluate those expressions, and take their symbolic derivatives. Here we will be accomplishing this task using - of course - classes and inheritance, with a couple of added wrinkles to ensure some conceptual challenge.

Note: the keyword \verb=final= indicates that the value of the variable in question may be set only in the constructor of a class, and cannot be modified thereafter.

\section{Part I: Shapes}

The code for this part is in {\tt Shapes.java}.

Your first task is to implement classes for Square, Circle, and EqTriangle.

These classes all implement the interface Shape. As such, they must implement the abstract methods that the Shape interface declares:

\begin{verbatim}
interface Shape {
    abstract double perimeter();
    abstract double area();
    abstract void scale(double scaleFactor);
    abstract void translate(double xTranslation, double yTranslation);
    abstract Shape boundingBox();
    //abstract void rotate(double radians);
}
\end{verbatim}

You may define whatever private data you need in your classes to be able to give accurate return values for each of the above methods on the three types of shapes in question. (The \verb=rotate= method is commented out because it is a karma problem.) In order to aid you in this problem, we have also defined a class representing a Point in two-space (you may assume that your shapes live within the Cartesian plane):

\begin{verbatim}
class Point {
    double x;
    double y;
    Point(double a, double b) { x = a; y = b; }
}
\end{verbatim}

You should use the Point class in your definitions of your three shapes.

Each shape should have a constructor that takes a Point and a double. For the Square and the EqTriangle, the Point should be the bottom-left corner and the double should be the side length (you may assume that your shapes are initially oriented with the bottom side along the x-axis); for the Circle, the point will be the center and the double will be the radius. You may also create other constructors if you find it necessary.

\begin{enumerate} % Exercise numbers

\item Implement the Square, Circle, and EqTriangle classes, ensuring that they implement the Shape interface.
\begin{enumerate}
\item The \verb=perimeter= method should return the perimeter of the shape.
\item The \verb=area= method should return the area of the shape.
\item The \verb=scale= method should destructively update the Shape so that it is scaled by the input scaleFactor. For a Circle, the center should be fixed and the circle should expand around it. For a Square and an EqTriangle, the lower left corner should remain in place, and the length of each side should be expanded by the scale factor.
\item The \verb=translate= method should destructively update the Shape so that it is translated in the x direction by \verb=xTranslation= and in the y direction by \verb=yTranslation=.
\item The \verb=boundingBox= method should return a Shape - specifically, a Square - that represents the minimal Square that encloses the given shape completely, oriented with its bottom edge along the x-axis.
\item KARMA: The \verb=rotate= method should update the Shape to
  rotate it clockwise by \verb=radians= radians. Note that since this is a karma problem, you do not need to ensure that your Shapes have the data necessary to support the implementation of this method if you choose to skip it.
\end{enumerate}

\subsection{Testing}

We want you to write tests into a separate Tests class. You should be able to create a new Tests object in the \verb=main= method and then call various methods on it representing tests of the various functions you've written. You should stop now and do this for the Shapes code you just wrote, then go back after completing part 2 and do the same for the Expressions exercises.

The thoroughness of your tests will count towards your final grade. We are not going to specify a certain number of tests that we wish you to write per function or per class; rather, you should write enough tests to convince yourself - and, by extension, us - that your code works as intended. You can lose points here even if all your code is correct if we have no reason to believe that you knew it was correct.

\section{Expression Language}

The code for this part is in {\tt Expressions.java}.

In this section, you will retrace many of your steps from the second part of PS2. You can refresh your memory of the original assignment at:

\verb=http://www.seas.harvard.edu/courses/cs51/hw/ps2.html=.

As before, we are attempting to define a language for mathematical expressions. The abstract syntax for the language is specified by the OCaml code given in the original assignment, reproduced here for your convenience:

\begin{verbatim}
(* Binary operators. *)
type binop = Add | Sub | Mul | Div | Pow ;;

(* Unary operators. *)
type unop = Sin | Cos | Ln | Neg ;;

type expression =
  | Number of float
  | Var
  | Binop of binop * expression * expression
  | Unop of unop * expression
\end{verbatim}

\subsection{Environments}

In the original version of this assignment, we allowed only one variable, ``x". Here, however, our expressions might contain many variables. Thus, we will be defining a kind of environment that will allow us to assign values to variables in our evaluation.

We will implement these environments as linked lists of nodes
containing a String (the variable) and a double (its value). The class
VarValNode will represent a node; the class VarValList will represent
the environment as a whole. Post on the Bulletin Board or ask a TF (or
Google) if you are unfamiliar with how linked lists work.

\item Begin by filling in the definitions of the methods in the VarValNode
  and VarValList classes.

\subsection{Expression classes}

Once you have accomplished the task of defining environments, you will
need to define classes for the possible types of Expressions. As you
can see from the syntax, you should have four classes implementing the
Expression interface: Number, Var, Binop, and Unop. Binop and Unop
should themselves be abstract classes (or interfaces); Binop should
have five subclasses that inherit from it or implement it (Add, Sub,
Mul, Div, and Pow), and Unop four (Sin, Cos, Ln, and Neg).

All non-abstract classes that ultimately implement Expression must implement the four abstract methods we have declared in the Expression interface. Thus, your assignment for this part is as follows:

\item Implement the classes necessary to be able to specify any expression allowable in the given abstract syntax. Each non-abstract subclass you define should support the following four methods:
\begin{enumerate}
\item The \verb=hasVar= method should return true if the expression contains the variable \verb=v=, false otherwise.
\item The \verb=toString= method should return a String that would
  be a valid representation of the Expression if we had a parser for
  the above language. It can have lots of parentheses.
\item The \verb=eval= method should evaluate the Expression to a double according to \verb=env=, the given mapping of variables to values. If it encounters an unbound variable, it should throw an UnboundVar exception as appropriate.
\item The \verb=derivative= method should return a new Expression
  representing the symbolic derivative of the Expression it is called
  on with respect to the variable \verb=v=. See the original problem
  set writeup for an explanation of derivatives if you need a
  refresher. The one difference here from the original assignment is
  that our Expressions might now contain multiple variables; when
  taking a derivative with respect to one variable, all other
  variables in the expression should be treated as constants. If you
  are asked to take the derivative of an Expression with respect to a
  variable not present in the Expression, the result should therefore
  be 0. Post on the Bulletin Board or ask a TF if you have questions
  about derivatives.
\end{enumerate}

Once more, you should fill in tests in the Tests class, and then invoke them from the \verb=main= method as appropriate; once more, the thoroughness of your tests will count towards your grade.

\section{Reflection}

You have now implemented Expressions in both ML and Java. Which was
better? While this question is obviously too vague to answer without
many qualifications, consider the following two more specific
questions. Put your answers in {\tt writeup.txt}.

\item Let's say that we wanted to add a new type of expression, Tan (for tangent). What code would you have to change in the ML version? What code would you have to change in the Java version? Which is easier, and why? (3-6 sentences)

\item Let's say that we wanted to add a new operation on expressions, integral(). What code would you have to change in the ML version? What code would you have to change in the Java version? Which is easier, and why? (3-6 sentences)

\end{enumerate} % For exercise numbers

\section{Submission}

To submit, do the following:

\begin{enumerate}
  \item Copy your files to {\tt nice.fas.harvard.edu}.  Make sure
    you get all the {\tt java} files and {\tt writeup.txt}.
  \item Run {\tt make}.  If it complains that your files don't
  compile, fix them.  This would also be a good time to double check
  that all your tests pass ({\tt java Main} will run your tests.)
  \item Run {\tt make check}.  If it complains about lines longer than 80
  characters, fix them.
  \item Run {\tt make submit}.  If it complains, post to the BB,
  pasting in the output.
  \item {\bf If any of the above scripts fails to run, perhaps with
      perplexing error messages such as "Permission deniedn" [sic],
      try running {\tt ./fixwin} from the assignment directory
      (you may need to {\tt chmod u+x ./fixwin} first).
      (These problems are likely caused by Windows/DOS
      CR/LF line endings.)}
\end{enumerate}


\end{document}
