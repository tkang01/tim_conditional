/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Part 1: Shapes
 */

// To be used internally in your class definitions
class Point {
    double x;
    double y;
    Point(double a, double b) { x = a; y = b; }
}

interface Shape {
    abstract double perimeter();
    abstract double area();
    abstract void scale(double scaleFactor);
    abstract void translate(double xTranslation, double yTranslation);
    abstract Shape boundingBox();
    //abstract void rotate(double radians);
}

/* YOUR CLASS DEFINITIONS GO HERE */
// Dummy definition as an illustration of syntax.
class Square implements Shape {
    private Point ll;
    private double edgeLen;

    Square(double x, double y, double eLen) {
        ll = new Point(x,y);
        edgeLen = eLen;
    }
    public double perimeter() { return 4 * edgeLen; }
    public double area() { return edgeLen * edgeLen; }
    public void scale(double s) { edgeLen *= s; }
    public void translate (double xt, double yt) { ll.x += xt; ll.y += yt; }
    public Shape boundingBox() { return new Square(ll.x, ll.y, edgeLen); }
}

class Circle implements Shape {
    private Point c;
    private double rad;

    Circle(double x, double y, double r) {
        c = new Point(x,y);
        rad = r;
    }
    public double perimeter() { return 2 * Math.PI * rad; }
    public double area() { return Math.PI * rad * rad; }
    public void scale(double s) { rad *= s; }
    public void translate(double xt, double yt) { c.x += xt; c.y += yt; }
    public Shape boundingBox() { return new Square(c.x-rad, c.y-rad, 2*rad); }
}

class EqTriangle implements Shape {
    private Point ll;
    private double edgeLen;

    EqTriangle(double x, double y, double eLen) {
        ll = new Point(x,y);
        edgeLen = eLen;
    }
    public double perimeter() { return 3 * edgeLen; }
    public double area() { return edgeLen * edgeLen * Math.sqrt(3) / 4; }
    public void scale(double s) { edgeLen *= s; }
    public void translate(double xt, double yt) { ll.x += xt; ll.y += yt; }
    public Shape boundingBox() { return new Square(ll.x, ll.y, edgeLen); }
}