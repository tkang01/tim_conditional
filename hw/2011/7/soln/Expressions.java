/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/*
 * Part 2: Expressions
 */

// Exception to handle unbound variables in your Expressions.
class UnboundVar extends Exception {
    final String badVar;
    UnboundVar(String var) { badVar = var; }
}

// Define the nodes of your linked-list environment here.
class VarValNode {
    String name;
    double val;
    VarValNode next;

    VarValNode(String n, double v, VarValNode nxt) {
        name = n;
        val = v;
        next = nxt;
    }
}

// Define your linked list environment here. See the problem set spec.
class VarValList {
    private VarValNode root;

    VarValList() { root = null; }
    /* Should define var to have the value v in this environment. */
    void addVar(String var, double v) {
        VarValNode t = new VarValNode(var,v,root);
        root = t;
    }
    /* Should return the value of var in this environment, throwing an
     * UnboundVar exception if var is unbound. */
    double lookup(String var) throws UnboundVar {
        VarValNode curr = root;
        while(curr != null) {
            if(var.equals(curr.name)) {
                return curr.val;
            }
            curr = curr.next;
        }
        throw new UnboundVar(var);
    }
}

// Expression class definition.
interface Expression {
    abstract boolean hasVar(String v);
    abstract String toString();
    abstract double eval(VarValList env) throws UnboundVar;
    abstract Expression derivative(String v);
}

/* Define your subclasses of Expression here. We've done Number for you. */

class Number implements Expression {
    final double num;
    Number(double n) { num = n; }
    public boolean hasVar(String v) { return false; }
    public String toString() { return Double.toString(num); }
    public double eval(VarValList env) throws UnboundVar { return num; }
    public Expression derivative(String v) { return new Number(0); }
}

class Var implements Expression {
    final String name;
    Var(String n) { name = n; }
    public boolean hasVar(String v) { return v.equals(name); }
    public String toString() { return name; }
    public double eval(VarValList env) throws UnboundVar { return env.lookup(name); }
    public Expression derivative(String v) {
        if(name.equals(v)) {
            return new Number(1);
        }
        return new Number(0);
    }
}

abstract class Binop implements Expression {
    final Expression e1;
    final Expression e2;
    Binop(Expression e1n, Expression e2n) { e1 = e1n; e2 = e2n; }
    public boolean hasVar(String v) { return e1.hasVar(v) || e2.hasVar(v); }
    public String sHelp(String op) {
        return "(" + e1.toString() + op + e2.toString() + ")";
    }
    abstract public String toString();
    abstract public double eval(VarValList env) throws UnboundVar;
    abstract public Expression derivative(String v);
}

class Add extends Binop {
    Add(Expression e1n, Expression e2n) { super(e1n,e2n); }
    public String toString() { return sHelp(" + "); }
    public double eval(VarValList env) throws UnboundVar {
        return e1.eval(env) + e2.eval(env);
    }
    public Expression derivative(String v) {
        if(!this.hasVar(v)) return new Number(0);
        return new Add(e1.derivative(v),e2.derivative(v));
    }
}

class Sub extends Binop {
    Sub(Expression e1n, Expression e2n) { super(e1n,e2n); }
    public String toString() { return sHelp(" - "); }
    public double eval(VarValList env) throws UnboundVar {
        return e1.eval(env) - e2.eval(env);
    }
    public Expression derivative(String v) {
        if(!this.hasVar(v)) return new Number(0);
        return new Sub(e1.derivative(v),e2.derivative(v));
    }
}

class Mul extends Binop {
    Mul(Expression e1n, Expression e2n) { super(e1n,e2n); }
    public String toString() { return sHelp(" * "); }
    public double eval(VarValList env) throws UnboundVar {
        return e1.eval(env) * e2.eval(env);
    }
    public Expression derivative(String v) {
        if(!this.hasVar(v)) return new Number(0);
        return new Add(new Mul(e1.derivative(v),e2),
                       new Mul(e1,e2.derivative(v)));
    }
}

class Div extends Binop {
    Div(Expression e1n, Expression e2n) { super(e1n,e2n); }
    public String toString() { return sHelp(" / "); }
    public double eval(VarValList env) throws UnboundVar {
        return e1.eval(env) / e2.eval(env);
    }
    public Expression derivative(String v) {
        if(!this.hasVar(v)) return new Number(0);
        return new Div(new Sub(new Mul(e1.derivative(v),e2),
                       new Mul(e1,e2.derivative(v))),new Pow(e2,new Number(2)));
    }
}

class Pow extends Binop {
    Pow(Expression e1n, Expression e2n) { super(e1n,e2n); }
    public String toString() { return sHelp(" ^ "); }
    public double eval(VarValList env) throws UnboundVar {
        return Math.pow(e1.eval(env), e2.eval(env));
    }
    public Expression derivative(String v) {
        if(!this.hasVar(v)) return new Number(0);
        if(e2.hasVar(v)) {
            return new Mul(this,
                           new Add(new Mul(e2.derivative(v),new Ln(e1)),
                                   new Div(new Mul(e1.derivative(v),e2),e1)));
        }
        return new Mul(e2,new Mul(e1.derivative(v),
                                  new Pow(e1,new Sub(e2,new Number(1)))));
    }
}

abstract class Unop implements Expression {
    final Expression e;
    Unop(Expression en) { e = en; }
    public boolean hasVar(String v) { return e.hasVar(v); }
    public String sHelp(String op) { return op + "(" + e.toString() + ")"; }
    abstract public String toString();
    abstract public double eval(VarValList env) throws UnboundVar;
    abstract public Expression derivative(String v);
}

class Sin extends Unop {
    Sin(Expression e) { super(e); }
    public String toString() { return sHelp("Sin"); }
    public double eval(VarValList env) throws UnboundVar {
        return Math.sin(e.eval(env));
    }
    public Expression derivative(String v) {
        if(!this.hasVar(v)) return new Number(0);
        return new Mul(e.derivative(v),new Cos(e));
    }
}

class Cos extends Unop {
    Cos(Expression e) { super(e); }
    public String toString() { return sHelp("Cos"); }
    public double eval(VarValList env) throws UnboundVar {
        return Math.cos(e.eval(env));
    }
    public Expression derivative(String v) {
        if(!this.hasVar(v)) return new Number(0);
        return new Mul(e.derivative(v),new Neg(new Sin(e)));
    }
}

class Neg extends Unop {
    Neg(Expression e) { super(e); }
    public String toString() { return sHelp("-"); }
    public double eval(VarValList env) throws UnboundVar {
        return -(e.eval(env));
    }
    public Expression derivative(String v) {
        if(!this.hasVar(v)) return new Number(0); 
        return new Neg(e.derivative(v));
    }
}

class Ln extends Unop {
    Ln(Expression e) { super(e); }
    public String toString() { return sHelp("Ln"); }
    public double eval(VarValList env) throws UnboundVar {
        return Math.log(e.eval(env));
    }
    public Expression derivative(String v) {
        if(!this.hasVar(v)) return new Number(0);
        return new Mul(e.derivative(v),e);
    }
}