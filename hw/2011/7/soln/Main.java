
/**
 *
 * @author gidsthemathpirate
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // YOUR TESTS SHOULD GO HERE.
        Tests t = new Tests();
        t.envTest();
        t.printTest();
        t.dTest();
        t.evalTest();
    }
}
