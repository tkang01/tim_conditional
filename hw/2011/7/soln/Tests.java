/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gidsthemathpirate
 */
public class Tests {
    /* PLACE YOUR TESTS OF SHAPES AND EXPRESSIONS HERE, THEN INVOKE THEM
     * IN MAIN. */

    private int numTests;
    private VarValList env;
    private Expression x = new Var("x");
    private Expression y = new Var("y");
    private Expression n = new Number(42);
    private Expression three = new Number(3);
    private Expression[] es =
        { n,
          new Add(x,new Mul(y,n)),
          new Add(new Ln(x), new Mul(new Pow(x,three), y) ),
          new Div(new Pow(x,three),new Sin(y)) };

    Tests() {
        numTests = 1;
        env = new VarValList();
        env.addVar("x",2);
        env.addVar("y",3);
        env.addVar("z",4);
    }

    public void start() {
        System.out.println("Test " + numTests + ":");
        numTests++;
    }
    public void envTest() {
        start();
        try {
            env.lookup("y");
            System.out.println(" > Success");
        }
        catch(UnboundVar uv) {
            System.out.println(
                " > Failed envTest: Could not find variable " + uv.badVar);
        }
    }
    public void printTest() {
        start();
        for(Expression e : es) {
            System.out.println(" > " + e.toString());
        }
    }
    public void dTest() {
        start();
        System.out.println(" > Derivative with respect to x");
        for(Expression e : es) {
            System.out.println("   d/dx (" + e.toString() + ")=");
            System.out.println("   " + e.derivative("x").toString() + "\n");
        }
        System.out.println(" > Derivative with respect to y");
        for(Expression e : es) {
            System.out.println("   ~ d/dy (" + e.toString() + ")=");
            System.out.println("   ~ " + e.derivative("y").toString() + "\n");
        }
    }
    public void evalTest() {
        start();
        for(Expression e : es) {
            try {
                System.out.print(" > " + e.toString() + " = ");
                System.out.println(Double.toString(e.eval(env)));
            }
            catch(UnboundVar uv) {
                System.out.println(
                    " > Failed evalTest: Could not find variable " + uv.badVar);
            }
        }
    }
}
