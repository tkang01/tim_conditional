(*** General comments:

     1) Do not change the signatures of the functions we give you. This
     causes problems for our grading scripts.

     2) Do not leave code commented out for the same reason.

     3) Write test cases using assert statements.

     4) Make sure your code compiles using 'ocamlc' or 'make' (if a
     Makefile is provided). If you have functions out of order, it
     will still compile in the toplevel if you compile them in the
     correct order in the toplevel, but it won't compile in general.

***)

(*** CS 51 Problem Set 1 ***)
(*** February 4, 2011 ***)
(*** Solution Set ***)

(*>* Problem 1a *>*)

let prob1a : (string * int) list = [("CS", 50); ("CS", 51)] ;;
(*** You need those parentheses! "string * int list" gets parsed as a
     pair of a string and an int list. ***)

(*>* Problem 1b *>*)

let prob1b : int = (fun (x, y) -> x * y) (3, 7) ;;
(*** The first part of this expression has type float * float ->
     float, but it's then being applied to a value of type float *
     float. This means that the ultimate result is just a float. ***)

(*>* Problem 1c *>*)

let prob1c : 'a option * float option = (None, Some 123.0) ;;
(*** No real pattern to mistakes ***)

(*>* Problem 1d *>*)

let prob1d : (float * (string * int) list) list =
  [(2.0, [("White", 12) ; ("Red",16)]) ; (1.0, [("White", 17)])]
;;
(*** Watch your parentheses and brackets! ***)

(*>* Problem 1e *>*)

let prob1e : (int * int -> int) * (float -> float -> float) * float  =
  ((fun (x,y) -> x + y), (+.), 3.7)
;;
(*** Again, be careful with parentheses. One common mistake was to
     have the first function take its arguments separately rather
     than as a tuple
***)    

(*>* Problem 1f *>*)

let prob1f =
  let rec foo bar =
    match bar with
    | (a, (b, c)) :: xs -> a * (b + c + (foo xs))
    | _ -> 1
  in foo ([(2, (3, 5))])
;;
(*** No real pattern to mistakes ***)

(*>* Problem 1g *>*)

let prob1g =
  let v = (32.0, 28.0) in
  let square x = x *. x in
  let squared_norm (w: float * float) : float = let (a,b) = w in square a +. square b in
  let d = sqrt (squared_norm v) in
  int_of_float d
;;
(*** No real pattern to mistakes ***)

(*>* Problem 2 *>*)

let rec partition' (a: int list) (b: int) : (int list * int list) = 
  match a with
    | [] -> ([],[])
    |hd :: tl -> let (lt, gt) = partition' tl b in
	 if(hd < b) then (hd::lt, gt) else (lt, hd::gt)
;;
(*** Many people did not factor out as much code as they could have, as below ***)

let rec partition' (a: int list) (b: int) : (int list * int list) = 
  match a with
    | [] -> ([],[])
    |hd :: tl -> if (hd < b) then
	let (lt, gt) = partition' tl b in (hd::lt, gt)
      else
	let (lt, gt) = partition' tl b in (lt, hd::gt)
;;

(*** It was important to name the function partition' as opposed to
     partition. If you didn't do this, it caused problems for our grading
     scripts. In general, it's important that you don't change the
     signature of the functions we give you ***)

assert ((partition' [1;2;3;4;5] 3) = ([1;2],[3;4;5])) ;;
assert ((partition' [1;2;3;4;5] 1) = ([],[1;2;3;4;5])) ;;
assert ((partition' [1;2;3;4;5] 6) = ([1;2;3;4;5],[])) ;;
assert ((partition' [1;2;3;4;5] (-1)) = ([],[1;2;3;4;5])) ;;
assert ((partition' [] 3) = ([],[])) ;;
assert ((partition' [5;4;3;2;1] 3) = ([2;1],[5;4;3])) ;;
assert ((partition' [5;4;3;2;1] 1) = ([],[5;4;3;2;1])) ;;
assert ((partition' [5;4;3;2;1] 6) = ([5;4;3;2;1],[])) ;;
assert ((partition' [5;4;3;2;1] (-1)) = ([],[5;4;3;2;1])) ;;
assert ((partition' [-1] 0) = ([-1],[])) ;;
assert ((partition' [1] 0) = ([],[1])) ;;
assert ((partition' [-100] (-100)) = ([],[-100])) ;;

(*>* Problem 3a *>*)

let rec reversed (a:int list) : bool=
  match a with
    | [] -> true
    | [x] -> true
    | hd1::hd2::tl-> if (hd1 < hd2) then false
      else reversed (hd2::tl)
;;
(*** Many people called the function recursively on 'tl' instead of on
     'hd2::tl'. This fails on something like '[2;1;2;1]'. ***)

assert (reversed [] = true) ;;
assert (reversed [1] = true) ;;
assert (reversed [1; 2] = false) ;;
assert (reversed [2; 1] = true) ;;
assert (reversed [2; 2] = true) ;;
assert (reversed [2; 1; 0] = true) ;;
assert (reversed [10; 5; 5; 2; 3] = false) ;;
assert (reversed [10; 11; 4; 3; 2] = false) ;;
assert (reversed [4; 2; 0; -2; -4] = true) ;;

(*>* Problem 3b *>*)

let rec merge (a: int list) (b: int list):int list=
    match (a,b) with 
      | ([],_)-> b
      | (_,[])-> a
      | (hda::tla, hdb::tlb) ->
	if(hda < hdb) then hda :: merge tla b
	else hdb :: merge a tlb
;;
(*** Most common style mistake here was a redundant case: ***)
let rec merge' (a:int list) (b: int list) : int list =
  match a, b with
    | [], [] -> [] (* Unnecessary case - see the next two *)
    | [], _ -> b
    | _, [] -> a
    | (a1 :: a'), (b1 :: b') ->
	if a1 < b1 then (a1 :: merge a' b) else (b1 :: merge a b')
;;

(*** Several people also got the comparison operator wrong, using >
     instead of <. ***)

(*** Many people also thought that merge was the same as append, or
     cons, which it isn't (it combines the lists and sorts
     them). Thus, any solution of the following form, including ones
     that use @ instead of ::, is incorrect: ***)

let rec merge'' (a:int list) (b:int list) : int list =
  match a, b with
    | _, [] -> a
    | [], _ -> b
    | hd::tl, _ -> hd::merge tl b
;;

assert ((merge [1;2;3] [4;5;6;7]) = [1;2;3;4;5;6;7]) ;;
assert ((merge [4;5;6;7] [1;2;3]) = [1;2;3;4;5;6;7]) ;;
assert ((merge [4;5;6;7] [1;2;3]) = [1;2;3;4;5;6;7]) ;;
assert ((merge [2;2;2;2] [1;2;3]) = [1;2;2;2;2;2;3]) ;;
assert ((merge [1;2] [1;2]) = [1;1;2;2]) ;;
assert ((merge [-1;2;3;100] [-1;5;1001]) = [-1;-1;2;3;5;100;1001]) ;;
assert ((merge [] []) = []) ;;
assert ((merge [1] []) = [1]) ;;
assert ((merge [] [-1]) = [-1]) ;;
assert ((merge [1] [-1]) = [-1;1]) ;;

(*>* Problem 3c *>*)

let rec unzip (a: (int * int ) list ) : int list * int list =
  match a with 
    | [] -> ([],[])
    | (x,y) :: tl -> 
      let (a,b) = unzip tl in (x::a, y::b)
;;
(*** Many people used match statements with only one case. Always use
     a let statement if there is just one case. Consider the two equivalent
     things below. Use the let statement.

     match unzip tl with
       | (a,b) -> (x::a, y::b)

     let (a,b) = unzip tl in (x::a, y::b)

***)

assert ((unzip[] = ([],[])));;
assert ((unzip[(1,2)] = ([1],[2])));;
assert ((unzip[(1,2);(3,4);(5,6)]) = ([1;3;5],[2;4;6]));;

(*>* Problem 3d *>*)

let variance (xs : float list) : float option =
  let sum xs = List.fold_left (+.) 0. xs in
  let mean = (sum xs) /. float_of_int(List.length xs) in
  let devsq x = (x -. mean) *. (x -. mean) in
  match xs with
    | [] -> None
    | x::[] -> None
    | xs -> Some ((sum(List.map devsq xs)) /.
              (float_of_int((List.length xs)-1)))
;;

(*** There were many solutions here, using any number of helper
     functions from 0 to 4+. In general, correct solutions received
     full credit. It's good style to let-bind very small functions
     (e.g., square) rather than defining them separately. Also avoid
     redefining library functions (e.g., List.length), and use
     higher-order functions (e.g., List.map) where appropriate. ***)

assert (variance [] = None ) ;;
assert (variance [1.0] = None) ;;
assert (variance [1.0; 2.0; 3.0; 4.0; 5.0] > Some 2.499) ;;
assert (variance [1.0; 2.0; 3.0; 4.0; 5.0] < Some 2.501) ;;

(*>* Problem 3e *>*)

let rec count_divisors (n : int) (d : int) : int =
  if d * d > n then 0 else
    let count = count_divisors n (d + 1) in
    if n mod d = 0 then
      if d * d = n then count + 1
      else count + 2
    else count
;;

let few_divisors (n : int) (m : int) : bool =
  (count_divisors n 1) < m
;;
(*** Some people tried to cleverly count divisors by factoring. This
     was more difficult to implement and people often overlooked
     cases. ***)
(*** Some people wrote expressions of the form 'if expr then true else
     false' this can be written concisely in the form 'expr'.

     In other words, the two lines below are equivalent, use the
     shorter one
     
     if (x < 3) then true else false

     and

     (x < 3)
***)

assert ((few_divisors 17 2) = false) ;;
assert ((few_divisors 17 3) = true) ;;
assert ((few_divisors 4 3) = false) ;;
assert ((few_divisors 4 4) = true) ;;
assert ((few_divisors 1 1) = false) ;;
assert ((few_divisors 0 1) = true) ;;

(*>* Problem 3f *>*)
(* Challenge! *)

let rec interleave elt lst =
  match lst with
    | [] -> [[elt]]
    | hd :: tl ->
      let l1 = elt :: lst in
      let ls = interleave elt tl in
        l1 :: (List.map (fun a -> hd :: a) ls)
;;

let rec permutations lst =
  match lst with
    | [] -> [[]]
    | hd :: tl ->
      let subperms = permutations tl in
        List.flatten (List.map (interleave hd) subperms)
;;

assert (permutations [] = [[]]) ;;
assert (permutations [1] = [[1]]) ;;
assert (permutations [1;2] = [[1;2];[2;1]]) ;;
assert (permutations [1;2;3] = [[1;2;3]; [2;1;3]; [2;3;1]; [1;3;2]; [3;1;2]; [3;2;1]]) ;;
