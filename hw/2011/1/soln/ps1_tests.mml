(*** CS 51 Problem Set 1 ***)
(*** Jan 15, 2011 ***)
(*** Test Suite ***)


(*>* Problem 1a *>*)
(*>>* 2 *>>*)


>>>
let _ = (prob1a : (string * int) list) in true


(*>* Problem 1b *>*)
(*>>* 2 *>>*)

>>>
let _ = (prob1b : int) in true


(*>* Problem 1c *>*)
(*>>* 2 *>>*)

>>>
let _ = (prob1c : 'a option * float option) in true


(*>* Problem 1d *>*)
(*>>* 2 *>>*)

>>>
let _ = (prob1d : (float * (string *int) list) list) in true


(*>* Problem 1e *>*)
(*>>* 2 *>>*)

>>>
let _ = (prob1e :  (int * int -> int) * (float -> float -> float) * float) in true


(*>* Problem 1f *>*)
(*>>* 2 *>>*)

>>>
let _ = (prob1f : int) in true


(*>* Problem 1g *>*)
(*>>* 2 *>>*)

>>>
let _ = (prob1g : int) in true


(*>* Problem 2 *>*)
(*>>* 6 *>>*)

>>>
partition' [] 0 = ([], [])

>>>
partition' [1] 1 = ([], [1])

>>>
partition' [2] 1 = ([], [2])

>>>
partition' [1;2;3] 5 = ([1;2;3], [])

>>>
partition' [1;2;3] 0 = ([], [1;2;3])

>>>
partition' [1;2;3;4;5] 3 = ([1;2], [3;4;5])

>>>
partition' [2;1; 3;5;4] 3 = ([2;1], [3;5;4])

>>>
partition' [2;1; 3;5;2;4] 3 = ([2;1;2], [3;5;4])

>>>
partition' [3; 1; 4; 1; 5; 9; 2; 6] 4 = ([3; 1; 1; 2], [4; 5; 9; 6])



(*>* Problem 3a *>*)
(*>>* 5 *>>*)

>>>
reversed []

>>>
reversed [0]

>>>
not (reversed [1; 2])

>>>
reversed [8; 7; 4; 3; 2; 1]

>>>
not (reversed [8; 7; 4; 3; 4; 1])
 
>>>
not (reversed [8; 7; 4; 3; 2; 3])
 

(*>* Problem 3b *>*)
(*>>* 5 *>>*)

>>>
merge [] [] = []

>>>
merge [0] [] = [0]

>>>
merge [] [0] = [0]

>>>
merge [1; 2; 3; 5] [3; 3; 4; 7] = [1; 2; 3; 3; 3; 4; 5; 7]


(*>* Problem 3c *>*)
(*>>* 5 *>>*)

>>>
unzip [] = ([], [])

>>>
unzip [(1,2)] = ([1], [2])

>>>
unzip [(1,2);(17,42);(0,0)] = ([1;17;0], [2;42;0])


(*>* Problem 3d *>*)
(*>>* 5 *>>*)

let rec sum (l:float list) : float =
  match l with
  | x :: xs -> x +. sum xs
  | [] -> 0.0

let sol_variance (l:float list) : float option =
  let n = List.length l in
  if n < 2 then None else Some
    (let nf = float n in
     let mean = sum l /. nf in
     let square x = x *. x in
     (1.0 /. (nf -. 1.0)) *. sum (List.map (fun x -> square (x -. mean)) l))

let float_option_approx_eq (x1:float option) (x2:float option) : bool =
  match (x1, x2) with
  | (Some f1, Some f2) -> abs_float (f1 -. f2) < 1e-6
  | (None, None) -> true
  | (_, _) -> false

let correct_on (l:float list) =
  float_option_approx_eq (variance l) (sol_variance l) ;;

>>>
float_option_approx_eq (variance []) None

>>>
let res = variance [17.0] in
  float_option_approx_eq res None or
  float_option_approx_eq res (Some 0.0)


>>>
float_option_approx_eq (variance [3.0; 3.0]) (Some 0.0)

>>>
let res = (variance [3.0; 2.0; 1.0]) in
  float_option_approx_eq res (Some 1.0) or
  float_option_approx_eq res (Some (2./.3.))

>>>
let res = variance [1.0; 3.14; 2.72; 17.0] in
  float_option_approx_eq res (Some 54.9777) or
  float_option_approx_eq res (Some 41.233275)


(*>* Problem 3e *>*)
(*>>* 5 *>>*)

>>>
not (few_divisors 2 1)

>>>
not (few_divisors 4 2)

>>>
few_divisors 4 4

>>>
few_divisors 24 9

>>>
not (few_divisors 24 7)

>>>
not (few_divisors 216 16)

>>>
few_divisors 216 17


(*>* Problem 3f *>*)
(*>>* 2 *>>*)

let rec sol_interleave (n:int) (l:int list) : int list list =
  match l with
  | x :: xs ->
      (n :: x :: xs) :: List.map (fun l -> x :: l) (sol_interleave n xs)
  | [] -> [[n]]

let rec sol_permutations (l:int list) : int list list =
  match l with
  | x :: xs ->
      List.concat (List.map (sol_interleave x) (sol_permutations xs))
  | [] -> [[]]

let rec list_cmp (a:int list) (b:int list) =
  match a, b with
  | x :: xs, y :: ys ->
      if x < y then -1 else if x > y then 1 else list_cmp xs ys
  | [], y :: ys -> -1
  | x :: xs, [] -> 1
  | [], [] -> 0

let list_sort = List.sort compare

let list_eq (a:int list) (b:int list) = (list_cmp a b = 0)

let correct_on (l:int list) =
  (list_sort (permutations l) = list_sort (sol_permutations l))
;;

>>>
permutations [] = [[]]

>>>
permutations [42] = [[42]]

>>>
correct_on [1; 2; 3]

>>>
correct_on [2; 3; 5; 7; 2]
