(*** CS 51 Problem Set 1 ***)
(*** February 4, 2011 ***)
(*** YOUR NAME HERE ***)

(* Fill in types: *)
(*>* Problem 1a *>*)

(*
let prob1a : ??? = [("CS", 50); ("CS", 51)] ;;
*)


(*>* Problem 1b *>*)

(*
let prob1b : ??? = (fun (x, y) -> x * y) (3, 7) ;;
*)


(*>* Problem 1c *>*)

(*
let prob1c : ??? = (None, Some 123.0) ;;
*)

(* Fill in expressions with these types: *)
(*>* Problem 1d *>*)

(*
let prob1d : (float * (string * int) list) list =
  ???
;;
*)


(*>* Problem 1e *>*)

(*
let prob1e : (int * int -> int) * (float -> float -> float) * float  =
  ???
;;
*)

(* Fill in ??? with something that makes these typecheck: *)
(*>* Problem 1f *>*)

(*
let prob1f =
  let rec foo bar =
    match bar with
    | (a, (b, c)) :: xs -> a * (b + c + (foo xs))
    | _ -> 1
  in foo (???)
;;
*)


(*>* Problem 1g *>*)

(*
let prob1g =
  let v = (32.0, 28.0) in
  let square x = x *. x in
  let squared_norm (w: float * float) : float = ??? in
  let d = sqrt (squared_norm v) in
  int_of_float d
;;
*)


(*>* Problem 2 *>*)

(* Fix the style! *)

(* Note: type annotations like the following are just a guide (as is
   this comment); you can remove them once you have finished your solution. *)
(* let _ = partition : int list -> int -> int list * int list ;; *)

let rec partition (a: int list) (b: int) : (int list * int list) = 
  if (List.length a) = 0 then (a, a)
  else if (List.hd(a) < b) = true then 
    let (lt, gt) = partition (List.tl a) b in
      ([List.hd a] @ lt, gt)
  else
    let (lt, gt) = partition (List.tl a) b in
      (lt, [List.hd a] @ gt)
;;


(* TODO: Either add a signature for partition' here and make it clear that
 * that's what students are supposed to fill in, or change the writeup so that
 * it's clear that they're supposed to destructively update partition itself.
 * Basically 1/3 of the students in cs51spring11 rewrote partition instead of
 * creating a new partition' (for reference, we did not take off points for this
 * mistake. *)

(* Sample Tests *)
assert ((partition [1;2;3;4;5] 3) = ([1;2],[3;4;5])) ;;
assert ((partition [1;2;3;4;5] 1) = ([],[1;2;3;4;5])) ;;
assert ((partition [1;2;3;4;5] 6) = ([1;2;3;4;5],[])) ;;
assert ((partition [1;2;3;4;5] (-1)) = ([],[1;2;3;4;5])) ;;
assert ((partition [] 3) = ([],[])) ;;
assert ((partition [5;4;3;2;1] 3) = ([2;1],[5;4;3])) ;;
assert ((partition [5;4;3;2;1] 1) = ([],[5;4;3;2;1])) ;;
assert ((partition [5;4;3;2;1] 6) = ([5;4;3;2;1],[])) ;;
assert ((partition [5;4;3;2;1] (-1)) = ([],[5;4;3;2;1])) ;;
assert ((partition [-1] 0) = ([-1],[])) ;;
assert ((partition [1] 0) = ([],[1])) ;;
assert ((partition [-100] (-100)) = ([],[-100])) ;;
assert ((partition [0;-1;5;2;3;6;100;123;43] 17) =
		   ([0;-1;5;2;3;6],[100;123;43])) ;;


(*>* Problem 3a *>*)

(* reversed lst should return true if the items in the list are in 
 * non-increasing order. The empty list is considered to be reversed.*)

(* let _ = (reversed : int list -> bool) ;; *)


(*>* Problem 3b *>*)

(* merge xs ys takes two lists sorted in increasing order, and returns a
single merged list in sorted order.  For example:
   # merge [1;3;5] [2;4;6];;
- : int list = [1; 2; 3; 4; 5; 6]
# merge [1;3;5] [2;4;6;12];;
- : int list = [1; 2; 3; 4; 5; 6; 12]
# merge [1;3;5;700;702] [2;4;6;12];;
- : int list = [1; 2; 3; 4; 5; 6; 12; 700; 702]
*)

(* let _ = (merge : int list -> int list -> int list) ;; *)

(*
(* sample tests *)
assert ((merge [1;2;3] [4;5;6;7]) = [1;2;3;4;5;6;7]) ;;
assert ((merge [4;5;6;7] [1;2;3]) = [1;2;3;4;5;6;7]) ;;
assert ((merge [4;5;6;7] [1;2;3]) = [1;2;3;4;5;6;7]) ;;
assert ((merge [2;2;2;2] [1;2;3]) = [1;2;2;2;2;2;3]) ;;
assert ((merge [1;2] [1;2]) = [1;1;2;2]) ;;
assert ((merge [-1;2;3;100] [-1;5;1001]) = [-1;-1;2;3;5;100;1001]) ;;
assert ((merge [] []) = []) ;;
assert ((merge [1] []) = [1]) ;;
assert ((merge [] [-1]) = [-1]) ;;
assert ((merge [1] [-1]) = [-1;1]) ;;
*)

(*>* Problem 3c *>*)
(* unzip should be a function which, given a list of pairs, returns a
   pair of lists, the first of which contains each first element of
   each pair, and the second of which contains each second element.
   The returned lists should have the elements in the order that they
   appeared in the input.  So, for instance, 
   unzip [(1,2);(3,4);(5,6)] = ([1;3;5],[2;4;6]) 
*)

(* let _ = (unzip : (int * int) list -> int list * int list) ;; *)


(*>* Problem 3d *>*)

(* variance lst should be a function that returns None if lst has fewer than 2
elements, and Some of the variance of lst otherwise: 1/n * sum (x_i-m)^2, where
 a^2 means a squared, and m is the arithmetic mean of the list (sum of list /
length of list).  For example,
 - variance [1.0; 2.0; 3.0; 4.0; 5.0] = Some 2.5
 - variance [1.0] = None

Remember to use the floating point version of the arithmetic operators when
operating on floats (+. *., etc).  The "float" function can cast an int to a
float. 
*)

(* let _ = (variance : float list -> float option) ;; *)

(*>* Problem 3e *>*)

(* few_divisors n m should return true if n has fewer than m divisors, 
 * (including 1 and n) and false otherwise:
few_divisors 17 3;;
- : bool = true
# few_divisors 4 3;;
- : bool = false
# few_divisors 4 4;;
- : bool = true
 *) 
(* let _ = (few_divisors : int -> int -> bool) ;; *)


(*>* Problem 3f *>*)

(* Challenge!

  permutations lst should return a list containing every
  permutation of lst.  For example, one correct answer to 
  permutations [1;	2; 3] is 
  [[1; 2; 3]; [2; 1; 3]; [2; 3; 1]; [1; 3; 2];	[3; 1; 2]; [3; 2; 1]].  
   
  It doesn't matter what order the permutations appear in the returned list.
  Note that if the input list is of length n then the answer should be of
length n!.

  Hint:
  One way to do this is to write an auxiliary function,
  interleave : int -> int list -> int list list,
  that yields all interleavings of its first argument into its second:
  interleave 1 [2;3] = [ [1;2;3]; [2;1;3]; [2;3;1] ].
  You may also find occasion for the library functions
  List.map and List.concat.
*)

(* let _ = (permutations : int list -> int list list) ;; *)

