#!/bin/bash

if test -f /usr/local/bin/submit; then
    # TODO: If we can't debug why check_width doesn't always work, get rid of
    # requiring it!!!
    echo "Attempting line width check..."
    chmod u+x ./check_width
    ./check_width ps1.ml
    if (( $? != 0 )); then
	echo "Line width check failed; please fix before submitting."
    else
	rm -rf ps1-submit
	mkdir ps1-submit
	cp -p ps1.ml ps1-submit
	/usr/local/bin/submit lib51 1 `pwd`/ps1-submit
	echo "Done!"
    fi
else
    echo "You must run this on one of the nice.fas machines"
fi
