(*** CSCI E-250 Problem Set 1 ***)
(*** February 7, 2010 ***)
(*** YOUR NAME HERE ***)


(*>* Problem 1a *>*)

(*
let prob1a : ??? = [("CS", 50); ("CS", 51)] ;;
*)


(*>* Problem 1b *>*)

(*
let prob1b : ??? = (fun (x, y) -> x *. y) (4.0, 5.0) ;;
*)


(*>* Problem 1c *>*)

(*
let prob1c : ??? = (Some 123, None) ;;
*)


(*>* Problem 1d *>*)

(*
let prob1d : ((int * float) list * string) list =
  ???
;;
*)


(*>* Problem 1e *>*)

(*
let prob1e : (float -> float -> float) * (int * int -> int) =
  ???
;;
*)


(*>* Problem 1f *>*)

(*
let prob1f =
  let rec foo bar =
    match bar with
    | ((a, b), c) :: xs -> a * (b + c + (foo xs))
    | _ -> 1
  in foo (???)
;;
*)


(*>* Problem 1g *>*)

(*
let prob1g =
  let v = (32.0, 28.0) in
  let square x = x *. x in
  let squared_norm (w:float * float) : float = ??? in
  let d = sqrt (squared_norm v) in
  int_of_float d
;;
*)


(*>* Problem 2 *>*)

(* Note: type annotations like the following are just a guide (as is
   this comment); you can remove them once you have finished your solution. *)
(* let _ = merge : int list -> int list -> int list ;; *)


(*>* Problem 3a *>*)

(* let _ = sorted : int list -> bool ;; *)


(*>* Problem 3b *>*)

(* let _ = partition : int list -> int -> int list * int list ;; *)


(*>* Problem 3c *>*)

(* let _ = unzip : (int * int) list -> int list * int list ;; *)


(*>* Problem 3d *>*)

(* let _ = variance : float list -> float option ;; *)


(*>* Problem 3e *>*)

(* let _ = perfect : int -> bool ;; *)


(*>* Problem 3f *>*)

(* let _ = permutations : int list -> int list list ;; *)

