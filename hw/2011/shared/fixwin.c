/*
  CS 51 Tools
  Remove carriage returns (Windows/DOS CR/LF line endings) from submit scripts.
*/

#include <stdlib.h>

int main() {
  system("cat ./check_width | tr -d '\r' > ./check_width.fixed");
  system("mv ./check_width.fixed ./check_width");
  system("cat ./Makefile | tr -d '\r' > ./Makefile.fixed");
  system("mv ./Makefile.fixed ./Makefile");
  system("cat ./submit.sh | tr -d '\r' > ./submit.sh.fixed");
  system("mv ./submit.sh.fixed ./submit.sh");
  return 0;
}
