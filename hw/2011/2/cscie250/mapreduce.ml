(* This function should prove useful *)
let rec reduce (f:'a -> 'b -> 'b) (u:'b) (xs:'a list) : 'b =
  match xs with
    | [] -> u
    | hd::tl -> f hd (reduce f u tl) ;;



(***********************************************)
(******            1.1 WARM UP            ******)
(***********************************************)

(*>* Problem 1.1.a *>*)

(* Exercise 0.  Implement reduce using List.fold_right *)
let reduce_mine (f:'a -> 'b -> 'b) (u:'b) (xs:'a list) : 'b =
  raise (Failure "Not implemented")
;;



(****************************************************)
(******      1.2: Higher-Order Arithmetic      ******)
(****************************************************)

(*>* Problem 1.2.a *>*)

(* a. negate_all: Flips the sign of each element in a list *)
let negate_all (nums:int list) : int list =
  raise (Failure "Not implemented")
;;

(* Unit test example.  Uncomment after writing negate_All *)
(* assert ((negate_all [1; -2; 0]) = [-1; 2; 0]);; *)


(*>* Problem 1.2.b *>*)

(* b. sum: Implement sum using reduce! *)
let sum (nums:int list) : int =
  raise (Failure "Not implemented")
;;


(*>* Problem 1.2.c *>*)

(* c. sum_rows: Sums each list of integers in a list of lists of integers *)
let sum_rows (rows:int list list) : int list =
  raise (Failure "Not implemented")
;;


(*>* Problem 1.2.d *>*)

(* d. filter_odd: Retains only the odd numbers from the given list *)
let filter_odd (nums:int list) : int list =
  raise (Failure "Not implemented")
;;


(*>* Problem 1.2.e *>*)

(* e. num_occurs: Returns the number of times a given number (n)
 *                appears in a list (nums) *)
let num_occurs (nums:int list) (n:int) : int =
  raise (Failure "Not implemented")
;;


(*>* Problem 1.2.f *>*)

(* f. super_sum: Sums all of the numbers in a list of int lists *)
let super_sum (nlists:int list list) : int =
  raise (Failure "Not implemented")
;;

(*>* Problem 1.2.g *>*)

(* g. filter_range: Returns a list of numbers in the input list within a
 *                  given range (inclusive), in the same order they appeared
 *                  in the input list.
 * For example, filter_range [1;3;4;5;2] (1,3) = [1;3;2] *)
let filter_range (nums:int list) (range:int * int) : int list =
  raise (Failure "Not implemented")
;;



(****************************************************)
(**********       1.3 Fun with Types       **********)
(****************************************************)

(*>* Problem 1.3.a *>*)

(* a. floats_of_ints: Converts an int list into a list of floats *)
let floats_of_ints (nums:int list) : float list =
  raise (Failure "Not implemented")
;;


(*>* Problem 1.3.b *>*)

(* b. optionalize: Returns a list in which each element of the original
 * list is converted into the corresponding option. *)
let optionalize (lst: 'a list) : 'a option list =
  raise (Failure "Not implemented")
;;


(*>* Problem 1.3.c *>*)

(* c. some_sum: Sums all of the numbers in a list of int options;
 *              ignores None values *)
let some_sum (nums:int option list) : int =
  raise (Failure "Not implemented")
;;


(*>* Problem 1.3.d *>*)

(* d. deoptionalize: Extracts values from a list of options.
 * Ex: deoptionalize [Some 3; None; Some 5; Some 10] = [3;5;10] *)
let deoptionalize (l:'a option list) : 'a list =
  raise (Failure "Not implemented")
;;


(*>* Problem 1.3.e *>*)

(* e. mult_odds: Product of all of the odd members of a list.
 * Ex: mult_odds [1;3;0;2;5] = 15 *)
let mult_odds (nums:int list) : int =
  raise (Failure "Not implemented")
;;


(*>* Problem 1.3.f *>*)

(* f. concat: Concatenates a list of lists. See the Ocaml library ref *)
let concat (lists:'a list list) : 'a list =
  raise (Failure "Not implemented")
;;


(*>* Problem 1.3.g *>*)

(* the student's name and year *)
type name = string
type year = int
type student = name * year

(* g. filter_by_year: returns the names of the students in a given year
 * Ex: let students = [("Joe",2010);("Bob",2010);("Tom",2013)];;
 *     filter_by_year students 2010 => ["Joe","Bob"] *)
let filter_by_year (slist:student list) (y:year) : name list =
  raise (Failure "Not implemented")
;;


(*>* Problem 1.4 *>*)

(* Please give us an honest estimate of how long this Part of the problem
 * set took you to complete.  We care about your responses and will use
 * them to help guide us in creating future assignments. *)
let minutes_spent_on_part_1 : int = raise (Failure "Not answered") ;;
