(* CS51 PS2 Spring 2011 *)
(* SOLUTIONS *)
open Ast ;;
open ExpressionLibrary ;;

(*>* Problem 2.1 *>*)

(* contains_var : tests whether an expression contains a variable "x"
 *     Examples : contains_var (parse "4^x") = true
 *                contains_var (parse "4+3") = false *)
let rec contains_var (e:expression) : bool =
  match e with
    | Var -> true
    | Unop (_,e1) -> contains_var e1
    | Binop (_,e1,e2) -> contains_var e1 || contains_var e2
    | _ -> false
;;



(*>* Problem 2.2 *>*)

(* --------------------- Evaluate ---------------------- *)

(* The "denotation" of a unary operator.
   We map object-language unary operators to Caml unary operators
   (i.e., functions). *)
let unop_denote (u:unop) =
  match u with
    | Sin -> sin
    | Cos -> cos
    | Ln -> log
    | Neg -> fun x -> -. x  (* We need to wrap this in a fun since otherwise
			       the -. syntax is interpreted as binary minus
			       (i.e. subtraction rather than negation). *)
;;

(* The "denotation" of a binary operator.
   We map object-language binary operators to Caml binary operators
   (i.e., functions). *)
let binop_denote (b:binop) =
  match b with
    | Add -> ( +. )
    | Sub -> ( -. )
    | Mul -> ( *. )
    | Div -> ( /. )
    | Pow -> ( ** )
;;

(* Evaluate expression e at x.
   0/0 is evaluated rather arbitrarily as 0.
   So are 0*nan and 0*infinity. *)

(* evaluate : evaluates an expression for a particular value of x.
 *  example : evaluate (parse "x^4 + 3") 2.0 = 19.0 *)
let rec evaluate (e:expression) (x:float) : float =
  match e with
    | Num n -> n
    | Var -> x
    | Unop (u,e1) -> unop_denote u (evaluate e1 x)
    | Binop (b,e1,e2) -> binop_denote b (evaluate e1 x) (evaluate e2 x)
;;



(*>* Problem 2.3 *>*)

let rec derivative (e:expression) : expression =
  match e with
    | Num _ -> Num 0.
    | Var -> Num 1.
    | Unop (u,e1) ->
        (match u with
           | Sin -> Binop(Mul,Unop(Cos,e1),derivative e1)
           | Cos -> Binop(Mul,Unop(Neg,Unop(Sin,e1)),derivative e1)
           | Ln -> Binop(Div,derivative e1,e1)
           | Neg -> Unop(Neg,derivative e1)
	)
    | Binop (b,e1,e2) ->
        match b with
          | Add -> Binop(Add,derivative e1,derivative e2)
          | Sub -> Binop(Sub,derivative e1,derivative e2)
          | Mul -> Binop(Add,Binop(Mul,e1,derivative e2),
                         Binop(Mul,derivative e1,e2))
          | Div -> Binop(Div,
			 Binop(Sub,Binop(Mul,derivative e1,e2),
			       Binop(Mul,e1,derivative e2)),
			 Binop(Pow,e2,Num(2.)))
          | Pow ->
	      if contains_var e2 then
                Binop(Mul,e,Binop(Add,Binop(Mul,derivative e2,Unop(Ln,e1)),
                                  Binop(Div,Binop(Mul,derivative e1,e2),e1)))
              else Binop(Mul,e2,
			 Binop(Mul,derivative e1,Binop(Pow,e1,
                                                       Binop(Sub,e2,Num 1.))))
;;


(*>* Problem 2.4 *>*)

let rec find_zero (e:expression) (guess:float) (epsilon:float) (lim:int)
    : float option =
  let e' = derivative e in
  let rec find_zero' (guess:float) (count:int) : float option =
    if count >= lim then None else
    let e_of_guess = evaluate e guess in
    if abs_float e_of_guess >= epsilon then
      find_zero' (guess -. (e_of_guess /. evaluate e' guess)) (count + 1)
    else Some guess
  in find_zero' guess 0
;;


(*>* Problem 2.5 *>*)

let rec simplify (e:expression) : expression =
  match e with
    | Num n -> if (n < 0.) then Unop (Neg, Num (-. n)) else e
    | Var -> e
    | Unop (u,e1) ->
        let e1' = simplify e1 in
          (match u,e1' with
             | Neg, Unop (Neg,e2) -> simplify e2
             | Neg,Num 0. -> Num 0.
             | Ln,Num 1. -> Num 0.
             | Sin,Num 0. -> Num 0.
             | Cos,Num 0. -> Num 1.
             | _ -> Unop(u,e1')
	  )
    | Binop(b,e1,e2) ->
        let e1' = simplify e1 in
        let e2' = simplify e2 in
          match b with
            | Add ->
		if (e1'=e2') then simplify (Binop(Mul,Num 2.,e1')) else
                  (match e1',e2' with
                     | Num 0.,_ -> e2'
                     | _,Num 0. -> e1'
                     | Unop(Neg,e1''),_ -> simplify (Binop(Sub,e2',e1''))
                     | _,Unop(Neg,e2'') -> simplify (Binop(Sub,e1',e2''))
                     | Num n1,Num n2 -> simplify (Num (n1 +. n2))
                     | _ -> Binop(Add,e1',e2')
		  )
            | Sub ->
		if (e1'=e2') then Num 0. else
                  (match e1',e2' with
                     | Num 0.,_ -> simplify (Unop(Neg,e2'))
                     | _,Num 0. -> e1'
                     | _,Unop(Neg,e2'') -> simplify (Binop(Add,e1',e2''))
                     | Unop(Neg,e1''),_ ->
			 simplify (Unop(Neg,Binop(Add,e1'',e2')))
                     | Num n1,Num n2 -> simplify (Num(n1 -. n2))
                     | _ -> Binop(Sub,e1',e2')
                  )
            | Mul ->
		if (e1'=e2') then simplify (Binop(Pow,e1',Num 2.)) else
                  (match e1',e2' with
                     | Num 0.,_ -> Num 0.
                     | _,Num 0. -> Num 0.
                     | Num 1.,_ -> e2'
                     | _,Num 1. -> e1'
                     | Unop(Neg,e1''),_ ->
                         simplify (Unop(Neg,Binop(Mul,e1'',e2')))
                     | _,Unop(Neg,e2'') ->
                         simplify (Unop(Neg,Binop(Mul,e1',e2'')))
                     | Binop(Div,e1'1,e1'2),_ ->
                         simplify (Binop(Div,Binop(Mul,e1'1,e2'),e1'2))
                     | _,Binop(Div,e2'1,e2'2) ->
                         simplify (Binop(Div,Binop(Mul,e1',e2'1),e2'2))
                     | Num n1,Num n2 -> simplify (Num(n1 *. n2))
                     | _ -> Binop(Mul,e1',e2')
                  )
            | Div ->
		if (e1'=e2') then Num 1. else
                  (match e1',e2' with
                     | Num 0.,_ -> Num 0.
                     | _,Num 1. -> e1'
                     | Unop(Neg,e1''),_ ->
                         simplify (Unop(Neg,Binop(Div,e1'',e2')))
                     | _,Unop(Neg,e2'') ->
                         simplify (Unop(Neg,Binop(Div,e1',e2'')))
                     | Binop(Div,e1'1,e1'2),_ ->
                         simplify (Binop(Div,e1'1,Binop(Mul,e1'2,e2')))
                     | _,Binop(Div,e2'1,e2'2) ->
                         simplify (Binop(Div,Binop(Mul,e1',e2'2),e2'1))
                     | Num n1,Num n2 -> simplify (Num(n1 /. n2))
                     | _ -> Binop(Div,e1',e2')
                  )
          | Pow ->
	      (match e1',e2' with
                 | _,Num 0. -> Num 1.
                 | Num 0.,_ -> Num 0.
                 | _,Num 1. -> e1'
                 | _,Unop(Neg,e2'') ->
                     simplify (Binop(Div,Num 1.,Binop(Pow,e1',e2'')))
                 | Binop(Pow,e1'1,e1'2),_ ->
                     simplify (Binop(Pow,e1'1,Binop(Mul,e1'2,e2')))
                 | _ -> Binop(Pow,e1',e2')
              )
;;


(*>* Problem 2.6 *>*)

let minutes_spent_on_part_2 : int = 42 ;;
