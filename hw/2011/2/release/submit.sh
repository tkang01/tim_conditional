#!/bin/bash -e

if test -f /usr/local/bin/submit; then
	rm -rf ps2-submit
	mkdir ps2-submit
	cp -p expression.ml ps2-submit
	cp -p mapreduce.ml ps2-submit
	/usr/local/bin/submit lib51 2 `pwd`/ps2-submit
	echo "Done!"
else
    echo "You must run this on one of the nice.fas machines"
fi
