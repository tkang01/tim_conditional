(* PS3
 * CS51 Spring 2011
 *** SOLUTION SET ***
 *)

(* This function should prove useful *)
let rec reduce (f:'a -> 'b -> 'b) (u:'b) (xs:'a list) : 'b =
  match xs with
    | [] -> u
    | hd::tl -> f hd (reduce f u tl) ;;

(************************************************************************)
(*>* Problem 0 *>*)
(* Warmup.
 * a. Define a function, add1, that takes an int->int function f and returns a
 * function that, given an integer, returns 1 more than f would return. *)

let add1 (f: int->int) : int->int = 
  fun x -> (f x) + 1;;

(* b. Now define add2, which does the same thing as add2, but adds 2 instead
   of 1. In writing add2, you may use only the argument f, add1, and
   parentheses. *)

let add2 (f: int->int) : int->int =  
  add1 (add1 f);; 

(* c. Define addn, which, you guessed it, adds n, which is passed in as
   another argument. You must use add1 and may not explicitly add or
   multiply. You can tell from the signature that this function is
   recursive. Consider what the base case is. *)

let rec addn (n: int) (f: int->int) : int->int =
  if (n=0) then f else add1 (addn (n-1) f);;

(************************************************************************)
(* Here is a module for defining binary trees *)
module Tree = 
struct
  (* Here is a type definition for an 'a binary_tree. Note that 'a is
     polymorphic; thus, this type definition allows you to create string
     binary_trees, int binary_trees, etc. It is also recursive, in that
     a Branch takes an 'a and two 'a binary_trees. *)
  type 'a binary_tree =
      Empty | Branch of 'a * 'a binary_tree * 'a binary_tree;;

  let some_tree = Branch(1, Branch(2,Empty,Empty), 
                         Branch(3,Empty,Empty)) ;;

  (* We're giving you a working version of tree_fold. The name should
     aid you in figuring out what it does. *)
  let rec tree_fold (f:'a -> 'b -> 'b -> 'b) (u:'b) (t:'a binary_tree) : 'b =
    match t with 
      | Empty -> u
      | Branch(value,left,right) ->
	      f value (tree_fold f u left) (tree_fold f u right)
  ;;

  (* This initializes the random number generator. *)
  Random.self_init ();;

  (* We'll also give you a function to generate a random integer tree of a 
     specified depth. You don't need to write it, and shouldn't read it too 
     carefully since it uses side-effects and is therefore pure evil. *)
  let rec random_tree (depth: int) : int binary_tree =
    if depth = 0 then Empty
    else
      Branch ((Random.int 20)-10, random_tree (depth-1), 
              random_tree (depth-1));;

  (* This function print out a tree, given a way to print elements *)
  let print_tree (print_elt : 'a -> unit) (t:'a binary_tree) : unit =
    (* print out n spaces *)
    let rec print_spaces(n:int) : unit = 
      match n with 
        | 0 -> ()
        | _ -> (Printf.printf " " ; print_spaces (n-1)) in
      (* print a tree, but indent the tree by indent spaces *)
    let rec print(indent:int)(t:'a binary_tree) : unit = 
      print_spaces indent ; 
      match t with 
        | Empty -> Printf.printf "Empty\n"
        | Branch(value,left,right) -> 
            (Printf.printf "Value: ";
             print_elt value ; 
             Printf.printf "\n" ; 
             (* increase indentation by 2 *)
             print (indent + 2) left ; 
             print (indent + 2) right)
    in 
      print 0 t
  ;;

  let print_int_tree = print_tree (fun (x:int) -> Printf.printf "%d" x)
end (* Module Trees *)

(************************** Part 1: Trees *******************************)

(************************************************************************)
(*>* Problem 1.1 *>*)
(* Write a function which produces a mirror image of a tree.  The mirror
 * image has (recursively) all left and right sub-trees swapped. *)
let tree_mirror (tree: 'a Tree.binary_tree) : 'a Tree.binary_tree = 
  Tree.tree_fold (fun v l r -> Tree.Branch(v, r, l)) Tree.Empty tree

(************************************************************************)
(*>* Problem 1.2 *>*)

(* Write tree_sum using tree_fold. tree_sum takes an int binary_tree
 * and returns the sum of all the values in the tree. *)
let tree_sum (tree: int Tree.binary_tree) = 
  Tree.tree_fold (fun x l r -> x + l + r) 0 tree

(************************************************************************)
(*>* Problem 1.3 *>*)

(* Write tree_map using tree_fold. Tree_map should take a
   binary_tree of arbitrary type and map the function f over all of
   its elements. *)
let tree_map (f:'a -> 'b) (tree:'a Tree.binary_tree) : 'b Tree.binary_tree = 
  Tree.tree_fold (fun x l r -> Tree.Branch(f x, l, r)) Tree.Empty tree

(************************************************************************)
(*>* Problem 1.4 *>*)

(* Write tree_to_list using tree_fold.
   tree_to_list takes an int binary_tree and returns a list of all
   the elements in the tree in left-to-right order, such that
   if the tree were a binary search tree, the numbers would
   be sorted low-to-high. (Don't worry about efficiency.) *)

let tree_to_list (tree: int Tree.binary_tree) = 
  Tree.tree_fold (fun x l r -> l @ [x] @ r) [] tree;;

(************************************************************************)  
(*>* Problem 1.5 *>*)

(* Above, you wrote an implementation of tree_map.  Why isn't
   there a natural way to write tree_filter?
   
   -- Write your answer here.
   
*)

(************************************************************************)
(*>* Problem 1.6 *>*)
module NaryTree = 
struct   
  (* Exercise: n_trees. Define a datatype for non-binary trees, where a
     node may have any number of children. You should probably pattern
     your solution after the binary_tree datatype given above. *)
  
  type 'a n_tree = Empty | Branch of 'a * 'a n_tree list

  (************************************************************************)
  (*>* Problem 1.7 *>*)

  (* Exercise: Implement n_tree_sum, a function to add up all the values
     in an int n_tree. *)
  let rec n_tree_sum (t: int n_tree) = 
    match t with
      | Empty -> 0
      | Branch(x, l) -> x + (reduce (+) 0 (List.map n_tree_sum l))

  (************************************************************************)
  (*>* Problem 1.8 *>*)

  (* Karma problem: define a fold for your n_tree.  Use it to redefine 
     n_tree_sum. *)
  let rec n_tree_fold (f : 'a -> 'b list -> 'b) (u: 'b) (t : 'b n_tree) =
    match t with
      | Empty -> u
      | Branch(x, l) -> f x (List.map (n_tree_fold f u) l)

  let n_tree_sum_better (t: int n_tree) = 
    n_tree_fold (fun x l -> x + reduce (+) 0 l) 0 t
  
  (* This is a more general fold *)
  let rec n_tree_fold' (nt:'a -> 'b -> 'c) (cons:'c -> 'b -> 'b) (nil:'c) (t: 'a n_tree) : 'c =
    let rec fold_children (l:'a n_tree list) : 'b = 
      match l with
        | [] -> nil
        | hd::tl -> cons ((n_tree_fold' nt cons nil hd):'c) ((fold_children tl):'b)
    in
      match t with
        | Empty -> nil
        | Branch(x, l) -> nt x (fold_children l) 


  let n_tree_sum_more_better (t : int n_tree) =
    n_tree_fold' (+) (+) 0 t;;


end (* module NaryTree *)

(***************************** Part 2: Games ****************************)


module GameTree = 
struct
  open Tree 
    (* A move is either left or right, and a strategy is a function
       from trees to moves. *)
  type game_tree = int binary_tree 
  type move = Left | Right 
  type strategy = game_tree -> move 
    
  let left_tree = Branch(7, Branch(2, Empty, Empty),
                         Branch(6, Branch(5, Empty, Empty),
                                Branch(11, Empty, Empty)));;

  let right_tree = Branch(5, Empty, Branch(9, Branch(4, Empty, Empty),
                                           Empty));;

  let test_tree = Branch(2, left_tree, right_tree);;

  (*>* Problem 2.1 *>*)
  (* Implement tree_move. 
     Given a player's next move (Left or Right), tree_move returns a
     function mapping a game tree (int binary_tree) to
     the subtree on which play will continue, assuming there is
     such a sub-tree.  If there is no such game-tree, it returns
     the Empty tree.  
  *)
  let tree_move (m : move) : (game_tree->game_tree) =
    (fun t ->
       match t with
         | Empty -> Empty
         | Branch(_, l, r) ->
             (match m with
                | Left -> l
                | Right -> r));;

  (* Computes the net score (current player's score minus opponent's score)
   * that will be achieved if the players play with the given strategies. *)
  let rec tgame_score (strat: strategy) (oppstrat: strategy) 
      (t: game_tree) : int =
    match t with
      | Empty -> 0
      | Branch (value,left,right) -> 
          value - tgame_score oppstrat strat (tree_move (strat t) t) ;;

  (* Computes the result of the game (+1: player to move wins; -1: opponent 
   * wins 0: draw) assuming the players play with the given strategies. *)
  let tgame_result (strat: strategy) (oppstrat: strategy) 
      (t: game_tree) : int =
    let r = (tgame_score strat oppstrat t) in
      if r < 0 then -1
      else if r > 0 then 1
      else 0
        
  (* Given two strategies, return the total result of playing them against
   * each other in n random games on trees of depth d. *)
  let rec tgame_results_sum (strat: strategy) (oppstrat: strategy) (n: int) 
      (d: int) : int =
    if (n <= 0) then 0
    else
      (tgame_result strat oppstrat (random_tree d)) 
      + (tgame_results_sum strat oppstrat (n-1) d);;
  
  (* Given two strategies, return the average result of playing them 
   * against each other in 2n random games on trees of depth d, n 
   * games with each strategy playing first to compensate for any 
   * advantage. *)
  let rec tgame_results_average (strat: strategy) (oppstrat: strategy) 
      (n: int) (d: int) : float =
    let norm = 2.0 *. (float_of_int n) in
    let strat_first = float_of_int (tgame_results_sum strat oppstrat n d) in
    let strat_second = float_of_int (tgame_results_sum oppstrat strat n d) in
      (strat_first -. strat_second) /. norm;;
  
end (* module GameTree *)

open Tree;;
open GameTree;;

(* We can define a simple, and very silly, strategy: always move left. *)
let tgame_silly_strategy (t: game_tree) : move = Left ;;

(*>* Problem 2.2 *>*)
(* Write tgame_greedy_strategy, a greedy strategy for The Tree
 * Game.  On a game tree t, a player playing this strategy
 * should choose the branch that minimizes the score the other
 * player will receive on the very next turn (consider this
 * amount zero if there would be no next turn).  If the value
 * is the same for both subtrees, you can return either one.
 *)

let tgame_immed_val (t: game_tree) : int =
  match t with
    | Empty -> 0
    | Branch(v, l, r) -> v;;

let tgame_greedy_strategy (t: game_tree) : move =
  match t with
    | Empty -> Left
    | Branch(v, l, r) -> if tgame_immed_val l < tgame_immed_val r
      then Left else Right;;

assert (tgame_greedy_strategy test_tree = Right);;

(* Now test the greedy strategy against the silly strategy in 1000
 * games on trees of depth 6, using the functions provided above,
 * and record the result below. If you've designed your greedy strategy
 * well, this number should be positive if tgame_greedy_strategy 
 * is player 1 and negative if it's player 2. *)

(* --Run and Record test results here. *)

(*>* Problem 2.3 *>*)
(* We can do better than a greedy strategy. In particular,
 * suppose for the moment that we know our opponent's
 * strategy. Then we can predict how our opponent will respond
 * to any potential situation for the rest of the game.  If we
 * also assume that we ourselves will continue to choose the
 * best response to the opponent's strategy for the rest of
 * the game, then given any candidate move, we can completely
 * determine how the game will play out if we make that move;
 * we can then use this information to select the move that
 * will maximize our score.
 * 
 * Write tgame-best-response-strategy, which, when given the
 * opponent's strategy, returns a strategy that will yield
 * the highest possible score. (Hint: you can write this in
 * terms of tgame-score, but you need to use recursion in an
 * unusual way. It may help to remember that functions,
 * including strategies, are first-class values. Again, 
 * don't worry about efficiency.)
 *)

let rec tgame_best_response_strategy (oppstrat: strategy) 
    : strategy =
  fun (t: game_tree) -> 
    let s = tgame_best_response_strategy oppstrat in
      match t with
        | Empty -> Left
        | Branch (v, l, r) -> 
            (* Does opponent do better if I go right or left? *)
            if (tgame_score oppstrat s l) > (tgame_score oppstrat s r)
            then Right
            else Left;;


assert (tgame_best_response_strategy tgame_silly_strategy left_tree =
    Right);;

assert (tgame_best_response_strategy tgame_greedy_strategy left_tree =
    Right);;

(*>* Problem 2.4 *>*) 
(* Define strategies, tgame_best_vs_silly_strategy and 
 * tgame_best_vs_greedy_strategy, that are the best strategies to 
 * use when playing against the silly strategy and the greedy 
 * strategy, respectively. *)

let tgame_best_vs_silly_strategy (t : game_tree) = 
  tgame_best_response_strategy tgame_silly_strategy t;;

let tgame_best_vs_greedy_strategy (t : game_tree) = 
  tgame_best_response_strategy tgame_greedy_strategy t;;

(* Now test tgame_best_vs_silly_strategy against tgame_silly_strategy 
 * and tgame_best_vs_greedy_strategy against tgame_greedy_strategy, 
 * as you did in Problem 2.2. *)

(* --Record test results here. *)

(*>* Problem 2.5 *>*)
(* Karma problem: *)
(* Using tgame-best-response-strategy is great if we know
 * our opponent's strategy.  But what if we're playing an
 * arbitrary opponent?  A standard technique in game theory
 * is to assume perfect play: that is, we play a strategy
 * that maximizes our score, under the assumption that the
 * opponent will play a strategy that maximizes their score
 * (under the assumption that we will play a strategy that
 * maximizes our score, and so on).
 * 
 * Write tgame-perfect-strategy, which maximizes the current
 * player's score assuming perfect play on the part of
 * the opponent.  (Hint: this should be short, elegant,
 * and written in terms of tgame-best-response-strategy.)
 *)
let rec tgame_perfect_strategy (t: game_tree) =
  (tgame_best_response_strategy tgame_perfect_strategy) t;;

assert (tgame_perfect_strategy test_tree = Right);;



(*>* Problem 3 *>*)
(* Please give us an honest estimate of how long this problem set took 
 * you to complete.  We care about your responses and will use
 * them to help guide us in creating future assignments. *)
let minutes_spent : int = -1
