#!/bin/bash -e

if test -f /usr/local/bin/submit; then
	rm -rf ps3-submit
	mkdir ps3-submit
	cp -p ps3.ml ps3-submit
	/usr/local/bin/submit lib51 3 `pwd`/ps3-submit
	echo "Done!"
else
    echo "You must run this on one of the nice.fas machines"
fi
