(*** CS 51 Problem Set 1 ***)
(*** January 31, 2014 ***)
(*** YOUR NAME HERE ***)

(* Open up the library we'll be using in this course *)
open Core.Std

(* Problem 1 - Fill in types:
 * Replace each ??? with the appropriate type of the corresponding expression.
 * Be sure to remove the comments from each subproblem and to type check it
 * before submission. *)

(*>* Problem 1a *>*)
let prob1a : string = let greet y = "Hello " ^ y in greet "World!";;

(*>* Problem 1b *>*)
let prob1b : int option list = [Some 4; Some 2; None; Some 3];;

(*>* Problem 1c *>*)
let prob1c : ('a option * float option) * bool = ((None, Some 42.0), true);;


(* Explain in a comment why the following will not type check,
   and provide a fix *)

(*>* Problem 1d *>*)

(* The type should be a list of tuples, not a tuple with an string
 * and an int list *)
let prob1d : (string * int) list = [("CS", 51); ("CS", 50)];;

(*>* Problem 1e *>*)
(* Cannot compare an int with a float *)
let prob1e : int =
  let compare (x,y) = x < y in
  if compare (4.0, 3.9) then 4 else 2;;

(*>* Problem 1f *>*)
(* The type of the second part of the tuple says string, but there are
 * both 'a options (None) and ints as the second part. Changing
 * the type to int option and all of the ints to "Some x" is a fix *)
let prob1f : (string * int option) list =
  [("January", None); ("February", Some 1); ("March", None); ("April", None);
   ("May", None); ("June", Some 1); ("July", None); ("August", None);
   ("September", Some 3); ("October", Some 1); ("November", Some 2);
   ("December", Some 3)] ;;



(* Problem 2 - Write the following functions *)
(* For each subproblem, you must implement a given function and corresponding
 * unit tests (i.e. assert expressions). You are provided a high level
 * description as well as a prototype of the function you must implement. *)

(*>* Problem 2a *>*)

(* `reversed lst` should return true if the integers in lst are in
 * decreasing order. The empty list is considered to be reversed. Consecutive
 * elements can be equal in a reversed list. *)

(* Here is its prototype/signature: *)
(* reversed : int list -> bool *)

(* Implement reversed below, and be sure to write tests for it (see 2b for
 * examples of tests). *)


let rec reversed (lst : int list) : bool =
  match lst with
  | []
  | [_] -> true
  | hd1 :: (hd2 :: _ as tl) -> hd1 >= hd2 && reversed tl

let () = assert (reversed [])
let () = assert (reversed [2])
let () = assert (reversed [2;2;2])
let () = assert (not (reversed [2;1;2]))
let () = assert (reversed [2;2;1])
let () = assert (reversed [9;8;7;6;5;5;5;5;4;-2])
let () = assert (not (reversed [9;8;7;6;7;5;5;5;5;4;3]))

(*>* Problem 2b *>*)

(* merge takes two integer lists, each sorted in increasing order,
 and returns a single merged list in sorted order. For example:

merge [1;3;5] [2;4;6];;
- : int list = [1; 2; 3; 4; 5; 6]
merge [1;3;5] [2;4;6;12];;
- : int list = [1; 2; 3; 4; 5; 6; 12]
merge [1;3;5;700;702] [2;4;6;12];;
- : int list = [1; 2; 3; 4; 5; 6; 12; 700; 702]

*)

(* The type signature for merge is as follows: *)
(* merge : int list -> int list -> int list *)

let rec merge (a : int list) (b : int list) : int list =
  match (a, b) with
  | (_, []) -> a
  | ([], _) -> b
  | (hd1 :: tl1, hd2 :: tl2) ->
    if hd1 < hd2 then hd1 :: merge tl1 b else hd2 :: merge a tl2


(* sample tests *)
let () = assert (merge [1;2;3] [4;5;6;7] = [1;2;3;4;5;6;7]);;
let () = assert (merge [4;5;6;7] [1;2;3] = [1;2;3;4;5;6;7]);;
let () = assert (merge [4;5;6;7] [1;2;3] = [1;2;3;4;5;6;7]);;
let () = assert (merge [2;2;2;2] [1;2;3] = [1;2;2;2;2;2;3]);;
let () = assert (merge [1;2] [1;2] = [1;1;2;2]);;
let () = assert (merge [-1;2;3;100] [-1;5;1001] = [-1;-1;2;3;5;100;1001]);;
let () = assert (merge [] [] = []);;
let () = assert (merge [1] [] = [1]);;
let () = assert (merge [] [-1] = [-1]);;
let () = assert (merge [1] [-1] = [-1;1]);;


(*>* Problem 2c *>*)
(* unzip should be a function which, given a list of pairs, returns a
 * pair of lists, the first of which contains each first element of
 * each pair, and the second of which contains each second element.
 * The returned lists should have the elements in the order in which
 * they appeared in the input. So, for instance:

unzip [(1,2);(3,4);(5,6)];;
- : int list * int list = ([1;3;5],[2;4;6])

*)


(* The type signature for unzip is as follows: *)
(* unzip : (int * int) list -> int list * int list) *)

let rec unzip (lst : (int * int) list) : int list * int list =
  match lst with
  | [] -> ([], [])
  | (l, r) :: tl ->
    let (left, right) = unzip tl in (l :: left, r :: right)

let () = assert(unzip [] = ([],[]))
let () = assert(unzip [(1,2);(3,4)] = ([1;3],[2;4]))


(*>* Problem 2d *>*)

(* `variance lst` returns None if lst has fewer than 2 floats, and
 * Some of the variance of the floats in lst otherwise.  Recall that
 * the variance of a sequence of numbers is 1/(n-1) * sum (x_i-m)^2,
 * where a^2 means a squared, and m is the arithmetic mean of the list
 * (sum of list / length of list). For example:

variance [1.0; 2.0; 3.0; 4.0; 5.0];;
- : int option = Some 2.5
variance [1.0];;
- : int option = None

 * Remember to use the floating point version of the arithmetic
 * operators when operating on floats (+. *., etc). The "float"
 * function can cast an int to a float. *)

(* variance : float list -> float option *)

(* Returns the sum and length of a float list *)
let variance (lst : float list) : float option =
  let rec sum_length (lst : float list) : float * int =
    match lst with
    | [] -> (0., 0)
    | hd :: tl -> let (sum, len) = sum_length tl in (hd +. sum, 1 + len) in

  let (sum, length) = sum_length lst in

  if length < 2 then None else Some (
    let flength = float length in
    let mean = sum /. flength in
    let rec residuals (lst : float list) : float =
      match lst with
      | [] -> 0.
      | hd :: tl -> (hd -. mean) ** 2. +. residuals tl in
    residuals lst /. (flength -. 1.)
  )

let float_option_approx_eq (x1 : float option) (x2 : float option) : bool =
  let epsilon = 1e-6 in
  match (x1, x2) with
  | (Some f1, Some f2) -> Float.abs (f1 -. f2) < epsilon
  | (None, None) -> true
  | (_, _) -> false

let () = assert (float_option_approx_eq (variance []) None)
let () = assert (float_option_approx_eq (variance [1.]) None)
let () = assert (float_option_approx_eq (variance [1.0;2.0;3.0;4.0;5.0])
    (Some 2.5))
let () = assert (float_option_approx_eq (variance [1.0;1.0]) (Some 0.0))
let () = assert (float_option_approx_eq (variance [3.0;2.0;1.0]) (Some 1.0))


(*>* Problem 2e *>*)

(* few_divisors n m should return true if n has fewer than m divisors,
 * (including 1 and n) and false otherwise. Note that this is *not* the
 * same as n having fewer divisors than m:

few_divisors 17 3;;
- : bool = true
few_divisors 4 3;;
- : bool = false
few_divisors 4 4;;
- : bool = true

 * Do not worry about negative integers at all. We will not test
 * your code using negative values for n and m, and do not
 * consider negative integers for divisors (e.g. don't worry about
 * -2 being a divisor of 4) *)

(* The type signature for few_divisors is: *)
(* few_divisors : int -> int -> bool *)

let few_divisors (n:int) (m:int) : bool =
  let rec count_divs (div : int) : int =
    if div > n/2 then 1 else
      (if n mod div = 0 then 1 else 0) + count_divs (div + 1) in
  count_divs 1 < m

let () = assert (few_divisors 17 3)
let () = assert (not (few_divisors 4 3))
let () = assert (not (few_divisors 13 2))
let () = assert (few_divisors 15 5)
let () = assert (not (few_divisors 30 6))
let () = assert (few_divisors 1 2)
let () = assert (not (few_divisors 0 1))

(*>* Problem 2f *>*)

(* `concat_list sep lst` returns one big string with all the string
 * elements of lst concatenated together, but separated by the string
 * sep. Here are some example tests:

concat_list ", " ["Greg"; "Anna"; "David"];;
- : string = "Greg, Anna, David"
concat_list "..." ["Moo"; "Baaa"; "Quack"];;
- : string = "Moo...Baaa...Quack"
concat_list ", " [];;
- : string = ""
concat_list ", " ["Moo"];;
- : string = "Moo"

*)

(* The type signature for concat_list is: *)
(* concat_list : string -> string list -> string *)

let rec concat_list (sep:string) (lst:string list) : string =
  match lst with
  | [] -> ""
  | [hd] -> hd
  | hd :: tl -> hd ^ sep ^ (concat_list sep tl)

let () = assert (concat_list "" ["a"; "b"; "c"] = "abc")
let () = assert (concat_list ", " ["Greg"; "Anna"; "David"] =
    "Greg, Anna, David")
let () = assert (concat_list ", " ["Greg"] = "Greg")
let () = assert (concat_list ", " [] = "")


(*>* Problem 2g *>*)

(* One way to compress a list of characters is to use run-length encoding.
 * The basic idea is that whenever we have repeated characters in a list
 * such as ['a';'a';'a';'a';'a';'b';'b';'b';'c';'d';'d';'d';'d'] we can
 * (sometimes) represent the same information more compactly as a list
 * of pairs like [(5,'a');(3,'b');(1,'c');(4,'d')].  Here, the numbers
 * represent how many times the character is repeated.  For example,
 * the first character in the string is 'a' and it is repeated 5 times,
 * followed by 3 occurrences of the character 'b', followed by one 'c',
 * and finally 4 copies of 'd'.
 *
 * Write a function to_run_length that converts a list of characters into
 * the run-length encoding, and then write a function from_run_length
 * that converts back. Writing both functions will make it easier to
 * test that you've gotten them right. *)

(* The type signatures for to_run_length and from_run_length are: *)
(* to_run_length : char list -> (int * char) list *)
(* from_run_length : (int * char) list -> char list *)

let rec to_run_length (lst : char list) : (int * char) list =
  match lst with
  | [] -> []
  | hd :: tl ->
    match to_run_length tl with
    | [] -> [(1,hd)]
    | (n, c) :: tl as tl' -> if hd = c then (n+1, c)::tl else (1, hd)::tl'

let () = assert (to_run_length [] = [])
let () = assert (to_run_length ['a'] = [(1, 'a')])
let () = assert (to_run_length ['a';'a'] = [(2, 'a')])
let () = assert (to_run_length ['a';'a';'b';'a'] = [(2, 'a');(1, 'b');(1,'a')])

let rec from_run_length (lst : (int * char) list) : char list =
  match lst with
  | [] -> []
  | (0, _) :: tl -> from_run_length tl
  | (i, c) :: tl -> c :: from_run_length ((i-1, c) :: tl)

let () = assert (from_run_length [] = [])
let () = assert (from_run_length [(1,'a')] = ['a'])
let () = assert (from_run_length [(1,'a');(2, 'b')] = ['a';'b';'b'])
let () = assert (from_run_length [(1,'a');(2, 'a')] = ['a';'a';'a'])

let () = assert (from_run_length (to_run_length ['h';'e';'l';'l';'o']) =
  ['h';'e';'l';'l';'o'])

(*>* Problem 3 *>*)

(* Challenge!

 * permutations lst should return a list containing every
 * permutation of lst. For example, one correct answer to
 * permutations [1; 2; 3] is
 * [[1; 2; 3]; [2; 1; 3]; [2; 3; 1]; [1; 3; 2]; [3; 1; 2]; [3; 2; 1]].

 * It doesn't matter what order the permutations appear in the returned list.
 * Note that if the input list is of length n then the answer should be of
 * length n!.

 * Hint:
 * One way to do this is to write an auxiliary function,
 * interleave : int -> int list -> int list list,
 * that yields all interleavings of its first argument into its second:
 * interleave 1 [2;3] = [ [1;2;3]; [2;1;3]; [2;3;1] ].
 * You may also find occasion for the library functions
 * List.map and List.concat. *)

(* The type signature for permuations is: *)
(* permutations : int list -> int list list *)

let rec interleave (n : int) (lst : int list) : int list list =
  match lst with
  | [] -> [[n]]
  | x :: xs -> (n :: x :: xs) :: List.map ~f:(fun l -> x :: l) (interleave n xs)

let rec permutations (lst : int list) : int list list =
  match lst with
  | [] -> [[]]
  | x :: xs -> List.concat (List.map ~f:(interleave x) (permutations xs))
;;

(* since permutations doesn't specify the order the elements
   are returned in, we need to sort them to test our results *)
let rec list_cmp (a:int list) (b:int list) =
  match a, b with
  | [], [] -> 0
  | [], _ -> -1
  | _, [] -> 1
  | x :: xs, y :: ys ->
      if x < y then -1 else if x > y then 1 else list_cmp xs ys

let list_sort = List.sort ~cmp:list_cmp;;


let () = assert (permutations [] = [[]])
let () = assert (permutations [1] = [[1]])
let () = assert (list_sort (permutations [1;2]) = list_sort [[1;2];[2;1]])
let () = assert (list_sort (permutations [1;2;3]) =
  list_sort [[1;2;3];[1;3;2];[2;1;3];[2;3;1];[3;1;2];[3;2;1]])
