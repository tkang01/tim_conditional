import Control.Monad
import Data.List
import System.IO
import System.Process
import Text.Printf
import qualified Data.ByteString.Char8 as BS

main = do
  files <- liftM (lines . BS.unpack) $ BS.readFile "dynamic_files.txt"
  dirs <- liftM (lines . BS.unpack) $ BS.readFile "dirs.txt"
  let dirsStagger = zip dirs (tail dirs)
  forM dirsStagger $ \ (d1,d2) -> do
    putStrLn $ printf "\n\n---------- %s v %s ----------\n\n" d1 d2
    forM files $ \ f -> do
      let p1 = printf "%s/%s" d1 f :: String
          p2 = printf "%s/%s" d2 f :: String
      (_,result,_) <- readProcessWithExitCode "diff" [p1,p2] ""
      when (result /= "") $ do
        putStrLn $ printf "%s and %s are different:" p1 p2
        putStrLn result
  hFlush stdout
  return ()
