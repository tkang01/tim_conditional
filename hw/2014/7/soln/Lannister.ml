open Core.Std
open WorldObject
open WorldObjectI

(** Random bees will move randomly. *)
class lannister p city : Human.human_t =
object
  inherit Human.human p city

  (********************************)
  (***** WorldObjectI Methods *****)
  (********************************)

  (* ### TODO: Part 5 Smart Bees *)
  method! get_name = "lannister"

  (***********************)
  (***** Bee METHODS *****)
  (***********************)

  (* ### TODO: Part 5 Smart Bees *)
  method! private next_direction_default = Some (Direction.random World.rand)

end


