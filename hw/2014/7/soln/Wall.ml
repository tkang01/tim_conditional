open Core.Std
open Helpers
open WorldObject
open WorldObjectI

(* ### Part 6 Custom Events ### *)
let smelly_object_limit = 200

(** A pasture will spawn a cow when there are enough objects in the world that
    smell like pollen. *)
class wall p (city : KingsLanding.kings_landing) : world_object_i =
object (self)
  inherit world_object p

  (******************************)
  (***** Instance Variables *****)
  (******************************)

  (* ### TODO Part 6 Custom Events ### *)
  val mutable cow_is_grazing = false

  (***********************)
  (***** Initializer *****)
  (***********************)

  (* ### TODO Part 6 Custom Events ### *)
  initializer
    self#register_handler World.action_event (fun () -> self#do_action)

  (**************************)
  (***** Event Handlers *****)
  (**************************)

  (* ### TODO Part 6 Custom Events ### *)
  method private do_action =
    let smelly_objects =
      World.fold begin fun o t -> match o#smells_like_gold with
        | Some _ -> t + 1
        | None -> t
      end 0
    in
    if smelly_objects >= smelly_object_limit && not cow_is_grazing then begin
      print_string "mooooooooo " ; flush_all () ;
      let c = new WhiteWalker.white_walker self#get_pos (self :> world_object_i) city in
      cow_is_grazing <- true ;
      self#register_handler c#get_die_event (fun () -> cow_is_grazing <- false)
    end

  (********************************)
  (***** WorldObjectI Methods *****)
  (********************************)

  (* ### TODO Part 1 Basic ### *)
  method! get_name = "wall"

  method! draw =
    self#draw_circle (Graphics.rgb 70 100 130) Graphics.white "W"

end

