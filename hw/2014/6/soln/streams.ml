(*********************************** trees ************************************)
(* Here we provide a definition for infinite binary treestreams. This
   definition is analogous to the definition of streams provided in
   lecture. *)

type 'a tree = unit -> 'a tr
and 'a tr = Stem of 'a * 'a tree * 'a tree ;;

(*>* Problem 2.1.a *>*)
(* Write a function headt which takes an 'a treestream and returns the value
   at the root of the tree *)

let headt (t: 'a tree) : 'a =
  let Stem(v, _, _) = t () in v
;;

(*>* Problem 2.1.b *>*)
(* Write functions ltail and rtail which take a treestream and return its
   left and right subtrees respectively *)

let ltail (t: 'a tree) : 'a tree =
  let Stem(_, t', _) = t () in t'
;;

let rtail (t: 'a tree) : 'a tree =
  let Stem(_, _, t') = t () in t'
;;

(*>* Problem 2.1.c *>*)
(* Write a function mapt which takes takes a function f and maps it
   over the given treestream *)

let rec mapt (f: 'a -> 'b) (t: 'a tree) : 'b tree =
  fun () -> let Stem(v, left, right) = t () in
    Stem(f v, mapt f left, mapt f right)
;;

(*>* Problem 2.1.d *>*)
(* Write a function zipt which takes a function f and two treestreams
   t1 and t2 and combines them into one treestream. If x1 and x2 are the
   values at corresponding locations in t1 and t2 respectively, then
   the corresponding value in "zipt f t1 t2" should be "f x1 x2" *)

let rec zipt (f: 'a -> 'b -> 'c) (t1: 'a tree) (t2: 'b tree) : 'c tree =
  fun () -> Stem(f (headt t1) (headt t2), zipt f (ltail t1) (ltail t2), zipt f (rtail t1) (rtail t2))
;;

(* Define a treestream of all ones *)

(*>* Problem 2.1.e *>*)
let rec onest () =
  Stem(1, onest, onest)
;;

(* Define a treestream in which each positive natural number appears
exactly once, and where the first few rows are 1; 2 3; 4 5 6 7,
etc. This should look something like the following

           1
         /   \
       2       3
     /   \   /   \
    4    5   6    7
   / \  / \ / \  / \
...
*)

(*>* Problem 2.1.f *>*)
let rec treenats () =
  (* Tree where the top element is x *)
  let rec treenat x =
    fun () -> Stem(x, treenat (2*x), treenat (2*x+1)) in
    treenat 1 ()
;;


(***************** Using the Lazy module ******************)
(* Here we provide an alternate implementation of streams using
   OCaml's lazy module. We recommend that you explore the
   documentation at
   http://caml.inria.fr/pub/docs/manual-ocaml/libref/Lazy.html

   In this portion, you will be reimplementing various functions that
   were defined in class.
*)

open Lazy

type 'a stream = Cons of 'a * 'a stream Lazy.t

let rec ones = Cons(1, lazy(ones));;
let rec twos = Cons(2, lazy(twos));;

(*>* Problem 2.2.a *>*)
(* Implement the head function *)

let head (s:'a stream) : 'a =
  let Cons(v,_) = s in v
;;

(*>* Problem 2.2.b *>*)
(* Implement map *)

let rec map (f:'a -> 'b) (s:'a stream) : 'b stream =
  let Cons(v,t) = s in Cons(f v, lazy(map f (Lazy.force t))) ;;

(*>* Problem 2.2.c *>*)
(* Define nats *)

let rec nats = Cons(0, lazy(map ((+) 1) nats)) ;;

(*>* Problem 2.2.d *>*)
(* Write a function nth, which returns the nth element of a stream *)

let rec nth (n:int) (s:'a stream) : 'a =
  if n = 0 then head s else let Cons(_,t) = s in
    nth (n-1) (Lazy.force t)
;;

(*>* Problem 2.2.e *>*)
(* Now suppose we have two int streams s1 and s2 sorted in ascending order. We wish to merge these into a single stream s such that s is sorted in ascending order and has no duplicates. Implement this function *)

let merge (s1:int stream) (s2:int stream) : int stream =
  let rec helper (s1:int stream) (s2:int stream) (v:int) : int stream =
    let (Cons(v1,t1),Cons(v2,t2)) = (s1,s2) in
      if v1 < v2 then
	if v1 = v then helper (Lazy.force t1) s2 v
	else Cons(v1, lazy(helper (Lazy.force t1) s2 v1))
      else
	if v2 = v then helper s1 (Lazy.force t2) v
	else Cons(v2, lazy(helper s1 (Lazy.force t2) v2)) in
  let (Cons(v1,_),Cons(v2,_)) = (s1,s2) in
  let v = (min v1 v2) - 1 in
    helper s1 s2 v
;;

(*>* Problem 2.2.f *>*)
(* What problems can we run into with this conception of "merge"? What
   if we were to run "merge ones ones"? Answer within the comment. *)

(* 
   Answer: It will fail find a second element for the stream because
   all of the elements are duplicates, so it will cause a stack
   overflow error
*)

(*>* Problem 2.2.g *>*)
(* Write a function "scale", which takes an integer "n" and an int
   stream "s", and multiplies each element of "s" by "n". *)

let scale n = map (( * ) n) ;;

(*>* Problem 2.2.h *>*)
(* Suppose we wish to create a stream of the positive integers "n" in
   increasing order such that any prime factor of "n" is either 3 or
   5. The first few numbers are 1, 3, 5, 9, 15, 25, 27, ... hopefully
   you get the idea. One way to do this is to run "filter" over
   "nats". But we can do better than that. *)

(* Let "hs" denote the desired stream. Observe that "hs" satisfies the
   following properties

   1. The elements of "scale S 3" are elements of "hs"
   2. The elements of "scale S 5" are elements of "hs"
   3. "hs" is the minimal stream (sorted in increasing order) which
   contains "1" and satisfies the above properties

   Think about why properties 1-3 uniquely characterize "hs".
*)

(* Use the above discussion and functions you've already written to
   give a simple definition for "hs". This can be done quite
   elegantly. *)

let rec selectivestream = Cons(1, lazy(merge (scale 3 selectivestream) (scale 5 selectivestream))) ;;

assert ((nth 0 selectivestream) = 1);;
assert ((nth 1 selectivestream) = 3);;
assert ((nth 2 selectivestream) = 5);;
