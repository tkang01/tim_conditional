#!/bin/bash

function total()
{
    if [ -f $1 ]
    then
	echo `grep ':total' $1 | awk -F ' ' '{ print $5 }'`
    else
	echo $2
    fi
}

function correct()
{
    if [ -f $1 ]
    then
	echo `grep ':total' $1 | awk -F ' ' '{ print $3 }'`
    else
	echo $2
    fi
}

function total_style()
{
    echo `grep 'res:' $1 | awk -F ' ' '{ print $3 }'`
}


for x in submissions/*
do
    OUT=$x/moogle_gc.txt

    P1=../ps4/$x/moogle_phase1_grade.txt
    C1=`correct ${P1} 0`
    T1=`total ${P1} 33`
    P2=$x/moogle_phase2_grade.txt
    C2=`correct ${P2} 0`
    T2=`total ${P2} 31`
    ST=`total_style $x/style.txt`

    echo $x "-" $C1 $C2 $T1 $T2 $ST
    if [ $ST == / ]
    then
	echo "Missing style for " $x
	ST=-
	C=`expr $C1 + $C2`
	T=`expr $T1 + $T2 + 10`
    else
	C=`expr $C1 + $C2 + $ST`
	T=`expr $T1 + $T2 + 10`
    fi

    echo "Please forward to your partner" >  ${OUT}
    echo ""                               >> ${OUT}
    echo ""                               >> ${OUT}
    echo "Correctness"                    >> ${OUT}
    echo "  Phase 1  " ${C1} " / " ${T1}  >> ${OUT}
    echo "  Phase 2  " ${C2} " / " ${T2}  >> ${OUT}
    echo "Style/Misc " ${ST} " / 10"      >> ${OUT}
    echo "---------------------------"    >> ${OUT}
    echo "Total      " ${C}  " / " ${T}   >> ${OUT}

    echo ""                               >> ${OUT}
    echo "Part 1"                         >> ${OUT}
    if [ -f ../ps4/$x/moogle_phase1_grade.txt ]
    then
	cat ../ps4/$x/moogle_phase1_grade.txt >> ${OUT}
    else
	echo "Missing"                        >> ${OUT}
    fi

    echo ""                               >> ${OUT}
    echo "Part 2"                         >> ${OUT}
    if [ -f ../ps4/$x/moogle_phase1_grade.txt ]
    then
	cat $x/moogle_phase2_grade.txt    >> ${OUT}
    else
	echo "Missing"                    >> ${OUT}
    fi
    echo ""                               >> ${OUT}
    echo "Style/Misc"                     >> ${OUT}
    cat $x/style.txt                      >> ${OUT}

    echo "-- pagerank.ml"                 >> ${OUT}
    cat $x/pagerank.ml                    >> ${OUT}
    
done