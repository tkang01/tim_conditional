#!/bin/bash

echo InDegree stats
echo -n total: ;./grab.sh | grep InDegree | wc -l
echo -n success: ;./grab.sh | grep InDegree | grep SUCCESS | wc -l
echo -n uninit: ;./grab.sh | grep InDegree | grep MissNode | wc -l
echo -n fail: ;./grab.sh | grep InDegree | grep FAIL | wc -l 
echo -n unimplemented: ;./grab.sh | grep InDegree | grep -e " 0 " | wc -l

echo Random stats
echo -n total: ;./grab.sh | grep Random | wc -l
echo -n success: ;./grab.sh | grep Random | grep SUCCESS | wc -l
echo -n fail: ;./grab.sh | grep Random | grep FAIL | wc -l
echo -n uninit: ;./grab.sh | grep Random | grep MissNode | wc -l
echo -n unimplemented: ;./grab.sh | grep Random | grep -e " 0 " | wc -l

echo Quantum stats
echo -n total: ;./grab.sh | grep Quantum | wc -l
echo -n success: ;./grab.sh | grep Quantum | grep SUCCESS | wc -l
echo -n fail: ;./grab.sh | grep Quantum | grep FAIL | wc -l
echo -n unimplemented: ;./grab.sh | grep Quantum | grep -e " 0 " | wc -l

echo Per student
for x in submissions/*
do 
    echo $x
    grep -e "res: InDegree SUCCESS" $x/moogle.txt | wc -l
    grep -e "res: Random SUCCESS" $x/moogle.txt | wc -l
    grep -e "res: Quantum SUCCESS" $x/moogle.txt | wc -l
done
