open Graph
open Nodescore
open Pagerank
open HelpNS

let unop def = function
  | None -> def
  | Some x -> x
  
module JohnInDegreeRanker  (GA: GRAPH) (NSA: NODE_SCORE with module N = GA.N) : 
  (RANKER with module G = GA with module NS = NSA) =
struct
  module G = GA
  module NS = NSA

  let rank g = 
    let update ns n =
      let nl = unop [] (G.neighbors g n) in
      List.fold_left (fun ns n -> NS.add_score ns n 1.) ns nl in
    let ns = List.fold_left update (NS.zero_node_score_map (G.nodes g)) (G.nodes g) in
    NS.normalize ns
 
end

module Student = InDegreeRanker (TestGraph) (TestScore)
module Staff = JohnInDegreeRanker (TestGraph) (TestScore)

let test e =
  let nsref = Staff.rank gsimple in
  Printf.fprintf stderr "*** InDegreeRanker ***\n";
  Printf.fprintf stderr "res: InDegree ";
  if try
       compare nsref (Student.rank gsimple) e
    with
      | _ -> false
  then
    Printf.fprintf stderr "4 / 4\n"
  else
    Printf.fprintf stderr "0 / 4\n"
;;

test 0.01
