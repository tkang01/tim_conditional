#!/usr/bin/env node
// Script to check that the Moogle server returns the expected set of pages,
// and prints the computed page ranks.
// Before running this script, make sure the Moogle server is running and
// listening on port 8080.

var http = require('http');

// load a search results page
function grab_page(search_term, cb) {
	http.get({
		host: 'localhost',
		port: 8080,
		path: '/?q=' + search_term
	}, function(res) {
		var page = '';
		res.setEncoding('utf8');
		res.on('data', function(chunk) { page += chunk; });
		res.on('end', function() {
			cb(page);
		});
	});
}

// extract the pages returned by a search from the HTML
function extract_pages(html) {
	// who needs an HTML parser when you can use regex?
	var matches = html.match(/<li>\d*\.\d*\s*<a href="\/([a-zA-Z_0-9]+)\.html">/g) || [];
	return matches.map(function(frag) {
		return frag.match(/<li>(\d*\.\d*)\s*<a href="\/([a-zA-Z_0-9]+)\.html">/).slice(1);
	});
}

// test whether two arrays are equal, because JS does not do that natively
function equal_arrays(arr1, arr2) {
	return arr1.length === arr2.length && arr1.reduce(function(a, b, i) {
		return a && (b === arr2[i]);
	}, true);
}

// test a single search, print result
function test_search(search_term, words) {
	grab_page(search_term, function(html) {
		var result_words = extract_pages(html);
		var just_words = result_words.map(function(f) { return f[1]; }).sort();
		if(equal_arrays(just_words, words.sort())) {
			console.log("PASSED: " + search_term);
			console.log("RANKS: ");
			result_words.forEach(function(arr) {
				console.log(arr[0] + " - " + arr[1]);
			});
		} else {
			console.log("FAILED: " + search_term);
			console.log("Expected: " + words);
			console.log("Found: " + result_words);
		}
	});
}

var tests = [
	{'term': 'cow', 'pages': ['cow', 'moo', 'index']},
	{'term': 'functor', 'pages': ['functor', 'ocaml', 'index']},
	{'term': 'barrier', 'pages': ['abstraction']},
	{'term': 'fuzzy', 'pages': ['functor']},
	{'term': 'abstraction', 'pages': ['abstraction', 'ocaml', 'index']},
	{'term': 'greg', 'pages': []},
];

tests.forEach(function(test) {
	test_search(test.term, test.pages);
});
