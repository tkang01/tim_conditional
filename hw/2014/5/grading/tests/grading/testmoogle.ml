(* Test the dictionary implementation *)

open Dict
open Set
open Testdict

let check_assert b s = 
    if b then ()
    else (Printf.printf "Assertion failed: %s\n" s)

(***** Dict tests *****)

module D =  RBTreeDict( 
  struct 
	type key = int
	type value = int
	let compare = Order.int_compare
  end);;

let ins = (fun (a,b) d -> D.insert d a b)

let is_empty d = 
  match D.choose d with
    | None -> true
    | _ -> false

let rec insert_up_to d a n = 
  if a >= n then d else 
    insert_up_to (D.insert d a 1) (a+1) n;;


let check_dict1 u = 
  let d = List.fold_right ins [(1,2); (2,7); (3,5); 
                               (4,12) ; (5,-1); (6,-2)] D.empty in
    check_assert (D.member d 1) "check_dict1, member 1";
    check_assert (D.member d 2) "check_dict1, member 2";
    check_assert (D.member d 3) "check_dict1, member 3";
    check_assert (D.member d 4) "check_dict1, member 4";
    check_assert (D.member d 5) "check_dict1, member 5";
    check_assert (not (D.member d 0)) "check_dict1, not member 0";
    check_assert (not (D.member (D.remove d 1) 1)) "check_dict1, remove";; 

let check_dict2 u = 
  (* Test choose *)
  let d = D.empty in
    check_assert ((D.choose d) = None) "check_dict2, empty";
    let d = ins (1,2) d in
      check_assert ((D.choose d) = Some (1, 2, D.empty)) "check_dict2, choose";;

let check_dict3 u = 
  (* Test fold *)
  let d = insert_up_to D.empty 0 10 in
  let sumkeys = D.fold (fun k v a -> k + a) 0 d in
  let sumvals = D.fold (fun k v a -> v + a) 0 d in
    check_assert (sumkeys = 45) "check_dict3, sumkeys";
    check_assert (sumvals = 10) "check_dict3, sumvals";;    
    
let check_dict4 u = 
  let d = D.insert D.empty 1 2 in 
  let d1 = List.fold_right ins [(1,2); (2,7)] D.empty in
  let d2 = D.remove d1 2 in 
  let d3 = D.remove d1 3 in
    check_assert (is_empty D.empty) "check_dict4, Empty is empty";
    check_assert (D.member d 1) "check_dict4, member test1";

    check_assert (D.lookup d1 1 = Some 2) "check_dict4, lookup 1";
    check_assert (D.lookup d1 2 = Some 7) "check_dict4, lookup 2";
    check_assert (D.lookup d1 3 = None) "check_dict4, lookup 3";
    
    check_assert (D.lookup d2 2 = None) "check_dict4, lookup 4";
    check_assert (D.lookup d3 2 = (Some 7)) "check_dict4, lookup 5";;

let time =
    let start = Unix.gettimeofday () in
    fun () -> Unix.gettimeofday () -. start;;

let rec do_lookups d n =
  if n < 0 then () else
    let _ = D.lookup d n in
      do_lookups d (n-1);;


let perf_test (u:unit) : unit = 
  (* Return a dict with (0,1), (1,1), (2, 1) ... (n, 1) inserted *)
    (* Look up the numbers from n down to 0 *)
  let n = 4000 in
  let d = insert_up_to D.empty 0 n in
    (* And now do some lookups and time them *)
  let start = time () in
  let _ = do_lookups d n in
  let end_t = time () in
  let t = (end_t -. start) in
    Printf.printf "Perf test took %f seconds\n" t;
    (* Empirically tested on Victor's laptop: RB-trees finish in 0.005 seconds.
       assoclist in 0.73 seconds *)
    if t > 0.1 then
      Printf.printf "Looks suspiciously slow. Unbalanced tree?\n"
    else ();;


(***** Set tests *****)

(* Test the set in terms of the working dicts we gave them, not their RB ones *)
module S = DictSet(GradingAssocListDict(  
  struct 
	type key = int
	type value = unit
	let compare = Order.int_compare
  end));;

let check_set1 u =
  (* Basic ops on tiny sets *)
  let s = S.empty in
  let s' = S.insert 12 s in
    check_assert (S.is_empty s) "check_set1, is_empty";
    check_assert (not (S.is_empty s')) "check_set1, not is_empty";
    check_assert (S.is_empty (S.remove 12 s')) "check_set1, remove";
    check_assert (S.member s' 12) "check_set1, member";
    check_assert (S.choose s = None) "check_set1, choose empty";
    check_assert (S.choose s' = Some (12, s)) "check_set1, choose non-empty";;

let set_ins lst = 
  List.fold_right S.insert lst S.empty;;

let check_all_members s lst =
  List.fold_right (fun x b -> b && S.member s x) lst true;;

(* True if same elements in any order *)
let list_set_equal xs ys =
  let xs = List.sort compare xs in
  let ys = List.sort compare ys in
    (List.length xs = List.length ys && (List.for_all2 (=) xs ys));;

let list_of_set s = 
  S.fold (fun x l -> x::l) [] s

let check_set2 u = 
  (* Bigger sets, insersect + union *)
  let odd = set_ins [1;3;5;7] in
  let even = set_ins [0;2;4;6] in
  let union = S.union odd even in
  let diff = S.intersect odd even in
  let odd' = S.intersect odd union in
    check_assert (check_all_members union [0;1;2;3;4;5;6;7]) "check_set2, union";
    check_assert (S.is_empty diff) "check_set2, diff";
    check_assert (list_set_equal (list_of_set odd) [1;3;5;7]) "check_set2, fold";
    check_assert (list_set_equal (list_of_set odd') [1;3;5;7]) "check_set2, diff2";;
    
let check_set3 u =
  let s = set_ins [1;2;3;3;3;3;4;5;6;7] in
    check_assert (List.length (list_of_set s) = 7) "check_set3, uniqueness";;

Printf.printf "check_dict1\n";;
check_dict1 ();;
Printf.printf "check_dict2\n";;
check_dict2 ();;
Printf.printf "check_dict3\n";;
check_dict3 ();;
Printf.printf "check_dict4\n";;
check_dict4 ();;

Printf.printf "perf_test\n";;
perf_test ();;

Printf.printf "check_set1\n";;
check_set1 ();;
Printf.printf "check_set2\n";;
check_set2 ();;
Printf.printf "check_set3\n";;
check_set3 ();;
