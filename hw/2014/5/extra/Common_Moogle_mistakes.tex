\documentclass[12pt]{article}

\usepackage{alltt}
\usepackage{hyperref}

% \input{decls-common}

%% Page layout
\oddsidemargin 0pt
\evensidemargin 0pt
 \textheight 600pt 
\textwidth 469pt 
\setlength{\parindent}{0em} 
\setlength{\parskip}{1ex}

\begin{document}

\title{Common Mistakes and Confusions on Moogle: Part 1}

\author{Gideon Wald} \date{February 26$^{th}$, 2010}

\maketitle

\section{Introduction}

The purpose of this document is to attempt to consolidate the explanations, solutions, fixes and hints for a number of very common recurring troubles that you have all been having on Moogle Part 1. We hope that this will help those of you still working on Part 1, as well as help everybody get up to speed and work more efficiently on Part 2.

\section{Sharing definitions between files ("Unbound type constructor Order.order")}

Many of you use Eclipse, and as with most IDEs, it hides from you much of what is happening behind the scenes. Additionally, we have not explained clearly the differences between "\#load," "\#use," and "open" in lecture or section.

\begin{itemize}
\item Modules+files: when you compile a file ``order.ml'', everything in it is implicitly wrapped in a module called ``Order'' (capitalize the filename to get the module name).  This means that the compiled file behaves differently than the contents of the file simply typed into the top level.  See the discussion of \#use below.
\item \#load: This directive takes a ".cmo" or ".cma" file. These abbreviations stand for "compiled caml object" and "compiled caml archive" (yes, m is for caml). It will load the compiled objects in the supplied file and make them available to the program. For example, if one of these compiled objects contains a module called Order, you will then be able to access Order.foo in the top-level for each value foo in the module Order. In particular, in order to get rid of the irritating error quoted in the section header, first type "\#load order.cmo" into your top-level.
\begin{itemize}
\item This is only a top-level directive. You can't put it in your code.
\item In order to generate the .cmo/.cma files, you can either use make (if we've supplied you a Makefile) or compile the .ml files yourself. For example: "ocamlc -g -o moogle.exe unix.cma str.cma order.ml dict.ml set.ml util.ml query.ml moogle.ml" from the command line should work (only supply moogle.exe if you're on Windows). Note that the order of the files supplied to ocamlc matters.
\item CAUTIONS: None, really - you should make liberal use of this directive if you must do top-level development or testing. Do remember to recompile a module and retype "\#load module.cmo" every time you make edits to the module.
\end{itemize}

\item \#use: When supplied with a file, this directive is identical to typing the contents of the file into the top-level. Thus, it should be given a .ml file. However, note that in particular, it will not wrap the module name around the code in question. \#load is vastly preferable; rather than using \#use, you should first compile your code and then employ \#load.
\begin{itemize}
\item This is also only a top-level directive.
\item CAUTIONS: First compiling all of your code and then using \#load in the top-level is safer and will sometimes work where \#use will not (e.g., when your code explicitly refers to the name of the module that contains a given definition, which is good coding practice - see below).
\item Bottom line: don't use \#use. Post on the BB if you think you need it, and we'll help you find another way.
\end{itemize}

\item open: This is an ocaml command, and thus can be placed in your code as well as run from the top-level. It takes the name of a module. What it does is import all the names from the module into your local namespace, meaning that you won't need to type the name of the module before accessing types, functions or values defined in that module.
\begin{itemize}
\item CAUTIONS: Using open is often dangerous. It makes it hard to tell what dependencies your code has, and can make it hard to track down the source of certain errors. It is usually preferable to avoid using open, and instead simply use the name of the module every time you need to access one of its members (e.g., use "Order.order" everywhere instead of having "open Order" at the top of your file and then using "order" throughout).
\end{itemize}

\end{itemize}

\subsection{Relevant bulletin board links}

\begin{itemize}
\item \url{https://forums.seas.harvard.edu/cs51/viewtopic.php?f=14\&t=129}
\item \url{https://forums.seas.harvard.edu/cs51/viewtopic.php?f=14\&t=132}
\item \url{https://forums.seas.harvard.edu/cs51/viewtopic.php?f=19\&t=165}
\end{itemize}

\section{Testing your code}

The question of how to test your code well is not an easy one to resolve for this problem set. The main reason that it is so difficult is that we are doing our development within {\bf functors}.

Recall from lecture that a functor is a function from modules to modules. In particular, both {\it RBTreeDict} and {\it DictSet} are functors. This means that they are {\bf not} modules! You cannot create an instance of an RBTreeDict and try to call {\it insert} on it, because an instance of an {\it RBTreeDict} is a module, not a dictionary.

In order to test code that lives within these functors, you must make a module that specializes the {\it key} and {\it value} types - e.g., a dictionary from ints to ints, or floats to strings, or whatever. Once you have such a module, you can then make instances of that module and call the dictionary functions on them with arguments of the appropriate types. You can see moogle.ml for an example - the creation of LinkSet and WordDict provide examples of the needed syntax.

Even with this confusion cleared up, however, there are still two ways to test: the right way and the easy way.

\underline{{\bf The Right Way}}: Create a new interface, including the DICT\_ARG interface but extending it to include debugging functions, and a new functor that implements that interface to create modules for testing your code. Then you can use this testing module's additional testing functions to run whatever tests you like.

Alternatively, you can write unit tests within the module - called, e.g., test1(), test2(), ..., testn() - and then at the end of your module include the line "let \_ = (test1(); test2(); ...; testn())". This will run all of your unit tests in succession when the module loads. If you take this approach and your tests take a long time, you could comment out that line before submitting so that Moogle doesn't re-run all of your unit tests every time it starts up.  However, I would suggest leaving them in unless they're causing performance problems--computer time is cheap; debugging time, less so.

See \url{https://forums.seas.harvard.edu/cs51/viewtopic.php?f=14\&t=163} for details.

\underline{{\bf The Easy Way}}: Create a new file, copy/paste all of your code from within the module into the new file with specific definitions for the types (e.g., "type key = int;; type value = int;;") and then test using that file. Since the definitions will all be in their own functions, you can load each function individually into the top-level and test them interactively that way.
\begin{itemize}
\item CAUTIONS: There are very serious disadvantages to testing this way. First of all, your tests are only valid for this {\bf particular} implementation of dicts - as soon as you change the implementations of your functions, your tests will not apply. Using the right approach, you can write unit tests from within the module system that don't depend on how dicts are implemented. This makes development much easier, since changes that don't break the unit tests will generally not need to be tested (much) further. In addition, if you test this way, you will suddenly be maintaining two different versions of the code, and need to be careful to synchronize any changes you make between the two.
\end{itemize}

\subsection{Grading}

For the purposes of this problem set, however you choose to test your functions will be okay. No part of your grade explicitly depends on your testing, and thus we will not be deducting points based on which route you take. That said, however you test, you should definitely test extensively, since this assignment is much more complicated than past assignments. 

\section{Errors in Sedgewick's code}

Sadly, there are several of these. Sedgewick is handy with an algorithm, but not overly detail-oriented.

\begin{itemize}
\item Root node being black: Various versions of Sedgewick's code don't make it clear that he enforces the invariant of the root node always being black. See the section notes' Java version of insert for an illustration of how this invariant can be preserved without major changes to the code. (Section notes: \url{http://www.seas.harvard.edu/courses/cs51/docs/section3.ml}.) However, be sure to see the next item...
\item Placement of colorFlip: Unfortunately, the section notes (read: Gideon) took the wrong version of insert from Sedgewick's slides. (For the curious, I copied the version of insert corresponding to 2-3-4 trees; however, Sedgewick's delete is for 2-3 trees, so we need to use the version of insert that operates on 2-3 trees as well.) The fix is very simple - just move the if statement that does colorFlipping to the very end of the code, just before the return statement, instead of where we have it close to the top of the function. This corresponds to Sedgewick's insert where the flag "species == BU23" is true. See \url{https://forums.seas.harvard.edu/cs51/viewtopic.php?f=14&t=142}, among other threads, for a discussion of this. (And a great diagram posted by mxhe - how did you do that, anyway?)
\item Sedgewick's delete implementation does not fail gracefully if the node is not present in the tree; yours should. This can easily be fixed by appropriately preceding your delete code by a call to {\it member}.
\item Delete also has a couple of typo-bugs. There should be "int cmp = key.compareTo(h.key);" after "if (isRed(h.left)) h = leanRight(h);". For that matter, leanRight should be rotateRight.
\end{itemize}

\section{Miscellany and dictionary/set fold}

\begin{itemize}
\item Recursivity: Interfaces don't specify whether a function should be recursive or not. Though the "rec" may seem to be a part of the function signature, it is really an implementation detail, and therefore not relevant to the signature. Thus, you may feel free to add or delete "rec" from the signatures of any functions that you are asked to write or modify.
\item Dictionaty/set fold: this function caused a lot of confusion. We're okay with that; it was intended to get you thinking. But, a couple of hints:
\begin{itemize}
\item The fold is part of the interface for sets/dictionaries, so it processes one element/(key,value) pair at a time.  Your {\em implementation} has to do implement this using your tree.  
\item The fold takes one element at a time. It combines that element (a key/value pair) with the current accumulator and returns the new accumulator. (The initial accumulator is the base case passed to the function originally.)  Thus, you cannot fold up the left tree, fold up the right tree, and then combine them both with the current node at the same time like the binary tree fold did on PS3. You must instead find a way to build up the result by only processing one node at once.
\item Begin by writing out the types of the various calls you're thinking of making. This might guide your thinking.
\item It doesn't matter what order you process the elements of the tree in, since we didn't specify. The three most common ways of doing this are inorder, preorder, and postorder. (You may want to Wiki these terms if you are stuck.)
\item Think about the base case argument to fold as an accumulator that you are updating as you move through the tree.
\end{itemize}

\end{itemize}

\section{Conclusion}

Your first step when you get stuck or confused should always be to check the BB. My fellow TFs, not to mention Professor Morrisett, have been extremely good at responding to questions promptly, and you can usually save yourself a lot of pain by reading the explanations that are already up there.

Your second step, if your question is not already answered on the BB, should be to make a new BB thread about it. This will result in your question being answered promptly and with positive externalities for the other students who end up stuck on the same thing as you.

If you are stymied because you can't post your code on the BB, or you want more in-depth help, go to office hours. There are no more office hours for Part 1 of Moogle, but there will be next week for Part 2.

Thank you all for your patience as we work hard to create fun new assignments for CS51! Your feedback is always appreciated, so shoot the course staff an email if you have particular suggestions for how to improve the course, either for this year or for next year.

\end{document}