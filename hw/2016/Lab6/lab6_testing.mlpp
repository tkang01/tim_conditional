(*** CS 51 Lab 6 ***)
(*** February 16 2015 ***)
(*** Test Suite - Lab 6 ***)

#ifdef SOLN
module L = Lab6_soln
#else
module L = Lab6
#endif

open Testing ;;

#include "testing.ml.include"

open L ;;

let suite_lab6 = Testing.create_suite "lab6" ;;

Testing.add_test suite_lab6 "submission" (lazy true);;

let tester = Testing.add_test suite_lab6 ~points:0 ;;

# 0 "Part 1 - basic_streams"
	
open L.LazyStream ;;

tester "twos" (lazy (first 5 twos = [2; 2; 2; 2; 2]));
tester "threes" (lazy (first 5 threes = [3; 3; 3; 3; 3]));
tester "nats" (lazy (first 5 nats = [0; 1; 2; 3; 4]));

# 0 "Part 1 - evens_odds"

tester "evens" (lazy (first 5 evens = [0; 2; 4; 6; 8]));;
tester "odds" (lazy (first 5 odds = [1; 3; 5; 7; 9]));;

# 0 "Part 1 - sfilter"

let div3 x = x mod 3 = 0;;

tester "sfilter mod3" (lazy (first 4 (sfilter div3 nats) = [0; 3; 6; 9]));;
tester "evens2" (lazy (first 5 evens2 = [0; 2; 4; 6; 8]));;
tester "odds2" (lazy (first 5 odds2 = [1; 3; 5; 7; 9]));;

# 0 "Part 2 - sieve"

let s () = tail (tail nats);;
tester "sieve two_start" (lazy (first 4 (sieve (s())) = [2; 3; 5; 7]));;
tester "sieve three_start" (lazy (first 4 (sieve (tail (s()))) = [3; 4; 5; 7]));;

# 0 "Part 3 - native_module"

open L.NativeLazyStreams ;;

let both_even x y = x mod 2 = 0 && y mod 2 = 0;;
tester "smap double" 
	(lazy (first 5 (smap (fun x -> 2 * x) nats2) = [0; 2; 4; 6; 8]));;
tester "smap float_of_int" 
	(lazy (first 5 (smap float_of_int nats2) = [0.; 1.; 2.; 3.; 4.]));;
tester "smap2 add" (lazy (first 5 (smap2 (+) nats2 nats2) = [0; 2; 4; 6; 8]));;
tester "smap2 both_even" 
	(lazy (first 5 (smap2 both_even nats2 nats2) = 
		[true; false; true; false; true]));;
tester "sfilter even" 
	(lazy (first 5 (sfilter (fun x -> x mod 2 = 0) nats2) = [0; 2; 4; 6; 8]));;
tester "sfilter odds" 
	(lazy (first 5 (sfilter (fun x -> x mod 2 <> 0) nats2) = [1; 3; 5; 7; 9]));;

# 0 "Part 3 - lazy"

tester "nats2" (lazy (first 5 nats2 = [0; 1; 2; 3; 4]));;
let s () = tail (tail nats2);;
tester "sieve2 two_start" 
	(lazy (first 10 (sieve2 (s())) = [2; 3; 5; 7; 11; 13; 17; 19; 23; 29]));;
tester "sieve2 three_start" 
	(lazy (first 10 (sieve2 (tail (s()))) = 
	[3; 4; 5; 7; 11; 13; 17; 19; 23; 29]));;
tester "primes2" 
	(lazy (first 10 primes2 = [2; 3; 5; 7; 11; 13; 17; 19; 23; 29]));;
tester "nth nats2_0" (lazy (nth nats2 0 = 0));;
tester "nth nats2_50" (lazy (nth nats2 50 = 50));;
tester "nth primes2_5" (lazy (nth primes2 5 = 13));;
tester "nth primes2_2000" (lazy (nth primes2 1999 = 17389));;

(* Run the necessary reports *)
let _ = Testing.run_vocareum_report ~detailp:!dp ~gradep:!gp suite_lab6 ;;
