(* 
			      CS51 Pset 2
		          Spring 2017
			        TESTING
*)

#ifndef SOLN
module MF = Mapfold ;;
module EX = Expression ;;
module EL = ExpressionLibrary ;;
#else
module MF = Mapfold_soln ;;
module EX = Expression_soln ;;
module EL = ExpressionLibrary ;;
#endif

open Testing ;;
#include "testing.ml.include"

(* Set up the test suite *)
let suite_mapfold = Testing.create_suite "mapfold" ;;

(* #### PROBLEM 1 #### *)

(* Tests for Problem 1.1 *)
# 1 "negate_all"
Testing.add_test suite_mapfold "negate_all empty" (lazy (MF.negate_all [] = []));;
Testing.add_test suite_mapfold "negate_all pos" (lazy (MF.negate_all [1; 2] = [-1; -2]));;
Testing.add_test suite_mapfold "negate_all neg" (lazy (MF.negate_all [-1; -2] = [1; 2]));;
Testing.add_test suite_mapfold "negate_all both" (lazy (MF.negate_all [1; -2; 0] = [-1; 2; 0]));;

(* Tests for Problem 1.2 *)
# 1 "sum"
Testing.add_test suite_mapfold "sum empty" (lazy (MF.sum [] = 0));;
Testing.add_test suite_mapfold "sum singleton" (lazy (MF.sum [3] = 3));;
Testing.add_test suite_mapfold "sum multiple" (lazy (MF.sum [1; 2; 3; 4; 5] = 15));;
Testing.add_test suite_mapfold "sum negatives" (lazy (MF.sum [1; -2; 3; -4; 5] = 3));;

(* Tests for Problem 1.3 *)
# 1 "sum_row"
Testing.add_test suite_mapfold "sum_rows empty" (lazy (MF.sum_rows [] = []));;
Testing.add_test suite_mapfold "sum_rows singleton" (lazy (MF.sum_rows [[-1; 2; 3; 4; 5]] = [13]));;
Testing.add_test suite_mapfold "sum_rows equal lengths" (lazy (MF.sum_rows [[-1; 2]; [3; 4]] = [1; 7]));;
Testing.add_test suite_mapfold "sum_rows different lengths" (lazy (MF.sum_rows [[1; 2]; [-3; 4; 5]] = [3; 6]));;

(* Tests for Problem 1.4 *)
# 1 "filter_odd"
Testing.add_test suite_mapfold "filter_odd empty" (lazy (MF.filter_odd [] = []));;
Testing.add_test suite_mapfold "filter_odd odd" (lazy (MF.filter_odd [1] = [1]));;
Testing.add_test suite_mapfold "filter_odd even" (lazy (MF.filter_odd [2] = []));;
Testing.add_test suite_mapfold "filter_odd mixed" (lazy (MF.filter_odd [1; 2; 3; 4; 5; 6] = [1; 3; 5]));;
Testing.add_test suite_mapfold "filter_odd neg" (lazy (MF.filter_odd [1; 4; 5; -3] = [1; 5; -3]));;

(* Tests for Problem 1.5 *)
# 1 "num_occurs"
Testing.add_test suite_mapfold "num_occurs some" (lazy (MF.num_occurs 3 [1; 2; 3; 3; 4; 5; 3; 6] = 3));;
Testing.add_test suite_mapfold "num_occurs none" (lazy (MF.num_occurs 8 [1; 2; 3; 4; 5; 6; 7] = 0));;
Testing.add_test suite_mapfold "num_occurs empty" (lazy (MF.num_occurs 4 [] = 0));;

(* Tests for Problem 1.6 *)
# 1 "super_sum"
Testing.add_test suite_mapfold "super_sum empty" (lazy (MF.super_sum [] = 0));;
Testing.add_test suite_mapfold "super_sum empty lists" (lazy (MF.super_sum [ []; [] ] = 0));;
Testing.add_test suite_mapfold "super_sum both lists" (lazy (MF.super_sum [ [1; 2]; [3; 4] ] = 10));;
Testing.add_test suite_mapfold "super_sum first empty" (lazy (MF.super_sum [ []; [-10; 15] ] = 5));;
Testing.add_test suite_mapfold "super_sum second empty" (lazy (MF.super_sum [ [10; 15]; [] ] = 25));;

(* Tests for Problem 1.7 *)
# 1 "filter_range"
Testing.add_test suite_mapfold "filter_range empty" (lazy (MF.filter_range [] (-10, 10) = []));;
Testing.add_test suite_mapfold "filter_range some" (lazy (MF.filter_range [1; 3; 4; 5; 2] (1, 3) = [1; 3; 2]));;
Testing.add_test suite_mapfold "filter_range all" (lazy (MF.filter_range [0; 5; 10] (0, 10) = [0; 5; 10]));;
Testing.add_test suite_mapfold "filter_range neg" (lazy (MF.filter_range [1; -2; 8; 7; 4] (0, 5) = [1; 4]));;

(* Tests for Problem 1.8 *)
# 1 "floats_of_ints"
Testing.add_test suite_mapfold "floats_of_ints empty" (lazy (MF.floats_of_ints [] = []));;
Testing.add_test suite_mapfold "floats_of_ints singleton" (lazy (MF.floats_of_ints [1] = [1.0]));;
Testing.add_test suite_mapfold "floats_of_ints misc" (lazy (MF.floats_of_ints [-1; 0; 1; 2] = [-1.0; 0.0; 1.0; 2.0]));;

(* Tests for Problem 1.9 *)
# 1 "log10s"
Testing.add_test suite_mapfold "log10s empty" (lazy (MF.log10s [] = []));;
Testing.add_test suite_mapfold "log10s zero" (lazy (MF.log10s [0.0] = [None]));;
Testing.add_test suite_mapfold "log10s neg" (lazy (MF.log10s [-1.0] = [None]));;
Testing.add_test suite_mapfold "log10s misc pos" (lazy (MF.log10s [1.0; 10.0; 0.1; 100.0] = [Some 0.0; Some 1.0; Some (-1.0); Some 2.0]));;

(* Tests for Problem 1.10 *)
# 1 "deoptionalize"
Testing.add_test suite_mapfold "deoptionalize empty" (lazy (MF.deoptionalize [] = []));;
Testing.add_test suite_mapfold "deoptionalize singleton none" (lazy (MF.deoptionalize [None] = []));;
Testing.add_test suite_mapfold "deoptionalize singleton some" (lazy (MF.deoptionalize [Some 3.14] = [3.14]));;
Testing.add_test suite_mapfold "deoptionalize misc" (lazy (MF.deoptionalize [Some 3.14; None; Some 1.3; Some (-1.0); None] = [3.14; 1.3; -1.0]));;

(* Tests for Problem 1.11 *)
# 1 "some_sum"
Testing.add_test suite_mapfold "some_sum empty" (lazy (MF.some_sum [] = 0));;
Testing.add_test suite_mapfold "some_sum singleton none" (lazy (MF.some_sum [None] = 0));;
Testing.add_test suite_mapfold "some_sum singleton some" (lazy (MF.some_sum [Some 4] = 4));;
Testing.add_test suite_mapfold "some_sum some" (lazy (MF.some_sum [Some 1; Some 2; Some 3; Some (-1)] = 5));;
Testing.add_test suite_mapfold "some_sum misc" (lazy (MF.some_sum [None; Some 1; None; Some (-1); Some 4; Some 5] = 9));;

(* Tests for Problem 1.12 *)
# 1 "mult_odds"
Testing.add_test suite_mapfold "mult_odds empty" (lazy (MF.mult_odds [] = 1));;
Testing.add_test suite_mapfold "mult_odds pos" (lazy (MF.mult_odds [1; 2; 3; 4; 5] = 15));;
Testing.add_test suite_mapfold "mult_odds neg" (lazy (MF.mult_odds [0; 1; -1; 2; -2; -3; -5] = -15));;
Testing.add_test suite_mapfold "mult_odds evens" (lazy (MF.mult_odds [0; 2; -2; 4; -4] = 1));;

(* Tests for Problem 1.13 *)
let lst = [ []; [0; 1; 2]; [0; 1]; [-1; 5]; [] ] ;;
# 1 "concat"
Testing.add_test suite_mapfold "concat empty" (lazy (MF.concat [[]] = []));;
Testing.add_test suite_mapfold "concat list of empty" (lazy (MF.concat [[]; []] = []));;
Testing.add_test suite_mapfold "concat empty and singleton" (lazy (MF.concat [[]; [1]] = [1]));;
Testing.add_test suite_mapfold "concat misc" (lazy (MF.concat lst = List.concat lst));;

(* Tests for Problem 1.14 *)
let students = [("Victor", 2018); ("Ariana", 2018); ("Sam", 2017)] ;;
# 1 "filter_by_year"
Testing.add_test suite_mapfold "filter_by_year empty 1" (lazy (MF.filter_by_year [] 2018 = []));;
Testing.add_test suite_mapfold "filter_by_year empty 2" (lazy (MF.filter_by_year [] 0 = []));;
Testing.add_test suite_mapfold "filter_by_year two" (lazy (MF.filter_by_year students 2018 = ["Victor"; "Ariana"]));;
Testing.add_test suite_mapfold "filter_by_year one" (lazy (MF.filter_by_year students 2017 = ["Sam"]));;
Testing.add_test suite_mapfold "filter_by_year none" (lazy (MF.filter_by_year students 2010 = []));;

(* Functions for testing expressions *)
let within epsilon a b =
  abs_float(a -. b) < epsilon ;;

let tolerance = within 0.0001 ;;

let rec ifold_left
    (a:'a -> 'b -> 'b) (f:int -> 'a) (z:'b) (lo:int) (hi:int) : 'b =
  if lo > hi then z else ifold_left a f (a (f lo) z) (lo+1) hi ;;

let check_at (e) (e') (x:float) =
  tolerance (EX.evaluate e x) (EX.evaluate e' x) ;;

let check (e) (e') =
  ifold_left (&&) (fun x -> check_at e e' (float x)) true (1) (10) ;;
		      
(* Set up the test suite *)
let suite_expression = Testing.create_suite "expression" ;;

(* Tests for Problem 2.1 *)
# 1 "contains_var"
Testing.add_test suite_expression "contains_var with level 0" (lazy (EX.contains_var (EL.parse "x") = true));;
Testing.add_test suite_expression "contains_var with level 1" (lazy (EX.contains_var (EL.parse "x^4") = true));;
Testing.add_test suite_expression "contains_var with level 2" (lazy (EX.contains_var (EL.parse "(3+x)^4") = true));;
Testing.add_test suite_expression "contains_var without level 0" (lazy (EX.contains_var (EL.parse "5") = false));;
Testing.add_test suite_expression "contains_var without level 1" (lazy (EX.contains_var (EL.parse "5^4") = false));;
Testing.add_test suite_expression "contains_var without level 2" (lazy (EX.contains_var (EL.parse "(3+5)^4") = false));;

(* Tests for Problem 2.2 *)
# 1 "evaluate"
Testing.add_test suite_expression "evaluate var" (lazy (tolerance (EX.evaluate (EL.parse "x") 3.) 3.));;
Testing.add_test suite_expression "evaluate plus" (lazy (tolerance (EX.evaluate (EL.parse "3+4") 2.) 7.));;
Testing.add_test suite_expression "evaluate div by zero" ~points:0 (lazy ((EX.evaluate (EL.parse "3/0") 2.) = infinity));;
Testing.add_test suite_expression "evaluate pow" (lazy (tolerance (EX.evaluate (EL.parse "x^4") 2.) 16.));;
Testing.add_test suite_expression "evaluate mult" (lazy (tolerance (EX.evaluate (EL.parse "x * x") 2.5) 6.25));;
Testing.add_test suite_expression "evaluate neg" (lazy (tolerance (EX.evaluate (EL.parse "~x") (-2.5)) 2.5));;
Testing.add_test suite_expression "evaluate sin pi" (lazy (tolerance (EX.evaluate (EL.parse "sin(x)") 3.14159) 0.));;
Testing.add_test suite_expression "evaluate cos 0" (lazy (tolerance (EX.evaluate (EL.parse "cos(x)") 0.) 1.));;
Testing.add_test suite_expression "evaluate cos pi" (lazy (tolerance (EX.evaluate (EL.parse "cos(x)") 3.14159) (-1.)));;
Testing.add_test suite_expression "evaluate ln" (lazy (tolerance (EX.evaluate (EL.parse "ln(x)") 1.) 0.));;
Testing.add_test suite_expression "evaluate embedded" (lazy (tolerance (EX.evaluate (EL.parse "(3 + ~7) * x") 5.) (-20.)));;

(* Tests for Problem 2.3 *)
# 1 "derivative"
Testing.add_test suite_expression "derivative constant" (lazy (check (EX.derivative (EL.parse "2")) (EL.parse "0")));;
Testing.add_test suite_expression "derivative linear" (lazy (check (EX.derivative (EL.parse "x")) (EL.parse "1")));;
Testing.add_test suite_expression "derivative power" (lazy (check (EX.derivative (EL.parse "x^3")) (EL.parse "3*x^2")));;
Testing.add_test suite_expression "derivative trig" (lazy (check (EX.derivative (EL.parse "sin(x)")) (EL.parse "cos(x)")));;
Testing.add_test suite_expression "derivative neg power" (lazy (check (EX.derivative (EL.parse "1/x")) (EL.parse "~1/(x^2)")));;
Testing.add_test suite_expression "derivative mixed 1" (lazy (check (EX.derivative (EL.parse "ln(x*cos(x))+47")) (EL.parse "1/x-sin(x)/cos(x)")));;
Testing.add_test suite_expression "derivative mixed 2" (lazy (check (EX.derivative (EL.parse "2*(x^(x))*(1+ln(x))")) (EL.parse "2*x^x*(1/x+(1+ln(x))^2)")));;
Testing.add_test suite_expression "derivative mixed 3" (lazy (check (EX.derivative (EL.parse "sin(x)^3+x*x-1/(5*x)")) (EL.parse "1/(5*x^2)+2*x+3*cos(x)*(sin(x))^2")));;

(* Code for evaluating expressions *)
let unop_denote (u:EL.unop) =
  match u with
    | Sin -> sin
    | Cos -> cos
    | Ln -> log
    | Neg -> (~-.)
;;

let binop_denote (b:EL.binop) =
  match b with
    | Add -> ( +. )
    | Sub -> ( -. )
    | Mul -> ( *. )
    | Div -> ( /. )
    | Pow -> ( ** )
;;

let rec evaluate (e:EL.expression) (x:float) : float =
  match e with
    | Num n -> n
    | Var -> x
    | EL.Unop (u,e1) -> unop_denote u (evaluate e1 x)
    | EL.Binop (b,e1,e2) -> binop_denote b (evaluate e1 x) (evaluate e2 x)
;;

(* Helper functions for checking *)
let check_zero (s:string) (c:float) =
  match (EX.find_zero (EL.parse s) c 0.0001 100000) with
    | Some x -> tolerance (evaluate (EL.parse s) x) 0.
    | None -> false ;;

let check_no_zero (s:string) (c:float) =
  match (EX.find_zero (EL.parse s) c 0.0001 100000) with
    | Some _ -> false
    | None -> true ;;

(* Tests for Problem 2.4 *)
# 1 "find_zero"
Testing.add_test suite_expression "find_zero constant" (lazy (check_no_zero "10." 5.));;
Testing.add_test suite_expression "find_zero linear" (lazy (check_zero "x^2" 5.));;
Testing.add_test suite_expression "find_zero quadratic" (lazy (check_zero "x+3" 5.));;
Testing.add_test suite_expression "find_zero sublinear" (lazy ((check_no_zero "x^(1/3)" 2.) || (check_zero "x^(1/3)" 2.)));;
Testing.add_test suite_expression "find_zero sinoidal" (lazy (check_zero "sin(x)" 3.));;
Testing.add_test suite_expression "find_zero logarithm" (lazy (check_zero "ln(x)" 2.));;
Testing.add_test suite_expression "find_zero logarithm shifted" (lazy (check_zero "ln(x)-3" 20.));;
Testing.add_test suite_expression "find_zero sinoidal shifted" (lazy (check_zero "2*sin(x)*cos(x)-0.5" 0.15));;
Testing.add_test suite_expression "find_zero sinoidal shifted by 2" (lazy (check_no_zero "sin(x)+2.0" 3.1415));;

(* Tests for Problem 2.5 *)
# 1 "deopt"
let deopt opt = 
	match opt with
	| None -> ""
	| Some a -> (EL.to_string_smart a)
;;

let check_zero_exact (s:string) (ans:string) =
	deopt (EX.find_zero_exact (EL.parse s)) = ans
;;

(* FAULTY UNIT TESTING FOR FIND_ZERO_EXACT *)
# 1 "find_zero_exact"
Testing.add_test suite_expression "find_zero_exact linear simple" (lazy (check_zero_exact "42*x+2" "~(2./42.)"));;
Testing.add_test suite_expression "find_zero_exact linear simple2" (lazy (check_zero_exact "42*x+6*x-3*x+96" "~(96./45.)"));;

(* Problem Set 2 - Unit Testing

This Problem Set contains two parts: Mapfold and Expression. The unit tests for each portion are pretty extensive.

The extra credit problem has faulty unit testing. Expressions such as "~(2./42.)" and "~(1./21.)" are considered as not equivalent, as of now. This ought to be fixed in the future.**
*)

(* Run the necessary reports *)
let id = Testing.run_vocareum_report ~detailp:!dp ~gradep:!gp suite_mapfold in
    Testing.run_vocareum_report ~detailp:!dp ~gradep:!gp ~idoption:id suite_expression ;;