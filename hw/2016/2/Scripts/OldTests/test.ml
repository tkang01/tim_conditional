
(* HINT: This function should prove useful! *)
let rec reduce (f:'a -> 'b -> 'b) (u:'b) (xs:'a list) : 'b =
  match xs with
    | [] -> u
    | hd::tl -> f hd (reduce f u tl) ;;

let rec zip xs ys =
  match (xs,ys) with
    | ([],[]) -> Some []
    | (xhd::xtl, yhd::ytl) -> (match zip xtl ytl with
				 | Some tl -> Some ((xhd,yhd)::tl)
				 | None -> None)
    | _ -> None
;;

(*>* Problem *>*)

(*  sum : Returns the sum of the elements in the list using reduce! *)
let sum (nums:float list) : float =
  reduce (+.) 0.0 nums
;;

(*>* Problem *>*)

let average (nums:float list) : float =
  sum nums /. float_of_int (List.length nums)
;;

(*>* Problem *>*)

let weighted_average (grades_list:float list list) : float =
  average (List.map average grades_list)
;;

(*>* Problem *>*)

let minimum (xs:'a list) : 'a option =
  match xs with
    | [] -> None
    | hd::tl -> Some (reduce min hd tl)
;;

(*>* Problem *>*)

let rec remove_first elem lst =
  match lst with
    | [] -> []
    | hd::tl -> if hd = elem then tl else hd :: remove_first elem tl
;;

let drop_lowest grades =
  match minimum grades with
    | Some g -> remove_first g grades
    | None -> []
;;

let drop_lowest_grades (grades_list:float list list) : float list list =
  List.map drop_lowest grades_list
;;

(*>* Problem *>*)

let dot_product (a:float list) (b:float list) : float option =
  match zip a b with
    | Some lst -> Some (sum (List.map (fun (x,y) -> x *. y) lst))
    | None -> None
;;


(*>* Problem *>*)

(* the student's name and year *)
type name = string
type year = int
type student = name * year

(*  class_of : returns the names of the students in a given year
 *   Example : let students = [("Joe",2010);("Bob",2010);("Tom",2013)];;
 *               class_of 2010 students => ["Joe","Bob"] *)
let class_of (yr:year) (slist:student list) : name list =
  List.map fst (List.filter (fun (_,year) -> year = yr) slist)
;;


