\documentclass{amsart}

\input{../LaTeX/cs51-preamble}
\include{graphics}

\title[CS51 Problem Set 2]{CS51 Problem Set 2:\\Higher-Order
  Functional Programming and \\ Symbolic Differentiation}
%\author{Stuart M. Shieber}
\date{\today}

\begin{document}

\maketitle
\linenumbers

\textbf{This problem set is due February 19, 2016 at 5:00pm EST.}

\setcounter{section}{-1}
\section{Introduction}

This assignment focuses on programming in the functional programming
paradigm, with special attention to the idiomatic use of higher-order
functions like \code{map}, \code{fold}, and \code{filter}. In doing
so, you will exercise important features of functional languages, such
as recursion, pattern matching, and list processing, and you will gain
the ability to design your own mini-language -- a mathematical
expression language over which you'll compute derivatives
symbolically.

\subsection{Setup}

To download the problem set, follow the instructions found \href{http://tiny.cc/cs51reference}{here}.

For this assignment, you will be working in two different files:
%
\begin{itemize}
\item \code{mapfold.ml}: A self-contained series of exercises
  involving higher order functions.
\item \code{expression.ml}: A number of functions that will allow you
  to perform symbolic differentiation of algebraic
  expressions. Support code can be found in \code{ast.ml} and
  \code{expressionLibrary.ml}.
\end{itemize}

\subsection{Reminders}

\begin{description}
\item[Compilation Errors.] In order to submit your work to Vocareum, \textbf{your solution
  must compile against our test suite.} The system will reject submissions that do not compile.
  If there are problems that you are unable to solve, you must still write a function
  that matches the expected type signature, or your code will not compile. When we provide stub code, 
  that code will compile to begin with. If you are having difficulty
  getting your code to compile, please visit office hours or post on Piazza. {\em \bf Emailing your homework to your TF or the Head TFs is \textbf{not} a valid substitute for submitting on Vocareum. Please start early, and submit frequently, to ensure that you are able to submit before the deadline.}
\item[Testing is required] As with last week, we ask that you
  explicitly add tests to your code in the files
  \code{mapfold_tests.ml} and \code{expression_tests.ml}. 
  
\item[Part 2] Part 2 may prove quite challenging, so we encourage you
  to start early and ask questions if you get stuck.

\item[Time Estimates]
  Please estimate how much time you spent on each part of the problem set
  by editing the line: 
  % 
  \begin{minted}{ocaml}
    let minutes_spent_on_part_X : int = _ ;;
  \end{minted}
  % 
  at the bottom of each file.
  
\item[Style]
  Style is one of the most important aspects of this problem set and
  all problem sets in CS51. As such, \emph{style will be a significant
    portion of your grade.} Please
  refer to the \href{http://tiny.cc/cs51styleguide}{CS51 Style Guide}
  to ensure the quality of your code.

\end{description}

As in the previous assignment, the following must be true for you to
receive full credit and prevent delays in grading:
%
\begin{itemize}
\item Your code compiles without warnings.
\item Your code adheres to the \href{http://tiny.cc/cs51styleguide}{CS51 Style Guide}.
\item You have written one test per code path per function.
\item Your functions have the correct names and number of arguments. Do not
  change the signature of a function that we have provided.
\end{itemize}

\section{Compilation with Make}

In your last problem set, we provided a Makefile that allowed you 
to compile your code without directly calling \code{ocamlbuild}, the 
compiler that takes your \code{.ml} files and turns them into executable
\code{.byte} files.

For this problem set, a Makefile might again prove helpful in compiling 
your code, especially if you don't want to compile it all at once! This 
time, you will be given the opportunity to write a Makefile yourself.  
Take a look \href{http://tiny.cc/cs51reference}{here} for instructions on how to create a Makefile!

\section{Higher Order Functional Programming (\code{mapfold.ml})}

Mapping, folding, and filtering are important techniques in functional
languages that allow the programmer to abstract out the details of
traversing a list. Each can be used to accomplish a great deal with
very little code. In this part, you will create a number of functions
using the higher-order functions \code{map}, \code{filter}, and
\code{fold}.

In OCaml, these functions are available as \code{List.map},
\code{List.filter}, \code{List.fold_right}, and \code{List.fold_left}
in
\href{http://caml.inria.fr/pub/docs/manual-ocaml/libref/List.html}{the
  \code{List} module}.

The file \code{mapfold.ml} contains starter code for a set of
functions that operate on lists. For each one, you are to provide the implementation of that
function using the higher-order functions directly. The point is to
use the higher-order functional programming paradigm idiomatically.
The problems will be graded accordingly: a solution, even a working
one, that does not use the higher-order functional paradigm, deploying
these higher-order functions properly, will receive little or no
credit. For instance, solutions that require you to change the
\code{let} to a \code{let
  rec} show that you haven't assimilated the higher-order functional
paradigm. However, you should feel free to use one of the functions
you've defined to implement others.

Remember to provide unit tests for all of the functions in
\code{mapfold.ml} in a file \code{mapfold_tests.ml}. For this purpose,
you should write a function \code{test: unit -> unit} that executes a
series of tests using the \code{assert: bool -> unit} function. The
\code{assert} function simply returns \code{()} if its argument is
\code{True}, but if it is \code{False} it raises an exception. We have
included an example of its use in the starter code for
\code{mapfold_tests.ml}. You should provide at least one test per code
path (that is, every match case and if branch) for every function that
you write on this problem set.

\section{A Language for Symbolic Mathematics (\code{expression.ml})}

In the summer of 1958, John McCarthy (recipient of the Turing Award in
1971) made a major contribution to the field of programming languages.
With the objective of writing a program that performed symbolic
differentiation of algebraic expressions in an effective way, he
noticed that some features that would have helped him to accomplish
this task were absent in the programming languages of that time. This
led him to the invention of LISP (published in \emph{Communications of
  the ACM} in 1960) and other ideas, such as the concept of list
processing (from which Lisp derives its name), recursion, and garbage
collection, which are essential to modern programming languages.
Nowadays, symbolic differentiation of algebraic expressions is a task
that can be conveniently accomplished on modern mathematical packages,
such as Mathematica and Maple.

In this section, your mission is to build a language that can
differentiate and evaluate symbolically represented mathematical
expressions that are functions of a single variable. Symbolic
expressions consist of numbers, variables, and standard numeric
functions (addition, subtraction, multiplication, division,
exponentiation, negation, trigonometric functions, and so forth)
applied to them.

\subsection{Conceptual Overview}

We want to be able to manipulate symbolic expressions such as
$x^2 + sin(~x)$, so we'll need a way of representing these as data in
OCaml. For that purpose, we use OCaml types to define the appropriate
data structures. The \code{expression} data type allows for four
different kinds of expressions: numbers, variables, and unary and
binary operator expressions. For our purposes, only one variable (call
it $x$) is needed, and it will be represented by the \code{Var}
constructor for the \code{expression} type. Numbers are represented
with the \code{Num} constructor, which takes a single float argument
to specify which number is being denoted. Binary operator expressions,
in which a binary operator like addition or division is applied to two
subexpressions, is represented by the \code{Binop} constructor, and
similarly for unary operators like sine or negation, which take only a
single subexpression.

The \code{expression} data type can therefore be defined as follows
(and as provided in the file \code{ast.ml}).

\begin{minted}{ocaml}
(* Binary operators. *)
type binop = Add | Sub | Mul | Div | Pow ;;

(* Unary operators. *)
type unop = Sin | Cos | Ln | Neg ;;

type expression =
  | Num of float
  | Var
  | Binop of binop * expression * expression
  | Unop of unop * expression
;;
\end{minted}

You can think of the data objects of this \code{expression} type as
defining trees where nodes are the type constructors and the children
of each node are the specific operator to use and the arguments of
that constructor. Such trees are called \emph{abstract syntax
  trees} (or AST).

For instance, the expression $x^{\sim 2}$ would be represented by the
OCaml object \code{Binop(Pow, Var, Unop(Neg, Num(2)))}. (Although
numeric expressions frequently make use of parentheses, and sometimes
necessarily so, as in the case of the expression $(x+3)(x-1)$, the data
type definition has no provision for parenthesization. Why isn't that
needed? It might be helpful to think about how this example would be
represented.)

\subsection{Compiling and testing}

The code you will be editing is in \code{expression.ml}.

You can compile and test from the terminal command line. To use this method,
compile your code from the command command line using \code{ocamlbuild} or
\code{make}. Once you have done this, you can run the compiled code with
\code{./expression.byte}. 

You should provide a complete set of unit tests for your code in the
file \code{expression_tests.ml}, using \code{assert} to specify the
tests. As a helpful note, rather than testing that you get a
particular float value, you may want to \code{assert} that it is
within a small epsilon of the answer you expect. This is necessary to
avoid small differences due to the imprecision of \code{float}, and
can save you a headache down the road.

We have provided some functions to create and manipulate
\code{expression} values. \code{checkexp} is contained in
\code{expression.ml}. The others are contained in
\code{expressionLibrary.ml}. Here, we provide a brief description of
them and some example evaluations.

\begin{itemize}
\item \code{parse} : Translates a string in infix form (such as
  \code{"x\textasciicircum 2 + sin(\textasciitilde x)"}) into an
  \code{expression} (treating \code{"x"} as the variable). (The
  function uses ``\textasciitilde'' for unary negation rather than
  ``--'', to make it distinct from binary subtraction.) The
  \code{parse} function parses according to the standard order of
  operations -- so \code{"5+x*8"} will be read as \code{"5+(x*8)"}.

\begin{caml}
\?{parse("5+x*8") ;;}
\:{ - : expression = Binop (Add, Num 5., Binop (Mul, Var, Num 8.))}
\end{caml}

\item \code{to_string}: Returns a string representation of an
  expression in a readable form, using
  infix notation. This function adds parentheses around every binary
  operation so that the output is completely unambiguous.

\begin{caml}
\?{let exp = Binop(Add, Binop(Pow, Var, Num 2.0),}
\?{                Unop(Sin, Binop(Div, Var, Num 5.0))) ;;}
\:{val exp : expression =}
\:{  Binop (Add, Binop (Pow, Var, Num 2.), Unop (Sin, Binop (Div, Var, Num }
\:{5.)))}
\;\?{to_string exp ;;}
\:{- : string = "((x^2.)+(sin((x/5.))))"}
\end{caml}

\item \code{to_string_smart} : Returns a string representation of an
  expression in an even more
  readable form, only adding parentheses when there may be ambiguity.

\begin{caml}
\?{to_string_smart exp ;;}
\:{- : string = "x^2.+sin(x/5.)"}
\end{caml}

\item \code{rand_exp} : Takes a length $l$ and returns a
  randomly generated \code{expression} of length at most $2^l$. Useful
  for generating expressions for debugging purposes.

\begin{caml}
\?{let () = Random.init 2 (* for consistency *) ;;}
\;\?{rand_exp 5 ;;}
\:{- : expression =}
\:{Binop (Mul, Unop (Ln, Num (-11.)), Binop (Sub, Var, Num (-17.)))}
\;\?{rand_exp 5 ;;}
\:{- : expression = Binop (Mul, Num 4., Var)}
\end{caml}

\item \code{rand_exp_str} : Takes a length $l$ and returns a string
  representation of length at most $2^l$.

\begin{caml}
\?{let () = Random.init 2 (* for consistency *) ;;}
\;\?{rand_exp_str 5 ;;}
\:{- : string = "ln(~11.)*(x-~17.)"}
\;\?{rand_exp_str 5 ;;}
\:{- : string = "4.*x"}
\end{caml}

\item \code{checkexp} : Takes a string and a value and
  prints the results of calling every function to be tested except
  \code{find_zero}.
\end{itemize}

\subsection{Simple expression manipulation}

Start by implementing two functions that perform simple expression
manipulation. The function \code{contains_var : expression -> bool}
returns \code{True} when its argument contains a variable (that is,
the constructor \code{Var}). The function \code{evaluate : expression
  -> float -> float} takes an expression and a numeric value for the
variable in the expression and returns the numerical evaluation of the
expression at that value.

\subsubsection{Symbolic differentation}

Next, we want to develop a function that takes an expression \code{e}
as its argument and returns an expression \code{e'} representing the
derivative of the expression with respect to \code{x}. This process is
referred to as symbolic differentiation.

When implementing this function, recall the chain rule from your calculus course:
%
\[ (f(g(x)))' = f'(g(x)) \cdot g'(x)\]
%
Using that, we can write the derivatives for the other functions in our
language, as shown in Figure~\ref{fig:calc}.

\begin{figure}[h]
\begin{align*}
(f(x)+g(x))' &= f'(x)+g'(x) \\
(f(x)-g(x))' &= f'(x) - g'(x) \\
(f(x) \cdot g(x))' &= f'(x) \cdot g(x) + f(x) \cdot g'(x) \\
\left( \frac{f(x)}{g(x)} \right)' &=
 \frac{(f'(x) \cdot g(x) - f(x) \cdot g'(x))}{g(x)^2} \\
(\sin f(x))' &= f'(x) \cdot \cos f(x) \\
(\cos f(x))' &= f'(x) \cdot {\textasciitilde \sin f(x)} \\
(\ln f(x))' &= \frac{f'(x)}{f(x)} \\
(f(x) ^ h)' &= h \cdot f'(x) \cdot f(x)^{h-1} && \text{where $h$ contains
                                               no variables}\\
(f(x) ^ {g(x)})' &= f(x)^{g(x)} \cdot \left(g'(x) \cdot \ln f(x) +
  \frac{f'(x) \cdot g(x)}{f(x)} \right) \\
(n)' &= 0 && \text{where $n$ is any constant} \\
(x)' &= 1
\end{align*}
\caption{Rules for taking derivatives for a variety of expression
  types.}
\label{fig:calc}
\end{figure}

We've provided two cases for calculating the derivative of
\(f(x) ^ {g(x)}\),
one for where \(g(x)\)
is an expression ($h$) that contains no variables, and one for the
general case. The first is a special case of the second, but it is
useful to treat them separately, because when the first case applies,
the second case produces unnecessarily complicated expressions.

Your task is to implement the \code{derivative} function whose type is
\code{expression -> expression}. The result of your function must be
correct, but need not be expressed in the simplest form. Take
advantage of this in order to keep the code in this part as short as
possible.

To make your task easier, we have provided an outline of the function
with many of the cases filled in. We have also provide a function,
\code{checkexp}, which checks parts 2.1-2.3 for a given input. The
portions of the function that require your attention currently read
 \code{raise (Failure ``Not implemented'')}.

\subsection{Problem 2.4: Zero Finding}
  

One application of the derivative of a function is to find zeros of a
function. One way to do so is
\href{http://en.wikipedia.org/wiki/Newton_method}{Newton's method}.
Your task is to implement the function \code{find_zero : expression ->
  float -> float -> int -> float option}. 

This function should take an expression, a starting guess for the zero,
a precision requirement, and a limit on the number of times to repeat
the process. It should return \code{None} if no zero was found within
the desired precision by the time the limit was reached, and
\code{Some r} if a zero was found at \code{r} within the desired
precision.  

If the expression that \code{find_zero} is operating on represents
$f(x)$ and the precision is $\epsilon$, we are asking you to find
a value $x$ such that $|f(x)| < \epsilon$, that is, the value that the
expression evaluates to at $x$ is ``within $\epsilon$ of $0$''. We are
\emph{not} requiring you to find an
$x$ such that $|x - x_0| < \epsilon$ for some $x_0$ for which $f(x_0) = 0$.

Note that there are cases where Newton's method will fail to produce a
zero, such as for the function $x^{1/3}$. You are not responsible for
finding a zero in those cases, but just for the correct implementation
of Newton's method.

\subsection{Problem 2.5: Symbolic Zero-Finding (Karma)}

If you find yourself with plenty of time on your hands after
completing the problem set to this point, feel free to try this extra
problem, which is completely optional and earns you no points.

The function you wrote above allows you to find the zero (or a zero)
of most functions that can be represented with our AST. This makes it
quite powerful. However, in addition to numeric solving like this,
Mathematica and many similar programs can perform symbolic algebra.
These programs can solve equations using techniques similar to those
you learned in middle and high school (as well as more advanced
techniques for more complex equations) to get exact, rather than
approximate answers. For example, given the expression representing
$3x-1$, your \code{find_zero} function might return something like
\code{0.33333}, depending on your value of $\epsilon$. The exact
solution, however is given by the expression \code{1/3}, and this
answer can be found by a program that solves equations symbolically.

Performing the symbolic manipulations on complex expressions necessary
to solve equations is quite difficult in general, and we do not expect
you to handle the general case. However, there is one type of
expression for which symbolic zero-finding is not so difficult. These
are \emph{expressions of degree one}, those that can be simplified to
the form $a \cdot x + b$, where the highest exponent of the variable
is $1$. You likely learned how to solve equations of the form
$a \cdot x + b = 0$ years ago, and can apply the same skills in
writing a program to solve these.

Write a function, \code{find_zero_exact} which will exactly
find the zero of degree one expressions. Your function should, given a
valid input expression that has a zero,
return \code{Some exp} where \code{exp} is an expression that contains
no variables, evaluates to the zero of the given expression, and is exact.
If the expression is not degree one or has no zero, it should return
\code{None}.

You need not return the simplest expression, though it could be
instructive to think about how to simplify results. For example,
\code{find_zero_exact (parse "3*x-1")} might return
\code{Binop (Div, Num 1., Num 3.)} or \code{Unop(Neg, Binop
  (Div, Num -1., Num 3.))} but should \emph{not}
return \code{Num 0.333333333} as this is not exact.

Note that degree-one expressions need not be as simple as $ax + b$.
Something like $5x - 3 + 2(x - 8)$ is also a degree-one expression,
since it can be simplified to $ax + b$ by distributing and
simplifying. You may want to think about how to handle these types of
expressions as well, and think more generally about how to determine
whether an expression is of degree one.

\emph{Hint:} You may want to start by writing a function that will
crawl over an expression and distribute any multiplications or divisions,
resulting in something of a form like $ax + b$ (or maybe $ax
+ bx + cx + d + e + f$ or similar).

\section{Submit!}

To submit the problem set, follow the instructions found \href{https://canvas.harvard.edu/courses/7591/files/?preview=1766004}{here}.
That's it for Problem Set 2!

\end{document}
