"01234" ^ "56789" ;;
#use "expression_soln.ml" ;;
parse("5+x*8") ;;
let exp = Binop(Add, Binop(Pow, Var, Num 2.0),
                Unop(Sin, Binop(Div, Var, Num 5.0))) ;;
to_string exp ;;
to_string_smart exp ;;
let () = Random.init 2 (* for consistency *) ;;
rand_exp 5 ;;
rand_exp 5 ;;
let () = Random.init 2 (* for consistency *) ;;
rand_exp_str 5 ;;
rand_exp_str 5 ;;
