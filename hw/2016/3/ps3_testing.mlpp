(* 
			      CS51 Pset 3
	          	  Spring 2017
			      TESTING
*)

#ifndef SOLN
module P = Ps3 ;;
#else
module P = Ps3_soln ;;
#endif

open Testing;;
#include "testing.ml.include"

open P ;;
let suite_ps3 = Testing.create_suite "ps3";;

(* Checks representation invariant *)
let repOK (b: P.bignum) : bool =
  let rec all_less_than_base coeffs =
    match coeffs with
      | [] -> true
      | h::t -> (h < P.base) && (h >= 0) && (all_less_than_base t) 
  in
  match b.coeffs with
    | [] -> not b.neg (* If 0, neg = false *)
    | 0::_ -> false (* No leading zeroes *)
    | coeffs -> all_less_than_base coeffs (* coeffs in range [0,P.base) *)
;;

(** Some helpful string functions **)
(* Splits a string into a list of its characters. *)
let rec explode (s: string) : char list =
  let len = String.length s in
  if len = 0 then []
  else (s.[0])::(explode (String.sub s 1 (len - 1)))

(* Condenses a list of characters into a string. *)
let rec implode (cs: char list) : string =
  match cs with
    | [] -> ""
    | c::t -> (String.make 1 c)^(implode t)

(** Other funtions you may find useful. *)
(* Returns the first n elements of list l (or the whole list if too short) *)
let rec take_first (l: 'a list) (n: int): 'a list =
  match l with
    | [] -> []
    | h::t -> if n <= 0 then [] else h::(take_first t (n - 1))

(* Returns a pair
 * (first n elements of lst, rest of elements of lst) *)
let rec split lst n =
  if n <= 0 then ([], lst)
  else match lst with
    | [] -> ([], [])
    | h::t -> let (lst1, lst2) = split t (n-1) in
        (h::lst1, lst2)

(* Removes zero coefficients from the beginning of the bignum representation *)
let rec stripzeroes (b: int list) : int list =
  match b with
    | 0::t -> stripzeroes t
    | _ -> b

(* Returns the floor of the base 10 log of an integer *)
let intlog (base: int): int =
  int_of_float (log10 (float_of_int base))
;;

let fromInt (n: int) : P.bignum =
  let rec make_coeffs (n: int) (r: int list) : int list =
    if n = 0 then r
    else make_coeffs (n / base) ((abs (n mod P.base))::r)
  in
  {neg = (n < 0); coeffs = make_coeffs n []} ;;
  
(* Converts a bignum to its string representation.
 * Returns a string beginning with ~ for negative integers. *)
let toString (b: P.bignum): string =
  let rec pad_with_zeroes_left (s: string) (len: int) =
    if (String.length s) >= len then s else
      "0"^(pad_with_zeroes_left s (len - 1))
  in
  let rec stripstrzeroes (s: string) (c: char) =
    if String.length s = 0 then "0" else
    if (String.get s 0) = '0' then 
      stripstrzeroes (String.sub s 1 ((String.length s) - 1)) c
    else s
  in
  let rec coeffs_to_string (coeffs: int list): string =
    match coeffs with
      | [] -> ""
      | h::t -> (pad_with_zeroes_left (string_of_int h) (intlog P.base))^
          (coeffs_to_string t)
  in
  let stripped = stripzeroes b.coeffs in
    if List.length stripped = 0 then "0" else
      let from_coeffs = stripstrzeroes (coeffs_to_string stripped) '0' in
        if b.neg then "~"^from_coeffs else from_coeffs
;;

(* Use to avoid maldefined fromString in their code. *)
let fromString (s: string): P.bignum =
  let rec fromString_rec (cs: char list) : int list =
    if cs = [] then [] else
    let (chars_to_convert, rest) = split cs (intlog P.base) in
    let string_to_convert = implode (List.rev chars_to_convert) in
      (int_of_string string_to_convert)::(fromString_rec rest)
  in
  match explode s with
    | [] -> fromInt 0
    | h::t -> if (h = '-')||(h = '~') then 
        {neg = true; coeffs = stripzeroes (List.rev (fromString_rec (List.rev t)))}
      else {neg = false; coeffs = stripzeroes (List.rev (fromString_rec (List.rev (h::t))))}
;;

(* Create test suite *)    
let suite_ps3 = Testing.create_suite "ps3" ;;

exception InvariantProblem ;;
  
let bignum_equal (value: P.bignum) (shouldbe: P.bignum) : bool =
  if repOK value
  then (value = shouldbe)
  else raise InvariantProblem ;;

# 1 "minutes spent test"
Testing.add_test suite_ps3 "minutes spent" (lazy (P.minutes_spent () >= 0)) ;;
  
# 1 "negate tests"
Testing.add_test suite_ps3 "negate neg"
     (lazy (bignum_equal (P.negate (fromString "~1")) (fromString "1")));;
Testing.add_test suite_ps3 "negate pos"
     (lazy (bignum_equal (P.negate (fromString "1")) (fromString "~1")));;
Testing.add_test suite_ps3 "negate zero"
     (lazy (bignum_equal (P.negate (fromString "0")) (fromString "0")));;

let equal_test (s1: string) (s2: string) (expected: bool) : bool = 
  let b1 = fromString s1 in
  let b2 = fromString s2 in
  P.equal b1 b2 = expected
;;

(* NB: ASSUMES INPUT BIGNUMS ARE POSITIVE *)
(* By default, does less tests, but tests greater if optional flag is set *)
let gl_test ?(greater=false) (s1: string) (s2: string) (exp: bool) : bool = 
  let b1 = fromString s1 in
  let b2 = fromString s2 in
  let test b1 b2 exp = 
    if greater then P.greater b1 b2 = exp
    else P.less b1 b2 = exp
  in
  if b1 = b2 then 
    test b1 b2 exp
  else
    (* avoid redundant tests *)
    test b1 b2 exp &&
    test b2 b1 (not exp) &&
    test (P.negate b2) b1 (not greater) &&
    test b2 (P.negate b1) greater &&
    test (P.negate b2) (P.negate b1) exp &&
    (* extra tests if the first number is non-zero *)
    (if not (b1.P.coeffs = []) then
      test (P.negate b1) b2 (not greater) &&
      test b1 (P.negate b2) greater &&
      test (P.negate b1) (P.negate b2) (not exp)
    else true)
;;

# 1 "equal tests"
Testing.add_test suite_ps3 "equal small" (lazy (equal_test "12345" "12345" true));;
Testing.add_test suite_ps3 "equal neg" (lazy (equal_test "1" "~1" false));;
Testing.add_test suite_ps3 "equal big neg" (lazy (equal_test "1073741824" "~1073741824" false));;
Testing.add_test suite_ps3 "equal big neg 2" (lazy (equal_test "100200300" "~100200300" false));;
Testing.add_test suite_ps3 "equal big neg 2" (lazy (equal_test "~100200300" "100200300" false));;
Testing.add_test suite_ps3 "equal big neg 2" (lazy (equal_test "100200300" "100200300" true));;

# 1 "less tests"
Testing.add_test suite_ps3 "less zeroes" (lazy (gl_test "0" "0" false));;
Testing.add_test suite_ps3 "less zero 1" (lazy (gl_test "0" "1" true));;
Testing.add_test suite_ps3 "less zero 2" (lazy (gl_test "0" "1000" true));;
Testing.add_test suite_ps3 "less zero 3" (lazy (gl_test "0" "1001" true));;

Testing.add_test suite_ps3 "less singledigit 1" (lazy (gl_test "5" "5" false));;
Testing.add_test suite_ps3 "less singledigit 2" (lazy (gl_test "5" "7" true));;
Testing.add_test suite_ps3 "less singledigit 3" (lazy (gl_test "5" "5005" true));;
Testing.add_test suite_ps3 "less singledigit 4" (lazy (gl_test "5" "4004" true));;
Testing.add_test suite_ps3 "less singledigit 5" (lazy (gl_test "5" "6006" true));;
Testing.add_test suite_ps3 "less singledigit 6" (lazy (gl_test "5" "4005" true));;
Testing.add_test suite_ps3 "less singledigit 7" (lazy (gl_test "5" "5004" true));;
Testing.add_test suite_ps3 "less singledigit 8" (lazy (gl_test "5" "5006" true));;
Testing.add_test suite_ps3 "less singledigit 9" (lazy (gl_test "5" "6005" true));;
Testing.add_test suite_ps3 "less singledigit 10" (lazy (gl_test "5" "4006" true));;
Testing.add_test suite_ps3 "less singledigit 11" (lazy (gl_test "5" "6004" true));;

Testing.add_test suite_ps3 "less twodigit 1" (lazy (gl_test "5005" "4004" false));;
Testing.add_test suite_ps3 "less twodigit 2" (lazy (gl_test "5005" "4005" false));;
Testing.add_test suite_ps3 "less twodigit 3" (lazy (gl_test "5005" "5006" true));;
Testing.add_test suite_ps3 "less twodigit 4" (lazy (gl_test "5005" "4006" false));;

# 1 "greater tests"  
Testing.add_test suite_ps3 "greater 1" (lazy (gl_test ~greater:true "5" "5" false));;
Testing.add_test suite_ps3 "greater 2" (lazy (gl_test ~greater:true "5" "5005" false));;
Testing.add_test suite_ps3 "greater 3" (lazy (gl_test ~greater:true "5005" "5005" false));;
Testing.add_test suite_ps3 "greater 4" (lazy (gl_test ~greater:true "5005" "5006" false));;

let fromInt_test (i: int) (s: string) : bool =
  let b = fromString s in
  bignum_equal (P.fromInt i) b
;;

let toInt_test (s: string) (expected: int option) =
  let i = P.toInt (fromString s) in
  match expected, i with
  | None, None -> true
  | Some x, Some y -> x = y
  | _, _ -> false
;;

# 1 "fromInt tests"  
Testing.add_test suite_ps3 "fromInt 1" (lazy (fromInt_test 0 "0"));;
Testing.add_test suite_ps3 "fromInt 2" (lazy (fromInt_test 123456789 "123456789"));;
Testing.add_test suite_ps3 "fromInt 3" (lazy (fromInt_test (-123456789) "~123456789"));;
Testing.add_test suite_ps3 "fromInt 4" (lazy (fromInt_test 10002 "10002"));;
Testing.add_test suite_ps3 "fromInt max_int" (lazy (fromInt_test max_int (string_of_int max_int)));;
Testing.add_test suite_ps3 "fromInt min_int" (lazy (fromInt_test min_int (string_of_int min_int)));;
# 1 "toInt tests"
Testing.add_test suite_ps3 "toInt 1" (lazy (toInt_test "0" (Some 0)));;
Testing.add_test suite_ps3 "toInt 2" (lazy (toInt_test "123456789" (Some 123456789)));;
Testing.add_test suite_ps3 "toInt 3" (lazy (toInt_test "~123456789" (Some (-123456789))));;
Testing.add_test suite_ps3 "toInt max_int" (lazy (toInt_test (string_of_int max_int) (Some max_int)));;
Testing.add_test suite_ps3 "toInt min_int" (lazy (toInt_test (string_of_int min_int) (Some min_int)));;
Testing.add_test suite_ps3 "toInt overflow" (lazy (toInt_test "1234567834569120239458" None));;
Testing.add_test suite_ps3 "toInt underflow" (lazy (toInt_test "~1234567834569120239458" None));;

let plus_test (s1:string) (s2: string) (ssum: string) (sdiff: string) : bool =
  let b1 = fromString s1 in
  let b2 = fromString s2 in
  let sum = fromString ssum in
  let diff = fromString sdiff in 
  bignum_equal (P.plus b1 b2) sum
  && bignum_equal (P.plus b2 b1) sum
  && bignum_equal (P.plus (P.negate b1) b2) (P.negate diff)
  && bignum_equal (P.plus (P.negate b2) b1) diff 
  && bignum_equal (P.plus b1 (P.negate b2)) diff
  && bignum_equal (P.plus b2 (P.negate b1)) (P.negate diff)
  && bignum_equal (P.plus (P.negate b1) (P.negate b2)) (P.negate sum) 
  && bignum_equal (P.plus (P.negate b2) (P.negate b1)) (P.negate sum) ;;
  
# 1 "plus tests"
Testing.add_test suite_ps3 "plus identity" (lazy (plus_test "0" "123" "123" "~123"));;
Testing.add_test suite_ps3 "plus inverse" (lazy (plus_test "123" "~123" "0" "246"));;
Testing.add_test suite_ps3 "plus same numbers 1" (lazy (plus_test "5" "5" "10" "0"));;
Testing.add_test suite_ps3 "plus same numbers 2" (lazy (plus_test "~100" "100" "0" "~200"));;
Testing.add_test suite_ps3 "plus same numbers 3" (lazy (plus_test "1001001" "~1001001" "0" "2002002"));;
Testing.add_test suite_ps3 "plus different numbers 1" (lazy (plus_test "3" "123" "126" "~120"));;
Testing.add_test suite_ps3 "plus different numbers 2" (lazy (plus_test "1232" "3" "1235" "1229"));;
Testing.add_test suite_ps3 "plus different numbers 3" (lazy (plus_test "42" "~17" "25" "59"));;
Testing.add_test suite_ps3 "plus different numbers 4" (lazy (plus_test "3003" "123123" "126126" "~120120"));;
Testing.add_test suite_ps3 "plus different numbers 5" (lazy (plus_test "1003002" "123123123" "124126125" "-122120121"));;
      
let times_test (s1: string) (s2: string) (sprod: string) : bool = 
  let b1 = fromString s1 in
  let b2 = fromString s2 in 
  let expected = fromString sprod in
  bignum_equal (P.times b1 b2) expected
  && bignum_equal (P.times b2 b1) expected
  && bignum_equal (P.times (P.negate b1) b2) (P.negate expected)
  && bignum_equal (P.times (P.negate b2) b1) (P.negate expected)
  && bignum_equal (P.times b1 (P.negate b2)) (P.negate expected)
  && bignum_equal (P.times b2 (P.negate b1)) (P.negate expected)
  && bignum_equal (P.times (P.negate b1) (P.negate b2)) expected
  && bignum_equal(P.times (P.negate b2) (P.negate b1)) expected ;; 

# 1 "times tests"
Testing.add_test suite_ps3 "times zero 1" (lazy (times_test "0" "0" "0"));;
Testing.add_test suite_ps3 "times zero 2" (lazy (times_test "0" "123" "0"));;
Testing.add_test suite_ps3 "times zero 3" (lazy (times_test "0" "1234" "0"));
Testing.add_test suite_ps3 "times identity 1" (lazy (times_test "1" "1" "1"));;
Testing.add_test suite_ps3 "times identity 2" (lazy (times_test "1" "123" "123"));;
Testing.add_test suite_ps3 "times identity 3" (lazy (times_test "1" "1234" "1234"));;
Testing.add_test suite_ps3 "times neg one 1" (lazy (times_test "~1" "1" "~1"));;
Testing.add_test suite_ps3 "times neg one 2" (lazy (times_test "~1" "123" "~123"));;
Testing.add_test suite_ps3 "times neg one 3" (lazy (times_test "~1" "1234" "~1234"));;
Testing.add_test suite_ps3 "times squares 1" (lazy (times_test "~5" "5" "~25"));;
Testing.add_test suite_ps3 "times squares 2" (lazy (times_test "1001" "1001" "1002001"));;
Testing.add_test suite_ps3 "times square 3" (lazy (times_test "987987" "~987987" "~976118312169"));;
Testing.add_test suite_ps3 "times neg squares 1" (lazy (times_test "~5" "~5" "25"));;
Testing.add_test suite_ps3 "times neg squares 2" (lazy (times_test "~1001" "~1001" "1002001"));;
Testing.add_test suite_ps3 "times neg square 3" (lazy (times_test "~987987" "~987987" "976118312169"));;
Testing.add_test suite_ps3 "times singles 1" (lazy (times_test "3" "123" "369"));;
Testing.add_test suite_ps3 "times singles 2" (lazy (times_test "3" "~1232" "~3696"));;
Testing.add_test suite_ps3 "times singles 3" (lazy (times_test "~3" "3122123" "~9366369"));;
Testing.add_test suite_ps3 "times singles 4" (lazy (times_test "~9" "~987987" "8891883"));;
Testing.add_test suite_ps3 "times singles 5" (lazy (times_test "9" "987987987" "8891891883")) ;;
Testing.add_test suite_ps3 "times thousands 1" (lazy (times_test "3003" "123123" "369738369"));;
Testing.add_test suite_ps3 "times thousands 2" (lazy (times_test "976999" "79987" "78147219013"));;
Testing.add_test suite_ps3 "times thousands 3" (lazy (times_test "~976976999" "79987987" "~78146423495311013"));;
Testing.add_test suite_ps3 "times big 1" (lazy (times_test "0" "123123123123123123123123123123123123" "0"));;
Testing.add_test suite_ps3 "times big 2" (lazy (times_test "1" "123123123123123123123123123123123123" "123123123123123123123123123123123123"));;
Testing.add_test suite_ps3 "times big 3" (lazy (times_test "~3002001002003002003" "123123123123123123123123123123123123" "~369615738985354600969969969969969969600354230984615369"));;
Testing.add_test suite_ps3 "times big 4" (lazy (times_test "777888999" "987987987987987987987987987987" "768544986999999999999999999999231455013"));;
Testing.add_test suite_ps3 "times big 5" (lazy (times_test "~999888777888999888777888999" "~987987987987987987987987987987" "987878101878321211435211653665678788564788345455231455013"));;

# 1 "encrypt/decrypt test"
Testing.add_test suite_ps3 "encryptDecrypt 1"  ~points:0 (lazy (
  let encryptDecrypt_test (s1: string) (s2: string) (s3: string) (s4: string) : bool =  
    let b1 = fromString s1 in
    let b2 = fromString s2 in 
    let b3 = fromString s3 in 
    let expected = fromString s4 in 
      bignum_equal (P.encryptDecryptBignum b1 b2 b3) expected in 
  encryptDecrypt_test "10000" "2" "293" "5849"));;

Testing.add_test suite_ps3 "encryptDecrypt 2" ~points:0 (lazy(
  let encryptDecrypt_test1 b1 b2 b3 b4= 
    bignum_equal (P.encryptDecryptBignum b1 b2 b3) b4 in 
  let (e,d,n) = P.generateKeyPair (P.fromInt 20) in 
  encryptDecrypt_test1 n d (P.encryptDecryptBignum n e (P.fromString "3")) (P.fromString "3")));;

Testing.add_test suite_ps3 "encryptDecrypt 3" ~points:0  (lazy(
  let encryptDecrypt_test1 b1 b2 b3 b4= 
    bignum_equal (P.encryptDecryptBignum b1 b2 b3) b4 in 
  let (a,b,c) = P.generateKeyPair (P.fromInt 17) in 
  encryptDecrypt_test1 c b (P.encryptDecryptBignum c a (P.fromString "142")) (P.fromString "142")));;  
Testing.add_test suite_ps3 "encryptDecrypt 3" ~points:0  (lazy(
  let encryptDecrypt_test1 b1 b2 b3 b4= 
    bignum_equal (P.encryptDecryptBignum b1 b2 b3) b4 in 
  let (a,b,c) = P.generateKeyPair (P.fromInt 17) in 
  encryptDecrypt_test1 c b (P.encryptDecryptBignum c a (P.fromString "142")) (P.fromString "142")));;

# 1 "decrypt & encrypt test"
Testing.add_test suite_ps3 "encryptDecrypt 4" ~points:0  (lazy(
  let encryptDecrypt_test2 b1 b2 b3 =
    P.decrypt b3 b2 (P.encrypt b3 b1 "This is CS51!") = "This is CS51!" in
  let (e,d,n) = P.generateKeyPair (P.fromInt 5) in 
  encryptDecrypt_test2 e d n))


let times_faster_test (s1: string) (s2: string) (sprod: string) : bool = 
  let b1 = fromString s1 in
  let b2 = fromString s2 in 
  let expected = fromString sprod in 
    bignum_equal (P.times_faster b1 b2) expected && 
    bignum_equal (P.times_faster b2 b1) expected && 
    bignum_equal (P.times_faster (P.negate b1) b2) (P.negate expected) && 
    bignum_equal (P.times_faster (P.negate b2) b1) (P.negate expected) && 
    bignum_equal (P.times_faster b1 (P.negate b2)) (P.negate expected) &&  
    bignum_equal (P.times_faster b2 (P.negate b1)) (P.negate expected) && 
    bignum_equal (P.times_faster (P.negate b1) (P.negate b2)) expected && 
    bignum_equal(P.times_faster (P.negate b2) (P.negate b1)) expected;; 

# 1 "times_faster test"
Testing.add_test suite_ps3 "times faster 1" ~points:0  (lazy (times_faster_test "100200300" "3" "300600900"));;
Testing.add_test suite_ps3 "times faster 2" ~points:0  (lazy (times_faster_test "100200300" "100200300" "10040100120090000"));;
Testing.add_test suite_ps3 "times faster 3" ~points:0  (lazy (times_faster_test "~293348" "~928349" "272329322452"));;
    
(* Run the necessary reports *)
Testing.run_vocareum_report ~detailp:!dp ~gradep:!gp suite_ps3 ;;