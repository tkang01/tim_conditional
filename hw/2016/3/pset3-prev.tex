\section{Bignums and RSA}

\subsection{CS51 Problem Set 3}

\subsubsection{Due Friday, February 27 at 5pm (Sunday, March 1 at
11:59pm for Extension).}

\begin{center}\rule{3in}{0.4pt}\end{center}

\subsubsection{Introduction}

In this assignment, you will be implementing the handling of large
numbers. OCaml's int type is only 32 bits, so we need to write our own
way to handle large numbers. You'll implement several operations on
these large numbers, including addition and multiplication. The
challenge problem, should you choose to accept it, will be to implement
part of RSA public key cryptography, the protocol that encrypts and
decrypts data sent between computers.

\subsubsection{Downloading}

To download Problem Set 3, navigate to the folder you created in PS0
(the folder that contains the folders ``0'', ``1'', and ``2''). Once
there, execute:

\begin{verbatim}
git pull git@code.seas.harvard.edu:cs51-2015/psets.git master
\end{verbatim}

This will download PS3 in a folder named ``3''.

\subsubsection{Reminders}

\begin{itemize}
\itemsep1pt\parskip0pt\parsep0pt
\item
  \textbf{Testing is required.} As with last week, we ask that you
  explicitly add tests to your code. For this purpose, Ocaml provides
  the function ``assert'' which takes a bool, does nothing if the bool
  is true, and throws an error if the bool is false. We have included
  examples in problem 1.1. We expect at least 1 test per cod path (every
  match case/if else case) for every function that you write. Please put
  the tests just below the function being tested.
\item
  \textbf{Compilation}

  \begin{itemize}
  \itemsep1pt\parskip0pt\parsep0pt
  \item
    \texttt{make} and \texttt{make all} compile the pset. These two
    commands are exactly the same, so it doesn't matter which one you
    use.
  \item
    \texttt{make check} checks your line widths and reports any lines
    that are too long.
  \end{itemize}
\item
  \textbf{Time Estimates.} Please estimate how much time you spent on
  each part of the problem set by editing the line:
  \texttt{let minutes\_spent : int = raise ImplementMe ;;} at the bottom
  of the file.
\item
  \textbf{Style.} Style is one of the most important aspects of this
  problem set. As such, \textbf{style will be a significant portion of
  your grade.} Please refer to the
  \href{http://sites.fas.harvard.edu/~cs51/style_guide.html}{CS51 Style
  Guide} to ensure the quality of your code.
\end{itemize}

As in the previous assignment, the following must be true for you to
receive full credit and prevent delays in grading:

\begin{itemize}
\itemsep1pt\parskip0pt\parsep0pt
\item
  Your code compiles without warnings.
\item
  Your code adheres to the CS51 Style Guide.
\item
  You have written one test per code path per function.
\item
  You have not changed any lines needed by our automated correctness
  grader.
\item
  Your functions have the correct names and number of arguments. Do not
  change the signature of a function that we have provided!
\end{itemize}

This assignment was adapted from CS312 at Cornell.

\subsubsection{Part 1: Big Numbers}

The native integer type in OCaml has a size of 31 bits, thus can
represent integers between −2\textsuperscript{30} = 1073741824 and
2\textsuperscript{30}~−~1 = 1073741823 (given respectively by the values
\texttt{min\_int} and \texttt{max\_int}). Some computations, however,
require integers that are larger than these; for instance, RSA public
key cryptography requires big integers. In this part of the problem set,
we will write code that allows OCaml to handle arbitrarily large
integers (well, as large as the memory allows).~ We define a new type to
represent bignums, along with the standard arithmetic operations
\texttt{plus}, \texttt{times}, comparison operators, and so on.

\paragraph{Bignum Implementation}

In this bignum implementation, an integer \emph{n} will be represented
as a list of integers, namely the coefficients of the expansion of
\emph{n} in some base. For example, suppose the base is 1000. Then the
number 123456789 can be written as:

\begin{quote}
(123 * 1000\textsuperscript{2}) + (456 * 1000\textsuperscript{1}) + (789
* 1000\textsuperscript{0}),
\end{quote}

which we will represent by the list \texttt{{[}123; 456; 789{]}}. Notice
that the least-significant coefficient appears last in the list.~

For another example, consider the number 12000000000000, which is
represented by the list \texttt{{[}12; 0; 0; 0; 0{]}}.

Here is the actual type definition for bignums:

\begin{quote}
\texttt{type bignum = \{neg : bool; coeffs   : int list\}}
\end{quote}

The \texttt{neg} field specifies whether the number is positive (if
\texttt{neg} is false) or negative (if \texttt{neg} is true). The
\texttt{coeffs} field is the list of coefficient in some base, where
each coefficient is between 0 and \texttt{base~-~1}, inclusive. Assuming
a base of 1000, to represent the integer 123456789, we would use:

\begin{quote}
\texttt{\{neg~=~false, coeffs~=~{[}123; 456; 789{]}\}}
\end{quote}

and to represent −9999 we would use:

\begin{quote}
\texttt{\{neg~=~true, coeffs~=~{[}9; 999{]}\}}
\end{quote}

In other words, the record
\texttt{\{neg = false, coeffs =   {[}}\emph{a}\textsubscript{\emph{n}}\texttt{;    }\emph{a}\textsubscript{\emph{n}-1}\texttt{;    }\emph{a}\textsubscript{\emph{n}-2}\texttt{;    }. . . \texttt{;    }\emph{a}\textsubscript{0}\texttt{{]}\}}
represents the integer \emph{a\textsubscript{n}} ×
\texttt{base}\emph{\textsuperscript{n}}
\texttt{ + }\emph{a}\textsubscript{\emph{n}-1} ×
\texttt{base}\textsuperscript{\emph{n}-1}
\texttt{ + }. . .\texttt{ + }\emph{a}\textsubscript{0}; an empty list
represents 0. If \texttt{neg = true}, the record represents the
corresponding negative integer. This defines the correspondance between
\texttt{bignum} and the integers.

The base used by your bignum implementation is defined at the top of the
file:

\begin{verbatim}
let base = 1000
\end{verbatim}

To make it easier to implement some of the functions below, you may
assume that the base is 1000. However, you should avoid including the
number 1000 in your code, and instead refer to \texttt{base}. This is
good programming practice in general, and makes it easier to modify code
later. (In particular, if \texttt{base} is changed to a different value,
your code should still work.)

\paragraph{Using your solution}

Using the functions \texttt{fromString} and \texttt{toString}, you will
be able to test your functions by converting the result to strings and
printing them. Here is a sample interaction with a completed
implementation of bignums, in which we multiply 123456789 by 987654321:

\begin{verbatim}
# let ans = times (fromString "123456789") (fromString "987654321")
val ans : bignum = {neg = false; coeffs = [121; 932; 631; 112; 635; 269]}
# toString ans;;
- : string = "121932631112635269"
\end{verbatim}

The implementation of big numbers will be simpler if we impose a
representation invariant. A representation invariant is a statement that
you enforce about values in your representation (and that you assume is
true when writing functions to handle these values.) For example, if we
design a data type to hold names, we might impose the representation
invariant that all of the characters stored are letters or spaces. If
the representation invariant is broken, the value is not a valid value
for your type. Any function you write that produces a value of the type
(e.g. \texttt{fromInt}) should produce a value satisfying the
representation invariant.

The invariant for big numbers will be as follows:

\begin{itemize}
\itemsep1pt\parskip0pt\parsep0pt
\item
  Zero will be represented as \texttt{\{neg = false; coeffs = {[}{]}\}}.
  It should never be considered negative.
\item
  There will be no leading zeroes on the list of coefficients. An
  example of this would be a big number of the form:
  \texttt{\{neg = false; coeffs = {[}0;0;125{]}\}.}
\item
  Coefficients are never negative and are always less than
  \texttt{base}.
\end{itemize}

Functions that consume big numbers should assume that they satisfy the
invariant. We will not test your code using big numbers that violate the
invariant.

Be sure that your functions preserve this invariant! In other words,
your functions should never return a big number representing zero with
the neg flag set to true or a big number with a coefficients list with
leading zeros. \\

\subsubsection{What you need to do}

Implement the following functions defined in ps3.ml which operate on big
numbers. We have provided~the conversion function \texttt{fromString}
~that might prove useful during testing. Feel free to change functions
to be recursive if you think it would help but do not alter the function
signature!

\textbf{Problem 1.1}

Implement the function \texttt{negate}, which gives the negation of a
bignum (the bignum times -1).
\\\texttt{ negate : bignum -\textgreater{} bignum}

\textbf{Problem 1.2}

Implement the functions \texttt{equal}, \texttt{less}, and
\texttt{greater} that compare two numbers \texttt{b1} and \texttt{b2}
and return a boolean indicating whether \texttt{b1} is equal to (for the
function \texttt{equal}) or less than (for \texttt{less}) \texttt{b2}.
Assume that the arguments satisfy the representation invariant.\\
\\\texttt{equal : bignum -\textgreater{} bignum -\textgreater{} bool}\\
\texttt{less : bignum -\textgreater{} bignum -\textgreater{} bool}\\
\texttt{greater : bignum -\textgreater{} bignum -\textgreater{} bool}\\

\textbf{Problem 1.3}

Implement conversion functions between integers and big numbers.
\texttt{toInt} should return None if the number is too large to fit in
an OCaml integer.\\ ~\\
\texttt{toInt : bignum -\textgreater{} int option}\\
\texttt{fromInt : int -\textgreater{} bignum}\\ \texttt{~ }

\textbf{Problem 1.4}

We have provided you with a function, \texttt{plus\_pos}, that adds two
bignums and provides the result as a bignum. However, it has a problem:
this function only works if the resulting sum is positive (this might
not be the case, for example if b2 is negative and larger in absolute
value than b1.) Write the function \texttt{plus}, which can correctly
add arbitrary bignums. It should call \texttt{plus\_pos} and shouldn't
be too complex. \emph{Hints:} How can you use the functions you've
written so far to check, without doing the addition, whether the
resulting sum will be negative? Also remember that a + b = −(−a + −b).
\\
\texttt{plus : bignum -\textgreater{} bignum -\textgreater{} bignum}\\
One hint on your tests for this problem: you'd better test a case where
the result comes out negative!

\textbf{Problem 1.5}

Implement the function \texttt{times}, which multiplies two bignums. Use
the traditional algorithm you learned in grade school, but remember that
we are counting in base 1000, not 10. The main goal is correctness, so
keep your code simple. Make sure your code works with positive numbers,
negative numbers, and zero. Assume that the arguments satisfy the rep
invariant. \emph{Hint:} You may want to write a helper function that
multiplies a bignum by a single int (which might be one coefficient of a
bignum).

The function \texttt{divmod}(\emph{m},\emph{n}), which we have provided,
yields a pair (\emph{q},\emph{r}) consisting of the quotient \emph{q}
and the reminder \emph{r}: \emph{m} = \emph{n} \emph{q} + \emph{r}, and
\emph{r} is between 0 and \emph{n}-1.~~\\

\paragraph{Challenge 1: The RSA Cryptosystem}

As on last week's problem set and several in the future, we are
providing an extra problem or two for those who would like an extra
challenge. These problems are extra credit, and will earn you relatively
few points relative to their difficulty. We encourage you to attempt
this problem once you have completed the rest of the problem set, but do
not feel you have to do it to get a good grade.

NOTE: This problem is a great way to verify that your previous functions
work correctly, since it uses almost all of them. Each of the three
parts of RSA can be implemented in approximately one line of code if you
use the right functions.

In a world that relies increasingly upon digital information, public key
encryption plays an important part in achieving private communication.
RSA is a popular public-key encryption system. It is based on the fact
that there are fast algorithms for exponentiation and for testing prime
numbers, but no known fast algorithms for factoring large numbers. In
this problem set you will implement a version of the RSA system. The
algorithms will you implement have both a deep mathematical foundation
and practical importance.\\ \\ Cryptographic systems typically use keys
for encryption and decryption. An encryption key is used to convert the
original message (the plaintext) to coded form (the ciphertext). A
corresponding decryption key is used to convert the ciphertext back to
the original plaintext.\\ \\ In traditional cryptographic systems, the
same key is used for both encryption and decryption. Two parties can
exchange coded messages only if they share a secret key. Since anyone
who learned that key would be able to decode the messages, keys must be
carefully guarded and transmitted only under tight security: for
example, couriers handcuffed to locked, tamper-resistant briefcases!\\
\\ Diffie and Hellman (1976) discovered a new approach to encryption and
decryption: public key cryptography. In this approach, the encryption
and decryption keys are different. Knowing the encryption key cannot
help you find the decryption key. Thus you can publish your encryption
key on the web, and anyone who wants to send you a secret message can
use it to encode a message to send to you. You do not have to worry
about key security at all, for even if everyone in the world knew your
encryption key, no one could decrypt messages sent to you without
knowing your decryption key, which you keep private to yourself. You
used public-key encryption when you set up your CS51 git repositories:
the command \texttt{ssh-keygen} generated a public encryption key and
private decryption key for you. You uploaded the public key and
(hopefully) kept the private key to yourself.\\ \\ The RSA system, due
to Rivest, Shamir, and Adelman, is the best-known public-key
cryptosystem; the security of your web browsing probably depends on it.

\paragraph{How RSA Works}

RSA does not work with characters, but with integers. That is, it
encrypts numbers. To encrypt a piece of text, just combine the ASCII
codes of several characters into a big number, and then encrypt the
resulting number.

Suppose you want to obtain public and private keys in the RSA system.
You select two very large prime numbers \emph{p} and \emph{q}. (Recall
that a prime number is a positive integer greater than 1 with no
divisors other than itself and 1). You then compute numbers \emph{n} and
\emph{m}:

\begin{quote}
\emph{n} = \emph{pq} \\ \emph{m} = (\emph{p}−1)(\emph{q}−1)
\end{quote}

It turns out that

\begin{quote}
\begin{itemize}
\itemsep1pt\parskip0pt\parsep0pt
\item
  With very few exceptions, for almost all numbers \emph{e} less than
  \emph{n}, {[}\emph{em} = 1{]} mod \emph{n}
\item
  no one knows how to compute \emph{m}, \emph{p}, or \emph{q}
  efficiently, even knowing \emph{n}.
\end{itemize}
\end{quote}

(Notation: {[}\emph{a}=\emph{b}{]} mod \emph{n} means that (\emph{a} mod
\emph{n}) = (\emph{b} mod \emph{n}), where \emph{a} mod \emph{n} is the
remainder obtained when dividing \emph{a} by \emph{n} using ordinary
integer division. Equivalently, {[}\emph{a}=\emph{b}{]} mod \emph{n} if
\emph{a}−\emph{b} is divisible by \emph{n}. For example, {[}17 = 32{]}
mod 5.)\\ \\ Now you pick a number \emph{e} \textless{} \emph{m}
relatively prime to \emph{m}; that is, such that \emph{e} and \emph{m}
have no factors in common except 1. The significance of relative
primality is that \emph{e} is relatively prime to \emph{m} iff \emph{e}
is invertible mod \emph{m}; that is, iff there exists a \emph{d} such
that {[}\emph{de} = 1{]} mod \emph{m}. Moreover, it is possible to
compute \emph{d} from \emph{e} and \emph{m} using Euclid's algorithm,
described below. Your public key, which you can advertise to the world,
is the pair (\emph{n},\emph{e}). Your private key is
(\emph{n},\emph{d}).\\ \\ Anyone who wants to send you a secret message
\emph{s} (represented by an integer) encrypts it by computing E(s),
where\\ \\ ~~~ \emph{E}(\emph{s}) = \emph{s\textsuperscript{e}} mod
\emph{n}\\ \\ That is, if the plaintext is represented by the number
\emph{s} which is less than \emph{n}, the ciphertext \emph{E}(\emph{s})
is obtained by raising \emph{s} to the power \emph{e}, then taking the
remainder modulo \emph{n}.\\ \\ The decryption process is exactly the
same, except that \emph{d} is used instead of \emph{e}:\\ \\ ~~~
\emph{D}(\emph{s}) = \emph{s\textsuperscript{d}} mod \emph{n} \\ \\ The
operations \emph{E} and \emph{D} are inverses:\\ \\ ~~~
\emph{D}(\emph{E}(\emph{s}))~~~ =
(\emph{s\textsuperscript{e}})\textsuperscript{\emph{d}}mod \emph{n} \\
~~~~~~~~~~~~~~~~~~~ = \emph{s}\textsuperscript{\emph{de}} mod \emph{n}
\\ ~~~~~~~~~~~~~~~~~~~ = \emph{s}\textsuperscript{1+\emph{km}} mod
\emph{n} \\ ~~~~~~~~~~~~~~~~~~~ =
\emph{s}(\emph{s\textsuperscript{m}})\textsuperscript{\emph{k}} mod
\emph{n} \\ ~~~~~~~~~~~~~~~~~~~ = \emph{s}\textsuperscript{(1)\emph{k}}
mod \emph{n} \\ ~~~~~~~~~~~~~~~~~~~ = \emph{s} mod \emph{n}~~~\\
~~~~~~~~~~~~~~~~~~~ = \emph{s}\\ \\ The integer \emph{s} representing
the plaintext must be less than \emph{n}. That's why we break the
message up into chunks. Also, this only works if \emph{s} is relatively
prime to \emph{n}: it has no factors in common with \emph{n} other than
1. If n is the product of two large primes, then all but negligibly few
messages \emph{s} \textless{} \emph{n} satisfy this property. If by some
freak chance \emph{s} and \emph{n} turned out not to be relatively
prime, then the code would be broken; but the chances of this happening
by accident are insignificantly small. In summary, to use the RSA
system:\\ \\ ~~~ 1. pick large primes \emph{p} and \emph{q};\\ ~~~ 2.
compute \emph{n} = \emph{pq} and \emph{m} = (\emph{p}−1)(\emph{q}−1);\\
~~~ 3. choose \emph{e} relatively prime to \emph{m} and use this to
compute \emph{d} such that {[}\emph{de} = 1{]} mod \emph{m};\\ ~~~ 4.
publish the pair (\emph{n},\emph{e}) as your public key, but keep
\emph{d},\emph{p}, and \emph{q} secret.\\ \\ How secure is RSA? At
present, the only known way to obtain \emph{d} from \emph{e} and
\emph{n} is to factor \emph{n} into its prime factors \emph{p} and
\emph{q}, then compute \emph{m} and proceed as above. But despite
centuries of effort by number theorists, factoring large integers
efficiently is still an open problem. Until someone comes up with an
efficient way to factor numbers, or discovers some other way to compute
\emph{d} from \emph{e} and \emph{n}, the cryptosystem appears to be
secure for large numbers \emph{n}.

\paragraph{Your task: Implement RSA}

You have been given a partial implementation of the RSA algorithm. The
given code provides additional functions for computations on big
numbers; functions to generate prime numbers and code that generates key
pairs. We briefly discuss a few of these functions. You should call
these functions as needed in your code. They handle most of the
mathematical and cryptographic detail for you.

Function \texttt{expmod}(\emph{b, e, m}) computes
\emph{b\textsuperscript{e}} mod \emph{m}. Function
\texttt{euclid}(\emph{m,n}) yields numbers (\emph{s,t,g}) such that
s\emph{m + tn = g}, where \emph{g}is the greatest common divisor of
\emph{m}and \emph{n}.~

Function \texttt{isPrime} tests if a number is prime. This is a
probabilistic test: it yields true if there is a high probability that
\emph{n} is prime; and yields false if \emph{n} is definitely not prime.
It's worth mentioning that this function uses the well-known
Miller-Rabin primality test, partially named after Harvard Professor
Michael Rabin, who developed this variation of the algorithm.

The function \texttt{generateKeyPair r} generates an RSA key pair, and
returns a tuple (e, d, n). It picks random primes \emph{p} and \emph{q}
that are in the range from 2\textsuperscript{r} to
2*2\textsuperscript{r} so that \emph{n} = \emph{pq} will be in the range
2\textsuperscript{2r} to 2\textsuperscript{2r+2}.

You must add the encryption and decryption functions specified below.
Your functions encode an input list of bytes into an output list of
bytes. Your functions will operate on \emph{blocks of bytes} (or,
equivalently, blocks of chars, since a char takes 1 byte to store). Each
block consists of M bytes or (M-1) bytes, where M is the number of bytes
required to store the modulus \emph{n}. of the key (\emph{n,e}) or
(\emph{n,d}). You can use the function \texttt{bytesInKey} to compute
this from \texttt{n}

\textbf{Challenge 1.1}

Implement the function \texttt{encryptDecryptBignum}, which encrypts or
decrypts a bignum (note that, since the operations are so similar, as
described above, we can use the same function. The only difference is
that to encrypt, you pass e and to decrypt, you pass d.)

\textbf{Note:} For this problem and the one below, it's probably
difficult to write tests that check whether the result matches your
expected result. However, you can write tests that check whether a
bignum encrypted using RSA and then decrypted using the corresponding
decryption key results in the original bignum.

\textbf{Challenge 1.2}

You'll note that RSA acts on bignums, so this gives us no way to
directly encode strings. Since very few secret message consist of large
integers, we need a way to convert strings (or lists of bytes) into
bignums. You may also realize from the description of RSA that each
bignum must be less than the modulus n. We have given you the function
\texttt{charsToBignums} which transforms a list of chars to a list of
bignums, each of which is less than n. It does this by breaking the list
of chars into blocks of M bytes, where M is the number of bytes required
to store n. We've also provided the inverse transformation
\texttt{bignumsToChars}. You may also find the functions
\texttt{explode} and \texttt{implode} defined higher in the file useful.

Finally, we've provided the function \texttt{encDecBignumList} which
encrypts or decrypts a list of bignums. This simply calls your
\texttt{encryptDecryptBignum} repeatedly.

Your job is to write the functions \texttt{encrypt}, which takes n, e
and a string to encode, and returns the encoded message as a list of
bignums, and \texttt{decrypt}, which takes n, d and a list of bignums
provided by \texttt{encrypt}, decrypts the message and produces a
string. Note that these functions are doing more than just a direct
translation of chars to bignums and vice versa: we've given you those
functions. Your functions should actually encrypt these bignums with
RSA, so that nobody will be able to reconstruct the original bignums, or
the original message, without the decryption key. Not bad for your
second week of CS51!

To exchange secret messages with your friends, use
\texttt{generateKeyPair} to get an RSA keypair. Send the pair
\texttt{(n, e)} to your friend and have them encrypt a message using
this key and send it to you. You can then decrypt it using \texttt{n}
and \texttt{d}, but nobody else can because they don't have \texttt{d}!

\paragraph{Challenge 2: Multiply Faster!}

\textbf{Problem 3.1}

The multiplication algorithm you implemented in Part 1 will work just
fine for most integers of reasonable sizes, including the ones we will
be using in this assignment. However, some exceedingly smart people have
devised algorithms which, on very large numbers (think thousands of
digits), are considerably faster than the multiplication algorithm you
learned in grade school.

See if you can implement such an algorithm in \texttt{times\_faster}.
You may want to start by googling the Karatsuba algorithm. This
algorithm recursively multiplies smaller and smaller numbers. Note that,
when the numbers become small enough (2-4 digits), you can and probably
should simply call the \texttt{times} function you implemented earlier.
However, don't just call this function on any numbers you are given;
that's not any faster!

\subsubsection{Submit!}

Before submitting, it is \textbf{very important} that you compile your
code by running the \texttt{make} command while in the directory with
your ps3.ml file. Run it by calling \texttt{./ps3.native} in this same
directory. There should be no output from running the program. If you do
not see any output, your asserts passed and you are good to
go.Evaluating definitions in the toplevel, while useful during
development, \textbf{is not sufficient} for preparing your code for
submission.

To submit, commit all your changes and push them to code.seas by running
\texttt{git commit -am "commit message here"} followed by
\texttt{git push}. This will allow us to access your code from the
grading server.
