\documentclass{amsart}

\input{../LaTeX/cs51-preamble}
\include{graphics}

\title[CS51 Problem Set 3]{CS51 Problem Set 3:\\Bignums and RSA}
%\author{Stuart M. Shieber} \date{\today}

\begin{document}

\maketitle
\linenumbers

\textbf{This problem set is due February 26, 2016 at 5:00pm EST.}

\textbf{This problem set is {\em not} a partner problem set.}

\setcounter{section}{-1}
\section{Introduction}

In this assignment, you will be implementing the handling of large
integers. OCaml's \texttt{int} type is only 64 bits, so we need to
write our own way to handle very large numbers.  Arbitrary size
integers are traditionally referred to as
``\href{https://en.wikipedia.org/wiki/Arbitrary-precision_arithmetic}{bignums}''. You'll
implement several operations on bignums, including addition and
multiplication. The challenge problem, should you choose to accept it,
will be to implement part of RSA public key cryptography, the protocol
that encrypts and decrypts data sent between computers, which requires
bignums as the keys.

To download the problem set, follow the instructions found \href{http://tiny.cc/cs51reference}{here}.

\subsection{Reminders}

\begin{itemize}
\item \textbf{Compilation Errors.} In order to submit your work to Vocareum, \textbf{your solution
  must compile against our test suite.} The system will reject submissions that do not compile.
  If there are problems that you are unable to solve, you must still write a function
  that matches the expected type signature, or your code will not compile. When we provide stub code, 
  that code will compile to begin with. If you are having difficulty
  getting your code to compile, please visit office hours or post on Piazza. {\em \bf Emailing your homework to your TF or the Head TFs is \textbf{not} a valid substitute for submitting on Vocareum. Please start early, and submit frequently, to ensure that you are able to submit before the deadline.}
\item \textbf{Testing is required.} As with last week, we ask that you
  explicitly add tests to your code. For this purpose, Ocaml provides
  the function ``assert'' which takes a bool, does nothing if the bool
  is true, and throws an error if the bool is false. Please put the
  tests in a separate file \texttt{ps3_tests.ml}, a sample of which we
  have provided. The sample already includes a couple of tests to give
  you the idea. We expect at least one test per code path (every match
  case/if else case) for every function that you write.
\item \textbf{Compilation.}  Write a Makefile if you want one. Take a
  look at the instructions \href{http://tiny.cc/cs51reference}{here}
  if you're stuck.
\item \textbf{Time Estimates.} Please estimate how much time you spent
  on each part of the problem set by editing the line: \texttt{let
    minutes\_spent () = raise ImplementMe} at the bottom of the file.
\item \textbf{Style.} Style is one of the most important aspects of
  this and all future problem sets, and will be a significant portion
  of your grade. Please refer to the
  \href{http://tiny.cc/cs51styleguide}{CS51 Style Guide} to ensure the
  quality of your code.
\end{itemize}

As in the previous assignment, please make sure that:

\begin{itemize}
\item Your code compiles without warnings.
\item Your code compiles {\bf when submitted on Vocareum}.
\item Your code adheres to the CS51 Style Guide.
\item You have written one test per code path per function.
\item Your functions have the correct names and number of
  arguments. Do not change the signature of a function that we have
  provided.
\end{itemize}

(This assignment was adapted from CS312 at Cornell.)

\section{Big Numbers}

The byte integer type in OCaml has a size of 63 bits, and therefore
can represent integers between $-2^{62}$ and $2^{62}-1$ (given
respectively by the OCaml values \texttt{min\_int} and
\texttt{max\_int}).
%
\begin{caml}
\?{min_int, max_int ;;}
\:{- : int * int = (-4611686018427387904, 4611686018427387903)}
\end{caml}
%
Some computations, however, require integers that are larger than
these; for instance, RSA public key cryptography requires big
integers. In this part of the problem set, you will write code that
allows OCaml to handle arbitrarily large integers (as large as the
memory allows). We define a new algebraic data type to represent
bignums, along with the standard arithmetic operations \texttt{plus},
\texttt{times}, comparison operators, and so on.

\subsection{Bignum Implementation}

In this bignum implementation, an integer \emph{n} will be represented
as a list of integers, namely the coefficients of the expansion of
\emph{n} in some base. For example, suppose the base is 1000. Then the
number 123456789 can be written as:
%
\[(123 \cdot 1000^2) + (456 \cdot 1000^1) + (789 \cdot 1000^0)\eqpunc{,}\]
%
which we will represent by the list \verb.[123; 456; 789].. Notice
that the least-significant coefficient appears last in the list.  As
another example, the number 12000000000000 is represented by the list
\verb.[12; 0; 0; 0; 0]..

Here is the actual type definition for bignums:
%
\begin{minted}{ocaml}
type bignum = {neg : bool; coeffs : int list} ;;
\end{minted}
%
The \texttt{neg} field specifies whether the number is positive (if
\texttt{neg} is false) or negative (if \texttt{neg} is true). The
\texttt{coeffs} field is the list of coefficients in some base, where
each coefficient is between 0 and \texttt{base~-~1}, inclusive. Assuming
a base of 1000, to represent the integer 123456789, we would use:
%
\begin{minted}{ocaml}
{neg = false, coeffs = [123; 456; 789]}
\end{minted}
%
and to represent $-9999$ we would use:
%
\begin{minted}{ocaml}
{neg = true, coeffs = [9; 999]}
\end{minted}
%
In other words, the record
%
\begin{quote}\tt
\{neg = false, coeffs = [$a_n$; $a_{n-1}$; $a_{n-2}$; \ldots; $a_0$]\}
\end{quote}
%
represents the integer 
\[ a_n \cdot base^n + a_{n-1} \cdot base^{n-1} + a_{n-2} \cdot
base^{n-2} + \cdots + a_0 \]
%
An empty list represents 0. If \texttt{neg = true}, the record
represents the corresponding negative integer. This defines the
correspondance between \texttt{bignum} and the integers.

The base used by your bignum implementation is defined at the top of the
file:
%
\begin{minted}{ocaml}
let base = 1000 ;;
\end{minted}
%
To make it easier to implement some of the functions below, you may
want to use a base of 1000 while debugging and testing. However,
you'll want to make sure that the code works for any value of
\texttt{base}, always referring to that variable rather than
hard-coding a value of 1000, so that if \texttt{base} is changed to a
different value, your code should still work. This is good programming
practice in general, and makes it easier to modify code later.

\subsection{Using your solution}

Using the functions \texttt{fromString} and \texttt{toString}, you will
be able to test your functions by converting the result to strings and
printing them. Here is a sample interaction with a completed
implementation of bignums, in which we multiply 123456789 by 987654321:

\begin{caml}
\?{let ans = times (fromString "123456789") (fromString "987654321")}
\:{val ans : bignum = {neg = false; coeffs = [121; 932; 631; 112; 635; 269]}}
\?{toString ans;;}
\:{- : string = "121932631112635269"}
\end{caml}

The implementation of big numbers will be simpler if we impose certain
representation invariants. A {\bf representation invariant} is a statement
that you enforce about values in your representation and that you can
thus assume is true when writing functions to handle these
values. (For example, if we design a data type to hold person names,
we might impose the representation invariant that all of the
characters stored are letters or spaces.) If the representation
invariant is violated, the value is not a valid value for your
type. Any function you write that produces a value of the type (for
example, \texttt{fromInt}) should produce a value satisfying the
representation invariant.

The invariants for our bignum representation are as follows:
%
\begin{itemize}
\item
  Zero will always be represented as \texttt{\{neg = false; coeffs = {[}{]}\}}.
  It should never be represented as \texttt{\{neg = true; coeffs = {[}{]}\}}.
\item
  There will be no leading zeroes on the list of coefficients. The value
  \texttt{\{neg = false; coeffs = [0;0;125]\}} violates this invariant.
\item
  Coefficients are never negative and are always strictly less than
  \texttt{base}.
\end{itemize}
%
Functions that consume bignums should assume that they satisfy the
invariant. We will not test your code using bignums that violate the
invariant.

Be sure that your functions preserve this invariant. For example, your
functions should never return a bignum representing zero with the
\texttt{neg} flag set to \texttt{true} or a bignum with a coefficients
list with leading zeros.

\subsection{What you need to do}

Implement the following functions defined in \texttt{ps3.ml}, which
operate on bignums. We have provided a conversion function
\texttt{fromString}, which might prove useful during testing. Feel free
to change functions to be recursive if you think it would help but do
not alter the signatures of any functions, as we will be unit testing
assuming those signatures.

\begin{problem}
  Implement the function \texttt{negate : bignum -> bignum}, which
  gives the negation of a bignum (the bignum times -1).
\end{problem}

\begin{problem}
  Implement the functions \texttt{equal : bignum -> bignum -> bool},
  \texttt{less : bignum -> bignum -> bool}, and \texttt{greater :
    bignum -> bignum -> bool} that compare two numbers \texttt{b1} and
  \texttt{b2} and return a boolean indicating whether \texttt{b1} is
  equal to, less than, or greater than \texttt{b2}, respectively.
  Assume that the arguments satisfy the representation invariant.
\end{problem}

\begin{problem}
  Implement conversion functions \texttt{toInt : bignum -> int option}
  and \texttt{fromInt : int -> bignum} between integers and big
  numbers.  \texttt{toInt : bignum -> int option} should return
  \texttt{None} if the number is too large to fit in an OCaml integer.
\end{problem}

\begin{problem}
  We have provided you with a function, \texttt{plus\_pos : bignum ->
    bignum -> bignum}, which adds two bignums and provides the result
  as a bignum. However, it has a problem: this function only works if
  the resulting sum is positive. (This might not be the case, for
  example if \texttt{b2} is negative and larger in absolute value than
  \texttt{b1}.)  Write the function \texttt{plus : bignum -> bignum ->
    bignum}, which can correctly add arbitrary bignums. It should call
  \texttt{plus\_pos} and shouldn't be too complex. \emph{Hints:} How
  can you use the functions you've written so far to check, without
  doing the addition, whether the resulting sum will be negative? Also
  remember that $a + b = -(-a + -b)$.  And a hint on your tests for
  this problem: you'll definitely want to test a case where the result
  comes out negative.
\end{problem}

\begin{problem}
  Implement the function \texttt{times : bignum -> bignum -> bignum},
  which multiplies two bignums. Use the traditional algorithm you
  learned in grade school, but remember that we are counting in base
  1000, not 10. The main goal is correctness, so keep your code
  simple. Make sure your code works with positive numbers, negative
  numbers, and zero. Assume that the arguments satisfy the 
  invariant. \emph{Hint:} You may want to write a helper function that
  multiplies a bignum by a single \texttt{int} (which might be one
  coefficient of a bignum).

  The function \texttt{divmod($m$,$n$)}, which we have provided,
  yields a pair \texttt{($q$,$r$)} consisting of the quotient $q$ and
  the reminder $r$: $m = n\,q + r$ and $0 \leq r \leq n-1$.
\end{problem}


\section{Challenge Problem: The RSA Cryptosystem}

As on last week's problem set and several in the future, we are
providing an additional problem or two for those who would like an
extra challenge. These problems are for your karmic edification only,
and will not affect your grade. We encourage you to attempt this
problem only once you have done your best work on the rest of the
problem set.

This problem is a great way to verify that your previous functions
work correctly, since it uses almost all of them. Each of the three
parts of RSA can be implemented in approximately one line of code if you
use the right functions.

In a world that relies increasingly upon digital information, public key
encryption plays an important part in achieving private communication.
RSA is a popular public-key encryption system. It is based on the fact
that there are fast algorithms for exponentiation and for testing prime
numbers, but no known fast algorithms for factoring large numbers. In
this problem set you will implement a version of the RSA system. The
algorithms will you implement have both a deep mathematical foundation
and practical importance.

Cryptographic systems typically use keys
for encryption and decryption. An encryption key is used to convert the
original message (the plaintext) to coded form (the ciphertext). A
corresponding decryption key is used to convert the ciphertext back to
the original plaintext.

In traditional cryptographic systems, the
same key is used for both encryption and decryption. Two parties can
exchange coded messages only if they share a secret key. Since anyone
who learned that key would be able to decode the messages, keys must be
carefully guarded and transmitted only under tight security: for
example, couriers handcuffed to locked, tamper-resistant briefcases!

Diffie and Hellman (1976) discovered a new approach to encryption and
decryption: public key cryptography. In this approach, the encryption
and decryption keys are different. Knowing the encryption key cannot
help you find the decryption key. Thus you can publish your encryption
key on the web, and anyone who wants to send you a secret message can
use it to encode a message to send to you. You do not have to worry
about key security at all, for even if everyone in the world knew your
encryption key, no one could decrypt messages sent to you without
knowing your decryption key, which you keep private to yourself. You
used public-key encryption when you set up your CS51 git repositories:
the command \texttt{ssh-keygen} generated a public encryption key and
private decryption key for you. You uploaded the public key and
(hopefully) kept the private key to yourself.

The RSA system, due
to Rivest, Shamir, and Adelman, is the best-known public-key
cryptosystem; the security of your web browsing probably depends on it.

\subsection{How RSA Works}

RSA does not work with characters, but with integers. That is, it
encrypts numbers. To encrypt a piece of text, just combine the ASCII
codes of several characters into a bignum, and then encrypt the
resulting number.

Suppose you want to obtain public and private keys in the RSA system.
You select two very large prime numbers \emph{p} and \emph{q}. (Recall
that a prime number is a positive integer greater than 1 with no
divisors other than itself and 1.) You then compute numbers \emph{n} and
\emph{m}:
%
\begin{align*}
n &= p\,q \\
m &= (p-1)(q-1)
\end{align*}

It turns out that 
%
\begin{itemize}
\item With very few exceptions, for almost all numbers $e < n$, $e\,m\pmod{n} = 1$.
\item No one knows how to compute $m$, $p$, or $q$ efficiently, even
  knowing $n$.
\end{itemize}

(Notation: We write $a \pmod{n}$ for the remainder obtained when
dividing $a$ by $b$ using ordinary integer division.  We use the
notation $[a=b] \pmod{n}$ to mean that $a\pmod{n} = b\pmod{n}$.
Equivalently, $[a=b]\pmod{n}$ if $a-b$ is divisible by $n$. For
example, $[17 = 32]\pmod{5}$.)

Now you pick a number$e < m$ relatively prime to $m$; that is, such
that $e$ and $m$ have no factors in common except $1$. The
significance of relative primality is that $e$ is relatively prime to
$m$ if and only if $e$ is invertible mod $m$; that is, if and only if
there exists a $d$ such that $[d\,e = 1] \pmod{m}$. Moreover, it is
possible to compute $d$ from $e$ and $m$ using Euclid's algorithm,
described below. Your public key, which you can advertise to the
world, is the pair $(n,e)$. Your private key is $(n,d)$.

Anyone who wants to send you a secret message $s$ (represented by an
integer) encrypts it by computing $E(s)$, where
%
\[e(s) = s^e \pmod{n} \]
%
That is, if the plaintext is represented by the number $s$ which is
less than $n$, the ciphertext $E(s)$ is obtained by raising $s$ to the
power $,$then taking the remainder modulo $n$.

The decryption process is exactly the same, except that $d$ is used
instead of $e$:
%
\[D(s) = s^d \pmod{n} \]

The operations $E$ and $D$ are inverses:
%
\begin{align*}
D(E(s)) &= (s^e)^d \pmod{n} \\
& = s^{de} \pmod{n} \\
& = s^{1+km} \pmod{n} \\
& = s(s^m)^k \pmod{n} \\
& = s\,1^k \pmod{n} \\
& = s \pmod{n} \\ 
& = s
\end{align*}

For the last step to hold, the integer $s$ representing the plaintext
must be less than $n$. That's why we break the message up into
chunks. Also, this only works if $s$ is relatively prime to $n$: it
has no factors in common with $n$ other than 1. If n is the product of
two large primes, then all but negligibly few messages $s<n$ satisfy
this property. If by some freak chance $s$ and $n$ turned out not to
be relatively prime, then the code would be broken; but the chances of
this happening by accident are insignificantly small.

In summary, to use the RSA system:
%
\begin{enumerate}
\item Pick large primes $p$ and $q$.
\item Compute $n = pq$ and $m = (p-1)(q-1)$.
\item Choose $e$ relatively prime to $m$ and use this to
compute $d$ such that $[d\,e = 1] \pmod{m}$.
\item Publish the pair $(n,e)$ as your public key, but keep
$d$, $p$, and $q$ secret.
\end{enumerate}

How secure is RSA? At present, the only known way to obtain $d$ from
$e$ and $n$ is to factor $n$ into its prime factors $p$ and $q$, then
compute $m$ and proceed as above. But despite centuries of effort by
number theorists, factoring large integers efficiently is still an
open problem. Until someone comes up with an efficient way to factor
numbers, or discovers some other way to compute $d$ from $e$ and $n$,
the cryptosystem appears to be secure for large numbers $n$.

\subsection{Your task: Implement RSA}

You have been given a partial implementation of the RSA algorithm. The
given code provides additional functions for computations on big
numbers; functions to generate prime numbers and code that generates key
pairs. We briefly discuss a few of these functions. You should call
these functions as needed in your code. They handle most of the
mathematical and cryptographic detail for you.

Function \texttt{expmod(\textit{b, e, m})} computes $b^e \pmod{m}$. Function
\texttt{euclid(\emph{m,n})} yields numbers (\emph{s,t,g}) such that
$sm + tn = g$, where $g$ is the greatest common divisor of
$m$ and $n$.

Function \texttt{isPrime} tests if a number is prime. This is a
probabilistic test: it yields true if there is a high probability that
$n$ is prime; and yields false if $n$ is definitely not prime.
It's worth mentioning that this function uses the well-known
Miller-Rabin primality test, partially named after Harvard Professor
Michael Rabin, who developed the algorithm.

The function \texttt{generateKeyPair r} generates an RSA key pair, and
returns a tuple $(e, d, n)$. It picks random primes $p$ and $q$
that are in the range from $2^{r}$ to
$2^{r+1}$ such that $n=pq$ will be in the range
$2^{2r}$ to $2^{2r+2}$.

You must add the encryption and decryption functions specified below.
Your functions encode an input list of bytes into an output list of
bytes. Your functions will operate on \emph{blocks of bytes} (or,
equivalently, blocks of chars, since a char takes 1 byte to store). Each
block consists of $M$ bytes or $(M-1)$ bytes, where M is the number of bytes
required to store the modulus $n$ of the key $(n,e)$ or
$(n,d)$. You can use the function \texttt{bytesInKey} to compute
this from \texttt{n}.

\begin{problem} \textbf{Challenge}
Implement the function \texttt{encryptDecryptBignum}, which encrypts or
decrypts a bignum. (Since the operations are so similar, as
described above, we can use the same function. The only difference is
that to encrypt you pass $e$ and to decrypt you pass $d$.)
\end{problem}

\textbf{Note:} For this problem and the one below, it's probably
difficult to write tests that check whether the result matches your
expected result. However, you can write tests that check whether a
bignum encrypted using RSA and then decrypted using the corresponding
decryption key results in the original bignum.

\begin{problem}
\textbf{Challenge}
You'll note that RSA acts on bignums, so this gives us no way to
directly encode strings. Since very few secret message consist of large
integers, we need a way to convert strings (or lists of bytes) into
bignums. You may also realize from the description of RSA that each
bignum must be less than the modulus n. We have given you the function
\texttt{charsToBignums} which transforms a list of chars to a list of
bignums, each of which is less than n. It does this by breaking the list
of chars into blocks of M bytes, where M is the number of bytes required
to store n. We've also provided the inverse transformation
\texttt{bignumsToChars}. You may also find the functions
\texttt{explode} and \texttt{implode} defined higher in the file useful.

Finally, we've provided the function \texttt{encDecBignumList} which
encrypts or decrypts a list of bignums. This simply calls your
\texttt{encryptDecryptBignum} repeatedly.

Your job is to write the functions \texttt{encrypt}, which takes n, e
and a string to encode, and returns the encoded message as a list of
bignums, and \texttt{decrypt}, which takes n, d and a list of bignums
provided by \texttt{encrypt}, decrypts the message and produces a
string. Note that these functions are doing more than just a direct
translation of chars to bignums and vice versa: we've given you those
functions. Your functions should actually encrypt these bignums with
RSA, so that nobody will be able to reconstruct the original bignums, or
the original message, without the decryption key. Not bad for your
second week of CS51!

To exchange secret messages with your friends, use
\texttt{generateKeyPair} to get an RSA keypair. Send the pair
\texttt{(n, e)} to your friend and have them encrypt a message using
this key and send it to you. You can then decrypt it using \texttt{n}
and \texttt{d}, but nobody else can because they don't have
\texttt{d}.
\end{problem}

\section{Challenge Problem: Multiply Faster}

The multiplication algorithm you implemented in Section 1 will work just
fine for most integers of reasonable sizes, including the ones we will
be using in this assignment. However, some exceedingly smart people have
devised algorithms which, on very large numbers (think thousands of
digits), are considerably faster than the multiplication algorithm you
learned in grade school.

\begin{problem} \textbf{Challenge} 
See if you can implement such an algorithm in \texttt{times\_faster}.
You may want to start by googling the Karatsuba algorithm. This
algorithm recursively multiplies smaller and smaller numbers. Note that,
when the numbers become small enough (2-4 digits), you can and probably
should simply call the \texttt{times} function you implemented earlier.
However, don't just call this function on any numbers you are given;
that's not any faster!
\end{problem}

\section{Submit}

To submit the problem set, follow the instructions found
\href{http://tiny.cc/cs51reference}{here}.

\end{document}
