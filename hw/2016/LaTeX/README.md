Both the url's for the general reference and the style guide are set up
as shortened url's through tiny.cc (which allows you to change the long
url to which the short url points).

http://tiny.cc/cs51reference
http://tiny.cc/cs51styleguide

The login is:
username: arianna51
password: CS51issofun