(* 
			      CS51 Lab 2
		    Polymorphism and record types
			     Spring 2017
			       TESTING
 *)

#ifdef SOLN
module L = Lab2_soln ;;
#else
module L = Lab2 ;;
#endif

open Testing ;;
#include "testing.ml.include"

open L ;;

let suite_lab2 = Testing.create_suite "lab2" ;;

Testing.add_test suite_lab2 "submission" (lazy true);;

# 0 "Exercise zip"
Testing.add_test suite_lab2 "zip empty" ~points:0 (lazy (zip [] [] = Some []));;
Testing.add_test suite_lab2 "zip mismatched 1" ~points:0 (lazy (zip [] [true] = None));;
Testing.add_test suite_lab2 "zip mismatched 2" ~points:0 (lazy (zip ["abc"] [] = None));;
Testing.add_test suite_lab2 "zip mismatched 3" ~points:0 (lazy (zip [1.0] [true;false;true] = None));;
Testing.add_test suite_lab2 "zip ints" ~points:0 (lazy (zip [1; 2; 3] [4; 5; 6] = Some [(1, 4); (2, 5); (3, 6)]));;
Testing.add_test suite_lab2 "zip bool float" ~points:0 (lazy (zip [true;false;true] [4.0;5.0;6.0] = Some [(true, 4.0); (false, 5.0); (true, 6.0)]));;

# 0 "Exercise min_option"
Testing.add_test suite_lab2 "min_option none/none" ~points:0 (lazy (min_option None None = None));;
Testing.add_test suite_lab2 "min_option some/none" ~points:0 (lazy (min_option (Some 5) None = Some 5));;
Testing.add_test suite_lab2 "min_option some/none 2" ~points:0 (lazy (min_option None (Some 10) = Some 10));;
Testing.add_test suite_lab2 "min_option some/some" ~points:0 (lazy (min_option (Some 1) (Some 10) = Some 1));;
Testing.add_test suite_lab2 "min_option some/some 2" ~points:0 (lazy (min_option (Some (-5)) (Some (-3)) = Some (-5)));;
Testing.add_test suite_lab2 "min_option some/some 3" ~points:0 (lazy (min_option (Some 42) (Some 42) = Some 42));;

# 0 "Exercise max_option"
Testing.add_test suite_lab2 "max_option none/none" ~points:0 (lazy (max_option None None = None));;
Testing.add_test suite_lab2 "max_option some/none" ~points:0 (lazy (max_option (Some 5) None = Some 5));;
Testing.add_test suite_lab2 "max_option some/none 2" ~points:0 (lazy (max_option None (Some 10) = Some 10));;
Testing.add_test suite_lab2 "max_option some/some" ~points:0 (lazy (max_option (Some 1) (Some 10) = Some 10));;
Testing.add_test suite_lab2 "max_option some/some 2" ~points:0 (lazy (max_option (Some (-5)) (Some (-3)) = Some (-3)));;
Testing.add_test suite_lab2 "max_option some/some 3" ~points:0 (lazy (max_option (Some 42) (Some 42) = Some 42));;

# 0 "Exercise min_option_2"
Testing.add_test suite_lab2 "min_option_2 none/none" ~points:0 (lazy (min_option_2 None None = None));;
Testing.add_test suite_lab2 "min_option_2 some/none" ~points:0 (lazy (min_option_2 (Some 5) None = Some 5));;
Testing.add_test suite_lab2 "min_option_2 some/none 2" ~points:0 (lazy (min_option_2 None (Some 10) = Some 10));;
Testing.add_test suite_lab2 "min_option_2 some/some" ~points:0 (lazy (min_option_2 (Some 1) (Some 10) = Some 1));;
Testing.add_test suite_lab2 "min_option_2 some/some 2" ~points:0 (lazy (min_option_2 (Some (-5)) (Some (-3)) = Some (-5)));;
Testing.add_test suite_lab2 "min_option_2 some/some 3" ~points:0 (lazy (min_option_2 (Some 42) (Some 42) = Some 42));;

# 0 "Exercise max_option_2"
Testing.add_test suite_lab2 "max_option_2 none/none" ~points:0 (lazy (max_option_2 None None = None));;
Testing.add_test suite_lab2 "max_option_2 some/none" ~points:0 (lazy (max_option_2 (Some 5) None = Some 5));;
Testing.add_test suite_lab2 "max_option_2 some/none 2" ~points:0 (lazy (max_option_2 None (Some 10) = Some 10));;
Testing.add_test suite_lab2 "max_option_2 some/some" ~points:0 (lazy (max_option_2 (Some 1) (Some 10) = Some 10));;
Testing.add_test suite_lab2 "max_option_2 some/some 2" ~points:0 (lazy (max_option_2 (Some (-5)) (Some (-3)) = Some (-3)));;
Testing.add_test suite_lab2 "max_option_2 some/some 3" ~points:0 (lazy (max_option_2 (Some 42) (Some 42) = Some 42));;

# 0 "Exercise and_option"
Testing.add_test suite_lab2 "and_option none/none" ~points:0 (lazy (and_option None None = None));;
Testing.add_test suite_lab2 "and_option some/none" ~points:0 (lazy (and_option None (Some true) = Some true));;
Testing.add_test suite_lab2 "and_option some/none 2" ~points:0 (lazy (and_option (Some false) None = Some false));;
Testing.add_test suite_lab2 "and_option some/some" ~points:0 (lazy (and_option (Some true) (Some true) = Some true));;
Testing.add_test suite_lab2 "and_option some/some 2" ~points:0 (lazy (and_option (Some false) (Some false) = (Some false)));;
Testing.add_test suite_lab2 "and_option some/some 3" ~points:0 (lazy (and_option (Some true) (Some false) = (Some false)));;

# 0 "Exercise curry"
let f = fun (x, y) -> max x y in
Testing.add_test suite_lab2 "curry max" ~points:0 (lazy (let f = curry f in f 1 2 = 2));;
let f = fun (x, y) -> x = y in
Testing.add_test suite_lab2 "curry equal" ~points:0 (lazy (let f = curry f in f 1 2 = false));;

# 0 "Exercise uncurry"
Testing.add_test suite_lab2 "uncurry max" ~points:0 (lazy (let f = uncurry max in f (1, 2) = 2));;
Testing.add_test suite_lab2 "uncurry equal" ~points:0 (lazy (let f = uncurry ( = ) in f (3, 3) = true));;

# 0 "Exercise plus"
Testing.add_test suite_lab2 "plus" ~points:0 (lazy (plus (1, 2) = 3));;
Testing.add_test suite_lab2 "plus 2" ~points:0 (lazy (plus (-2, -5) = -7));;

# 0 "Exercise times"
Testing.add_test suite_lab2 "times" ~points:0 (lazy (times (1, 2) = 2));;
Testing.add_test suite_lab2 "times 2" ~points:0 (lazy (times (-2, -5) = 10));;

# 0 "Exercise prods"
Testing.add_test suite_lab2 "prods empty" ~points:0 (lazy (prods [] = []));;
Testing.add_test suite_lab2 "prods singleton" ~points:0 (lazy (prods [(2, -2)] = [-4]));;
Testing.add_test suite_lab2 "prods many" ~points:0 (lazy (prods [(1, -4); (-2, -5); (3, 6)] = [-4; 10; 18]));;

# 0 "Exercise maybe"Testing.add_test suite_lab2 "maybe none" ~points:0 (lazy (maybe None (fun x -> x) = None));;
Testing.add_test suite_lab2 "maybe identity" ~points:0 (lazy (maybe (Some 2) (fun x -> x) = Some 2));;
Testing.add_test suite_lab2 "maybe bool" ~points:0 (lazy (maybe (Some true) (fun b -> not b) = Some false));;

# 0 "Exercise dot_prod"
Testing.add_test suite_lab2 "dot_prod empty" ~points:0 (lazy (dot_prod [] [] = Some 0));;
Testing.add_test suite_lab2 "dot_prod mismatch 1" ~points:0 (lazy (dot_prod [] [5] = None));;
Testing.add_test suite_lab2 "dot_prod mismatch 2" ~points:0 (lazy (dot_prod [5] [] = None));;
Testing.add_test suite_lab2 "dot_prod mismatch 3" ~points:0 (lazy (dot_prod [1; 2; 3] [1; 2; 3; 4] = None));;
Testing.add_test suite_lab2 "dot_prod single" ~points:0 (lazy (dot_prod [6] [7] = Some 42));;
Testing.add_test suite_lab2 "dot_prod many" ~points:0 (lazy (dot_prod [1; 2; 3] [0; 1; 2] = Some 8));;
Testing.add_test suite_lab2 "dot_prod neg" ~points:0 (lazy (dot_prod [1; -2; -3] [0; 1; -2] = Some 4));;

# 0 "Exercise zip_2"
Testing.add_test suite_lab2 "zip_option_2 empty" ~points:0 (lazy (zip_option_2 [] [] = Some []));;
Testing.add_test suite_lab2 "zip_option_2 mismatched 1" ~points:0 (lazy (zip_option_2 [] [5] = None));;
Testing.add_test suite_lab2 "zip_option_2 mismatched 2" ~points:0 (lazy (zip_option_2 [3] [] = None));;
Testing.add_test suite_lab2 "zip_option_2 mismatched 3" ~points:0 (lazy (zip_option_2 [1] [1;2;3] = None));;
Testing.add_test suite_lab2 "zip_option_2 single" ~points:0 (lazy (zip_option_2 [1] [4] = Some [(1, 4)]));;
Testing.add_test suite_lab2 "zip_option_2 many" ~points:0 (lazy (zip_option_2 [1; 2; 3] [4; 5; 6] = Some [(1, 4); (2, 5); (3, 6)]));;

# 0 "Exercise max_list"
Testing.add_test suite_lab2 "max_list empty" ~points:0 (lazy (max_list [] = None));;
Testing.add_test suite_lab2 "max_list singleton" ~points:0 (lazy (max_list [0] = Some 0));;
Testing.add_test suite_lab2 "max_list positive" ~points:0 (lazy (max_list [1; 2; 3] = Some 3));;
Testing.add_test suite_lab2 "max_list negative" ~points:0 (lazy (max_list [-1; 1; 0] = Some 1));;
Testing.add_test suite_lab2 "max_list mixed" ~points:0 (lazy (max_list [-10; -3; -42] = Some (-3)));;
Testing.add_test suite_lab2 "max_list repeated max" ~points:0 (lazy (max_list [-5; 0; 3; 3; 3] = Some 3));;
Testing.add_test suite_lab2 "max_list more repeats" ~points:0 (lazy (max_list [0; 1; 0; 1; 0] = Some 1));;

(* Run the necessary reports *)
let _ = Testing.run_vocareum_report ~detailp:!dp ~gradep:!gp suite_lab2 ;;
