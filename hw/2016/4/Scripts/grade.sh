rm -rf _build *.byte 2> /dev/null

cp -r $LIB Lib
cp -r $ASNLIB AsnLib
cp AsnLib/_tags _tags

echo "Checking line lengths..."

python Lib/check_width.py ps4.ml
python Lib/check_width.py modules.ml

echo "Compiling with test harness..."

ocamlbuild -no-hygiene -quiet AsnLib/ps4_testing.byte
rc=$?

# Clean compiled sources
rm -rf Lib AsnLib _tags 2> /dev/null

if [ $rc -ne 0 ]
then
    echo "Compilation failed"
    exit 1
else
    echo "Compilation succeeded; proceeding to unit testing..."
    ./ps4_testing.byte -detail -grade
fi
