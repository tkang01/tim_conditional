rm -rf _build *.byte 2> /dev/null

cp -r $LIB Lib
cp -r $ASNLIB AsnLib
cp AsnLib/_tags _tags

echo "Checking line lengths..."

python Lib/check_width.py ps4.ml
python Lib/check_width.py modules.ml

echo "Checking for evil..."

for file in *.ml
do
	python Lib/check_evil.py $file

	if [ $? -ne 0 ]
	then
		echo "Malicious code detected in" $file
		
		# Clean up
		rm -rf Lib AsnLib
		exit 1
	fi
done

echo "Compiling with test harness..."

ocamlbuild -no-hygiene -quiet AsnLib/ps4_testing.byte
rc=$?

# Clean compiled sources
rm -rf Lib AsnLib _build *.byte _tags 2> /dev/null

if [ $rc -ne 0 ]
then
	echo -e "\n\n"
    echo -e "~ * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ~
*                                                                                 *
* Compilation failed. Your submission was unsuccessful and will receive no credit *
*                                                                                 *
~ * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ~\n\n"
    exit 1
else
	echo -e "\n\n"
    echo -e "~ * * * * * * * * * * * * * * * * * * * * * * * * * * ~
*                                                     *
* Compilation succeeded! Your submission was received *
*                                                     *
~ * * * * * * * * * * * * * * * * * * * * * * * * * * ~\n\n"
fi
