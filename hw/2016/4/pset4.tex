\documentclass{amsart}

\input{../LaTeX/cs51-preamble}
\include{graphics}
\usepackage{longtable}
\usepackage{multirow}
\usepackage{multicol}

\title[CS51 Problem Set 4]{CS51 Problem Set 4:\\Modules, Functors, and Priority Queues}
%\author{Stuart M. Shieber} \date{\today}

\begin{document}

\maketitle
\linenumbers
\begin{center}
\textbf{This problem set is due March 4, 2016 at 5:00pm EST.}

\textbf{You may work with a partner on this problem set. If you choose to work 
with a partner, you can find instructions for configuring 
Vocareum \href{http://tiny.cc/cs51reference}{here}.}
\end{center}

\setcounter{section}{-1}
\section{Introduction}

In this assignment you will learn about OCaml modules and how to use
modules to create data structure interfaces with useful abstractions.
In the process you will work with the following signatures, modules,
and functors (marked as to which we provide and which you write or
complete [in italics]):
% 
\begin{itemize}
\item Binary trees
  \begin{itemize} 
  \item A binary tree signature [provided]   
  \item \emph{A functor for generating implementations of the signature
    [you complete]}
  \item A module generated using the functor implementing integer
    binary trees [provided] 
  \end{itemize}
\item Priority queues
  \begin{itemize}
  \item A priority queue signature [provided]
    
  \item \emph{A functor for generating implementations based on lists [you complete]}
  \item \emph{A functor for generating implementations based on binary trees [you write, using the binary tree work above]}
  \item \emph{A functor for generating implementations based on binary heaps [you complete]}
  \item Modules generated from all three functors implementing integer priority queues [provided]
  \item Sort functions that use these modules [provided]
  \end{itemize}
\end{itemize}

\subsection{Questions} The ``questions to consider'' in this writeup (as
opposed to the numbered problems) are not graded. They exist to help
you check your understanding of the material. You are not expected to
include answers to them in your submission, but they are things that
we could test on. Answers to most of the questions are in the comments
of the problem set.

\subsection{Testing} Testing is of course required. Please see
tests.ml for how to test.

\subsection{Partners} You are allowed (and encouraged) to work with a
partner on this assignment. You are allowed to work alone if that is
your preference. If you are an extension student, you are free to pair
up with another extension or on-campus student. \textbf{
Do not start solo unless you are absolutely certain you will be working alone!
} For more detailed instructions,
 look \href{http://tiny.cc/cs51reference}{here}.

\subsection{Downloading} To download the problem set, follow the
instructions found \href{http://tiny.cc/cs51reference}{here}. Note
that, now that you are working with a partner, you should endeavour to
keep up-to-date with your partner's code. Run \code{git pull} to fetch
any changes, and \code{git push} to upload your changes to the
repository.  It's generally a good practice to \code{pull} before you
\code{push}, so you can resolve any conflicts between your and your
partner's code! More information about Git and its use can be found in
Eddie Kohler's
\href{http://cs61.seas.harvard.edu/wiki/2014/Git}{guide to Git}.

\section{Binary trees introduced}

We'll start with a simple module for binary trees, called
FIRSTBINTREE. The FIRSTBINTREE signature specifies an interface with a
data type for binary trees and \emph{empty}, \emph{insert},
\emph{search}, \emph{delete}, \emph{getmin}, and \emph{getmax}
operations.  It makes reference to an \texttt{order} type useful as
the result of comparing two values:
%
\begin{caml}
\?{type order = Equal | Less | Greater ;;}
\:{type order = Equal | Less | Greater}
\end{caml}

Here is the FIRSTBINTREE signature itself. You'll want to read and
understand it to familiarize yourself with the syntax for how to write
module signatures.
%
\begin{caml}
\?{module type FIRSTBINTREE =}
\?{sig}
\?{  exception EmptyTree}
\?{  exception NodeNotFound}
\?{}
\?{  (* What this type actually looks like is left up to the implementation *)}
\?{  type 'a tree}
\?{}
\?{  (* Returns an empty tree *)}
\?{  val empty : 'a tree}
\?{}
\?{  (* Insert elt into tree *)}
\?{  val insert : ('a -> 'a -> order) -> 'a -> 'a tree -> 'a tree}
\?{}
\?{  (* Search a binary tree for the given value. See note below about}
\?{   * the first argument. *)}
\?{  val search : ('a -> 'a -> order) -> 'a -> 'a tree -> bool}
\?{}
\?{  (* Delete the given value from a binary tree. See note below about}
\?{   * the first argument. May raise NodeNotFound exception. *)}
\?{  val delete : ('a -> 'a -> order) -> 'a -> 'a tree}
\?{}
\?{  (* Return the minimum value of a binary tree. See note below about}
\?{   * the first argument. May raise EmptyTree exception *)}
\?{  val getmin : ('a -> 'a -> order) -> 'a tree -> 'a}
\?{}
\?{  (* Return the maximum value of a binary tree. See note below about}
\?{   * the first argument. May raise EmptyTree exception *)}
\?{  val getmax : ('a -> 'a -> order) -> 'a tree -> 'a}
\?{end ;;}
\:{*       *       *       *     module type FIRSTBINTREE =}
\:{  sig}
\:{    exception EmptyTree}
\:{    exception NodeNotFound}
\:{    type 'a tree}
\:{    val empty : 'a tree}
\:{    val insert : ('a -> 'a -> order) -> 'a -> 'a tree -> 'a tree}
\:{    val search : ('a -> 'a -> order) -> 'a -> 'a tree -> bool}
\:{    val delete : ('a -> 'a -> order) -> 'a -> 'a tree}
\:{    val getmin : ('a -> 'a -> order) -> 'a tree -> 'a}
\:{    val getmax : ('a -> 'a -> order) -> 'a tree -> 'a}
\:{  end}
\end{caml}

Notice that it explicitly lists the types and values that any module
implementing this interface must define, as well as the exceptions
that any function in the interface may throw.\footnote{ Because of how OCaml
handles exceptions, listing exceptions is optional, and you can't
indicate with code which functions may cause which exceptions, but it
is good style to mention in a function's comments what exceptions it
may throw and under what conditions.} For a function like
\emph{getmin}, we could instead choose to return an \texttt{'a
option}, which would avoid the need for an exception. But you should
get used to exceptions like these in modules, since OCaml modules tend
to use them.  Remember, functions \emph{are} values, so functions are
also listed with the \texttt{val} keyword.

The interface for FIRSTBINTREE is not ideal.  Consider the following
questions:
%
\begin{itemize}
\item Why is FIRSTBINTREE not ideal?
\item How could a call to \emph{delete} give you incorrect behavior
  for a correctly constructed tree?
\item Is FIRSTBINTREE a type?
\item How would one \emph{use} FIRSTBINTREE?
\end{itemize}

\section{Binary search trees}

An improved signature BINTREE is provided in the file
\texttt{ps4.ml}. To create this better interface we need to introduce
another module type -- COMPARABLE -- which has its own signature.

Consider these questions:
%
\begin{itemize}
\item Why is BINTREE a better interface?
\item Why did we need to introduce another module type COMPARABLE?
\end{itemize}

You will provide a functor, called BinSTree,
for generating \emph{implementations} of
the BINTREE interface. BinSTree
implements a binary search tree where repeated values are compressed
into a single node containing a list of those values.
Remember that functors are not yet modules; they must be applied
to an argument module in order to produce a module. 
In this case, BinSTree takes a module
satisfying the COMPARABLE signature \emph{as an argument} and returns
a BINTREE module.  Once you have implemented BinSTree, you can create
IntTree -- a binary search tree of integers -- by applying BinSTree to
an integer implementation of COMPARABLE.

\begin{problem}
  Implement the \texttt{insert}, \texttt{search}, \texttt{getmin}, and
  \texttt{getmax} functions for BinSTree. (We've provided the rest.)
\end{problem}

\section{Priority queues}

A priority queue is a data structure that allows you to add elements
and extract a minimum element. (Priority queues are widely useful, for
instance, when implementing
\href{http://en.wikipedia.org/wiki/Dijkstra's_algorithm}{Dijkstra's
  algorithm} for efficiently computing shortest paths in a network.)
We have provided the PRIOQUEUE interface for priority queues, which
supports \texttt{empty}, \texttt{is\_empty}, \texttt{add}, and
\texttt{take} operations. The \texttt{add} function inserts an element
into the priority queue and the \texttt{take} function removes the
minimum element. In this section you will be implementing priority
queues in multiple ways -- with lists, binary trees, and binary heaps.

\begin{problem}
  Complete the \texttt{ListQueue} functor: a naive implementation of a
  priority queue.  In this implementation, the elements in the queue
  are stored in a simple list in priority order.  This implementation
  is not ideal because either the \texttt{take} or the \texttt{add}
  operation is $O(n)$ complexity.
\end{problem}

\begin{problem}
  Next, implement \texttt{TreeQueue}, which is less naive than
  \texttt{ListQueue} (but still not ideal). In this implementation of
  the PRIOQUEUE interface, the queue is stored as a binary search tree
  using the BinSTree abstract structure data type that you've already
  implemented.
\end{problem}

Consider these questions:
%
\begin{itemize}
  \item Why is the \texttt{TreeQueue} implementation not ideal?
  \item What is the worst case complexity of \texttt{add} and
    \texttt{take} for a \texttt{TreeQuee}?
\end{itemize}

Finally, you will implement a priority queue using a binary heap,
which has the attractive property of $O(log(n))$ complexity for both
\texttt{add} and \texttt{take}.  Binary (min)heaps are binary trees
for which the following \emph{ordering invariant} holds: the value
stored at a node is smaller than all values stored in the subtrees
below the node. Binary heaps as you will implement them obey a further
invariant of being \emph{balanced}: For a given node, its left branch
has either the same number or one more node than its right branch.%
%
\footnote{This definition of balance is a bit different from a
  traditional variant for balanced binary trees that requires the last
  (lowest) level in the tree to be filled strictly from left to right.}
%
To define the invariant, we will call a balanced tree \emph{odd} or
\emph{even}. A tree is odd if its left child has one more node than
its right child. A tree is even if its children are of equal size. All
subtrees must be either odd or even.  \emph{The functions add and take
  must return balanced trees for this definition of `balanced'}.  

In the skeleton code for the BinaryHeap functor in \texttt{ps4.ml}, we
have defined the \texttt{tree} type for implementing the binary
heap. Functions over the type will often need to respect one of these
representation invariants:
%
\begin{itemize}
\item \emph{Weak invariant}: The tree is balanced. That is, for any
  given node in the tree, there are the same number of nodes in the
  left subtree as in the right subtree, or the left has exactly 1
  more. (The tree cannot have more nodes on the right than the left.)
\item \emph{Strong invariant}: The tree satisfies
  the weak invariant, and for every
  node, the value stored at that node is less than or equal to the
  values stored at all of its child nodes.
\end{itemize}
%
The \texttt{add} and \texttt{take} functions must return trees that
respect the strong invariant, and should assume they will only be
passed trees that also obey the strong invariant. That is, they
\emph{preserve the strong invariant}. We have provided stubs for
helper functions that operate on trees that are required to preserve
only the weak invariant.  Hint: Your nodes should track whether they
are odd or even. This will help you keep your tree balanced at all
times.

Notice that we have encoded the difference between odd and even nodes
in the \texttt{tree} type that we've provided for \texttt{BinaryHeap}.
You should probably first write a size function for your tree
type. This will help you check your representation invariant. You
should \emph{not} be calling \texttt{size} in the implementation of
\texttt{take}; rather, you should be using \texttt{size} to test
\texttt{take}.  We have provided you with the implementation of
\texttt{add} and a partial implementation of \texttt{take}. Below are
some guidelines when implementing \texttt{take} and its helper
functions, as well as in understanding \texttt{add}.

\subsection{add}\label{add}
The \texttt{add} function inserts a node into a spot that will either
turn the main tree from odd to even or from even to odd. We implement
this function for you, but you should understand how it works.

\subsection{take}\label{take}

The \texttt{take} function removes the root of the tree (the minimum
element) and replaces it by a leaf of the tree that, when removed,
turns the tree from odd to even or from even to odd.

After removing and replacing the root node your tree will respect the
weak invariant. You must ``fix'' the tree to respect the strong
invariant, as depicted in Figure~\ref{fig:fix}.

\begin{figure}
\begin{tabular}{c|c}
\textbf{Take} & \textbf{Fix} \\
\includegraphics[scale=0.5]{./down-heap-1.png} & \includegraphics[scale=0.5]{./down-heap-3.png} \\
\includegraphics[scale=0.5]{./down-heap-2.png} & \includegraphics[scale=0.5]{./down-heap-4.png}\\
\end{tabular}
\caption{Visual depiction of fixing a tree to rebalance it after
  taking an element from it.}
\label{fig:fix}
\end{figure}

Some questions to consider:
%
\begin{itemize}
\item How do we know that our binary heap stays balanced?
\item How might you test your binary heap?
\item How might you test the helper functions used in implementing
  your binary heap?
\item Why is it useful to use ListQueue, TreeQueue, and BinaryHeap
  behind a PRIOQUEUE interface?
\end{itemize}

\begin{problem}
  Complete the implementation of the binary heap priority queue by
  providing definitions for \texttt{get_top}, \texttt{fix},
  \texttt{get_last}, and \texttt{run_tests}, and completing the
  definition for \texttt{take}.
\end{problem}

Now that you've provided three different implementations of priority
queues, all satisfying the PRIOQUEUE interface, we give you an example
of how to use them to implement sort functions. You should use these
for testing (in addition to testing within the modules).

\section{Challenge problem: Sort functor}

Write a functor for sorting which takes a COMPARABLE module as an
argument and provides a sort function. You should abide by the
following interface:
%
\begin{verbatim}
type c
val sort : c list -> c list
\end{verbatim}
%
You should use your BinaryHeap implementation, and test it.

\section{Challenge problem: Benchmarking}
%
Benchmark the running times of heapsort, treesort, and selectionsort.
Arrive at an algorithmic complexity for each sorting algorithm. Record
the results of your tests. Be convincing with your data and analysis
when establishing the algorithmic complexity of each sort.

\section{Submit}

To submit the problem set, follow the instructions found
\href{http://tiny.cc/cs51reference}{here}. Please note that only one
of your partners needs to submit the problem set.


\end{document}
