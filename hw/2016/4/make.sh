#!/bin/sh

caml-tex -o pset4.tex pset4.mlt
pdflatex -shell-escape pset4
bibtex pset4
makeindex pset4
pdflatex -shell-escape pset4
pdflatex -shell-escape pset4
