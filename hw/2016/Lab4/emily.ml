(*

A collection of statements contained in an "Emily" module.

 *)

type house =
   Adams | Lowell | Quincy |
   Kirkland | Winthrop | Eliot |
   Leverett | Mather | Dunster |
   Pforzheimer | Cabot | Currier

type info = {
   hometown : string;
   year : int;
   concentration : string;
   house : house
}

let hometown = "Scotia, NY"
let year = 2017
let concentration = "Computer Science"
let house = Cabot
let fold = List.fold_right (+)

let info = {
   hometown;
   year;
   concentration;
   house
}

let grade_assignment assignment =
  "Everyone gets a perfect score for pset " ^ String.trim assignment ^ "!"

let favorite_function _ = failwith "I don't have a favorite function"
let least_favorite_function = ( ** )

let print_info () =
   let _ = print_string (
       info.hometown ^ "\n" ^
       string_of_int year ^ "\n" ^
       info.concentration ^ "\n") in
   match info.house with
   | Cabot -> print_string "Cabot!\n"
   | _ -> failwith "Do any other houses matter?"
