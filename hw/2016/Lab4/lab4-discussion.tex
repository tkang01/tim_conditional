\documentclass{amsart}

\input{../LaTeX/cs51-preamble}
\include{graphics}
\usepackage{longtable}
\usepackage{multirow}
\usepackage{multicol}

\title[CS51 Lab 4 Discussion]{CS51 Lab 4: Modules \& Functors\\Discussion and Considerations}
%\author{Dan Armendariz} \date{\today}

\begin{document}

\maketitle
\linenumbers

\setcounter{section}{-1}
\section{Introduction}

In this document, we'll review the thought process and provide some additional
context or information involved with a subset of the problems presented in Lab 4.
The intention of this document is to serve as review for the midterm (or final),
and is not meant to replace your attempt of the problems.
In fact, if you have not completed Lab 4, please do so before reading the
remainder of this document: completing the lab is the best way to
truly internalize the material presented.
Even after you've completed the lab, we hope you find the following
useful and clarifying for your studies.

Throughout this document, we assume that you have completed Lab 4 and 
have your solutions open adjacent to this discussion as you follow along.

% change section prefixes to "Part N." rather than just "N."
\renewcommand\thesection{Part \arabic{section}}
% but keep subsections as they are
\renewcommand\thesubsection{\arabic{subsection}}

\setcounter{section}{1}
\section{Files as modules}

\subsection{Exercise 2C: Write a TF signature}

In real-world programming, the task of writing a signature to match a pre-existing
module implementation is arguably an uncommon approach: you
normally would pre-define the abstraction and write code that accomplishes the goals
of the specification.

This is not to say that exercising this uncommon task is without value.
In fact, as you'll see when we discuss functors in more detail, it can be advantageous
to identify common methods across multiple modules and figure out ways to leverage
those similarities to extend the capabilities of pre-existing modules.
Ultimately, the intention of this exercise is to practice your ability to think more deeply
about types, and successful completion requires the ability to determine
\emph{compatible} types. 

The exercise asks you to determine the set of functions and labels that are compatible
between the \code{Sam} and \code{Emily} modules, and then record the most generic
type in the signature that maintains that compatibility.
We'll look at some examples.

\subsubsection{Incompatible types}
Here is the \code{least_favorite} function defined
in \texttt{Sam.ml}:

\begin{verbatim}
let least_favorite_function = (land)
\end{verbatim}

And here is the same from \texttt{Emily.ml}:

\begin{verbatim}
let least_favorite_function = ( ** )
\end{verbatim}

From the \href{http://caml.inria.fr/pub/docs/manual-ocaml/libref/Pervasives.html}{Pervasives
module documentation}, we see that \code{land} is a function for "bitwise logical and" and is
of type \code{int -> int -> int}, whereas the \code{(**)} operator is exponentiation and is of type
\code{float -> float -> float}. 
These functions are incompatible types and we cannot write a signature that is compatible
with both.
As a result, the signature for this exercise \emph{cannot contain a
\code{least_favorite_function}}.

You may wonder why we could not write the following signature to apply to
both:

\begin{verbatim}
(* This is bad news! *)
val least_favorite_function =  'a -> 'a -> 'a
\end{verbatim}

But this does not work, because the signature guarantees the types of
the implementation.
Neither of the \code{least_favorite_function} implementations defined in 
the modules can accept a generic type.
As an example, the above signature would allow \code{least_favorite_function}
to accept a string type, but this is an incompatible type with both
implementations.
Although there are times you may want a generic type in a signature, it
would be wrong for this exercise because the modules would not be a valid
implementation of it.

\subsubsection{Compatible types}
On the other hand, the \code{favorite_function} implementations \emph{are}
compatible.
\texttt{Sam.ml} has the following implementation\footnote{By the way, 
have you tried converting the hexadecimal number from Sam's
\code{favorite_function} into a string?}:

\begin{verbatim}
let favorite_function x y =
  log x +. log y +. float_of_string "0x74686563616b656973616c6965"
\end{verbatim}

Which has type \code{float -> float -> float}.
\texttt{Emily.ml} has the following implementation:

\begin{verbatim}
let favorite_function _ = failwith "I don't have a favorite function"
\end{verbatim}

Since \code{failwith}
\href{http://caml.inria.fr/pub/docs/manual-ocaml/libref/Pervasives.html}{returns
a generic type}, and given the unspecified type of the ignored parameter, the
\code{favorite_function} overall has type \code{'a -> 'b}. 
At first, these appear to be incompatible types, because Sam's
\code{favorite_function} accepts two parameters while Emily's accepts one.
However, it's conceivable that \code{'b} could be a 
function of type \code{float -> float}, which would allow the types of
the two functions to be compatible.

When writing the signature we would then include the following:

\begin{verbatim}
val favorite_function =  float -> float -> float
\end{verbatim}

Both Sam and Emily's \code{favorite_function} are compatible types,
but the signature is a promise of adherence and therefore the more
tightly bound type is the one that we must specify to guarantee the
type is compatible with both implementations.

\subsection{Exercise 2D: TFEmily and TFSam}

This exercise mainly requires the lookup of the correct syntax. 
We complete the exercise with the following lines of code:

\begin{verbatim}
module TFEmily : TF = Emily ;;
module TFSam : TF = Sam ;;
\end{verbatim}

Which is syntactic sugar for the following:

\begin{verbatim}
module TFEmily = (Emily : TF) ;;
module TFSam = (Sam : TF) ;;
\end{verbatim}

In either case, we might interpret the lines as creating a new module,
like \code{TFEmily}, that is the result of the binding of the \code{TF} 
signature to the \code{Emily} module. The binding ensures that the
\code{TFEmily} module only contains the methods and properties
specified by the \code{TF} signature.

The latter syntax should not be confused with OCaml's notion
of first-class modules; although we did not address this topic in
lab, you may have come across examples of this elsewhere.
First-class modules use syntax like \code{(module Name : SIG)} to 
encapsulate a module as a value. Using this syntax, modules can be 
passed as parameters to functions or stored in labels using \code{let}.
We do not do this for this exercise, however, because in order to 
access values or functions in the encapsulated modules we would
need to unpack them.
Purely for your curiosity, we show a concrete example of this below.

\begin{verbatim}
# (* What follows is NOT the correct answer for this exercise *)
  (* First, we encapsulate the Emily module and store it in a label *)
  let tfe = (module Emily : TF) ;;
val tfe : (module TF) = <module>

# (* Now we'll attempt to access a value, but it's encapsulated *)
  tfe.hometown ;;
Error: Unbound record field hometown 

# (* We must unpack the module to access it *)
  module TFE = (val tfe : TF) ;;
module TFE : TF

# TFE.hometown ;;
- : bytes = "Scotia, NY"
\end{verbatim}

Again, do not be confused by the similarity in the syntax. First-class
modules are defined by the \code{(module Name : SIG)} syntax, whereas
we define a module using \code{(Name : SIG)}.
The latter evaluates the module defined by \code{Name}, and binds the
signature \code{SIG} to it; either this syntax or its syntactic sugar,
provided early in this discussion, are the intended answers for this
exercise.

\setcounter{section}{4}
\section{Functors}

Functors are a way to perform operations on modules; we might think
of them as special functions that accept a module as a parameter, and
use the implementation provided in the passed module to implement a
new module.
Part 5 creates a functor called \code{MakeStack} that, in a certain sense,
is a special function that accepts a module \code{Element} as a
"parameter" and uses it to implement a stack.
Since \code{Element} must adhere to the \code{SERIALIZE} signature, we
are guaranteed that no matter which module is applied to \code{MakeStack},
we know that it has a type \code{t} and a \code{serialize} function that
transforms that type \code{t} into a string.
The OCaml type checker enforces this requirement at compile time.

We implement \code{MakeStack} as a functor so that we can create an 
abstract \code{stack} data type while still providing additional functionality
that depends on the type of data of each element.
In other words, we don't have to somehow create, in advance, a serialize
function that works on every possible type!

\subsection{Exercise 5A: Stack implementation}

Based on the above information, writing the serialization method in the
stack implementation depends on the \code{serialize} function provided
by the \code{Element} module, accessing it using dot notation:
\code{Element.serialize}.

One additional hint: although it is possible to implement recursively (we
even defined the function in the library code using \code{let rec}) it can
also be done in one or two lines of code by using other functions you
implemented in \code{MakeStack}!

\subsection{Exercises 5B: \code{IntStack} and 5C: \code{IntStringStack}}

These exercises necessitate the use of the \code{MakeStack} functor
to implement two different types of stacks: one using integers and another
with \code{(int, string)} tuples.
Although you've already created the functor \code{MakeStack} that
creates a stack, your task for these exercises is to define the module
implementation that adheres to the \code{SERIALIZE} signature for each.
In other words, you are implementing modules passed as the \code{Element}
"parameter" to the \code{MakeStack} functor.
Once done, the final application of the new modules to the functor create
the final \code{IntStack} and \code{IntStringStack} modules.

You may do this explicitly in two steps, like the below skeleton code:

\begin{verbatim}
module IntSerialize : SERIALIZE =
  struct
    (* complete implementation here *)
  end ;;

module IntStack = MakeStack(IntSerialize) ;;
\end{verbatim}

Or collapse both steps into one, applying an anonymous module to
the functor:

\begin{verbatim}
module IntStack = 
  MakeStack(struct
    (* complete implementation here *)
  end) ;;
\end{verbatim}

Choosing one over the other is a stylistic and design choice.

\subsection{Additional practice}

Given the length of the lab, we weren't able to add some additional practice
for functors.
You may wish to take the time to implement the following additional exercise:

Write a \emph{new} module that implements a \code{length} function for
the abstract \code{stack} type defined by \code{MakeStack}.
The \code{length} function should accept a \code{stack} as a parameter
and return the count of elements held within it, using only the module's
\code{fold_left} function.

By using a functor, you will not need to modify the implementation of any
of the code from part 5 to complete this additional exercise.
In essence, you would be providing \code{length} functionality to
\code{MakeStack} (albeit in a new module) after you have completed
its implementation.

This may feel like a needless exercise, since you could easily add a
\code{length} function to the \code{MakeStack} signature and implementation.
The practice is not without merit, though; had we defined the stack
type in a different way --- naming it \code{t} instead of \code{stack} ---
this extension functor that you write would be applicable to \emph{any}
module that exposes a type \code{t} and a \code{fold_left} function.
It is common among \texttt{Core.Std} modules to expose their abstract
types in this way, though we don't use those modules in this course.

You could even further make the exercise useful by adding other, more
complex functions based on \code{fold_left}, like \code{for_all},
\code{exists}, or \code{mem}. The behavior of these functions would
be analogous to those with the same names as found in the
\href{http://caml.inria.fr/pub/docs/manual-ocaml/libref/List.html}{List
module}.

Back to the exercise; try this on your own!
Below is a hint if you get stuck.

\subsubsection{Hint}

First, create a signature that ensures that a module adheres to both 
\code{fold_left} and also a type \code{stack}.
The type is necessary so that you can accept the data structure as parameter
to \code{length}.
You might call this signature \code{FOLD}, for instance, to make its purpose
clear, but the name is up to you.

Next, write a functor that accepts a module that matches the signature you
wrote and implements the \code{length} function.
The function implementation should use the applied module's \code{fold_left}
function to accomplish its task.

Finally, try to create a new module to test your extended functionality!

\end{document}
