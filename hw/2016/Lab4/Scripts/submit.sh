set -u

rm -rf _build *.byte 2> /dev/null

cp -r $LIB Lib
cp -r $ASNLIB AsnLib
cp AsnLib/_tags _tags

LAB="lab4"
PARTS=5

echo "Checking line lengths..."

for i in $(seq 1 $PARTS); do
    python Lib/check_width.py "${LAB}_part${i}.ml"
done

echo "Checking for evil..."

for file in *.ml
do
    python Lib/check_evil.py $file

    if [ $? -ne 0 ]
    then
        echo "Malicious code detected in ${file}"

        # Clean up
        rm -rf Lib AsnLib
        exit 1
    fi
done

echo "Compiling with test harness..."

echo "*** Test suite ${LAB} ***"
echo "submission : [1/1] passed"

VOCAREUM_ID_FILE="./vocareum_lab_submission_id"
touch $VOCAREUM_ID_FILE

for i in $(seq 1 $PARTS); do
    PART="${LAB}_part${i}"

    echo "** COMPILING ${PART}.ml **"

    ocamlbuild -no-hygiene -quiet "AsnLib/${PART}_testing.byte" >"err.${PART}" 2>&1
    rc=$?
done

# Clean compiled sources
rm -rf Lib AsnLib _tags 2> /dev/null

for i in $(seq 1 $PARTS); do
    PART="${LAB}_part${i}"

    echo "** TESTING ${PART}.ml **"

    if [ -f "./${PART}_testing.byte" ]
    then
        echo "Compilation succeeded! Proceeding to unit testing..."
        ./${PART}_testing.byte -detail -nograde
    else
        if [ -f "./err.${PART}" ]
        then
            echo "Compilation failed with the following error:"
            cat "./err.${PART}"
        else
            echo "Compilation failed."
        fi
    fi
done

rm -f "err.${LAB}_part*"
rm -f $VOCAREUM_ID_FILE
 
