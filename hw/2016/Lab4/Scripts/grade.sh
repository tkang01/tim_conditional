set -u

rm -rf _build *.byte 2> /dev/null

cp -r $LIB Lib
cp -r $ASNLIB AsnLib
cp AsnLib/_tags _tags

LAB="lab4"
PARTS=5

echo "Checking line lengths..."

for i in $(seq 1 $PARTS); do
    python Lib/check_width.py "${LAB}_part${i}.ml"
done

echo "Compiling with test harness..."

echo "*** Test suite ${LAB} ***"
echo "submission : [1/1] passed"

for i in $(seq 1 $PARTS); do
    PART="${LAB}_part${i}"

    echo "** TESTING ${PART}.ml **"
    ocamlbuild -no-hygiene -quiet "AsnLib/${PART}_testing.byte"
    rc=$?

    # Clean compiled sources
    rm -rf Lib AsnLib _tags 2> /dev/null

    if [ $rc -ne 0 ]
    then
        echo "Compilation failed"
        exit 1
    else
        echo "Compilation succeeded; proceeding to unit testing..."
        ./${PART}_testing.byte -detail -grade
    fi
done