# 1 "AsnLib/lab4_part5_soln_testing.mlpp"
module L = Lab4_part5_soln

open Testing ;;


# 2 "Lib/testing.ml.include"
open Testing ;;

let dp = ref true ;;
let gp = ref false ;;

let argspec = [
  ("-detail", Arg.Set dp, "provide detailed test report to student");
  ("-nodetail", Arg.Clear dp, "do not provide detailed test report to student");
  ("-grade", Arg.Set gp, "generate grading information");
  ("-nograde", Arg.Clear gp, "do not generate grading information")
];;
  
Arg.parse argspec (fun _ -> ()) "see the code" ;;

# 1 "AsnLib/lab4_part5_testing.ml.include"
(*** CS 51 Lab 4 Part 5 ***)
(*** February 24 2015 ***)
(*** Test Suite - Lab 4 ***)

open L ;;

let suite_lab4_part5 = Testing.create_suite "lab4_part5" ;;

let tester = Testing.add_test suite_lab4_part5 ~points:0 ;;



# 0 "Exercise 5A"

# 1 "Exercise 5A"
module IS = MakeStack(struct type t = int let serialize n = string_of_int n end);;

tester "5A MakeStack empty/push/top" (lazy (let open IS in 1 = (empty() |> push 1 |> top))) ;;
tester "5A MakeStack empty/push/pop" (lazy (let open IS in empty() = (empty() |> push 1 |> pop))) ;;
tester "5A MakeStack push/top empty" (lazy (let open IS in try ignore(empty() |> top);false with Empty -> true));;
tester "5A MakeStack push/pop empty" (lazy (let open IS in try ignore(empty() |> pop);false with Empty -> true));;
tester "5A MakeStack ordering" (lazy (let open IS in 2 = (empty() |> push 1 |> push 2 |> top))) ;;
tester "5A MakeStack ordering 2" (lazy (let open IS in 3 = (empty() |> push 1 |> push 2 |> pop |> push 3 |> top))) ;;
tester "5A MakeStack serialize" (lazy (let open IS in "1" = (empty() |> push 1 |> serialize)));;
tester "5A MakeStack serialize 2" (lazy (let open IS in "1:2" = (empty() |> push 1 |> push 2 |> serialize)));;
tester "5A MakeStack serialize 3" (lazy (let open IS in "1:2:3:4:5" = (empty() |> push 1 |> push 2|> push 3|> push 4|> push 5 |> serialize)));;
tester "5A MakeStack serialize empty" (lazy (let open IS in "" = (empty() |> serialize)));;
tester "5A MakeStack map" (lazy (let open IS in 6 = (empty() |> push 1 |> push 3 |> map (fun x -> x*2) |> top)));;
tester "5A MakeStack map2" (lazy (let open IS in 2 = (empty() |> push 1 |> push 3 |> map (fun x -> x*2) |> pop |> top)));;
tester "5A MakeStack filter" (lazy (let open IS in 1 = (empty() |> push 1 |> push 2 |> filter (fun x -> x mod 2 = 1) |> top)));;
tester "5A MakeStack filter 2" (lazy (let open IS in 1 = (empty() |> push 1 |> push 2 |> push 3 |> push 4 |> filter (fun x -> x mod 2 = 1) |> pop |> top)));;
tester "5A MakeStack fold_left" (lazy (let open IS in 2 = (empty() |> push 0 |> push 1 |> fold_left (fun x _ -> x + 1) 0)));;


# 0 "Exercise 5B"

# 1 "Exercise 5B"
tester "5B IntStack" (lazy (let open IntStack in 1 = (empty() |> push 1 |> push 2 |> pop |> top)));;
tester "5B IntStack serialize" (lazy (let open IntStack in "1:2" = (empty() |> push 1 |> push 2 |> serialize)));;


# 0 "Exercise 5C"

# 1 "Exercise 5C"
tester "5C IntStringStack" (lazy (let open IntStringStack in (1,"ohai") = (empty() |> push (1,"ohai") |> push (2,"world") |> pop |> top)));;
tester "5C IntStringStack serialize" (lazy (let open IntStringStack in "(1,'ohai'):(2,'world')" = (empty() |> push (1,"ohai") |> push (2,"world") |> serialize)));;

# 9 "AsnLib/lab4_part5_soln_testing.mlpp"
(* Run the necessary reports *)
let _ = Testing.run_vocareum_report ~detailp:!dp ~gradep:!gp suite_lab4_part5 ;;
