# 1 "AsnLib/lab4_part4_soln_testing.mlpp"
module L = Lab4_part4_soln

open Testing ;;


# 2 "Lib/testing.ml.include"
open Testing ;;

let dp = ref true ;;
let gp = ref false ;;

let argspec = [
  ("-detail", Arg.Set dp, "provide detailed test report to student");
  ("-nodetail", Arg.Clear dp, "do not provide detailed test report to student");
  ("-grade", Arg.Set gp, "generate grading information");
  ("-nograde", Arg.Clear gp, "do not generate grading information")
];;
  
Arg.parse argspec (fun _ -> ()) "see the code" ;;

# 1 "AsnLib/lab4_part4_testing.ml.include"
(*** CS 51 Lab 4 Part 4 ***)
(*** February 24 2015 ***)
(*** Test Suite - Lab 4 ***)

open L ;;

let suite_lab4_part4 = Testing.create_suite "lab4_part4" ;;

let tester = Testing.add_test suite_lab4_part4 ~points:0 ;;



# 0 "Exercise 4A"

# 1 "Exercise 4A"
tester "4A queue" (lazy (let open Queue in "hello" = (empty() |> enqueue "hello" |> front)));;
tester "4A queue 2" (lazy (let open Queue in "hello" = (empty() |> enqueue "hello" |> enqueue "world" |> front)));;
tester "4A queue 3" (lazy (let open Queue in "world" = (empty() |> enqueue "hello" |> dequeue |> enqueue "world" |> front)));;
tester "4A empty queue" (lazy (let open Queue in try ignore(empty() |> dequeue); false with EmptyQueue -> true));;
tester "4A empty queue 2" (lazy (let open Queue in try ignore(empty() |> enqueue "hello" |> dequeue |> enqueue "world" |> dequeue |> front); false with EmptyQueue -> true));;


# 0 "Exercise 4B"

# 1 "Exercise 4B"
tester "4B q" (lazy (let open Queue in "Computer" = (q() |> front)));;
tester "4B q 2" (lazy (let open Queue in "Science" = (q() |> dequeue |> front)));;
tester "4B q 3" (lazy (let open Queue in "51" = (q() |> dequeue |> dequeue |> front)));;
tester "4B q 4" (lazy (let open Queue in try ignore (q() |> dequeue |> dequeue |> dequeue |> front); false with EmptyQueue -> true));;


# 0 "Exercise 4C"

# 1 "Exercise 4C"
tester "4C front_el" (lazy ("Computer" = front_el)) ;;
# 9 "AsnLib/lab4_part4_soln_testing.mlpp"
(* Run the necessary reports *)
let _ = Testing.run_vocareum_report ~detailp:!dp ~gradep:!gp suite_lab4_part4 ;;
