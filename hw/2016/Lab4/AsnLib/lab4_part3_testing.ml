# 1 "AsnLib/lab4_part3_testing.mlpp"
module L = Lab4_part3

open Testing ;;


# 2 "Lib/testing.ml.include"
open Testing ;;

let dp = ref true ;;
let gp = ref false ;;

let argspec = [
  ("-detail", Arg.Set dp, "provide detailed test report to student");
  ("-nodetail", Arg.Clear dp, "do not provide detailed test report to student");
  ("-grade", Arg.Set gp, "generate grading information");
  ("-nograde", Arg.Clear gp, "do not generate grading information")
];;
  
Arg.parse argspec (fun _ -> ()) "see the code" ;;

# 1 "AsnLib/lab4_part3_testing.ml.include"
(*** CS 51 Lab 4 Part 3 ***)
(*** February 24 2015 ***)
(*** Test Suite - Lab 4 ***)

open L ;;

let suite_lab4_part3 = Testing.create_suite "lab4_part3" ;;

let tester = Testing.add_test suite_lab4_part3 ~points:0 ;;



# 0 "Exercise 3A"

# 1 "Exercise 3A"
tester "3A empty" (lazy ([] = (IntListStack.empty ()))) ;;
tester "3A push 1" (lazy ([1] = let open IntListStack in empty() |> push 1));;
tester "3A push 2" (lazy ([2; 1] = let open IntListStack in empty() |> push 1 |> push 2));;
tester "3A push 3" (lazy ([3; 2; 1] = let open IntListStack in empty() |> push 1 |> push 2 |> push 3));;
tester "3A top 0" (lazy (let open IntListStack in try ignore (top (empty())); false with EmptyStack -> true));;
tester "3A top 1" (lazy (1 = let open IntListStack in empty() |> push 1 |> top));;
tester "3A top 2" (lazy (2 = let open IntListStack in empty() |> push 1 |> push 2 |> top));;
tester "3A top 3" (lazy (~-3 = let open IntListStack in empty() |> push 1 |> push 2 |> push ~-3 |> top));;
tester "3A pop 0" (lazy (let open IntListStack in try ignore (pop (empty())); false with EmptyStack -> true));;
tester "3A pop 1" (lazy ([] = let open IntListStack in empty() |> push 1 |> pop));;
tester "3A pop 2" (lazy ([2; 1] = let open IntListStack in empty() |> push 1 |> push 2 |> push ~-3 |> pop));;
tester "3A pop 3" (lazy ([1] = let open IntListStack in empty() |> push 1 |> push 2 |> pop |> push ~-3 |> pop));;


# 0 "Exercise 3B"

# 1 "Exercise 3B"
tester "3B small_stack" (lazy ([1; 5] = small_stack ()));;


# 0 "Exercise 3C"

# 1 "Exercise 3C"
tester "3C last_el" (lazy (1 = last_el));;


# 0 "Exercise 3D"

# 1 "Exercise 3D"
tester "3D invert_stack" (lazy ([5; 1] = invert_stack [1; 5]));;


# 0 "Exercise 3E"

# 1 "Exercise 3E"
tester "3E bad_el" (lazy (5 = bad_el));;


# 0 "Exercise 3F"

# 1 "Exercise 3F"
tester "3F abstract type" (lazy (let open IntListStack in 1 = (empty() |> push 1 |> top)));;
tester "3F abstract type" (lazy (let open IntListStack in empty() = (empty() |> push 1 |> pop)));;
tester "3F exception" (lazy (let open IntListStack in try ignore (empty() |> pop);false with EmptyStack -> true));;


# 0 "Exercise 3G"

# 1 "Exercise 3G"
tester "3G safe_stack" (lazy (1 = (safe_stack() |> SafeIntListStack.top))) ;;
tester "3G safe_stack 2" (lazy (let open SafeIntListStack in 5 = (safe_stack() |> pop |> top))) ;;
tester "3G safe_stack 3" (lazy (let open SafeIntListStack in try ignore(safe_stack() |> pop |> pop |> top);false with EmptyStack -> true)) ;;
# 9 "AsnLib/lab4_part3_testing.mlpp"
(*
Return the Submission ID from a file, if present. Otherwise generate a new
one and save in a file for later iterations to find.
*)
let saved_id =
  let vid_file = "vocareum_lab_submission_id" in
  try
    let f = open_in vid_file in
    let id = Some (int_of_string (input_line f)) in
    close_in f;
    id
  with _ ->
    let x = if !gp then Testing.vocareum_init () else 0 in
    try
      let f = open_out vid_file in
      Printf.fprintf f "%d" x;
      close_out f;
      Some x
    with _ -> Some x;;


(* Run the necessary reports *)
let _ = Testing.run_vocareum_report ~detailp:!dp ~gradep:!gp
                                    ~idoption:saved_id suite_lab4_part3 ;;