# 1 "AsnLib/lab4_part2_soln_testing.mlpp"
module L = Lab4_part2_soln

open Testing ;;


# 2 "Lib/testing.ml.include"
open Testing ;;

let dp = ref true ;;
let gp = ref false ;;

let argspec = [
  ("-detail", Arg.Set dp, "provide detailed test report to student");
  ("-nodetail", Arg.Clear dp, "do not provide detailed test report to student");
  ("-grade", Arg.Set gp, "generate grading information");
  ("-nograde", Arg.Clear gp, "do not generate grading information")
];;
  
Arg.parse argspec (fun _ -> ()) "see the code" ;;

# 1 "AsnLib/lab4_part2_testing.ml.include"
(*** CS 51 Lab 4 Part 2 ***)
(*** February 24 2015 ***)
(*** Test Suite - Lab 4 ***)

open L ;;

let suite_lab4_part2 = Testing.create_suite "lab4_part2" ;;

let tester = Testing.add_test suite_lab4_part2 ~points:0 ;;



# 0 "Exercise 2A"

# 1 "Exercise 2A"
let comp_float x y = abs_float (x -. y) <= epsilon_float ;;

tester "2A least_fav" (lazy (comp_float (Emily.least_favorite_function 4.0 2.5) least_fav));;


# 0 "Exercise 2B"

# 1 "Exercise 2B"
tester "2B most_fav" (lazy (comp_float (Sam.favorite_function 4.0 2.5) most_fav));;


# 0 "Exercise 2C"

# 1 "Exercise 2C"
module Dan : TF =
  struct
    type info = { house:string; name:string }
    let info = { house="New House"; name="Dan" }
    let hometown = "Cambridge"
    let print_info = fun () -> ()
    let favorite_function = fun _ _ -> nan
    let fold = List.fold_right (+)
  end ;;

tester "2C values" (lazy (let _ = Dan.info in true)) ;;
tester "2C values 2" (lazy (let _ = Dan.hometown in true)) ;;
tester "2C fn" (lazy (let _ = Dan.print_info in true)) ;;
tester "2C fn 2" (lazy (let _ = Dan.favorite_function in true));;
tester "2C fn 3" (lazy (let _ = Dan.fold in true));;



# 0 "Exercise 2D"

# 1 "Exercise 2D"
tester "2D emily" (lazy (Emily.fold [3] 0 = TFEmily.fold [3] 0));;
tester "2D sam" (lazy (Sam.fold [3] 0 = TFSam.fold [3] 0));;
# 9 "AsnLib/lab4_part2_soln_testing.mlpp"
(* Run the necessary reports *)
let _ = Testing.run_vocareum_report ~detailp:!dp ~gradep:!gp suite_lab4_part2 ;;
