# 1 "AsnLib/lab4_part1_testing.mlpp"
module L = Lab4_part1

open Testing ;;


# 2 "Lib/testing.ml.include"
open Testing ;;

let dp = ref true ;;
let gp = ref false ;;

let argspec = [
  ("-detail", Arg.Set dp, "provide detailed test report to student");
  ("-nodetail", Arg.Clear dp, "do not provide detailed test report to student");
  ("-grade", Arg.Set gp, "generate grading information");
  ("-nograde", Arg.Clear gp, "do not generate grading information")
];;
  
Arg.parse argspec (fun _ -> ()) "see the code" ;;

# 1 "AsnLib/lab4_part1_testing.ml.include"
(*** CS 51 Lab 4 Part 1 ***)
(*** February 24 2015 ***)
(*** Test Suite - Lab 4 ***)

open L ;;

let suite_lab4_part1 = Testing.create_suite "lab4_part1" ;;

let tester = Testing.add_test suite_lab4_part1 ~points:0 ;;



# 0 "Exercise 1A"

# 1 "Exercise 1A"
let comp_float x y = abs_float (x -. y) <= epsilon_float ;;

let f1 = 56.2 ;;

tester "1A pi" (lazy (Math.pi < 3.2 && Math.pi > 3.1)) ;;
tester "1A cos 0" (lazy (comp_float (cos 0.0) (Math.cos 0.0))) ;;
tester "1A cos pi/2" (lazy (comp_float (cos Math.pi /. 2.0) (Math.cos Math.pi /. 2.0))) ;;
tester "1A cos pi" (lazy (comp_float (cos Math.pi) (Math.cos Math.pi))) ;;
tester "1A sin 0" (lazy (comp_float (sin 0.0) (Math.sin 0.0))) ;;
tester "1A sin pi/2" (lazy (comp_float (sin Math.pi /. 2.0) (Math.sin Math.pi /. 2.0))) ;;
tester "1A sin pi" (lazy (comp_float (sin Math.pi) (Math.sin Math.pi))) ;;
tester "1A sum" (lazy (comp_float (f1 +. 0.0) (Math.sum f1 0.0))) ;;
tester "1A sum 2" (lazy (comp_float (Math.pi +. f1) (Math.sum Math.pi f1))) ;;
tester "1A max None" (lazy (Math.max [] = None));;
tester "1A max one" (lazy (Math.max [f1] = Some f1));;
tester "1A max two" (lazy (Math.max [f1; infinity] = Some infinity));;
tester "1A max infinity" (lazy (Math.max [infinity; f1; neg_infinity] = Some infinity));;


# 0 "Exercise 1B"

# 1 "Exercise 1B"
let deopt x y =
  match x, y with
  | (Some a, Some b) -> comp_float a b
  | _ -> false ;;

let tr = Math.max([Math.cos(Math.pi); Math.sin(Math.pi)]) ;;

tester "1B result" (lazy (deopt tr result)) ;;


# 0 "Exercise 1C"

# 1 "Exercise 1C"
tester "1C result_local_open" (lazy (deopt tr result_local_open));;
# 9 "AsnLib/lab4_part1_testing.mlpp"
(*
Return the Submission ID from a file, if present. Otherwise generate a new
one and save in a file for later iterations to find.
*)
let saved_id =
  let vid_file = "vocareum_lab_submission_id" in
  try
    let f = open_in vid_file in
    let id = Some (int_of_string (input_line f)) in
    close_in f;
    id
  with _ ->
    let x = if !gp then Testing.vocareum_init () else 0 in
    try
      let f = open_out vid_file in
      Printf.fprintf f "%d" x;
      close_out f;
      Some x
    with _ -> Some x;;


(* Run the necessary reports *)
let _ = Testing.run_vocareum_report ~detailp:!dp ~gradep:!gp
                                    ~idoption:saved_id suite_lab4_part1 ;;