rm -rf _build *.byte 2> /dev/null

cp -r $LIB Lib
cp -r $ASNLIB AsnLib
cp AsnLib/_tags _tags

LAB="lab7"

echo "Checking line lengths..."

python Lib/check_width.py "${LAB}.ml"

echo "Compiling with test harness..."

ocamlbuild -no-hygiene -quiet "AsnLib/${LAB}_testing.byte"
rc=$?

# Clean compiled sources
rm -rf Lib AsnLib _tags 2> /dev/null

if [ $rc -ne 0 ]
then
    echo "Compilation failed!"
    echo "*** Test suite lab7 ***"
    echo "submission  : [1/1] passed"
else
    echo "Compilation succeeded; proceeding to unit testing..."
    ./${LAB}_testing.byte -detail -grade
fi
