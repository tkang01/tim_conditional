(*** CS 51 Lab 7 ***)
(*** April 5 2015 ***)
(*** Test Suite - Lab 7 ***)

#ifdef SOLN
module L = Lab7_soln ;;
#else
module L = Lab7 ;;
#endif

open Testing ;;

#include "testing.ml.include"

open L ;;

let suite_lab7 = Testing.create_suite "lab7" ;;

Testing.add_test suite_lab7 "submission" (lazy true);;

let tester = Testing.add_test suite_lab7 ~points:0 ;;

# 0 "Part 1 - Flatland"

let origin = (0., 0.) ;;
let s1 = Square(origin, 4.0) ;;
let r1 = Rect(origin, 4.0, 3.0) ;;
let c1 = Circle(origin, 3.0) ;;

tester "area_adt 1" (lazy (area_adt s1 = 16.)) ;;
tester "area_adt 2" (lazy (area_adt r1 = 12.)) ;;
tester "area_adt 3" (lazy (let c = area_adt c1 in c > 27.9 && c < 28.35)) ;;
tester "list_area_adt" (lazy (list_area_adt [s1;r1;c1] = [area_adt s1; area_adt r1; area_adt c1])) ;;

# 0 "Part 2 - Interfaces, Classes, Objects"

let p = (1., -3.) ;;
let t = (0.5, -0.5) ;;
let r2 = new rect p 3. 4. ;;
let c2 = new circle p 5. ;;
let s2 = new square p 6. ;;

tester "rect area" (lazy (r2#area = 12.)) ;;
tester "rect bounding box" (lazy (r2#bounding_box = (p, (4., 1.)))) ;;
tester "rect center" (lazy (r2#center = (2.5, -1.))) ;;
tester "rect scale" (lazy (r2#scale 0.5; r2#bounding_box = (p, (2.5, -1.)))) ;;
tester "rect translate" (lazy (r2#translate t; r2#center = (2.25, -2.5))) ;;

tester "circle area" (lazy (let c = c2#area in c > 77.5 && c < 78.75));;
tester "circle bounding box" (lazy (c2#bounding_box = ((-4., -8.), (6., 2.))));;
tester "circle center" (lazy (c2#center = p)) ;;
tester "circle translate" (lazy (c2#translate t; c2#center = (1.5, -3.5))) ;;
tester "circle scale" (lazy (c2#scale 0.5; c2#center = (1.5, -3.5) && c2#bounding_box = ((-1.,-6.), (4.,-1.))));;

tester "square area" (lazy (s2#area = 36.));;
tester "square bounding_box" (lazy (s2#bounding_box = (p, (7., 3.))));;
tester "square center" (lazy (s2#center = (4., 0.)));;
tester "square scale" (lazy (s2#scale 0.5; s2#bounding_box = (p, (4., 0.))));;
tester "square translate" (lazy (s2#translate t; s2#center = (3., -2.)));;

tester "area" (lazy (List.map area [r2;s2] = [3.;9.]));;

# 0 "Part 3 - Representation, Inheritance"

let s3 = new square_rect p 6. ;;
let sc = new square_center_scale p 6. ;;

tester "square_rect area" (lazy (s3#area = 36.));;
tester "square_rect bounding_box" (lazy (s3#bounding_box = (p, (7., 3.))));;
tester "square_rect center" (lazy (s3#center = (4., 0.)));;
tester "square_rect scale" (lazy (s3#scale 0.5; s3#bounding_box = (p, (4., 0.))));;
tester "square_rect translate" (lazy (s3#translate t; s3#center = (3., -2.)));;

tester "square_center_scale area" (lazy (sc#area = 36.));;
tester "square_center_scale bounding_box" (lazy (sc#bounding_box = (p, (7., 3.))));;
tester "square_center_scale center" (lazy (sc#center = (4., 0.)));;
tester "square_center_scale translate" (lazy (sc#translate t; sc#center = (4.5, -0.5)));;
tester "square_center_scale scale" (lazy (sc#scale 0.5; sc#bounding_box = ((3., -2.), (6., 1.))));;

# 0 "Part 4 - Subtyping Polymorphism and Dynamic Dispatch"

let rq = new rect_quad p 3. 4. ;;

tester "rect_quad SIDES 1" (lazy (rq#sides = (3., 4., 3., 4.)));;
tester "rect_quad area" (lazy (rq#area = 12.)) ;;
tester "rect_quad bounding box" (lazy (rq#bounding_box = (p, (4., 1.)))) ;;
tester "rect_quad center" (lazy (rq#center = (2.5, -1.))) ;;
tester "rect_quad scale" (lazy (rq#scale 0.5; rq#bounding_box = (p, (2.5, -1.)))) ;;
tester "rect_quad translate" (lazy (rq#translate t; rq#center = (2.25, -2.5))) ;;
tester "rect_quad SIDES 2" (lazy (rq#sides = (1.5, 2., 1.5, 2.)));;

tester "area_list" (lazy (area_list [sc; (rq :> shape)] = [9.; 3.]));;

# 0 "Part 5 - Events and Listeners"

let ev : unit WEvent.event = WEvent.new_event ();;
let counter : int ref = ref 0 ;;
let handler () : unit = incr counter ;;
let id1 = WEvent.add_listener ev handler ;;
let id2 = WEvent.add_listener ev handler ;;

let test_event (x : int) : bool =
  let c = !counter in
  WEvent.fire_event ev () ; !counter = c + x ;;

tester "add_listener" (lazy (id1 <> id2)) ;;
tester "fire_event" (lazy (test_event 2));;
tester "remove_listener 1" (lazy (WEvent.remove_listener ev id2; test_event 1));;
tester "remove_listener 2" (lazy (WEvent.remove_listener ev id1; test_event 0));;

(* Run the necessary reports *)
let _ = Testing.run_vocareum_report ~detailp:!dp ~gradep:!gp suite_lab7 ;;
