open Lab1

(* Exercise 2 *)
let () = assert (exercise2 () = 42)

(* Exercise 6: square_all *)

let () = assert (square_all [] = [])
let () = assert (square_all [1] = [1])
let () = assert (square_all [-1] = [1])
let () = assert (square_all [3; 4; 5] = [9; 16; 25])
let () = assert (square_all [4; -10; 12] = [16; 100; 144])

(* Exercise 7: sum *)

let () = assert (sum [] = 0)
let () = assert (sum [3] = 3)
let () = assert (sum [1; 2; 3; 4; 5] = 15)
let () = assert (sum [-1; -2; -3; -4; -5] = -15)
let () = assert (sum [1; -2; 3; -4; 5] = 3)

(* Exercise 8: max_list *)

let () = assert (max_list [] = None)
let () = assert (max_list [0] = Some 0)
let () = assert (max_list [1; 2; 3] = Some 3)
let () = assert (max_list [-1; 1; 0] = Some 1)
let () = assert (max_list [-10; -3; -42] = Some (-3))
let () = assert (max_list [-5; 0; 3; 3; 3] = Some 3)
let () = assert (max_list [0; 1; 0; 1; 0] = Some 1)

(* Exercise 9: zip *)

let () = assert (zip [] [] = Some [])
let () = assert (zip [] [5] = None)
let () = assert (zip [3] [] = None)
let () = assert (zip [1] [2; 3; 4] = None)
let () = assert (zip [1] [1] = Some [(1, 1)])
let () = assert (zip [1; 2; 3] [4; 5; 6] = Some [(1, 4); (2, 5); (3, 6)])

(* Exercise 10: dotprod *)

let () = assert (prods [] = [])
let () = assert (prods [(2, -2)] = [-4])
let () = assert (prods [(1, -4); (-2, -5); (3, 6)] = [-4; 10; 18])

let () = assert (dotprod [] [] = Some 0)
let () = assert (dotprod [] [5] = None)
let () = assert (dotprod [5] [] = None)
let () = assert (dotprod [1; 2; 3] [1; 2; 3; 4] = None)
let () = assert (dotprod [6] [7] = Some 42)
let () = assert (dotprod [1; 2; 3] [0; 1; 2] = Some 8)
let () = assert (dotprod [1; -2; -3] [0; 1; -2] = Some 4)

(* Exercise 11: sum_ho *)

let () = assert (sum_ho [] = 0)
let () = assert (sum_ho [3] = 3)
let () = assert (sum_ho [1; 2; 3; 4; 5] = 15)
let () = assert (sum_ho [-1; -2; -3; -4; -5] = -15)
let () = assert (sum_ho [1; -2; 3; -4; 5] = 3)

(* Exercise 12: prods_ho *)

let () = assert (prods_ho [] = [])
let () = assert (prods_ho [(2, -2)] = [-4])
let () = assert (prods_ho [(1, -4); (-2, -5); (3, 6)] = [-4; 10; 18])

(* Exercise 13: zip_ho *)

let () = assert (zip_ho [] [] = Some [])
let () = assert (zip_ho [] [5] = None)
let () = assert (zip_ho [3] [] = None)
let () = assert (zip_ho [1] [2; 3; 4] = None)
let () = assert (zip_ho [1] [1] = Some [(1, 1)])
let () = assert (zip_ho [1; 2; 3] [4; 5; 6] = Some [(1, 4); (2, 5); (3, 6)])

(* Exercise 14: evens *)

let () = assert (evens [] = [])
let () = assert (evens [1] = [])
let () = assert (evens [2] = [2])
let () = assert (evens [1; 2; 3; 4; 5; 6] = [2; 4; 6])
let () = assert (evens [1; 4; 5; -2] = [4; -2])
