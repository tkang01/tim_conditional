open Helpers
open WorldObject
open WorldObjectI

(* ### Part 3 Actions ### *)
let pollen_theft_amount = 1000

(* ### Part 4 Aging ### *)
let bear_starting_life = 20

(* ### Part 2 Movement ### *)
let bear_inverse_speed = Some 10

class bear p (home : world_object_i) hive : Movable.movable_t =
object (self)
  inherit Movable.movable p bear_inverse_speed as super

  (******************************)
  (***** Instance Variables *****)
  (******************************)

  (* ### TODO: Part 3 Actions ### *)
  val mutable stolen_honey = 0

  (* ### TODO: Part 6 Events ### *)
  val mutable life = bear_starting_life

  (***********************)
  (***** Initializer *****)
  (***********************)

  (* ### TODO: Part 3 Actions ### *)
  initializer
    self#register_handler World.action_event (fun _ -> self#do_action)

  (**************************)
  (***** Event Handlers *****)
  (**************************)

  (* ### TODO: Part 3 Actions ### *)
  method private do_action : unit =
    if self#get_pos = hive#get_pos then begin
      let amt = hive#forfeit_honey pollen_theft_amount (self :> world_object_i) in
      stolen_honey <- stolen_honey + amt
    end;

  (* ### TODO: Part 6 Custom Events ### *)
    if self#get_pos = home#get_pos && stolen_honey > 0 then begin
      stolen_honey <-
        List.length (home#receive_pollen (Helpers.replicate stolen_honey 0)) ;
      if hive#get_pollen < (pollen_theft_amount/2) then self#die
    end

  (********************************)
  (***** WorldObjectI Methods *****)
  (********************************)

  (* ### TODO: Part 1 Basic ### *)
  method get_name = "bear"

  method draw =
    self#draw_circle (Graphics.rgb 170 130 110) Graphics.black
      (string_of_int stolen_honey)

  method draw_z_axis = 40

  (* ### TODO: Part 6 Custom Events ### *)
  method receive_sting =
    life <- life - 1 ;
    if life = 0 then self#die

  (***************************)
  (***** Movable Methods *****)
  (***************************)

  (* ### TODO: Part 2 Movement ### *)
  (* ### TODO: Part 6 Custom Events ### *)
  method next_direction =
    if stolen_honey = 0 then
      World.direction_from_to self#get_pos hive#get_pos
    else
      World.direction_from_to self#get_pos home#get_pos

end
