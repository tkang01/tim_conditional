rm -rf _build *.byte 2> /dev/null

cp -r $LIB Lib
cp -r $ASNLIB AsnLib
cp AsnLib/_tags _tags

echo "Checking line lengths..."
python Lib/check_width.py *.ml

# echo "Compiling with test harness..."
echo "No automated tests for pset7"

# ocamlbuild -no-hygiene -quiet Main.byte
# rc=$?

# # Clean compiled sources
# rm -rf Lib AsnLib _tags 2> /dev/null

# if [ $rc -ne 0 ]
# then
#     echo "Compilation failed"
#     exit 1
# else
#     echo "Compilation succeeded; proceeding to unit testing..."
# fi

