#! /usr/bin/env python

import os
from subprocess import call
import subprocess
from zipfile import ZipFile
import sys
import shutil
import csv

OUTPUT_DIRECTORY = 'grading7files'
BYTE_FILE = 'Main.byte'
exit_code = 0
PARTS = range(1,7)
GRADE_HEADERS = ['student'] + PARTS

class Chdir:
	def __init__(self, newPath):
		self.savedPath = os.getcwd()
		os.chdir(newPath)

	def __del__(self):
		os.chdir(self.savedPath)

def compilecode():
	call(["rm", "-rf", "_build *.byte"])
	return call(["ocamlbuild", BYTE_FILE])

"""

"""
def grade(student, directory):
	change = Chdir(directory)
	rval = compilecode()

	grades = {"student":student}

	if rval != 0:
		print "Compilation Failed. Proceeding to next student."
		return grades

	for part in PARTS:
		print "Running ./Main.byte part{0}".format(part)
		call(["./{0}".format(BYTE_FILE), "part{0}".format(part)])
		inp = raw_input("Enter grade for part{0}, \
						or 'r' to re-run: ".format(part))
		while True:
			try:
				if not 'r' in inp.lower():
					grades[part] = int(inp)
					break
				else:
					call(["./{0}".format(BYTE_FILE), "part{0}".format(part)])
					inp = raw_input("Enter grade for part{0},\
										 or 'r' to re-run: ".format(part))
			except Exception as e:
				print e
				inp = raw_input("Bad entry.\
								 Enter grade for part{0}: ".format(part))

	return grades


"""
Build the list of directories containing the downloaded work.
Better hope that Vocareum doesn't change the file structure!
"""
def build_grading_paths(root_dir):
	student_list = filter(lambda s : not '.csv'\
								 in s, os.listdir(OUTPUT_DIRECTORY))
	
	directory_dict = {s: '{0}/{1}/{2}'.format(OUTPUT_DIRECTORY,\
						 s, os.listdir(OUTPUT_DIRECTORY + '/' + s)[0]) \
						for s in student_list}
	return directory_dict

def menu(dirdict):
	print "Please choose submission(s) to grade:"
	keys = dirdict.keys()
	for i, d in enumerate(dirdict.keys(), start=0):
		print "{0}: {1}".format(i,d)
	print "A: grade all"
	print "E: exit"
	entry = raw_input("Selection: ")
	while True:
		if entry.upper() == 'A':
			return keys
		if entry.upper() == 'E':
			return []
		else:
			try:
				entry = int(entry)
				return keys[entry:entry+1]
			except:
				entry = raw_input('Bad entry, try again: ')


def run():
	if len(sys.argv) != 2 and not os.path.isdir(OUTPUT_DIRECTORY):
		print "Usage: ./grader.py </path/to/downloads.zip>"
		exit_code = 1
		return

	if os.path.isdir(OUTPUT_DIRECTORY) and len(sys.argv) > 1:
		print "Warning: grading7files will be not overriden."
		print "To update unzipped files, please delete or rename that directory, then restart."

		exit_code = 1
		return

	if len(sys.argv) == 2:
		fname = sys.argv[1]

		if not fname.endswith(".zip"):
			print "Usage: argument must be a .zip archive"
			exit_code = 1
			return

		with ZipFile(fname) as zipf:
			zipf.extractall()

		# relocate files to a fixed directory for user-friendly inspection
		shutil.move(fname.split(".zip")[0] , OUTPUT_DIRECTORY)

		with open("{0}/grades.csv".format(OUTPUT_DIRECTORY), 'w') as f:
			writer = csv.DictWriter(f, fieldnames=GRADE_HEADERS)
			writer.writeheader()

	dir_dict = build_grading_paths(OUTPUT_DIRECTORY)
	
	while True:
		selections =  menu(dir_dict)

		if len(selections) == 0:
			break

		for selection in selections:
			print "grading"
			grades = grade(selection, dir_dict[selection])
			with open("{0}/grades.csv".format(OUTPUT_DIRECTORY, selection), 'a') as f:
				writer = csv.DictWriter(f, fieldnames = GRADE_HEADERS)
				writer.writerow(grades)


	print "###########################"
	print "Don't forget to check for extra credit!"
	print "Thanks for using the grading script!"
	return

if __name__ == "__main__":
	run()
	exit(exit_code)





