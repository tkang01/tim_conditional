rm -rf _build *.byte 2> /dev/null

cp -r $LIB Lib
cp -r $ASNLIB AsnLib
cp AsnLib/_tags _tags

LAB="lab3"

echo "Checking line lengths..."

python Lib/check_width.py "${LAB}.ml"

echo "Checking for evil..."

for file in *.ml
do
	python Lib/check_evil.py $file

	if [ $? -ne 0 ]
	then
		echo "Malicious code detected in" $file

		# Clean up
		rm -rf Lib AsnLib
		exit 1
	fi
done

echo "Compiling with test harness..."

ocamlbuild -no-hygiene -quiet "AsnLib/${LAB}_testing.byte"
rc=$?

# Clean compiled sources
rm -rf Lib AsnLib _tags 2> /dev/null

if [ $rc -ne 0 ]
then
    echo "Compilation failed. However, your submission was accepted."
    echo "*** Test suite lab3 ***"
    echo "submission : [1/1] passed"
else
    echo "Compilation succeeded; your submission was received! Proceeding to unit testing..."
    ./${LAB}_testing.byte -detail -nograde
fi
