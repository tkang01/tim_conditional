\documentclass{amsart}

\input{../LaTeX/cs51-preamble}
\include{graphics}
\usepackage{float}

\title[CS51 Problem Set 0]{CS51 Problem Set 0:\\Native Windows Installation}
%\author{Stuart M. Shieber}
\date{\today}

\begin{document}

\maketitle

\linenumbers

\section{Windows Setup}

\subsection{Determine what version of Windows you are using}

On Windows 10 type in ``system information'' into the windows search bar.
Click on the system information app, and there should be a summary of
what architecture you are using (32 bit or 64bit). From here on,
install the appropriate version of software that matches your system.

\subsection{Install git shell (adapted from CS 109
instructions)}

Follow this link: \url{https://git-scm.com/downloads}. Click install
for Windows. An \texttt{.exe} file will download. Run it. Accept all
defaults in the GUI installer that opens. A few defaults that you must
be \textbf{certain} to follow can be found in Figures~1, 2, 3, and 4,
which you should reference as you move through the installer screens.

\begin{figure}[H]
\centering
\includegraphics[scale=0.4]{gitbash.png}
\caption{This makes sure that you get a bash shell to use. }
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.4]{gitlines.png}
\caption{This ensures the proper default line-encoding conversion.
}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.35]{gitlinux.png}
\caption{This uses a better terminal emulator than the one shipped on windows.
}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.35]{gitprocess.png}
\caption{If you see this message, just click continue.}
\end{figure}

\clearpage

\subsection{Install make}
Follow this link: \url{http://gnuwin32.sourceforge.net/packages/make.htm}.
Click on the setup link under ``Complete package, except source''.
Another .exe file will download, run that. If you follow all default
options, the confirmation page before you install \texttt{make} should look like
Figure~\ref{fig:makeinstaller}.
%
\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.35]{make.png}
\end{center}
\caption{make installer}\label{fig:makeinstaller}
\end{figure}

Note the directory in which \texttt{make} will be installed, as you
will later need to add that directory to the System Path in
Windows. Click ``install'', then wait for the process to finish. After
it completes, go to your Control Panel and open up ``System
Choices''. Click on ``Advanced system settings'', so that you see the System
Properties window, which should look like Figure~\ref{fig:controlpanel}:
%
\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.20]{controlp.png}
\end{center}
\caption{Control panel and system properties}\label{fig:controlpanel}
\end{figure}

Click on ``Environmental Variables'', as highlighted at the
right of Figure~\ref{fig:controlpanel}. Click on the path variable under System variables and choose to
edit it, as highlighted in Figure~\ref{fig:path}.

\begin{figure}[H]
\centering
\includegraphics[scale=0.35]{envvars.png}
\caption{Finding Path environment variable to edit.}\label{fig:path}
\end{figure}

A page similar to Figure~\ref{fig:pathedit} should pop up. 

\begin{figure}[H]
\centering
\includegraphics[scale=0.35]{pathedit.png}
\caption{Altering your Windows Path}\label{fig:pathedit}
\end{figure}

Add one more path on the bottom by clicking New, as highlighted in
Figure~\ref{fig:pathedit}.\footnote{As noted by Nicholas on Piazza: ``Just a heads up--the path variable editor shown in the Windows setup documentation is new to Windows 10's November 2015 release. In case some out there haven't made the leap yet: you'll need to scroll all the way to the end of the text in the box, add a semicolon if one isn't already there, then type the path.''} For most people, the path that you should
paste is highlighted in Figure~\ref{fig:pathedit}. To ensure that this
path is correct, search for make.exe on your computer, to determine
where it was saved.

Save and apply all those settings.

\newpage
\subsection{Install OCaml and \texttt{opam}}
Go to this link: \url{http://protz.github.io/ocaml-installer/} and install the
proper version of OCaml for your machine. (If you have a 64bit system,
choose the largest link that says ``64bit''; if you have a 32bit system,
choose the first smaller link immediately below.) When running the GUI
installer, you have to change one default option, where you uncheck
cygwin installation:

\begin{figure}[H]
\centering
\includegraphics[scale=0.4]{ocamlinstall.png}
\caption{Installing OCaml}
\end{figure}

The OCaml installer adds to your path automatically, so you don't need
to follow the same steps for make. From there, everything should be set
up.

\subsection{Testing to see if things work:}

If you press the Windows key, and then type \texttt{git bash} and run \texttt{git bash},
you should be able to navigate around your file structure using Unix
commands like \texttt{ls}, \texttt{cd}, etc.

Proceed through the rest of the problem set. If you run into issues, check Piazza
to see if they have been rectified. If not, then post a question of your
own tagged with the \texttt{problem\_set0} folder and the \texttt{windows\_setup} folder.

\end{document}
