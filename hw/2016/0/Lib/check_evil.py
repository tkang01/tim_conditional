#!/usr/bin/env python
# CS51 Tools
# Check for evil code

import re
import sys

exit_code = 0

def check(filename):
    global exit_code
    try:
        buff = open(filename).read()
    except:
        print >> sys.stderr, 'Failed to open "%s" for reading' % filename
        exit_code = 2
        return
    re.I
    match = re.search('(open\s+sys)|(module[A-Za-z0-9._+:\-\'\s]*=\s*Sys)|(Sys.[^(time)])', buff)
    if (match):
        print >> sys.stderr, 'Attempt to use Sys module detected in: %s: %s' % (filename, match.group(0))
        exit_code = 1
    return


if __name__ == '__main__':
    if len(sys.argv) < 1:
        print >> sys.stderr, 'Usage: check_evil [files]'
    for filename in sys.argv[1:]:
        check(filename)

    exit(exit_code)
