## Student Procedures
### Determine What Version of windows you are using

On Windows 10 type in system information into the windows search bar.  Click on the system information app, and there should be a summary of what architecture you are using (32 bit or 64bit).  From here on out, install the appropriate version of software that matches your system. 

### Install git shell (adapted from CS 109 instructions)

Follow this link: https://git-scm.com/downloads.  Click install for windows.  An .exe file will be downloaded. Run it.  Accept all defaults in the GUI installer that opens. A few defaults that you must be **certain** to follow:

This makes sure that you get a bash shell to use.
![git bash](images/git_bash.png)

This ensures the proper default line-encoding conversion.
![git encoding](images/git_lines.png)

This uses a better terminal emulator than the one shipped on windows.
![git linux](images/git_linux.png)

If you see this message, just click continue.
![git process](images/git_process.png)


### Install make

Follow this link: http://gnuwin32.sourceforge.net/packages/make.htm .  Click on the setup link under "Complete package, except source".  Another .exe file will download, run that.  If you folllow all default options, the confirmation page before you install make should look like this. 

![make installer](images/make.png)

Note the directory to which `make` will be installed, as we will now need to add that directory to the System Path in Windows. Click install, then wait for the process to finish. After it completes, go to your control panel, and open up the system choices.  

This is the depth of navigation you need to go through with the control panel.  Now click on Advanced system setup.
![control panel process](images/control_p.png)

You should be taken to the following page.  Click on Environmental Variables.  Now you need to click on the path variable under System variables and choose to edit it.
![env_vars](images/env_vars.png)

Now a page similar to this should pop up.  Add one more path on the bottom.  For most people, the path that you should paste is highlighted below.  To ensure that this path is correct, search for make.exe on your computer, to determine where it was saved.  Save and apply all those settings.
![path](images/path_edit.png) 

### Install OCaml and opam

Go to this link: http://protz.github.io/ocaml-installer/ and install the proper version of ocaml for your machine (if you have a 64bit system, choose the largest link that says "64bit"; if you have a 32bit system, choose the 1st smaller link immediately below).  When running the GUI installer, you have to change one default option, where you uncheck cygwin installation:

![ocaml install](images/ocaml_install.png)

The OCaml installer adds to your path automatically, so you don't need to follow the same steps for make.  From there, everything should be set up.  

### Testing to see if things work: 

If you press the windows key, and then type git bash and run git bash, you should be able to navigate around your file structure using Unix commands like ls, cd, etc.  

Run through the rest of the pset.  If you run into issues, check Piazza to see if they have been rectified.  If not, then post a question of your own, make sure to specifiy the windows_setup folder.