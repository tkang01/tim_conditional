\documentclass{amsart}

\input{../LaTeX/cs51-preamble}
\include{graphics}

\usepackage{bbding}

\title[CS51 Problem Set 5]{CS51 Problem Set 5:\\Moogle}
%\author{Stuart M. Shieber}
\date{\today}

\begin{document}

\maketitle

\linenumbers

\textbf{This problem set is due April 1, 2016 at 5:00pm EST.}

\textbf{You may work with a partner on this problem set. If you choose to work 
with a partner, you can find instructions for configuring 
Vocareum \href{http://tiny.cc/cs51reference}{here}.}

\textbf{If you do not choose to work with a partner,
you must specify that decision on Vocareum to
receive a repository invitation.}

\setcounter{section}{-1}
\section{Introduction}

\subsection{Description}

The goal of this problem set is to build a small web search engine
called Moogle. \emph{You will be able (and encouraged) to work with a
partner for this problem set.}

You'll start with a working skeleton of the search engine web server,
which we've provided. Your job is to implement some key components of
Moogle in three stages:

\begin{enumerate}

\item First, you will implement a \emph{web crawler}, which retrieves
  web pages by following links from page to page, building up an
  \emph{index} of the pages as it crawls. The index stores a mapping
  from words appearing on the pages to the pages that they appear on.

\item The index makes use of naive default implementations of sets and
  dictionaries that we provide. But these implementations don't scale
  very well, so you'll reimplement sets using more
  efficient abstractions.

  % You'll be implementing a 2-3 tree, which is a type of balanced
  % tree. 

\item In order to gauge the benefit of the new implementations, you
  will design, develop, test, and deploy a performance testing system
  for timing the performance of the search engine, both in its
  crawling and query evaluation. Unlike in previous problem sets (and
  in previous parts of this problem set), we provide no skeleton code
  to carry out this part of the problem set. Part of the point of this
  problem set is to allow you the freedom to experiment with \emph{ab
    initio} design. The deliverable for this part of the problem set,
  in addition to any code that you write, is a report on the relative
  performance of the two implementations.

\end{enumerate}

As a further completely optional challenge, you can follow our
walkthroughs on how to implement two versions of Google's PageRank
algorithm which the server can use to sort the links returned for a
query so that more ``important'' links are returned first.

\subsection{Testing explanation}\label{sec:testing}

Testing your code is required. You will be using a similar method for
testing as you did on the previous problem set. All the functors you
write will have a \verb|run_tests : unit -> unit| function. See the
examples of tests in \texttt{dict.ml} (the tests for \texttt{remove}),
and see how to run them by looking at the very bottom of
\texttt{dict.ml}.

For testing the crawler, we have provided three sample dumps of
webpages.

\begin{itemize}
\item
  \texttt{simple-html}: a directory containing a small list (7 pages) of
  pages to test your crawler.
\item
  \texttt{html}: a directory containing a medium-sized list (20
  pages) of pages (the Ocaml manual).
\item
  \texttt{wiki}: a directory containing a large list (224 pages) of
  pages to test your crawler, once you have finished implementing your
  sets and dictionaries. These pages have been scraped from Wikipedia,
  using
  \texttt{http://en.wikipedia.org/wiki/Teenage\_Mutant\_Ninja\_Turtles}
  as the start point. \textbf{Note: Even with optimized dictionaries and
  sets, indexing all the pages (224) in the \texttt{wiki} directory may
  take several minutes.}
\end{itemize}

\subsection{Words of warning}

Please heed these three important words of warning.

\begin{enumerate}

\item This project may present new difficulties as compared to
  previous problem sets because you need to read and understand all of
  the code that we have given you, as well as write new
  code. Consequently, we highly recommend you start as early as
  possible. This is not the kind of problem set you can expect to
  complete at the last minute.

\item Once you've completed the assignment, you'll have implemented a
  web crawler, which you might be inclined to let loose on the open
  web. However, you shouldn't just point Moogle at an arbitrary web
  site and start crawling it unless you understand the
  \href{http://en.wikipedia.org/wiki/Robots.txt}{robots.txt}
  protocol. This protocol lets web sites tell crawlers (like Google's
  or Yahoo's or Bing's) which subdirectories they are allowed to
  index, at what rate they can connect to the server, how frequently
  they can index the material, etc. If you don't follow this protocol,
  then you are likely to have some very angry people on your
  doorsteps. So to be on the safe side, you should restrict your
  crawling to your local disk.

\item Setting up and configuring a real web server demands a lot of
  knowledge about security. In particular, we do not recommend that
  you poke holes in your machine's firewall so that you can talk to
  Moogle from a remote destination. Rather, you should restrict your
  interaction to a local machine.

\end{enumerate}

\subsection{Getting started}

\newcommand{\pencil}{\raisebox{-.5ex}{\PencilRight}}
\newcommand{\dotouch}[1]{\item[\pencil] \textbf{\texttt{#1}:}}
\newcommand{\notouch}[1]{\item \textbf{\texttt{#1}:}}

To download the problem set, as before, follow the instructions found
\href{http://tiny.cc/cs51reference}{here}. Within the directory you
receive, you will find a set of \texttt{.ml} files that make up the
project source. You will also find three directories of web pages
that you can use for testing: \texttt{simple-html}, \texttt{html}, and
\texttt{wiki}. Below is a brief description of the contents of each
file. You should only have to make changes to the files marked like
\pencil~this, though you may well need to refer to the others for
important functionality for you to use.

\begin{itemize}
\notouch{moogle.ml} The main code for the Moogle server. 
\dotouch{crawl.ml} Includes a stub for the crawler that you will 
  need to complete. 
\notouch{crawler_services.ml\textnormal{ and }.mli}
  Crawler services needed to build the web index. This includes
  definitions of link and page datatypes, a function for fetching a
  page given a link, and the values of the command line arguments
  (e.g., the initial link, the number of pages to search, and the
  server port).
\notouch{query.ml} A datatype for Moogle queries
  and a function for evaluating a query given a web index.
\dotouch{myset.ml} An interface and simple implementation of a
  set abstract datatype. You'll be adding a more efficient
  implementation based on dictionaries.
\notouch{dict.ml} An interface and simple implementation of a
  dictionary abstract datatype.
\notouch{order.ml} Definitions for an order 
  datatype used to compare values. 
\notouch{graph.ml} Definitions for a graph abstract
  data type, including a graph signature, a node signature, and a
  functor for building graphs from a node module.
\notouch{nodescore.ml} Definitions for node scores
  maps, which are used in the Challenge Questions as part of the
  page-rank algorithm.
\notouch{pagerank.ml} Skeleton code for the
  page-rank algorithm including a dummy indegree algorithm for
  computing page ranks. You will be editing this if you decide to do
  the Challenge Questions.
\end{itemize}

\subsection{Build Moogle}

In its initial state, the code that we provide implements a
functioning web server, but it doesn't yet crawl over a set of files
and index them, so searching never generates any
results. Nevertheless, it's useful to try it to make sure that
everything is working.

Compile Moogle via the command line and start it up
from a terminal or shell by executing the following commands:

\begin{verbatim}
    ocamlbuild moogle.byte
    ./moogle.byte 8080 42 simple-html/index.html
\end{verbatim}

The first command line argument (8080) represents the \emph{port} that
your moogle server listens to. Unless you know what you are doing, you
should leave it as 8080.

The second command line argument (42) represents the \emph{number of
  pages to index}. Moogle will index no more than that many pages.

The final command line argument (simple-html/index.html) indicates the
\emph{page from which your crawler should start.} Moogle will only
index pages that are on your local file system (inside the
\texttt{simple-html}, \texttt{html}, or \texttt{wiki} directories).

You should see that the server starts and then prints some debugging
information ending with the lines:

\begin{verbatim}
    Starting Moogle on port 8080.
    Press Ctrl-c to terminate Moogle.
\end{verbatim}

Now try to connect to Moogle with your web browser. Open up a browser
and type in the following URL:

\begin{verbatim}
    http://localhost:8080
\end{verbatim}

You may need to try a different port (e.g., 8081, 8082, etc.) to find a
free port.

Once you connect to the server, you should see a web page that says
``Welcome to Moogle!''. You can try to type in a query and if you do,
you'll see an empty response. There are known problems with trying to
view Moogle from Safari. If you experience these problems, try another
web browser.

The Moogle page lets you enter a search query. The query is either
a word (e.g., ``moo''), a conjunctive query (e.g., ``moo AND cow''), or
a disjunctive query (e.g., ``moo OR cow''). By default, queries are
treated as conjunctive so if you write ``moo cow'', you should get back
all of the pages that have both the word ``moo'' and the word ``cow''.

The query is parsed and sent back to Moogle as an http request. At
this point, Moogle computes the set of URLs (drawn from those pages it
has indexed) whose web pages satisfy the query. For instance, if we
have a query ``moo OR cow'', then Moogle would compute the set of
URLs of all pages that contain either ``moo'' or ``cow''.

After computing the set of URLs that satisfy the user's query, Moogle
builds an html page response and sends the page back to the user to
display in their web-browser.

\section{Implementing the crawler}

Your first task is to implement the web crawler and build the search
index. The search index is a \emph{dictionary}, a finite mapping that
maps each word found on the crawled pages to the set of pages on which it
occurs. The dummy \texttt{crawl} function in the file
\texttt{crawl.ml} that we provide just returns an empty
dictionary. You will need to replace it with a proper implementation
that crawls all of the pages and builds an appropriate
\texttt{WordDict.dict} dictionary mapping the words on the pages to
the sets of links).

You will find the definitions in the \texttt{Crawler_services} module
(in the file \texttt{crawler\_services.mli} and \texttt{.ml})
useful. For instance, you will notice that the initial URL provided on
the command line can be found in the variable
\texttt{Crawler_services.initial\_link}. You should use the function
\texttt{Crawler_services.get\_page} to fetch a page given a link. The
data returned, of type \texttt{Crawler\_services.page}, contains the
URL for the page, a list of links that occur on that page, and a list
of words that occur on that page. You need to update your
\texttt{WordDict.dict} so that it maps each word on the page to a set
that includes the page's URL. Then you need to continue crawling the
other links on the page recursively. Of course, you need to figure out
how to avoid an infinite loop when one page links to another and vice
versa, and for efficiency, you should only visit a page at most once.

The variable \texttt{Crawler_services.num\_pages\_to\_search} contains
the command-line argument specifying how many unique pages you should
crawl. So you'll want to stop crawling after you've seen that number
of pages, or you run out of links to process.

The module \texttt{WordDict} provides operations for building and
manipulating dictionaries mapping words (strings) to sets of links. It
is implemented in \texttt{crawl.ml} by calling a functor and passing it an
argument where keys are defined to be strings, and values are defined
to be \texttt{LinkSet.set}. The interface for \texttt{WordDict} can be
found in \texttt{dict.ml}.

The module \texttt{LinkSet} is defined in \texttt{pagerank.ml} and like
\texttt{WordDict}, it is built using a set functor, where the element
type is specified to be a \texttt{link}. The interface for
\texttt{LinkSet} can be found in the \texttt{myset.ml} file.

Running the crawler in the top-level loop won't really be possible, so
to test and debug your crawler, you will want to compile via command
line, and add code that prints things out. See the OCaml documentation
for the \texttt{Printf.printf} functions for how to do this, and note
that all of our abstract types (sets, dictionaries, etc.) provide
operations for converting values to strings for easy printing.

Once you are confident your crawler works, run it on the small html
directory:

\begin{verbatim}
    ./moogle.byte 8080 7 simple-html/index.html
\end{verbatim}

The \texttt{simple-html} corpus contains seven very small html files
that you can inspect yourself, and you should compare that against the
output of your crawler.

If you attempt to run your crawler on the larger sets of pages, you
may notice that your crawler takes a very long time to build up your
index.  The dummy list implementations of dictionaries and sets that
we provide do not scale very well.

\textbf{Note:} Unless you do a tiny bit of extra work, your index will
be case sensitive. This is fine, but may be confusing when testing, so
keep it in mind.

\section{Sets as dictionaries}

A \emph{set} is a data structure that stores \emph{keys} where keys
cannot be duplicated (unlike a \emph{list} which may store
duplicates).  Moogle uses lots of sets. For example, the web index
maps words to sets of URLs, and the query evaluator manipulates sets
of URLs. It would be useful to have sets be faster than the naive
list-based implementation we have provided in \texttt{myset.ml}.  To
improve on that implementation, you will build sets of \emph{ordered
keys}, keys on which there is provided a total ordering.

As lazy computer scientists, you will want you to use what you already
have for dictionaries to build sets. You will write a functor which,
when given a \texttt{COMPARABLE} module, produces a \texttt{SET}
module by building and adapting an appropriate \texttt{DICT} module
(provided in \texttt{dict.ml}).

Part of the problem is to figure out how you can implement sets in
terms of dictionaries. As a hint, you may want to think about a
dictionary, abstractly, as a \textbf{set} of (key,value) pairs.

For this part, you will need to uncomment the \texttt{DictSet} functor
in the file \texttt{myset.ml}. The first step is to build a suitable
dictionary \texttt{D} by calling \texttt{Dict.Make}. The key question
to figure out is what to pass in for the type definitions of keys and
values.  Then you need to build the set definitions in terms of the
operations provided by \texttt{D\ :\ DICT}.

You should make sure to write a set of unit tests that exercise your
implementation, following the testing explanation in
Section~\ref{sec:testing}. You must test \emph{all} your functions
(except for the string functions).

\section{Try your crawler}

Finally, you should be able to easily change the \texttt{Make} functor
at the bottom of \texttt{myset.ml} so that it uses the
\texttt{DictSet} functor instead of \texttt{ListSet}. Make the change
and try out your new crawler on the three corpora. The startup code
prints out the crawling time, so you can get a sense of which
implementation is faster. Try it on the larger test set in the
\texttt{wiki} directory in addition to the \texttt{html} and
\texttt{simple-html} directories with:

\begin{verbatim}
    ./moogle.byte 8080 42 wiki/Teenage_Mutant_Ninja_Turtles
\end{verbatim}

If you are confident everything is working, try changing 42 to 224.
(This may take several minutes to crawl and index). You're done! We've
provided you with a debug flag at the top of \texttt{moogle.ml}, that
can be set to either true or false. If debug is set to true, Moogle
prints out debugging information using the string conversion functions
defined in your dictionary and set implementations. This is set to
true by default.


\section{Testing the performance of the implementations}

You now have two possible implementations of sets (\texttt{ListSet}
and \texttt{DictSet}) that can be used throughout the implementation
of the crawler and search engine. Write some code (we provide no
skeleton or code of any kind; you're on your own) to time the crawling
of the three provided corpora with each of the set implementations, as
well as testing query performance. You may find the \texttt{time} and
\texttt{gettimeofday} functons in the \texttt{Sys} and \texttt{Unix}
modules useful for this purpose. (In general, we automatically reject
submitted code on Vocareum that uses the \texttt{Sys} and
\texttt{Unix} modules but we make an exception for direct calls to
these particular functions when explicitly specified with the
appropriate module prefix -- no global or local opens.)

Write up a short report outlining what you discover about the impact
of the two implementations on the performance of web crawling and
query response.

This portion of the problem set is purposefully underspecified. There
is no ``right answer'' that we are looking for. It is up to you to
determine what makes sense to evaluate the performance of the two set
implementations. It is up to you to decide how to present your results
in clear, well-structured, cogent prose.

You should submit the writeup to Vocareum with the rest of your code
as a text file named \texttt{writeup.txt} or \texttt{writeup.md}. We
recommend that you use
\href{https://daringfireball.net/projects/markdown/}{Markdown format}
for the writeup. (See
\href{http://blogs.harvard.edu/pamphlet/2014/08/29/switching-to-markdown-for-scholarly-article-production/}{here}
for why.) There are
\href{https://www.google.com/#q=markdown+tools}{many tools} for
writing and rendering Markdown files. Some of our favorites are:
ByWord, Marked, MultiMarkdown Composer, Sublime Text, and the Swiss
army knife of file format conversion tools,
\href{http://pandoc.org/}{pandoc}. If you use Markdown, you can
optionally include the rendered version as a PDF file in your
submission.

\section{Submit!}

To submit the problem set, follow the instructions found
\href{http://tiny.cc/cs51reference}{here}.  Note that only one of your
partners needs to submit the problem set on Vocareum. (Don't forget to
submit the writeup as well.)

If you find yourself with extra time on your hands, you may want to
try the challenge problem in the next section and resubmit
after. Otherwise, that's it for Problem Set 5!

\section{Challenge: PageRank}

\textcolor{red}{Only do this part if you know for sure the rest of
  your problem set is working and you have done an exemplary job on the
  performance writeup. You may do one or both of these rankers for
  fun!}

We will now apply our knowledge of ADTs and graphs to explore solutions
to a compelling problem: finding ``important'' nodes in graphs like the
Internet, or the set of pages that you're crawling with Moogle.

The concept of assigning a measure of importance to nodes is very useful
in designing search algorithms, such as those that many popular search
engines rely on. Early search engines often ranked the relevance of
pages based on the number of times that search terms appeared in the
pages. However, it was easy for spammers to game this system by
including popular search terms many times, propelling their results to
the top of the list.

When you enter a search query, you really want the important pages: the
ones with valuable information, a property often reflected in the
quantity and quality of other pages linking to them. Better algorithms
were eventually developed that took into account the relationships
between web pages, as determined by links. (For more about the history
of search engines, you can check out
\href{http://en.wikipedia.org/wiki/Search_engine}{this page}.) These
relationships can be represented nicely by a graph structure, which is
what we'll be using here.

\subsubsection{NodeScore ADT}

Throughout the assignment, we'll want to maintain associations of graph
nodes to their importance, or ``NodeScore'': a value between 0
(completely unimportant) and 1 (the only important node in the graph).

In order to assign NodeScores to the nodes in a graph, we've provided a
module with an implementation of an ADT, \texttt{NODE\_SCORE}, to hold
such associations. The module makes it easy to create, modify, normalize
(to sum to 1), and display NodeScores. You can find the module signature
and implementation in \texttt{nodescore.ml}.

\subsubsection{NodeScore Algorithms}

In this section, you'll implement a series of NodeScore algorithms in
different modules: that is, functions \texttt{rank} that take a graph
and return a \texttt{node\_score\_map} on it. As an example, we've
implemented a trivial NodeScore algorithm in the \texttt{IndegreeRanker}
module that gives all nodes a score equal to the number of incoming
edges.

\subsubsection{Random Walk Ranker, or Sisyphus walks the web}

In this section, we will walk you through creating a robust ranking
algorithm based on random walks. The writeup goes through several steps,
and we encourage you to do your implementation in that order. However,
you will only submit the final version.

\subsection{Challenge 1}

You may realize that we need a better way of saying that nodes are
popular or unpopular. In particular, we need a method that considers
global properties of the graph and not just edges adjacent to or near
the nodes being ranked. For example, there could be an extremely
relevant webpage that is several nodes removed from the node we start
at. That node might normally fare pretty low on our ranking system, but
perhaps it should be higher based on there being a high probability that
the node could be reached when browsing around on the internet.

So consider Sisyphus, doomed to crawl the Web for eternity: or more
specifically, doomed to start at some arbitrary page, and follow links
randomly. (We assume for the moment that the Web is strongly connected
and that every page has at least one outgoing link, unrealistic
assumptions that we will return to address soon.)

Let's say Sisyphus can take \texttt{k} steps after starting from a
random node. We design a system to determine nodescores based off how
likely Sisyphus reaches a certain page. In other words, we ask: where
will Sisyphus spend most of his time?

\subsubsection{Step 1}

Implement \texttt{rank} in the \texttt{RandomWalkRanker} functor in
pagerank.ml, which takes a graph, and returns a normalized nodescore on
the graph where the score for each node is proportional to the number of
times Sisyphus visits the node i n \texttt{num\_steps} steps, starting
from a random node. For now, your function may raise an exception if
some node has no outgoing edges. Note that \texttt{num\_steps} is passed
in as part of the functor parameter \texttt{P}.

You may find the library function \texttt{Random.int} useful, and may
want to write some helper functions to pick random elements from lists
(don't forget about the empty list case).

\subsubsection{Step 2}

Our Sisyphean ranking algorithm does better at identifying important
nodes according to their global popularity, rather than being fooled by
local properties of the graph. But what about the error condition we
mentioned above: that a node might not have any outgoing edges? In this
case, Sisyphus has reached the end of the Internet. What should we do? A
good solution is to jump to some random page and surf on from there.

Modify your rank function so that that instead of raising an error at
the end of the Internet, it has Sisyphus jump to a random node.

\subsubsection{Step 3}

This should work better. But what if there are very few pages that have
no outgoing edges - or worse, what if there is a set of vertices with no
outgoing edges to the rest of the graph, but there are still edges
between vertices in the set? Sisyphus would be stuck there forever, and
that set would win the node popularity game hands down, just by having
no outside links. What we'd really like to model is the fact that
Sisyphus doesn't have an unlimited attention span, and starts to get
jumpy every now and then\ldots{}

Modify your rank function so that if the module parameter
\texttt{P.do\_random\_jumps} is \texttt{Some\ alpha}, then with
probability \texttt{alpha} at every step, Sisyphus will jump to a random
node in the graph, regardless of whether the current node has outgoing
edges. (If the current node has no outgoing edges, Sisyphus should still
jump to a random node in the graph, as before.)

Don't forget to add some testing code. As stated in ``Testing'' section
below, possible approaches include just running it by hand a lot of
times and verifying that the results seem reasonable, or writing an
approx-equal function to compare the result of a many-step run with what
you'd expect to find.

Submit your final version of \texttt{RandomWalkRanker}.

\subsubsection{QuantumRanker}

Our algorithm so far works pretty well, but on a huge graph it would
take a long time to find all of the hard-to-get-to nodes. (Especially
when new nodes are being added all the time\ldots{})

\subsection{Challenge 2}

We'll need to adjust our algorithm somewhat. In particular, let's
suppose Sisyphus is bitten by a radioactive eigenvalue, giving him the
power to subdivide himself arbitrarily and send parts of himself off to
multiple different nodes at once. We have him start evenly spread out
among all the nodes. Then, from each of these nodes, the pieces of
Sisyphus that start there will propagate outwards along the graph,
dividing themselves evenly among all outgoing edges of that node.

So, let's say that at the start of a step, we have some fraction q of
Sisyphus at a node, and that node has 3 outgoing edges. Then q/3 of
Sisyphus will propagate outwards from that node along each edge. This
will mean that nodes with a lot of value will make their neighbors
significantly more important at each timestep, and also that in order to
be important, a node must have a large number of incoming edges
continually feeding it importance.

Thus, our basic algorithm takes an existing graph and NodeScore, and
updates the NodeScore by propagating all of the value at each node to
all of its neighbors. However, there's one wrinkle: we want to include
some mechanism to simulate random jumping. The way that we do this is to
use a parameter \texttt{alpha}, similarly to what we did in exercise 2.
At each step, each node propagates a fraction \texttt{1-alpha} of its
value to its neighbors as described above, but also a fraction
\texttt{alpha} of its value to all nodes in the graph. This will ensure
that every node in the graph is always getting some small amount of
value, so that we never completely abandon nodes that are hard to reach.

We can model this fairly simply. If each node distributes alpha times
its value to all nodes at each timestep, then at each timestep each node
accrues (alpha/n) times the overall value in this manner. Thus, we can
model this effect by having the base NodeScore at each timestep give
(alpha/n) to every node.

That gives us our base case for each timestep of our quantized Sisyphus
algorithm. What else do we need to do at each timestep? Well, as
explained above, we need to distribute the value at each node to all of
its neighbors.

\subsubsection{Step 1}

Write a function \texttt{propagate\_weight} that takes a node, a graph,
a NodeScore that it's building up, and the NodeScore from the previous
timestep, and returns the new NodeScore resulting from distributing the
weight that the given node had in the old NodeScore to all of its
neighbors. Remember that only (1-alpha) of the NodeScore gets
distributed among the neighbors (\texttt{alpha} of the weight was
distributed evenly to all nodes in the base case). A value for alpha is
passed in to the functor as part of the P parameter. \textbf{Note:} To
handle the case of nodes that have no outgoing edges, assume that each
node has an implicit edge to itself.

\subsubsection{Step 2}

Now we have all the components we need. Implement the \texttt{rank}
function in the \texttt{QuantumRanker} module. It should return the
NodeScore resulting from applying the updating procedures described
above. The number of timesteps to simulate is passed in to the functor
as part of the P parameter.

Don't forget to add some tests!

\subsection{Using your page rankers}

At the top of \texttt{crawl.ml} is a definition of a
\texttt{MoogleRanker} module that's used to order the search results.
Replace the call to the \texttt{IndegreeRanker} functor with calls to
the other rankers you wrote, and experiment with how it changes the
results.

\subsubsection{Fun fact}\label{fun-fact}

You know by now that PageRank assigns an importance value to every page
on the web in order to rank it. But did you know the naming of the
algorithm has nothing to do with pages, and it's
\href{http://web.archive.org/web/20090424093934/http://www.google.com/press/funfacts.html}{actually
named after Larry Page}?

\end{document}








\section{Dictionaries as balanced trees}

The critical data structure in Moogle is the dictionary. A
\emph{dictionary} is a data structure that maps \emph{keys} to
\emph{values}. Each key in the dictionary has a unique value, although
multiple keys can have the same value. For example, WordDict is a
dictionary that maps words on a page to the set of links that contain
that word. At a high level, you can think of a dictionary as a set of
(key,value) pairs.

As you may have observed as you tested your crawler with your new set
functor, building dictionaries on top of association lists (which we
have for you) does not scale particularly well. The implementation we
have provided in \texttt{dict.ml} is pretty inefficient, as it is based
on lists, so the asymptotic complexity of operations, such as looking up
the value associated with a key can take linear time in the size of the
dictionary.

For this part of the problem set, we are going to build a different
implementation of dictionaries using a kind of \emph{balanced tree}
called a \emph{2-3 tree}. Since our trees are guaranteed to be balanced,
then the complexity for our insert, remove, and lookup functions will be
logarithmic in the number of keys in our dictionary (why?).

\textcolor{red}{Before you start coding, please read
\href{http://sites.fas.harvard.edu/~cs51/2-3-trees.pdf}{this} writeup,
which explains the invariants in a 2-3 tree and how to implement insert
and remove.}

Read every word of it. Seriously.

In the file \texttt{dict.ml}, you will find a commented out functor
\texttt{BTDict} which is intended to build a \texttt{DICT} module from a
\texttt{DICT\_ARG} module. Here is the type definition for a dictionary
implemented as a 2-3 tree:

\begin{verbatim}
     type pair = key * value
     type dict =
       | Leaf
       | Two of dict * pair * dict
       | Three of dict * pair * dict * pair * dict
\end{verbatim}

Notice the similarities between this type definition of a 2-3 tree and
the type definition of a binary search tree as provided in pset4 and
lecture. Here, in addition to nodes that contain two subtrees, we have a
Three node which contains two (key,value) pairs (k1,v1) and (k2,v2), and
three subtrees left, middle, and right. Below are the invariants as
stated in the handout:

\textbf{2-node: \texttt{Two(left,(k1,v1),right)}}

\begin{enumerate}
 \setlength{\itemsep[{0pt}\setlength{\parskip}{0pt}
\item
  Every key k appearing in subtree left must be k \textless{} k1.
\item
  Every key k appearing in subtree right must be k \textgreater{} k1.
\item
  The length of the path from the 2-node to \textbf{every} leaf in its
  two subtrees must be the same.
\end{enumerate}

\textbf{3-node] \texttt{Three(left,(k1,v1),middle,(k2,v2),right)}}

\begin{enumerate}
\item
  k1 \textless{} k2.
\item
  Every key k appearing in subtree left must be k \textless{} k1.
\item
  Every key k appearing in subtree right must be k \textgreater{} k2.
\item
  Every key k appearing in subtree middle must be k1 \textless{} k
  \textless{} k2.
\item
  The length of the path from the 3-node to \textbf{every} leaf in its
  three subtrees must be the same.
\end{enumerate}

Note that all of these bounds here are non inclusive (\textless{} and
\textgreater{} vs. \textless{}= and \textgreater{}=), unlike the
handout. The handout permitted storing multiple keys that are equal to
each other, however, for our dictionary, a key can only be stored once.
The last invariants of both types of nodes imply that our tree is
\textbf{balanced}, that is, the length of the path from our root node to
any Leaf node is the same.

Open up \texttt{dict.ml} and locate the commented out BTDict module.

\subsection{Part a: balanced {[}6 pts{]}}

Locate the \texttt{balanced} function at the bottom of the BTDict
implementation (right before the
tests): \\


\texttt{balanced\ :\ dict\ -\textgreater{}\ bool}
\\

You must write this function which takes a dictionary (our 2-3 tree) and
returns true if and only if the tree is balanced (you only need to check
the path length invariants, not the order of the keys). You must
carefully think about how to efficiently check this. (Our solution runs
in O(n) where n is the size of the dictionary). In a comment above the
function, please explain carefully in English how you are testing that
the tree is balanced. Once you have written this function, scroll down
to \texttt{run\_tests} and uncomment out \texttt{test\_balanced()\ ;\ }.
Next, scroll down to \texttt{IntStringBTDict} and uncomment out those
two lines. All the tests should pass. Now that you are confident in your
\texttt{balanced} function, \textbf{you are required to use it on all
your tests involving insert}.

\subsection{Part b: strings, fold, lookup, member {[}5 pts{]}}

Implement these functions according to their specification provided in
\texttt{DICT}:

\begin{verbatim}

  val fold : (key -> value -> 'a -> 'a) -> 'a -> dict -> 'a
  val lookup : dict -> key -> value option
  val member : dict -> key -> bool
  val string_of_key : key -> string
  val string_of_value : value -> string
  val string_of_dict : dict -> string
\end{verbatim}

You may change the order in which the functions appear, but \textbf{you
may not change the signature of any of these functions (name, order of
arguments, types).} You may add a ``rec'' to make it ``let rec'' if you
feel that you need it.

\subsection{Part c: insert (upward phase) {[}6 pts{]}}

Read the handout if you have not already done so. To do insertion, there
are two ``phases'' - the \emph{downward phase} to find the right place
to insert, and the \emph{upward phase} to rebalance the tree, if
necessary, as we go back up the tree. To distinguish when we need to
rebalance and when we don't, we have created a new type to represent a
``kicked-up'' configuration:

\begin{verbatim}
  type kicked =
  | Up of dict * pair * dict (* only two nodes require rebalancing *)
  | Done of dict             (* we are done rebalancing *)
\end{verbatim}

The only kind of node that could potentially require rebalancing is a
2-node (we can see this pictorially: the only subtrees that require
rebalancing are represented by an up arrow in the handout; only 2-nodes
have up arrows), hence we make the Up constructor take the same
arguments as the Two constructor. We have provided stubs for functions
to perform the upward phase on a 2-node and a 3-node:

\begin{verbatim}
  let insert_upward_two (w: pair) (w_left: dict) (w_right: dict)
    (x: pair) (x_other: dict) : kicked = ...

  let insert_upward_three (w: pair) (w_left: dict) (w_right: dict)
     (x: pair) (y: pair) (other_left: dict) (other_right: dict) : kicked = ...
\end{verbatim}

Please read the specification in the source code. These functions return
a ``kicked'' configuration containing the new balanced tree (See page 5
on the handout), and this new tree will be wrapped with ``Up'' or
``Done'', depending on whether this new balanced tree requires further
upstream rebalancing (indicated by an upward arrow). Implement these
functions according to the specification in the handout.

\subsubsection{Part d: insert (downward phase) {[}6 pts{]}}

Now that we have the upward phase (the hard part) taken care of, we can
worry about the downward phase. We have provided three \emph{mutually
recursive functions} to simplify the main \texttt{insert} function. Here
are the downward phase functions:

\begin{verbatim}

  let rec insert_downward (d: dict) (k: key) (v: value) : kicked =
    match d with
      | Leaf -> ??? (* base case! see handout *)
      | Two(left,n,right) -> ??? (* mutual recursion! *)
      | Three(left,n1,middle,n2,right) -> ??? (* mutual recursion! *)

  and insert_downward_two ((k,v): pair) ((k1,v1): pair)
    (left: dict) (right: dict) : kicked = ...

  and insert_downward_three ((k,v): pair) ((k1,v1): pair) ((k2,v2): pair)
    (left: dict) (middle: dict) (right: dict) : kicked = ...

  (* the main insert function *)
  let insert (d: dict) (k: key) (v: value) : dict =
    match insert_downward d k v with
      | Up(l,(k1,v1),r) -> Two(l,(k1,v1),r)
      | Done x -> x
\end{verbatim}

Note that, like the upward phase, the downward phase also returns a
``kicked-up'' configuration. The main insert function simply extracts
the tree from the kicked up configration returned by the downward
phases, and returns the tree.

Implement these three
downward phase functions. You will need to use your upward phase
functions that you wrote in part c. Once you have written your code, you
must write tests to test your insert function, lookup function, and
member function. We have provided generator functions to generate keys,
values, and pairs to help you test (See DICT\_ARG). Use the test
functions that we have provided for remove as an example.
\textbf{Remember to make run\_tests () call your newly created test
functions.}

\subsection{Part e: choose {[}2 pt{]}}

Implement the function according to the specification in
\texttt{DICT}:\\[2\baselineskip]\texttt{choose\ :\ dict\ -\textgreater{}\ (key\ *\ value\ *\ dict)\ option}\\

Write tests to test your choose function.