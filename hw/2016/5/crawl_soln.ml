(**********************************************************************
 * CS51 Problem Set 5, 2016 - Moogle
 * crawl.ml: the crawler, which builds a dictionary from words to sets
 *           of links
 **********************************************************************)

(* Rename modules for convenience *)
module CS = Crawler_services ;;
module PR = Pagerank_soln ;; 
  
(* Specify the web page ranking method to be used. If you do the
   Challenge section 6, you'll want to modify this. *)
module MoogleRanker
  = PR.InDegreeRanker (PR.PageGraph_soln) (PR.PageScore)
  (*
     = PR.RandomWalkRanker (PR.PageGraph_soln) (PR.PageScore) (struct
       let do_random_jumps = Some 0.20
       let num_steps = 1000
     end)
  *)

   (*= PR.QuantumRanker (PR.PageGraph_soln) (PR.PageScore) (struct
       let alpha = 0.01
       let num_steps = 1
       let debug = true
     end)*)


(* Dictionaries mapping words (strings) to sets of crawler links *)
module WordDict = Dict.Make(
  struct
    type key = string
    type value = PR.LinkSet.set
    let compare = Order.string_compare
    let string_of_key = (fun s -> s)
    let string_of_value = PR.LinkSet.string_of_set

    (* These functions are for testing purposes *)
    let gen_key () = ""
    let gen_key_gt _ () = gen_key ()
    let gen_key_lt _ () = gen_key ()
    let gen_key_random () = gen_key ()
    let gen_key_between _ _ () = None
    let gen_value () = PR.LinkSet.empty
    let gen_pair () = (gen_key(),gen_value())
  end)

(* A query module that uses LinkSet and WordDict *)
module Q = Query_soln.Query_soln(
  struct
    module S = PR.LinkSet
    module D = WordDict
  end)


(***********************************************************************)
(*    Section 1: CRAWLER                                                  *)
(***********************************************************************)

(* TODO: Build an index as follows:
 *
 * Remove a link from the frontier (the set of links that have yet to
 * be visited), visit this link, add its outgoing links to the
 * frontier, and update the index so that all words on this page are
 * mapped to linksets containing this url.
 *
 * Keep crawling until we've
 * reached the maximum number of links (n) or the frontier is empty. *)
let rec crawl (n:int) (frontier: PR.LinkSet.set)
    (visited : PR.LinkSet.set) (d:WordDict.dict) : WordDict.dict =
  if n <= 0 then d
  else
    match PR.LinkSet.choose frontier with
      | None -> d
      | Some (link, new_frontier) ->
        if PR.LinkSet.member visited link then
          crawl n new_frontier visited d
        else
          let visited = PR.LinkSet.insert link visited in
          match CS.get_page link with
            | None -> crawl n new_frontier visited d
            | Some page ->
              let new_new_frontier =
                List.fold_left (fun s x -> PR.LinkSet.insert x s)
                                new_frontier page.links in
              let insert_word d' w =
                (match WordDict.lookup d' w with
                  | None ->
                    WordDict.insert d' w (PR.LinkSet.singleton page.url)
                  | Some s ->
                    WordDict.insert d' w (PR.LinkSet.insert page.url s)) in
              let d' = List.fold_left insert_word d page.words in
              crawl (n - 1) new_new_frontier visited d'
;;

let crawler () =
  crawl CS.num_pages_to_search
	(PR.LinkSet.singleton CS.initial_link)
	PR.LinkSet.empty
	WordDict.empty ;;
