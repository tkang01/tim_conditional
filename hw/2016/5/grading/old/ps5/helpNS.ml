open Graph_soln
open Nodescore_soln
open Pagerank_soln
open Util_soln
open CrawlerServices

exception MissNode

let compare i j =
  if i < j then Order_soln.Less 
  else 
    if i = j 
    then Order_soln.Eq 
    else Order_soln.Greater 

module TestSet = 
  Myset_soln.Make(
    struct
      type t = int
      let compare = compare
      let string_of_t = string_of_int
    end
  )

module TestGraph_soln =
  Graph_soln (
    struct
      type node = int
      let compare = compare
      let string_of_node = string_of_int
    end
  )

module TestScore =
  NodeScore(
    struct 
      type node = int
	let compare = compare
	let string_of_node = string_of_int
    end
  )
    
module TS = TestScore
module TG = TestGraph_soln

let compare nsref ns error =
  let f node weight res =
    let b = 
      match TS.get_score ns node with
	| None -> raise MissNode
	| Some w -> weight -. error < w && w < weight +. error   
    in
    b && res
    
  in
  TS.fold f true nsref

let add_nodes l =
  List.fold_left TG.add_node TG.empty l  

let add_edges n l g =
  List.fold_left (fun g n' -> TG.add_edge g n' n) g l   

let gsimple =
  let g = add_nodes [0;1;2;3;4;5;6;7;8;9] in
  let g = add_edges 0 [1;2;3;4;5;6;7;8;9] g in
  let g = add_edges 1 [6;7;8;9] g in
  let g = add_edges 2 [1;2;3;6;7;8;9] g in
  let g = add_edges 3 [9] g in
  let g = add_edges 4 [7] g in
  let g = add_edges 5 [2;3;4;6;7;8;9] g in
  let g = add_edges 6 [1;2] g in
  let g = add_edges 7 [1;4;7] g in  
  let g = add_edges 8 [4;5;6;1] g in  
  g 
  
 
