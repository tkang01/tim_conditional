open Util_soln ;;    
open CrawlerServices ;;
open Order_soln ;;
open Pagerank_soln ;;


(* Phase II: Change these as you implement smarter ranking algorithms *)
module Moogle_solnRanker
  = DummyRanker (PageGraph_soln) (PageScore) 
  (* = InDegreeRanker (PageGraph_soln) (PageScore) *)

  (* = RandomWalkRanker (PageGraph_soln) (PageScore) (struct 
       let do_random_jumps = None
       let num_steps = 1000
     end) *)

  (* = QuantumRanker (PageGraph_soln) (PageScore) (struct 
       let alpha = 0.01
       let num_steps = 1
       let debug = true
     end)
  *)

(* Dict_solnionaries mapping words (strings) to sets of crawler links *)
module WordDict = Dict.Make(
  struct 
    type key = string
    type value = LinkSet.set
    let compare = string_compare
    let string_of_key = (fun s -> s)
    let string_of_value = LinkSet.string_of_set
  end)

(* A query module that uses LinkSet and WordDict *)
module Q = Query_soln.Query(
  struct
    module S = LinkSet
    module D = WordDict
  end)

let print s = 
  let _ = Printf.printf "%s\n" s in
  flush_all();;


(* Build an index as follows:
 * 
 * Remove a link from the frontier (the set of links that have yet to
 * be visited), visit this link, add its outgoing links to the
 * frontier, and update the index so that all words on this page are
 * mapped to linksets containing this url.
 *
 * Keep crawling until we've
 * reached the maximum number of links (n) or the frontier is empty. *)
let rec crawl (n:int) (frontier: LinkSet.set)
    (visited : LinkSet.set) (d:WordDict.dict) : WordDict.dict = 
  if n <= 0 then d 
  else
    match LinkSet.choose frontier with 
      | None -> d 
      | Some (link, new_frontier) -> 
          if LinkSet.member visited link then 
            crawl n new_frontier visited d
          else 
            let page = get_page link in 
            (* let _ = Printf.printf "Page: %s\n" (string_of_page page) in *)
            let new_new_frontier = 
              List.fold_left (fun s x -> LinkSet.insert x s) new_frontier 
                page.links in
            let visited = LinkSet.insert page.url visited in
            let insert_word d' w = 
              (match WordDict.lookup d' w with 
                 | None -> 
                     WordDict.insert d' w (LinkSet.singleton page.url)
                 | Some s -> 
                     WordDict.insert d' w (LinkSet.insert page.url s)) in
            let d' = List.fold_left insert_word d page.words in 
              crawl (n - 1) new_new_frontier visited d' 
                (* STUDENT WORK
                   raise (Failure "Please, implement crawl (Part 1)")
                *)
;;

let crawler () = 
  crawl num_pages_to_search (LinkSet.singleton initial_link) LinkSet.empty
    WordDict.empty
;;

(* Debugging note: if you set debug=true in moogle.ml, it will print out your
 * index after crawling. *)
