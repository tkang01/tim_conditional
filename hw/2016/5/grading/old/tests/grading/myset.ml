(* Definitions for sets. *)

(* An interface for set modules *)
module type SET = 
sig
  type elt  (* type of elements in the set *)
  type set  (* abstract type for the set *)

  val empty : set

  val is_empty : set -> bool

  val insert : elt -> set -> set

  (* same as insert x empty *)
  val singleton : elt -> set

  val union : set -> set -> set
  val intersect : set -> set -> set

  (* remove an element from the set -- if the
   * element isn't present, does nothing. *)
  val remove : elt -> set -> set

  (* returns true iff the element is in the set *)
  val member : set -> elt -> bool

  (* chooses some member from the set, removes it 
   * and returns that element plus the new set.  
   * If the set is empty, returns None. *)
  val choose : set -> (elt * set) option

  (* fold a function across the elements of the set
   * in some unspecified order. *)
  val fold : (elt -> 'a -> 'a) -> 'a -> set -> 'a

  val string_of_set : set -> string
  val string_of_elt : elt -> string
end

(* parameter to Set modules -- we must pass in some 
 * type for the elements of a set, a comparison
 * function, and a way to stringify it.
 *)
module type COMPARABLE = 
  sig
    type t
    val compare : t -> t -> Order_soln.order
    val string_of_t : t -> string
  end

(* A simple, list-based implementation of sets. *)
module ListSet(C: COMPARABLE) : (SET with type elt = C.t) = 
  struct
    open Order_soln
    type elt = C.t 
    (* invariant: sorted, no duplicates *)
    type set = elt list
    let empty = []
    let is_empty xs = 
      match xs with 
        | [] -> true
        | _ -> false
    let singleton x = [x]
    let rec insert x xs = 
      match xs with 
        | [] -> [x]
        | y::ys -> (match C.compare x y with 
                      | Greater -> y::(insert x ys)
                      | Eq -> xs
                      | Less -> x::xs)

    let union xs ys = List.fold_right insert xs ys
    let rec remove y xs = 
      match xs with 
        | [] -> []
        | x::xs1 -> (match C.compare y x with 
                      | Eq -> xs1
                      | Less -> xs
                      | Greater -> x::(remove y xs1))

    let rec intersect xs ys = 
      match xs, ys with 
        | [], _ -> []
        | _, [] -> []
        | xh::xt, yh::yt -> (match C.compare xh yh with 
                               | Eq -> xh::(intersect xt yt)
                               | Less -> intersect xt ys
                               | Greater -> intersect xs yt)

    let rec member xs x = 
      match xs with 
        | [] -> false
        | y::ys -> (match C.compare x y with
                      | Eq -> true
                      | Greater -> member ys x
                      | Less -> false)

    let choose xs = 
      match xs with 
        | [] -> None
        | x::rest -> Some (x,rest)
    let fold f e = List.fold_left (fun a x -> f x a) e 
    
    let string_of_elt = C.string_of_t
    let string_of_set (s: set) : string = 
      let f = (fun y e -> y ^ "; " ^ C.string_of_t e) in
        "set([" ^ (List.fold_left f "" s) ^ "])"

  end

(* An implementation of sets using dictionaries *)
module Dict_solnSet(C : COMPARABLE) : (SET with type elt = C.t) = 
struct
  module D = Dict_soln.Make(struct
(* STUDENT WORK    
      TODO Part 1
*)
                         type key = C.t
                         type value = unit
                         let compare = C.compare
                         let string_of_key = C.string_of_t
                         let string_of_value = fun () -> "()"
                       end)
(* STUDENT WORK *)
  type elt = D.key
  type set = D.dict
  let empty = D.empty
  let insert x s = D.insert s x ()
  let singleton x = insert x empty
  let union s1 s2 = D.fold (fun x _ s -> insert x s) s2 s1
  let member = D.member 
  let intersect s1 s2 = 
    D.fold (fun x _ s -> if member s2 x then insert x s else s) 
      empty s1
  let remove x s = D.remove s x
  let choose s = 
    match D.choose s with
      | None -> None
      | Some (k,_,s') -> Some (k,s')
  let fold f u s = D.fold (fun x _ a -> f x a) u s
  let is_empty s = 
    match choose s with 
      | None -> true
      | Some (_,_) -> false
  let string_of_elt = D.string_of_key
  let string_of_set s = D.string_of_dict s
(* STUDENT WORK
  TODO Part 1
*)
end

module Make (C : COMPARABLE) : (SET with type elt = C.t) = 
  (* Change this line to change the set implementation *)
  ListSet (C) 
  (* Dict_solnSet (C) *)

