#!/bin/bash
set -e

# Hack!  Careful...

echo "This is a hack.  Are you sure you're in the right dir? 3 seconds to stop"
sleep 3

echo Running...
# Problems: cmceachr rjmiller tsrice
for x in adaly alin aszasz atai belal blsilver cashann  cpillot crichton dane dcai dmuhlest dsun dzhu efarber egan ekogan esoliman faux fritz hgommers hlhindal ifofana ikamoun jhacker joyzhang jtgreen kim31 kogos kopparty lfduarte li15 long lwelch mbachman mbwong mchance mchen mdboyd mniu mpope mxhe nbrown nhoward nishihar nshah phung predescu rbowden  scrouch sdchan sincock strican tmacwill  tzou wcnewell zhang32 zhou zinoveva 
do
    echo Grading $x
    rm -rf tmp.123
    mkdir tmp.123
    # copy our test code
    cp ../hw/moogle/grading/testmoogle.ml tmp.123
    cp ../hw/moogle/grading/testdict.ml tmp.123
    cp ../hw/moogle/grading/order.ml tmp.123
    cp ../hw/moogle/grading/Makefile tmp.123

    # Copy student's code
    cp ps4/submissions/$x/dict.ml tmp.123
    cp ps4/submissions/$x/set.ml tmp.123
    
    # Run
    cd tmp.123
    touch output
    (make && make run) &> grading.compile
    
    # Copy output
    cp grading.compile ../ps4/submissions/$x/
    cp output ../ps4/submissions/$x/grading.out
    
    cd ..
done
