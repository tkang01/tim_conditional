To grade:

Assuming a directory, let's call it:
~/repo

Place this grading folder into the directory:
~/repo/grading/{grade_ps5.py,prepare_moogle.sh,5}

Place a student's moogle submission in a folder called 5:
~/repo/5/*

Prepare the submission for grading:
cd ~/repo
./grading/prepare_moogle.sh

Run the grading script:
./grading/grade_ps5.py 5 -u 5
