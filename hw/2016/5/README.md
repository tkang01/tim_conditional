# Structure of PS5 Directory

+ Makefiles

There are two makefiles in this directory, makefile and makefile-code. Makefile is the testing makefile, and can be run with the command make ps5. Makefile-code, meanwhile, is the makefile necessary to compile and run Moogle_soln. At this stage, since the two files are doomed to be in the same folder, it's simplest just to rename the desired file to "makefile" and run "make" or "make ps5".

+ Testing crawl

The crawler is designed to take in all of the pages and produce a mapping of pages to words. Since this is quite arbitary (and depends largely on the directory it's being used on), tests for crawl aim to compare the output of the staff solution to the output of a student's solution. I adknowledge that this isn't ideal, but it's the best way I've found so far!

+ Testing myset and dict

We just have general unit tests for myset.ml and dict.ml! Point rankings haven't been completely solidified yet.

+ Testing Moogle_soln

Previous iterations of tests for Moogle_soln have focused on the output of Moogle, when targeted with different queries. This has been done through shell scripts, python scripts, and javascript. In this iteration, we aim to work within the existing unit testing framework to test the output of Moogle's ranking system instead! By default, we want to test InDegreeRanker. If the student's code is using RandomWalkerRanker or QuantumRanker instead, unit tests will fail because the indegree ranking won't be the same -- to solve this, just modify crawler.ml so that MoogleRanker implements InDegreeRanker again!

Since our moogle.ml is slightly modified, to account for testing system (and to make sure we don't accidentally the server), it is necessary to delete the student's moogle.ml and replace it with the staff solution's moogle.ml. Just run `diff moogle.ml release/moogle.ml` to see what's different!

+ Testing extra credit
Extra credit testing (specifically RandomWalkRanker and QuantumRanker) should be done by the grader on a case-by-case basis; only a small proportion of students will likely attempt these problems anyway.

+ Remaining TODOs
The point values of the individual unit tests have not yet been aligned to the point values stated in the writeup!
