(*** CS51 Problem Set 5 ***)
(*** January 12, 2015 ***)
(*** Test Suite - Myset ***)

(* Defines the module for IntComparable, which is
 * supposed to be used for testing. Applies the functor
 * DictSet to IntComparable, which will then be tested. 
 *)
open MS
module C = MS.IntComparable
(* Will not compile if they haven't implemented DictSet *)
module B = DictSet(C)


(* Set up the test suite *)
let suite_myset = Testing.create_suite "myset" ;;

(* Check that two sets have the same elements *)
let subset s1 s2 =
  B.fold (fun acc elt -> B.member s2 elt && acc) true s1 = true ;;
let same s1 s2 =
  (subset s1 s2) && (subset s2 s1) ;;

(* Tests empty *) 
Testing.add_test suite_myset "empty" (lazy (
	let s1 = B.empty in 
	B.is_empty s1));;

(* Tests fold *)
Testing.add_test suite_myset "fold 1" (lazy(
	let insert_list d list = List.fold_left (fun x y -> B.insert y x) d list in
	let rec generate_random_list size = 
		if size <= 0 then [] 
		else (C.gen_random () )::(generate_random_list (size - 1)) in 
	let e1 = generate_random_list(100) in 
	let e2 = generate_random_list(100) in		
	let s1 = insert_list B.empty e1 in 
	let s2 = insert_list B.empty e2 in 
	let s12 = B.intersect s1 s2 in
	(*** What the hell is this test anyway? ***)
	subset s12 s12)) ;;

Testing.add_test suite_myset "fold 2" (lazy(
	let insert_list d list = List.fold_left (fun x y -> B.insert y x) d list in
	let rec generate_random_list size = 
		if size <= 0 then [] 
		else (C.gen_random () )::(generate_random_list (size - 1)) in 
	let e1 = generate_random_list(100) in 
	let e2 = generate_random_list(100) in		
	let s1 = insert_list B.empty e1 in 
	let s2 = insert_list B.empty e2 in 
	let s12 = B.intersect s1 s2 in 
	B.fold (fun y x -> B.member s1 x || B.member s2 x && y) true s12 = true));;

(* Tests insert and remove *)
Testing.add_test suite_myset "insert and remove 1" (lazy(
	let k1 = C.gen() in
	let d1 = B.insert k1 B.empty in 
	B.remove k1 d1 = B.empty));;

Testing.add_test suite_myset "insert and remove 2" (lazy(
	let k1 = C.gen() in
	let k2 = C.gen_lt k1 () in
	let k3 = C.gen_lt k2 () in
	let d1 = B.insert k1 B.empty in 
	let d2 = B.insert k2 d1 in 
	let d3 = B.insert k3 d2 in 
	B.remove k3 d3 = d2));;

Testing.add_test suite_myset "insert and remove 3" (lazy(
	let k1 = C.gen() in
	let k2 = C.gen_lt k1 () in
	let d1 = B.insert k1 B.empty in
	let d2 = B.insert k2 d1 in 
	B.remove k2 d2 = d1));;

(* Tests singleton *)
Testing.add_test suite_myset "singleton 1" (lazy(
	let s1 = C.gen() in 
	let s2 = B.insert s1 B.empty in 
	let s3 = B.singleton s1 in 
	s2 = s3));;

Testing.add_test suite_myset "singleton 2" (lazy(
	let k1 = C.gen() in 
	let d1 = B.insert k1 B.empty in 
	B.singleton k1 = d1));;

Testing.add_test suite_myset "singleton 3" (lazy(
	let k1 = C.gen() in 
	B.remove k1 (B.singleton k1) = B.empty));;

(* Tests union *)
Testing.add_test suite_myset "union 1" (lazy (
	B.union B.empty B.empty = B.empty));;

Testing.add_test suite_myset "union 2" (lazy (
	let k1 = C.gen() in
	let k2 = C.gen_lt k1 () in 
	let d1 = B.insert k1 B.empty in 
	let d2 = B.insert k2 d1 in 
	B.union d1 d2 = d2));;

(* Tests member *)
Testing.add_test suite_myset "member 1" (lazy(
	let k1 = C.gen() in 
	let k2 = C.gen_lt k1 () in
	let k3 = C.gen_lt k2 () in 
	let w = B.insert k3 (B.insert k2 (B.singleton k1)) in
	B.member w k1));;

Testing.add_test suite_myset "member 2"	(lazy(
	let k1 = C.gen() in 
	let k2 = C.gen_lt k1 () in 
	let k3 = C.gen_lt k2 () in 
	let k4 = C.gen_lt k3 () in 
	let w = B.insert k3 (B.insert k2 (B.singleton k1)) 
	in not (B.member w k4)));;

(* Tests choose *)
Testing.add_test suite_myset "choose 1" (lazy(
	let elt = C.gen() in
	let set = B.singleton elt in 
	B.choose set = Some (elt, B.empty)));;

Testing.add_test suite_myset "choose 2"	(lazy(
	B.choose B.empty = None));;

(* Tests intersect *)

  
Testing.add_test suite_myset "intersect 1" (lazy(
	let insert_list d lst = List.fold_left (fun x y -> B.insert y x) d lst in
	let rec generate_random_list size = 
		if size <= 0 then [] 
		else (C.gen_random () )::(generate_random_list (size - 1)) in
	let e1 = generate_random_list(100) in
	let e2 = generate_random_list(100) in
	let s1 = insert_list B.empty e1 in 
	let s2 = insert_list B.empty e2 in 
	let s12 = B.intersect s1 s2 in 
	B.fold (fun y x -> B.member s12 x && y) true s12 = true));;

Testing.add_test suite_myset "intersect 2" (lazy(
	let k1 = C.gen() in
	let k2 = C.gen_lt k1 () in
	let k3 = C.gen_lt k2 () in
	let d1 = B.insert k1 B.empty in 
	let d2 = B.insert k2 d1 in 
	let d3 = B.insert k3 d2 in 
	same (B.intersect d2 d3) d2));;

Testing.add_test suite_myset "intersect 3" (lazy(
	let k1 = C.gen() in 
	let k2 = C.gen_lt k1 () in 
	let k3 = C.gen_lt k2 () in 
	let d1 = B.insert k1 B.empty in 
	let d2 = B.insert k2 d1 in 
	let d3 = B.insert k3 d2 in 
	same (B.intersect d1 d3) d1)) ;;

Testing.add_test suite_myset "intersect 4" (lazy(
	same (B.intersect B.empty B.empty) B.empty)) ;;

Testing.add_test suite_myset "intersect 5" (lazy(
	let k1 = C.gen() in 
	let d1 = B.insert k1 B.empty in 
	same (B.intersect B.empty d1) B.empty)) ;;

Testing.add_test suite_myset "intersect 6" (lazy(
	let k1 = C.gen() in 
	let d1 = B.insert k1 B.empty in 
	same (B.intersect d1 B.empty) B.empty)) ;;

(* Tests a combination of the functions *)	
Testing.add_test suite_myset "empty and insert" (lazy(
	let s2 = B.insert(C.gen()) B.empty in 
	not (B.is_empty s2)));;

Testing.add_test suite_myset "member and union" (lazy(
	let a = C.gen() in
	let b = C.gen_gt a () in 
	let c = B.union (B.singleton a) (B.singleton b) in 
	(B.member c a) && (B.member c b)));;

Testing.add_test suite_myset "intersect and union" (lazy(
	let x = C.gen() in 
	let y = C.gen_gt x () in 
	let z = C.gen_gt y () in 
	let t1 = B.union (B.singleton x) (B.singleton y) in 
	let t2 = B.union (B.singleton z) (B.singleton y) in
	let t3 = B.intersect t1 t2 in 
	(B.member t3 y) && not (B.member t2 x) && not (B.member t3 z)));;

Testing.add_test suite_myset "union and remove" (lazy(
	let x = C.gen() in 
	let y = C.gen_gt x () in
	let z = C.gen_gt y () in
	let s1 = B.union (B.singleton x) (B.union (B.singleton y) (B.singleton z)) in 
	let s2 = B.remove y s1 in 
	not (B.member s2 y) && (B.member s2 x) && (B.member s2 z)));;

Testing.add_test suite_myset "choose and empty" (lazy(
	let x = B.empty in 
	B.choose x = None));;

Testing.add_test suite_myset "choose and singleton" (lazy(
	let x = C.gen() in 
	let s1 = B.singleton x in 
	B.choose s1 = Some (x, B.empty)));;

Testing.add_test suite_myset "fold and union" (lazy(
	let x = C.gen() in 
	let y = C.gen_gt x () in 
	let z = C.gen_gt y () in 
	let s1 = B.union (B.singleton x) (B.union (B.singleton y) (B.singleton z)) in 
	let size = B.fold (fun rest _ -> rest + 1) 0 s1 in
	size = 3));;
