#!/bin/sh

caml-tex -o pset5.tex pset5.mlt
pdflatex -shell-escape pset5
bibtex pset5
makeindex pset5
pdflatex -shell-escape pset5
pdflatex -shell-escape pset5
