
(** Abstract syntax of MiniML expressions *)

type expr =
  | Var of varid                         (* variables *)
  | Num of int                           (* integers *)
  | Bool of bool                         (* booleans *)
  (* Unop and Binop should use an enum type instead of varid *)
  | Unop of varid * expr                 (* unary operators *)
  | Binop of varid * expr * expr         (* binary operators *)
  | Conditional of expr * expr * expr    (* if then else *)
  | Fun of varid * expr                  (* function definitions *)
  | Let of varid * expr * expr           (* local naming *)
  | Letrec of varid * expr * expr        (* recursive local naming *)
  | Raise                                (* exceptions *)
  | Unassigned                           (* (temporarily) unassigned *)
  | App of expr * expr                   (* function applications *)
 and varid = string ;;
  
(** Sets of strings *)
module SS = Set.Make(struct
		      type t = varid
		      let compare = String.compare
		    end);;
  
type varidset = SS.t ;;
  
(** Test to see if two sets have the same elements (for
    testing purposes) *)
let same_vars = SS.equal;;

(** Generate a set of variable names from a list of strings (for
    testing purposes) *)
let vars_of_list = SS.of_list ;;

(** Return a set of the variable names free in [exp] *)
let rec free_vars (exp : expr) : varidset =
  match exp with
  | Var x -> SS.singleton x
  | Num _ -> SS.empty
  | Bool _ -> SS.empty
  | Unop(_, arg) -> free_vars arg
  | Binop(_, arg1, arg2) -> SS.union (free_vars arg1) (free_vars arg2)
  | Conditional(cond, thenbranch, elsebranch) ->
     SS.union (free_vars cond)
	      (SS.union (free_vars thenbranch)
			(free_vars elsebranch)) 
  | Fun(x, body) -> SS.remove x (free_vars body)
  | Let(x, def, body) -> SS.union (free_vars def) (SS.remove x (free_vars body))
  | Letrec(x, def, body)
    ->  SS.remove x (SS.union (free_vars def) (free_vars body))
  | Raise -> SS.empty
  | Unassigned -> SS.empty
  | App(e1, e2) -> SS.union (free_vars e1) (free_vars e2)
;;
  
(** Return a fresh variable, constructed with a running counter a la
    gensym. Assumes no variable names use the prefix "var". *)
let new_varname : unit -> varid =
  let counter : int ref = ref 0 in
  function () -> 
	   counter := !counter + 1;
	   "var" ^ string_of_int !counter;;
  
(** Substitute [repl] for free occurrences of [var_name] in [exp] *)
let rec subst (var_name: varid) (repl: expr) (exp: expr) : expr =
  let rec sub (exp: expr) : expr =
    match exp with
    | Var x ->
       if x = var_name then repl else exp
    | Num _ -> exp
    | Bool _ -> exp
    | Unop(op, arg) -> Unop(op, sub arg)
    | Binop(op, arg1, arg2) -> Binop(op, sub arg1, sub arg2)
    | Conditional(cond, thenbranch, elsebranch) ->
       Conditional((sub cond),
		   (sub thenbranch),
		   (sub elsebranch))
    | Fun(x, body) ->
       if x = var_name then exp
       else (if SS.mem x (free_vars repl) then
	       let fresh = new_varname () in
	       Fun(fresh, sub (subst x (Var fresh) body))
	     else Fun(x, sub body))
    | Let(x, def, body) ->
       if x = var_name then Let(x, sub def, body)
       else (if SS.mem x (free_vars repl) then
        let fresh = new_varname () in
        Let(fresh, sub def, sub (subst x (Var fresh) body))
      else Let(x, sub def, sub body))
    | Letrec(x, def, body) ->
       if x = var_name
       then exp
       else Letrec (x, sub def, sub body)
    | Raise -> exp
    | Unassigned -> exp
    | App(e1, e2) ->
       App(sub e1, sub e2)
  in
  sub exp ;;
  
(** Returns a string representation of the expr *)
let rec exp_to_string (exp: expr) : string =
  match exp with
  | Var x -> x
  | Num x -> if x < 0
	     then "~" ^ (string_of_int ~- x)
	     else (string_of_int x)
  | Bool x -> string_of_bool x
  | Unop(op, arg) -> "[" ^ op ^ " " ^ (exp_to_string arg) ^ "]"
  | Binop(op, arg1, arg2) -> "[" ^ (exp_to_string arg1) ^ " " ^ op ^ " "
			     ^ (exp_to_string arg2) ^ "]" 
  | Conditional(cond, thenbranch, elsebranch) ->
     Printf.sprintf "if %s then %s else %s"
		    (exp_to_string cond)
		    (exp_to_string thenbranch)
		    (exp_to_string elsebranch)
  | Fun(x, body) -> "[function " ^ x ^ " -> " ^ (exp_to_string body) ^ "]"
  | Let(x, def, body) ->
     "let " ^ x ^ " = " ^ (exp_to_string def) ^ " in " ^ (exp_to_string body)
  | Letrec(x, def, body) ->
     "let rec " ^ x ^ " = " ^ (exp_to_string def) ^ " in " ^ (exp_to_string body)
  | Raise -> "raise"
  | Unassigned -> "*unassigned*"
  | App(e1, e2) ->
     "(" ^ (exp_to_string e1) ^ " " ^ (exp_to_string e2) ^ ")"
;;
  
(** Returns a string representation of the expr *)
let rec exp_to_abs_string (exp: expr) : string =
  let pabs op arglist =
    op ^ "(" ^ (CS51.reduce (fun s1 s2 -> s1 ^ ", " ^ s2) arglist) ^ ")" in
  match exp with
  | Var x -> pabs "Var" [x]
  | Num x -> pabs "Num" [if x < 0
			 then "~" ^ (string_of_int ~- x)
			 else (string_of_int x)]
  | Bool x -> pabs "Bool" [string_of_bool x]
  | Unop(op, arg) -> pabs "Unop" [op; (exp_to_abs_string arg)]
  | Binop(op, arg1, arg2) -> pabs "Binop" [op; (exp_to_abs_string arg1); (exp_to_abs_string arg2)]
  | Conditional(cond, thenbranch, elsebranch) ->
     pabs "Conditional" [(exp_to_abs_string cond);
			 (exp_to_abs_string thenbranch);
			 (exp_to_abs_string elsebranch)]
  | Fun(x, body) -> pabs "Fun" [x; (exp_to_abs_string body)]
  | Let(x, def, body) -> pabs "Let" [x; (exp_to_abs_string def); (exp_to_abs_string body)]
  | Letrec(x, def, body) -> pabs "Letrec" [x; (exp_to_abs_string def); (exp_to_abs_string body)]
  | Raise -> pabs "Raise" []
  | Unassigned -> pabs "Unassigned" []
  | App(e1, e2) -> pabs "App" [(exp_to_abs_string e1); (exp_to_abs_string e2)]
;;

