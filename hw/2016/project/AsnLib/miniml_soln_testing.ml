(* MiniML: a subset of ML
   Unit testing
   Stuart M. Shieber
   November 19, 2015
 *)

open Testing ;;
module Ev = Evaluation_soln ;;
module Miniml = Miniml_soln ;;
module Exp = Expr_soln ;;

(* Define the test suite for the expressions implementation (free variables and substitutions *)

let par = Miniml.str_to_exp ;;
  
let suite_expr = Testing.create_suite "expressions" ;;
let _ =
  Testing.add_test suite_expr "freevars int literal"
		   (lazy (Exp.same_vars (Exp.free_vars (par "3 ;;"))
					(Exp.vars_of_list []))) ;
  Testing.add_test suite_expr "freevars bool literal"
		   (lazy (Exp.same_vars (Exp.free_vars (par "true ;;"))
					(Exp.vars_of_list []))) ;
  Testing.add_test suite_expr "freevars var"
		   (lazy (Exp.same_vars (Exp.free_vars (par "x ;;"))
					(Exp.vars_of_list ["x"]))) ;
  Testing.add_test suite_expr "freevars unary"
		   (lazy (Exp.same_vars (Exp.free_vars (par "~- x ;;"))
					(Exp.vars_of_list ["x"]))) ;
  Testing.add_test suite_expr "freevars binary both"
		   (lazy (Exp.same_vars (Exp.free_vars (par "x + y ;;"))
					(Exp.vars_of_list ["x"; "y"]))) ;
  Testing.add_test suite_expr "freevars binary right"
		   (lazy (Exp.same_vars (Exp.free_vars (par "3 + y ;;"))
					(Exp.vars_of_list ["y"]))) ;
  Testing.add_test suite_expr "freevars cond all"
		   (lazy (Exp.same_vars (Exp.free_vars (par "if x then y else z ;;"))
					(Exp.vars_of_list ["x"; "y"; "z"]))) ;
  Testing.add_test suite_expr "freevars cond if else"
		   (lazy (Exp.same_vars (Exp.free_vars (par "if x then 3 else z ;;"))
					(Exp.vars_of_list ["x"; "z"]))) ;
  Testing.add_test suite_expr "freevars cond then else"
		   (lazy (Exp.same_vars (Exp.free_vars (par "if true then y else z ;;"))
					(Exp.vars_of_list ["y"; "z"]))) ;
  Testing.add_test suite_expr "freevars app"
		   (lazy (Exp.same_vars (Exp.free_vars (par "f x ;;"))
					(Exp.vars_of_list ["f"; "x"]))) ;
  Testing.add_test suite_expr "freevars let def"
		   (lazy (Exp.same_vars (Exp.free_vars (par "let x = y in x ;;"))
					(Exp.vars_of_list ["y"]))) ;
  Testing.add_test suite_expr "freevars let def body"
		   (lazy (Exp.same_vars (Exp.free_vars (par "let x = x in y ;;"))
					(Exp.vars_of_list ["x"; "y"]))) ;
  Testing.add_test suite_expr "freevars let same def"
		   (lazy (Exp.same_vars (Exp.free_vars (par "let x = x in x ;;"))
					(Exp.vars_of_list ["x"]))) ;
  Testing.add_test suite_expr "freevars let rec def"
		   (lazy (Exp.same_vars (Exp.free_vars (par "let rec x = y in x ;;"))
					(Exp.vars_of_list ["y"]))) ;
  Testing.add_test suite_expr "freevars let rec body "
		   (lazy (Exp.same_vars (Exp.free_vars (par "let rec x = x in y ;;"))
					(Exp.vars_of_list ["y"]))) ;
  Testing.add_test suite_expr "freevars let rec same def "
		   (lazy (Exp.same_vars (Exp.free_vars (par "let rec x = x in x ;;"))
					(Exp.vars_of_list []))) ;
  Testing.add_test suite_expr "freevars raise"
		   (lazy (Exp.same_vars (Exp.free_vars (par "raise ;;"))
					(Exp.vars_of_list []))) ;
  
  Testing.add_test suite_expr "subst int int"
		   (lazy ((Exp.subst "x" (par "3 ;;") (par "3 ;;"))
			    = (par "3 ;;"))) ;
  Testing.add_test suite_expr "subst int same var"
		   (lazy ((Exp.subst "x" (par "3 ;;") (par "x ;;"))
			    = (par "3 ;;"))) ;
  Testing.add_test suite_expr "subst unop"
		   (lazy ((Exp.subst "x" (par "3 ;;") (par "~- x ;;"))
			    = (par "~- 3 ;;"))) ;
  Testing.add_test suite_expr "subst binop left right"
		   (lazy ((Exp.subst "x" (par "3 ;;") (par "x + x ;;"))
			    = (par "3 + 3 ;;"))) ;
  Testing.add_test suite_expr "subst conditional all"
		   (lazy ((Exp.subst "x" (par "true ;;") (par "if x then x else x ;;"))
			  = (par "if true then true else true ;;"))) ;
  Testing.add_test suite_expr "subst conditional all"
		   (lazy ((Exp.subst "x" (par "true ;;") (par "if x then x else x ;;"))
			  = (par "if true then true else true ;;"))) ;
  
  Testing.add_test suite_expr "subst fun same"
		   (lazy ((Exp.subst "x" (par "true ;;") (par "fun x -> x ;;"))
			  = (par "fun x -> x ;;"))) ;
  Testing.add_test suite_expr "subst fun diff"
		   (lazy ((Exp.subst "x" (par "true ;;") (par "fun y -> x ;;"))
			  = (par "fun y -> true ;;"))) ;
  Testing.add_test suite_expr "subst fun diff capture"
		   (lazy ((Exp.subst "x" (par "y ;;") (par "fun y -> x ;;"))
			  <> (par "fun y -> y ;;"))) ;

  Testing.add_test suite_expr "subst let same"
		   (lazy ((Exp.subst "x" (par "true ;;") (par "let x = x in x ;;"))
			  = (par "let x = true in x ;;"))) ;
  Testing.add_test suite_expr "subst let diff"
		   (lazy ((Exp.subst "x" (par "true ;;") (par "let y = x in x ;;"))
			  = (par "let y = true in true ;;"))) ;
  
  Testing.add_test suite_expr "subst let rename"
		   (lazy ((Exp.subst "f" (par "fun x -> y + y ;;") (par "let y = 4 in f 3 ;;"))
			  <> (par "let y = 4 in (fun x -> y + y) 3 ;;"))) ;

  Testing.add_test suite_expr "subst letrec same"
		   (lazy ((Exp.subst "x" (par "true ;;") (par "let rec x = x in x ;;"))
			  = (par "let rec x = x in x ;;"))) ;
  Testing.add_test suite_expr "subst letrec diff"
		   (lazy ((Exp.subst "x" (par "true ;;") (par "let rec y = x in x ;;"))
			  = (par "let rec y = true in true ;;"))) ;

  Testing.add_test suite_expr "subst raise"
		   (lazy ((Exp.subst "x" (par "true ;;") (par "raise ;;"))
			  = (par "raise ;;"))) ;

  Testing.add_test suite_expr "subst letrec diff"
		   (lazy ((Exp.subst "x" (par "true ;;") (par "x x ;;"))
			  = (par "true true ;;"))) ;
  
  () ;;
		   
  
(* Define the test suite for the evaluation system *)
let suite_eval suite  (eval_fn: Exp.expr -> Ev.Env.env -> Ev.Env.value) =
  let env = Ev.Env.create () in
  let parseval (str: string) =
    eval_fn (Miniml.str_to_exp str) env in
  let check (exp: string) (res: string) =
    let exp = parseval exp in
    let res = parseval res in
    exp = res in

  (* functions as values *)
  Testing.add_test suite "lambda" (lazy (check "lambda x. x ;;" 
						"lambda x. x ;;"));

  (* integer literals and operators *)
  Testing.add_test suite "int literals" (lazy (check "34 ;;" "34 ;;"));
  Testing.add_test suite "addition" (lazy (check "3 + 4 ;;" "7 ;;"));
  Testing.add_test suite "multiplication" (lazy (check "3 * 4 ;;" "12 ;;"));
  Testing.add_test suite "subtraction positive result" (lazy (check "4 - 1 ;;" "3 ;;"));
  Testing.add_test suite "subtraction negative result" (lazy (check "3 - 4 ;;" "~1 ;;"));

  (* test operator precedence *)
  Testing.add_test suite "precedence +*" (lazy (check "3 + 4 * 5 ;;" "23 ;;"));
  Testing.add_test suite "precedence *+" (lazy (check "3 * 4 + 5 ;;" "17 ;;"));
  Testing.add_test suite "associativity -" (lazy (check "5 - 4 - 1 ;;" "0 ;; {not 2}"));

  (* booleans *)
  Testing.add_test suite "true literal" (lazy (check "true ;;" "true ;;"));
  Testing.add_test suite "false literal" (lazy (check "false ;;" "false ;;"));
  Testing.add_test suite "equality testing" (lazy (check "3 < 4 ;;" "true ;;"));
  Testing.add_test suite "inequality testing" (lazy (check "3 = 4 ;;" "false ;;"));
  
  (* applications *)
  Testing.add_test suite "apply identity" (lazy (check "(function x -> x * x) 5 ;;" "25 ;;"));
  Testing.add_test suite "application associativity"
		   (lazy (check "let f = (function x -> x * x) in
				  let app = (function h -> function x -> h x) in
				  app f 5 ;;"
				 "25 ;;"));
  
  (* let binding *)
  Testing.add_test suite "simple let binding"
		   (lazy (check "let x = 2 + 3 in 
				  (function x -> x) (x * 5) ;;"
				 "25 ;;"));
  
  (* test that attempt to capture a free variable in fun leads to an error *)
  Testing.add_test suite "variable capture in fun"
		   (lazy (try ignore (parseval "let x = y in
						(function y -> x) 3 ;;");
			      false
			  with Ev.EvalError _ -> true));
  (* test that attempt to capture a free variable in let leads to an error *)
  Testing.add_test suite "variable capture in let"
		   (lazy (try ignore (parseval "let f = fun x -> y + y in
						let y = 4 in
						f y ;;");
			      false
			  with Ev.EvalError _ -> true));
  
  (* test that scope is determined lexically rather than dynamically *)
  Testing.add_test suite "lexical scope" (lazy (check "let x = 1 in 
							let f = function y -> x + y in 
							let x = 2 in
							f 3 ;;"
						       "4 ;;"));

  (* test that numbers aren't applicable *)
  Testing.add_test suite "bad redex: apply int as fun"
		   (lazy (try ignore (parseval "3 4 ;;");
			      false
			  with Ev.EvalError _ -> true
			     | _ -> false));
  Testing.add_test suite "bad redex unused in let"
		   (lazy (try ignore (parseval "let exbad = (3 4) in 5 ;;");
			      false
			  with Ev.EvalError _ -> true
			     | _ -> false));
  Testing.add_test suite "bad redex used in let"
		   (lazy (try ignore (parseval "let exbad = (3 4) in exbad ;;");
			      false
			  with Ev.EvalError _ -> true
			     | _ -> false));

  (* test exceptions *)
  Testing.add_test suite "raise"
		   (lazy (try ignore (parseval "raise ;;");
			      false
			  with Ev.EvalException -> true
			     | _ -> false ));
  Testing.add_test suite "raise in context"
		   (lazy (try ignore (parseval "let x = 3 in x + raise ;;");
			      false
			  with Ev.EvalException -> true
			     | _ -> false ));
		    
  (* conditionals *)
  Testing.add_test suite "conditional then" (lazy (check "if true then 3 else 4 ;;"
							  "3 ;;"));
  Testing.add_test suite "conditional else" (lazy (check "if false then 3 else 4 ;;"
							  "4 ;;"));
  
  (* test let rec *)
  Testing.add_test suite "letrec factorial" (lazy (check "let rec fact = 
							   function y -> 
							   if y = 0 then 1 else y * (fact (y - 1)) in
							   fact 4 ;;"
							  "24 ;;"));
  Testing.add_test suite "let not recursive"
		   (lazy (try (ignore (parseval "let fact = 
								   function y -> 
								   if y = 0 then 1 else y * (fact (y - 1)) in
								   fact 4 ;;");
			       false)
			  with Ev.EvalError _ -> true
			     | _ -> false));
  Testing.add_test suite "letrec fibonacci"
		   (lazy (check "let rec fib = 
				  function x -> 
				  if x < 2 then 1 else (fib (x - 1)) + (fib (x - 2)) in
				  fib 6 ;;"
				 "13 ;;"));
  Testing.add_test suite "letrec non-well-founded"
		   (lazy (try (ignore (parseval "let rec x = x in x ;;");
			       false)
			  with Ev.EvalError _ -> true
			     | _ -> false));
		    
  (* test refs *)
  Testing.add_test suite "ref deref" (lazy (check "!(ref 42) ;;" "42 ;;")) ;
  Testing.add_test suite "ref deref in let" (lazy (check "let x = ref 42 in !x ;;" "42 ;;")) ;
  Testing.add_test suite "ref aliasing" (lazy (check "let x = ref 42 in 
						      let y = x in !y ;;" "42 ;;")) ;
  Testing.add_test suite "ref deref in def" (lazy (check "let x = ref 42 in 
							  let y = !x in y ;;" "42 ;;")) ;

  Testing.add_test suite "ref assign" (lazy (check "let x = ref 42 in
						    let y = (x := 21) in
						    !x ;;" "21 ;;")) ;

  Testing.add_test suite "ref aliasing assign" (lazy (check "let x = ref 42 in
							     let y = x in
							     let z = (x := 21) in
							     !y ;;" "21 ;;")) ;
  Testing.add_test suite "state persists" (lazy (check "let x = ref 42 in
							let f = fun y -> (let a = (x := !x + y) in !x) in
							let z = f 10 in
							z ;;" "52 ;;")) ;
  Testing.add_test suite "state persists 2" (lazy (check "let x = ref 42 in
							let f = fun y -> (let a = (x := !x + y) in !x) in
							let x = ref 12 in
							let z = f 10 in
							z ;;" "52 ;;")) ;

  suite ;;

let suite_t = suite_eval (Testing.create_suite "evaluation - trivial model") Ev.eval_t ;;
let suite_s = suite_eval (Testing.create_suite "evaluation - substitution model") Ev.eval_s ;;
let suite_d = suite_eval (Testing.create_suite "evaluation - dynamically scoped environment model") Ev.eval_d ;;
let suite_l = suite_eval (Testing.create_suite "evaluation - lexically scoped environment model") Ev.eval_l ;;

(* Execute the test suite *)
let _ = Testing.run_report suite_expr ;;
let _ = Testing.run_report suite_t ;;
let _ = Testing.run_report suite_s ;;
let _ = Testing.run_report suite_d ;;
let _ = Testing.run_report suite_l ;;
  
