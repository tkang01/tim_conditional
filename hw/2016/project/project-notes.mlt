\documentclass[fleqn]{amsart}

\input{../LaTeX/cs51-preamble}

\title{CS51 Final Project: Implementing MiniML \\ Further Notes}
\author{Stuart M. Shieber}
\date{\today}

\renewcommand{\solution}[1]{}

\hyphenation{Mini-ML}

\begin{document}

\maketitle

\linenumbers

Students may find some of the notations in the final project writeup
unfamiliar, especially in Figure~1. In this document, I describe some
of the notations for reference.

\section{Equational definition of functions}

Mathematics is full of functions, and of ways of defining them. A
standard technique is to define functions using a set of
equations. Each of the equations provides a part of the definition
based on a particular subset of the possible argument values of the
function. For instance, the factorial function, denoted by a postfix
$!$ in standard mathematical notation, is defined by these two
equations:
%
\begin{align*}
0! &= 1 \\
n! &= n \cdot (n-1)! \mbox{\quad for $n > 0$}
\end{align*}
%
Notice the following conventions: 
%
\begin{itemize}
\item A `for' or `where' clause after an equation provides further
  constraint on the applicability of that equation. In the case at
  hand, the second equation applies only when the argument $n$ is
  greater than $0$. 

\item Mathematics often uses different conventions for denoting
  operations than any given programming language. Here, for instance,
  a center dot $\cdot$ is used for multiplication instead of the
  \texttt{*} more common in programming languages. In other cases,
  juxtaposition is used for multiplication, as in\footnote{By the way,
  the notation $\frac{d }{dx} x^3$ is yet another example of a
  nonstandard notation for a function application. Here, the function
  being applied is the derivative function $\frac{d}{dx}$, its
  argument the expression $x^3$. The notational profligacy of
  mathematics -- especially having many different notations for
  functions -- hides a lot of commonality shared among mathematical
  processes.} \[ \frac{d}{dx} x^3
  = 3x^2\] where the juxtaposition of the $3$ and the $x^2$ indicates
  that they are to be multiplied. The details of these notations are
  often unspecified in
  mathematical writing, reflecting the reality that mathematics is
  written to be read by \emph{people} with sufficient common knowledge
  with the author to know the background assumptions or to figure them
  out from context. We don't have such a privilege with computers, so
  notations are typically more carefully explicated in programming
  language documentation.

\item The kind of thing that the argument must be is often left
  implicit in mathematical notation. In the factorial example, we
  didn't state explicitly that the argument of factorial must be a
  nonnegative integer, yet the
  definition is only appropriate for that case. Negative integers are
  not provided a well-founded definition for instance, nor are
  noninteger numbers. Again, the omission of these requirements is
  based on an
  assumption of shared context
  with the reader. So as not to have to make that assumption, computer
  programs that implement function definitions
  make use of type constraints (whether explicit or inferred) or
  invariant assertions or (as a last resort) documentation to capture
  these assumptions.

\item The entire set of equations defines a single function, so that
  in converting definitions of this sort to code, they will end up in
  a single function definition. The individual equations correspond to
  different cases, which will likely be manifest by conditionals or
  case statements (such as OCaml \texttt{match} expressions).
\end{itemize}

\section{Glossary of notations}

In the definition of the $FV$ function (which you will implement in
the form of \texttt{free_vars}) we take advantage of some standard set
notations, which we review here.

\begin{itemize}

\item The \firstuse{empty set}, notated $\emptyset$, is the set containing no
  members. 

\item An \firstuse{extensional} set definition (given by an
  explicit list of its members) is notated by listing the elements in
  braces separated by commas, as, for instance, $\set{1,2,3,4}$.
  Obviously, this notation only works for finite sets, although
  infinite sets can be indicated with ellipses (as
  $\set{1,2,3,\ldots})$
  in cases where the rule for filling in the remaining elements is
  sufficiently
  obvious to the reader.

\item An \firstuse{intensional} set definition (given by
  describing all members of the set rather than listing them) is
  notated by placing in braces a schematic element of the set,
  followed by a vertical bar, followed by a description of the range
  of any variables in the schema. For instance, the set of all even
  numbers might be $\set{\, x \mid x \pmod 2 = 0 \,}$, read ``the set of
  all $x$ such that $x$ is evenly divisible by 2.'' Similarly, the set
  of all squares of prime numbers would be $\set{\, x^2 \mid \mbox{$x$
      is prime}\,}$. (Note the combination of mathematical notation and
  natural language, a typical instance of
  ``\href{https://en.wikipedia.org/wiki/Code-switching}{code
    switching}'' in
  mathematical writing.)

\item The standard operations on sets are notated with infix
  operators:
  %
  \begin{description}
  \item[Union] $s \cup t$ is the \firstuse{union} of sets $s$ and $t$,
    that is, the set containing all the elements that are in either of
    the two sets;

  \item[Intersection] $s \cap t$ is the \firstuse{intersection},
    containing just the elements that are in both of the sets;

  \item[Difference] $s - t$ is the set \firstuse{difference}, all
    elements in $s$ except for those in $t$; and

  \item[Membership] $x \in s$ specifies \firstuse{membership}, stating
    that $x$ is a member of the set $s$.
  \end{description}
  %
  By way of example, the following are all true statements,
  expressed in this notation:
  %
  \begin{align*}
    \set{1,2,3} \cup \set{3,4} &= \set{1,2,3,4} \\
    \set{1,2,3} \cap \set{3,4} &= \set{3} \\
    \set{1,2,3} - \set{3,4} &= \set{1,2} \\
    3 &\in \set{1,2,3} \\
    3 &\not \in \set{2,4,6}
  \end{align*}
\end{itemize}

Note the use of a slash through a symbol to indicate its
\firstuse{negation}: $\not \in$ for `is not a member of'.

There are different notions of identity used in mathematical notation.
The $=$ symbol typically connotes two values being the same
``semantically''. The $\equiv$ symbol connotes a stronger notion of
syntactic identity, so that $x \equiv y$ means that $x$ and $y$ are
the same syntactic entity (variable say) rather than that they have
the same value (in whatever context that might be appropriate). For
instance, consider these equations found in the definition of substitution
%
\begin{align*}
  \subst{x}{P}{x} &= P \\
  \subst{y}{P}{x} &= y \mbox{\qquad where $x \not\equiv y$}
\end{align*}
%
(Recall that $\subst{P}{x}{Q}$ specifies the expression $P$ with all
free occurrences of $x$ replaced by the expression $Q$ (with care
taken not to capture any free occurrences of $x$ in $Q$). The notation
$x \not \equiv y$ indicates that the variable $y$ that constitutes the
expression being substituted into is a different variable from the
variable $x$ that is being substituted for. (In this context, $x$ and
$y$ don't have natural values that might or might not be identical in
any case.)

\end{document}