(** MiniML evaluation

    This module implements a small untyped ML-like language under
    various operational semantics. 
 *)

open Printf ;;
module Expr = Expr_soln ;;
open Expr;;
       
(* Exception for evaluator runtime generated by a runtime error *)
exception EvalError of string ;;
(* Exception for evaluator runtime generated by an explicit "raise" construct *)
exception EvalException ;;

(** {1 Setup for all versions of the semantics} *)

let eval_error message =
  raise (EvalError message) ;;
let eval_error_exp exp message =
  raise (EvalError ((exp_to_string exp) ^ " " ^ message))
;;
    
(** {1 Environments and values} *)

module type Env_type = sig
    type env
    type value =
       | Val of expr
       | Closure of (expr * env)
       | Ref of value ref
    val create : unit -> env
    val close : expr -> env -> value
    val lookup : env -> varid -> value
    val extend : env -> varid -> value ref -> env
    val env_to_string : env -> string
    val value_to_string : ?printenvp:bool -> value -> string
  end
			 
module Env : Env_type =
  struct

    type env = (varid * value ref) list
     and value =
       | Val of expr
       | Closure of (expr * env)
       | Ref of value ref

    exception EnvUnbound

    (* Creates an empty environment *)
    let create () : env = []

    (* Creates a closure from an expression and the environment it's
       defined in *)
    let close (exp: expr) (env: env) : value = Closure(exp, env)

    (* Looks up the value of a variable in the environment *)
    let lookup (env: env) (varname: varid) : value =
      if List.mem_assoc varname env then !(List.assoc varname env)
      else eval_error_exp (Var varname) "unbound"

    (* Returns a new environment just like env except that it maps the
       variable varid to loc *)
    let extend (env: env) (varname: varid) (loc: value ref) : env =
      (varname, loc) ::
	if List.mem_assoc varname env
	then List.remove_assoc varname env
	else env

    (* Returns a printable string representation of an environment *)
    let env_to_string (env: env) : string =
      if env = []
      then "[]"
      else "[" ^ (CS51.reduce
		    (fun s1 s2 -> s1 ^ ", " ^ s2)
		    (List.map (function (varname, loc) ->
					(match !loc with
					 | Val exp ->
					    varname ^ "->" ^ (exp_to_string exp)
					 | Closure(exp, _envp) ->
					    varname ^ "->" ^ (exp_to_string exp)
					    ^ " closed"))
			      env)) ^ "]"

    (* Returns a printable string representation of a value; the flag
       printenvp determines whether to include the environment in the
       string representation when called on a closure *)
    let rec value_to_string ?(printenvp : bool = true) (v: value) : string =
      match v with
      | Val exp -> exp_to_string exp
      | Ref vref -> "ref " ^ (value_to_string ~printenvp (!vref))
      | Closure(exp, env) ->
	 (exp_to_string exp)
	 ^ (if printenvp then " where " ^ (env_to_string env) else "")

  end
;;
    
(** {1 Evaluators} *)

module type Evaluator =
  sig
    type expr
    type env
    type value
    val eval : expr -> env -> value
    val value_to_string : ?printenvp:bool -> value -> string
  end

       
(** {2 Trivial evaluator -- the identity function} *)

module TrivEval: (Evaluator with type expr = Expr.expr
			     and type env = unit
			     and type value = Expr.expr)  =
  struct
    type expr = Expr.expr
    type env = unit
    type value = expr
	
    let value_to_string ?(printenvp : bool = true) (v: value) : string =
      exp_to_string v ;;

    let eval (e : expr) () : value = e ;;
  end

(** {2 Evaluation via substitution model} *)

module SubstEval: (Evaluator with type expr = Expr.expr
			      and type env = unit
			      and type value = Expr.expr)  =
  struct
    type expr = Expr.expr
    type env = unit
    type value = expr
	
    let value_to_string ?(printenvp : bool = true) (v: value) : string =
      exp_to_string v ;;
      	   
    let eval_unop op exp =
      match op with
      | "~" -> (match exp with
		| Num x -> Num (~- x)
		| _ -> eval_error_exp (Unop(op, exp)) "argument error")
      | _ -> eval_error_exp (Unop(op, exp)) "unknown unop"
    ;;
      
    let eval_binop op exp1 exp2=
      match op with
      | "+" -> (match (exp1, exp2) with
		| (Num x1, Num x2) -> Num (x1 + x2)
		| _ -> eval_error_exp (Binop(op, exp1, exp2)) "argument error")
      | "-" -> (match (exp1, exp2) with
		| (Num x1, Num x2) -> Num (x1 - x2)
		| _ -> eval_error_exp (Binop(op, exp1, exp2)) "argument error")
      | "*" -> (match (exp1, exp2) with
		| (Num x1, Num x2) -> Num (x1 * x2)
		| _ -> eval_error_exp (Binop(op, exp1, exp2)) "argument error")
      | "=" -> (match (exp1, exp2) with
		| (Num x1, Num x2) -> Bool (x1 = x2)
		| _ -> eval_error_exp (Binop(op, exp1, exp2)) "argument error")
      | "<" -> (match (exp1, exp2) with
		| (Num x1, Num x2) -> Bool (x1 < x2)
		| _ -> eval_error_exp (Binop(op, exp1, exp2)) "argument error")
      | _ -> eval_error_exp (Binop(op, exp1, exp2)) "unknown binop"
    ;;
      
    let rec eval_s (exp : expr) : expr =
      match exp with
      (* values *)
      | Num _ -> exp
      | Bool _ -> exp
      | Fun(varname, body) -> exp
      (* primitives *)
      | Unop(op, arg) ->
	 eval_unop op (eval_s arg)
      | Binop(op, arg1, arg2) ->
	 eval_binop op (eval_s arg1) (eval_s arg2)
      (* conditionals *)
      | Conditional(cond, thenbranch, elsebranch) ->
	 if eval_s cond = Bool true
	 then eval_s thenbranch
	 else eval_s elsebranch
      (* variables -- should have been replaced by now *)
      | Var _ -> eval_error_exp exp "unbound"
      (* define variables *)
      | Let(x, def, body) ->
	 eval_s (App(Fun(x, body), def))
      | Letrec(x, def, body) ->
	 eval_s (subst x (eval_s (subst x (Letrec(x, def, Var x)) def)) body) 
      (* raise exception *)
      | Raise -> raise EvalException
      (* unassigned value *)
      | Unassigned -> eval_error "unassigned"
      (* applications *)
      | App(e1, e2) ->
	 let e1val = eval_s e1 in
	 match e1val with
	 | Fun(var_name, body) ->
	    eval_s (subst var_name (eval_s e2) body)
	 | _ -> eval_error_exp exp "bad redex"
    ;;

    let eval (exp : expr) () : expr =
      eval_s exp ;;
      
  end
    
(** {2 Evaluation against an environment: dynamically scoped version} *)

module DynEval : (Evaluator with type expr = Expr.expr
			     and type env = Env.env
			     and type value = Env.value)  =
  struct
    type expr = Expr.expr
    type env = Env.env
    type value = Env.value
		  
    let value_to_string = Env.value_to_string ;;
      
		   
    let eval_unop (op: string) (v: Env.value) : Env.value =
      match op with
      | "~" -> (match v with
		| Env.Val (Num x) -> Env.Val (Num (~- x))
		| _ -> eval_error "negation requires a num")
      | _ -> eval_error ("unknown unop " ^ op)
    ;;

    let eval_binop (op: string) (v1: Env.value) (v2: Env.value) : Env.value =
      match op with
      | "+" -> (match (v1, v2) with
		| ((Env.Val (Num e1)), (Env.Val (Num e2))) ->
		   Env.Val (Num (e1 + e2))
		| _ -> eval_error "plus requires two nums")
      | "-" -> (match (v1, v2) with
		| ((Env.Val (Num e1)), (Env.Val (Num e2))) ->
		   Env.Val (Num (e1 - e2))
		| _ -> eval_error "minus requires two nums")
      | "*" -> (match (v1, v2) with
		| ((Env.Val (Num e1)), (Env.Val (Num e2))) ->
		   Env.Val (Num (e1 * e2))
		| _ -> eval_error "times requires two nums")
      | "=" -> (match (v1, v2) with
		| ((Env.Val (Num e1)), (Env.Val (Num e2))) ->
		   Env.Val (Bool (e1 = e2))
		| _ -> eval_error "equals requires two nums")
      | "<" -> (match (v1, v2) with
		| ((Env.Val (Num e1)), (Env.Val (Num e2))) ->
		   Env.Val (Bool (e1 < e2))
		| _ -> eval_error "lessthan requires two nums")
      | _ -> eval_error ("unknown binop " ^ op)
    ;;

    let rec eval (exp: expr) (env: Env.env) : Env.value =
      match exp with
      (* values *)
      | Num x -> Env.Val exp
      | Bool x -> Env.Val exp
      | Fun(varname, body) -> Env.Val exp
      (* primitives *)
      | Unop(op, arg) ->
	 eval_unop op (eval arg env)
      | Binop(op, arg1, arg2) ->
	 eval_binop op (eval arg1 env) (eval arg2 env)
      (* conditionals *)
      | Conditional(cond, thenbranch, elsebranch) ->
	 if eval cond env = Env.Val (Bool true)
	 then eval thenbranch env
	 else eval elsebranch env
      (* lookup variables *)
      | Var x ->
	 let v = Env.lookup env x in
	 if v = Env.Val Unassigned
	 then eval_error_exp exp "unassigned variable"
	 else v
      (* define variables *)
      | Let(x, def, body) ->
	 eval body (Env.extend env x (ref (eval def env)))
      | Letrec(x, def, body) ->
	 (* prepare a location for the recursive variable with a temporary bad value *)
	 let recloc = ref (Env.Val Unassigned) in
	 (* extend the environment with the new binding to the temporary bad value *)
	 let env' = Env.extend env x recloc in
	 (* evaluate the definition in this environment; it better not use x yet *)
	 let defval = eval def env' in
	 (* replace the binding with the just computed def value *)
	 recloc := defval;
	 (* evaluate the body in this environment *)
	 eval body env'
      (* raise exception *)
      | Raise -> raise EvalException
      (* unassigned value *)
      | Unassigned -> eval_error "unassigned"
      (* applications *)
      | App(e1, e2) ->
	 let e1val = eval e1 env in
	 match e1val with
	 | Env.Val(Fun(var_name, body)) ->
	    eval body (Env.extend env var_name (ref (eval e2 env)))
	 | _ -> eval_error_exp exp "bad redex"
    ;;

  end

(** {2 Evaluation against an environment: lexically scoped version} *)
    
module LexEval : (Evaluator with type expr = Expr.expr
			     and type env = Env.env
			     and type value = Env.value)  =
  struct
    type expr = Expr.expr
    type env = Env.env
    type value = Env.value
		  
    let value_to_string = Env.value_to_string ;;
		   
    let eval_unop (op: string) (v: Env.value) : Env.value =
      match op with
      | "~" -> (match v with
		| Env.Val (Num x) -> Env.Val (Num (~- x))
		| _ -> eval_error "negation requires a num")
      | "ref" -> Env.Ref (ref v)
      | "!" -> (match v with
		| Env.Ref vref -> !vref
		| _ -> eval_error "dereference requires a ref")
      | _ -> eval_error ("unknown unop " ^ op)
    ;;

    let eval_binop (op: string) (v1: Env.value) (v2: Env.value) : Env.value =
      match op with
      | "+" -> (match (v1, v2) with
		| ((Env.Val (Num e1)), (Env.Val (Num e2))) ->
		   Env.Val (Num (e1 + e2))
		| _ -> eval_error "plus requires two nums")
      | "-" -> (match (v1, v2) with
		| ((Env.Val (Num e1)), (Env.Val (Num e2))) ->
		   Env.Val (Num (e1 - e2))
		| _ -> eval_error "minus requires two nums")
      | "*" -> (match (v1, v2) with
		| ((Env.Val (Num e1)), (Env.Val (Num e2))) ->
		   Env.Val (Num (e1 * e2))
		| _ -> eval_error "times requires two nums")
      | "=" -> (match (v1, v2) with
		| ((Env.Val (Num e1)), (Env.Val (Num e2))) ->
		   Env.Val (Bool (e1 = e2))
		| _ -> eval_error "equals requires two nums")
      | "<" -> (match (v1, v2) with
		| ((Env.Val (Num e1)), (Env.Val (Num e2))) ->
		   Env.Val (Bool (e1 < e2))
		| _ -> eval_error "lessthan requires two nums")
      | ":=" -> (match v1 with
		| Env.Ref r -> r := v2; Env.Val (Bool true)
		| _ -> eval_error "assignment requires a ref")
      | _ -> eval_error ("unknown binop " ^ op)
    ;;

    let rec eval (exp: expr) (env: Env.env) : Env.value =
      match exp with
      (* values *)
      | Num x -> Env.Val exp
      | Bool x -> Env.Val exp
      | Fun(varname, body) -> Env.close exp env
      (* primitives *)
      | Unop(op, arg) ->
	 eval_unop op (eval arg env)
      | Binop(op, arg1, arg2) ->
	 eval_binop op (eval arg1 env) (eval arg2 env)
      (* lookup variables *)
      | Var x ->
	 let v = Env.lookup env x in
	 if v = Env.Val Unassigned
	 then eval_error_exp exp "unassigned variable"
	 else v
      (* conditionals *)
      | Conditional(cond, thenbranch, elsebranch) ->
	 if eval cond env = Env.Val (Bool true)
	 then eval thenbranch env
	 else eval elsebranch env
      (* define variables *)
      | Let(x, def, body) ->
	 eval body (Env.extend env x (ref (eval def env)))
      | Letrec(x, def, body) ->
	 (* prepare a location for the recursive variable with a temporary bad value *)
	 let recloc = ref (Env.Val Unassigned) in
	 (* extend the environment with the new binding to the temporary bad value *)
	 let env' = Env.extend env x recloc in
	 (* evaluate the definition in this environment; it better not use x yet *)
	 let defval = eval def env' in
	 (* replace the binding with the just computed def value *)
	 recloc := defval;
	 (* evaluate the body in this environment *)
	 eval body env'
      (* raise exception *)
      | Raise -> raise EvalException
      (* unassigned value *)
      | Unassigned -> eval_error "unassigned"
      (* applications *)
      | App(e1, e2) ->
	 let e1val = eval e1 env in
	 match e1val with
	 | Env.Closure(Fun(var_name, body), cenv) ->
	    eval body (Env.extend cenv var_name (ref (eval e2 env)))
	 | _ -> eval_error_exp exp "bad redex"
    ;;

  end
  		 
(** The external evaluator, which can be either the identity function,
    the substitution model version or the dynamic or lexical
    environment model version. *)

let eval_t exp env = Env.Val (TrivEval.eval exp ()) ;;
let eval_s exp env = Env.Val (SubstEval.eval exp ()) ;;
let eval_d = DynEval.eval ;;
let eval_l = LexEval.eval ;;

let evaluate = eval_d ;;
