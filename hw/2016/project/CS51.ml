
(** {1 Utilities} *)

(** Traditional higher-order reduce function, applies [f] to the
    elements of list left-to-right (as in [fold_left]). *)
let reduce f list = 
  match list with
  | head::tail -> List.fold_left f head tail
  | [] -> failwith "can't reduce empty list"

(** [call_timed f x] applies f to x returning the result reporting
    timing information on stdout as a side effect. *)
let call_timed f x = 
  let t0 = Unix.gettimeofday() in 
  let result = f x in 
  let t1 = Unix.gettimeofday() in 
  Printf.printf "time (msecs): %f\n" ((t1 -. t0) *. 1000.);
  result ;;
