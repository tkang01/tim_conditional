"01234" ^ "56789" ;;
type expr =
  | Num of int
  | Unop of string * expr
  | Binop of string * expr * expr
;;
Binop ("+", Num 3, Binop ("*", Num 4, Num 5)) ;;
