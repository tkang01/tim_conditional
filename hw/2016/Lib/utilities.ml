(** Some utilities

    @author: Stuart M. Shieber
    @version: August 19, 2015
 *)

(** An unwind-protect from Yaron Minsky.  See
    http://caml.inria.fr/pub/ml-archives/caml-list/2003/07/5ff669a9d2be35ec585b536e2e0fc7ca.en.html *)
let protect ~f ~(finally: unit -> unit) =
  let result = ref None in	
  try 
    result := Some (f ());
    raise Exit
  with
    Exit as e ->
    finally ();
    (match !result with Some x -> x | None -> raise e)
  | e ->
     finally (); raise e
;;

(** Allow redirection of stdout. See
    https://stackoverflow.com/questions/19527859/redirect-standard-output-ocaml
    and http://pleac.sourceforge.net/pleac_ocaml/fileaccess.html for
    background. *)
let redirect_stdout filename (f: unit -> 'a) =
  flush stdout;
  let new_stdout = open_out filename in
  let old_stdout = Unix.dup Unix.stdout in
  Unix.dup2 (Unix.descr_of_out_channel new_stdout) Unix.stdout;
  protect ~f:f
	  ~finally: (fun ()  ->
		     Unix.dup2 old_stdout Unix.stdout)
;;

(* Here's some test code that demonstrates redirection and the unwind-protect. 
let test raisep =
  print_endline "before redirect";
  redirect_stdout "redirect"
		  (fun () ->
		   print_endline "after redirect";
		   if raisep then raise (Failure "forced exception") else () );
  print_endline "after restore";;
 *)
