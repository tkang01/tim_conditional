(** General utilities for CS51
    Stuart M. Shieber

    Note that some of these may not work on Windows systems, as they
    do not provide full support for Sys and Unix modules. Conditional
    execution could be added using Sys.os_type.  *)


(** {Functional utilities} *)

(** [reduce f list] applies [f] to the elements of list left-to-right
    (as in [fold_left]) using first element as initial. This is a
    traditional higher-order function, standard in the literature, but
    an oversight in not appearing in the Pervasives or List module. *)
let reduce f list = 
  match list with
  | head::tail -> List.fold_left f head tail
  | [] -> failwith "can't reduce empty list"

		   
(** {Performance monitoring} *)

(** [call_timed f x] applies [f] to [x] returning the result,
    reporting timing information on stdout as a side effect. *)
let call_timed f x = 
  let t0 = Unix.gettimeofday() in 
  let result = f x in 
  let t1 = Unix.gettimeofday() in 
  Printf.printf "time (msecs): %f\n" ((t1 -. t0) *. 1000.);
  result ;;
