
(** A very simple unit testing framework for CS51

    @author Stuart M. Shieber
    @version 0.5 of August 6, 2015
 *)

(** {2 Timeout handling} from Nate Foster of Cornell *)

exception Timeout
let sigalrm_handler = Sys.Signal_handle (fun _ -> raise Timeout)
let timeout (time : int) (f : 'a Lazy.t) : 'a =
   let old_behavior = Sys.signal Sys.sigalrm sigalrm_handler in
   let reset_sigalrm () = ignore (Unix.alarm 0); Sys.set_signal Sys.sigalrm old_behavior in
   ignore (Unix.alarm time) ;
   let res = Lazy.force f in reset_sigalrm () ; res
;;
(* This seems only to work with .byte rather than .native compilation *)

(** {2 Testing suites} The testing module defines a test suite data structure and
    functions for adding tests to a suite and executing the tests to
    generate a report. *)

(** The [test] type stores information about a single unit test including:
    [label]: a mnemonic label string
    [content]: a delayed computation that comprises the test; returns [true] on passing, [false] on failing
    [fail_msg]: a more detailed string describing what would cause a failure
    [time_allocation]: the maximum number of seconds to allow the test to run before timing out
    [points]: the number of points for passing the test 

    The [content] should not raise an exception. If you want to test
    code that *should* raise an exception, wrap it in a try...with to
    handle the exception yourself. 
 *)
type test =
    {label: string;
     content: bool Lazy.t;
     fail_msg: string;
     time_allocation: int;
     points: int
    } ;;
  
(** The [suite] type stores the [label] and current set of [tests] for
    a test suite. Each test has a name, a deferred computation and a
    failure message.  

    The tests are executed in order of their being added by
    [add_test]. If you want there to be state shared between multiple
    tests, the tests can be wrapped in a let...in to store the state.
 *)
type suite =
    {label: string; (* name of the test suite *)
     mutable tests: test list (* list of unit tests *)
    }

(** The possible results of a test are values of the [status] type. *)
type status = Passed (** the test passed *)
	    | Failed of string (** the test failed for reason given by [string] *)
	    | Timed_out of int (** the test timed out after [int] seconds *)
	    | Raised_exn of string (** the test raised an inappropriate exception described by [string] *)
			      
let status_to_string status =
  match status with
  | Passed -> "passed"
  | Failed msg -> "failed " ^ msg
  | Timed_out time -> "timed out after " ^ string_of_int time ^ " seconds"
  | Raised_exn msg -> "raised " ^ msg ;;
    
(** [create_suite name] creates an empty test suite with the given [name] *)
let create_suite suite_name = {label = suite_name; tests = []}
				
(** [add_test suite label test ?failmsg]: Add a unit test to the
    [suite]. The new test has a name given by [label], the unit [test]
    code to be executed at test time (a delayed computation). The
    string [failmsg] is an optional string describing the failure in
    case the test fails. The optional [timeout_time] (default 5
    seconds) gives a maximum time in seconds before the test fails for
    timing out. The optional [points] determines the number of points
    (default 1) for passing the test.  *)
let add_test ?(failmsg="somehow") ?(points=1) ?(timeout_time=5) suite label test =
  suite.tests <- suite.tests @
		   [ {label = label;
		      content = test;
		      fail_msg = failmsg;
		      points = points;
		      time_allocation = timeout_time} ] ;;

(** [max_points suite]: Returns the maximum number of points available
    in the test suite, the sum of the points for each test. *)

let max_points {label = _; tests = tsts} =
  List.fold_left (+) 0 (List.map (fun tst -> tst.points) tsts) ;;  
		  
(** [run_tests suite]: Runs all of the tests of the provided suite in
    the order that they were added, calling a [result_continuation]
    function for each result. Returns a list of all the results
    returned by the results continuation.

    The continuation function is called with a triple of the test's
    [label], its [status], and the number of points allocated to
    it. The points allocated are the number associated to the test
    whether or not the test was passed.  

    Stdout is redirected to a file during running of the tests so that
    it can be captured but doesn't get in the way of the test
    frameworks output.  
 *) 
let run_tests {label = _; tests = tsts} result_continuation =
  let run_test (tst : test) = 
    try
      Printf.printf "Standard output for test \"%s\":\n" tst.label;
      flush stdout;
      if timeout tst.time_allocation tst.content
      then (result_continuation (tst.label, Passed, tst.points))
      else (result_continuation (tst.label, Failed tst.fail_msg, tst.points))
    with
    | Timeout -> (result_continuation (tst.label, Timed_out tst.time_allocation, tst.points))
    | exn -> (result_continuation (tst.label, Raised_exn (Printexc.to_string exn), tst.points))
  in
  Utilities.redirect_stdout "redirectedStdout.txt"
			    (fun () -> List.map run_test tsts)
;;

(* [status_to_points status points] returns the number of points
   awarded for a test that executed with a given [status] and that can
   award the given [points]. Points are awarded only if the test is
   passed, otherwise 0 points are awarded.
 *)
let status_to_points status points =
  match status with
  | Passed -> points
  | Failed _msg -> 0
  | Timed_out _time -> 0
  | Raised_exn _msg -> 0 ;; 

(* [present_report suite_name results]: Generates a nicely formatted
   report on stdout on the [results] of the suite named
   [suite_name]. The [results] are a list of triples giving the label,
   execution status, and number of points for each test in the suite,
   as generated for instance by [run_report].  
 *)
let present_report suite_name results =
  let label_width =
    (* max width of test labels for alignment *)
    List.fold_left max 0
		   (List.map (function (label,_,_) -> String.length label) results)
  in begin
    Printf.printf "*** Test suite %s ***\n" suite_name;
    List.iter (fun (label, status, points) ->
	       Printf.printf "%-*s : [%d/%d] %s\n" label_width label
			     (status_to_points status points) points
			     (status_to_string status))
	      results
  end ;;

(** [run_report suite]: Runs all of the tests of the provided suite in
    the order that they were added, printing to stdout a report on the
    results. *)
let run_report suite =
  present_report suite.label (run_tests suite (fun x -> x)) ;;

(** {2 Vocareum support}
    Allows generating reports to stdout to communicate with the
    Vocareum system to report the grades. See
    <https://labs.vocareum.com/docs/VocareumCommands.htm> for further
    information on the Vocareum Command Interface. 

    Vocareum uses a unique integer id to tie together the
    commands. That id needs to be pritned to stdout before any
    student code is run. *)

(* The maximum integer for generating id numbers. *)
let vocareum_max_int = 10000 ;;

(* [vocareum_init ()]: Generate and return the Vocareum id for this
   invocation, informing Vocareum immediately. This needs to be run
   before running any user code. 
*)
let vocareum_init () =
  let () = Random.self_init () in
  let id = Random.int vocareum_max_int in
  begin
    Printf.printf "VOC::ID=%d\n" id ;
    id
  end
;;

(* [vocareum_announce id key_val_alist]: Prints on stdout the Vocareum
   commands associated with the given [key_val_alist].  
 *)
let vocareum_announce id key_val_list =
  List.iter (fun (key, value) ->
	     Printf.printf "VOC::%d::%s=%S\n"
			   id (String.uppercase key) value)
	    key_val_list ;;

(** [run_vocareum_report ?detailp ?gradep ?id suite]: Generates on
    stdout a report including communication through the Vocareum
    command interface for informing the Vocareum backend about the
    results of executing the test [suite]. 

    If detailp is true (default true), then a student-oriented
    per-test report is provided. This is appropriate for a submission
    script and for a grading script. If gradep is true (default
    false), then the Vocareum site is instructed to record a single
    grade for the test suite given as the sum of the points associated
    with all of the passed tests in the suite.

    If id is provided, it is used as the unique if for Vocareum. If it
    is not provided but needed (because gradep is true), a new id will
    be generated. The id used (whether generated or provided) is
    returned, allowing chaining of reports, as:

         let id = run_vocareum_report ~gradep suite1 in
         run_vocareum_report ~idoption:id ~gradep

    For CS51, use detailp=true and gradep=false for submission scripts
    and detailp=true and gradep=true for grading scripts.
 *)
let run_vocareum_report ?(detailp=true) ?(gradep=false)
			?(idoption:int option=None)
			({label = suite_name; tests = _} as suite) =
  let id = (match idoption with
	    | None -> if gradep then vocareum_init () else 0
	    | Some id -> id) in
  let results = run_tests suite (fun x -> x) in
  let score = List.fold_left (+) 0
			     (List.map (fun (_, status, points) ->
					(status_to_points status points))
				       results) in
  begin
    if gradep then vocareum_announce id
				     [("criterion", suite_name);
				      ("grade", string_of_int score)] ;
    if detailp then present_report suite_name results;
    if gradep then Some id else None
  end
;;
  
				      
