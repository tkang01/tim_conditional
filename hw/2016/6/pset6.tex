\documentclass{amsart}

\input{../LaTeX/cs51-preamble}
\include{graphics}

\title[CS51 Problem Set 6]{CS51 Problem Set 6:\\Refs, Streams, and Music}
%\author{Stuart M. Shieber} \date{\today}

\begin{document}

\maketitle
\linenumbers

\textbf{This problem set is due April 8, 2016 at 5:00pm EST.}

\setcounter{section}{-1}
\section{Introduction}

As in the previous assignments, all of your programs must
compile. \textbf{Programs that do not compile will receive an
automatic zero.} Make sure that the functions you are asked to write
have the correct names and the number and type of arguments specified
in the assignment. Finally, please pay attention to style and follow
the style guidelines posted on the course web site. Think carefully
before writing the code, and try to come up with simple, elegant
solutions. As usual, to download the problem set, follow the
instructions found \href{http://tiny.cc/cs51reference}{here}.

\begin{itemize}
\item \textbf{Testing:} This week, we are only requiring that you
  write explicit tests for \textbf{part 1}, placed in the file
  \texttt{refs\_test.ml}. For each of the functions that you write in
  this part, you should, include at least two tests. We encourage you
  to view this as a minimum; you should test your functions until you
  are sure that they work, which in many cases will require more than
  two simple tests. For the other parts, you are not required to
  submit tests, but you ought to convince yourself that your solution
  is correct. For \textbf{part 3}, testing using traditional methods
  is difficult but there is an easy (and hopefully fun) way to
  determine if your code works.
\item \textbf{Grading:} Your code will be evaluated on correctness,
  design (including testing), and style (see
  \href{http://tiny.cc/cs51styleguide}{style guide}).
\item \textbf{Collaboration policy:} You are encouraged to work with a
  partner on this problem set.
\item \textbf{Time Spent:} At the bottom of each file, you will notice
  a prompt for you to enter the approximate number of minutes that you
  spent (per person, averaged) working on that part. We are interested
  in your sincere answers, and this will help us tailor future problem
  sets.
\end{itemize}

\section{Mutable lists and cycles} 

Do these problems in \texttt{refs.ml}, using the provided definitions
of mutable lists. \textbf{Remember that you must test each function in
  this part in \texttt{refs\_test.ml}.} You will not be graded for
time or space complexity.

Recall from lecture that a major difficulty with mutable lists is that
it is possible to introduce cycles, links to elements that appeared
earlier in the list, which cause a naive traversal of the list to loop
forever.

Here is an example of a mutable list that doesn't have a cycle, even
though the same element is repeated twice (MList A):
% 
\begin{center}
\begin{tikzpicture}[scale=0.2]
\tikzstyle{every node}+=[inner sep=0pt]
\draw [black] (9.4,-15) circle (3);
\draw (9.4,-15) node {$1$};
\draw [black] (22.1,-15) circle (3);
\draw (22.1,-15) node {$2$};
\draw [black] (36,-15) circle (3);
\draw (36,-15) node {$3$};
\draw [black] (50.5,-15) circle (3);
\draw (50.5,-15) node {$2$};
\draw [black] (12.4,-15) -- (19.1,-15);
\fill [black] (19.1,-15) -- (18.3,-14.5) -- (18.3,-15.5);
\draw [black] (25.1,-15) -- (33,-15);
\fill [black] (33,-15) -- (32.2,-14.5) -- (32.2,-15.5);
\draw [black] (39,-15) -- (47.5,-15);
\fill [black] (47.5,-15) -- (46.7,-14.5) -- (46.7,-15.5);
\end{tikzpicture}
\end{center}
%
Here is an example of a mutable list with a cycle (MList B):
\begin{center}
\begin{tikzpicture}[scale=0.2]
\tikzstyle{every node}+=[inner sep=0pt]
\draw [black] (9.4,-15) circle (3);
\draw (9.4,-15) node {$1$};
\draw [black] (22.1,-15) circle (3);
\draw (22.1,-15) node {$2$};
\draw [black] (36,-15) circle (3);
\draw (36,-15) node {$3$};
\draw [black] (29.1,-25.3) circle (3);
\draw (29.1,-25.3) node {$2$};
\draw [black] (12.4,-15) -- (19.1,-15);
\fill [black] (19.1,-15) -- (18.3,-14.5) -- (18.3,-15.5);
\draw [black] (25.1,-15) -- (33,-15);
\fill [black] (33,-15) -- (32.2,-14.5) -- (32.2,-15.5);
\draw [black] (34.33,-17.49) -- (30.77,-22.81);
\fill [black] (30.77,-22.81) -- (31.63,-22.42) -- (30.8,-21.86);
\draw [black] (27.41,-22.82) -- (23.79,-17.48);
\fill [black] (23.79,-17.48) -- (23.82,-18.42) -- (24.65,-17.86);
\end{tikzpicture}
\end{center}
% 

\begin{problem}\label{prob:hascycle}
  Write a function \texttt{has\_cycle} that returns \texttt{true} if
  and only if a mutable list has a cycle. Your function must not alter
  the original mutable list, and it must terminate eventually.

  It may be helpful to write a helper function that tells you whether
  you have traversed a node before. Testing whether the ref \texttt{a}
  points to \texttt{b} can be done with \texttt{(!a) == b}. The
  \texttt{(==)} function tests equality at the level of memory
  location rather than value.
\end{problem}

\begin{problem}
  Write a function \texttt{flatten} that takes a mutable list and
  removes any cycle in it destructively by removing backward
  links. This means that the data structure should be changed
  \emph{in-place}, such that the list passed as an argument itself is
  altered if necessary. Note that this is very different from the
  functional programming approach that we have been using up to this
  point, where functions might return an altered \emph{copy} of a data
  structure. Suppose you pass in MList B, as shown above;
  \texttt{flatten} should alter it such that it looks like MList A,
  and then return \texttt{unit}. If you are unsure how to
  destructively alter a mutable list, take a look at
  \textbf{\texttt{reflist}} in \texttt{refs\_test.ml}.
\end{problem}

\begin{problem}
  Write \texttt{mlength}, which finds the number of elements in a
  mutable list. This should always terminate, \emph{even if the list
    has a cycle}. For example, both MList A and MList B have length
  4. \texttt{mlength} must be \emph{nondestructive}, that is, the
  original mutable list should not change.
\end{problem}

\begin{problem}
  \textbf{Challenge problem:} It's possible to complete
  Problem~\ref{prob:hascycle} in such a way that it doesn't use any
  additional space other than that taken up by the list passed as an
  argument.  Attempt this only if you've finished the rest of the
  assignment and are up for a challenge.
\end{problem} 

\section{Lazy evaluation}

In this section you'll gain practice with lazy evaluation using the
OCaml \texttt{Lazy} module through problems with infinite streams and
infinite trees.

\subsection{Series acceleration with infinite streams}

In lecture, we showed how to use Taylor series to approximate
$\pi$. The code needed to do so is provided in a module
\texttt{NativeLazyStreams}. The method relies on generating the series
of terms of a Taylor series, computing the partial sums that
approximate $\pi$, and then finding a pair of consecutive
approximations that are within the desired tolerance $\epsilon$, as
shown here:
%
\begin{minted}{ocaml}
# let pi_sums = sums pi_stream;;
val pi_sums : float stream = <lazy>
# first 5 pi_sums ;;
- : float list =
[4.; 2.66666666666666696; 3.46666666666666679; 2.89523809523809561;
 3.33968253968254025]
# within 0.1 pi_sums ;;
- : int * float = (19, 3.09162380666784)
# within 0.01 pi_sums ;;
- : int * float = (199, 3.13659268483881615)
# within 0.001 pi_sums ;;
- : int * float = (1999, 3.14109265362104129)
# within 0.0001 pi_sums ;;
- : int * float = (19999, 3.14154265358982476)
\end{minted}
%
The method works, but converges quite slowly. Notice that it takes
some 200 terms in the expansion to get within 0.01 of $\pi$. I nthis
section of the problem set, you will use a technique called
\emph{series acceleration} to speed up the process of converging on a
value. The necessary code for you to use and modify can be found in
the file \texttt{streamtrees.ml}.

But we can increase the speed dramatically through a technique called
\emph{series acceleration}. A simple method is to average adjacent
elements in the approximation stream.

\begin{problem}
  Write a function average that takes a \texttt{float stream} and
  returns a stream of floats each of which is the average of adjacent
  values in the input stream. For example:
%
\begin{minted}{ocaml}
# first 5 (average (to_float nats)) ;;
- : float list = [0.5; 1.5; 2.5; 3.5; 4.5]
\end{minted}
%
You should then be able to define a stream \texttt{pi\_avgs} of the
averages of the partial sums in \texttt{pi\_sums}. How many steps does
it take to get within 0.01 of $\pi$ using \texttt{pi\_avgs} instead of
\texttt{pi\_sums}?
\end{problem}

An even better accelerator is Aitken's method. Given a sequence $s$,
Aitken's method generates the sequence $s'$ given by
%
\[ s'_n = s_n -  \frac{(s_n - s_{n-1})^2}{s_n - 2 s_{n-1} +
  s_{n-2}} \]

\begin{problem}
  Implement a function \texttt{aitken} that applies Aitken's method to
  a \texttt{float stream} returning the resulting \texttt{float stream}.
\end{problem}

\begin{problem}
  Try the various methods to compute approximations of $\pi$ and fill
  out the table in the \texttt{streamtrees.ml} file with what you
  find.
\end{problem}


\subsection{Infinite trees}

Just as streams are a lazy form of list, we can have a lazy form of
trees. In the definition below, each node in a lazy tree of type
\texttt{'a tree} holds a value of some type \texttt{'a}, and a
(conventional, finite) list of one or more (lazy) child trees.

Complete the implementation by writing functions \texttt{node},
\texttt{children}, \texttt{print\_depth}, \texttt{tmap},
\texttt{tmap2}, and \texttt{bfenumerate} as described in
\texttt{streamtrees.ml}.  We recommend implementing them in that order.

\section{The song that never ends}

In this part, we will explore a fun application of streams: music.
Before you begin, if you're not already familiar with basic music
notation and theory, you should learn some of these concepts. Numerous
online tutorials exist, such as
\href{http://datadragon.com/education/reading/}{this one}, though if
you're short on time, the following introduction should be sufficient
for this assignment.

\newcommand{\mflat}{\ensuremath{\flat}}
\newcommand{\msharp}{\ensuremath{\sharp}}

The major musical objects we see in this assignment are \emph{notes} and
\emph{rests}.  Notes indicate when a sound is heard, and rests indicate a
certain period of silence. Notes have a pitch and a duration, rests
have only a duration. A pitch has one of twelve names: C, D{\mflat}
(pronounced D-flat, also called C{\msharp}, pronounced C-sharp), D,
E\mflat (also called D{\msharp}), E, F, G{\mflat} (F{\msharp}), G,
A{\mflat} (G{\msharp}), A, B{\mflat} (A{\msharp}), B. The distance between
two consecutive pitches in this list is called a
\emph{half-step}. Each of these pitches can be played in many
\emph{octaves}. For example, a piano keyboard has many Cs. The one in
the middle of the keyboard is called ``middle C'', but the key 12
half-steps (that is, twelve piano keys counting white and black keys)
above and below it are both Cs as well, in different octaves. Thus, a
pitch can be thought of as a name and an octave.

A basic unit of time in music is called the \emph{measure}, and notes
are named based on what fraction of a measure they last. The most common
notes are \emph{whole notes}, \emph{half notes}, \emph{quarter notes}
and \emph{eighth notes.} Rests can be similarly named for their
duration, and so there are \emph{whole rests}, \emph{half rests} and so
on.

The top of \texttt{music.ml} provides simple definitions for musical
data types.  The type \texttt{pitch} is a tuple of a \texttt{p}, which
has one constructor for each of the 12 pitch names, and an integer
representing the octave. Note that our definition includes only flats
and not sharps.  We apologize if this offends any music
theorists. There is also a data type representing musical objects,
\texttt{obj}. An \texttt{obj} can either be a \texttt{Note}, which
contains a pitch, the duration as a float and the volume as an integer
from 0 to 128, or a \texttt{Rest} which contains the duration as a
float. Durations are in units of measures, so 1.0 is a whole note or
rest, 0.5 is a half note or rest, and so on. You may also indicate
other float values, like 0.75 (called a \emph{dotted half} note or
rest) or 1.5, 2.0, 3.0 , etc., for multi-measure notes and rests.

This data type is great for representing the kind of music notation
you'd see in sheet music, but what does this have to do with streams?
For the purpose of representing or playing music on a computer (for
example, in a MIDI sequencer), a different representation is more
convenient. In this representation, a piece of music is a stream of
events, where an event is the start of a certain tone or the end of a
certain tone. Both kinds of events carry two pieces of information:
how much time should elapse between the previous event and this one,
and the pitch of the event. Note that stops, in addition to starts,
must have pitch. This is so that we can have multiple notes played at
a time. The sequencer has to know which of the notes currently playing
we want to stop. Starts also have a volume (it so happens that in the
MIDI specification a stop can be represented like a start with a
volume of 0.) A data type for musical events is also given at the top
of \texttt{music.ml}.  An \texttt{event} can be a \texttt{Tone} of a
time, a pitch and a volume, or a stop of a time and a
pitch. \emph{Remember that the times stored with events are relative
  to the previous event and not absolute.} A natural way to represent
music is as a stream of events. The sequencer doesn't care about the
rest of the piece, it just takes events as they come and processes
them. This also allows us to lazily perform actions on an entire piece
of music. The downside of this is that, because our streams are always
infinite, songs represented in this way can't end. With the exception
of certain songs commonly performed in the back seats of cars on
family trips, pieces of music are of finite length, but we'll get back
to this later. For convenience, the stream code from part 1 has been
reproduced here. You may use any of the stream helper functions from
this assignment or from lecture if they're useful.

\begin{problem}
  Write a function \texttt{list\_to\_stream} that builds a music
  stream (\texttt{event\ stream}) from a finite list of musical
  objects. The stream should repeat this music forever.

  \textbf{Hint:} Use a recursive helper function as defined, which
  will change the list but keep the original list around as
  \texttt{lst}. Both need to be recursive, since you will call both
  the inner and outer functions at some point. If you'd rather solve
  the problem a different way, you may remove the definition of the
  recursive helper as long as you keep the type signature of
  \texttt{list\_to\_stream} the same.
\end{problem}

\begin{problem}
  Write a function pair that merges two event streams into one. Events
  that happen earlier in time should appear earlier in the merged
  stream.  Events in the merged stream should happen at the same
  absolute time they happened in the individual streams. For example,
  if one stream has a start at time 0, and a stop at time 0.5 after
  that, and the other stream has a start at time 0.25 and a stop at
  time 0.5 after that, the combined stream should have a start at time
  0, the second start at 0.25 after that, the first stop at 0.25 after
  that, and the second stop at 0.25 after that. This will be fairly
  similar to the merge function you wrote in part 2, though will
  require some careful thought to update the time differences of the
  event that is not chosen.
\end{problem}

\begin{problem}
  Write a function \texttt{transpose} that takes an event stream and
  transposes it (moves each pitch up by \texttt{half\_steps}.) For
  example, a C transposed up 7 half-steps is a G. A C transposed up 12
  half-steps is the C in the next octave up. Use the helper function
  \texttt{transpose\_pitch}, which transposes a single pitch value by
  the correct number of half-steps.
\end{problem}

What fun would an assignment about music be if there was no way to
hear music? We've given you a function \texttt{output\_midi} that
takes a string \texttt{filename}, integer \texttt{n}, and an event
stream \texttt{str} and outputs the first \texttt{n} events of
\texttt{str} as a MIDI file with the given filename. Note that the
integer argument is necessary because streams are infinite and we
can't have infinite MIDI files. To hear some music and test the
functions you've written so far, uncomment the lines of code under the
comment ``Start off with some scales.'' When you compile and run the
file at the command line or execute the file's definitions in the
OCaml toplevel, this will output a file \texttt{scale.mid}, which
plays a C major scale together with a G major scale. 

Please watch this informative
\href{http://www.youtube.com/watch?v=JdxkVQy7QLM}{YouTube video} that
relates to the next problem.

As any fourth-grader in school band can tell you, scales are boring.
This example also doesn't show us why we should care about the power
music streams give us. For a more realistic example, we need a piece
of music that is repetitive, and has several parts played together
which resemble each other with small modifications. To be a good
example, it should also be instantly recognizable and in the public
domain so we don't get sued. Such a piece is Johann Pachelbel's
\emph{Canon in D major}. (We guarantee you've heard it.)

We'll be building a music stream of a simplified version of eight
measures of Pachelbel's canon. This segment contains a \emph{basso
  continuo}, defined as the event stream \texttt{bass.} This is a
two-measure pattern that repeats over and over (ah, so this is where
infinite streams are useful.) It also contains a melody, of which six
measures are contained in the stream \texttt{melody.} The bass plays
for two measures, and then the melody starts and \emph{plays over
  it}. Two measures later, the melody \emph{starts again}, playing
over the bass and the first melody. Finally, two measures after this
(six measures from the start), the melody starts once more. For the
final two measures, there are four streams playing at once: the bass
and three copies of the melody, at different points.


\begin{problem} 
  Define a stream \texttt{canon} that represents this piece. Use the
  functions \texttt{shift\_start} and \texttt{pair}, and the streams
  \texttt{bass} and \texttt{melody}. When you're done, uncomment the
  line below the definition and compile and run the code. It will
  produce a file canon.mid which, if you've done the problem
  correctly, will contain the first eight measures of the piece (we
  export 208 events since these eight measures should contain 104
  notes; you could increase this number if you want to hear more
  music, though it won't be true to the original). If it sounds wrong,
  it probably is. If it sounds right, either it is or you're a
  brilliant composer. (Note: As this is not a music class, brilliant
  composers will not earn extra points on this question.)
\end{problem}

\begin{problem}
  \textbf{Challenge problem:} There's lots of opportunity for
  extending your system. If you want, try implementing other functions
  for manipulating event streams. Maybe one that increases or
  decreases the timescale, or one that doubles every note, or
  increases or decreases the volume. See what kind of music you can
  create. We've given you some more interesting streams of music to
  play around with if you'd like, named \texttt{part1} through
  \texttt{part4}. They're all the same length, 2.25 measures.
\end{problem}

\section{Submit!}

To submit the problem set, follow the instructions found
\href{http://tiny.cc/cs51reference}{here}.  Please note that only one
of your partners needs to submit the problem set. That's it for
Problem Set 6!

\end{document}
