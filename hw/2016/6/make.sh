#!/bin/sh

caml-tex -o pset6.tex pset6.mlt
pdflatex -shell-escape pset6
bibtex pset6
makeindex pset6
pdflatex -shell-escape pset6
pdflatex -shell-escape pset6
