(* Consider this mutable list type. *)
type 'a mlist = Nil | Cons of 'a * (('a mlist) ref)

(*>* Problem 1.1 *>*)
(* Write a function has_cycle that returns whether a mutable list has a cycle.
 * You may want a recursive helper function. Don't worry about space usage. *)

let next node =
    let Cons (el, refr) = node in !refr
;;

let rec is_backedge node mlist =
    match mlist with
    | Nil -> false
    | a -> if (a == (next node)) then true 
                                    else if (a == node) then false
                                    else is_backedge node (next a)
;;

let rec helper (perm_lst : 'a mlist) temp_lst : bool =
    match temp_lst with
    | Nil -> false
    | a -> if (is_backedge a perm_lst) then true else helper perm_lst (next a)
;;

let has_cycle (lst : 'a mlist) : bool =
    helper lst lst
;;
    

(* Some mutable lists for testing. *)
let list1a = Cons(2, ref Nil)
let list1b = Cons(2, ref list1a)
let list1 = Cons(1, ref list1b)
;;

assert(has_cycle list1a = false);;
assert(has_cycle list1b = false);;
assert(has_cycle list1 = false);;

let reflist = ref (Cons(2, ref Nil))
let list2 = Cons(1, ref (Cons (2, reflist)))
let _ = reflist := list2
;;

assert(has_cycle list2 = true);;

(*>* Problem 1.2 *>*)
(* Write a function flatten that flattens a list (removes its cycles if it
 * has any) destructively. Again, you may want a recursive helper function and
 * you shouldn't worry about space. *)

let rec helper (perm_lst : 'a mlist) temp_lst : unit =
  match temp_lst with
  | Nil -> ()
  | a -> if (is_backedge a perm_lst) then let Cons (_, refr) = a in let _ = refr := Nil in () 
                                                        
                                                        else helper (perm_lst) (next temp_lst)
;;

let flatten lst =
    helper lst lst
;;

flatten list2 ;;
flatten list1 ;;

assert (list2 = Cons (1, ref (Cons (2, ref Nil)))) ;;
assert (list1 = Cons (1, ref (Cons (2, ref (Cons (2, ref Nil))))));;

(*>* Problem 1.3 *>*)
(* Use write mlength, which finds the number of nodes in a mutable list. *)
let rec count perm_lst temp_lst =
    match temp_lst with
    | Nil -> 0
    | a -> if (is_backedge a perm_lst) then 1 else 1 + (count perm_lst (next a))
;;

let mlength (lst : 'a mlist) : int =
    count lst lst
;;

let reflist = ref (Cons(2, ref Nil))
let list2 = Cons(1, ref (Cons (2, reflist)))
let _ = reflist := list2
;;

assert (mlength list2 = 2);;
assert (mlength list1 = 3);;

(*>* Problem 1.4 *>*)
(* Please give us an honest estimate of how long this part took 
 * you to complete.  We care about your responses and will use
 * them to help guide us in creating future assignments. *)
let minutes_spent : int = 120
