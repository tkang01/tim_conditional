(* Consider this mutable list type. *)
type 'a mlist = Nil | Cons of 'a * (('a mlist) ref)

(*>* Problem 1.1 *>*)
(* Write a function has_cycle that returns whether a mutable list has a cycle.
 * You may want a recursive helper function. Don't worry about space usage. *)
(*to determine whether a node has been traversed before*)
let rec altraversed (lst: 'a mlist) (lst2: 'a mlist list): bool =
match lst with
| Nil-> false
| Cons(h, t)->
   match lst2 with
   |[]-> false
   |hd::tl -> 
     let q = ((fun x y->x-y)=0)
     List.filter q h hd true else altraversed !t tl 

let has_cycle (lst : 'a mlist) : bool =
match lst with
|Nil -> false
|Cons (h,t) -> if altraversed lst= true then true
               else false


(* Some mutable lists for testing. *)
let list1a = Cons(2, ref Nil)
let list1b = Cons(2, ref list1a)
let list1 = Cons(1, ref list1b)

let reflist = ref (Cons(2, ref Nil))
let list2 = Cons(1, ref (Cons (2, reflist)))
let _ = reflist := list2

(*>* Problem 1.2 *>*)
(* Write a function flatten that flattens a list (removes its cycles if it
 * has any) destructively. Again, you may want a recursive helper function and
 * you shouldn't worry about space. *)

let rec decycle (lst: 'a mlist): 'a mlist =
if has_cycle lst = false then lst else
match lst with
 |Nil -> Nil
 |Cons (h, t) ->
   match !t with
   |Nil -> lst
   |Cons (h1,t1)-> 
     if ((!h1)==h) then  Cons (h, ref Cons( h1, Nil)
      else Cons( h, ref Cons(h1 ,(decycle (!t1))))

let flatten (lst : 'a mlist) : unit =
  let _ = lst := decycle lst


(*>* Problem 1.3 *>*)
(* Use write mlength, which finds the number of nodes in a mutable list. *)
let rec mlength (lst : 'a mlist) : int =
match lst with
| Nil -> 0
| Cons (h, t) ->
  match !t with
  | Nil -> 1
  | Cons (h1, t1) -> 
   if ((!t1)== Nil) then 2 else (2+mlength(!t1))
;;

(*>* Problem 1.4 *>*)
(* Please give us an honest estimate of how long this part took 
 * you to complete.  We care about your responses and will use
 * them to help guide us in creating future assignments. *)

let minutes_spent : int = -1
