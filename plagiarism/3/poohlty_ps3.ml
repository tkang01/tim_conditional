exception ImplementMe;;

(**************************** Part 1: Bignums *********************************)
type bignum = {neg: bool; coeffs: int list};;
let base = 1000;;

(* Please make sure you fully understand the representation invariant for bignums, 
   as documented in the problem set specification. *)

(*>* Problem 1.1 *>*)
let negate (b: bignum) : bignum =
<<<<<<< HEAD
  if b.coeffs != [] then {neg = not b.neg; coeffs = b.coeffs} else b
;;

assert (negate {neg = false; coeffs = []} = {neg = false; coeffs = []});;
assert (negate {neg = false; coeffs = [1]} = {neg = true; coeffs = [1]});;
assert (negate {neg = false; coeffs = [3;5]} = {neg = true; coeffs = [3;5]});;

(*>* Problem 1.2 *>*)

let fromInt (n: int) : bignum =
  let rec formCoeffs x =
    if x > 0 then (x mod base)::formCoeffs((x - x mod base) / base) else [] in
  {neg = (n < 0); coeffs = List.rev (formCoeffs(abs(n)))}
;;

assert (fromInt 0 = {neg = false; coeffs = []});;
assert (fromInt 10 = {neg = false; coeffs = [10]});;
assert (fromInt 5000 = {neg = false; coeffs = [5;0]});;
assert (fromInt (-10) = {neg = true; coeffs = [10]});;


let toInt (b: bignum) : int option =
  let rec sumInt nums: int =
  match nums with
  | []-> 0
  | hd::tl ->
    match tl with
    | [] -> hd
    | _ -> if (hd + base * (sumInt tl)) <= max_int then (hd + base * (sumInt tl)) else -1
  in if sumInt b.coeffs < 0 then None else
    if b.neg then Some (- sumInt (List.rev b.coeffs)) else
    Some (sumInt (List.rev b.coeffs))
;;

assert ((toInt {neg = false; coeffs = [123]}) = Some 123);;
assert ((toInt {neg = true; coeffs = [9;876;543]}) = Some (-9876543));; 
assert ((toInt {neg = false; coeffs = []}) = Some (0));;
assert ((toInt {neg = false; coeffs =[999;1;645;172;634;571;235;476;125]}) = None);; 
=======
  raise ImplementMe
;;

(*>* Problem 1.2 *>*)

let fromInt (n: int) : bignum =
  raise ImplementMe
;;

let toInt (b: bignum) : int option =
  raise ImplementMe
;;

>>>>>>> 88fa670f235344f0951be37d90fc024b7ee16ef6

(** Some helpful string functions **)
(* Splits a string into a list of its characters. *)
let rec explode (s: string) : char list =
  let len = String.length s in
  if len = 0 then []
  else (s.[0])::(explode (String.sub s 1 (len - 1)))

(* Condenses a list of characters into a string. *)
let rec implode (cs: char list) : string =
  match cs with
    | [] -> ""
    | c::t -> (String.make 1 c)^(implode t)

(** Other functions you may find useful. *)
(* Returns the first n elements of list l (or the whole list if too short) *)
let rec take_first (l: 'a list) (n: int): 'a list =
  match l with
    | [] -> []
    | h::t -> if n <= 0 then [] else h::(take_first t (n - 1))

(* Returns a pair
 * (first n elements of lst, rest of elements of lst) *)
let rec split lst n =
  if n = 0 then ([], lst)
  else match lst with
    | [] -> ([], [])
    | h::t -> let (lst1, lst2) = split t (n-1) in
        (h::lst1, lst2)

(* Removes zero coefficients from the beginning of the bignum representation *)
let rec stripzeroes (b: int list) : int list =
  match b with
    | 0::t -> stripzeroes t
    | _ -> b

(* Returns the floor of the base 10 log of an integer *)
let intlog (base: int): int =
  int_of_float (log10 (float_of_int base))
;;

(* fromString and toString assume the base is a power of 10 *) 
(* Converts a string representing an integer to a bignum. *)
let fromString (s: string): bignum =
  let rec fromString_rec (cs: char list) : int list =
    if cs = [] then [] else
    let (chars_to_convert, rest) = split cs (intlog base) in
    let string_to_convert = implode (List.rev chars_to_convert) in
      (int_of_string string_to_convert)::(fromString_rec rest)
  in
  match explode s with
    | [] -> fromInt 0
    | h::t -> if (h = '-')||(h = '~') then 
        {neg = true; coeffs = (List.rev (fromString_rec (List.rev t)))}
<<<<<<< HEAD
      else {neg = false; coeffs = stripzeroes (List.rev (fromString_rec (List.rev (h::t))))}
=======
      else {neg = false; coeffs = (List.rev (fromString_rec (List.rev (h::t))))}
>>>>>>> 88fa670f235344f0951be37d90fc024b7ee16ef6
;;

(* Converts a bignum to its string representation.
 * Returns a string beginning with ~ for negative integers. *)
let toString (b: bignum): string =
  let rec pad_with_zeroes_left (s: string) (len: int) =
    if (String.length s) >= len then s else
      "0"^(pad_with_zeroes_left s (len - 1))
  in
  let rec stripstrzeroes (s: string) (c: char) =
    if String.length s = 0 then "0" else
    if (String.get s 0) = '0' then 
      stripstrzeroes (String.sub s 1 ((String.length s) - 1)) c
    else s
  in
  let rec coeffs_to_string (coeffs: int list): string =
    match coeffs with
      | [] -> ""
      | h::t -> (pad_with_zeroes_left (string_of_int h) (intlog base))^
          (coeffs_to_string t)
  in
  let stripped = stripzeroes b.coeffs in
    if List.length stripped = 0 then "0" else
      let from_coeffs = stripstrzeroes (coeffs_to_string stripped) '0' in
        if b.neg then "~"^from_coeffs else from_coeffs
;;

(*>* Problem 1.3 *>*)
let equal (b1: bignum) (b2: bignum) : bool =
<<<<<<< HEAD
  (0 = String.compare (toString b1) (toString b2))
;;

assert (equal (fromInt 1) (fromInt (-1))= false);;
assert (equal (fromInt 101) (fromInt 101) = true);;
assert (equal (fromInt 1005) (fromInt (-17)) = false);;
assert (equal (fromInt 0) (fromInt 0) = true);;
assert (equal (fromInt (-17)) (fromInt (-17)) = true);;


let less (b1: bignum) (b2: bignum) : bool =
  if (b1.neg != b2.neg) then b1.neg
  else 
    let (b1s,b2s) = (toString b1, toString b2) in
    if b1.neg then 
      (if String.length b1s < String.length b2s then false
      else if String.length b1s > String.length b2s then true
      else if -1 = String.compare b1s b2s then false
      else if 1 = String.compare b1s b2s then true
      else false)
    else
      (if String.length b1s < String.length b2s then true
      else if String.length b1s > String.length b2s then false
      else if -1 = String.compare b1s b2s then true
      else if 1 = String.compare b1s b2s then false
      else false)
;;

assert (less (fromInt 1000) (fromInt (50000)) = true);;
assert (less (fromInt 1) (fromInt 1) = false);;
assert (less (fromInt 1005) (fromInt (-17)) = false);;
assert (less (fromInt (-90)) (fromInt (-70)) = true);;
assert (less (fromInt 0) (fromInt 10) = true);;
 
let greater (b1: bignum) (b2: bignum) : bool =
  not ((less b1 b2) || (equal b1 b2))
;;

assert (greater (fromInt 5000) (fromInt (-4)) = true);;
assert (greater (fromInt 500) (fromInt 500) = false);;
assert (greater (fromInt 7000) (fromInt 6000) = true);;
assert (greater (fromInt 17) (fromInt (-17)) = true);;


=======
  raise ImplementMe
;;

let less (b1: bignum) (b2: bignum) : bool =
  raise ImplementMe
;;

let greater (b1: bignum) (b2: bignum) : bool =
  raise ImplementMe
;;

>>>>>>> 88fa670f235344f0951be37d90fc024b7ee16ef6
(** Some arithmetic functions **)

(* Returns a bignum representing b1 + b2.
 * Assumes that b1 + b2 > 0. *)
let plus_pos (b1: bignum) (b2: bignum) : bignum =
  let pair_from_carry (carry: int) =
    if carry = 0 then (false, [])
    else if carry = 1 then (false, [1])
    else (true, [1])
  in
  let rec plus_with_carry (neg1, coeffs1) (neg2, coeffs2) (carry: int) 
      : (bool * int list) =
    match (coeffs1, coeffs2) with
      | ([], []) -> pair_from_carry carry
      | ([], _) -> if carry = 0 then (neg2, coeffs2) else
          plus_with_carry (neg2, coeffs2) (pair_from_carry carry) 0
      | (_, []) -> if carry = 0 then (neg1, coeffs1) else
          plus_with_carry (neg1, coeffs1) (pair_from_carry carry) 0
      | (h1::t1, h2::t2) -> 
          let (sign1, sign2) = 
            (if neg1 then -1 else 1), (if neg2 then -1 else 1) in
          let result = h1*sign1 + h2*sign2 + carry in
          if result < 0 then 
            let (negres, coeffsres) = plus_with_carry (neg1, t1) (neg2, t2) (-1)
            in (negres, (result+base)::coeffsres)
          else if result >= base then
            let (negres, coeffsres) = plus_with_carry (neg1, t1) (neg2, t2) 1
            in (negres, (result-base)::coeffsres)
          else 
            let (negres, coeffsres) = plus_with_carry (neg1, t1) (neg2, t2) 0
            in (negres, result::coeffsres)
  in
  let (negres, coeffsres) = 
<<<<<<< HEAD
        plus_with_carry (b1.neg, List.rev (stripzeroes b1.coeffs))
          (b2.neg, List.rev (stripzeroes b2.coeffs)) 0
  in {neg = negres; coeffs = List.rev coeffsres}
;;


=======
        plus_with_carry (b1.neg, List.rev b1.coeffs)
          (b2.neg, List.rev b2.coeffs) 0
  in {neg = negres; coeffs = stripzeroes (List.rev coeffsres)}
;;

>>>>>>> 88fa670f235344f0951be37d90fc024b7ee16ef6
(*>* Problem 1.4 *>*)
(* Returns a bignum representing b1 + b2.
 * Does not make the above assumption. *)
let plus (b1: bignum) (b2: bignum) : bignum =
<<<<<<< HEAD
  let x = plus_pos b1 b2 in
  if x.neg then negate (plus_pos (negate b1) (negate b2))
  else x
;;
assert (plus (fromInt 1000) (fromInt 2000) = fromInt 3000);;
assert (plus (fromInt (-1000)) (fromInt (-2000)) = fromInt (-3000));;
assert (plus (fromInt (-2000)) (fromInt (1000)) = fromInt (-1000));;
assert (plus (fromInt (-10000)) (fromInt 20000) = fromInt 10000);;
assert (plus (fromInt 0) (fromInt (-1000)) = fromInt (-1000));;

(*>* Problem 1.5 *>*)
(* Returns a bignum representing b1*b2 *)

 (* times implementation: this times is implemented using the traditional algorithm. 
 The result is calculated by adding the result of each int of the second number times 
 the first number. It also need four helper functions that operates on int lists *)

(* a plus helper function that accept int lists *)
let simple_plus (a:int list) (b:int list) : int list =
  let x = plus {neg = false; coeffs = a} {neg = false; coeffs = b} in
  x.coeffs
;;

(* times an int with an int list and return the result list *)
let rec timesInt_with_carry (b: int list) (n: int) (c:int) =
  match b with
  | [] -> if c = 0 then [] else [c]
  | hd::tl -> 
    let nonCar = (hd * n + c) mod base in
    let carry = (hd * n + c - nonCar) / base in
    nonCar:: timesInt_with_carry tl n carry
;;


(* take in int list as arguement and performing reverse on input and output *)
let simple_timesInt (a: int list) (n: int) =
  List.rev (timesInt_with_carry (List.rev a) n 0) 
;;

(* times two bignums using the previous implementation of simple_timesInt *)
let rec simple_times (a:int list) (b:int list): int list=
  match b with
   | [] -> []
   | hd::tl -> 
    match tl with
      | [] -> simple_timesInt a hd
      | _ -> simple_plus (simple_timesInt a hd) (simple_timesInt (simple_times a tl) base)
;;

(* the actual times function, also handling result negative sign and times zero *)
let times (b1: bignum) (b2: bignum) : bignum =
  if (b2.coeffs = []) || (b1.coeffs = []) then {neg = false; coeffs = []} else
  {neg = (b1.neg != b2.neg); coeffs =  (simple_times b1.coeffs (List.rev b2.coeffs))}
;;

assert (times (fromInt 3) (fromInt 0) = fromInt 0);;
assert (times (fromInt 23) (fromInt 23) = fromInt 529);;
assert (times (fromInt 1) (fromInt (3)) = fromInt 3);;
assert (times (fromInt 2003) (fromInt (-2003)) = fromInt (-4012009));;
assert (times (fromInt (-349)) (fromInt (-6)) = fromInt 2094);;

=======
  raise ImplementMe
;;

(*>* Problem 1.5 *>*)
(* Returns a bignum representing b1*b2 *)
let times (b1: bignum) (b2: bignum) : bignum =
  raise ImplementMe
;;

>>>>>>> 88fa670f235344f0951be37d90fc024b7ee16ef6
let clean (b : bignum) : bignum = 
  {neg = b.neg; coeffs = stripzeroes b.coeffs}
;;


(* Return the number of bytes required to represent an RSA modulus. *)
let bytesInKey (n: bignum) =
  int_of_float ((float_of_int ((List.length (stripzeroes n.coeffs)) - 1)) 
                *. (log10 (float_of_int base)) /. ((log10 2.) *. 8.))
;;

(* Returns a bignum representing b/n, where n is an integer less than base *)
let divsing (b: int list) (n: int) : int list * int =
  let rec divsing_rec (b: int list) (r: int) : int list * int =
    match b with
      | [] -> [], r
      | h::t -> 
          let dividend = r*base + h in
          let quot = dividend / n in
          let (q, r) = divsing_rec t (dividend-quot*n) in
            (quot::q, r)
  in
    match b with
      | [] -> [], 0
      | [a] -> [a / n], a mod n
      | h1::h2::t -> if h1 < n then divsing_rec ((h1*base + h2)::t) 0
        else divsing_rec b 0
;;

(* Returns a pair (floor of b1/b2, b1 mod b2), both bignums *)
let rec divmod (b1: bignum) (b2: bignum): bignum * bignum =
  let clean_b1, clean_b2 = clean b1, clean b2 in
  let rec divmod_rec m n (psum: bignum) : bignum * bignum =
    if less m n then (psum, m) else
      let mc = m.coeffs in
      let nc = n.coeffs in
      match nc with
        | [] -> failwith "Division by zero"
        | ns::_ -> let (p, _) =

            if ns + 1 = base then 
              take_first mc ((List.length mc) - (List.length nc)), 0
            else 
              let den = ns + 1 in
              let num = take_first mc ((List.length mc) - (List.length nc) + 1)
              in divsing num den 
          in
          let bp = clean {neg = false; coeffs = p} in
          let p2 = if equal bp (fromInt 0) then (fromInt 1) else bp in
            divmod_rec (plus m (negate (times n p2))) n (plus psum p2)
  in
    divmod_rec clean_b1 clean_b2 (fromInt 0)
;;

<<<<<<< HEAD
=======

>>>>>>> 88fa670f235344f0951be37d90fc024b7ee16ef6
(**************************** Part 2: RSA *********************************)

(** Support code for RSA **)
(* Returns a random bignum from 0 to bound - 1 (inclusive). *)
let randbignum (bound: bignum) =
  let rec randbignum_rec (bound: int list) =
    match bound with
      | [] -> []
      | [h] -> if h = 0 then [] else [Random.int h]
      | _::t -> (Random.int base)::(randbignum_rec t)
  in {neg = false; coeffs = stripzeroes (List.rev (randbignum_rec (List.rev bound.coeffs)))}
;;

(* Returns b to the power of e mod m *)
let rec expmod (b: bignum) (e: bignum) (m: bignum): bignum =
  let clean_b, clean_e, clean_m = clean b, clean e, clean m in
  if equal clean_e (fromInt 0) then (fromInt 1) else if equal clean_e (fromInt 1) then 
    let (_, x) = divmod clean_b clean_m in x
  else 
    let (q, r) = divmod clean_e (fromInt 2) in
    let res = expmod clean_b q clean_m in
    let (_, x) = divmod (times (times res res) (expmod clean_b r clean_m)) clean_m in 
      {neg = x.neg; coeffs = stripzeroes x.coeffs}

(* Returns b to the power of e *)
let rec exponent (b: bignum) (e: bignum): bignum =
  let clean_e, clean_b = clean e, clean b in
  if equal clean_e (fromInt 0) then (fromInt 1) else if equal clean_e (fromInt 1) 
  then clean_b
  else 
    let (q, r) = divmod clean_e (fromInt 2) in
    let res = exponent clean_b q in
    let exp = (times (times res res) (exponent clean_b r))
    in {neg = exp.neg; coeffs = stripzeroes exp.coeffs}

(* Returns true if n is prime, false otherwise. *)
let isPrime (n: bignum): bool =
  let clean_n = clean n in
  let rec miller_rabin (k: int) (d: bignum) (s: int): bool =
    if k < 0 then true else
    let rec square (r: int) (x: bignum) =
      if r >= s then false else
      let x = expmod x (fromInt 2) clean_n in
        
        if equal x (fromInt 1) then false
        else if equal x (plus clean_n (fromInt (-1))) then miller_rabin (k-1) d s
        else square (r + 1) x
    in
    let a = plus (randbignum (plus n (fromInt (-4)))) (fromInt 2) in
    let x = expmod a d n in
      if (equal x (fromInt 1)) || (equal x (plus clean_n (fromInt (-1)))) then 
        miller_rabin (k - 1) d s
      else square 1 x
  in 
    (* Factor powers of 2 to return (d, s) such that n=(2^s)*d *)
  let rec factor (n: bignum) (s: int) =
    let (q, r) = divmod n (fromInt 2) in
      if equal r (fromInt 0) then factor q (s + 1) else (n, s)
  in
  let (_, r) = divmod n (fromInt 2) in
    if equal r (fromInt 0) then false else
      let (d, s) = factor (plus clean_n (fromInt (-1))) 0 in
        miller_rabin 20 d s

(* Returns (s, t, g) such that g is gcd(m, d) and s*m + t*d = g *)
let rec euclid (m: bignum) (d: bignum) : bignum * bignum * bignum =
  let clean_m, clean_d = clean m, clean d in
  if equal clean_d (fromInt 0) then (fromInt 1, fromInt 0, m)
  else
    let (q, r) = divmod clean_m clean_d in
    let (s, t, g) = euclid clean_d r in 
      (t, plus s (negate (times q t)), g)
;;

(* Generate a random prime number between min and max-1 (inclusive) *)
let rec generateRandomPrime (min: bignum) (max: bignum) : bignum =
  let rand = plus (randbignum (plus max (negate min))) min in
    if isPrime rand then rand else generateRandomPrime min max
;;
<<<<<<< HEAD
 
=======

>>>>>>> 88fa670f235344f0951be37d90fc024b7ee16ef6
(** Code for encrypting and decrypting messages using RSA **)

(* Generate a random RSA key pair, returned as (e, d, n). 
 * p and q will be between 2^n and 2^(n+1).
 * Recall that (n, e) is the public key, and (n, d) is the private key. *)
let rec generateKeyPair (r: bignum) : bignum * bignum * bignum =
  let c1 = fromInt 1 in
  let c2 = fromInt 2 in
  let p = generateRandomPrime (exponent c2 r) (exponent c2 (plus r c1)) in
  let q = generateRandomPrime (exponent c2 r) (exponent c2 (plus r c1)) in
  let m = times (plus p (negate c1)) (plus q (negate c1)) in
  let rec selectPair () =
    let e = generateRandomPrime (exponent c2 r) (exponent c2 (plus r c1)) in
    let (_, d, g) = euclid m e in
    let d = if d.neg then plus d m else d in
      if equal g c1 then (e, d, times p q) else selectPair ()
  in
    if equal p q then generateKeyPair r else selectPair ()
;;

<<<<<<< HEAD

(*>* Problem 2.1 *>*)
(* To encrypt, pass in n e s. To decrypt, pass in n d s. *)
let encryptDecryptBignum (n: bignum) (e: bignum) (s: bignum) : bignum =
  expmod s e n
;;

assert (encryptDecryptBignum (fromInt 37909) (fromInt 15751) (encryptDecryptBignum (fromInt 37909) (fromInt 131) (fromInt 42)) = fromInt 42);;
assert (encryptDecryptBignum (fromInt 6887) (fromInt 5111) (encryptDecryptBignum (fromInt 6887) (fromInt 71) (fromInt 37)) = fromInt 37);;
assert (encryptDecryptBignum (fromInt 177773) (fromInt 85001) (encryptDecryptBignum (fromInt 177773) (fromInt 281) (fromInt 0)) = fromInt 0);;

=======
(*>* Problem 2.1 *>*)
(* To encrypt, pass in n e s. To decrypt, pass in n d s. *)
let encryptDecryptBignum (n: bignum) (e: bignum) (s: bignum) : bignum =
  raise ImplementMe
;;

>>>>>>> 88fa670f235344f0951be37d90fc024b7ee16ef6
(* Pack a list of chars as a list of bignums, with m chars to a bignum. *)
let rec charsToBignums (lst: char list) (m: int) : bignum list =
  let rec encchars lst =
    match lst with
      | [] -> (fromInt 0)
      | c::t -> plus (times (encchars t) (fromInt 256)) (fromInt (Char.code c))
  in
    match lst with
      | [] -> []
      | _ -> let (enclist, rest) = split lst m in
          (encchars enclist)::(charsToBignums rest m)
;;

(* Unpack a list of bignums into chars (reverse of charsToBignums) *)
let rec bignumsToChars (lst: bignum list) : char list =
  let rec decbignum b =
    if equal b (fromInt 0) then []
    else let (q, r) = divmod b (fromInt 256) in
      match toInt r with
        | None -> failwith "bignumsToChars: representation invariant broken"
        | Some ir -> (Char.chr ir)::(decbignum q)
  in
    match lst with
      | [] -> []
      | b::t -> (decbignum b)@(bignumsToChars t)
;;

(* Encrypts or decrypts a list of bignums using RSA.
 * To encrypt, pass in n e lst.
 * To decrypt, pass in n d lst. *)
let rec encDecBignumList (n: bignum) (e: bignum) (lst: bignum list) =
  match lst with
    | [] -> []
    | h::t -> (encryptDecryptBignum n e h)::(encDecBignumList n e t)
;;


(*>* Problem 2.2 *>*)
let encrypt (n: bignum) (e: bignum) (s: string) =
<<<<<<< HEAD
  encDecBignumList n e (charsToBignums (explode s) 1)
;;


(* Decrypt an encrypted message (list of bignums) to produce the 
 * original string. *)
let decrypt (n: bignum) (d: bignum) (m: bignum list) =
  implode (bignumsToChars (encDecBignumList n d m))
;;

assert (decrypt (fromInt 37909) (fromInt 15751) (encrypt (fromInt 37909) (fromInt 131) "TIANYU") = "TIANYU");;
assert (decrypt (fromInt 6887) (fromInt 5111) (encrypt (fromInt 6887) (fromInt 71) "CS51") = "CS51");;
assert (decrypt (fromInt 177773) (fromInt 85001) (encrypt (fromInt 177773) (fromInt 281) "Awesome!") = "Awesome!");;

(* Returns a bignum representing b1*b2 *)
let splitBignum (a:bignum) (n:int): (bignum * bignum) =
  let (a1,a2) = split a.coeffs n in
  ({neg = a.neg;coeffs = a1},{neg = a.neg;coeffs = a2})
;;

let expBignum (a:bignum) (b:int): bignum =
  let rec appendZero (lst:int list) (n:int): int list = 
    if n > 0 then appendZero (lst @ [0]) (n-1) else lst
  in {neg=a.neg;coeffs = (appendZero a.coeffs b)}
;;

let rec times_faster (a1: bignum) (a2: bignum) : bignum =
  let len1, len2 = List.length a1.coeffs, List.length a2.coeffs in
  if len1 = 1 || len2 = 1 then times a1 a2 else
    let (x1, x2) = splitBignum a1 (len1/2) in
    let (y1, y2) = splitBignum a2 (len2/2) in
    let deg = (len1/2 + len1 mod 2) in
      let z0 = times_faster x1 y1 in
      let z1 = times_faster x2 y2 in
      let z2 = plus (times_faster (plus x1 x2) (plus y1 y2)) (negate(plus z0 z1)) in
      plus (expBignum z0 (2*deg)) (plus z1 (expBignum z2 deg))
;;

assert (times_faster (fromInt 3) (fromInt 0) = fromInt 0);;
assert (times_faster (fromInt 23) (fromInt 23) = fromInt 529);;
assert (times_faster (fromInt 1) (fromInt (3)) = fromInt 3);;
assert (times_faster (fromInt 2003) (fromInt (-2003)) = fromInt (-4012009));;
assert (times_faster (fromInt (-349)) (fromInt (-6)) = fromInt 2094);;

let minutes_spent = 400;;
=======
  raise ImplementMe
;;

(* Decrypt an encrypted message (list of bignums) to produce the 
 * original string. *)
let decrypt (n: bignum) (d: bignum) (m: bignum list) =
  raise ImplementMe
;;

(**************************** Part 3: Challenge *********************************)

(* Returns a bignum representing b1*b2 *)
let times_faster (b1: bignum) (b2: bignum) : bignum =
  raise ImplementMe
;;

let minutes_spent = 0;;
>>>>>>> 88fa670f235344f0951be37d90fc024b7ee16ef6
