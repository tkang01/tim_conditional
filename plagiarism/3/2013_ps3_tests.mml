(*** CS 51 Problem Set 1 ***)
(*** February 3, 2012 ***)
(*** Test Suite ***)


(*>* Problem 1.1 *>*)
(*>>* 3 *>>*)

>>>
let _ = (repOK: bignum -> bool) in true

>>>
not (repOK {neg=false; coeffs = [1001;0]})

>>>
repOK {neg=false; coeffs = [1;2;3]}

(*>* Problem 1.2 *>*)
(*>>* 6 *>>*)

let rec stripzeroes (b: int list) : int list =
  match b with
    | 0::t -> stripzeroes t
    | _ -> b

let equal_sol (b1: bignum) (b2: bignum) : bool =
  let rec listsequal (l1: int list) (l2: int list) =
    match (l1, l2) with
      | ([], []) -> true
      | (h1::t1, h2::t2) -> (h1 = h2) && (listsequal t1 t2)
      | ([], _) -> false
      | (_, []) -> false
  in
    (b1.neg = b2.neg) && 
      (listsequal (stripzeroes b1.coeffs) (stripzeroes b2.coeffs))
;;

>>>
equal_sol (fromInt 123456789) (fromString "123456789")

>>>
equal_sol (fromInt (-123456789)) (fromString "~123456789")

>>>
equal_sol (fromInt 10002) (fromString "10002")

>>>
toInt (fromString "123456789") = Some 123456789

>>>
toInt (fromString "~123456789") = Some (-123456789)

>>>
toInt (fromString "10002") = Some 10002

>>>
toInt (fromString "12345678912") = None

(*>* Problem 1.3 *>*)
(*>>* 11 *>>*)

let less_sol (b1: bignum) (b2: bignum) : bool =
  let rec less_rec l1 l2 =
    match (l1, l2) with
      | ([], []) -> false
      | (h1::t1, h2::t2) -> if h1 < h2 then true
        else if h1 > h2 then false else less_rec t1 t2
      | ([], _) -> true
      | (_, []) -> false
  in
  let less_abs l1 l2 =
    let (len1, len2) =
      (List.length (stripzeroes l1), List.length (stripzeroes l2)) in
      if len1 < len2 then true else if len1 > len2 then false
      else less_rec (stripzeroes b1.coeffs) (stripzeroes b2.coeffs)
  in
    match (b1.neg, b2.neg) with
      | (true, true) -> less_abs b2.coeffs b1.coeffs
      | (true, false) -> true
      | (false, true) -> false
      | (false, false) -> less_abs b1.coeffs b2.coeffs
;;


let fromInt (n: int) : bignum =
  let rec make_coeffs (n: int) (a: int list) =
    if n = 0 then a else
      make_coeffs (n / base) ((n mod base)::a)
  in
  let (neg, newn) = if n < 0 then (true, 0-n) else (false, n) in
    {neg = neg; coeffs = make_coeffs newn []}
;;

let toInt (b: bignum) : int option =
  let rec toInt_rec (coeffs : int list) (a: int) =
    match coeffs with
      | [] -> a*(if b.neg then -1 else 1)
      | h::t -> let newa = a*base + h in
          toInt_rec t newa
  in let max = fromInt max_int in
  let min = fromInt min_int in
    if (less_sol b max) && (less_sol min b) then Some (toInt_rec b.coeffs 0)
    else None
;;

>>>
equal (fromInt 12345) (fromString "12345")

>>>
not (equal (fromInt 1) (fromInt (-1)))

>>>
not (equal (fromString "1073741824") (fromString "~1073741824"))

>>>
less (fromInt 10) (fromInt 10010)

>>>
less (fromInt (-10005)) (fromInt 5001)

>>>
not (less (fromInt 5001) (fromInt (-10005)))

>>>
less (fromInt (-10010)) (fromInt (-10))

>>>
less (fromString "100200300") (fromString "1073741824")

>>>
less (fromString "~1073741825") (fromString "1073741824")

>>>
equal (negate (fromString "100200300")) (fromString "~100200300")

>>>
equal (negate (fromString "~100200300")) (fromString "100200300")

(*>* Problem 1.4 *>*)
(*>>* 3 *>>*)

let fromInt (n: int) : bignum =
  let rec make_coeffs (n: int) (a: int list) =
    if n = 0 then a else
      make_coeffs (n / base) ((n mod base)::a)
  in
  let (neg, newn) = if n < 0 then (true, 0-n) else (false, n) in
    {neg = neg; coeffs = make_coeffs newn []}
;;

let equal (b1: bignum) (b2: bignum) : bool =
  let rec listsequal (l1: int list) (l2: int list) =
    match (l1, l2) with
      | ([], []) -> true
      | (h1::t1, h2::t2) -> (h1 = h2) && (listsequal t1 t2)
      | ([], _) -> false
      | (_, []) -> false
  in
    (b1.neg = b2.neg) && 
      (listsequal (stripzeroes b1.coeffs) (stripzeroes b2.coeffs))
;;

(* Returns true if b1 is less than b2, false otherwise. *)
let less (b1: bignum) (b2: bignum) : bool =
  let rec less_rec l1 l2 =
    match (l1, l2) with
      | ([], []) -> false
      | (h1::t1, h2::t2) -> if h1 < h2 then true
        else if h1 > h2 then false else less_rec t1 t2
      | ([], _) -> true
      | (_, []) -> false
  in
  let less_abs l1 l2 = 
    let (len1, len2) = 
      (List.length (stripzeroes l1), List.length (stripzeroes l2)) in
      if len1 < len2 then true else if len1 > len2 then false
      else less_rec (stripzeroes b1.coeffs) (stripzeroes b2.coeffs)
  in
    match (b1.neg, b2.neg) with
      | (true, true) -> less_abs b2.coeffs b1.coeffs
      | (true, false) -> true
      | (false, true) -> false
      | (false, false) -> less_abs b1.coeffs b2.coeffs
;;

let negate (b: bignum) : bignum =
  {neg = not b.neg; coeffs = b.coeffs}

>>>
equal (plus (fromInt 1020) (fromInt 980)) (fromInt 2000)

>>>
equal (plus (fromInt 300) (fromInt (-1900))) (fromInt (-1600))

>>>
equal (plus (fromInt (-2400)) (fromInt (-6200))) (fromInt (-8600))

(*>* Problem 1.5 *>*)
(*>>* 9 *>>*)

let fromInt (n: int) : bignum =
  let rec make_coeffs (n: int) (a: int list) =
    if n = 0 then a else
      make_coeffs (n / base) ((n mod base)::a)
  in
  let (neg, newn) = if n < 0 then (true, 0-n) else (false, n) in
    {neg = neg; coeffs = make_coeffs newn []}
;;

let plus (b1: bignum) (b2: bignum) : bignum =
    (* Determine whether to negate the operands and answer.
     * Return true if different signs and pos. operand is greater in absolute
     * value than neg. operand, or if both are negative
*)
  let switch b1 b2 =
    (b1.neg && (not b2.neg) && (less b2 (negate b1))) ||
      ((not b1.neg) && b2.neg && (less b1 (negate b2))) ||
      (b1.neg && b2.neg)
  in
    if switch b1 b2 then negate (plus_pos (negate b1) (negate b2))
    else plus_pos b1 b2
;;

>>>
equal (times (fromInt 100200300) (fromInt 3)) (fromInt 300600900)

>>>
equal (times (fromInt 100200300) (fromInt 100200300)) (fromString "10040100120090000")

>>>
equal (times (fromInt (-123456789)) (fromInt 2)) (fromInt (-246913578))

>>>
equal (times (fromInt 2000) (fromInt (-123456789))) (fromString "~246913578000")

>>>
equal (times (fromInt (-293348)) (fromInt (-928349))) (fromString "272329322452")


(*>* Problem 2.1 *>*)
(*>>* 2 *>>*)

let fromInt (n: int) : bignum =
  let rec make_coeffs (n: int) (a: int list) =
    if n = 0 then a else
      make_coeffs (n / base) ((n mod base)::a)
  in
  let (neg, newn) = if n < 0 then (true, 0-n) else (false, n) in
    {neg = neg; coeffs = make_coeffs newn []}
;;

let equal (b1: bignum) (b2: bignum) : bool =
  let rec listsequal (l1: int list) (l2: int list) =
    match (l1, l2) with
      | ([], []) -> true
      | (h1::t1, h2::t2) -> (h1 = h2) && (listsequal t1 t2)
      | ([], _) -> false
      | (_, []) -> false
  in
    (b1.neg = b2.neg) && 
      (listsequal (stripzeroes b1.coeffs) (stripzeroes b2.coeffs))
;;

(* Returns true if b1 is less than b2, false otherwise. *)
let less (b1: bignum) (b2: bignum) : bool =
  let rec less_rec l1 l2 =
    match (l1, l2) with
      | ([], []) -> false
      | (h1::t1, h2::t2) -> if h1 < h2 then true
        else if h1 > h2 then false else less_rec t1 t2
      | ([], _) -> true
      | (_, []) -> false
  in
  let less_abs l1 l2 = 
    let (len1, len2) = 
      (List.length (stripzeroes l1), List.length (stripzeroes l2)) in
      if len1 < len2 then true else if len1 > len2 then false
      else less_rec (stripzeroes b1.coeffs) (stripzeroes b2.coeffs)
  in
    match (b1.neg, b2.neg) with
      | (true, true) -> less_abs b2.coeffs b1.coeffs
      | (true, false) -> true
      | (false, true) -> false
      | (false, false) -> less_abs b1.coeffs b2.coeffs
;;

let negate (b: bignum) : bignum =
  {neg = not b.neg; coeffs = b.coeffs}

let times (b1: bignum) (b2: bignum) : bignum =
  let timessing (b: int list) (n: int) (places: int) : int list =
    let rec timessing_rec (b: int list) (carry: int) : int list =
      match b with
        | [] -> [carry]
        | h::t -> let mult = fromInt (h*n + carry) in
            (match mult.coeffs with
               | [] -> 0::(timessing_rec t 0)
               | [h] -> h::(timessing_rec t 0)
               | c::h::_ -> h::(timessing_rec t c))
    in
    let rec addzeroes (b: int list) (places: int) : int list =
      if places <= 0 then b
      else addzeroes (0::b) (places - 1)
    in
      List.rev (addzeroes (timessing_rec b 0) places)
  in let rec times_rec l1 l2 (places: int) (a: bignum) : bignum =
      match l2 with
        | [] -> a
        | h::t -> let term = timessing l1 h places in
            times_rec l1 t (places + 1) (plus a {neg=false; coeffs=term})
  in
  let xor a b = (a || b) && (not (a && b))
  in
  let neg = xor b1.neg b2.neg in
  let abs_mult = 
    times_rec (List.rev b1.coeffs) (List.rev b2.coeffs) 0 (fromInt 0) in
    {neg = neg; 
     coeffs = abs_mult.coeffs}
;;

let (e, d, n) = ({neg = false; coeffs = [56; 713]},
 {neg = false; coeffs = [1; 711; 691; 57]},
 {neg = false; coeffs = [2; 260; 970; 923]})

>>>
equal (encryptDecryptBignum (fromInt 10000) (fromInt 2) (fromInt 293))
      (fromInt 5849)

>>>
equal (encryptDecryptBignum n d (encryptDecryptBignum n e (fromInt 23834)))
      (fromInt 23834)

>>>
equal (encryptDecryptBignum n d (encryptDecryptBignum n e (fromInt 843592)))
      (fromInt (843592))

(*>* Problem 2.2 *>*)
(*>>* 6 *>>*)

let fromInt (n: int) : bignum =
  let rec make_coeffs (n: int) (a: int list) =
    if n = 0 then a else
      make_coeffs (n / base) ((n mod base)::a)
  in
  let (neg, newn) = if n < 0 then (true, 0-n) else (false, n) in
    {neg = neg; coeffs = make_coeffs newn []}
;;

let equal (b1: bignum) (b2: bignum) : bool =
  let rec listsequal (l1: int list) (l2: int list) =
    match (l1, l2) with
      | ([], []) -> true
      | (h1::t1, h2::t2) -> (h1 = h2) && (listsequal t1 t2)
      | ([], _) -> false
      | (_, []) -> false
  in
    (b1.neg = b2.neg) && 
      (listsequal (stripzeroes b1.coeffs) (stripzeroes b2.coeffs))
;;

(* Returns true if b1 is less than b2, false otherwise. *)
let less (b1: bignum) (b2: bignum) : bool =
  let rec less_rec l1 l2 =
    match (l1, l2) with
      | ([], []) -> false
      | (h1::t1, h2::t2) -> if h1 < h2 then true
        else if h1 > h2 then false else less_rec t1 t2
      | ([], _) -> true
      | (_, []) -> false
  in
  let less_abs l1 l2 = 
    let (len1, len2) = 
      (List.length (stripzeroes l1), List.length (stripzeroes l2)) in
      if len1 < len2 then true else if len1 > len2 then false
      else less_rec (stripzeroes b1.coeffs) (stripzeroes b2.coeffs)
  in
    match (b1.neg, b2.neg) with
      | (true, true) -> less_abs b2.coeffs b1.coeffs
      | (true, false) -> true
      | (false, true) -> false
      | (false, false) -> less_abs b1.coeffs b2.coeffs
;;

let negate (b: bignum) : bignum =
  {neg = not b.neg; coeffs = b.coeffs}

let times (b1: bignum) (b2: bignum) : bignum =
  let timessing (b: int list) (n: int) (places: int) : int list =
    let rec timessing_rec (b: int list) (carry: int) : int list =
      match b with
        | [] -> [carry]
        | h::t -> let mult = fromInt (h*n + carry) in
            (match mult.coeffs with
               | [] -> 0::(timessing_rec t 0)
               | [h] -> h::(timessing_rec t 0)
               | c::h::_ -> h::(timessing_rec t c))
    in
    let rec addzeroes (b: int list) (places: int) : int list =
      if places <= 0 then b
      else addzeroes (0::b) (places - 1)
    in
      List.rev (addzeroes (timessing_rec b 0) places)
  in let rec times_rec l1 l2 (places: int) (a: bignum) : bignum =
      match l2 with
        | [] -> a
        | h::t -> let term = timessing l1 h places in
            times_rec l1 t (places + 1) (plus a {neg=false; coeffs=term})
  in
  let xor a b = (a || b) && (not (a && b))
  in
  let neg = xor b1.neg b2.neg in
  let abs_mult = 
    times_rec (List.rev b1.coeffs) (List.rev b2.coeffs) 0 (fromInt 0) in
    {neg = neg; 
     coeffs = abs_mult.coeffs}
;;

let (e, d, n) = ({neg = false; coeffs = [56; 713]},
 {neg = false; coeffs = [1; 711; 691; 57]},
 {neg = false; coeffs = [2; 260; 970; 923]})

let bytesInKey (n: bignum) =
  int_of_float ((float_of_int ((List.length (stripzeroes n.coeffs)) - 1)) 
                *. (log10 (float_of_int base)) /. ((log10 2.) *. 8.))
;;

let rec charsToBignums (lst: char list) (m: int) : bignum list =
  let rec encchars lst =
    match lst with
      | [] -> (fromInt 0)
      | c::t -> plus (times (encchars t) (fromInt 256)) (fromInt (Char.code c))
  in
    match lst with
      | [] -> []
      | _ -> let (enclist, rest) = split lst m in
          (encchars enclist)::(charsToBignums rest m)
;;

let encryptDecryptBignum (n: bignum) (e: bignum) (s: bignum) : bignum =
  expmod s e n
;;

let rec eq_list l1 l2 =
match (l1, l2) with
| ([], []) -> true
| ([], _) -> false
| (_, []) -> false
| (h1::t1, h2::t2) -> (equal h1 h2) && (eq_list t1 t2)
;;

>>>
eq_list (encrypt n e "I love CS51!") [{neg = false; coeffs = [438; 63; 724]};
 {neg = false; coeffs = [87; 174; 96]};
 {neg = false; coeffs = [1; 942; 532; 589]};
 {neg = false; coeffs = [904; 982; 929]}]

>>>
decrypt n d [{neg = false; coeffs = [438; 63; 724]};
 {neg = false; coeffs = [87; 174; 96]};
 {neg = false; coeffs = [1; 942; 532; 589]};
 {neg = false; coeffs = [904; 982; 929]}] = "I love CS51!"

>>>
decrypt n d (encrypt n e "MOOOOOOOOOO!") = "MOOOOOOOOOO!"

(*>* Problem 3.1 *>*)
(*>>* 2 *>>*)

>>>
equal (times_faster (fromInt 100200300) (fromInt 3)) (fromInt 300600900)

>>>
equal (times_faster (fromInt 100200300) (fromInt 100200300)) (fromString "10040100120090000")

>>>
equal (times_faster (fromInt (-123456789)) (fromInt 2)) (fromInt (-246913578))

>>>
equal (times_faster (fromInt 2000) (fromInt (-123456789))) (fromString "~246913578000")

>>>
equal (times_faster (fromInt (-293348)) (fromInt (-928349))) (fromString "272329322452")
