open Core.Std
open Helpers
open WorldObject
open WorldObjectI

(* ### Part 2 Movement ### *)
let walker_inverse_speed = Some 1

(* ### Part 6 Custom Events ### *)
let max_destroyed_objects = 100

(** Cows will graze across the field until it has destroyed a satisfactory number
    of flowers *)
class white_walker p (home : world_object_i) city : Movable.movable_t =
object (self)
  inherit Movable.movable p walker_inverse_speed

  (******************************)
  (***** Instance Variables *****)
  (******************************)

  (* ### TODO: Part 3 Actions ### *)
  val mutable destroyed_objects = 0

  (***********************)
  (***** Initializer *****)
  (***********************)

  (* ### TODO: Part 3 Actions ### *)
  initializer
    self#register_handler World.action_event (fun () -> self#do_action)

  (**************************)
  (***** Event Handlers *****)
  (**************************)

  (* ### TODO: Part 3 Actions ### *)
  method private do_action =
    if destroyed_objects < max_destroyed_objects then
      let neighbors = World.get self#get_pos in
      List.iter ~f:begin fun o -> match o#smells_like_gold with
        | Some _ -> o#die ;
          destroyed_objects <- destroyed_objects + 1
        | None -> ()
      end neighbors
  (* ### TODO: Part 6 Custom Events ### *)
    else if self#get_pos = home#get_pos then
      (* if no longer hungry and back at the pasture, then expire. *)
      self#die


  (********************************)
  (***** WorldObjectI Methods *****)
  (********************************)

  (* ### TODO: Part 1 Basic ### *)
  method! get_name = "white_walker"

  method! draw =
    self#draw_circle (Graphics.rgb 0x89 0xCF 0xF0) Graphics.black
      (string_of_int destroyed_objects)

  method! draw_z_axis = 40

  (* ### TODO: Part 3 Actions ### *)

  (***************************)
  (***** Movable Methods *****)
  (***************************)

  (* ### TODO: Part 2 Movement ### *)
  method! next_direction =
    if destroyed_objects >= max_destroyed_objects then
      (* if no longer hungry then return home. *)
      World.direction_from_to self#get_pos home#get_pos
    else if World.rand (World.size/2) = 0 then
      (* occasionally move toward the city where towns have a high
         probability of surviving. *)
      World.direction_from_to self#get_pos city#get_pos
    else
      (* otherwise random walk. *)
      Some (Direction.random World.rand)

  (* ### TODO: Part 6 Custom Events ### *)

end
