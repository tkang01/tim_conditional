open WorldObject
open WorldObjectI
open Cow

(* ### Part 6 Custom Events ### *)
let smelly_object_limit = 200

(** A pasture will spawn a cow when there are enough objects in the world that
    smell like pollen. *)
class pasture p hive: world_object_i =
object (self)
  inherit world_object p as super

  val mutable cow_in_pas = true

  initializer
    self#register_handler World.action_event (fun _ -> self#do_action)

  (******************************)
  (***** Instance Variables *****)
  (******************************)
 
  method private count_smelly = 
    World.fold (fun x y -> if ((x#smells_like_pollen)!= None) then y+1 else y) 0 

  method private do_action =
    if ((self#count_smelly > smelly_object_limit) && cow_in_pas) then 
    (new cow (self#get_pos) hive self;cow_in_pas <- false;flush_all(print_string "mooooooooo ")) 

  (* ### TODO Part 6 Custom Events ### *)

  (***********************)
  (***** Initializer *****)
  (***********************)

  (* ### TODO Part 6 Custom Events ### *)

  (**************************)
  (***** Event Handlers *****)
  (**************************)

  (* ### TODO Part 6 Custom Events ### *)

  (********************************)
  (***** WorldObjectI Methods *****)
  (********************************)

  (* ### TODO Part 1 Basic ### *)

  method get_name = "pasture"

  method draw = self#draw_circle (Graphics.rgb 70 100 130) Graphics.white "P"

  method draw_z_axis = 1


end

