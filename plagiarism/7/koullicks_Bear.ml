open WorldObject
open Movable
open Direction
open World
open WorldObjectI

(* ### Part 3 Actions ### *)
let pollen_theft_amount = 1000

(* ### Part 4 Aging ### *)
let bear_starting_life = 20

(* ### Part 2 Movement ### *)
let bear_inverse_speed = Some 40

class bear p goal : movable_t =
object (self)
  inherit movable p bear_inverse_speed as super

  (******************************)
  (***** Instance Variables *****)
  (******************************)

  val mutable stolen_honey = 0

  (* ### TODO: Part 6 Events ### *)

  (***********************)
  (***** Initializer *****)
  (***********************)

  initializer
  ignore(self#register_handler ( World.action_event) (fun () -> self#do_action))  


  (**************************)
  (***** Event Handlers *****)
  (**************************)

  method private do_action = 
  if (self#on_hive ()) 
  then (let amount = (goal#forfeit_honey pollen_theft_amount (self :> world_object_i))
  in (stolen_honey <- (stolen_honey + amount) ; self#draw))
  else ()
  
  method private on_hive () = 
  let neighbors = World.get self#get_pos in
  let rec helper lst =
  match lst with
  | [] -> false
  | hd::tl -> if (hd#get_name = "hive") then true else helper tl
  in
  helper neighbors

  (* ### TODO: Part 6 Custom Events ### *)

  (********************************)
  (***** WorldObjectI Methods *****)
  (********************************)
  method draw_z_axis = 3
  method get_name = "bear"
  method draw = super#draw_circle (Graphics.rgb 170 130 110) Graphics.black (string_of_int stolen_honey)

  (* ### TODO: Part 3 Actions ### *)

  (* ### TODO: Part 6 Custom Events ### *)

  (***************************)
  (***** Movable Methods *****)
  (***************************)

  (* ### TODO: Part 2 Movement ### *)
  
  method next_direction = World.direction_from_to self#get_pos goal#get_pos

  (* ### TODO: Part 3 Actions ### *)

  (* ### TODO: Part 6 Custom Events ### *)

end
