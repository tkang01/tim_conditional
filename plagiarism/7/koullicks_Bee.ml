open WorldObject
open Movable
open Direction
open World
open Ageable
open CarbonBased
open WorldObjectI

(* ### Part 2 Movement ### *)
let bee_inverse_speed = Some 1

(* ### Part 3 Actions ### *)
let max_pollen_types = 5

(* ### Part 4 Aging ### *)
let bee_lifetime = 1000

(* ### Part 5 Smart Bees ### *)
let max_sensing_range = 5

(** Bees travel the world searching for honey.  They are able to sense flowers
    within close range, and they will return to the hive once they have
    pollenated enough species of flowers. *)

class type bee_t =
object
  inherit ageable_t

  method next_direction_default : Direction.direction option
end

class bee p (home:world_object_i) : bee_t =
object (self)
  inherit carbon_based p bee_inverse_speed (World.rand bee_lifetime) bee_lifetime as super

  (******************************)
  (***** Instance Variables *****)
  (******************************)

  (* ### TODO: Part 3 Actions ### *)
  val mutable pollen = []
  
  val mutable unique_pollen = 0;

  (* ### TODO: Part 5 Smart Bees ### *)

  (* ### TODO: Part 6 Custom Events ### *)
  
  val sensing_range = World.rand max_sensing_range

  val pollen_types = World.rand max_pollen_types + 1

  (***********************)
  (***** Initializer *****)
  (***********************)

  (* ### TODO: Part 3 Actions ### *)
    
  initializer
  ignore(self#register_handler ( World.action_event) (fun () -> self#do_action))
  

  (* ### TODO: Part 6 Custom Events ### *)

  (**************************)
  (***** Event Handlers *****)
  (**************************)

  method private do_action : unit =
  let neighbors = World.get self#get_pos in 
  List.iter self#deposit_pollen neighbors ;
  List.iter self#extract_pollen neighbors 

  (* ### TODO: Part 6 Custom Events ### *)
    
  (**************************)
  (***** Helper Methods *****)
  (**************************)

  method private deposit_pollen neighbor =
  match neighbor#get_name with
  | "flower" -> let b = neighbor#receive_pollen pollen in pollen <- b
  | "hive" -> let b = neighbor#receive_pollen pollen in pollen <- b
  | _ -> ()
  
  
  method private extract_pollen neighbor = 
   let new_pollen = neighbor#forfeit_pollen
   in match new_pollen with
    |None -> ()
    |Some a -> pollen <- a::(pollen); 
                            if List.exists (fun x -> x=a) pollen 
                            then ()
                            else unique_pollen <- unique_pollen +1
  
  method private magnet_flower : world_object_i option =
  let things = (World.objects_within_range self#get_pos sensing_range) in
  let rec helper lst =
  match lst with
  | [] -> None
  | hd::tl -> if ((hd#get_name = "flower") && (self#new_pollen hd)) then Some hd else helper tl
  in
  helper things

  method private new_pollen thing =
  let rec helper lst obj =
  match lst with
  | [] -> false
  | hd::tl -> (let a = thing#smells_like_pollen in (
                match a with
                | None -> helper tl obj
                | Some b -> if (hd = b) then true else helper tl obj ))
  in
  helper pollen thing
  
  (* ### TODO: Part 5 Smart Bees ### *)

  (********************************)
  (***** WorldObjectI Methods *****)
  (********************************)
  
  method draw_z_axis = 2
  method get_name = "bee"
  method draw_picture = super#draw_circle Graphics.yellow Graphics.black (string_of_int (List.length pollen))

  (* ### TODO: Part 3 Actions ### *)

  (***************************)
  (***** Ageable Methods *****)
  (***************************)

  (* ### TODO: Part 4 Aging ### *)

  (***************************)
  (***** Movable Methods *****)
  (***************************)

  method next_direction = 
  if ((List.length pollen) > pollen_types)
  then (World.direction_from_to self#get_pos home#get_pos)
  else
  (match self#magnet_flower with
  | None -> self#next_direction_default
  | Some obj -> World.direction_from_to self#get_pos obj#get_pos)

  (* ### TODO: Part 5 Smart Bees ### *)

  (* ### TODO: Part 6 Custom Events ### *)

  (***********************)
  (***** Bee Methods *****)
  (***********************)

  (* ### TODO: Part 5 Smart Bees ### *)
  method next_direction_default = (Some (Direction.random World.rand))
end
