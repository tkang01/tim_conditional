open Helpers
open WorldObject
open WorldObjectI

(* ### Part 2 Movement ### *)
let cow_inverse_speed = Some 1

(* ### Part 6 Custom Events ### *)
let max_consumed_objects = 100

(** Cows will graze across the field until it has consumed a satisfactory number
    of flowers *)
class cow p (home : world_object_i) hive : Movable.movable_t =
object (self)
  inherit Movable.movable p cow_inverse_speed as super

  (******************************)
  (***** Instance Variables *****)
  (******************************)

  (* ### TODO: Part 3 Actions ### *)
  val mutable consumed_objects = 0

  (***********************)
  (***** Initializer *****)
  (***********************)

  (* ### TODO: Part 3 Actions ### *)
  initializer
    self#register_handler World.action_event (fun () -> self#do_action)

  (**************************)
  (***** Event Handlers *****)
  (**************************)

  (* ### TODO: Part 3 Actions ### *)
  method private do_action =
    if consumed_objects < max_consumed_objects then
      let neighbors = World.get self#get_pos in
      List.iter begin fun o -> match o#smells_like_pollen with
        | Some _ -> print_string "*nom* " ; flush_all () ;
          o#die ;
          consumed_objects <- consumed_objects + 1
        | None -> ()
      end neighbors
  (* ### TODO: Part 6 Custom Events ### *)
    else if self#get_pos = home#get_pos then
      (* if no longer hungry and back at the pasture, then expire. *)
      self#die


  (********************************)
  (***** WorldObjectI Methods *****)
  (********************************)

  (* ### TODO: Part 1 Basic ### *)
  method get_name = "cow"

  method draw =
    self#draw_circle (Graphics.rgb 180 140 255) Graphics.black
      (string_of_int consumed_objects)

  method draw_z_axis = 30

  (* ### TODO: Part 3 Actions ### *)

  (***************************)
  (***** Movable Methods *****)
  (***************************)

  (* ### TODO: Part 2 Movement ### *)
  method next_direction =
    if consumed_objects >= max_consumed_objects then
      (* if no longer hungry then return home. *)
      World.direction_from_to self#get_pos home#get_pos
    else if World.rand (World.size/2) = 0 then
      (* occasionally move toward the hive where flowers have a high
         probability of surviving. *)
      World.direction_from_to self#get_pos hive#get_pos
    else
      (* otherwise random walk. *)
      Some (Direction.random World.rand)

  (* ### TODO: Part 6 Custom Events ### *)

end
