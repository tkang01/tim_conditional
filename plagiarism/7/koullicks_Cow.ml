open WorldObject
open Movable
open Direction
open World

(* ### Part 2 Movement ### *)
let cow_inverse_speed = Some 1

(* ### Part 6 Custom Events ### *)
let max_consumed_objects = 100

(** Cows will graze across the field until it has consumed a satisfactory number
    of flowers *)
class cow p goal: movable_t =
object (self)
  inherit movable p cow_inverse_speed as super

  (******************************)
  (***** Instance Variables *****)
  (******************************)

  val mutable eaten = 0

  (***********************)
  (***** Initializer *****)
  (***********************)

  initializer
  ignore(self#register_handler ( World.action_event) (fun () -> self#do_action))

  (**************************)
  (***** Event Handlers *****)
  (**************************)

  method private eat flower = eaten <- eaten + 1 ; flower#die ; Printf.printf "*nom* " ; flush_all ()

  method private do_action = 
  let neighbors = World.get self#get_pos in
  List.iter (fun x -> if (not(x#smells_like_pollen = None)) then self#eat x else ()) neighbors

  (* ### TODO: Part 6 Custom Events ### *)

  (********************************)
  (***** WorldObjectI Methods *****)
  (********************************)

  method get_name = "cow"
  method draw = super#draw_circle (Graphics.rgb 180 140 255) (Graphics.black) (string_of_int eaten)
  method draw_z_axis = 4

  (* ### TODO: Part 3 Actions ### *)

  (***************************)
  (***** Movable Methods *****)
  (***************************)

  method next_direction = (Some (Direction.random World.rand))

  (* ### TODO: Part 6 Custom Events ### *)

end
