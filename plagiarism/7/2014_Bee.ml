open Core.Std
open Event51
open Helpers
open WorldObject
open WorldObjectI

(* ### Part 2 Movement ### *)
let human_inverse_speed = Some 1

(* ### Part 3 Actions ### *)
let max_gold_types = 5

(* ### Part 4 Aging ### *)
let human_lifetime = 1000

(* ### Part 5 Smart Humans ### *)
let max_sensing_range = 5

(** Bees travel the world searching for honey.  They are able to sense flowers
    within close range, and they will return to the hive once they have
    pollenated enough species of flowers. *)
class type human_t =
object
  inherit Ageable.ageable_t

  method private next_direction_default : Direction.direction option
end
class human p (home:world_object_i) : human_t = object(self)
  inherit CarbonBased.carbon_based p human_inverse_speed
    (World.rand human_lifetime) human_lifetime

  (******************************)
  (***** Instance Variables *****)
  (******************************)

  (* ### TODO: Part 3 Actions ### *)
  val mutable gold = []

  (* ### TODO: Part 5 Smart Bees ### *)
  val sensing_range = World.rand max_sensing_range

  val gold_types = World.rand max_gold_types + 1

  (* ### TODO: Part 6 Custom Events ### *)
  val mutable danger_object = None

  (***********************)
  (***** Initializer *****)
  (***********************)

  (* ### TODO: Part 3 Actions ### *)
  initializer
    self#register_handler World.action_event (fun () -> self#do_action);
  (* ### TODO: Part 6 Custom Events ### *)
    self#register_handler home#get_danger_event (fun o -> self#do_danger o)

  (**************************)
  (***** Event Handlers *****)
  (**************************)

  method private do_action : unit =
    let neighbors = World.get self#get_pos in
    List.iter ~f:self#deposit_gold neighbors;
    List.iter ~f:self#extract_gold neighbors;
  (* ### TODO: Part 6 Custom Events ### *)
    match danger_object with
    | None -> ()
    | Some o -> if self#get_pos = o#get_pos then begin
        o#receive_sting ;
        self#die
      end

  method private do_danger (o:world_object_i) : unit =
    danger_object <- Some o ;
    ignore(Event51.add_listener o#get_die_event
      (fun () -> danger_object <- None))

  (**************************)
  (***** Helper Methods *****)
  (**************************)

  (* ### TODO: Part 3 Actions ### *)
  method private deposit_gold (o:world_object_i) : unit =
    let gold' = o#receive_gold gold in
    gold <- gold'

  method private extract_gold (o:world_object_i) : unit =
    match o#forfeit_gold with
    | None -> ()
    | Some i -> gold <- i::gold

  (* ### TODO: Part 5 Smart Bees ### *)
  method private magnet_flower : world_object_i option =
    let os = World.objects_within_range self#get_pos sensing_range in
    let smelly = List.filter ~f:begin fun o ->
      match o#smells_like_gold with
      | None -> false
      | Some s -> not (List.mem gold s)
    end os in
    let ps = List.map ~f:(fun o ->
      (Direction.distance o#get_pos self#get_pos, o)) smelly in
    let sorted = List.sort ~cmp:compare ps in
    match sorted with
    | [] -> None
    | (_,o)::_ -> Some o

  (********************************)
  (***** WorldObjectI Methods *****)
  (********************************)

  (* ### TODO: Part 1 Basic ### *)
  method! get_name = "human"

  method! draw_picture =
    self#draw_circle (Graphics.rgb 0xC9 0xC0 0xBB) Graphics.black
                     (string_of_int (List.length gold))

  method! draw_z_axis = 20

  (* ### TODO: Part 3 Actions ### *)

  (***************************)
  (***** Ageable Methods *****)
  (***************************)

  (* ### TODO: Part 4 Aging ### *)

  (***************************)
  (***** Movable Methods *****)
  (***************************)

  (* ### TODO: Part 2 Movement ### *)
  (* ### TODO: Part 5 Smart Bees ### *)
  method! next_direction =
    match danger_object with
    | Some o ->
        World.direction_from_to self#get_pos o#get_pos
    | None ->
        if List.length (Helpers.unique gold) >= max_gold_types then
          World.direction_from_to self#get_pos home#get_pos
        else match self#magnet_flower with
        | Some m -> World.direction_from_to self#get_pos m#get_pos
        | None -> self#next_direction_default

  (* ### TODO: Part 6 Custom Events ### *)

  (***********************)
  (***** Bee Methods *****)
  (***********************)

  (* ### TODO: Part 5 Smart Bees ### *)
  method private next_direction_default = None

end
