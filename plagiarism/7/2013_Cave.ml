open Event
open WorldObject
open WorldObjectI

(* ### Part 6 Custom Events ### *)
let spawn_bear_pollen = 500

(** A cave will spawn a bear when the hive has collected a certain amount of
    honey. *)
class cave p (hive : Hive.hive) : world_object_i =
object (self)
  inherit world_object p as super

  (******************************)
  (***** Instance Variables *****)
  (******************************)

  (* ### TODO: Part 6 Custom Events ### *)
  val mutable bear_attacking = false

  (***********************)
  (***** Initializer *****)
  (***********************)

  (* ### TODO: Part 6 Custom Events ### *)
  initializer
    let spawn_condition pollen =
      pollen >= spawn_bear_pollen && not bear_attacking
    in
    self#register_handler (Event.filter spawn_condition hive#get_pollen_event)
                          (fun _ -> self#do_spawn_bear)

  (**************************)
  (***** Event Handlers *****)
  (**************************)

  (* ### TODO: Part 6 Custom Events ### *)
  method private do_spawn_bear =
    print_string "omg bears! " ; flush_all () ;
    let bear = new Bear.bear self#get_pos (self :> world_object_i) hive in
    bear_attacking <- true ;
    self#register_handler bear#get_die_event (fun () -> bear_attacking <- false)

  (********************************)
  (***** WorldObjectI Methods *****)
  (********************************)

  (* ### TODO: Part 1 Basic ### *)
  method get_name = "cave"

  method draw = self#draw_circle Graphics.black Graphics.white "C"

  (* ### TODO: Part 6 Custom Events *)
  method receive_pollen _ = []

end
