open WorldObject

(* ### Part 6 Custom Events ### *)
let spawn_bear_pollen = 500 

(** A cave will spawn a bear when the hive has collected a certain amount of
    honey. *)
class cave p : world_object_t =
object (self)
  inherit world_object p as super

  (******************************)
  (***** Instance Variables *****)
  (******************************)

  (* ### TODO: Part 6 Custom Events ### *)

  (***********************)
  (***** Initializer *****)
  (***********************)

  (* ### TODO: Part 6 Custom Events ### *)

  (**************************)
  (***** Event Handlers *****)
  (**************************)

  (* ### TODO: Part 6 Custom Events ### *)

  (********************************)
  (***** WorldObjectI Methods *****)
  (********************************)

  method get_name = "cave"
  method draw = super#draw_circle Graphics.white Graphics.black "C"

  (* ### TODO: Part 6 Custom Events *)

end
