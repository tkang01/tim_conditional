open Ageable

(* ### Part 4 Aging ### *)
let dust_lifetime = 50

(** Dust is what remains when carbon-based objects die. *)
class dust p (name:string) : ageable_t =
object (self)
  inherit ageable p None dust_lifetime dust_lifetime as super

  (***************************)
  (***** Ageable Methods *****)
  (***************************)

  method draw_picture = let first_two = (Char.escaped name.[0]) ^ (Char.escaped name.[1]) in
  super#draw_circle (Graphics.rgb 150 150 150) Graphics.black (first_two)

end
