open WorldObject

(** Ponds serve as obstruction for other world objects. *)
class pond p : world_object_t = 
object (self)
  inherit world_object p as super

  method get_name = "pond"
  
  method draw = super#draw_circle Graphics.blue Graphics.black ""
  
  method is_obstacle = true
  
  (********************************)
  (***** WorldObjectI Methods *****)
  (********************************)

end
