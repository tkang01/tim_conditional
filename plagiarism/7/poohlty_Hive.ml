open WorldObject
open WorldObjectI
open Bee
open BeeBouncy
open BeeRandom
open Event

(* ### Part 3 Actions ### *)
let starting_pollen = 500
let cost_of_bee = 10
let spawn_probability = 20
let pollen_probability = 50
let max_pollen_deposit = 3

(** A hive will spawn bees and serve as a deposit point for the pollen that bees
    harvest.  It is possible to steal honey from a hive, however the hive will
    signal that it is in danger and its loyal bees will become angry. *)
class hive p : 
object
  inherit world_object_i
  method forfeit_honey : int -> world_object_i -> int
  method get_pollen_event : int Event.event
  method get_pollen : int 
end =
object (self)
  inherit world_object p as super

  (******************************)
  (***** Instance Variables *****)
  (******************************)
  val mutable pollen_num : int = starting_pollen 

  val mutable ev : int Event.event = Event.new_event ()

  (* ### TODO: Part 3 Actions ### *)

  (* ### TODO: Part 6 Custom Events ### *)

  (***********************)
  (***** Initializer *****)
  (***********************)

  initializer
    self#register_handler World.action_event (fun _ -> self#do_action);
    (* self#register_handler self#get_pollen_event (fun _ -> self#get_pollen) *)

  (* ### TODO: Part 3 Actions ### *)

  method get_pollen_event = ev

  method get_pollen = pollen_num
  
  method private do_action = 
    Helpers.with_inv_probability World.rand pollen_probability 
      (fun () -> pollen_num <- pollen_num+1);
      
    Helpers.with_inv_probability World.rand spawn_probability
      (fun () -> 
      if pollen_num > cost_of_bee then
      (self#generate_bee; pollen_num <- pollen_num - cost_of_bee)
      else ())

  (**************************)
  (***** Event Handlers *****)
  (**************************)

  method receive_pollen p =
    Event.fire_event self#get_pollen_event self#get_pollen;
    if (List.length p) > max_pollen_deposit then
      (pollen_num <- pollen_num + max_pollen_deposit; [])
    else 
      (pollen_num <- pollen_num + List.length p; [])

  method forfeit_honey n obj = 
    if n < pollen_num then
      (pollen_num <- pollen_num - n;self#danger obj;n)
    else
      (let n2 = pollen_num in
        pollen_num <- 0;self#danger obj;n2)

  method private generate_bee = 
    Helpers.with_inv_probability_or World.rand 2 
      (fun () -> new bee_bouncy (self#get_pos) (self :> world_object_i))
      (fun () -> new bee_random (self#get_pos) (self :> world_object_i))
    

  (* ### TODO: Part 3 Actions ### *)

  (* ### TODO: Part 4 Aging ### *)

  (**************************)
  (***** Helper Methods *****)
  (**************************)

  (* ### TODO: Part 4 Aging ### *)

  (* ### TODO: Part 5 Smart Bees ### *)

  (****************************)
  (*** WorldObjectI Methods ***)
  (****************************)

  (* ### TODO: Part 1 Basic ### *)

  method get_name = "hive"

  method draw = self#draw_circle Graphics.cyan Graphics.black (string_of_int(pollen_num))

  method draw_z_axis = 1


  (* ### TODO: Part 3 Actions ### *)

  (* ### TODO: Part 6 Custom Events ### *)

  (************************)
  (***** Hive Methods *****)
  (************************)

  (* ### TODO: Part 3 Actions ### *)

  (* ### TODO: Part 6 Custom Events ### *)

end
