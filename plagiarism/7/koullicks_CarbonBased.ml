open Ageable
open Dust
open World

(** Carbon based objects eventually die, and leave dust behind when they do. *)
class carbon_based p inv_speed starting_lifetime max_lifetime : ageable_t =
object (self)
  inherit ageable p inv_speed starting_lifetime max_lifetime as super

  (***********************)
  (***** Initializer *****)
  (***********************)
  initializer
  ignore(self#register_handler ( self#get_die_event) (fun () -> self#do_die))
  
  method private do_die =  World.spawn 1 p (fun p -> ignore(new dust self#get_pos self#get_name))

end
