open Core.Std
open Event51
open Helpers
open WorldObject
open WorldObjectI

(* ### Part 3 Actions ### *)
let starting_gold = 500
let cost_of_human = 10
let spawn_probability = 20
let gold_probability = 50
let max_gold_deposit = 3

(** King's Landing will spawn humans and serve as a deposit point for the gold
    that humans harvest. It is possible to steal gold from King's Landing;
    however the city will signal that it is in danger and its loyal humans
    will become angry. *)
class kings_landing p :
object
  inherit world_object_i

  method forfeit_treasury : int -> world_object_i -> int

  method get_gold_event : int Event51.event

  method get_gold : int
end =
object (self)
  inherit world_object p

  (******************************)
  (***** Instance Variables *****)
  (******************************)

  (* ### TODO: Part 3 Actions ### *)
  val mutable gold = starting_gold

  (* ### TODO: Part 6 Custom Events ### *)
  val gold_event = Event51.new_event ()

  (***********************)
  (***** Initializer *****)
  (***********************)

  (* ### TODO: Part 3 Actions ### *)
  initializer
    self#register_handler World.action_event (fun () -> self#do_action)

  (**************************)
  (***** Event Handlers *****)
  (**************************)

  (* ### TODO: Part 3 Actions ### *)
  method private do_action =
    Helpers.with_inv_probability World.rand gold_probability
      begin fun () ->
        gold <- gold + 1
      end;
    if gold >= cost_of_human then
      Helpers.with_inv_probability World.rand spawn_probability
        begin fun () ->
          gold <- gold - cost_of_human ;
          self#generate_human self#get_pos
        end

  (* ### TODO: Part 4 Aging ### *)

  (**************************)
  (***** Helper Methods *****)
  (**************************)

  (* ### TODO: Part 4 Aging ### *)
  method private generate_human p =
    Helpers.with_equal_probability World.rand [
      (fun () -> ignore (new Lannister.lannister p (self :> world_object_i))) ;
      fun () -> ignore (new Baratheon.baratheon p (self :> world_object_i))
    ]

  (****************************)
  (*** WorldObjectI Methods ***)
  (****************************)

  (* ### TODO: Part 1 Basic ### *)
  method! get_name = "kings_landing"

  method! draw =
    self#draw_circle (Graphics.rgb 0xFF 0xD7 0x00)
      Graphics.black (string_of_int gold)

  (* ### TODO: Part 3 Actions ### *)
  method! receive_gold ps =
    gold <- gold + (min (List.length ps) max_gold_deposit) ;
    Event51.fire_event gold_event gold ;
    []

  (* ### TODO: Part 6 Custom Events ### *)

  (************************)
  (***** Hive Methods *****)
  (************************)

  (* ### TODO: Part 3 Actions ### *)
  method forfeit_treasury n b =
    let stolen = min gold n in
    gold <- gold - stolen ;
    self#danger b ;
    stolen

  (* ### TODO: Part 6 Custom Events ### *)
  method get_gold_event : int Event51.event = gold_event

  method get_gold = gold

end
