open WorldObject
open WorldObjectI
open Bee

(** Bouncy bees will travel in a straight line in a random direction until an
    obstacle or edge of the world is reached, at which point a new random
    direction will be chosen. *)
class bee_bouncy p hive : bee_t =
object (self)
  inherit bee p hive as super

  val mutable dir = Some(Direction.random World.rand)

  (******************************)
  (***** Instance Variables *****)
  (******************************)

  (* ### TODO: Part 5 Smart Bees *)

  method private next_direction_default = 
    if World.can_move (Direction.move_point self#get_pos dir)
    then dir
    else (dir <- self#get_valid_direction;dir)

  method private get_valid_direction =
    let d = Some(Direction.random World.rand) in
    if World.can_move (Direction.move_point self#get_pos d)
    then d
    else self#get_valid_direction

  method get_name = "bee_bouncy"
  (********************************)
  (***** WorldObjectI Methods *****)
  (********************************)

  (* ### TODO: Part 5 Smart Bees *)

  (***********************)
  (***** Bee Methods *****)
  (***********************)

  (* ### TODO: Part 5 Smart Bees *)

end