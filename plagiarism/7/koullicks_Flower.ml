open WorldObject
open World
open Ageable
open CarbonBased

(* ### Part 3 Actions ### *)
let next_pollen_id = ref 0
let get_next_pollen_id () =
  let p = !next_pollen_id in incr next_pollen_id ; p

(* ### Part 3 Actions ### *)
let max_pollen = 5
let produce_pollen_probability = 50
let bloom_probability = 4000
let forfeit_pollen_probability = 3

(* ### Part 4 Aging ### *)
let flower_lifetime = 5000

(** Flowers produce pollen.  They will also eventually die if they are not cross
    pollenated. *)
class flower p pollen_id: ageable_t =
object (self)
  inherit carbon_based p None (World.rand flower_lifetime) flower_lifetime as super

  (******************************)
  (***** Instance Variables *****)
  (******************************)

  (* ### TODO: Part 3 Actions ### *)
  
  val mutable pollen_to_offer = World.rand max_pollen

  (***********************)
  (***** Initializer *****)
  (***********************)

  initializer
  ignore(self#register_handler ( World.action_event) (fun () -> self#do_action))  

  (**************************)
  (***** Event Handlers *****)
  (**************************)

  method private do_action =
  let p = World.rand produce_pollen_probability
  in let b = World.rand bloom_probability in
  match (p < 1 && pollen_to_offer < max_pollen),(b < 1) with
  | true, true -> World.spawn 1 (World.next_available self#get_pos) (fun x -> ignore(new flower x pollen_id)) ;
                  if (not(pollen_to_offer = max_pollen)) then ((pollen_to_offer <- pollen_to_offer + 1) ; self#draw) else ()
  | true, _ -> if not(pollen_to_offer = max_pollen) then ((pollen_to_offer <- pollen_to_offer + 1); self#draw) else ()
  | _, true -> World.spawn 1 (World.next_available self#get_pos) (fun x -> ignore(new flower x pollen_id))
  | _, _ -> ()
   
  (********************************)
  (***** WorldObjectI Methods *****)
  (********************************)

  (* ### TODO: Part 1 Basic ### *)
  method next_direction = None
  
  method get_name = "flower"
  method draw_picture = super#draw_circle (Graphics.rgb 255 150 255) Graphics.black (string_of_int pollen_to_offer)

  (* ### TODO: Part 3 Actions ### *)
  
  method forfeit_pollen = 
  let a = World.rand forfeit_pollen_probability in 
  if (a = 0 && pollen_to_offer > 0) then (pollen_to_offer <- pollen_to_offer - 1 ; Some (pollen_id))
  else None
  
  method smells_like_pollen =
  if (pollen_to_offer > 0) then Some (pollen_id) else None
  
  method receive_pollen pollen_list = 
  if List.exists (fun ele -> (not (ele = pollen_id))) pollen_list 
  then (self#reset_life; pollen_list) else pollen_list
    
  (***************************)
  (***** Ageable Methods *****)
  (***************************)

  (* ### TODO: Part 4 Aging ### *)

end
