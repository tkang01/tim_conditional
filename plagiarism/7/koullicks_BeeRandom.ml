open WorldObject
open Bee
open Direction
open World

(** Random bees will move randomly. *)
class bee_random p hive: bee_t = 
object (self)
  inherit bee p hive as super

  (********************************)
  (***** WorldObjectI Methods *****)
  (********************************)

  (* ### TODO: Part 5 Smart Bees *)
  method get_name = "bee_random"

  (***********************)
  (***** Bee METHODS *****)
  (***********************)

  (* ### TODO: Part 5 Smart Bees *)
  
  method next_direction_default = Some(Direction.random World.rand) 
  
end


