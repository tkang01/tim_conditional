open Core.Std
open Helpers
open WorldObject
open WorldObjectI

(* ### Part 3 Actions ### *)
let next_gold_id = ref 0
let get_next_gold_id () =
  let p = !next_gold_id in incr next_gold_id ; p

(* ### Part 3 Actions ### *)
let max_gold = 5
let produce_gold_probability = 50
let expand_probability = 4000
let forfeit_gold_probability = 3

(* ### Part 4 Aging ### *)
let town_lifetime = 2000

(** Towns produce gold.  They will also eventually die if they are not cross
    pollenated. *)
class town p gold_id : Ageable.ageable_t =
object (self)
  inherit CarbonBased.carbon_based p None
    (World.rand town_lifetime) town_lifetime

  (******************************)
  (***** Instance Variables *****)
  (******************************)

  (* ### TODO: Part 3 Actions ### *)
  val mutable gold = World.rand max_gold

  (***********************)
  (***** Initializer *****)
  (***********************)

  (* ### TODO: Part 3 Actions ### *)
  initializer
    self#register_handler World.action_event (fun () -> self#do_action)

  (**************************)
  (***** Event Handlers *****)
  (**************************)

  (* ### TODO: Part 3 Actions ### *)
  method private do_action =
    Helpers.with_inv_probability World.rand produce_gold_probability
      begin fun () ->
        gold <- min max_gold (gold + 1)
      end ;
    Helpers.with_inv_probability World.rand expand_probability
      begin fun () ->
        World.spawn 1 self#get_pos (fun p -> ignore (new town p gold_id))
      end

  (********************************)
  (***** WorldObjectI Methods *****)
  (********************************)

  (* ### TODO: Part 1 Basic ### *)
  method! get_name = "town"

  (* ### TODO: Part 4 Aging ### *)
  method! draw_picture =
    self#draw_circle (Graphics.rgb 0x96 0x4B 0x00) Graphics.black
      (string_of_int gold)

  (* ### TODO: Part 3 Actions ### *)
  method! smells_like_gold = if gold = 0 then None else Some gold_id

  method! forfeit_gold =
    let result = ref None in
    if gold > 0 then
      Helpers.with_inv_probability World.rand forfeit_gold_probability
        begin fun () ->
          gold <- gold - 1 ;
          result := Some gold_id
        end ;
    !result

  (* ### TODO: Part 4 Aging ### *)
  method! receive_gold ps =
    if List.filter ~f:((<>) gold_id) ps <> [] then self#reset_life ;
    ps

end
