open Core.Std
open Event51
open WorldObject
open WorldObjectI

(* ### Part 6 Custom Events ### *)
let spawn_dragon_gold = 200

(** Dany will spawn a dragon when King's Lnading has collected a certain
    amount of gold. *)
class dany p (kings_landing : KingsLanding.kings_landing) : world_object_i =
object (self)
  inherit world_object p

  (******************************)
  (***** Instance Variables *****)
  (******************************)

  (* ### TODO: Part 6 Custom Events ### *)
  val mutable dragon_attacking = false

  (***********************)
  (***** Initializer *****)
  (***********************)

  (* ### TODO: Part 6 Custom Events ### *)
  initializer
    let spawn_condition gold =
      gold >= spawn_dragon_gold && not dragon_attacking
    in
    self#register_handler (Event51.filter spawn_condition kings_landing#get_gold_event)
                          (fun _ -> self#do_spawn_dragon)

  (**************************)
  (***** Event Handlers *****)
  (**************************)

  (* ### TODO: Part 6 Custom Events ### *)
  method private do_spawn_dragon =
    print_string "omg dragons! " ; flush_all () ;
    let dragon = new Dragon.dragon self#get_pos (self :> world_object_i)
      kings_landing in
    dragon_attacking <- true ;
    self#register_handler dragon#get_die_event
      (fun () -> dragon_attacking <- false)

  (********************************)
  (***** WorldObjectI Methods *****)
  (********************************)

  (* ### TODO: Part 1 Basic ### *)
  method! get_name = "dany"

  method! draw = self#draw_circle (Graphics.rgb 0x80 0x00 0x80) Graphics.white "D"

  (* ### TODO: Part 6 Custom Events *)
  method! receive_gold _ = []

end
