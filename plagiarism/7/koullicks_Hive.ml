open WorldObject
open World
open WorldObjectI
open Bee
open BeeRandom
open BeeBouncy

(* ### Part 3 Actions ### *)
let starting_pollen = 500
let cost_of_bee = 10
let spawn_probability = 20
let pollen_probability = 50
let max_pollen_deposit = 3

(** A hive will spawn bees and serve as a deposit point for the pollen that bees
    harvest.  It is possible to steal honey from a hive, however the hive will
    signal that it is in danger and its loyal bees will become angry. *)
class hive p :
object
 inherit world_object_t
 method forfeit_honey : int -> world_object_i -> int
end =
object(self)
 inherit world_object p as super

  (******************************)
  (***** Instance Variables *****)
  (******************************)

  (* ### TODO: Part 3 Actions ### *)
  val mutable honey = starting_pollen

  (* ### TODO: Part 6 Custom Events ### *)

  (***********************)
  (***** Initializer *****)
  (***********************)

  initializer
  ignore(self#register_handler ( World.action_event) (fun () -> self#do_action))  

  (**************************)
  (***** Event Handlers *****)
  (**************************)

  method private do_action = 
  let p = World.rand pollen_probability in (if (p < 1) then honey <- honey + 1) ; self#generate_bee ()
  
  method receive_pollen pollen_list =
    let amount = List.length pollen_list in
    match (amount > max_pollen_deposit) with
    | true -> (honey <- honey + max_pollen_deposit) ; self#draw ; []
    | false -> (honey <- honey + amount) ; self#draw ; []
  
  (* ### TODO: Part 4 Aging ### *)

  (**************************)
  (***** Helper Methods *****)
  (**************************)

  method private generate_bee () = 
  if ((World.rand spawn_probability = 0) && (honey >= cost_of_bee))
  then (honey <- honey - cost_of_bee ; if (World.rand 2 = 0) 
  then ignore (new bee_bouncy p (self :> world_object_i))
  else ignore (new bee_random p (self :> world_object_i)))
  else ()

  (* ### TODO: Part 5 Smart Bees ### *)

  (****************************)
  (*** WorldObjectI Methods ***)
  (****************************)

  method get_name = "hive"
  method draw = super#draw_circle Graphics.cyan Graphics.black (string_of_int honey)
  
  method forfeit_honey n stealer = 
  match (n > honey) with
  | true -> let n = honey in ((honey <- 0) ; (self#danger stealer); n)
  | false -> ((honey <- honey - n) ; (self#danger stealer); n)
  

  (* ### TODO: Part 3 Actions ### *)

  (* ### TODO: Part 6 Custom Events ### *)

  (************************)
  (***** Hive Methods *****)
  (************************)

  (* ### TODO: Part 3 Actions ### *)


  (* ### TODO: Part 6 Custom Events ### *)
  
end
