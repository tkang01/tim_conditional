open Direction
open WorldObject
open World
open Event

(** Class type for objects which constantly try to move in a calculated next
    direction. *)
class type movable_t =
object
  inherit world_object_t

  (** The next direction for which this object should move. *)
  method next_direction : Direction.direction option
end

class movable p (inv_speed:int option) : movable_t =
object (self)
  
  inherit world_object p as super

  (***********************)
  (***** Initializer *****)
  (***********************)
  
  initializer
  ignore(self#register_handler ( World.move_event) (fun () -> self#do_move))

  (**************************)
  (***** Event Handlers *****)
  (**************************)
  
  val speed =
  match inv_speed with
  | None -> 0
  | Some a -> a

  val mutable counter =
  match inv_speed with
  | None -> ref 0
  | Some a -> ref a
  
  method private do_move = if (!counter = 0 && not(inv_speed = None)) then 
  (super#move self#next_direction; counter := speed)
  else (counter := !counter - 1)

  (* ### TODO: Part 2 Movement ### *)

  (***************************)
  (***** Movable Methods *****)
  (***************************)

  (* ### TODO: Part 2 Movement ### *)
  method next_direction = Some(Direction.East)

end 
