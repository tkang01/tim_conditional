open WorldObject
open Bee
open Direction
open World

(** Bouncy bees will travel in a straight line in a random direction until an
    obstacle or edge of the world is reached, at which point a new random
    direction will be chosen. *)
class bee_bouncy p hive : bee_t =
object (self)
    inherit bee p hive as super

  (******************************)
  (***** Instance Variables *****)
  (******************************)

  val mutable direction = Some(Direction.random (World .rand))

  (********************************)
  (***** WorldObjectI Methods *****)
  (********************************)

  (* ### TODO: Part 5 Smart Bees *)
  method next_direction_default = 
  if (World.can_move (Direction.move_point self#get_pos direction)) then direction
  else let a = Some( Direction.random (World .rand)) in ((direction <- a) ; a)
  
  method get_name = "bee_bouncy"

  (***********************)
  (***** Bee Methods *****)
  (***********************)

  (* ### TODO: Part 5 Smart Bees *)

end


