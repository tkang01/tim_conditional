
open Ast ;;
open ExpressionLibrary ;;

(* TIPS FOR PROBLEM 2:
 * 1. Read the writeup.
 * 2. Use the type definitions in the ast.ml as a reference. But don't worry
 *    about expressionLibrary.ml
 * 3. Remember to test using the function "assert".
 *)

(*>* Problem 2.1 *>*)

(* contains_var : tests whether an expression contains a variable "x"
 *     Examples : contains_var (parse "x^4") = true
 *                contains_var (parse "4+3") = false *)

let rec contains_var (e:expression) : bool =
    match e with
    | Num _ -> false
    | Var -> true
    | Binop (_,e1,e2) -> contains_var e1 || contains_var e2
    | Unop (_,e1) -> contains_var e1
;;

assert (contains_var (parse "x^4") = true) ;;
assert (contains_var (parse "4+3") = false) ;;


(*>* Problem 2.2 *>*)

(* evaluate : evaluates an expression for a particular value of x. Don't
 *            worry about handling 'divide by zero' errors.
 *  Example : evaluate (parse "x^4 + 3") 2.0 = 19.0 *)
let rec evaluate (e:expression) (x:float) : float =
<<<<<<< HEAD
    match e with
    | Num num -> num
    | Var -> x
    | Binop (bop,e1,e2) -> 
        (match bop with
        | Add -> (evaluate e1 x) +. (evaluate e2 x)
        | Sub -> (evaluate e1 x) -. (evaluate e2 x)
        | Mul -> (evaluate e1 x) *. (evaluate e2 x)
        | Div -> (evaluate e1 x) /. (evaluate e2 x)
        | Pow -> (evaluate e1 x) ** (evaluate e2 x))
    | Unop (uop,e1) -> 
        (match uop with
        | Sin -> sin (evaluate e1 x)
        | Cos -> cos (evaluate e1 x)
        | Ln ->  log (evaluate e1 x)
        | Neg -> -. (evaluate e1 x))
;;

assert (evaluate (parse "x^4 + 3") 2.0 = 19.0) ;;
assert (evaluate (parse "x") 1.0 = 1.0) ;;
=======
    raise (Failure "Not implemented")
;;


>>>>>>> 88fa670f235344f0951be37d90fc024b7ee16ef6

(*>* Problem 2.3 *>*)

(* See writeup for instructions. We have pictures! *)
let rec derivative (e:expression) : expression =
    match e with
    | Num _ -> Num 0.
<<<<<<< HEAD
    | Var -> Num 1.0
    | Unop (u,e1) ->
        (match u with
        | Sin -> Binop(Mul,Unop(Cos,e1),derivative e1)
        | Cos -> Binop(Mul,Unop(Neg,Unop(Sin,e1)),derivative e1)
        | Ln ->  Binop(Div,derivative e1, e1)
=======
    | Var -> raise (Failure "Not implemented")
    | Unop (u,e1) ->
        (match u with
        | Sin -> raise (Failure "Not implemented")
        | Cos -> Binop(Mul,Unop(Neg,Unop(Sin,e1)),derivative e1)
        | Ln -> raise (Failure "Not implemented")
>>>>>>> 88fa670f235344f0951be37d90fc024b7ee16ef6
        | Neg -> Unop(Neg,derivative e1))
    | Binop (b,e1,e2) ->
        match b with
        | Add -> Binop(Add,derivative e1,derivative e2)
        | Sub -> Binop(Sub,derivative e1,derivative e2)
        | Mul -> Binop(Add,Binop(Mul,e1,derivative e2),
                        Binop(Mul,derivative e1,e2))
<<<<<<< HEAD
        | Div -> Binop(Div, Binop(Sub,Binop(Mul,e1,derivative e2),
                        Binop(Mul,derivative e1,e2)), Binop(Pow, e2, Num 2.0))
        | Pow ->
            if contains_var e2
            then Binop(Mul,Binop(Pow, e1, e2),Binop(Add, Binop(Mul, derivative e2, Unop(Ln, e1)), Binop(Div,Binop(Mul, derivative e1, e2),e1)))
            else Binop(Mul,Binop(Mul, e2, derivative e1),Binop(Pow, e1, Binop(Sub,e2, Num 1.0)))
;;

assert (evaluate (derivative (parse "x")) 2. = 1.);;
assert (evaluate (derivative (parse "x ^ 2")) 2. = 4.);;
assert (evaluate (derivative (parse "ln x")) 2. = 0.5);;
assert (evaluate (derivative (parse "x ^ 2")) 2. = 4.);;
assert (evaluate (derivative (parse "sin x")) 2. = cos 2.);;

=======
        | Div -> raise (Failure "Not implemented")
        | Pow ->
            if raise (Failure "Not implemented")
            then raise (Failure "Not implemented")
            else raise (Failure "Not implemented")
;;

>>>>>>> 88fa670f235344f0951be37d90fc024b7ee16ef6
(* A helpful function for testing. See the writeup. *)
let checkexp strs xval =
    print_string ("Checking expression: " ^ strs ^ "\n");
    let parsed = parse strs in (
        print_string "contains variable : ";
        print_string (string_of_bool (contains_var parsed));
        print_endline " ";
        print_string "Result of evaluation: ";
        print_float (evaluate parsed xval);
        print_endline " ";
        print_string "Result of derivative: ";
        print_endline " ";
        print_string (to_string (derivative parsed));
        print_endline " "
    )
;;

<<<<<<< HEAD
checkexp "3 * x" 2.0;;
checkexp "cos x" 2.0;;
checkexp "x^2" 2.0;;
checkexp "x" 2.0;;
=======
>>>>>>> 88fa670f235344f0951be37d90fc024b7ee16ef6

(*>* Problem 2.4 *>*)

(* See writeup for instructions. *)
<<<<<<< HEAD
let rec find_zero (e:expression) (g:float) (epsilon:float) (lim:int): float option =
    match lim with
    | 0 -> None
    | _ -> 
        if abs_float (evaluate e g) < epsilon 
        then Some g
        else find_zero e (g -. ((evaluate(derivative e) g)/.evaluate e g)) epsilon (lim - 1)
;;
assert (find_zero (parse "x") 1.0 0.1 50 = Some 0.);;
assert ((find_zero (parse "sin x") 3.0 0.05 15) = None);;
=======
let rec find_zero (e:expression) (g:float) (epsilon:float) (lim:int)
    : float option =
    raise (Failure "Not implemented")
;;

>>>>>>> 88fa670f235344f0951be37d90fc024b7ee16ef6


(*>* Problem 2.5 *>*)

(* Karma problem:
 * Just leave it unimplemented if you don't want to do it.
 * See writeup for instructions. *)
<<<<<<< HEAD


(* a function to test if an expression contains a mul/div operator, used for distributing expredssions *)
let rec contains_mul (e:expression) : bool =
    match e with
    | Binop (bop,e1,e2) -> 
        (match bop with
        | Add | Sub-> (contains_mul e1) || (contains_mul e2)
        | Mul | Div-> true
        | Pow -> false)
    | Num _ | Var | Unop _ -> false
;;

assert (contains_mul (parse "x + 4") = false) ;;
assert (contains_mul (parse "x * 1") = true) ;;
assert (contains_mul (parse "x * 1 + 1") = true) ;;

(* expand expression like 3 * (x + 1) *)
let rec expand (e:expression) : expression =
    match e with
    | Binop (bop,e1,e2) -> 
        (match bop with
        | Add -> Binop(Add, expand e1, expand e2)
        | Sub -> Binop(Sub, expand e1, expand e2)
        | Mul -> 
            if (not (contains_var e1)) && (contains_var e2)
            then (match e2 with
                | Binop (bop,e3,e4) -> Binop(Add, Binop(Mul, e1, expand e3), Binop(Mul, e1, expand e4)) 
                | Unop (uop,e5) -> Unop (uop,e5)
                | Var -> Var
                | Num num -> Num num)
            else (match e1 with
                | Binop (bop,e3,e4) -> Binop(Add, Binop(Mul, e2, expand e3), Binop(Mul, e2, expand e4)) 
                | Unop (uop,e5) -> Unop (uop,e5)
                | Var -> Var
                | Num num -> Num num)
             
        | Div -> Binop (bop,e1,e2)
        | Pow -> Binop (bop,e1,e2))
    | Num num -> Num num
    | Var -> Var
    | Unop (uop,e1) -> Unop (uop,e1) 
;;

(* crawl through an expression and redistribute the expression *)
let rec crawl (e:expression) : expression =
    match e with
    | Binop (bop,e1,e2) -> 
        (match bop with
        | Add -> 
            if (not (contains_mul e1)) || (contains_mul e2)
            then Binop(Add, crawl e2, crawl e1)
            else Binop(Add, crawl e1, crawl e2)
        | Sub -> 
            if (not (contains_mul e1)) || (contains_mul e2)
            then Binop(Sub, crawl e2, crawl e1)
            else Binop(Sub, crawl e1, crawl e2)
        | Mul -> Binop (bop,e1,e2)
        | Div -> Binop (bop,e1,e2)
        | Pow -> Binop (bop,e1,e2))
    | Num num -> Num num
    | Var -> Var
    | Unop (uop,e1) -> Unop (uop,e1) 
;;

let rec find_zero_exact (e:expression) : expression option =
    raise (Failure "Not implemented") 
=======
let rec find_zero_exact (e:expression) : expression option =
    raise (Failure "Not implemented")
>>>>>>> 88fa670f235344f0951be37d90fc024b7ee16ef6
;;


(*>* Problem 2.6 *>*)

<<<<<<< HEAD
let minutes_spent_on_part_2 : int = 90 ;;
=======
let minutes_spent_on_part_2 : int = raise (Failure "Not implemented") ;;
>>>>>>> 88fa670f235344f0951be37d90fc024b7ee16ef6
