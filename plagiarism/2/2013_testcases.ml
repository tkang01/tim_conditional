open Expression

(* e is solution's expression, e' is student's.
   Evaluates e and e' at -100, -99, ..., 99, 100, and
   returns the number of differences. *)
let check e e' =
  let max = 100. in
  let tol = 0.0000000001 in
  let testError a b = 
    if (a=0.) then abs b > tol else abs(a-.b)/.abs(a) > tol
  in
  (* Returns true if there is an error *)
  let rec helper cur = 
    if (cur > max) then 0 else 
    (if (testError (evaluate e cur) (evaluate e' cur)) then 1 else 0) +
      helper (cur+.1.)
  in
    helper (-. max);;

containsVar(parseString "x");; (* true *)
containsVar(parseString "sin(x)");; (* true *)
containsVar(parseString "5*(4+sin(3*x^5))");; (* true *)
containsVar(parseString "10");; (* false *)
containsVar(parseString "10*4*sin(8)+cos(1)");; (* false *)

evaluate (parseString "5/2+4*2^4-1") 0.;; (* 65.5 *)
evaluate (parseString "5/2+4*2^4-1") 5.;; (* 65.5 *)
evaluate (parseString "x") 37.;; (* 37. *)
evaluate (parseString "sin(x)+(cos(x)*~4)/(x^2)") 5.;; (* -1.0043102243372546 *)
evaluate (parseString "x^(~2)") 2.;; (* 0.25 *)
evaluate (parseString "x^.25") 256.;; (* 4. *)
evaluate (parseString "ln(x)+x") 10.;; (* 12.302585092994047 *)

differentiate(parseString "x");; (* Num 1. *)
differentiate(parseString "7");; (* Num 0. *)
differentiate(parseString "x^2");; (* BinOp (Mul, Num 2.,
 BinOp (Mul, Num 1., BinOp (Pow, Var, BinOp (Sub, Num 2., Num 1.)))) *)
differentiate(parseString "sin(x)");; (* BinOp (Mul, UnOp (Cos, Var), Num 1.) *)
differentiate(parseString "ln(x*cos(x))");; (* BinOp (Div,
 BinOp (Add,
  BinOp (Mul, Var, BinOp (Mul, UnOp (Neg, UnOp (Sin, Var)), Num 1.)),
  BinOp (Mul, Num 1., UnOp (Cos, Var))),
 BinOp (Mul, Var, UnOp (Cos, Var))) *)
differentiate(parseString "x^(2*x)");; 
(* BinOp (Mul, BinOp (Pow, Var, BinOp (Mul, Num 2., Var)),
 BinOp (Add,
  BinOp (Mul,
   BinOp (Add, BinOp (Mul, Num 2., Num 1.), BinOp (Mul, Num 0., Var)),
   UnOp (Ln, Var)),
  BinOp (Div, BinOp (Mul, Num 1., BinOp (Mul, Num 2., Var)), Var))) *)
differentiate(parseString "x^2-x");; (* BinOp (Sub,
 BinOp (Mul, Num 2.,
  BinOp (Mul, Num 1., BinOp (Pow, Var, BinOp (Sub, Num 2., Num 1.)))),
 Num 1.) *)
differentiate(parseString "sin(x)^3+x*x-1/(5*x)");;
(* BinOp (Sub,
 BinOp (Add,
  BinOp (Mul, Num 3.,
   BinOp (Mul, BinOp (Mul, UnOp (Cos, Var), Num 1.),
    BinOp (Pow, UnOp (Sin, Var), BinOp (Sub, Num 3., Num 1.)))),
  BinOp (Add, BinOp (Mul, Var, Num 1.), BinOp (Mul, Num 1., Var))),
 BinOp (Div,
  BinOp (Sub, BinOp (Mul, Num 0., BinOp (Mul, Num 5., Var)),
   BinOp (Mul, Num 1.,
    BinOp (Add, BinOp (Mul, Num 5., Num 1.), BinOp (Mul, Num 0., Var)))),
  BinOp (Pow, BinOp (Mul, Num 5., Var), Num 2.))) *)

toStringSmart(differentiate(parseString "x"));; (* "1." *)
toStringSmart(differentiate(parseString "7"));; (* "0." *)
toStringSmart(differentiate(parseString "x^2"));; (* "2.*1.*x^(2.-1.)" *)
toStringSmart(differentiate(parseString "sin(x)"));; (* "cos(x)*1." *)
toStringSmart(differentiate(parseString "ln(x*cos(x))"));; 
        (* "(x*~(sin(x))*1.+1.*cos(x))/x*cos(x)" *)
toStringSmart(differentiate(parseString "x^(2*x)"));;
        (* "x^(2.*x)*((2.*1.+0.*x)*ln(x)+1.*2.*x/x)" *)
toStringSmart(differentiate(parseString "x^2-x"));; (* "2.*1.*x^(2.-1.)-1." *)
toStringSmart(differentiate(parseString "sin(x)^3+x*x-1/(5*x)"));;
(* "3.*cos(x)*1.*sin(x)^(3.-1.)+x*1.+1.*x-(0.*5.*x-1.*(5.*1.+0.*x))/(5.*x)^2."*)

toStringSmart(reduce(parseString "2.*1.*x^(2.-1.)"));; (* "2.*x" *)
toStringSmart(reduce(parseString "~1*sin(x)"));; (* "~(sin(x))" *)
toStringSmart(reduce(parseString "~(5*2)/10*sin(x)"));; (* "~(sin(x))" *)
toStringSmart(reduce(parseString "(cos(x)^x)^(1/x)"));; (* "cos(x)" *)
toStringSmart(reduce(parseString "(x/x)*100+~100/1+ln(1)^500"));; (* "0." *)
toStringSmart(reduce(parseString "x^~1*x"));; (* "x" *)
toStringSmart(reduce(parseString "(sin(x)*1)*(1/sin(x))+1/x*(3-4)*x"));;(*"0."*)
