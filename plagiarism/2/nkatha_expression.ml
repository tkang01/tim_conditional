
open Ast ;;
open ExpressionLibrary ;;

(* TIPS FOR PROBLEM 2:
 * 1. Read the writeup.
 * 2. Use the type definitions in the ast.ml as a reference. But don't worry
 *    about expressionLibrary.ml
 * 3. Remember to test using the function "assert".
 *)

(*>* Problem 2.1 *>*)

(* contains_var : tests whether an expression contains a variable "x"
 *     Examples : contains_var (parse "x^4") = true
 *                contains_var (parse "4+3") = false *)
let rec contains_var (e:expression) : bool =
match e with
|Num _ ->false
|Var -> true
|Binop(bi, e1, e2)->contains_var e1|| contains_var e2
|Unop(u, e3) -> contains_var e3
;;

(*test*)
assert ((contains_var (parse "x^5")) = true);;
assert ((contains_var (parse "100^7")) = false);;



(*>* Problem 2.2 *>*)

(* evaluate : evaluates an expression for a particular value of x. Don't
 *            worry about handling 'divide by zero' errors.
 *  Example : evaluate (parse "x^4 + 3") 2.0 = 19.0 *)
 let rec evaluate (e:expression) (x:float) : float =
 match e with
 |Num q -> q
 |Var-> x
 |Binop(b,e1, e2)->
   let p = evaluate e1 x in
   let q= evaluate e2 x in
    (match b with
    |Add-> ( p+.q)
    |Sub-> (p-. q)
    |Mul-> (p *.q)
    |Div-> (p /.q)
    |Pow->  (p**q))
 |Unop (u, e3)->
    (match u with
    |Sin->  evaluate  e3 x
    |Cos-> evaluate  e3 x
    |Ln->  evaluate e3 x
    |Neg->  evaluate e3 x)
;;

(*test*)
assert ((evaluate (parse "x^2 + 3") 11.0) = 124.0);;
assert ((evaluate (parse "x^3 + 3") 3.0) = 30.0);;

(*>* Problem 2.3 *>*)

(* See writeup for instructions. We have pictures! *)
let rec derivative (e:expression) : expression =
    match e with
    | Num _ -> Num 0.0
    | Var -> Num  1.0
    | Unop (u,e1) ->
        (match u with
        | Sin -> Binop(Mul, Unop(Cos, e1), derivative e1)
        | Cos -> Binop(Mul,Unop(Neg,Unop(Sin,e1)),derivative e1)
        | Ln -> Binop(Mul, Binop(Div,Num 1., e1), derivative e1)
        | Neg -> Unop(Neg,derivative e1))
    | Binop (b,e1,e2) ->
        match b with
        | Add -> Binop(Add,derivative e1,derivative e2)
        | Sub -> Binop(Sub,derivative e1,derivative e2)
        | Mul -> Binop(Add,Binop(Mul,e1,derivative e2),
                        Binop(Mul,derivative e1,e2))
        | Div -> Binop(Div, Binop(Sub, Binop(Mul, derivative e1, e2),
                 Binop(Mul, derivative e2, e1)),
                 Binop(Pow, e2, Num(2.)))
        | Pow ->
                if contains_var e2 then  Binop(Mul, (Binop(Mul,
                 Binop(Pow, e1, e2))),(Binop(Add, Binop(Mul, derivative e1, 
                 Unop(Ln,e1)), Binop(Div,Binop(Mul, derivative e1, e2),e1))))
            else Binop(Mul, e2, derivative e1, 
            Binop(Pow, e1, Binop(Sub, e2, Num(1.))))
;;

(* A helpful function for testing. See the writeup. *)
let checkexp strs xval =
    print_string ("Checking expression: " ^ strs ^ "\n");
    let parsed = parse strs in (
        print_string "contains variable : ";
        print_string (string_of_bool (contains_var parsed));
        print_endline " ";
        print_string "Result of evaluation: ";
        print_float (evaluate parsed xval);
        print_endline " ";
        print_string "Result of derivative: ";
        print_endline " ";
        print_string (to_string (derivative parsed));
        print_endline " "
    )
;;


(*>* Problem 2.4 *>*)

(* See writeup for instructions. *)
let rec find_zero (e:expression) (g:float) (epsilon:float) (lim:int)
 : float option = 
  let x1 = g -. ((evaluate e g)/. (evaluate(derivative e)g)) in
  if evaluate (e g) > epsilon then find_zero e x1 epsilon (lim-1)
  else if evaluate (e g) > epsilon && lim = 0 then None
     else
     Some g
  
;;



(*>* Problem 2.5 *>*)
(* Karma problem:
 * Just leave it unimplemented if you don't want to do it.
 * See writeup for instructions. *)
let rec find_zero_exact (e:expression) : expression option =
    raise (Failure "Not implemented")
;;


(*>* Problem 2.6 *>*)

let minutes_spent_on_part_2 : int = 8 ;;
