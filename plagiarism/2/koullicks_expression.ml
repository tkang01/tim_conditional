open Ast ;;

open ExpressionLibrary ;;

(* TIPS FOR PROBLEM 2:
 * 1. Read the writeup.c
 * 2. Use the type definitions in the ast.ml as a reference. But don't worry 
 *    about expressionlibrary.ml
 * 3. Remember to test using the function "assert".
 *)

(*>* Problem 2.1 *>*)

(* contains_var : tests whether an expression contains a variable "x"
 *     Examples : contains_var (parse "x^4") = true
 *                contains_var (parse "4+3") = false *)
let rec contains_var (e:expression) : bool =
  match e with
    | Var -> true
    | Binop (_, a, b) -> if (contains_var a = false) then contains_var b else true
    | Unop ( _ , c ) -> contains_var c
    | _ -> false
;;

assert (contains_var (parse "2 ^ x") = true) ;;
assert (contains_var (parse "2+(2 ^ x)") = true) ;;
assert (contains_var (parse "2-(2 * 7) / 4") = false) ;;
assert (contains_var (parse "2") = false) ;;

(*>* Problem 2.2 *>*)

(* evaluate : evaluates an expression for a particular value of x. Use OCaml's
 *            built in method of handling 'divide by zero' errors.
 *  Example : evaluate (parse "x^4 + 3") 2.0 = 19.0 *)
let rec evaluate (e:expression) (x:float) : float =
  match e with
    | Num a -> a
    | Var -> x
    | Binop (op, a, b) -> (match op with
                           | Add -> (evaluate a x) +. (evaluate b x)
                           | Sub -> (evaluate a x) -. (evaluate b x)
                           | Mul -> (evaluate a x) *. (evaluate b x)
                           | Div -> (evaluate a x) /. (evaluate b x)
                           | Pow -> (evaluate a x) ** (evaluate b x)
                           )
    | Unop (op, a) -> match op with
                        | Sin -> sin (evaluate a x)
                        | Cos -> cos (evaluate a x)
                        | Ln -> log (evaluate a x)
                        | Neg -> (-1.) *. (evaluate a x)
;;

assert (evaluate (parse "x^4 + (x-1) - x + 14 * x" ) 4. = 311.) ;;
assert (evaluate (parse "x - 1 - (x-1) - x +  x" ) 4. = 0.) ;;


(*>* Problem 2.3 *>*)

(* See writeup for instructions. We have pictures! *)
let rec derivative (e:expression) : expression =
  match e with
    | Num _ -> Num 0.
    | Var -> Num 1.
    | Unop (u,e1) ->
        (match u with
           | Sin -> Binop(Mul,Unop(Cos,e1),derivative e1)
           | Cos -> Binop(Mul,Unop(Neg,Unop(Sin,e1)),derivative e1)
           | Ln -> Binop(Mul, Binop(Div, Num 1., e1), derivative e1)
           | Neg -> Unop(Neg,derivative e1)
	)
    | Binop (b,e1,e2) ->
        match b with
          | Add -> Binop(Add,derivative e1,derivative e2)
          | Sub -> Binop(Sub,derivative e1,derivative e2)
          | Mul -> Binop(Add,Binop(Mul,e1,derivative e2),
                         Binop(Mul,derivative e1,e2))
          | Div -> Binop(Div, Binop(Sub, Binop(Mul, derivative e1, e2) , Binop(Mul, e1, derivative e2)) , Binop(Pow, e2, Num 2.))
          | Pow -> 
              if (contains_var e2) then
		Binop(Mul, Binop(Pow, e1, e2), Binop(Add, Binop(Mul, derivative e2, Unop(Ln, e1)), Binop(Div, Binop(Mul, derivative e1, e2), e1)))
	      else Binop(Mul, e2, Binop(Mul, derivative e1, Binop(Pow, e1, Binop(Sub, e2, Num 1.))))
;;

assert(derivative (parse "x^2") = Binop (Mul, Num 2.,
 Binop (Mul, Num 1., Binop (Pow, Var, Binop (Sub, Num 2., Num 1.))))) ;; (* this is bad form, but correct *)

assert(derivative (parse "1") = Num 0.) ;;

(* A helpful function for testing. See the writeup. *)
let checkexp strs xval=
  print_string ("Checking expression: " ^ strs^"\n");
  let parsed = parse strs in (
        print_string "contains variable : ";
	print_string (string_of_bool (contains_var parsed));
	print_endline " ";
	print_string "Result of evaluation: ";
	print_float  (evaluate parsed xval);
	print_endline " ";
	print_string "Result of derivative: ";
	print_endline " ";
	print_string (to_string (derivative parsed));
	print_endline " ")
	
;;


(*>* Problem 2.4 *>*)

(* the absolute value of a float *)
let abs_float f =
  if (f < 0.) then (-1.) *. f else f
;;

assert (abs_float 3. = 3.);;
assert (abs_float (-3.) = 3.);;

(* See writeup for instructions. *)
let rec find_zero (e:expression) (g:float) (epsilon:float) (lim:int)
    : float option =
  if (lim < 0) then None else
  if ((abs_float (evaluate e g)) < epsilon) then Some g else 
    let g_next = g -. ((evaluate e g) /. (evaluate (derivative e) g)) in
    find_zero e g_next epsilon (lim - 1)
 ;;

assert (find_zero (parse "x") 5. 0.1 1000 = Some 0.) ;;
assert (find_zero (parse "2") 2. 1. 1000 = None) ;;

(*>* Problem 2.5 *>*)

(* Karma problem:
 * Just leave it unimplemented if you don't want to do it.
 * See writeup for instructions. *)
let rec find_zero_exact (e:expression) : expression option =
  None (* Simply evaluate the quadratic formula *)
;;


(*>* Problem 2.6 *>*)

(* not including the Karma problem *)
let minutes_spent_on_part_2 : int = 70

;;


