(* Interfaces and implementations of dictionaries.  A dictionary
 * is used to associate a value with a key.  In our case, we will
 * be using a dictionary to build an index for the web, associating
 * a set of URLs with each word that we find as we crawl the web.
 *)
module type DICT = 
sig
  type key   
  type value 
  type dict
  val empty : dict 
  val insert : dict -> key -> value -> dict
  val lookup : dict -> key -> value option
  val remove : dict -> key -> dict
  val member : dict -> key -> bool

  (* Return an arbitrary key, value pair along with a new dict with that
   * pair removed.  Return None if the input dict is empty *)
  val choose : dict -> (key * value * dict) option
  val fold : (key -> value -> 'a -> 'a) -> 'a -> dict -> 'a
  val string_of_key: key -> string
  val string_of_value : value -> string
  val string_of_dict : dict -> string
end

(* Arguments to the AssocListDict functor *)
module type DICT_ARG = 
sig
  type key
  type value
  val compare : key -> key -> Order.order
  val string_of_key: key -> string
  val string_of_value : value -> string
end

(* An association list implementation of dictionaries.  *)
module AssocListDict(D:DICT_ARG) : (DICT with type key = D.key
with type value = D.value) = 
struct
  open Order;;
  type key = D.key;;
  type value = D.value;;
  (* invariant: sorted by key, no duplicates *)
  type dict = (key * value) list;;
  let empty = [] ;;
  let rec insert d k v = 
    match d with 
      | [] -> [(k,v)]
      | (k1,v1)::d1 -> (match D.compare k k1 with 
                          | Less -> (k,v)::d
                          | Eq -> (k,v)::d1
                          | Greater -> (k1,v1)::(insert d1 k v))

  let rec lookup d k = 
    match d with 
      | [] -> None
      | (k1,v1)::d1 -> (match D.compare k k1 with
                          | Eq -> Some v1
                          | Greater -> lookup d1 k 
                          | _ -> None)

  let member d k = match lookup d k with | None -> false | Some _ -> true
    
  let rec remove d k = 
    match d with 
      | [] -> []
      | (k1,v1)::d1 ->
	      (match D.compare k k1 with 
             | Eq -> d1
             | Greater -> (k1,v1)::(remove d1 k)
             | _ -> d)
	        
  let choose d = 
    match d with 
      | [] -> None
      | (k,v)::rest -> Some(k,v,rest)
          
  let fold f d = List.fold_left (fun a (k,v) -> f k v a) d 

  let string_of_key = D.string_of_key
  let string_of_value = D.string_of_value
  let string_of_dict (d: dict) : string = 
    let f = (fun y (k,v) -> y ^ "\n key: " ^ D.string_of_key k ^ 
               "; value: (" ^ D.string_of_value v ^ ")") in
      List.fold_left f "" d

end    

(* Testing example (not comrehensive!) *)
module TestAssocListDicts = 
struct
  module D = AssocListDict(
    struct 
      type key = int
      type value = int
      let compare = Order.int_compare
      let string_of_key =  string_of_int
      let string_of_value = string_of_int
    end)

  let d = List.fold_right (fun (a,b) d -> D.insert d a b) 
    [(1,2); (2,7)] D.empty;;

  assert (D.lookup d 1 = Some 2);;
  assert (D.lookup d 2 = Some 7);;
  assert (D.lookup d 3 = None);;

  let d2 = D.remove d 2;;
  assert (D.lookup d2 2 = None);;

  let d3 = D.remove d 3;;
  assert (D.lookup d3 2 = Some 7);;
end 

(************************* PART 2 *********************************)

(******************************************************************)
(* BTDict: a functor that implements our DICT signature           *)
(* using a binary search tree.                                    *)
(******************************************************************)

module BTDict(D:DICT_ARG) : (DICT with type key = D.key
with type value = D.value) =
struct
  type key = D.key
  type value = D.value 
        
  type dict = 
    | Empty 
    | Branch of key * value * dict * dict
      
  let empty : dict = Empty
    
  let rec insert (d:dict) (k:key) (v:value) : dict = 
    match d with 
      | Empty -> Branch (k,v,Empty,Empty)
      | Branch (kk,vv,l,r) ->
	match D.compare k kk with
	  | Order.Less -> Branch(kk,vv,insert l k v,r)
	  | Order.Greater -> Branch(kk,vv,l,insert r k v)
	  | Order.Eq -> Branch (k,v,l,r)
      
  let rec lookup (d:dict) (k:key) : value option = 
    match d with 
      | Empty -> None
      | Branch (kk,vv,l,r) ->
	match D.compare k kk with
	  | Order.Less -> lookup l k
	  | Order.Greater -> lookup r k
	  | Order.Eq -> Some vv
	    
  let member (d:dict) (k:key) : bool = 
    lookup d k <> None

  let rec remove_right k v l r : key * value * dict =
    match r with
      | Empty -> (k,v,l)
      | Branch (kk,vv,ll,rr) ->
	let (a,b,c) = remove_right kk vv ll rr in
	(a,b,Branch(k,v,l,c))

  let merge (l : dict) (r : dict) : dict =
    match l,r with 
      | Empty, _ -> r
      | _, Empty -> l
      | Branch(kk,vv,ll,rr),_ ->
	let (a,b,c) = remove_right kk vv ll rr in
	Branch(a,b,c,rr)
     
  let rec remove (l : dict) (r : dict) : dict =
    match l, r with
      | Empty, _ -> r
      | _, Empty -> l
      | Branch(k,v,ll,rr), _ -> Branch(k,v,ll,merge rr r)

  let rec remove (d:dict) (k:key) : dict = 
    match d with 
      | Empty -> Empty
      | Branch (kk,vv,l,r) ->
	match D.compare k kk with
	  | Order.Less -> Branch(kk,vv,remove l k, r)
	  | Order.Greater -> Branch(kk,vv,l,remove r k)
	  | Order.Eq -> merge l r
	      
  let choose (d:dict) : (key*value*dict) option = 
    match d with
      | Empty -> None
      | Branch (kk,vv,l,r) -> Some (kk,vv,merge l r)            

  let fold (f:key -> value -> 'a -> 'a) (u:'a) (d:dict) : 'a = 
    let rec recur d k a =
      match d with 
	| Empty -> k a
	| Branch (kk,vv,l,r) ->
	  recur l (recur r (f kk vv)) a
    in
    recur d (fun x -> x) u

  let string_of_key = D.string_of_key
  let string_of_value = D.string_of_value
    
  let list_of_tree t =
    fold (fun k v r -> (k,v)::r) [] t
      
  let string_of_dict (t: dict) : string = 
    let f = (fun y (k,v) -> y ^ "\n key: " ^ D.string_of_key k ^ 
               "; value: (" ^ D.string_of_value v ^ ")") in
      List.fold_left f "" (list_of_tree t)
end


(******************************************************************)
(* MapDict: a functor that implements our DICT signature          *)
(* using the Map module built into ocaml.                         *)
(* http://caml.inria.fr/pub/docs/manual-ocaml/libref/Map.html     *)
(******************************************************************)

module MapDict(D:DICT_ARG) : (DICT with type key = D.key
with type value = D.value) = 
struct
  module M = Map.Make (
    struct
      type t = D.key
      let compare l r = 
	    match D.compare l r with
	      | Order.Eq -> 0
	      | Order.Less -> -1
	      | Order.Greater -> 1
    end)

  type key = D.key
  type value = D.value
  type dict = D.value M.t



  let empty = M.empty
  let insert d k v = M.add k v d
  let lookup d k = 
    try 
      Some (M.find k d)
    with
      | Not_found -> None
  let remove d k = M.remove k d
  let member d k = M.mem k d

  (* The ocaml Map module didn't have a choose method in versions 
   * before 3.12.0.  To make this work on nice.fas and be O(1), need
   * this hack:
   *)
  exception PairExc of (D.key * D.value)

  let choose d = 
    try 
      M.fold (fun k v _ -> raise (PairExc (k,v))) d ();
      None
    with
      | PairExc(k,v) -> Some (k,v, remove d k)
          
  (***** End hack *****)

  let fold f a d = M.fold f d a 
  let string_of_key = D.string_of_key
  let string_of_value = D.string_of_value
  let string_of_dict = 
    let f k v acc =
      acc ^ "\n key: " ^ D.string_of_key k ^ 
        "; value: (" ^ D.string_of_value v ^ ")"
    in
      fold f ""

(* STUDENT WORK
   Part 1
*)
end

  

module Make (D:DICT_ARG) : 
  (DICT with type key = D.key
with type value = D.value) = 
  (* Change this line to change the map implementation *)
  AssocListDict(D)
    (* BTDict (D) *)
    (* MapDict (D) *)



