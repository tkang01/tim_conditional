(*** CS 51 Problem Set 1 ***)
(*** February 9, 2010 ***)
(*** Solution Set ***)


(*>* Problem 1a *>*)

let prob1a : (string * int) list = [("CS", 50); ("CS", 51)] ;;


(*>* Problem 1b *>*)

let prob1b : int = (fun (x, y) -> x * y) (3, 7);;


(*>* Problem 1c *>*)

let prob1c : 'a option * float option = (None, Some 123.0) ;;


(*>* Problem 1d *>*)

let prob1d : (float * (string * int) list) list = 
  [(42.0, [("ducks", 3); ("geese", 7)]) ] ;;

(*>* Problem 1e *>*)

let prob1e : (int * int -> int) * (float -> float -> float) * float =
  ((fun (x, y) -> x * y), (+.), 42.42)
;;


(*>* Problem 1f *>*)

let prob1f =
  let rec foo bar =
    match bar with
    | (a, (b, c)) :: xs -> a * (b + c + (foo xs))
    | _ -> 1
  in foo ([(5, (2, 3))] : (int * (int * int)) list)
;;


(*>* Problem 1g *>*)

let prob1g =
  let v = (32.0, 28.0) in
  let square x = x *. x in
  let squared_norm (w:float * float) : float =
    let (f1,f2) = w in square f1 +. square f2 in
  let d = sqrt (squared_norm v) in
  int_of_float d
;;


(*>* Problem 2 *>*)

let rec partition lst pivot =
  match lst with
    | [] -> ([], [])
    | x :: xs -> let (lt, gt) = partition xs pivot in
        if x < pivot then (x :: lt, gt) else (lt, x :: gt)
;;

(* Sample Tests *)
assert ((partition [1;2;3;4;5] 3) = ([1;2],[3;4;5])) ;;
assert ((partition [1;2;3;4;5] 1) = ([],[1;2;3;4;5])) ;;
assert ((partition [1;2;3;4;5] 6) = ([1;2;3;4;5],[])) ;;
assert ((partition [1;2;3;4;5] (-1)) = ([],[1;2;3;4;5])) ;;
assert ((partition [] 3) = ([],[])) ;;
assert ((partition [5;4;3;2;1] 3) = ([2;1],[5;4;3])) ;;
assert ((partition [5;4;3;2;1] 1) = ([],[5;4;3;2;1])) ;;
assert ((partition [5;4;3;2;1] 6) = ([5;4;3;2;1],[])) ;;
assert ((partition [5;4;3;2;1] (-1)) = ([],[5;4;3;2;1])) ;;
assert ((partition [-1] 0) = ([-1],[])) ;;
assert ((partition [1] 0) = ([],[1])) ;;
assert ((partition [-100] (-100)) = ([],[-100])) ;;
assert ((partition [0;-1;5;2;3;6;100;123;43] 17) = ([0;-1;5;2;3;6],[100;123;43])) ;;

(* An even shorter way is the following, which you should be able to follow
 * in a week or so *)
let partition' lst pivot =
  List.fold_right (fun x (lt, gt) ->
                     if x < pivot then (x :: lt, gt) else (lt, x :: gt))
    lst ([], []) 

(*>* Problem 3a *>*)

let rec reversed (l:int list) : bool =
  match l with
  | a :: ((b :: _) as l') -> a >= b && reversed l'
  | _ -> true
;;


(*>* Problem 3b *>*)

let rec merge (a:int list) (b:int list) : int list =
  match a, b with
  | (a1 :: a'), (b1 :: b') ->
      if a1 < b1 then (a1 :: merge a' b) else (b1 :: merge a b')
  | a, [] -> a
  | [], b -> b
;;

(* sample tests *)
assert ((merge [1;2;3] [4;5;6;7]) = [1;2;3;4;5;6;7]) ;;
assert ((merge [4;5;6;7] [1;2;3]) = [1;2;3;4;5;6;7]) ;;
assert ((merge [4;5;6;7] [1;2;3]) = [1;2;3;4;5;6;7]) ;;
assert ((merge [2;2;2;2] [1;2;3]) = [1;2;2;2;2;2;3]) ;;
assert ((merge [1;2] [1;2]) = [1;1;2;2]) ;;
assert ((merge [-1;2;3;100] [-1;5;1001]) = [-1;-1;2;3;5;100;1001]) ;;
assert ((merge [] []) = []) ;;
assert ((merge [1] []) = [1]) ;;
assert ((merge [] [-1]) = [-1]) ;;
assert ((merge [1] [-1]) = [-1;1]) ;;



(*>* Problem 3c *>*)

let rec unzip (l:(int * int) list) : int list * int list =
  match l with
  | (a, b) :: ps -> let (l1, l2) = unzip ps in (a :: l1, b :: l2)
  | [] -> ([], [])
;;


(*>* Problem 3d *>*)

let rec sum (l:float list) : float =
  match l with
  | x :: xs -> x +. sum xs
  | [] -> 0.0

let variance (l:float list) : float option =
  let n = List.length l in
  if n < 2 then None else Some
    (let nf = float n in
     let mean = sum l /. nf in
     let square x = x *. x in
     (1.0 /. (nf -. 1.0)) *. sum (List.map (fun x -> square (x -. mean)) l))
;;


(*>* Problem 3e *>*)

let few_divisors (n:int) (m:int) : bool =
  let rec count_divs_from (start:int) : int =
    if start > n then 0 else
      (if n mod start = 0 then 1 else 0) + count_divs_from (start + 1)
  in count_divs_from 1 < m
;;



(*>* Problem 3f *>*)

let rec interleave (n:int) (l:int list) : int list list =
  match l with
  | x :: xs -> (n :: x :: xs) :: List.map (fun l -> x :: l) (interleave n xs)
  | [] -> [[n]]

let rec permutations (l:int list) : int list list =
  match l with
  | x :: xs -> List.concat (List.map (interleave x) (permutations xs))
  | [] -> [[]]
;;
