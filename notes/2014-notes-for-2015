NOTES ON CS 51 (2014)
=====================

Book
----

This year, we switched to “Real World OCaml” as our required textbook.
We found it very useful as a reference. For example, students who wanted
to write a program that handled command-line arguments were able to
figure out how to do that by reading the chapter on that topic. I didn’t
assign reading from the book, and I think that was fine, but it might
still be useful to figure out how the book lines up with the syllabus.

One complication introduced by the book is that it depends on the Jane
St. Core library rather than the OCaml standard library. For that
reason, and because it’s an excellent library, we switched the entire
course and most course materials to use Core as well. Materials that we
neglected to switch, such as the lecture introducing ‘map’ and ‘fold’
(and I suspect some section notes) caused confusion. Because labeled
arguments aren’t especially difficult to understand, I’d prefer to
introduce them earlier, and then in lecture write versions of ‘map,’
etc., that match those in Core.List.

Problem Sets
------------

The problem sets seem to have stabilized this year—we didn’t encounter a
lot of bugs after they were distributed. Our biggest problem, where we
clearly performed the worst, was in grading. We returned some
assignments as late as four weeks after the students submitted them, and
many students were quite (justifiably, I’d say) upset by this. It may be
that I, as the instructor, needed to manage/cajole more. But I think
this was not merely a management problem, but that grading 300 (or 150)
problem sets every week is inherently difficult. and we probably need to
find a better way to do it. Whether this takes the form of more
automation, less through grading (sampling?), or self-grading of some
kind, I’m not sure, but I would not do it the same way again.

There were a few other mistakes worth not repeating:

 - Getting rid of different due dates for Extension was a not a good
   idea, because having things do at 5 PM on Friday is terrible for
   students who work full time.

 - Making PS7 Game of Thrones–themed was annoying for students who were
   unfamiliar with GoT; they said there were too many weird names to
   keep straight. So I think staying in a universally familiar domain is
   a good idea.

 - I think four late days was too many. It made them insufficiently
   valuable, and many students squandered them early in the term and
   then asked for extensions, which is what late days were intended to
   avoid. For PS7, the treatment of late days in partnerships was kind
   of a mess, since there were cases where one partner had to turn in
   the problem set and the other, with more late days, wanted to keep
   working, so in the future I would strongly caution students about
   this conflict before they choose partners. And I would go with fewer
   (or possibly no) late days.

Exams
-----

Instead of a midterm exam and a final, I gave two in-class exams, one
before the add-drop date, and the other near the end of term. Having the
last exam before the end was a strong disincentive to attend the last
few lecture.

Content
-------

I was quite satisfied, overall, with the content of the course. The one
portion that I think still needs some work is the treatment of
parallelism and concurrency at the end. Perhaps it’s valuable as a
shallow introduction to those topics, but without some exercises, I
don’t think much sinks in.

Wide variance in ability and experience means that pacing is a big
problem. Advanced sections and challenge problems are still a good idea.
A larger enrollment should make having advanced sections more practical.

Three other things struck me as especially confusing:

 - Tuareg mode encourages sending definitions to an OCaml toplevel in an
   arbitrary order, which means that students would often redefine types
   and then be surprised when operations defined on the old versions of
   the types no longer applied. The error message, which shows the same
   type name, but with different gensyms (or whatever OCaml does for
   type generativity), is not very enlightening.

 - More generally, students tended not to read error messages very
   carefully. Reading and interpreting error messages is a valuable
   skill that should be taught in lecture and/or section.

 - I was surprised at how confusing many students found pattern matching
   to be; many of them were having trouble with it for longer than I
   would have expected. I think many students would benefit from having
   pattern matching explained in detail.

Teaching Staff
--------------

Many TFs start the semester with fuzzy recall of CS 51 material, which
can be a problem. If I were teaching it again, I would try to have an
intensive one-day TF bootcamp. Beyond that, for the most part our TFs
were excellent this year.

