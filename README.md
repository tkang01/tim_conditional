# CS 51 Materials Repository

This is the staff-internal materials repository for Computer Science 51. 

Click "issues" to see open development issues.

Click "Wiki" to see course & administrative documents that are being developed. 