<!DOCTYPE html>
<html>

<head>
  <title>CS51 OCaml Style Guide</title>
  <link href="css/cs51.css" rel="stylesheet" type="text/css">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <style type="text/css">
    .heading {
      font-weight: bold;
    }
    .heading:after {
      content: ": ";
    }
    /* http://stackoverflow.com/questions/4615500/how-to-start-a-new-list-continuing-the-numbering-from-the-previous-list */
    ol.split {
      list-style-type: none;
    }
    ol.split li:before {
      counter-increment: mycounter;
      content: counter(mycounter) ".\00A0\00A0";
    }
    ol.split li {
      text-indent: -1.3em;
    }
    ol.split li table {
      text-indent: 0;
    }
    ol.start {
      counter-reset: mycounter;
    }
    .code-sample {
      margin-left: 25px;
      margin-top: 5px;
      margin-bottom: 5px;
      font-family: monospace;
      white-space: pre;
      text-indent: 0;
    }
    .code-sample .comment {
      color: gray;
    }
  </style>
</head>

<body>

<!-- leftnav -->
<iframe id="leftnav" src="leftnav.html" width="100%" height="100%" frameborder="0">
  <p>Your browser does not support iframes.</p>
</iframe>
<!-- end leftnav -->

<div id="rightframe">
  <h1 class="header">CS51 OCaml Style Guide</h1>

<p>For more see the
  <a href="http://caml.inria.fr/resources/doc/guides/guidelines.en.html">
  official OCaml guidelines</a>.
</p>

<p id="acknowledgments">Acknowledgment: Parts of this style guide were
  unlawfully appropriated from
  <a href="http://www.cs.princeton.edu/~dpw/courses/cos326-12/style.php">COS
  326</a> at Princeton, which is taken from
  <a href="http://www.seas.upenn.edu/~cis120/programming_style.shtml">CIS
  120</a> and
  <a href="http://www.seas.upenn.edu/~cis500/cis500-f06/resources/programming_style.html">CIS
  500</a> at UPenn, which stole from
  <a href="http://www.cs.cornell.edu/Courses/cs312/2001sp/style.html">CS
  312</a> at Cornell University. All this shows the great power of recursion.
  (Also, the joke about recursion was stolen from COS 326.)
</p>

  <h2>Formatting</h2>
  <ol class="start split">
    <li><span class="heading">80 Column Limit.</span> No line of code may have
      more than 80 columns. Using more than 80 columns causes your code to wrap
      around to the next line, which is devastating to readability.
    </li>
    <li><span class="heading">Indent consistently</span> People usually indent by
      two <a href="http://en.wikipedia.org/wiki/Exclusive_or">xor</a> four
      spaces. By default, Emacs uses two.
    </li>
    <li><span class="heading">No Tab Characters</span> Do not use the tab
      character (0x09). Instead, use spaces to control indenting. This is
      because the width of a tab is not uniform across all computers, and what
      looks good on your machine may look terrible on mine, especially if you
      have mixed spaces and tabs. Some editors, including Tuareg, map the
      tab key to a sequence of spaces rather than a tab character; in this
      case, it's fine to use the tab key.
    </li>
    <li><span class="heading">No Needless Line Breaks</span> Obviously the
      best way to stay within the 80 character limit imposed by the rule above
      is pressing the enter key every once and a while. However, including
      empty lines should only be done between value declarations, especially
      between function declarations. Often it is not necessary to have empty
      lines between other declarations unless you are separating the different
      types of declarations (such as structures, types, exceptions and
      values). Unless function declarations within a <tt>let</tt> block are
      long, there should be no empty lines within a let block. There should
      absolutely never be an empty line within an expression.
    </li>
    <li><span class="heading">Overparenthesizing</span> Parentheses have many
      semantic purposes in OCaml, including constructing tuples, grouping
      sequences of side-effect expressions, forcing higher-precedence on an
      expression for parsing, and grouping structures for functor arguments.
      Clearly, parentheses must be used with care. You may only use
      parentheses when necessary or when it improves readability. Consider
      the following two function applications:
        <pre class="code-sample">
let x = function1 (arg1) (arg2) (function2 (arg3)) (arg4)

let x = function1 arg1 arg2 (function2 arg3) arg4
        </pre>
      The latter is considered better style. On the other hand, it is often
      useful to add parentheses to help indentation algorithms, as in this
      example:
        <pre class="code-sample">
let x = "Long line ..." ^
  "Another long line..."

let x = ("Long line ..." ^
          "Another long line...")
        </pre>
      The latter is preferred. Similarly, wrapping <tt>match</tt> expressions
      in parentheses helps avoid a common (and confusing) error that you get
      when you have a nested <tt>match</tt> expression. Parentheses should
      never appear
      on a line by themselves, nor should they be the first graphical
      character &ndash; parentheses do not serve the same purpose as brackets
      do in C or Java.
    </li>
    <li><span class="heading">Indenting <tt>match</tt> Expressions</span>
      Indent similar to the following.
        <pre class="code-sample">
match expr with
| pat1 -&gt; ...
| pat2 -&gt; ...
        </pre>
    </li>
    <li><span class="heading">Indenting <tt>if</tt> Expressions</span>Indent
      similar to the following.
        <pre class="code-sample">
if exp1 then exp2
else if exp3 then exp4
else if exp5 then exp6
  else exp8

if exp1 then
  exp2
else exp3

if exp1 then exp2 else exp3

if exp1 then exp2
else exp3
        </pre>
    </li>
  </ol>

  <h2>Comments</h2>
  <ol class="split">
    <li><span class="heading">Comments Go Above the Code They Reference</span>
      Consider the following:
      <pre class="code-sample">
let sum = List.fold_left (+) 0
<span class="comment">(* Sums a list of integers. *)</span>

<span class="comment">(* Sums a list of integers. *)</span>
let sum = List.fold_left (+) 0
</pre>
      The latter is the better style, although you may find some source
      code that uses the first. We require that you use
      the latter. Comments should be indented to the level of the line of code
      that follows the comment.
    </li>
  <li><span class="heading">Avoid Over-commenting</span> Incredibly long
    comments are not very useful. Long comments should only appear at the top of a file &ndash; here you should explain the overall design of the code
    and reference any sources that have more information about the algorithms
    or data structures. All other comments in the file should be as short as
    possible; after all, brevity is the soul of wit. Most often the best
    place for any comment is just before a function declaration. Rarely
    should you need to comment within a function &ndash; variable naming
    should be enough.
  </li>
  <li><span class="heading">Avoid Useless Comments</span> Avoid comments that
    merely repeat the code they reference or state the obvious. Comments
    should state the invariants, the non-obvious, or
    any references that have more information about the code.
  </li>
  <li><span class="heading">Multi-line Commenting</span> When comments are
    printed on paper, the reader lacks the advantage of color highlighting
    performed by an editor such as Emacs. Multiline comments can be
    distinguished from code by preceding each line of the comment with a
    <tt>*</tt> similar to the following:
      <pre class="code-sample">
<span class="comment">(* This is one of those rare but long comments
 * that need to span multiple lines because
 * the code is unusually complex and requires
 * extra explanation. *)</span>
let complicated_function () = ...</pre>
    </li>
  </ol>

  <h2>Naming and Declarations</h2>
  <ol class="split">
    <li><span class="heading">Use Meaningful Names</span> Variable names should
      describe what they are for. Distinguishing what a variable references
      is best done by following a particular naming convention (see suggestion
      below). Variable names should be words or combinations of words.
      Cases where variable names can be one letter are in a short let
      blocks. Often it is the case that a function used in a fold, filter,
      or map is bound to the name <tt>f</tt>. Here is an example for short
      variable names:
      <pre class="code-sample">
let date = Unix.localtime (Unix.time ()) in
let minutes = date.Unix.tm_min in
let seconds = date.Unix.tm_min in
let f n = (n mod 3) = 0 in
List.filter f [minutes; seconds]</pre>
    </li>
    <li><span class="heading">Type Annotations</span> Top-level functions and
      values should be declared with types. Consider the following:
      <pre class="code-sample">
let incr x = x + 1

let incr (x : int) : int = x + 1</pre>
      The latter is considered better.
    </li>
    <li><span class="heading">Avoid Global Mutable Variables</span> Mutable
      values, on the rare occasion that they are necessary at all,
      should be local to closures and almost never declared as a structure's
      value. Making a mutable value global causes many problems. First,
      an algorithm that mutates the value cannot be ensured that the value
      is consistent with the algorithm, as it might be modified outside the
      function or by a previous execution of the algorithm. Second, and more
      importantly, having global mutable values makes it more likely that your
      code is nonreentrant. Without proper knowledge of the ramifications,
      declaring global mutable values can easily lead not only to bad style but
      also to incorrect code.
    </li>
    <li><span class="heading">When to Rename Variables</span> You should
      rarely need to rename values: in fact, this is a sure way to obfuscate
      code. Renaming a value should be backed up with a very good reason. One
      instance where renaming a variable is common and encouraged is aliasing
      structures. In these cases, other structures used by functions within
      the current structure are aliased to one or two letter variables at the
      top of the <tt>struct</tt>
      block. This serves two purposes: it shortens the name of the structure and
      it documents the structures you use. Here is an example:
      <pre class="code-sample">
module H = Hashtbl
module L = List
module A = Array
...
      </pre>
    </li>
    <li><span class="heading">Order of Declarations in a Structure</span> When
      declaring elements in a file (or nested module) you first alias the
      structures you intend to use, followed by the types, followed by
      exceptions, and lastly list all the value declarations for the structure.
      Here is an example:
      <pre class="code-sample">
module L = List
type foo = int
exception InternalError
let first list = L.nth list 0
      </pre>
      Note that every declaration within the structure should be indented the
      same amount.
    </li>
    <li><span class="heading">Naming Conventions</span> Although we don't
      expect you to follow this convention in detal, the following are the
      rules that are followed by the OCaml standard library:
        <table>
        <thead>
          <tr>
            <th>Token</th>
            <th>Convention</th>
            <th>Example</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Variables and functions</td>
            <td>Symbolic or initial lower case. Use underscores for multiword names:</td>
            <td><tt>get_item</tt></td>
          </tr>
          <tr>
            <td>Constructors</td>
            <td>Initial upper
              case. Use embedded caps for multiword names. Historic
              exceptions are <tt>true</tt> and <tt>false</tt>.</td>
            <td><tt>Node<br>
              EmptyQueue</tt></td>
          </tr>
          <tr>
            <td>Types</td>
            <td>All lower
              case. Use underscores for multiword names.</td>
            <td><tt>priority_queue</tt></td>
          </tr>
          <tr>
            <td>Module Types</td>
            <td>Initial upper
              case. Use embedded caps for multiword names.</td>
            <td><tt>PriorityQueue</tt></td>
          </tr>
          <tr>
            <td>Modules</td>
            <td>Same as
               module type convention.</td>
            <td><tt>PriorityQueue</tt></td>
          </tr><tr>
            <td>Functors</td>
            <td>Same as module type convention.</td>
            <td><tt>PriorityQueue</tt></td>
          </tr>
        </tbody>
        </table>
      Some of these naming convetions are enforced by the compiler (for
      example, it is not possible to have the name of a variable start with an
      uppercase letter).
    </li>
  </ol>

  <h2>Pattern Matching</h2>
  <ol class="split">
    <li><span class="heading">No Incomplete Pattern Matches</span>Incomplete
      pattern matches are flagged with compiler warnings. We do not allow any
      compiler warnings when grading.
    </li>
    <li><span class="heading">Pattern Match in the Function Arguments When
      Possible</span> Tuples, records and algebraic datatypes can be
      deconstructed using pattern matching. If you simply deconstruct the
      function argument before you do anything useful, it is better to pattern
      match in the function argument. Consider these examples:
        <table>
          <thead>
            <tr>
              <th>Bad</th>
              <th>Good</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <pre class="code-sample">
let f arg1 arg2 =
  let x = fst arg1 in
  let y = snd arg1 in
  let z = fst arg2 in
  ...
                </pre>
              </td>
              <td>
                <pre class="code-sample">
let f (x, y) (z, _) =
  ...
                </pre>
              </td>
            </tr>
            <tr>
              <td>
                <pre class="code-sample">
let f arg1 =
  let x = arg1.foo in
  let y = arg1.bar in
  let baz = arg1.baz in
  ...
                </pre>
              </td>
              <td>
                <pre class="code-sample">
let f {foo = x; bar = y; baz} =
  ...
                </pre>
              </td>
            </tr>
          </tbody>
        </table>
    </li>
    <li><span class="heading">Function Arguments Should Not Use Values for
      Patterns</span> You should only deconstruct values with variable names
      and/or wildcards in function arguments. If you want to pattern match
      against a specific value, use a <tt>match</tt> expression or an
      <tt>if</tt> expression. We include this rule because there are too many
      errors that can occur when you don't do this exactly right. Consider
      the following:
        <pre class="code-sample">
let rec fact = function 0 -> 1
  | n -> n * fact(n - 1)

let rec fact n =
  if n = 0 then 1
  else n * fact(n - 1)
        </pre>
      The latter is considered better style.
    </li>
    <li><span class="heading">Avoid Using Too Many Projections</span>
      Frequently projecting a value from a record or tuple causes your code to
      become unreadable. This is especially a problem with tuple projection
      because the value is not documented by a variable name. To prevent
      projections, you should use pattern matching with a function argument or
      a value declaration. Of course, using projections is okay as long as it
      is infrequent and the meaning is clearly understood from the context.
      The above rule shows how to pattern match in the function arguments.
      Here is an example for pattern matching with value declarations.
        <pre class="code-sample">
let v = someFunction() in
  let x = fst v in
  let y = snd v in
    x + y

let x, y = someFunction() in
  x + y
        </pre>
      The latter is the preferred style.
    </li>
    <li><span class="heading">Pattern Match with as Few <tt>match</tt>
      Expressions as Necessary</span> Rather than nesting <tt>match</tt>
      expressions, you can combine them by pattern matching against a tuple.
      Of course, this doesn't work
      if one of the nested <tt>match</tt> expressions matches against a value
      obtained from a branch in another <tt>match</tt> expression.
      Nevertheless, if all the values are independent of each other you should
      combine the values in a tuple and match against that. Here is an
      example:
        <pre class="code-sample">
<span class="comment">(* Bad *)</span>
let d = Date.fromTimeLocal(Unix.time()) in
  match Date.month d with
  | Date.Jan -&gt; (match Date.day d with
                    | 1 -&gt; print "Happy New Year"
                    | _ -> ())
  | Date.Jul -&gt; (match Date.day d with
                    | 4 -&gt; print "Happy Independence Day"
                    | _ -> ())
  | Date.Oct -&gt; (match Date.day d with
                    | 10 -&gt; print "Happy Metric Day"
                    | _ -&gt; ())

<span class="comment">(* Good *)</span>
let d = Date.fromTimeLocal(Unix.time()) in
  match (Date.month d, Date.day d) with
  | (Date.Jan, 1) -&gt; print "Happy New Year"
  | (Date.Jul, 4) -&gt; print "Happy Independence Day"
  | (Date.Oct, 10) -&gt; print "Happy Metric Day"
  | _ -> ()
        </pre>
    </li>
    <li><span class="heading">Don't use <tt>List.hd</tt> or <tt>List.tl</tt>
      </span>
      The functions <tt>hd</tt> and <tt>tl</tt> are used to
      deconstruct  list types; however, they raise exceptions on
      certain inputs. You should never use these functions. In the
      case that you find it absolutely necessary to use these (something that
      probably won't ever happen), you should handle any exceptions that can be
      raised by these functions.
    </li>
  </ol>

  <h2>Verbosity</h2>
  <ol class="split">

    <li><span class="heading">Don't Rewrite Existing Code</span> The OCaml
      <a href="http://caml.inria.fr/pub/docs/manual-ocaml/manual034.html">
        standard libraries
      </a>
      have a great number of functions and data structures &ndash; unless told
      otherwise, use them! Often students will recode <tt>
      <a href="http://caml.inria.fr/pub/docs/manual-ocaml/libref/List.html">
      List.filter</a></tt>, <tt>List.map</tt>, and similar functions. A more
      subtle situation for recoding is all the fold functions. Writing a
      function that recursively walks down the list should make vigorous use
      of <tt>List.fold_left</tt> or <tt>List.fold_right</tt>. Other data
      structures often have a folding function; use them whenever they are
      available. However, sometimes we want you to implement some constructs
      yourself rather than relying on the library. In such cases, we'll specify
      that you are not allowed to use the library.
    </li>
    <li><span class="heading">Misusing <tt>if</tt> Expressions</span> Remember
      that the type of the condition in an <tt>if</tt> expression is
      <tt>bool</tt>. In general, the type of an <tt>if</tt> expression is
      <tt>'a</tt>, but in the case that the type is <tt>bool</tt>, you should
      not be using <tt>if</tt> at all. Consider the following:
      <table>
        <thead>
          <tr>
            <th>Bad</th>
            <th>Good</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <pre class="code-sample">
if e then true else false
              </pre>
            </td>
            <td>
              <pre class="code-sample">
e
              </pre>
            </td>
          </tr>
          <tr>
            <td>
              <pre class="code-sample">
if e then false else true
              </pre>
            </td>
            <td>
              <pre class="code-sample">
not e
              </pre>
            </td>
          </tr>
          <tr>
            <td>
              <pre class="code-sample">
if e then e else false
              </pre>
            </td>
            <td>
              <pre class="code-sample">
e
              </pre>
            </td>
          </tr>
          <tr>
            <td>
              <pre class="code-sample">
if not e then x else y
              </pre>
            </td>
            <td>
              <pre class="code-sample">
if e then y else x
              </pre>
            </td>
          </tr>
          <tr>
            <td>
              <pre class="code-sample">
if x then true else y
              </pre>
            </td>
            <td>
              <pre class="code-sample">
x || y
              </pre>
            </td>
          </tr>
          <tr>
            <td>
              <pre class="code-sample">
if x then y else false
              </pre>
            </td>
            <td>
              <pre class="code-sample">
x &amp;&amp; y
              </pre>
            </td>
          </tr>
          <tr>
            <td>
              <pre class="code-sample">
if x then false else y
              </pre>
            </td>
            <td>
              <pre class="code-sample">
not x &amp;&amp; y
              </pre>
            </td>
          </tr>
        </tbody>
      </table>
    </li>
    <li><span class="heading">Misusing <tt>match</tt> Expressions</span> The
      <tt>match</tt> expression is misused in two common situations. First,
      <tt>match</tt> should never be used in place of an <tt>if</tt>
      expression (that's why <tt>if</tt> exists). Note the following:
        <pre class="code-sample">
(* Bad *)
match e with
| true -> x
| false -> y

(* Good *)
if e then x else y
        </pre>
      Another situation where <tt>if</tt> expressions are preferred over
      <tt>match</tt> expressions is as follows:
        <pre class="code-sample">
(* Bad *)
match e with
| c -&gt; x (* c is a constant value *)
| _ -&gt; y

(* Good *)
if e = c then x else y
        </pre>

      A final misuse is using <tt>match</tt> when pattern matching within the
      <tt>let</tt> expression is enough. Consider the following:
        <pre class="code-sample">
(* Bad *)
let x = match expr with
        | y, z -&gt; y

(* Good *)
let x, _ = expr
        </pre>
    </li>
    <li><span class="heading">Other Common Mistakes</span> Here is a bunch of
      other common mistakes to watch out for:
      <table>
        <thead>
          <tr>
            <th>Bad</th>
            <th>Good</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><tt>l::[]</tt></td>
            <td><tt>[l]</tt></td>
          </tr>
          <tr>
            <td><tt>length + 0</tt></td>
            <td><tt>length</tt></td>
          </tr>
          <tr>
            <td><tt>length * 1</tt></td>
            <td><tt>length</tt></td>
          </tr>
          <tr>
            <td><tt>big exp * same big exp</tt></td>
            <td><tt><span class="keyword">let</span>
               x = big exp <span class="keyword">in</span>
              x*x </tt></td>
          </tr>
          <tr>
            <td><tt><span class="keyword">if</span>
              x <span class="keyword">then</span> f a b c1<br>
              <span class="keyword">else</span> f a b c2</tt></td>
            <td><tt>f a b <span class="keyword">(if</span>
              x <span class="keyword">then</span> c1 <span class="keyword">else</span>
              c2)</tt></td>
          </tr>
          <tr>
            <td><tt>String.compare x y = 0</tt></td>
            <td><tt>x = y</tt></td>
          </tr>
          <tr>
            <td><tt>String.compare x y &lt; 0</tt></td>
            <td><tt>x&lt;y</tt></td>
          </tr>
          <tr>
            <td><tt>String.compare x y &gt; 0</tt></td>
            <td><tt>x&gt;y</tt></td>
          </tr>
        </tbody>
      </table>
    </li>
    <li><span class="heading">Don't Rewrap Functions</span> When passing a
      function around as an argument to another function, don't rewrap the
      function if it already does what you want it to. Here's an example:
        <pre class="code-sample">
(* Bad *)
List.map (fun x -&gt; sqrt x) [1.0; 4.0; 9.0; 16.0]

(* Good *)
List.map sqrt [1.0; 4.0; 9.0; 16.0]
        </pre>
      Another case for rewrapping a function is often
      associated with infix binary operators. To prevent rewrapping the binary
      operator, just place it between parentheses:
        <pre class="code-sample">
List.fold_left (fun x y -> x + y) 0

List.fold_left (+) 0
        </pre>
    </li>
    <li><span class="heading">Avoid Computing Values Twice</span> When
      computing values twice you're wasting the CPU time and making your
      program ugly. The best way to avoid computing things twice is to create
      a <tt>let</tt> expression and bind the computed value to a variable name.
      This has the added benefit of letting you document the purpose of the
      value with a variable name &ndash; which means less commenting. On the
      other hand, not every computed subvalue needs to be <tt>let</tt>-bound.
    </li>
  </ol>

<!--
  <h2>Actual Code Examples</h2>
  <p>Be sure to parenthesize nested <tt>match</tt> expressions. You can
    often combine them into a single <tt>match</tt> to avoid this issue, but
    only do this if it increases clarity.
  </p>
  <p>This is more of a guideline than a rule, but it will often make
    your code more readable. Try to make your <tt>if</tt> expressions one line.
    This may require putting some expressions in a <tt>let</tt>.
    Again, this is not always the best choice, so just go with the most
    readable option.
  </p>
  <table>
    <tr>
      <th>Good</th>
      <th>Bad</th>
      <th>Explanation</th>
    </tr>
    <tr>
      <td><tt>e1 && e2</tt></td>
      <td><tt>if e1 then e2 else false</tt></td>
      <td>If the branches of an <tt>if</tt> statement evaluate to booleans,<br>
	you probably don't need the <tt>if</tt> statement.</td>
    </tr>
    <tr>
      <td><tt>let (x,y) = (4,5) in<br>&nbsp;&nbsp;...</tt></td>
      <td><tt>let x = fst (4,5) in<br>
	  let y = snd (4,5) in<br>&nbsp;&nbsp;...</tt>
      </td>
      <td>You can <tt>match</tt> on tuples in <tt>let</tt> statements.</td>
    </tr>
    <tr>
      <td><tt>(fun (x,y) -> x + y) (4,5)</tt></td>
      <td><tt>(fun tuple -> fst tuple + snd tuple) (4,5)</tt></td>
      <td>You can <tt>match</tt> on tuples anywhere it makes sense.</td>
    </tr>
    <tr>
      <td><tt>if x = 0 then e1 else e2</tt></td>
      <td><tt>match x with
	  <br>&nbsp;&nbsp;| 0 -> e1
	  <br>&nbsp;&nbsp;| _ -> e2</tt>
      </td>
      <td>Try not to <tt>match</tt> on primitive values.</td>
    </tr>
    <tr>
      <td><tt>match ... with
	  <br>&nbsp;&nbsp;| ...</tt>
      </td>
      <td><tt>List.head</tt>, <tt>Option.is_some</tt>
      </td>
      <td>In almost all cases, you should not unpack<br>
	abstract data types with specialized functions.
      </td>
    </tr>
  </table>
-->
</div>
</body>

</html>
