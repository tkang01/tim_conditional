
/* ================ PART 3 ================================
 * Recurrence relations: consider the following functions, from the Checkers
 * assignment. */

private ArrayList<Move> possibleJumps(Piece p, Move soFar, Location l) {
    boolean fresh = (soFar == null);
    if(soFar == null) {
        soFar = new Move(l);
    }
    	
    ArrayList<Move> possible = new ArrayList<Move>();
    	
    Taken t = soFar.getTaken();
    Move afterStep;
    	
    Location proposedVictimLoc;
    ArrayList<Location> jumpsAvail = filterAvailable(jumpsWithinRange(p, l), t);
    for (Location nextStep : jumpsAvail) {
        proposedVictimLoc = getJumpedLocation(p, t, l, nextStep);
        if(proposedVictimLoc != null) {
            // If and only if we're actually going to jump something,
            // we clone our current Move before modifying it.
            afterStep = (Move)soFar.clone();
            afterStep.addLocation(nextStep);
            afterStep.recordTaken(proposedVictimLoc);
            possible.addAll(possibleJumps(p, afterStep, nextStep));
        }
    }
    if(!fresh && possible.isEmpty()) {
        if(soFar != null) {
            ArrayList<Move> justSoFar = new ArrayList<Move>();
            justSoFar.add(soFar);
            return justSoFar;
        }
        else {
            return new ArrayList<Move>();
        }
    }
    else {
        return possible;
    }
}

private ArrayList<Location> filterAvailable(ArrayList<Location> list,
                                            Taken t) {
    ArrayList<Location> avail = new ArrayList<Location>();
    for(Location poss : list) {
        if(isAvailable(t, poss)) {
            avail.add(poss);
        }
    }
    return avail;
}


/*
 * The code above is a bit complex, and we've omitted the bodies of many of the
 * functions that it depends on.   However, we want to see if we can reason
 * about the recurrence of possibleJumps, given a few simplifying assumptions.
 * 
 * Notice that the recursion takes place in the line:
 *   X: possible.addAll(possibleJumps(p, afterStep, nextStep));
 * And that line X happens for each nextStep in jumpsAvail, iff
 * proposedVictimLoc is not equal to null.
 * Let's assume that we execuute line X for approximately 1/3 of the
 * elements in jumpsAvail.  
 * 
 * But what about the size of jumpsAvail?  Let's say that when we first call
 * possibleJumps with an empty Move soFar, jumpsAvail.size() == n.
 * Now, let's assume that each time we add a move (each time we go one step
 * "deeper" recursively), jumpsAvail.size()  decreases by a factor of 4. 
 *
 * Given these assumptions, and assuming that all the rest of the functions
 * called complete in constant time, give a recurrence relation to describe the
 * running time of possibleJumps.
 */











/* Using Big-O notation, describe the running time for the possibleJumps
 * function. In particular, use the _simplest_ function possible to
 * characterize the worst-case running time. */

