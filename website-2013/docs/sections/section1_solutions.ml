(* CS51 Section 1 *)

(* Algebraic Data Types *)
(* Exercise 1 *)
(* Look at the following type definition. *)
type color = Red | Yellow | Blue | Green | Crimson | Other of string
type favorite = Color of color | Movie of string | Tvshow of string |
                Number of float | Letter of char

let a : favorite list = [Movie "A Beautiful Mind"; Color Blue;
                         Tvshow "The Simpsons"; Color Crimson];;
let b : favorite list = [Number 1.0; Number 2.0; Number 5.0;
                         Number 14.0; Number 42.0];;
let c : favorite list = [Movie "Love Story"; Tvshow "On Harvard Time";
                         Letter 'H'; Color Crimson];;
let d : favorite list = [Tvshow "Lost"; Number 3.14];;

let students = [a; b; c; d];;

(* 1a. Define a value of type favorite list for someone whose
 * favorite color is chartreuse and whose favorite number is 5. *)

let prob1a : favorite list = [Color (Other("Chartreuse")); Number 5.0];;

(* 1b. Write a function that takes a value of type favorite list (like the
 * ones above) and returns this person's favorite movie, or None if a
 * favorite movie isn't given. If multiple movies are listed, return the first.
 * What return type does this function have?*)

let rec favmovie (lst: favorite list) : string option =
  match lst with
    | [] -> None
    | (Movie m)::_ -> Some m
    | _::t -> favmovie t
;;

(* 1c. Write a function that takes a value of type favorite list and returns 
 * true if and only if this person has listed crimson as a favorite color. *)

let rec harvardpride (lst: favorite list) : bool =
  match lst with
    | [] -> false
    | (Color Crimson)::_ -> true
    | _::t -> harvardpride t
;;

let harvardpride' = List.mem (Color Crimson) ;;

(* 1d. Write a function that takes a list of favorite lists and returns any
   with a favorite color listed as crimson. *) 

let rec harvardfilter (lst: favorite list list) : favorite list list =
  match lst with
    | [] -> []
    | hd::tl ->
	if harvardpride hd then hd :: (harvardfilter tl) else harvardfilter tl
;;

let rec filter pred lst =
  List.fold_right (fun x y -> if (pred x) then x::y else y) lst []

let harvardfilter2 (lst: favorite list list) : favorite list list =
  filter harvardpride lst

(* Even shorter: *)
let harvardfilter' = List.filter harvardpride


(* Exercise 2 *)
(* No matter how good you are at writing code, errors are bound to pop up.
 * As such, learning how to debug your code is just as important as 
 * learning how to write the code itself. If it does not work properly, 
 * it's not very useful!!! Bugs/errors fall under three general categories:
 * 1. Syntax errors: errors in which the code written does not follow the 
 *    rules of the language. ex: not using "then" after an if statement or 
 *    forgetting the double semi-colon after an expression
 * 2. Type errors: where the return type conflicts with a predeterminied 
 *                 value
 *    ex: returning a double instead of an int.
 * 3. Runtime errors: errors in which the logic of the code is faulty. ex: 
 *    multiplying where addition was required.
 *)

(* In the following example, we have a function, calculate_pref, which is 
 * supposed to help determine "Professor Grogg"'s preference for a certain
 * student based on that student's "favorite list." This function should 
 * take a student's "favorite list" and return the value ("score) Grogg 
 * gives that person based on the following criteria:
 *
 * 1. Grogg assigns a preference score to each item in the list and 
 *    multiplies those scores together to determine his preference for a 
 *    student.
 * 2. Grogg's base score for any one "favorite" item is 1, that is, that
 *    favorite item will receive a score of 1 except those items listed
 *    below in criteria #3 and #4.
 * 3. Grogg preferences the Red, Crimson, & "Black and White" colors at 2.
 * 4. Grogg preferences any Movie at 3, any Tvshow at 2, numbers between 10
 *    and 15 at 4, and chars a-f at 4.
 *
 * A function has been written below to perform these calculations, but it 
 * has a myraid of problems. 
 *)
let calculate_pref (ls:favorite list) : int = 
let color_matcher col = 
match col with
| Red ->  2.0
| Other a -> (if String.compare a "Black and White" == 0 then 2.0 
else 1.0) in
let calculate_pref_helper (l:favorite) : int = 
match l with
| Movie _ -> 3 
| Color a -> (color_matcher a)
| Tvshow _ -> 2 
| Number a -> (if a <= 15.0 && a >= 10.0 then 4 
else 1)
| Letter a -> (let pref = Char.code a in
if pref >= 97 && pref <= 102 then 
4 else 1) in   
reduce (fun a b -> calculate_pref_helper a + b) 0 ls ;;

(* 2. Due to style issues (and the fact that it's one gigantic function!), 
 * it is very difficult indeed to try to determine where the problems come
 * from. This is an example of bad code.
 * If we see this in office hours, WE WILL NOT HELP UNTIL THE CODE HAS BEEN
 * REWRITTEN TO FOLLOW STYLE GUIDELINES. 
 *
 * To make the above an easier problem to tackle, this function should be 
 * broken down into manageable parts into distinct helper functions. In the
 * example, this has already been done. However, inner helper functions 
 * cannot be explicitly tested. To do this, these functions must be 
 * written as their own outer functions in the top level.
 * 
 * After searching, it was found that the two functions color_matcher and 
 * calculate_pref are the cause of the problems above. What errors exist 
 * in the following code, what types of errors are they, and how can they 
 * be fixed?
 * 
 * NOTE: First find and correct all syntax and type errors. Second, use 
 * assert statements to check that each function follows Professor Grogg's
 * preference invariants appropriately. If an assert statement fails,
 * and you cannot tell why, use a print statement to see what the result
 * was, or use more granular assert statements.
 *) 

let color_matcher (col:color) : int = 
  match col with
    | Red  ->  2.0
    | Other a -> (if String.compare a "Black and White" = 0 then 2.0 else 1.0);;

let calculate_pref_helper (l:favorite) : int = 
  match l with
    | Movie _ -> 3 
    | Color a -> (color_matcher a)
    | Tvshow _ -> 2 
    | Number a -> (if a <= 15.0 && a >= 10.0 then 4 
                   else 1)
    | Letter a -> (let pref = Char.code a in
                     if pref >= 97 && pref <= 102 then 
                       4 else 1) ;;

let calculate_pref (ls:favorite list) : int = 
  reduce (fun a b -> calculate_pref_helper a + b) 0 ls ;;

(* 1. type error -- return of color_matcher is ints, not doubles
 * 2. syntax error -- missing options for Colors Crimson, etc. 
 *                 for color_matcher
 * 3. runtime error -- addition instead of multiplication for 
 *                  the function in calculate_pref, base case should be 1, not 0.
 *
 * See the corrections in the code below.
 *)

let color_matcher col = 
match col with
  | Red | Crimson ->  2
  | Other a -> (if String.compare a "Black and White" == 0 then 2 else 1)
  | _ -> 1;;

let calculate_pref_helper (l:favorite) : int = 
  match l with
    | Movie _ -> 3 
    | Color a -> (color_matcher a)
    | Tvshow _ -> 2 
    | Number a -> (if a <= 15.0 && a >= 10.0 then 4 
                   else 1)
    | Letter a -> (let pref = Char.code a in
                     if pref >= 97 && pref <= 102 then 
                       4 else 1);;

let calculate_pref (ls:favorite list) : int = 
  reduce (fun a b -> calculate_pref_helper a * b) 1 ls ;;

(* In summary, debugging code is done by breaking problems down into 
 * manageable chunks that are dealt with separately. This helps pinpoint 
 * problems and will (hopefully!) let you quickly resolve them. 
 * Please refer to our debugging guide for more help on how to debug code.
 *)

(* Exercise 3 *)
(* 3a. Define an algebraic data type representing either ints or floats *)

type realnum = Int of int | Float of float

(* 3b. Define a function testing whether two realnums are equal. It shouldn't
 * matter whether they are ints or floats, e.g (realequal 4 4.0) => True. *)

let realequal (a: realnum) (b: realnum) : bool =
  match (a, b) with
    | (Int a1, Int b1) -> a1 = b1
    | (Float a1, Float b1) -> a1 = b1
    | (Int a1, Float b1) -> (float_of_int a1) = b1
    | (Float a1, Int b1) -> a1 = (float_of_int b1)
;;
	
type expr = Value of bool | Not of expr | And of expr * expr | Or of expr * expr

(* 3c. Write a function that takes in an expr and returns whether it evaluates
 * to true or false. *)
let rec eval (a:expr) : bool =
  match a with
    | Value v -> v
    | Not e -> not (eval e)
    | And (e1, e2) -> eval e1 && eval e2
    | Or (e1, e2) -> eval e1 || eval e2
;;

(* Higher-order functions and polymorphism *)
(* Exercise 4 *)

(* 4a. Write a function to return the smaller of two int options, or None
 * if both are None. If exactly one argument is None, return the other. *)
let min_option (x: int option) (y: int option) : int option =
  match (x, y) with
    | (Some x', Some y') -> Some (min x' y')
    | (_, None) -> x
    | (None, _) -> y (* Note: this also catches the (None, None) case *)
;;

(* 4b. Write a function to return the larger of two int options, or None
 * if both are None. If exactly one argument is None, return the other. *)
let max_option (x: int option) (y: int option) : int option =
  match (x, y) with
    | (Some x', Some y') -> Some (max x' y')
    | (_, None) -> x
    | (None, _) -> y
;;

(* Great, but do you see the pattern? How can we factor out similar code? *)
(* 4c. Write a higher-order function for binary operations on options.
 * If both arguments are None, return None.  If one argument is (Some x)
 * and the other argument is None, function should return (Some x) *)
(* What is calc_option's function signature? *)
let calc_option (f: 'a->'a->'a) (x: 'a option) (y: 'a option) : 'a option =
  match (x, y) with
    | (Some x', Some y') -> Some (f x' y')
    | (_, None) -> x
    | (None, _) -> y
;;

(* 4d. Now rewrite min_option and max_option using the higher-order function. *)
let min_option_2 (x: int option) (y: int option) : int option =
  calc_option min x y
;;

let max_option_2 (x: int option) (y: int option) : int option =
  calc_option max x y
;;

(* Cool! Now, can we use this in other ways? *)
(* 4e. Write a function to return the boolean AND of two bool options,
 * or None if both are None. If exactly one is None, return the other. *)
let and_option (x:bool option) (y: bool option) : bool option =
  calc_option (&&) x y;;

(* 4f. Write a function to return the boolean OR of two bool options,
 * or None if both are None. If exactly one is None, return the other. *)
let or_option (x:bool option) (y: bool option) : bool option =
  calc_option (||) x y;;

(* Map and reduce *)
(* Exercise 5 *)
let rec reduce f u xs =
  match xs with
    | [] -> u
    | hd::tl -> f hd (reduce f u tl);;

let rec map f xs =
  match xs with
    | [] -> []
    | hd::tl -> (f hd) :: (map f tl);;
	
(* let map = List.map;; *)

(* 5a. Implement length in terms of reduce. 
 * length lst returns the length of lst. length [] = 0. *)
let length (lst: int list) : int =
  reduce (fun x r -> r + 1) 0 lst
;;

(* Is there a way to write this without the argument? *)
let length' = reduce (fun x r -> r + 1) 0

(* 5b. Write a function that takes an int list and multiplies every int by 3.
 * Use map. *)
let times_3 (lst: int list): int list =
  map (fun a -> a * 3) lst
;;

(* 5c. Write a function that takes an int and an int list and multiplies every
 * entry in the list by the int. Use map. *)
let times_x (x : int) (lst: int list): int list =
  map (fun a -> a * x) lst
;;

(* 5d. Rewrite times_3 in terms of times_x.
 * This should take very little code. *)
let times_3_shorter = times_x 3 ;;

(* 5e. Write a function that takes an int list and generates a "multiplication
 * table", a list of int lists showing the product of any two entries in the
 * list.  e.g. mult_table [1;2;3] => [[1; 2; 3]; [2; 4; 6]; [3; 6; 9]] *)
let mult_table (lst: int list) : int list list =
  map (fun a -> times_x a lst) lst
;;
	
(* 5f. Write a function that takes a list of boolean values
 * [x1; x2; ... ; xn] and returns x1 AND x2 AND ... AND xn.
 * For simplicity, assume and_list [] is TRUE. Use reduce. *)
let and_list (lst: bool list) : bool =
  reduce (&&) true lst
;;

(* 5g. Do the same as above, with OR.
 * Assume or_list [] is FALSE. *)
let or_list (lst: bool list) : bool =
  reduce (||) false lst
;;
	
(* 5h.	 Write a function that takes a bool list list and returns
 * its value as a boolean expression in conjunctive normal form (CNF).
 * A CNF expression is represented as a series of OR expressions joined
 * together by AND.
 * e.g. (x1 OR x2) AND (x3 and x4 and x5) AND (x6).
 * Use map and/or reduce.
 * You may find it helpful to use and_list and or_list. *)
let cnf_list (lst: bool list list) : bool =
  and_list (map or_list lst)
;;
	
(* 5i. Write a function that takes an expr list and returns true if and only if
 * every expr in the list represents a true Boolean expression. *)
let all_true (lst: expr list) : bool =
  reduce (&&) true (map eval lst)
;;
	
(* 5j. Write and_list to return a bool option,
 * where the empty list yields None. Use reduce. *)
let and_list_smarter (lst: bool list) : bool option =
  reduce (fun x base -> and_option (Some x) base) None lst
;;

let and_list_smartest (lst: bool list) : bool option =
  reduce and_option None (map (fun x -> Some x) lst)
;;

(* 5k. Write max_of_list from section 0:
 * Return the max of a list, or None if the list is empty. *)
let max_of_list (lst:int list) : int option =
  match lst with
    | hd::tl -> Some (reduce max hd tl)
    | [] -> None
;;

(* 5l. Write bounds from section 0:
 * Return the min and max of a list, or None if the list is empty. *)
let bounds (lst:int list) : (int option * int option) =
  reduce (fun x (small, big) -> (min_option x small, max_option x big))
    (None, None)
    (map (fun x -> Some x) lst)
;;
