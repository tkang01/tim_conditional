(* *************** Example: refs  ******************** *)

let c = ref 0 ;;

let x = !c ;;   (* x will be 0 *)

c := 42 ;;

let y = !c ;;   (* y will be 42.
					 x will still be 0! *)


(* *************** Example: functions+refs ***************** *)

let c = ref 0 ;;

let next() = 
  let v = !c in
  (c := v+1 ; v);;

next() ;;                 (* returns 0 *)
next() ;;                 (* returns 1 *)
(next()) + (next()) ;;    (* returns ??? *)


(* Is there a difference? *)
let f = 
  let x = 12 in
  (fun a -> x + a)

let g = 
  (fun a -> 
     let x = 12 in
       x + a)

(* How about here? *)
let f' =
  let x = ref 12 in
    (fun a -> (x := !x + a; !x))

let g' =
    (fun a ->
       let x = ref 12 in
         (x := !x + a; !x))



(* *************** Example: aliasing  ******************** *)

let c = ref 0 ;;

let x = c ;;

c := 42 ;;

!x ;;

(* *************** Example: Imperative stack  ************ *)

module type IMP_STACK = 
  sig
    type 'a stack
    val empty : unit -> 'a stack
    val push : 'a -> 'a stack -> unit
    val pop : 'a stack -> 'a option
  end

module ImpStack : IMP_STACK = 
  struct
    type 'a stack = ('a list) ref

    let empty () : 'a stack = ref []
    
    (* Note: returns unit, modifying stack in place *)
    let push (x:'a) (s:'a stack) : unit = 
       s := x::(!s)

    let pop (s:'a stack) : 'a option = 
      match !s with 
      | [] -> None
      | h::t -> (s := t ; Some h)
  end

(* *************** Example:   ******************** *)

module type IMP_QUEUE = 
  sig
    type 'a queue
    val empty : unit -> 'a queue
    val enq : 'a -> 'a queue -> unit
    val deq : 'a queue -> 'a option
  end


module ImpQueue : IMP_QUEUE = 
  struct
    type 'a queue = 
       {front:'a list ref ; rear :'a list ref}

    let empty () = {front = ref [] ; rear = ref []} 

    let enq x q = (q.rear := x::(!(q.rear)))

    let deq q = 
        match !(q.front) with
        | h::t -> (q.front := t; Some h)
        | [] -> (match List.rev (!(q.rear)) with
                 | h::t -> (q.front := t; q.rear := []; Some h)
                 | [] -> None)
  end

let and_all (xs:bool list) = List.fold_left (&&) true xs

let test_imp_queue () = 
  let enq = ImpQueue.enq in
  let deq = ImpQueue.deq in 
  let q = ImpQueue.empty () in
  let v1 = (enq 1 q; enq 2 q; enq 3 q; deq q) in
  let v2 = deq q in
  let v3 = deq q in
    match (v1, v2, v3) with
      | (Some a, Some b, Some c) -> assert(and_all [a=1;b=2;c=3])
      | _ -> raise (Failure "ImpQueue FAIL")

(* Anything wrong with this? *)
module ImpQueue2 : IMP_QUEUE = 
  struct
    type 'a mlist = Nil | Cons of 'a * (('a mlist) ref)
    type 'a queue = {front:'a mlist ref ; rear :'a mlist ref}

    let empty() = {front=ref Nil ; rear=ref Nil} 

    let enq x q = 
        match !(q.rear) with
        | Nil -> (q.front := Cons (x, ref Nil) ; q.rear := !(q.front))
        | Cons (h,t) -> (t := Cons (x, ref Nil) ; q.rear := !t)
     
    let deq q = 
        match !(q.front) with
        | Cons(h,t) ->  (q.front := !t ; Some h)
        | Nil -> None
  end

let test_imp_queue2 () = 
  let enq = ImpQueue2.enq in
  let deq = ImpQueue2.deq in 
  let q = ImpQueue2.empty () in
  let v1 = (enq 1 q; enq 2 q; enq 3 q; deq q) in
  let v2 = deq q in
  let v3 = deq q in
    match (v1, v2, v3) with
      | (Some a, Some b, Some c) -> assert(and_all [a=1;b=2;c=3])
      | _ -> raise (Failure "ImpQueue FAIL")

let ignore v = ()

let test_imp_queue2_better () = 
  let enq = ImpQueue2.enq in
  let deq = ImpQueue2.deq in 
  let q = ImpQueue2.empty () in
  let _ = (enq 1 q; enq 2 q; ignore (deq q); ignore (deq q); enq 3 q;) in
  let v = deq q in
    match v with
      | Some a -> assert (a=3)
      | _ -> raise (Failure "ImpQueue FAIL")

open ImpQueue2;;
let q = empty ();;
enq 1 q;;
deq q;;
enq 2 q;;
deq q;;
(* Oops *)


module ImpQueue3 : IMP_QUEUE = 
  struct
    type 'a mlist = Nil | Cons of 'a * (('a mlist) ref)
    type 'a queue = {front:'a mlist ref ; rear :'a mlist ref}

    let empty() = {front=ref Nil ; rear=ref Nil} 

    let enq x q = 
        match !(q.rear) with
        | Nil -> (assert (!(q.front) = Nil); 
                  q.front := Cons(x,ref Nil); q.rear := !(q.front))
        | Cons(h,t) -> (assert (!t = Nil); t := Cons(x,ref Nil) ; q.rear := !t)

    let deq q = 
        match !(q.front) with
        | Cons(h,t) -> 
            (q.front := !t ; 
             (match !t with Nil -> q.rear := Nil | Cons(_,_) -> ()) ; 
             Some h)
        | Nil -> None
  end

(* *************** Example: mutable lists ******************** *)

type 'a mlist = 
  Nil | Cons of 'a * (('a mlist) ref)

let rec mlength(m:'a mlist) : int =
  match m with
  | Nil -> 0 
  | Cons(h,t) -> 1 + mlength(!t) 

let r = ref Nil ;;
let m = Cons(3,r) ;; 
r := m ;;
mlength m ;;  

(* *************** Example: mutable append ****************** *)

let rec mappend xs ys = 
  match xs with
  | Nil -> ()
  | Cons(h,t) -> 
     (match !t with
      | Nil -> t := ys 
      | Cons(_,_) as m -> mappend m ys) ;;

let xs = Cons (1,ref (Cons (2, ref (Cons (3, ref Nil))))) ;;
let ys = Cons (4,ref (Cons (5, ref (Cons (6, ref Nil))))) ;;
mappend xs ys ;;

let m = Cons(1,ref Nil);;
mappend m m ;;
mlength m ;;


(* *************** Exercises   ******************** *)

(* Make these evaluate to 42 *)

let f = ??? in
  match f () with
    | [] -> 12
    | a::b -> !a 21

let f = ??? in
if f () = 42 then 21 else f ()


(* Write a function call_counter : ('a -> 'b) -> (('a->'b)*int ref)
  The second component of the returned pair should contain the number
  of times the first component has been called.
*)
let call_counter (f:'a -> 'b) : (('a->'b)*int ref) =


(* Usage: *)
let (sq, cnt) = call_counter (fun x -> x*x);;

(*
# !cnt;;
- : int = 0
# sq 12;;
- : int = 144
# !cnt;;
- : int = 1
# sq 42
;;
  - : int = 1764
# !cnt ;;
- : int = 2
# 
*)

(* What is bad about the design of the call_counter interface? *) 

(* ---------------------------------------- *)

(* Write a function to check for a loop in a mutable list. *)
let has_loop (xs:'a mlist) : bool = 
  
