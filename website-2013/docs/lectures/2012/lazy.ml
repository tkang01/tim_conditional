module EagerStream = 
struct
  type 'a stream = Cons of 'a * ('a stream)

  let rec ones : int stream = Cons (1,ones) ;;

  let head (s:'a stream) : 'a = 
    match s with 
      | Cons(h,_) -> h ;;

  let tail (s:'a stream) : 'a stream = 
    match s with 
      | Cons(_,t) -> t ;;

  let rec nth (n:int) (s:'a stream) : 'a = 
    if n <= 0 then head s else nth (n-1) (tail s) ;;

  let rec map (f:'a -> 'b) (s:'a stream) : 'b stream = 
    match s with 
      | Cons(h,t) -> Cons(f h,map f t) ;;

  (* let twos = map ((+) 1) ones ;; *)
end;;

module FunctionStream = 
struct
  type 'a str = Cons of 'a * 'a stream
  and 'a stream = unit -> ('a str);;

  let rec ones : int stream = fun () -> (Cons (1,ones));;

  let head (s:'a stream) : 'a = 
    match s() with 
      | Cons (h,_) -> h
  ;;
  
  let tail (s:'a stream) : 'a stream = 
    match s() with 
      | Cons (_,t) -> t
  ;;

  let rec take(n:int) (s:'a stream) : 'a = 
    if n <= 0 then head s else take (n-1) (tail s)
  ;;

  let rec first(n:int) (s:'a stream) : 'a list = 
    if n <= 0 then [] else (head s)::(first (n-1) (tail s))
  ;;

  let rec map(f:'a -> 'b) (s:'a stream) : 'b stream = 
    fun () -> (Cons (f (head s), map f (tail s)))
  ;;

  let inc x = 1 + x ;;

  let twos = map inc ones ;;

  let rec nats = fun () -> (Cons (0, map inc nats)) ;;

  let rec zip (f:'a -> 'b -> 'c)  
      (s1:'a stream) (s2:'b stream) : 'c stream = 
    fun () -> (Cons (f (head s1) (head s2), 
                zip f (tail s1) (tail s2))) ;;

  let threes = zip (+) ones twos ;;

  let rec fibs = 
    fun () ->  
      (Cons (0, fun () -> 
               (Cons (1, zip (+) fibs (tail fibs))))) ;;

end      

module LazyStream = 
struct
  type 'a str = Cons of 'a * 'a stream
  and 'a stream = ('a str) lazy_t;;

  let rec ones : int stream = lazy (Cons (1,ones));;

  let head (s:'a stream) : 'a = 
    match Lazy.force s with 
      | Cons (h,_) -> h
  ;;
  
  let tail (s:'a stream) : 'a stream = 
    match Lazy.force s with 
      | Cons (_,t) -> t
  ;;

  let rec take(n:int) (s:'a stream) : 'a = 
    if n <= 0 then head s else take (n-1) (tail s)
  ;;

  let rec first(n:int) (s:'a stream) : 'a list = 
    if n <= 0 then [] else (head s)::(first (n-1) (tail s))
  ;;

  let rec map(f:'a -> 'b) (s:'a stream) : 'b stream = 
    lazy (Cons (f (head s), map f (tail s)))
  ;;

  let inc x = 1 + x ;;

  let twos = map inc ones ;;

  let rec nats = lazy (Cons (0, map inc nats)) ;;

  let rec zip (f:'a -> 'b -> 'c)  
      (s1:'a stream) (s2:'b stream) : 'c stream = 
    lazy (Cons (f (head s1) (head s2), 
                zip f (tail s1) (tail s2))) ;;

  let threes = zip (+) ones twos ;;

  let rec fibs = 
    lazy (Cons (0, lazy (Cons (1, zip (+) fibs (tail fibs))))) ;;

  let rec filter p s = 
    if p (head s) then 
      lazy (Cons (head s, filter p (tail s)))
    else (filter p (tail s))
  ;;
  
  let even x = (x mod 2) = 0;;
  let odd x = not(even x);;

  let evens = filter even nats ;;
  let odds = filter odd nats ;;

  let rec from n = lazy (Cons (n,from (n+1))) ;;
  let nats = from 0 ;;

  let not_div_by n m = not (m mod n = 0) ;;

  let rec sieve s = 
    lazy (let h = head s in 
            Cons (h, sieve (filter (not_div_by h) (tail s))))
  ;;

  let primes = sieve (from 2) ;;

  let rec fact n = if n <= 0 then 1 else n * (fact (n-1)) ;;

  let f_ones = map float_of_int ones ;;

  (* The following series corresponds to the Taylor 
   * expansion of e:
   *   1/1! + 1/2! + 1/3! + ...
   * So you can just pull the floats off and start adding
   * them up. *)
  let e_series = 
     zip (/.) f_ones (map float_of_int (map fact nats)) ;;

  let e_up_to n = 
    List.fold_left (+.) 0. (first n e_series) ;;

  (* pi is approximated by the Taylor series:
   *   4/1 - 4/3 + 4/5 - 4/7 + ...
   *)
  let rec alt_fours = 
    lazy (Cons (4.0, lazy (Cons (-4.0, alt_fours))));;

  let pi_series = zip (/.) alt_fours (map float_of_int odds);;

  let pi_up_to n = 
    List.fold_left (+.) 0.0 (first n pi_series) ;;

  let approx_area (f:float->float) (a:float) (b:float) = 
    (((f a) +. (f b)) *. (b -. a)) /. 2.0 ;;

  let mid a b = (a +. b) /. 2.0 ;;

  let rec integrate f a b = 
    lazy (Cons (approx_area f a b, 
                zip (+.) (integrate f a (mid a b))
                         (integrate f (mid a b) b))) ;;

  let abs x = if x < 0. then 0. -. x else x ;;

  let rec within eps s = 
    let (h,t) = (head s, tail s) in
    if abs(h -. (head t)) < eps then h else within eps t ;;

  let integral f a b eps = within eps (integrate f a b) ;;
      
end
