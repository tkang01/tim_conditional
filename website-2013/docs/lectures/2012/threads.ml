(* To use this file you must start Ocaml with the following options:

   ocaml -I /usr/lib/ocaml/threads unix.cma threads.cma

This assumes that the ocaml library was installed in /usr/lib as it
is on the appliance.  If you've installed ocaml on your own machine,
you'll need to find where the ocaml library has been installed.  For
instance, on my Mac, it is installed in /opt/local/lib/ocaml.

The "-I" option for ocaml tells ocaml where to search for libraries
and other files to be loaded.  By default, ocaml always searches for
libraries (like unix.cma, graphics.cma, etc.) in the ocaml library 
directory (e.g., /usr/lib/ocaml).  But the threads library is located
in /usr/lib/ocaml/threads so we must tell ocaml to include this path
in its search for libraries.

*)

let rand = Random.init 0 ; Random.float ;;

let test0 () = 
  let rec loop n = 
    Printf.printf "%d\n" n; flush_all () ; 
    match n with 
      | 0 -> Printf.printf "done!\n" ; flush_all () 
      | _ -> Thread.delay 3.0 ; loop (n-1)
  in 
    Thread.create loop 10
;;

let test1 () = 
  let rec loop name n = 
    match n with 
      | 0 -> ()
      | _ -> Printf.printf "[%s: %d]" name n ; loop name (n-1) in
  let t1 = Thread.create (loop "T1") 1000 in 
  let t2 = Thread.create (loop "T2") 1000
  in
    ()
;;

let test2 () = 
  let r = ref 0 in 
  let rec loop n = 
    if n <= 0 then () 
    else (r := !r + 1 ; Thread.delay (rand 0.01) ; loop (n-1))  
  in
    ignore (Thread.create loop 10) ; 
    loop 10 ;
    !r
;;

module type FUTURE = 
sig
  type 'a future
  (* future f x forks a thread to run f(x)
     and returns a future which can be used
     to synchronize with the thread and 
     get the result. *)  
  val future : ('a->'b) -> 'a -> 'b future 
   
  (* force f causes us to wait until the 
     thread computing the future value is done
     and then returns its value. *)
  val force : 'a future -> 'a 
end


module Future : FUTURE =
struct 
  type 'a future = {tid:Thread.t ; value:'a option ref}

  let future(f:'a->'b)(x:'a) : 'b future = 
    let r : 'b option ref = ref None in    
    let t = Thread.create (fun () -> r := Some(f x)) () 
    in {tid=t ; value=r}

  let force (f:'a future) : 'a = 
    Thread.join f.tid ; 
    match !(f.value) with 
      | Some v -> v
      | None -> failwith "impossible!"
end 

module SortingExample =
struct
  let mergesort(cmp:'a->'a->int)(arr : 'a array) : 'a array = 
    let rec msort (start:int) (len:int) : 'a array = 
      match len with 
        | 0 -> Array.of_list []
        | 1 -> Array.make 1 arr.(start)
        | _ -> 
            let half = len / 2 in
            let a1 = msort start half in
            let a2 = msort (start + half) (len - half) in
              merge a1 a2
    and merge (a1:'a array) (a2:'a array) : 'a array = 
      let a = Array.make (Array.length a1 + Array.length a2) a1.(0) in
      let rec loop i j k = 
        match i < Array.length a1, j < Array.length a2 with 
          | true, true -> 
              if cmp a1.(i) a2.(j) <= 0 then
                (a.(k) <- a1.(i) ; loop (i+1) j (k+1))
              else 
                (a.(k) <- a2.(j) ; loop i (j+1) (k+1))
          | true, false -> a.(k) <- a1.(i) ; loop (i+1) j (k+1)
          | false, true -> a.(k) <- a2.(j) ; loop i (j+1) (k+1)
          | false, false -> ()
      in 
        loop 0 0 0 ; a
    in
      msort 0 (Array.length arr)
  
  let parallel_mergesort(cmp:'a->'a->int)(arr : 'a array) : 'a array = 
    let rec msort (start:int) (len:int) : 'a array = 
      match len with 
        | 0 -> Array.of_list []
        | 1 -> Array.make 1 arr.(start)
        | _ -> 
            let half = len / 2 in
            let a1_f = Future.future (msort start) half in
            let a2 = msort (start + half) (len - half) in
              merge (Future.force a1_f) a2
    and merge (a1:'a array) (a2:'a array) : 'a array = 
      let a = Array.make (Array.length a1 + Array.length a2) a1.(0) in
      let rec loop i j k = 
        match i < Array.length a1, j < Array.length a2 with 
          | true, true -> 
              if cmp a1.(i) a2.(j) <= 0 then
                (a.(k) <- a1.(i) ; loop (i+1) j (k+1))
              else 
                (a.(k) <- a2.(j) ; loop i (j+1) (k+1))
          | true, false -> a.(k) <- a1.(i) ; loop (i+1) j (k+1)
          | false, true -> a.(k) <- a2.(j) ; loop i (j+1) (k+1)
          | false, false -> ()
      in 
        loop 0 0 0 ; a
    in
      msort 0 (Array.length arr)
end

module TreeFoldExample = 
struct
  type 'a tree = Leaf | Node of 'a node
  and 'a node = {left:'a tree ; value : 'a ; right : 'a tree}

  let rec fold (f:'a -> 'b -> 'b -> 'b) (u:'b) (t:'a tree) : 'b = 
    match t with 
      | Leaf -> u
      | Node n -> 
          f n.value (fold f u n.left) (fold f u n.right)

  let rec parallel_fold (f:'a -> 'b -> 'b -> 'b) (u:'b) (t:'a tree) : 'b = 
    match t with
      | Leaf -> u
      | Node n -> 
          let l_f = Future.future (parallel_fold f u) n.left in
          let r = parallel_fold f u n.right in 
            f n.value (Future.force l_f) r
end
