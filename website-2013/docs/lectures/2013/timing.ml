(* To load this file, you need to start Ocaml with unix.cma (the Unix library) as
 * an argument: 
 *
 *   % ocaml unix.cma
 *            Objective Caml version 3.11.0
 *   # #use "timing.ml";;
 *)

(* A signature for a sorting module *)
module type SORT = 
sig 
  val sort : ('a->'a->bool) -> 'a list -> 'a list 
end ;;

(* Insertion Sort *)
module InsertSort : SORT = 
struct
  let rec insert lt xs x = 
    match xs with 
      | [] -> [x]
      | h::t -> if lt x h then x::xs else h::(insert lt t x);;

  let sort lt = List.fold_left (insert lt) []
end

(* Merge Sort *)
module MergeSort : SORT = 
struct
  let rec split xs ys zs = 
    match xs with 
      | [] -> (ys, zs)
      | x::rest -> split rest zs (x::ys) ;;
  
  let rec merge lt xs ys = 
    match (xs,ys) with 
      | ([],_) -> ys
      | (_,[]) -> xs
      | (x::xst, y::yst) -> 
          if lt x y then x::(merge lt xst ys)
          else y::(merge lt xs yst) ;;

  let rec sort (lt:'a->'a->bool) (xs:'a list) : 'a list = 
    match xs with 
      | ([] | _::[]) -> xs
      | _ -> let (first,second) = split xs [] [] in
          merge lt (sort lt first) (sort lt second) ;;
end

(* time_fun f returns a function which when run on its argument,
 * calculates the time (in seconds) for the function to run and
 * returns that time as a floating point number.  *)
let time_fun f = 
  fun x -> 
    let t0 = Unix.time() in 
    let ans = f x in 
    let t1 = Unix.time() in 
      (t1 -. t0)
;;

(* For example, we can now define msort and isort to be 
 * functions which return the total time taken for 
 * merge-sort and insertion-sort, given a list. *)
let msort : int list -> float = time_fun (MergeSort.sort (<)) ;;
let isort : int list -> float = time_fun (InsertSort.sort (<)) ;;

(* iterate is like reduce or fold_left for numbers -- we have
 * a function s that tells us what to do when we have a non-zero
 * number, a value z that tells us what to do when the number is
 * zero, and a number n which is how far we want to count. *)
let rec iterate s z n = 
  if n >= 0 then iterate s (s n z) (n-1) else z
;;

(* (gen n) generates the list of numbers from 0 to n *)
let gen = iterate (fun i rest -> i::rest) [];;

(* This function iterates a given function n times on the
 * same argument and calculates the total time needed for
 * the n iterations.  It then divides the total time by 
 * the number of iterations to get the cost of running the
 * function once.  This is useful when the function runs
 * faster than the resolution of our clock.  Technically, 
 * we should subtract the overhead of the loop.  But this
 * isn't so important for relative timings. *)
let iter_time_fun f arg n = 
  let t = time_fun (iterate (fun i _ -> f arg; ()) ()) n in
    t /. (float_of_int n)
;;

(* msort_time n m runs merge-sort on lists of length n for m
 * iterations, returning the average time taken to sort a list
 * of length n. *)
let msort_time n m = iter_time_fun (MergeSort.sort (<)) (gen n) m
;;

(* isort_time n m runs insertion-sort on lists of length n for m
 * iterations, returning the average time taken to sort a list
 * of length n. *)
let isort_time n m = iter_time_fun (InsertSort.sort (<)) (gen n) m
;;

(* This is useful for generating a graph of running times for
 * merge-sort on various list lengths. *)
let msort_times start step times iterations = 
  iterate (fun i _ -> Printf.printf "%d: %g\n" (i*step+start) 
             (msort_time (i*step+start) iterations)) () times;;

(* msort_times 0 1000 10 200 ;;
15000: 0.05
14000: 0.045
13000: 0.04
12000: 0.035
11000: 0.035
10000: 0.03
9000: 0.025
8000: 0.025
7000: 0.02
6000: 0.015
5000: 0.01
4000: 0.01
3000: 0.01
2000: 0
1000: 0.005
0: 0
 *)

(* This is useful for generating a graph of running times for
 * insertion-sort on various list lengths. *)
let isort_times start step times iterations = 
  iterate (fun i _ -> Printf.printf "%d: %g\n" (i*step+start) 
             (isort_time (i*step+start) iterations)) () times;;

(* isort_times 0 1000 10 1 ;;
10000: 16
9000: 12
8000: 9
7000: 7
6000: 4
5000: 3
4000: 2
3000: 1
2000: 0
1000: 0
0: 0
*)

(* Let's analyze the running times of the functions below *)
let rec append xs ys = 
  match xs with 
    | [] -> ys
    | h::t -> h::(append t ys)
;;

T_append 0 m = c1
T_append n m = c2 + T_append (n-1) m

T_append n m = 
   c2 + (c2 + (c2 + c2 + ... c2 + c1) =
   n*c2 + c1

Time to run append is O(n) where n is the length of the first list.

let rec rev xs = 
  match xs with 
    | [] -> []
    | h::t -> append (rev t) [h]
;;

T_rev 0 = k1
T_rev n = k2 + T_rev (n-1) + T_append (n-1) 1
        = k2 + T_rev (n-1) + c2*(n-1) + c1
        = k3 + c2*(n-1) + T_rev (n-1)

        = k3 + c2*(n-1) + (k3 + c2*(n-2) + T_rev (n-2))
        = k3 + c2*(n-1) + (k3 + c2*(n-2) + (k3 + c2*(n-3) + T_rev (n-3))

        = n*k3 + c2*(sum [0..n-1])
        = n*k3 + c2*(n * (n-1) / 2)
        = n*k3 + c2*n*n/2 + c2*n*-1/2
        O(n^2)

let rec revappend xs ys = 
  match xs with 
    | [] -> ys
    | h::t -> revappend t (h::ys)
;;

T_revappend 0 m = c1
T_revappend n m = c2 + T_revappend (n-1) (m+1)

in O(n * lg n)

T(n) = k*n + 2*T(n/2)   
    =  k*n + 2*k*(n/2) + 4*T(n/4)
    =  k*n + k*(n) + k*n + 8*T(n/8)

lg_2 n (k*n)

let rec fact n = 
  if n <= 0 then 1
  else n * (fact (n-1))

let fact n = 
  let rec fact_helper n a = 
    if n <= 0 then a
    else fact_helper (n-1) (n*a)
  in
    fact_helper n 1




Time to revappend is in O(n)

let rev2 xs = revappend xs []
;;

c + T_revappend n 0 

Time to run rev2 is O(n).

let append2 xs ys = revappend (rev2 xs) ys
;;

let rec flatten (xs:int list list) : int list = 
  match xs with 
    | [] -> []
    | h::t -> append2 h (flatten t)
;;

let flatten2 xs = List.fold_right append2 xs [] ;;

let append_time n = 
  iter_time_fun (fun (xs,ys) -> append xs ys) (let xs = gen n in (xs, xs))
;;

let append2_time n = 
  iter_time_fun (fun (xs,ys) -> append2 xs ys) (let xs = gen n in (xs, xs))
;;

let rev_time n = iter_time_fun rev (gen n)
;;

let rev2_time n = iter_time_fun rev2 (gen n)
;;

type 'a vector = 'a list ;;
type 'a matrix = ('a vector) list ;;

let dim(m:'a matrix) : (int*int) = 
  (List.length m, 
   match m with 
     | [] -> 0
     | h::_ -> List.length h)
;;

exception BoundsError ;;

let rec ith (i:int) (xs:'a list) : 'a = 
  match i, xs with 
    | _, [] -> raise BoundsError
    | 0, h::_ -> h
    | _, _::t -> ith (i-1) t
;;

let rec map f xs = 
  match xs with 
    | [] -> []
    | h::t -> (f h)::(map f t)
;;

let rec combine xs ys = 
  match xs, ys with 
    | [], [] -> []
    | h1::t1, h2::t2 -> (h1,h2)::(combine t1 t2)
    | _, _ -> raise BoundsError 
;;

let row (r:int) (m:'a matrix) : 'a vector = ith r m ;;
let sub (r:int) (c:int) (m:'a matrix) : 'a = ith c (row r m) ;;
let col (c:int) (m:'a matrix) : 'a vector = map (ith c) m ;;

let create (f: int -> int -> 'a) (rows:int) (cols:int) : 'a matrix = 
  iterate (fun i r -> 
             (iterate (fun j c -> (f i j)::c) [] (cols - 1))::r) 
    [] (rows - 1)
;;

let invert (m:'a matrix) : 'a matrix = 
  let (r,c) = dim m in 
    create (fun i j -> sub j i m) c r
;;

let add (m1:int matrix) (m2:int matrix) = 
  let (r, c) = dim m1 in 
    create (fun i j -> (sub i j m1) + (sub i j m2)) r c
;;

let add2 (m1:int matrix) (m2:int matrix) = 
  let r1 : (int list * int list) list = combine m1 m2 in
  let r2 : ((int * int) list) list = map (fun (x,y) -> combine x y) r1 in
    map (map (fun (a,b) -> a + b)) r2
;;

let add3 (m1:int matrix) (m2:int matrix) = 
    map (fun (x,y) -> map (fun (a,b) -> a+b) (combine x y)) 
      (combine m1 m2)
;;


