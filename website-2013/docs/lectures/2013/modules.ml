module type INT_STACK = 
  sig
    type stack
    val empty : unit -> stack
    val push  : int -> stack -> stack
    val is_empty : stack -> bool
    val pop : stack -> stack option
    val top : stack -> int option
    val stack2list : stack -> int list
  end

module ListIntStack : INT_STACK = 
  struct
    type stack = int list
    let empty () : stack = []
    let push (i:int) (s:stack) = i::s
    let is_empty (s:stack) =
      match s with
       | [] -> true
       | _::_ -> false
    let pop (s:stack) =
      match s with 
       | [] -> None
       | _::t -> Some t
    let top (s:stack) = 
      match s with 
       | [] -> None
       | h::_ -> Some h
    let stack2list(s:stack) = s
  end

module type QUEUE = 
  sig
    type 'a queue
    val empty : unit -> 'a queue
    val enqueue : 'a -> 'a queue -> 'a queue
    val is_empty : 'a queue -> bool
    exception EmptyQueue
    val dequeue : 'a queue -> 'a queue
    val front : 'a queue -> 'a
  end

module AppendListQueue : QUEUE = 
  struct
    type 'a queue = 'a list
    let empty() = []
    let enqueue(x:'a)(q:'a queue) : 'a queue = q @ [x]
    let is_empty(q:'a queue) = 
	   match q with
     | [] -> true
     | _::_ -> false
    exception EmptyQueue
    let deq(q:'a queue) : ('a * 'a queue) = 
      match q with 
      | [] -> raise EmptyQueue
      | h::t -> (h,t)
    let dequeue(q:'a queue) : 'a queue = snd (deq q)
    let front(q:'a queue) : 'a = fst (deq q)
end

module DoubleListQueue : QUEUE = 
  struct
    type 'a queue = {front:'a list; rear:'a list}
    let empty() = {front=[]; rear=[]}
    let enqueue x q = {front=q.front; rear=x::q.rear}
    let is_empty q = 
      match q.front, q.rear with
      | [], [] -> true
      | _, _ -> false
    exception EmptyQueue
    let deq (q:'a queue) : 'a * 'a queue = 
      match q.front with
      | h::t -> (h, {front=t; rear=q.rear})
      | [] -> match List.rev q.rear with
              | h::t -> (h, {front=t; rear=[]})
              | [] -> raise EmptyQueue
    let dequeue (q:'a queue) : 'a queue = snd(deq q)
    let front (q:'a queue) : 'a = fst(deq q)
  end
