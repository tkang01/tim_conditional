open Graphics ;;

class foobarbaz = 
object(self)
  method foo = Printf.printf "foo"; self#bar
  method bar = Printf.printf "bar"; 42
  method baz = Printf.printf "baz"; self#foo
end

class moobarbaz = 
object(self)
  inherit foobarbaz
  method foo = Printf.printf "moo"; self#bar
end

let x = new moobarbaz ;;
x#baz ;;


type point = {x:int; y:int} ;;

type display_elt = {
  draw : unit -> unit ;
  move_to : point -> unit ;
  set_color : color -> unit ;
  get_color : unit -> color 
};;


let circle (p:point) (r:int) : display_elt = 
  let pos = ref p in  
  let color = ref black in  
    { draw = 
        (fun () -> (set_color (!color) ; 
                   fill_circle (!pos).x (!pos).y r)) ;
      move_to = (fun p -> pos := p) ;
      set_color = (fun c -> color := c) ; 
      get_color = (fun () -> !color)
    }

class type ['a] container = 
object
  method insert : 'a -> unit
  method take : 'a option
  method size : int
end

class ['a] stack : ['a] container = 
object
  val mutable contents : 'a list = [] 

  method insert (x:'a) = contents <- x::contents
  method take = 
    match contents with 
      | [] -> None
      | h::t -> contents <- t ; Some h
  method size = List.length contents
end

class ['a] fast_lenth_stack : ['a] container = 
object
  inherit ['a] stack as super
  val mutable count = 0 
  method size = count
  method insert (x:'a) = count <- count + 1 ; super#insert x
  method take = 
    match super#take with 
      | None -> None
      | v -> count <- count - 1 ; v
end

type order = Less | Eq | Greater

class type ['a] set_elt = 
object ('self)
  method eq : 'a set_elt -> bool
  method value : 'a
end

class int_t (i:int) : [int] set_elt = 
object
  val contents : int = i
  method eq x = contents = x#value
  method value = contents
end

class type intset =
object 
  method is_empty : bool  
  method insert : int -> unit
  method remove : int -> unit
  method member : int -> bool  
  method union : intset -> unit
  method intersect : intset -> unit  
  method iterate : (int -> unit) -> unit
end

class virtual vintset = 
object (self)
  method virtual insert : int -> unit
  method virtual remove : int -> unit
  method virtual is_empty : bool
  method virtual member : int -> bool
  method virtual iterate : (int -> unit) -> unit
  method union (other : intset) = other#iterate (fun x -> self#insert x)
  method intersect (other : intset) = 
    self#iterate (fun x -> if not (other#member x) then self#remove x)
end

class list_intset : intset = 
object (self)
  inherit vintset as super
  val mutable contents : 'a list = []

  method is_empty = contents = []

  method member x = List.mem x contents

  method insert (x:'a) = 
    if self#member x then () else 
      contents <- x::contents

  method remove (x:'a) = 
    contents <- List.filter ((<>) x) contents

  method iterate f = List.iter f contents
end


class type ['a,'b] set =
object ('self)
  constraint 'a = 'b set_elt
  method is_empty : bool  
  method insert : 'a -> unit
  method remove : 'a -> unit
  method member : 'a -> bool  
  method union : ('a,'b) set -> unit
  method intersect : ('a,'b) set -> unit  
  method iterate : ('a -> unit) -> unit
end

class virtual ['a,'b] vset  = 
object (self)
  constraint 'a = 'b set_elt
  method virtual insert : 'a -> unit
  method virtual remove : 'a -> unit
  method virtual is_empty : bool
  method virtual member : 'a -> bool
  method virtual iterate : ('a -> unit) -> unit
  method union (other : ('a,'b) set) = other#iterate (fun x -> self#insert x)
  method intersect (other : ('a,'b) set) = 
    self#iterate (fun x -> if not (other#member x) then self#remove x)
end

class ['a,'b] list_set : ['a,'b] set = 
object 
  inherit ['a,'b] vset as super

  val mutable contents : 'a list = []

  method is_empty = 
    match contents with 
      | [] -> true
      | _::_ -> false

  method member (x:'a) = 
    let rec mem xs = 
      match xs with 
        | [] -> false
        | h::t -> if h#eq x then true else mem t
    in
      mem contents

  method insert (x:'a) = 
    let rec ins xs = 
      match xs with 
        | [] -> [x]
        | h::t -> if h#eq x then xs else h::(ins t)
    in
      contents <- ins contents

  method remove (x:'a) = 
    let rec rem xs = 
      match xs with 
        | [] -> []
        | h::t -> if h#eq x then t else h::(rem t)
    in
      contents <- rem contents

  method iterate f = 
    let rec iter xs = 
      match xs with 
        | [] -> ()
        | h::t -> (f h; iter t)
    in
      iter contents
end
