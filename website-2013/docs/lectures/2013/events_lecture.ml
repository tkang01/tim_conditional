exception ImplementMe

(* A possible interface for events *)
module type EVENT = 
sig
  type id
  type 'a event

  (* creates a new event *)
  val new_event : unit -> 'a event

  (* adds a listener function to the event, returning a
     unique id that can be used to later remove the 
     listener. The function will be invoked each time
     the event is fired.  There is no guarantee about
     the order in which the listeners are invoked, so the
     user should not assume a particular order. *)
  val add_listener : 'a event -> ('a -> unit) -> id

  (* Similar to add_listener, except the function is only
     called the first time the event is fired.  *)
  val add_one_shot : 'a event -> ('a -> unit) -> id

  (* Removes a listener identified by id from the event. *)
  val remove_listener : 'a event -> id -> unit

  (* Signals that the event has occurred *)
  val fire_event : 'a event -> 'a -> unit

  (* Some built-in events from the Graphics package. *)
  val key_pressed : char event
  val button_down : (int * int) event
  val button_up   : (int * int) event
  val mouse_motion : (int * int) event
  val clock : unit event

  (* Clears out any listeners from the primitive events. *)
  val clear_listeners : unit -> unit

  (* Used to terminate the run loop below *)
  val terminate : unit -> 'a

  (* Takes an initializer function f, sets up the graphics
     frame, invokes the initializer f, and then starts the
     event loop listening for primitive events, which then
     fire the built-in events listed above.  *)
  val run : (unit -> unit) -> unit
end

(* A possible implementation of events using the Graphics library *)
module Event : EVENT = 
struct
  (* We use integers to represent unique ids *)
  type id = int

  (* This function generates a fresh id each time it is called *)
  let new_id : unit -> id = 
    let x = ref 0 in
      fun () -> 
        (x := !x + 1 ; !x)

  (* We represent events as a mutable list of waiters, where
     a waiter is an id and a function to invoke when the event
     is fired. *)
  type 'a waiter = {id : id ; action : 'a -> unit}
  type 'a event = 'a waiter list ref

  (* Create a new event *)
  let new_event () : 'a event = ref []

  (* Add a listener to the event *)
  let add_listener (e:'a event) (f:'a -> unit) : id = 
    let waiter = {id = new_id() ; action = f} in
      e := waiter :: (!e) ; waiter.id

  (* Fire an event -- simply iterate over the waiters and invoke
     their functions on the given value. *)
  let fire_event (e:'a event) (v:'a) : unit = 
    List.iter (fun h -> h.action v) (!e)

  (* Remove a listener *)
  let remove_listener (e:'a event) (i:id) : unit =
    e := List.filter (fun w -> w.id <> i) (!e)

  (* Add a one-shot listener.  We basically add a function
     that removes itself. *)
  let add_one_shot (e:'a event) (f:'a -> unit) : id = 
    let i = new_id() in 
      e := {id=i; action=(fun v -> remove_listener e i; f v)} :: (!e) ; i

  (* Events for the primitive Graphics actions. *)
  let key_pressed : char event = new_event () 
  let button_down : (int * int) event = new_event ()
  let button_up : (int * int) event = new_event ()
  let mouse_motion : (int * int) event = new_event ()
  let clock : unit event = new_event () 

  let clear_listeners () = 
    key_pressed := [] ; button_down := [] ; button_up := [] ; 
    mouse_motion := [] ; clock := [] 

  (* The Graphics event interface doesn't really provide the right functionality
     so that we can do a blocking operation and determine what event occurred.  
     (For instance, we can't tell whether the mouse moved, or the mouse button
     was released.)  So we use a polling approach with a little bit of auxilliary
     state to figure out what event happened, and the corresponding value.  
     We then fire the appropriate high-level event. *)
  let mouse_state = ref false
  let mouse_pos = ref (0,0) 

  let read_event () = 
    (* if mouse position changes, fire mouse_motion *)
    let new_pos = Graphics.mouse_pos() in 
      if new_pos <> !mouse_pos then 
        (mouse_pos := new_pos ; fire_event mouse_motion (Graphics.mouse_pos())) ; 
      (* if a key was pressed, fire key_pressed *)
      if Graphics.key_pressed () then fire_event key_pressed (Graphics.read_key()) ; 
      (if not(!mouse_state) then 
         let s = Graphics.wait_next_event [Graphics.Button_down ; Graphics.Poll] in 
           if s.Graphics.button then
             (mouse_state := true ; fire_event button_down new_pos)) ;
      (if !mouse_state then
         let s = Graphics.wait_next_event [Graphics.Button_up ; Graphics.Poll] in 
           if not s.Graphics.button then
             (mouse_state := false ; fire_event button_up new_pos));
      fire_event clock ()

  (* Our basic event loop just calls read_event, which fires the appropriate
     events, then synchronizes the shadow graphics buffer with the screen,
     and then loops again. *)
  let rec event_loop () = read_event () ; Graphics.synchronize () ; event_loop () 

  (* Users can break out of the event loop by calling the terminate function *)
  exception Terminate
  let terminate() = raise Terminate

  (* Run is used to run a graphics app.  We clear out all old event handlers,
     open the graphics page, call the user's init function (which typically
     installs listeners for the events) and then enter the event loop.  If
     the user calls terminate within the event loop, then the exception
     Terminated will be raised and cause the loop to be terminated. *)
  let run init = 
    try 
      Graphics.open_graph "" ; 
      Graphics.auto_synchronize false ; 
      init () ; 
      event_loop ()
    with _ -> (Printf.printf "Terminating\n" ; Graphics.close_graph ())
end

(* An example which demonstrates all of the different primitive
   events.  You can use WASD (or wasd) to move the ball around, and press the 
   mouse button down to change the color, and release it to change it back. 
   We also display a clock tick count (i.e., frame count) in the upper-right
   corner of the frame, and the mouse position in the lower-left. *)
module MovingBall = 
struct
  open Graphics
  open Event 
  let x_pos = ref 250
  let y_pos = ref 250
  let size = ref 40
  let delta = 10
  let color = ref blue
  let inc r = r := !r + delta
  let dec r = r := !r - delta

  let draw_ball () = 
    set_color (!color) ; fill_circle (!x_pos) (!y_pos) (!size)

  let erase_ball () = 
    set_color white ; fill_circle (!x_pos) (!y_pos) (!size)

  let key_handler (c:char) = 
    erase_ball () ;
    (match c with 
      | 'a' | 'A' -> dec x_pos
      | 's' | 'S' -> dec y_pos
      | 'd' | 'D' -> inc x_pos
      | 'w' | 'W' -> inc y_pos
      | '+' -> inc size
      | '-' -> dec size
      | 'q' | 'Q' -> terminate()
      | _ -> ()) ; 
    draw_ball () 

  let button_down_handler _ = 
    color := red ; draw_ball ()

  let button_up_handler _ = 
    color := blue ; draw_ball () 

  let mouse_handler (x,y) = 
    let (tx,ty) = text_size "mouse @ x:0000 y:0000" in
    set_color white ; fill_rect 10 10 tx ty ; 
    set_color black ; moveto 10 10 ; 
    draw_string (Printf.sprintf "mouse @ x:%4d y:%4d" x y) 

  let clock_ticks = ref 0 

  let clock_handler () = 
    let x = Graphics.size_x () in
    let y = Graphics.size_y () in 
    let (tx,ty) = text_size "frames: 000000000" in 
    let px = x - tx in
    let py = y - ty in 
      clock_ticks := (!clock_ticks) + 1 ; 
      set_color white ; fill_rect px py tx ty ; 
      set_color black ; moveto px py ; 
      draw_string (Printf.sprintf "frames: %d" (!clock_ticks)) 

  let moving_ball () = 
    Event.clear_listeners() ; 
    ignore (add_listener key_pressed key_handler) ; 
    ignore (add_listener button_down button_down_handler) ; 
    ignore (add_listener button_up button_up_handler) ; 
    ignore (add_listener mouse_motion mouse_handler) ;
    ignore (add_listener clock clock_handler) ;
    Event.run draw_ball
end

(* This example uses a list of objects that move around randomly
   until the bounce into a wall, and then change direction.  You
   generate a new square by pressing 's' and a new ball by pressing
   'b'. *)
module BouncingItems = 
struct
  let rand = Random.self_init () ; Random.int
  let max_value = 500

  type direction = Neg | Same | Pos

  let random_direction () = 
    match rand 3 with 
      | 0 -> Neg
      | 1 -> Same
      | _ -> Pos

  let delta (x:int) (d:direction) : int = 
    match d with 
      | Neg -> if x = 0 then 0 else x - 1
      | Same -> x
      | Pos -> if x = max_value - 1 then max_value - 1 else x + 1

  (* Both balls and squares will inherit from this.  It captures
     the position, the velocity (what direction we're moving),
     and the color.  It also defines a move method which tries
     to move the position, but makes sure we don't cross the edges,
     and changes direction if needed.  Also, we make sure
     that the velocity isn't zero. *)
  class item = 
  object 
    val mutable x = rand max_value
    val mutable y = rand max_value
    val mutable dx = random_direction()
    val mutable dy = random_direction()
    val color = rand 0xffffff

    method move = 
      x <- delta x dx ; y <- delta y dy ;
      if x <= 0 || x >= max_value - 1 || y <= 0 || y >= max_value - 1
        || (dx = Same && dy = Same)
      then 
        (dx <- random_direction () ; 
         dy <- random_direction ())
  end

  (* The generic interface for our items *)
  class type item_t = 
  object
    method move : unit
    method draw : unit
  end

  (* the ball class -- inherits from item and just defines
     the draw method. *)
  class ball : item_t = 
  object
    inherit item
    method draw = 
      Graphics.set_color color ; Graphics.fill_circle x y 10
  end

  (* the square class is similar. *)
  class square : item_t = 
  object
    inherit item 
    method draw = 
      Graphics.set_color color ; Graphics.fill_rect x y 20 20
  end

  (* a list of all of the items we've generated. *)
  let items : item_t list ref = ref []
      
  (* on each clock tick, we clear the frame, move the items, 
     and then draw them again. *)
  let clock_handler () = 
    (Graphics.clear_graph () ; 
     List.iter (fun i -> i#move) !items ; 
     List.iter (fun i -> i#draw) !items)

  let key_handler c = 
    match c with 
      | 'b' | 'B' -> items := (new ball) :: !items
      | 's' | 'S' -> items := (new square) :: !items
      | 'q' | 'Q' -> Event.terminate()
      | _ -> ()

  let bounce() =
    Event.clear_listeners () ; 
    ignore (Event.add_listener Event.key_pressed key_handler);
    ignore (Event.add_listener Event.clock clock_handler) ; 
    Event.run 
      (fun () -> Graphics.resize_window (max_value+20) (max_value+20))
    
end

(* This is a slightly different version of the BouncingItems demo
   where we use an event to notify the objects to move and draw 
   themselves. *)
module BouncingItems2 = 
struct
  let rand = Random.self_init () ; Random.int
  let max_value = 500

  type direction = Neg | Same | Pos

  let random_direction () = 
    match rand 3 with 
      | 0 -> Neg
      | 1 -> Same
      | _ -> Pos

  let delta (x:int) (d:direction) : int = 
    match d with 
      | Neg -> if x = 0 then 0 else x - 1
      | Same -> x
      | Pos -> if x = max_value - 1 then max_value - 1 else x + 1

  (* This is a global event that we will fire on each clock tick.
     Each item will listen for this event and the move & draw itself. *)
  let item_clock = Event.new_event()
      
  (* This time, we define item as a virtual class.  Furthermore, as an
     initializer, we add an appropriate listener to the item_clock. *)
  class virtual item = 
  object (self)
    val mutable x = rand max_value
    val mutable y = rand max_value
    val mutable dx = random_direction()
    val mutable dy = random_direction()
    val color = rand 0xffffff

    method move = 
      x <- delta x dx ; y <- delta y dy ;
      if x <= 0 || x >= max_value - 1 || y <= 0 || y >= max_value - 1
        || (dx = Same && dy = Same)
      then 
        (dx <- random_direction () ; 
         dy <- random_direction ())

    method virtual draw : unit

    initializer 
      ignore (Event.add_listener item_clock (fun () -> self#move ; self#draw))
  end

  class ball = 
  object
    inherit item
    method draw = 
      Graphics.set_color color ; Graphics.fill_circle x y 10
  end

  class square = 
  object
    inherit item 
    method draw = 
      Graphics.set_color color ; Graphics.fill_rect x y 20 20
  end

  (* Question:  why don't we just add the objects to the clock_event,
     along with this handler that clears the frame?  *)
  let clock_handler () = 
    (Graphics.clear_graph () ; Event.fire_event item_clock ())

  let key_handler c = 
    match c with 
      | 'b' | 'B' -> ignore(new ball)
      | 's' | 'S' -> ignore(new square)
      | 'q' | 'Q' -> Event.terminate()
      | _ -> ()

  let bounce() =
    Event.clear_listeners() ;
    ignore (Event.add_listener Event.key_pressed key_handler);
    ignore (Event.add_listener Event.clock clock_handler) ; 
    Event.run 
      (fun () -> Graphics.resize_window (max_value+20) (max_value+20))
end

(* Here is an interface for some derived operations on events. *)
module type DERIVED_EVENT_OPERATIONS =
sig
  include EVENT

  (* If event e fires with values [v1,v2,v3,...] then 
     the event (map f e) fires with values [f v1, f v2, f v3, ...]
  *)
  val map : ('a -> 'b) -> 'a event -> 'b event

  (* The event (filter f e) fires with those values v fired from
     e such that (f v) = true. *)
  val filter : ('a -> bool) -> 'a event -> 'a event

  (* Interleaves the values fired from the two events into a single
     event.  For instance, if we fire e1 v1; fire e2 v2; fire e1 v3
     then on this event, we will see [v1,v2,v3] fired. *)
  val (++) : 'a event -> 'a event -> 'a event

  (* Waits for the first event to fire a value v1, and then waits
     for the second event to fire a value v2, and then fires with
     value (v1,v2). *)
  val ($) : 'a event -> 'b event -> ('a * 'b) event

  (* If the input event fires with (v1,v2) then the returned events
     fire with v1 and v2 respectively. *)
  val split : ('a * 'b) event -> ('a event) * ('b event)

  (* Similar to a "reduce".  If e fires with events [v1,v2,v3,...]
     then (reduce f u e) is an event that fires with 
     [u, f v1 u, f v2 (f v1 u), f v3 (f v2 (f v1 u)), ...]. *)
  val integrate : ('a -> 'b -> 'b) -> 'b -> 'a event -> 'b event
end

(* This module demonstrates some combinators that can be used to
   build new kinds of events out of other events.  Many of the ideas
   should look familiar... *)
module DerivedEventOperations = 
struct
  open Event

  let map (f: 'a -> 'b) (e : 'a event) : 'b event = 
    let n : 'b event = new_event () in 
      add_listener e (fun v : 'a -> fire_event n (f v)) ; 
      n 

  (* Or we could define a little notation for map this like: *)
  let (>>) (e:'a event) (f:'a -> 'b) = map f e

  (* Now we can write: *)
  let mouse_moved : unit event = mouse_motion >> make_unit

  let filter (f:'a -> bool) (e:'a event) : 'a event = 
    let n : 'a event = new_event () in 
      add_listener e (fun v -> if f v then fire n v else ()) ; 
      n 

  (* For example, suppose we want an event that corresponds to the 'a' key
     being pressed: *)
  let a_pressed : char event = filter ((=) 'a') key_pressed

  (* or, suppose we only want to be notified when the mouse
     moves into our area... *)
  let my_mouse_moved my_x my_y w h : (int * int) event =
    filter (fun (x,y) -> x >= my_x && x <= my_x + w 
                      && y >= my_y && y <= my_y + h) mouse_motion

  (* should return an event that fires when either e1 fires, or e2 fires. *)
  let (++) (e1 : 'a event) (e2 : 'a event) : 'a event = 
    let n = new_event () in 
      add_listener e1 (fire_event n) ; 
      add_listener e2 (fire_event n) ; 
      n 

  (* For example, suppose we want to know if 'a' is pressed or the
     mouse button is pressed. *)
  let a_or_button_pressed : unit event = 
    (a_pressed >> make_unit) ++ (button_down >> make_unit)

  let ($) (e1:'a event) (e2:'b event) : ('a * 'b) event = 
    let e = new_event () in 
      ignore (add_listener e1 
                (fun v1 -> 
                   ignore (add_one_shot e2 
                             (fun v2 -> fire_event e (v1,v2))))) ; 
      e

   (* With the above definition, if I wait for (e1 $ e2) then both 
      events have to fire (in order) before the resulting event fires 
      and then I get the output event. For example, a full mouse click
      could be modeled as:  *)
  let mouse_up_down : ((int*int) * (int*int)) event = 
    button_down $ button_up

  (* Split an event into two separate events *)
  let split (e: ('a * 'b) event) : ('a event) * ('b event) = 
    let e1 = new_event () in
    let e2 = new_event () in
      (e >> (fun (x,y) -> fire_event e1 x), e >> (fun (x,y) -> fire_event e2 x))

  (* Integrate is a little like reduce for lists. *)
  let integrate (f:'a -> 'b -> 'b) (u:'b) (e:'a event) : 'b event = 
    let e' = new_event() in 
    let rec loop (u:'b) (v:'a) = 
      fire_event e' u ; ignore (add_one_shot e (loop (f v u)))
    in 
      ignore (add_one_shot e (loop u)) ; 
      e'

   (* As you might expect, we can implement some of the other 
      combinators using integrate (just as we could implement
      many list functions, such as map and filter, using reduce.)  *)
  let map' f e = integrate (fun x _ -> f x) () e

   (* As another simple example, we could count the number of clock
      ticks that have passed with this: 
   *)
  let total_clock_ticks : int event = 
    integrate (fun () count -> count+1) 0 clock

   (* A more interesting example is that we may want to know the 
      average of all of the values that have been signaled on an
      event.  For instance, if we have a network event that tracks
      the stock price for a company, then we can produce a new
      event that tracks the average stock price for that company
      over time as follows:
   *)
  let average (e:int event) : float event = 
    (integrate (fun (i:int) (sum,count) -> (sum+i, count+1)) (0,0) e) >> 
      (fun (sum,count) -> (float_of_int sum) /. (float_of_int count))
end

module TestDerivedEvents = 
struct
  open Graphics
  open Event 
  open DerivedEventOperations

  let _ = clear_listeners()

  let delta = 10

  type ball_state = { ball_x : int ; ball_y : int ; 
                      ball_size : int ; ball_color : color }

  let initial_state = { ball_x = 250 ; ball_y = 250 ; 
                        ball_size = 40 ; ball_color = yellow }

  let draw_ball (s:ball_state) = 
    set_color (s.ball_color) ; fill_circle (s.ball_x) (s.ball_y) (s.ball_size)

  let erase_ball (s:ball_state) = 
    set_color white ; fill_circle (s.ball_x) (s.ball_y) (s.ball_size)

  let delta_x (i:int) (s:ball_state)  = {s with ball_x=s.ball_x + i}
  let delta_y (i:int) (s:ball_state) = {s with ball_y=s.ball_y + i}
  let delta_size (i:int) (s:ball_state) = {s with ball_size=s.ball_size + i}
  let change_color (c:color) (s:ball_state) = {s with ball_color=c}

  let quit = 
    filter ((=) 'q') key_pressed >> (fun _ s -> terminate ())

  let ball_actions = quit 

  let change_ball (f : ball_state -> ball_state) (old_state:ball_state) = 
    let new_state = f old_state in 
      erase_ball old_state ; 
      draw_ball new_state ; 
      new_state

  let ball : ball_state event = 
    integrate change_ball initial_state ball_actions

  let test () = 
    Event.run (fun _ -> draw_ball initial_state)
end
