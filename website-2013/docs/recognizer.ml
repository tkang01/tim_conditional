module StringUtil = 
struct
  (* convert a string to a list of characters *)
  let explode(s:string) : char list = 
    let rec loop i a = 
      if i >= 0 then loop (i-1) ((String.get s i) :: a) else a
    in
      loop (String.length s - 1) []

  (* convert a list of characters to a string *)
  let implode(cs:char list) : string =
    let n = List.length cs in 
    let s = String.create n in 
    let rec loop i cs = 
      if i >= n then s else 
        (String.set s i (List.hd cs); loop (i+1) (List.tl cs))
    in loop 0 cs
end

module type RECOGNIZER = 
sig
  type pattern_result
  type pattern = char list -> pattern_result

  (* matches any string *)
  val always : pattern
  (* never matches a string *)
  val never : pattern
  (* satisfy(f) matches strings with one character c 
   * such that (f c) = true. *) 
  val satisfy : (char -> bool) -> pattern
  (* p1 ^ p2 matches a string s if s can be broken into 
   * a prefix s1 that matches p1, and a suffix s2 that 
   * matches p2. *)
  val (^) : pattern -> pattern -> pattern
  (* p1 || p2 matches a string s if p1 matches s or else
   * p2 matches s. *)
  val (||) : pattern -> pattern -> pattern
  (* not(p) matches s if s does not match p. *)
  val not : pattern -> pattern
  (* ifp p1 p2 matches s if p1 matches -- only if p1 fails
   * to match do we try to match against p2.  So 
   * ifp p1 p2 = p1 || (not p1 ^ p2) *)
  val ifp : pattern -> pattern -> pattern
  
  (* matches p s returns true iff s matches the pattern p *)
  val matches : pattern -> string -> bool
  (* find p s returns a substring of s that maches p if any *)
  val find : pattern -> string -> string option
end

module Patterns(R:RECOGNIZER) = 
struct
  include R
  let ch(c:char) : pattern = satisfy (fun x -> x = c)
  let notCh(c:char) : pattern = satisfy (fun x -> x <> c)
  let a : pattern = ch 'a'
  let b : pattern = ch 'b'
  let space = ch ' '
  let tab = ch '\t'
  let newline = ch '\n'
  let linefeed = ch '\r'

  let digit : pattern = satisfy (fun x -> x >= '0' && x <= '9')
  let lc_alpha : pattern = satisfy (fun x -> x >= 'a' && x <= 'z')
  let uc_alpha : pattern = satisfy (fun x -> x >= 'A' && x <= 'Z')

  let alpha : pattern = lc_alpha || uc_alpha

  let greg : pattern = ch 'G' ^ ch 'r' ^ ch 'e' ^ ch 'g'

  let opt(p:pattern) : pattern = p || always

  let greg_ory : pattern = greg ^ opt(ch 'o' ^ ch 'r' ^ ch 'y')

  let alts(ps:pattern list) : pattern = List.fold_right (||) ps never

  let alpha_or_digit : pattern = alts [digit; lc_alpha; uc_alpha]

  let seqs(ps:pattern list) : pattern = List.fold_right (^) ps always

  let victor : pattern = seqs [ch 'V'; ch 'i'; ch 'c'; ch 't'; ch 'o'; ch 'r']

  let str(s:string) : pattern = seqs (List.map ch (StringUtil.explode s))

  let chelsea : pattern = str("Chelsea")

  let rec star(p:pattern) : pattern = 
    fun cs -> (ifp (p ^ (star p)) always) cs

  let plus(p:pattern) : pattern = p ^ (star p)

  let num = plus digit
  let word = plus alpha
  let var = lc_alpha ^ star(alpha || num || ch '_')

  let comment = 
    seqs [ch '('; ch '*'; star (notCh '*' || (ch '*' ^ notCh ')')) ; ch '*'; ch ')']

  let white = alts [space; tab; newline; linefeed; comment]
  let w = star white

  let op = alts [ch '+'; ch '-'; ch '*'; ch '/'; ch '<'; ch '='; ch '>'; 
                 str "=="; str "<>"; str "<="; str ">="; str "::" ]

  let rec atom cs = 
    (w ^ alts [ num ; 
                var ;
                ch '[' ^ w ^ ch ']' ; 
                str "let" ^ w ^ var ^ w ^ ch '=' ^ w ^ exp ^ str "in" ^ exp ;
                str "fun" ^ w ^ var ^ w ^ str "->" ^ w ^ exp ;
                str "if" ^ exp ^ str "then" ^ exp ^ str "else" ^ exp ;
                ch '(' ^ exp ^ ch ')' ] ^ 
       w) cs
      
  and exp cs = 
    (atom ^ alts [ op ^ exp ;
                   exp ; 
                   always ] ^ w) cs
end

module Recognizer1 : RECOGNIZER = 
struct
  type pattern_result = (char list) option
  type pattern = char list -> pattern_result

  let always : pattern = 
    fun cs -> Some cs

  let never : pattern = 
    fun cs -> None

  let satisfy(pred:char -> bool) : pattern = 
    fun cs -> 
      match cs with 
        | c::rest -> if pred c then Some rest else None
        | [] -> None
    
  let (^)(p1:pattern)(p2:pattern) : pattern = 
    fun cs -> 
      match p1 cs with 
        | Some cs' -> p2 cs'
        | None -> None

  let (||)(p1:pattern)(p2:pattern) : pattern = 
    fun cs -> 
      match p1 cs with 
      | Some cs' -> Some cs'
      | None -> p2 cs 

  let opt(p:pattern) : pattern = p || always

  let rec star(p:pattern) : pattern = 
    fun cs -> (p ^ (star p) || always) cs

  let not(p:pattern) : pattern = 
    fun cs -> 
      match p cs with 
        | None -> Some cs
        | Some _ -> None

  let ifp p1 p2 = p1 || p2

  let matches(p:pattern)(s:string) = 
    match p (StringUtil.explode s) with
      | Some [] -> true
      | Some _ -> false
      | None -> false

  let find(p:pattern)(s:string) = 
    let rec find_loop l = 
      match p l with 
        | None -> (match l with 
                   | [] -> None
                   | _::t -> find_loop t)
        | Some tail -> Some (String.sub (StringUtil.implode l) 0 
                             ((List.length l) - (List.length tail)))
    in
      find_loop (StringUtil.explode s)
end

module Pattern1 = Patterns(Recognizer1) 

(* A simple set module -- doesn't avoid duplicates *)
module Set = 
struct
  type 'a set = 'a list
  let empty : 'a set = []
  let singleton(x:'a) = [x]
  let union(s1:'a set)(s2:'a set) = s1 @ s2
  let rec fold f a s = 
    match s with 
      | [] -> a
      | h::t -> fold f (f h a) t
end  

module Recognizer2 = 
struct
  type pattern_result = (char list) Set.set
  type pattern = char list -> pattern_result

  let always : pattern = 
    fun cs -> Set.singleton cs

  let never : pattern = 
    fun cs -> Set.empty

  let satisfy(pred:char -> bool) : pattern = 
    fun cs -> (match cs with 
                 | c::rest -> if pred c then Set.singleton rest else Set.empty
                 | [] -> Set.empty)
    
  let (||)(p1:pattern)(p2:pattern) : pattern = 
    fun cs -> Set.union (p1 cs) (p2 cs)

  let opt(p:pattern) : pattern = p || always

  let (^)(p1:pattern)(p2:pattern) : pattern = 
    fun cs -> Set.fold (fun cs' r -> Set.union (p2 cs') r) Set.empty (p1 cs)

  let rec star(p:pattern) : pattern = 
    fun cs -> (p ^ (star p) || always) cs

  let not(p:pattern) : pattern = 
    fun cs -> (match p cs with 
                 | [] -> [cs]
                 | _::_ -> [])

  let ifp(p1:pattern)(p2:pattern) : pattern = 
    fun cs -> 
      match p1 cs with 
      | [] -> p2 cs
      | res -> res

  let matches(p:pattern)(s:string) = 
    List.exists (fun cs -> cs = []) (p (StringUtil.explode s))

  let find(p:pattern)(s:string) = 
    let rec find_loop l = 
      match List.rev (p l) with 
        | [] -> (match l with 
                   | [] -> None
                   | _::t -> find_loop t)
        | tail::_ -> Some (String.sub (StringUtil.implode l) 0 
                             ((List.length l) - (List.length tail)))
    in
      find_loop (StringUtil.explode s)
end

module Pattern2 = Patterns(Recognizer2) 

