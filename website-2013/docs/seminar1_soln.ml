let my_if = fun f -> fun a -> fun b -> f a b

let my_true = fun a -> fun b -> a

let my_false = fun a -> fun b -> b

let zero = fun s -> fun z -> z

let succ = fun n -> fun s -> fun z -> s (n s z)

let plus = fun m -> fun n -> fun s -> fun z -> m s (n f s) 

let ycomb = fun g -> (fun x -> g (fun y-> x x y))
  (fun x -> g (fun y -> x x y));;

type variable = string ;;

type caml_exp = 
  | Bool_e of bool
  | Var_e of variable
  | If_e of caml_exp * caml_exp * caml_exp
  | Fun_e of variable * caml_exp
  | FunCall_e of caml_exp * caml_exp
  | Let_e of variable * caml_exp * caml_exp
;; 

type lambda_exp =
  | Lambda_e of variable * lambda_exp
  | V_e of variable
  | App_e of lambda_exp * lambda_exp
;;

let l_if = Lambda_e ("f", Lambda_e ("a", Lambda_e ("b", 
                                             App_e (App_e (V_e "f", V_e "a"),
                                                    V_e "b"))))

let l_true = Lambda_e ("a", Lambda_e ("b", V_e "a"));;

let l_false = Lambda_e ("a", Lambda_e ("b", V_e "b"));;

let rec compile (e: caml_exp) : lambda_exp =
    match e with
      | Bool_e true -> l_true
      | Bool_e false -> l_false
      | Var_e v -> V_e v
      | If_e (c, e1, e2) -> App_e (App_e (l_if, compile e1), compile e2)
      | Fun_e (v, e) -> Lambda_e (v, compile e)
      | FunCall_e (e1, e2) -> App_e (compile e1, compile e2)
      | Let_e (v, e1, e2) -> App_e (Lambda_e (v, compile e2), compile e1)
;;

let my_and = Fun_e ("a", Fun_e ("b", If_e (Var_e "a", Var_e "b", Bool_e false)))

let my_or = Fun_e ("a", Fun_e ("b", If_e (Var_e "a", Bool_e true, Var_e "b")))

let my_not = Fun_e ("a", If_e (Var_e "a", Bool_e false, Bool_e true))

let my_xor = Fun_e ("a", Fun_e ("b", 
                    FunCall_e (FunCall_e(my_and, (FunCall_e (FunCall_e (my_and, Var_e "a"),
                                                  Var_e "b"))),
                                 (FunCall_e (my_not, (FunCall_e 
                                                         (FunCall_e 
                                                            (my_and, Var_e "a"),
                                                  Var_e "b")))))))
