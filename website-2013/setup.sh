wget --no-check-certificate https://forge.ocamlcore.org/frs/download.php/882/tuareg-2.0.6.tar.gz
wget http://caml.inria.fr/pub/distrib/ocaml-4.00/ocaml-4.00.0.tar.gz
tar -xzf ocaml-4.00.0.tar.gz
tar -xzf tuareg-2.0.6.tar.gz

cd ocaml-4.00.0
sed '19 c\
prefix=/usr' configure > configure2
mv configure2 configure
chmod 700 configure
./configure
make world
make opt
sudo make install
sudo chmod 755 /usr/bin/{mkcamlp4,{o,}caml*}
sudo find /usr/lib/ocaml -type d -print0 | xargs -0 sudo chmod 755
sudo find /usr/lib/ocaml -type f -print0 | xargs -0 sudo chmod 644
cd ..
rm -rf ocaml-4.00.0*
cd tuareg-2.0.6
make
mkdir -p ~/.elisp/tuareg-mode
mv -f * ~/.elisp/tuareg-mode/
touch ~/.emacs
echo "(add-to-list 'load-path \"~/.elisp/tuareg-mode\")
(load \"tuareg-site-file.el\")" >> ~/.emacs
cd ~
rm -rf tuareg-2.0.6*
echo "alias submit51=submit50" >> ~/.bashrc
