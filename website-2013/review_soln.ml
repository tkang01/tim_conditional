(* 1 Datatype Design *)
(* Design a datatype to describe bibliography entries for publications.  Some
 * publications are journal articles, others are books, and others are
 * conference papers.  Journals have a name, number and issue; books have an 
 * ISBN number; All of these entries should have a title and author. *)
(** Answer: **)


type journal =  {name : string ; number: int ; issue : int} 
type book = {isbn : int}

type publication = Journal of journal
                   | Book of book | Paper
type entry = {title: string ; author: string; pub: publication}















(* Higher Order Functions *)
(* 2: Using map, write a function that takes a list of pairs of integers,
 * and produces a list of the sums of the pairs.
 * e.g., list_add [(1,3); (4,2); (3,0)] = [4; 6; 3] *)

let list_add xs = map (fun (x,y) -> x+y) xs



(* 3: Using map, write a function that takes a list of pairs of integers,
 * and produces their quotient if it exists.
 * e.g., list_div [(1,3); (4,2); (3,0)] = [Some 0; Some 2; None] *)


let list_div xs = map (fun (x,y) -> if y = 0 then None else Some x/y) xs







(* The Substitution Model  *)
(* BY HAND, evaluate the following expresssions.
 * OR, if the expressions do not parse/type check,
 * explain why they do not.  *)

(* 4 *)
let rec mystery x = if x < 1 then 0 else 1 + (mystery (x - 1))
  in mystery 5;;

(** Answer: **)
5;;


(* 5 *)
let sum x = List.fold_right (+) x 0 in sum [1.5; 5.3; 6.2];;

(** Answer: Floats in list, but + expects ints **)


(* 6 *)
let mystery' x =
  List.fold_right
    (fun x y -> let ((a, b), (c, d)) = y in
       ((if x mod 2 == 0 then (x::a, b) else (a, x::b)),
	(if x > 0 then (x::c, d) else (c, x::d))))
    x (([], []), ([], [])) in
  mystery' [-3; -2; -1; 0; 1; 2; 3];;

(** Answer: **)
(([-2; 0; 2], [-3; -1; 1; 3]), ([1; 2; 3], [-3; -2; -1; 0])) ;;


	   

(* 7 *)
let x = 3 in
let x = 2 * x in
let (x, y) = (x + 1, x + 2) in
  x * y ;;


(** Answer: **)
56;;




(* 8 *)
let mystery'' x =
  List.fold_right
    (fun x y -> (x (match y with [] -> 0 | hd::tl -> hd))::y)
    x [] in
  mystery'' [( * ) 3; (/) 8; (+) 4]
;;

(** Answer: **)
[6; 2; 4];;

(* 9 *)
(* What about if we used List.fold_left? *)
let mystery'' x =
  List.fold_left
    (fun y x -> (x (match y with [] -> 0 | hd::tl -> hd))::y)
    [] x in
  mystery'' [( * ) 3; (/) 8; (+) 4]
;;


(* You may notice that we switched the order of x "x y" to be "y x"
 * in the anonymous function, and we switched the order of the last
 * two arguments to be "[] x" instead of "x []".  Don't worry too
 * much about this -- it's just an artifact of the way List.fold_left
 * is implemented in Ocaml.  We won't be testing you on such
 * technicalities; if we require you to make use of a higher order
 * functions, we will provide definitions of them for you. *)

(** Answer: Exception: Division by 0
    ** Because we multiply 0 by 3, and then try to divide 8 by that **)


(* 10 *)
let foo bar baz = baz bar bar in foo 4 (+);;

(** Answer: 8 **)


(* 11 *)
let foo bar baz = bar (fun x -> x * 3) baz in
  foo (fun x y -> x y) 4;;

(** Answer: 12 **)


(* 12 *)
type 'a silt = Empty | Member of 'a * 'a silt ;;

let rec mystery_9 f u gravel =
  (match gravel with
     | Empty -> u
     | Member(a, b) -> f a (mystery_9 f u b))
in mystery_9 (+) 0 (Member(5, Member(6, Member(7, Empty))));;

(** Answer: 18 **)
(** "silt" = "list", get it? **)


(* 13 *)
let mystery_10 x =
  let rec mystery_10' x y =
    if x == 0 then [x]
    else if y then x::(mystery_10' x false)
    else (- x)::(mystery_10' (x - 1) true)
  in mystery_10' x true
in mystery_10 4;;


(** Dampened oscillation! **)
(** Answer: [4; -4; 3; -3; 2; -2; 1; -1; 0] **)


(* Abstract Data Types and Modules *)

(* 14:  Terminology: Define and describe the relationships between
   "Abstract Data Type", "Implementation", "Signature", "Module", and "Interface" *)

(** 

   Note: all these terms are used with widely varying meanings in Real
   Life.  These are the interpretations we want for CS51.  Don't be
   surprised if you see the same words used in slightly different
   ways.

   Abstract data type: an abstract description of a data structure, in
   terms of the operations that may be performed on it, and the
   invariants that must hold.  Some examples are stacks, queues, priority
   queues, trees, etc. 
   
   Interface: the set of operations that can be invoked by a client of
   some abstraction.  We can refer to the interface of a module, the
   interface of a function, the interface of an operating system, and
   so on.

   Implementation: a concrete set of instructions for how to perform
   the operations in some interface.  Again, this is an abstract term
   that can refer to the implementation of a function, of some
   algorithm, of a module, etc.

   Signature: the ocaml mechanism for defining Abstract Data Types--they
   specify the types and values that must be provided by any
   implementation of that data type.

   Module: the ocaml mechanism for implementing abstract data types.
   Actually specifies the concrete representations of the types, and
   provides implementations of the values (often functions) provided.
**)









(* 15.  What is a functor and why might you use one? *)


(** A functor is a warm fuzzy thing.  It provides a function from
   modules to modules.  For example, in moogle you wrote a functor
   that, when given a module that implemented a dictionary interface,
   returned a new module that implemented a set interface.  **)







(* 16.  What's the major problem with this signature for undirected graphs? *)

module type UNDIRECTED_GRAPH =
  sig
    type node
    type graph
    val nodes : graph -> node list
    val edges : graph -> (node * node) list
    val neighbors : graph -> node -> node list
    val add_node : graph -> node -> graph
    val add_edge : graph -> node -> node -> graph
    val delete_node : graph -> node -> graph
    val delete_edge : graph -> node -> node -> graph
  end


(** There's no way to actually create a graph! **)

(* 17. While working on some OCaml code, your friend, who has missed a few CS51
lectures, tells you, "OK, I'll write an interface for undirected graphs in 
terms of edge lists, and then write a signature implementing it. Then, can 
you write a graph search function that takes one of these graph signatures?"
List at least two errors your friend is making in his terminology and his 
ideas about the use of abstraction. Your comments should not address the 
efficiency or practicality of his proposed representation. *)

(** Modules, not signatures, implement interfaces. Signatures are interfaces in
    OCaml. Functions cannot take modules as arguments, so this would be a graph
    search functor (such as the ones we wrote in section.) In theory, the
    function could take an edge list instead of a module if this representation
    were exposed in the interface, but this would be a poor use of abstraction,
    since the interface shouldn't give away the implementation but leave it up
    to the implementer, like the interface above does. **)



(* Complexity Analysis of Programs *)

(* 18. What does "Big-O of f(n)" mean? *)

(** O(f(n)) is the class of functions g(n) such that g(n) is
   asymptotically no larger than f(n).  More formally, it would
   include all g(n) such that the exist c,m such that for n>m, g(n) <=
   c*f(n)
**)





(* Answer these questions for the following examples:
 *   1) What is the recurrence relation for this function's running time?
 *   2) What is the asymptotic running time?
 *   3) Does the function utilize tail-calls?
 *   4) If not, could we easily rewrite it so it does? *)

(* 19 *)

let sum (lst:int list) = foldr (+) 0 lst ;;

(* T(n) = c + T(n-1)
 * O(n)
 * No
 * foldl (+) 0 lst *)

(* 20 *)

module BSTTreeDict(D:DICT_ARG) : (DICT with type key = D.key
                                      with type value = D.value) = 
  struct
     ...  (* imagine a correct implementation of balanced binary
             search trees here *)

    let rec lookup (d:dict) (k:key) : value option = 
      match d with
        | Empty -> None
        | Node n -> (match D.compare k n.key with
                       | Eq -> Some (n.value)
                       | Less -> lookup n.lnode k
                       | _ -> lookup n.rnode k)
    ;;

  end

(** 
 * 1) T(n) = c + T(n_subtree).  Because we're using Red-black trees,
 * we know that the tree is close to balanced, so n_subtree is roughly
 * n/2, so we can write T(n) = c + T(n/2).  (This is a bit handwavy,
 * but it's fine for cs51).
 * 
 * 2) O(log(n)) 
 * 
 * 3) Yes
 **)

(* Pointer: There are some more exercises like this at the bottom of
   timing.ml, linked from the lectures page (complexity lecture) *)
