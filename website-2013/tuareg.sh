cd ~
rm .emacs

wget --no-check-certificate https://forge.ocamlcore.org/frs/download.php/882/tuareg-2.0.6.tar.gz
tar -xzf tuareg-2.0.6.tar.gz

cd tuareg-2.0.6
make
mkdir -p ~/.elisp/tuareg-mode
mv -f * ~/.elisp/tuareg-mode/
touch ~/.emacs
echo "(add-to-list 'load-path \"~/.elisp/tuareg-mode\")
(load \"tuareg-site-file.el\")" >> ~/.emacs
cd ~
rm -rf tuareg-2.0.6*
echo "alias submit51=submit50" >> ~/.bashrc
