cd ~
curl -O http://cs51.seas.harvard.edu/graphics.zip
unzip graphics.zip
sudo mv graphics/g* /usr/lib/ocaml
sudo mv graphics/dllgraphics.so /usr/lib/ocaml/stublibs
sudo chmod 644 /usr/lib/ocaml/stublibs/dllgraphics.so
rm -rf graphics.zip
rm -rf graphics
