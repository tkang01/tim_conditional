#!/usr/bin/python
# CS 51 Tools
# Update leftnav accross all pages in this folder.
#
# DEPRECATED!!! Don't use this unless you talk to Evan first.
#
# Replaces the text between the <!-- leftnav --> comments in all html
# files with the content of 'addiframe.html'.

import os
import shutil
import tempfile

def writeLeftnavTo(htmlFile):
    """writes the content of 'leftnav.html' into the open file f"""
    leftnav = open('addiframe.html')
    htmlFile.write(leftnav.read())
    leftnav.close()

def replaceLeftnav(filename):
    htmlFile = open(filename)
    temp = tempfile.NamedTemporaryFile()
    line = htmlFile.readline()
    while line != '':
        temp.write(line)
        if line == '<!-- leftnav -->\n':
            writeLeftnavTo(temp)
            while line != '<!-- end leftnav -->\n':
                line = htmlFile.readline()
        else:
            line = htmlFile.readline()
    htmlFile.close()
    os.remove(filename)
    htmlFile = open(filename,'w')
    temp.seek(0)
    line = temp.readline()
    while line != '':
        htmlFile.write(line)
        line = temp.readline()
    temp.close()
    htmlFile.close()

DIRECTORIES = [os.getcwd(),
               os.getcwd() + '/ext']

if __name__ == '__main__':
    for filename in [dn + '/' + fn
                     for dn in DIRECTORIES
                     for fn in os.listdir(dn)]:
        if filename.endswith('.html') \
           and filename[:-5] != 'leftnav' \
           and filename[:-5] != 'addiframe':
            replaceLeftnav(filename)
