type 'a btree = Empty | Node of 'a * 'a btree * 'a btree

let rec stats' t tmax =
  match t with
    | Empty -> (0,tmax)
    | Node (v,l,r) ->
	let ((lh,lmax),(rh,rmax)) = (stats' l tmax, stats' r tmax) in
	  (1 + (max lh rh),max (String.length (string_of_int v))
	                       (max rmax lmax))

(* returns (max depth of tree, max length of the elements) *)
let stats (t:int btree) : (int * int) option =
  match t with
    | Empty -> None
    | Node (v,l,r) -> Some (stats' t (String.length (string_of_int v)))

let rec ws n = if n = 0 then "" else " " ^ (ws (n-1))
let pow a b = int_of_float ((float a) ** (float b))

let print (t:int btree) =
  match stats t with
    | None -> print_string "This tree is empty!"
    | Some (depth, lmax) ->
	let len = lmax + (lmax mod 2) in
	let add_pad s = (ws (len - (String.length s))) ^ s in
	let int2str i = add_pad (string_of_int i) in
	let array = Array.make (pow 2 depth) (add_pad "_") in
	let printi i = print_string (Array.get array i) in
	let rec tarray t i =
	  match t with
	    | Empty -> ()
	    | Node (v,l,r) -> (Array.set array i (int2str v);
			       tarray l (2 * i);
			       tarray r (2 * i + 1))
	in
	let rec tprint i level =
	  match (i < pow 2 (level + 1), level < depth) with
	  | (_,false) -> ()
	  | (false,_) -> (print_string "\n"; tprint i (level + 1))
	  | (true,_) ->
	      let num = pow 2 (depth - level - 1) in
	      let space = ws (num + (num-1) * len / 2) in
	      (print_string space; printi i;
	       print_string space; tprint (i+1) level)
	in
	  (tarray t 1; tprint 1 0)

let t1 = Node(5,Node(2,Node(1,Empty,Empty),Node(4,Empty,Empty)),
                Node(7,Node(6,Empty,Empty),Node(9,Empty,Empty)))

let t2 = Node(10,Node(2,Node(-13,Empty,Empty),Node(4,Empty,Empty)),
                 Node(16,Node(11,Empty,Empty),Node(20,Empty,Empty)))
