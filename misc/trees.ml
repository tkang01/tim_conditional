type pair = int * string ;;

(* alt represents a value of either type 'a or 'b *)
type ('a,'b) alt = Left of 'a | Right of 'b ;;

type order = Less | Equal | Greater

(* compareo is an algebraic version of compare *)
let compareo x y = 
  let c = compare x y in 
  if c < 0 then Less 
  else if c = 0 then Equal 
  else Greater
;;

(* a two-three tree *)
type tree = 
  | Null 
  | Two of tree * pair * tree 
  | Three of tree * pair * tree * pair * tree
;;

(* a context of a two-three tree. this is the type of taking a tree and
 * replacing a sub-tree with a hole.
 *)
type context =
  | Top
  | TwoLeftHole of pair * tree * context
  | TwoRightHole of tree * pair * context
  | ThreeLeftHole of pair * tree * pair * tree * context
  | ThreeMiddleHole of tree * pair * pair * tree * context
  | ThreeRightHole of tree * pair * tree * pair * context
;;

(* zip takes a context (which can be thought of as a tree with a hole), and a
 * subtree, and fills the hole with the subtree
 *)
let rec zip : tree -> context -> tree =
  fun t c -> match c with
  | Top -> t
  | TwoLeftHole(p,r,c') -> zip (Two(t,p,r)) c'
  | TwoRightHole(l,p,c') -> zip (Two(l,p,t)) c'
  | ThreeLeftHole(lp,m,rp,r,c') -> zip (Three(t,lp,m,rp,r)) c'
  | ThreeMiddleHole(l,lp,rp,r,c') -> zip (Three(l,lp,t,rp,r)) c'
  | ThreeRightHole(l,lp,m,rp,c') -> zip (Three(l,lp,m,rp,t)) c'
;;

let rec 
  (* insert an element into the two-three tree *)
  insert : int -> string -> tree -> context -> tree =
  fun k v tree context -> match tree with
  | Null -> insert_up (Null,(k,v),Null) context
  | Two(l,(k',v'),r) -> (
    match compareo k k' with
      | Less -> insert k v l (TwoLeftHole((k',v'),r,context))
      | Equal -> zip (Two(l,(k',v),r)) context
      | Greater -> insert k v r (TwoRightHole(l,(k',v'),context))
  )
  | Three(l,(lk,lv),m,(rk,rv),r) -> (
    match compareo k lk,compareo k rk with
      | Less,_ -> insert k v l (ThreeLeftHole((lk,lv),m,(rk,rv),r,context))
      | Equal,_ -> zip (Three(l,(lk,v),m,(rk,rv),r)) context
      | Greater,Less -> insert k v m (ThreeMiddleHole(l,(lk,lv),(rk,rv),r,context))
      | _,Equal -> zip (Three(l,(lk,lv),m,(rk,v),r)) context
      | _,Greater -> insert k v r (ThreeRightHole(l,(lk,lv),m,(rk,rv),context))
  )
and
  (* if insertion results in a subtree which is too tall, propegate it up into
   * its context
   *)
  insert_up : tree * pair * tree -> context -> tree =
  fun (l,p,r) context -> match context with
  | Top -> Two(l,p,r)
  | TwoLeftHole(p',r',context') -> zip (Three(l,p,r,p',r')) context'
  | TwoRightHole(l',p',context') -> zip (Three(l',p',l,p,r)) context'
  | ThreeLeftHole(lp,m,rp,r',context') -> insert_up (Two(l,p,r),lp,Two(m,rp,r')) context'
  | ThreeMiddleHole(l',lp,rp,r',context') -> insert_up (Two(l',lp,l),p,Two(r,rp,r')) context'
  | ThreeRightHole(l',lp,m,rp,context') -> insert_up (Two(l',lp,m),rp,Two(l,p,r)) context'
;;

let rec
  (* remove an element from the two-three tree *)
  remove : int -> tree -> context -> tree =
  fun k tree context -> match tree with
  | Null -> zip Null context
  | Two(l,(k',v),r) -> (
    match compareo k k' with
    | Less -> remove k l (TwoLeftHole((k',v),r,context))
    | Equal -> (
        match remove_greatest l (fun p -> TwoLeftHole(p,r,context)) with
        | None -> assert (r = Null) ; remove_up Null context
        | Some t -> t
    )
    | Greater -> remove k r (TwoRightHole (l,(k',v),context))
  )
  | Three(l,(lk,lv),m,(rk,rv),r) -> (
    match compareo k lk,compareo k rk with
    | Less,_ -> remove k l (ThreeLeftHole((lk,lv),m,(rk,rv),r,context))
    | Equal,_ -> (
        match remove_greatest l (fun p -> ThreeLeftHole(p,m,(rk,rv),r,context)) with
        | None -> assert (m = Null) ; assert (r = Null) ; zip (Two(Null,(rk,rv),Null)) context
        | Some t -> t
    )
    | Greater,Less -> remove k m (ThreeMiddleHole(l,(lk,lv),(rk,rv),r,context))
    | _,Equal -> (
        match remove_greatest m (fun p -> ThreeMiddleHole(l,(lk,lv),p,r,context)) with
        | None -> assert (l = Null) ; assert (r = Null) ; zip (Two(Null,(lk,lv),Null)) context
        | Some t -> t
    )
    | _,Greater -> remove k r (ThreeRightHole(l,(lk,lv),m,(rk,rv),context))
  )
and
  (* removes the greatest element in a tree and if it results in a tree which is
   * too short it propegats the gap into a provided context
   *)
  remove_greatest : tree -> (pair -> context) -> tree option =
  fun t k -> match t with
  | Two(l,p,r) -> 
      if l = Null 
      then (assert (r = Null) ; Some(remove_up Null (k p)))
      else remove_greatest r (fun p' -> TwoRightHole(l,p,k p'))
  | Three(l,lp,m,rp,r) ->
      if l = Null
      then (assert (m = Null) ; assert (r = Null) ; Some(zip (Two(Null,lp,Null)) (k rp)))
      else remove_greatest r (fun p' -> ThreeRightHole(l,lp,m,rp,k p'))
  | Null -> None
and
  (* if remove results in a tree which is too short, propegate the gap into the
   * context *)
  remove_up : tree -> context -> tree =
  fun t context -> match context with
  | Top -> t
  | TwoLeftHole(p,Two(l',p',r'),context') -> remove_up (Three(t,p,l',p',r')) context'
  | TwoRightHole(Two(l',p',r'),p,context') -> remove_up (Three(l',p',r',p,t)) context'
  | TwoLeftHole(p,Three(l',lp',m',rp',r'),context') -> zip (Two(Two(t,p,l'),lp',Two(m',rp',r'))) context'
  | TwoRightHole(Three(l',lp',m',rp',r'),p,context') -> zip (Two(Two(l',lp',m'),rp',Two(r',p,t))) context'
  | ThreeLeftHole(lp,Two(l',p',r'),rp,r,context') -> zip (Two(Three(t,lp,l',p',r'),rp,r)) context'
  | ThreeMiddleHole(Two(l',p',r'),lp,rp,r,context') -> zip (Two(Three(l',p',r',lp,t),rp,r)) context'
  | ThreeMiddleHole(l,lp,rp,Two(l',p',r'),context') -> zip (Two(l,lp,Three(t,rp,l',p',r'))) context'
  | ThreeRightHole(l,lp,Two(l',p',r'),rp,context') -> zip (Two(l,lp,Three(l',p',r',rp,t))) context'
  | ThreeLeftHole(lp,Three(l',lp',m',rp',r'),rp,r,context') -> zip (Three(Two(t,lp,l'),lp',Two(m',rp',r'),rp,r)) context'
  | ThreeMiddleHole(Three(l',lp',m',rp',r'),lp,rp,r,context') -> zip (Three(Two(l',lp',m'),rp',Two(r',lp,t),rp,r)) context'
  | ThreeMiddleHole(l,lp,rp,Three(l',lp',m',rp',r'),context') -> zip (Three(l,lp,Two(t,rp,l'),lp',Two(m',rp',r'))) context'
  | ThreeRightHole(l,lp,Three(l',lp',m',rp',r'),rp,context') -> zip (Three(l,lp,Two(l',lp',m'),rp',Two(r',rp,t))) context'
  | TwoLeftHole(_,Null,_) 
  | TwoRightHole(Null,_,_)
  | ThreeLeftHole(_,Null,_,_,_)
  | ThreeMiddleHole(Null,_,_,_,_)
  | ThreeRightHole(_,_,Null,_,_) -> failwith "impossible"
;;
