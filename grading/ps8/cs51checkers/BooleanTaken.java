package cs51checkers;

import java.util.Arrays;

//TODO: Rewrite this with a 64-bit integer.
public class BooleanTaken implements Taken {
    private boolean[][] grid;
    
    public BooleanTaken(int size) {
    	grid = new boolean[size][size];
    }
    
    private BooleanTaken(boolean[][] grid) {
    	this.grid = grid;
    }
    
    public boolean isTaken(Location l) {
    	return grid[l.ROW][l.COL];
    }
    public boolean isTaken(int r, int c) {
    	return grid[r][c];
    }
    
    public Taken take(Location l) {
    	boolean[][] newArray = copy();
    	newArray[l.ROW][l.COL] = true;
    	return new BooleanTaken(newArray);
    }
    
    public Taken unTake(Location l) {
    	boolean[][] newArray = copy();
    	newArray[l.ROW][l.COL] = false;
    	return new BooleanTaken(newArray);
    }

    public boolean equals(Taken other) {
	for (int i = 0; i < Board.BOARD_SIZE; i++) {
	    for (int j = 0; j < Board.BOARD_SIZE; j++) {
    		if (grid[i][j] != other.isTaken(i,j)) {
	    	    return false;
	    	}
	    }
	}
	return true;
    }
    
    private boolean[][] copy() {
    	//return Arrays.copyOf(grid, grid.length);
    	boolean[][] copied =  new boolean[grid.length][grid.length];
    	for(int i = 0; i < grid.length; i++) {
    		for(int j = 0; j < grid.length; j++) {
    			copied[i][j] = grid[i][j];
    		}
    	}
    	return copied;
    }
    public String toString() {
    	String ret = "";
    	for (boolean[] row : grid) {
    	    for (boolean col : row) {
    		    ret += col ? '1' : '0';
    	    }
    	    ret += '\n';
    	}
    	return ret;
     }
}
