package cs51checkers;

import java.io.IOException;
import java.util.Random;
import java.util.ArrayList;
import java.io.*;


public class ConsolePlayer implements Player {
	BufferedReader reader = new BufferedReader (new InputStreamReader(System.in));

	public ConsolePlayer() {
	}
	
	private int readInt(int low, int high) {
		int n=0;
		try {
			n = Integer.parseInt(reader.readLine());
		}
		catch (IOException e) {
			n=-1;
		}
		catch (NumberFormatException e2) {
			n=-1;
		}
		while ((n<low)||(n>high)) {
			System.out.println("Invalid Input. Input should be in the range " + Integer.toString(low) + "-" + Integer.toString (high) + ".");
			try {
				n = Integer.parseInt(reader.readLine());
			}
			catch (IOException e) {
				n=-1;
			}
			catch (NumberFormatException e2) {
				n=-1;
			}
		}
		return n;
	}
	
	public Move play(Board board, Side side) {
	    //TODO: Implement this method.
            return null;
	}
}
