package cs51checkers;

import java.util.Random;
import java.util.ArrayList;

public class DummyPlayer implements Player {
	private Random myRandom;
	
	public DummyPlayer() {
		myRandom = new Random();
	}
	
	public Move play(Board board, Side side) {
		ArrayList<Move> allPoss = new ArrayList<Move>();
		for(int i = 0; i < Board.BOARD_SIZE; i++) {
			for(int j = 0; j < Board.BOARD_SIZE; j++) {
				allPoss.addAll(board.legalMoves(new Location(i, j)));
			}
		}
		return allPoss.get(myRandom.nextInt(allPoss.size()));
	}
}
