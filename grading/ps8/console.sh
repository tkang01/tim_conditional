#!/usr/bin/env bash

cd submissions/$1/

javac cs51checkers/*.java 

java cs51checkers.Checkers ConsolePlayer DummyPlayer 0 f

rm -f *.class

cd ../../