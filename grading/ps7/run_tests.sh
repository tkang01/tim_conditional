#!/bin/bash


# Usage: ./runtests.sh <fas username>

set -o nounset
# set -e

REPOBASE=../../../..
TESTSFOLDER=$REPOBASE/hw/7/tests
JUNITHOME=$REPOBASE/bin

TEMP_DIR=student_tests
OFILE=ps7_auto.txt


echo "RUNNING PS7 GRADING ON STUDENT $1"

echo "Setting up Junit."
export CLASSPATH=$JUNITHOME/junit-4.8.2.jar:
echo $CLASSPATH

echo "Going to directory submissions/$1"
cd submissions/$1

rm *compiler_output.txt

echo "First, trying to compile students' code.  If this breaks, we give up."
# Compile student's code and redirect stderr to stdout so that we can save the
# output.
compiler_output=`javac *.java 2>&1`
if [ $? -ne 0 ] ; then
    echo "COMPILER ERROR WITH STUDENT CODE: $1"
    echo "Submitted code did not compile.  Score is 0 until fixed." > $OFILE
    echo "$compiler_output" > compiler_output.txt
    exit 1
fi

echo "Moving any student tests to a temp folder."
mkdir $TEMP_DIR
mv *Test*.java $TEMP_DIR/
mv Main.java $TEMP_DIR/

echo "Problem Set 7 Grade Report" >  $OFILE
echo "Student: $1"                >> $OFILE
echo "Total Score:     /120"      >> $OFILE
echo -e "\n\n"                    >> $OFILE

for section in shapes expressions
do
    echo "Running tests for $section"
    folder="$TESTSFOLDER/$section"

    echo "$section: Copying in test files."
    cp $folder/*.java .
    echo "$section: Now, trying to compile..."
    echo "**** TEST OUTPUT FOR SECTION: $section *** " >> $OFILE
    
    compiler_output=`javac *.java 2>&1`
    if [ $? -ne 0 ] ; then
        echo "$1 had compiler errors in $section."
        echo "$compiler_output" > ${section}_compiler_output.txt
        echo "Compiler error building staff tests with your code." >> $OFILE
        echo "Notes about what went wrong: " >> $OFILE
        echo -e "\n\n" >> $OFILE
    else
        echo "Invoking the test runner..."
        if [ "$section" = "expressions" ] ;
        then
            java ExpressionsTestsRunner >> $OFILE || true
        elif [ "$section" = "shapes" ] ;
        then
            java ShapesTestsRunner >> $OFILE || true
        fi
        echo "Done invoking the test runner."
    fi
    
    echo -e "\n\n" >> $OFILE

    echo "**** GRADE SUMMARY FOR SECTION: $section *** " >> $OFILE
    if [ "$section" = "shapes" ] ; then
        echo "Given the similarity of the methods across the various " >> $OFILE
        echo "classes, scores are per-method, where some points will " >> $OFILE
        echo "be lost if the method is not implementd in all classes." >> $OFILE
        echo "We reserve 5 'miscellaneous' points for other mistakes." >> $OFILE
        echo "Scores: " >> $OFILE
        echo "perimeter:        / 5 " >> $OFILE
        echo "area:             / 5 " >> $OFILE
        echo "scale:            / 5 " >> $OFILE
        echo "translate:        / 5 " >> $OFILE
        echo "boundingBox:      / 5 " >> $OFILE
        echo "misc score:       / 5 " >> $OFILE
        echo "SHAPES TOTAL:     / 30 " >> $OFILE
        echo "" >> $OFILE
    elif [ "$section" = "expressions" ] ; then
        echo "Again, points are awarded per piece of functionality," >> $OFILE
        echo "rather than per class."  >> $OFILE
        echo "VarValLists:          /15 " >> $OFILE
        echo "eval:                 /10 " >> $OFILE
        echo "derivative...       " >> $OFILE
        echo "      [Add,Sub,Sin,Cos,Neg     /5 ]" >> $OFILE
        echo "      [Pow,Var                /12 ]" >> $OFILE
        echo "      [Ln,Mult,Div            /8  ]" >> $OFILE
        echo "derivative total:     /25" >>  $OFILE
        echo "misc score:           /10 " >> $OFILE
        echo "EXPRESSIONS TOTAL:    /60" >>  $OFILE
    fi
    echo "Comments: " >> $OFILE
    echo -e "\n\n\n\n\n" >> $OFILE

    echo "$section: Cleanup after section $section"
    echo "$section: Removing all class files."
    rm *.class

    echo "$section: Removing all of of our test files."
    rm *Test*.java
done

echo "*** GRADE FOR: REFLECTION ***" >> $OFILE
echo "Score:         / 5 " >> $OFILE
echo "Comments: "           >> $OFILE
echo -e "\n\n\n\n"          >> $OFILE

echo "*** GRADE FOR: TESTING ***"      >> $OFILE
echo "Score:         / 15 " >> $OFILE
echo "Comments: "           >> $OFILE
echo -e "\n\n\n\n"          >> $OFILE


echo "*** GRADE FOR: STYLE ***"        >> $OFILE
echo "Score:        / 10 "  >> $OFILE
echo "Comments: "           >> $OFILE
echo -e "\n\n\n\n"          >> $OFILE

echo "Final Cleanup"
echo "Moving student tests back and deleting temp folder."
mv $TEMP_DIR/*.java .
rm -r $TEMP_DIR

echo "DONE RUNNING PS7 GRADING ON STUDENT $1"
