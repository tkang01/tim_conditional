#!/bin/bash

echo "Clean up a folder after a failed attempt to run tests in."
echo "Runs svn update to restore deleted files, just in case"

cd submissions/$1

rm *.class
rm *Test*.java

mv student_tests/*.java .
rm -r student_tests

svn up
