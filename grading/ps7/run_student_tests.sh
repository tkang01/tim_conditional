
REPOBASE=../../../..
TESTSFOLDER=$REPOBASE/hw/7/tests
JUNITHOME=$REPOBASE/bin


export CLASSPATH=$JUNITHOME/junit-4.8.2.jar:


cd submissions/$1
javac *.java

java junit.textui.TestRunner Tests

