#!/bin/bash

for submission in submissions/* ; do
    username=`basename $submission`
    ./run_tests.sh $username
done
