import subprocess
import sys

print "Usage: python time_script.py pattern /path/to/dir"
print "Example: python time_script.py minutes_spent ../ps2/"
print ""

pattern = '"' + sys.argv[1] + '"'
dir = sys.argv[2]
grep = 'grep -hi ' + pattern + ' `find ' + dir + ' -name "*.ml"` 2>/dev/null | cut -d = -f 2 | awk -F ";" \'{print $1}\''

s = subprocess.check_output(grep, shell=True)

print "People who wrote strange syntax:"

sum = 0
count = 0
hist = {}
for i in s.splitlines():
    try:
        add = eval(i)
        if type(add) is str:
            print i
            continue
        if add in hist:
            hist[add] = hist[add] + 1
        else:
            hist[add] = 1
        if add != 0 and add <= 2000:
            sum += add
            count += 1
    except:
        print i

print "Average Time Spent: ",

print sum / count

import numpy as np
import matplotlib.pyplot as plt
from collections import OrderedDict

hist2 = OrderedDict(sorted(hist.items(), key=lambda x: x[0]))

alphab = hist2.keys()
frecuencies = hist2.values()

pos = np.arange(len(alphab))
width = 1.0     # gives histogram aspect to the bar diagram

ax = plt.axes()
ax.set_xticks(pos + (width / 2))
ax.set_xticklabels(alphab)

plt.bar(pos, frecuencies, width, color='r')
plt.show()

