open Unix
open Ps3

(* Printing to stderr *)
let error = Printf.eprintf ;;

(* Point values *)
let p11_max = 5 ;;
let p12_max = 10 ;;
let p13_max = 10 ;;
let p14_max = 10 ;;
let p15_max = 15 ;;

(* Stat code *)
type _stats = {part: string; 
	       mutable timeouts: int; 
	       mutable variants: int;
	       mutable failures: int; 
	       mutable tests: int;
	       mutable max_pts: int;} ;;

let init_stats (part: string) (pts: int) : _stats =
  let stats = {part = part;
	       timeouts = 0;
	       variants = 0;
	       failures = 0;
	       tests = 0;
	       max_pts = pts;}
  in
  stats
;;

let timeout_stats (s: _stats) : unit =
  s.timeouts <- -1;
  s.variants <- -1;
  s.failures <- -1;  
  s.tests <- -1; 
  ()
;;

let statistics = [];;

(* Timeout *)
exception Timeout;;
let sigalrm_handler = Sys.Signal_handle (fun _ -> raise Timeout) ;;
(* Use lazy ness to allow for any type of function - i.e. number of args *)
(* We don't want to have to deal with magic values either *)
let timeout f time =
    let old_behavior = Sys.signal Sys.sigalrm sigalrm_handler in
    let reset_sigalrm () = Sys.set_signal Sys.sigalrm old_behavior in
    ignore (Unix.alarm time);
    try  
      let res = f () in 
      reset_sigalrm (); Some res  
    with exc -> reset_sigalrm () ;
      if exc = Timeout then None else raise exc 
;;

let timed_part stats f time =
  match timeout f time with
    | None -> timeout_stats stats
    | _ -> ()
;;

(* Checks representation invariant *)
let repOK (b: bignum) : bool =
  let rec all_less_than_base coeffs =
    match coeffs with
      | [] -> true
      | h::t -> (h < base) && (h >= 0) && (all_less_than_base t) 
  in
  match b.coeffs with
    | [] -> not b.neg (* If 0, neg = false *)
    | 0::_ -> false (* No leading zeroes *)
    | coeffs -> all_less_than_base coeffs (* coeffs in range [0,base) *)
;;

(** Some helpful string functions **)
(* Splits a string into a list of its characters. *)
let rec explode (s: string) : char list =
  let len = String.length s in
  if len = 0 then []
  else (s.[0])::(explode (String.sub s 1 (len - 1)))

(* Condenses a list of characters into a string. *)
let rec implode (cs: char list) : string =
  match cs with
    | [] -> ""
    | c::t -> (String.make 1 c)^(implode t)

(** Other funtions you may find useful. *)
(* Returns the first n elements of list l (or the whole list if too short) *)
let rec take_first (l: 'a list) (n: int): 'a list =
  match l with
    | [] -> []
    | h::t -> if n <= 0 then [] else h::(take_first t (n - 1))

(* Returns a pair
 * (first n elements of lst, rest of elements of lst) *)
let rec split lst n =
  if n <= 0 then ([], lst)
  else match lst with
    | [] -> ([], [])
    | h::t -> let (lst1, lst2) = split t (n-1) in
        (h::lst1, lst2)

(* Removes zero coefficients from the beginning of the bignum representation *)
let rec stripzeroes (b: int list) : int list =
  match b with
    | 0::t -> stripzeroes t
    | _ -> b

(* Returns the floor of the base 10 log of an integer *)
let intlog (base: int): int =
  int_of_float (log10 (float_of_int base))
;;

(* Converts a bignum to its string representation.
 * Returns a string beginning with ~ for negative integers. *)
let toString (b: bignum): string =
  let rec pad_with_zeroes_left (s: string) (len: int) =
    if (String.length s) >= len then s else
      "0"^(pad_with_zeroes_left s (len - 1))
  in
  let rec stripstrzeroes (s: string) (c: char) =
    if String.length s = 0 then "0" else
    if (String.get s 0) = '0' then 
      stripstrzeroes (String.sub s 1 ((String.length s) - 1)) c
    else s
  in
  let rec coeffs_to_string (coeffs: int list): string =
    match coeffs with
      | [] -> ""
      | h::t -> (pad_with_zeroes_left (string_of_int h) (intlog base))^
          (coeffs_to_string t)
  in
  let stripped = stripzeroes b.coeffs in
    if List.length stripped = 0 then "0" else
      let from_coeffs = stripstrzeroes (coeffs_to_string stripped) '0' in
        if b.neg then "~"^from_coeffs else from_coeffs
;;

(* Use to avoid maldefined fromString in their code. *)
let fromString (s: string): bignum =
  let rec fromString_rec (cs: char list) : int list =
    if cs = [] then [] else
    let (chars_to_convert, rest) = split cs (intlog base) in
    let string_to_convert = implode (List.rev chars_to_convert) in
      (int_of_string string_to_convert)::(fromString_rec rest)
  in
  match explode s with
    | [] -> fromInt 0
    | h::t -> if (h = '-')||(h = '~') then 
        {neg = true; coeffs = stripzeroes (List.rev (fromString_rec (List.rev t)))}
      else {neg = false; coeffs = stripzeroes (List.rev (fromString_rec (List.rev (h::t))))}
;;

(* Done seperately to allow for repOK on return value *)
let timed_test_bignum (stats: _stats) f (exp: bignum) (s: string) : unit =
  stats.tests <- stats.tests + 1;
  let time = 1 in
  match timeout f time with
    | None -> (stats.timeouts <- stats.timeouts + 1;
	       error "Timeout (>%ds) - %s\n" time s)
    | Some res ->
      if not (repOK res) then (stats.variants <- stats.variants + 1;
			       error "Rep. Inv. Broken: %s\n" s)
      else if res <> exp then (stats.failures <- stats.failures + 1;
			       error "Incorrect Answer: %s\n" s)
      else ()
;;

let timed_test (stats: _stats) f exp (s: string) : unit =
  stats.tests <- stats.tests + 1;
  let time = 1 in
  match timeout f time with
    | None -> (stats.timeouts <- stats.timeouts + 1;
	       error "Timeout (>%ds): %s\n" time s)
    | Some res ->
      if res <> exp then (stats.failures <- stats.failures + 1;
			  error "Incorrect Answer: %s\n" s)
      else ()
;;

(* Problem 1.1 *)
let stats1_1 = init_stats "Part 1.1" p11_max ;;
let statistics = stats1_1::statistics;;

let negate_test (s1: string) (sexp: string) : unit = 
  let b1 = fromString s1 in
  let exp = fromString sexp in
  timed_test_bignum stats1_1 (fun _ -> negate b1) exp ("negate " ^ s1)
;;

(* 10 second time limit overall *)
timed_part stats1_1 (fun _ ->
  negate_test "~1" "1";
  negate_test "1" "~1";
  negate_test "0" "0"
) 10;;

(* Overide w/ our code *)
let negate (b: bignum) : bignum =
  if b.coeffs = [] then b 
  else {neg = not b.neg; coeffs = b.coeffs}
;;

(* Problem 1.2 *)
let stats1_2 = init_stats "Part 1.2" p12_max;;
let statistics = stats1_2::statistics;;

let equal_test (s1: string) (s2: string) (exp: bool) =
  let b1 = fromString s1 in
  let b2 = fromString s2 in
  timed_test stats1_2 (fun _ -> equal b1 b2) exp ("equal " ^ s1 ^ " " ^ s1)
;;

timed_part stats1_2 (fun _ ->
  equal_test "12345" "12345" true;
  equal_test "1" "~1" false;
  equal_test "1073741824" "~1073741824" false;
  equal_test "100200300" "~100200300" false;
  equal_test "~100200300" "100200300" false;
  equal_test "100200300" "100200300" true;
) 8;;

let less_test (s1: string) (s2: string) (exp: bool) : unit =
  let b1 = fromString s1 in
  let b2 = fromString s2 in
  let test b1 b2 exp = 
    timed_test stats1_2 (fun _ -> less b1 b2) exp 
      ("less " ^ toString b1 ^ toString b2)
  in
  (* avoid redundant tests *)
  if not (equal b1 b2) then
    (test b2 b1 (not exp);
     test (negate b2) b1 true;
     test b2 (negate b1) false;
     test (negate b2) (negate b1) exp);
  if not (b1.coeffs = []) then
    (test (negate b1) b2 true;
     test b1 (negate b2) false;
     if not (equal b1 b2) then test (negate b1) (negate b2) (not exp)
     else test (negate b1) (negate b2) false);
  test b1 b2 exp;
;;

timed_part stats1_2 (fun _ ->
  (* Zero Cases *)
  (* error "Zero Cases\n";; *)
  less_test "0" "0" false;
  less_test "0" "1" true; 
  less_test "0" "1000" true;
  less_test "0" "1001" true;

  (* Single Digit Cases *)
  (* error "Single Digit Cases\n"; *)
  less_test "5" "5" false; (* equals *)
  less_test "5" "7" true; (* less and greater *)
  less_test "5" "5005" true;
  less_test "5" "4004" true;
  less_test "5" "6006" true;
  less_test "5" "4005" true;
  less_test "5" "5004" true;
  less_test "5" "5006" true;
  less_test "5" "6005" true;
  less_test "5" "4006" true;
  less_test "5" "6004" true;
  
  (* Two Digit Cases *)
  (* error "Two Digit Cases\n"; *)
  (* less_test "5005" "5005" false; (* equals equals *) *)
  less_test "5005" "4004" false; (* less less and greater greater *)
  less_test "5005" "4005" false; (* less equal and greater equal *)
  less_test "5005" "5006" true; (* equal greater and equal less *)
  less_test "5005" "4006" false; (* less greater and greater less *)
) 10;;

(* Go easy on greater - No double jeopardy, but they will impact each other *)
let greater_test (s1: string) (s2: string) (exp: bool) : unit =
  let b1 = fromString s1 in
  let b2 = fromString s2 in
  let test b1 b2 exp = 
    timed_test stats1_2 (fun _ -> greater b1 b2) exp 
      ("greater " ^ toString b1 ^ toString b2)
  in
  (* avoid redundant tests *)
  if not (equal b1 b2) then
    (test b2 b1 (not exp);
     test (negate b2) b1 false;
     test b2 (negate b1) true;
     test (negate b2) (negate b1) exp);
  if not (b1.coeffs = []) then
    (test (negate b1) b2 false;
     test b1 (negate b2) true;
     if not (equal b1 b2) then test (negate b1) (negate b2) (not exp)
     else test (negate b1) (negate b2) false);
  test b1 b2 exp;
;;

timed_part stats1_2 (fun _ ->
  greater_test "5" "5" false;
  greater_test "5" "5005" false;
  greater_test "5005" "5005" false;
  greater_test "5005" "5006" false;
) 5;;

(* Our Code *)
let equal (b1: bignum) (b2: bignum) : bool = 
  b1 = b2
;;

let less (b1: bignum) (b2: bignum) : bool =
  let rec list_compare (l1: int list) (l2: int list) : bool =
    match l1, l2 with
      | h1::t1, h2::t2 -> 
	if h1 = h2 then list_compare t1 t2
	else h1 < h2
      | _, _ -> false
  in
  let less_abs (l1: int list) (l2: int list) : bool = 
    let len1, len2 = (List.length l1, List.length l2) in
    if not (len1 = len2) then len1 < len2 else list_compare l1 l2
  in
  if not (b1.neg = b2.neg) then b1.neg
  else if b1.neg then less_abs b2.coeffs b1.coeffs
  else less_abs b1.coeffs b2.coeffs
;;

let greater (b1: bignum) (b2: bignum) : bool =
  less b2 b1
;; 


(* Part 1.3 *)
let stats1_3 = init_stats "Part 1.3" p13_max
let statistics = stats1_3::statistics

let fromInt_test (i: int) (sexp: string) : unit = 
  let exp = fromString sexp in
  timed_test_bignum stats1_3 (fun _ -> fromInt i) exp ("fromInt " ^ string_of_int i)
;;

timed_part stats1_3 (fun _ ->
  fromInt_test 0 "0";
  fromInt_test 123456789 "123456789";
  fromInt_test (-123456789) "~123456789";
  fromInt_test 10002 "10002";
  fromInt_test max_int (string_of_int max_int);
  fromInt_test min_int (string_of_int min_int)
) 10;;

(* Not timed individually - magic int??? - Things are getting janky - need a good unit testing framework... *)
let toInt_test (s1: string) (exp: int option) : unit =
  let b1 = fromString s1 in
  timed_test stats1_3 (fun _ -> toInt b1) exp ("toInt " ^ s1)
;;

timed_part stats1_3 (fun _ ->
  toInt_test "0" (Some 0);
  toInt_test "123456789" (Some 123456789);
  toInt_test "~123456789" (Some (-123456789));
  toInt_test "10002" (Some 10002);
  toInt_test (string_of_int max_int) (Some max_int);
  toInt_test (string_of_int min_int) (Some min_int);
  toInt_test "1234567834569120239458" None;
  toInt_test "~1233456456789120239458" None
) 10;;

(* Our code *)
let fromInt (n: int) : bignum =
  let rec make_coeffs (n: int) (r: int list) : int list =
    if n = 0 then r
    else make_coeffs (n / base) ((n mod base)::r)
  in
  {neg = (n < 0); coeffs = make_coeffs (abs n) []}
;;

let toInt (b: bignum) : int option =
  let rec sum_coeffs (coeffs: int list) (sum: int) (sign: int) : int =
    match coeffs with
      | [] -> sum
      | h::t -> sum_coeffs t (sign * h + base * sum) sign
  in
  if less (fromInt max_int) b || less b (fromInt min_int) then None
  else if b.neg then Some (sum_coeffs b.coeffs 0 (-1))
  else Some (sum_coeffs b.coeffs 0 1)
;;


(* Part 1.4 *)
let stats1_4 = init_stats "Part 1.4" p14_max;;
let statistics = stats1_4::statistics;;

let plus_pos (b1: bignum) (b2: bignum) : bignum =
  let pair_from_carry (carry: int) =
    if carry = 0 then (false, [])
    else if carry = 1 then (false, [1])
    else (true, [1])
  in
  let rec plus_with_carry (neg1, coeffs1) (neg2, coeffs2) (carry: int) 
      : (bool * int list) =
    match (coeffs1, coeffs2) with
      | ([], []) -> pair_from_carry carry
      | ([], _) -> if carry = 0 then (neg2, coeffs2) else
          plus_with_carry (neg2, coeffs2) (pair_from_carry carry) 0
      | (_, []) -> if carry = 0 then (neg1, coeffs1) else
          plus_with_carry (neg1, coeffs1) (pair_from_carry carry) 0
      | (h1::t1, h2::t2) -> 
          let (sign1, sign2) = 
            (if neg1 then -1 else 1), (if neg2 then -1 else 1) in
          let result = h1*sign1 + h2*sign2 + carry in
          if result < 0 then 
            let (negres, coeffsres) = plus_with_carry (neg1, t1) (neg2, t2) (-1)
            in (negres, (result+base)::coeffsres)
          else if result >= base then
            let (negres, coeffsres) = plus_with_carry (neg1, t1) (neg2, t2) 1
            in (negres, (result-base)::coeffsres)
          else 
            let (negres, coeffsres) = plus_with_carry (neg1, t1) (neg2, t2) 0
            in (negres, result::coeffsres)
  in
  let (negres, coeffsres) = 
        plus_with_carry (b1.neg, List.rev b1.coeffs)
          (b2.neg, List.rev b2.coeffs) 0
  in {neg = negres; coeffs = stripzeroes (List.rev coeffsres)}
;;

let plus_test (s1: string) (s2: string) (ssum: string) (sdiff: string) : unit =
  let b1 = fromString s1 in
  let b2 = fromString s2 in
  let sum = fromString ssum in
  let diff = fromString sdiff in
  let test b1 b2 exp = timed_test_bignum stats1_4 
    (fun _ -> plus b1 b2) exp ("plus " ^ toString b1 ^ " " ^ toString b2)
  in
  test b1 b2 sum;
  test b2 b1 sum;
  test (negate b1) b2 (negate diff);
  test (negate b2) b1 diff;
  test b1 (negate b2) diff;
  test b2 (negate b1) (negate diff);
  test (negate b1) (negate b2) (negate sum);
  test (negate b2) (negate b1) (negate sum);
  ()
;;

timed_part stats1_4 (fun _ ->
  (* Zero Cases *)
  (* error "Zero Cases\n";; S*)
  plus_test "0" "0" "0" "0";
  plus_test "0" "123" "123" "-123";
  plus_test "0" "1234" "1234" "-1234";
  plus_test "0" "1234567" "1234567" "-1234567";

  (* Equal Cases *)
  (* error "Equal Cases\n"; *)
  plus_test "5" "5" "10" "0";
  plus_test "1001" "1001" "2002" "0";
  plus_test "1001001" "1001001" "2002002" "0";

  (* Plus (Other) Cases *)
  (* error "Plus (Other) Cases\n"; *)
  plus_test "3" "123" "126" "-120";
  plus_test "3" "1232" "1235" "-1229";
  plus_test "3" "3122123" "3122126" "-3122120";
  plus_test "3003" "123123" "126126" "-120120";
  plus_test "3003" "3123123" "3126126" "-3120120";
  plus_test "1003002" "123123123" "124126125" "-122120121"
) 10;;

(* Our implementation *)
let plus (b1: bignum) (b2: bignum) : bignum =
  let switch (b1: bignum) (b2: bignum) : bool = 
    (b1.neg && (not b2.neg) && (less b2 (negate b1))) ||
      ((not b1.neg) && b2.neg && (less b1 (negate b2))) ||
      (b1.neg && b2.neg)
  in
  if switch b1 b2 then negate (plus_pos (negate b1) (negate b2))
  else plus_pos b1 b2
;;


(* Part 1.5 *)
let stats1_5 = init_stats "Part 1.5" p15_max;;
let statistics = stats1_5::statistics;;

(* Given two positive bignums, perform checks on all sign and ordering variations *)
let times_test (s1: string) (s2: string) (sprod: string) : unit =
  let b1 = fromString s1 in
  let b2 = fromString s2 in
  let prod = fromString sprod in
  let test b1 b2 exp = timed_test_bignum stats1_5 
    (fun _ -> times b1 b2) exp ("times " ^ toString b1 ^ " " ^ toString b2)
  in
  test b1 b2 prod;
  test b2 b1 prod;
  test (negate b1) b2 (negate prod);
  test (negate b2) b1 (negate prod);
  test b1 (negate b2) (negate prod);
  test b2 (negate b1) (negate prod);
  test (negate b1) (negate b2) prod;
  test (negate b2) (negate b1) prod;
  ()
;;

(* order matters because of single int x list implementation *)
timed_part stats1_5 (fun _ ->
  (* Zero Cases *)
  (* error "Zero Cases\n";; S*)
  times_test "0" "0" "0";
  times_test "0" "123" "0";
  times_test "0" "1234" "0";
  times_test "0" "1234567" "0";

  (* Identity Cases *)
  (* error "Identity Cases\n"; *)
  times_test "1" "123" "123";
  times_test "1" "1234" "1234";
  times_test "1" "1234567" "1234567";

  (* Square Cases - no carry *)
  (* error "Square Cases - no carry\n"; *)
  times_test "5" "5" "25";
  times_test "1001" "1001" "1002001";
  times_test "1001001" "1001001" "1002003002001";

  (* Square Cases - w/ carry *)
  (* error "Square Cases - w/ carry\n"; *)
  times_test "987" "987" "974169";
  times_test "987987" "987987" "976118312169";
  times_test "987987987" "987987987" "976120262456312169";

  (* Single Digit Cases - no carry *)
  (* error "Single Digit Cases - no carry\n"; *)
  times_test "3" "123" "369";
  times_test "3" "1232" "3696";
  times_test "3" "3122123" "9366369";

  (* Single Digit Cases - w/ carry *)
  (* error "Single Digit Cases - w/ carry\n"; *)
  times_test "9" "987" "8883";
  times_test "9" "987987" "8891883";
  times_test "9" "987987987" "8891891883";

  (* Two Digit Cases - no carry *)
  (* error "Two Digit Cases - no carry\n"; *)
  times_test "3003" "123123" "369738369";
  times_test "3003" "3123123" "9378738369";

  (* Two Digit Cases - w/ carry *)
  (* error "Two Digit Cases - w/ carry\n"; *)
  times_test "976999" "79987" "78147219013";
  times_test "976999" "79987987" "78148183311013";

  (* Three Digit Cases - no carry *)
  (* error "Three Digit Cases - no carry\n"; *)
  times_test "1003002" "123123123" "123492738615246";

  (* Three Digit Cases - w/ carry *)
  (* error "Three Digit Cases - w/ carry\n"; *)
  times_test "976976999" "79987987" "78146423495311013";

  (* Bignums for cases > max_int *)
  (* Done last because efficienct might become a factor *)
  (* Should be able to do bignum mult however *)
  (* Bignums (10 digits) - no carry *)
  times_test "0" "123123123123123123123123123123123123" "0"; (* Zero *)
  times_test "1" "123123123123123123123123123123123123" "123123123123123123123123123123123123"; (* Identity *)
  times_test "3" "123123123123123123123123123123123123" "369369369369369369369369369369369369";
  times_test "2003" "123123123123123123123123123123123123" "246615615615615615615615615615615615369";
  times_test "3002003" "123123123123123123123123123123123123" "369615984984984984984984984984984984615369";
  times_test "2003002003" "123123123123123123123123123123123123" "246615862231231231231231231231231230984615369";
  times_test "1002003002003" "123123123123123123123123123123123123" "123369738985354354354354354354354354230984615369";
  times_test "2001002003002003" "123123123123123123123123123123123123" "246369615985231600600600600600600600354230984615369";
  times_test "3002001002003002003" "123123123123123123123123123123123123" "369615738985354600969969969969969969600354230984615369";
  times_test "2003002001002003002003" "123123123123123123123123123123123123" "246615861985231600847216216216216215969600354230984615369";
  times_test "1002003002001002003002003" "123123123123123123123123123123123123" "123369738985108354723970339339339339215969600354230984615369";
  times_test "3001002003002001002003002003" "123123123123123123123123123123123123" "369492739108354477724093339708708708339215969600354230984615369";

  (* Bignums (10 digits) - carry *)
  times_test "999" "987987987987987987987987987987" "986999999999999999999999999999013";
  times_test "888999" "987987987987987987987987987987" "878320333333333333333333333332455013";
  times_test "777888999" "987987987987987987987987987987" "768544986999999999999999999999231455013";
  times_test "888777888999" "987987987987987987987987987987" "878101878320333333333333333332455231455013";
  times_test "999888777888999" "987987987987987987987987987987" "987878101878320333333333333332345455231455013";
  times_test "888999888777888999" "987987987987987987987987987987" "878321211435211653666666666665788345455231455013";
  times_test "777888999888777888999" "987987987987987987987987987987" "768544987878101878320333333332564788345455231455013";
  times_test "888777888999888777888999" "987987987987987987987987987987" "878101878321211435211653666665788564788345455231455013";
  times_test "999888777888999888777888999" "987987987987987987987987987987" "987878101878321211435211653665678788564788345455231455013";
  times_test "888999888777888999888777888999" "987987987987987987987987987987" "878321211435211654544768544986121678788564788345455231455013"
) 20;;


(* sum = result of positive addition, diff result of +first and -second (order here matters, not sign of diff) *)
(* Given two positive bignums, perform checks on all sign and ordering variations *)

(* Tests that may seem excessive are a product of ridiculous implementations that have been seen *)
(* Testing based on structure of bignums not implementaiton - needs to be implementaiton independent *)


(* Signs - taken care of above *)
(* Size differences (0, 1, & 2)*)
(* For 2 x 2 case - Ordering w/in coeffs - less greater, greater less, less less, greater greater, less equals, equals less, equals equals, equals greater, greater equals *)
(* a lot get taken care of by the less_test funciton - anything thats a reverse *)
(* less_test takes care of signs and order of arguments - nothing else *)

(* dont take unrelated coeffs into consideration *)
(* people do 2 elements extracted at a time... *)

(* notate each one with possible bugs *)
(* bugs - don't length check *)
(* don't short circuit *)
(* deal with actual bignums - could do toInt comparison which shouldn't be valid *)

(* Mixing imperative and functional programming is ugly!!! *)
let total_stats (stats: _stats list) = 
  let t = {part = "Total"; timeouts = 0; variants = 0; failures = 0; tests = 0; max_pts = 0} in
  let rec sum_stats (stats: _stats list) : _stats =
    match stats with
      | [] -> t
      | hd::tl -> 
	t.timeouts <- t.timeouts + hd.timeouts;
	t.variants <- t.variants + hd.variants;
	t.failures <- t.failures + hd.failures;
	t.tests <- t.tests + hd.tests;
	t.max_pts <- t.max_pts + hd.max_pts;
	sum_stats tl
  in
  sum_stats stats
;;

let statistics = List.rev ((total_stats statistics)::statistics)

let rec print_stats (stats: _stats list) : unit =
  match stats with
    | [] -> ()
    | hd::tl ->
      error "\n**************** %s ****************\n" hd.part;
      if hd.timeouts > 0 then error "Timeout: %d/%d\n" hd.timeouts hd.tests;
      if hd.variants > 0 then error "Invariant Broken: %d/%d\n" hd.variants hd.tests;
      if hd.failures > 0 then error "Incorrect Answer: %d/%d\n" hd.failures hd.tests;
      let passed = hd.tests - hd.timeouts - hd.variants - hd.failures in 
      let percent: float = (float) passed /. (float) hd.tests in
      let grade: int = int_of_float (floor (percent *. (float) hd.max_pts)) in
      error "Passed: %d/%d\n" passed hd.tests;
      error "Points: %d/%d\n" grade hd.max_pts;
      Printf.printf "%d, %d, %d, %d, %d" hd.timeouts hd.variants hd.failures (hd.tests - hd.timeouts - hd.variants - hd.failures) hd.tests;
      (match tl with
	| [] -> Printf.printf "\n"
	| _ -> Printf.printf ", ");
      print_stats tl
;;

print_stats statistics;;

