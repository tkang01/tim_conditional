type bignum = {neg : bool; coeffs : int list}
val base : int
val negate : bignum -> bignum
val equal : bignum -> bignum -> bool
val less : bignum -> bignum -> bool
val greater : bignum -> bignum -> bool
val fromInt : int -> bignum
val toInt : bignum -> int option
val plus : bignum -> bignum -> bignum
val times : bignum -> bignum -> bignum
