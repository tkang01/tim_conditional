#!/usr/bin/python
# CS 51 Tools
# Automated grading for OCaml submissions.
#
# N.B.: Unless you're feeling dangerous, best run within a VM, to avoid
# injecting any evil student ML code.

import optparse
import os
import re
import shutil
import string
import subprocess
import sys
import time

MAX_LINE_LEN = 80
WRONG_ANSWER_ERROR_CODE = 17
# A random exit code that we hope is never used by OCaml to denote a crash.
MAX_TIME = 15
MAX_RUNTIME = 60
TIMEOUT = -1

class SubmissionException(Exception):
    """For some reason, we don't like the submission we're grading."""
    def __init__(self, desc):
        self.desc = desc
    def __str__(self):
        return self.desc

def grade(asst_name, fname_in, fname_out, f_stats):
    if not os.path.exists(fname_in):
        raise SubmissionException('File to be graded does not exist: %s' % fname_in)

    if os.path.exists('ps3.ml'):
        os.remove('ps3.ml')

    shutil.copy(fname_in, 'ps3.ml')

    time_taken = 0
    compile_ret = None
    compile_proc = subprocess.Popen(['corebuild', '-quiet', '-cflag', '-noassert', 'ps3_tests.byte'],
                                    stdout=sys.stdout, stderr=sys.stderr)

    while (compile_ret is None):
        if time_taken > MAX_TIME:
            compile_ret = TIMEOUT
            compile_proc.kill()
            break
        time.sleep(0.01)
        time_taken += 0.01
        compile_proc.poll()
        compile_ret = compile_proc.returncode
    if (compile_ret != 0):
        raise SubmissionException('Compile Error (%s)' % compile_ret)
    print >> sys.stderr, "Compiled...",

    f_out = open(fname_out, 'w')
    time_taken = 0
    run_ret = None
    run_proc = subprocess.Popen(['./ps3_tests.byte'], stdout=f_stats, stderr=f_out)
    print >> sys.stderr, "Running...",
    while (run_ret is None):
        if time_taken > MAX_RUNTIME:
            run_ret = TIMEOUT
            run_proc.kill()
            break
        time.sleep(0.01)
        time_taken += 0.01
        run_proc.poll()
        run_ret = run_proc.returncode
    f_out.close()
    f_stats.flush()
    if (run_ret != 0):
        raise SubmissionException('Runtime Error (%s)' % run_ret)

def grade_all(asst_name, submissions_dir, tests_full_path, fname_fail_log, fname_stat_log,
              uids, skipto=None, debug=False):
    bad_submissions = []
    fail_log = None
    if skipto is None: # Don't restart if skipping to uid
        if os.path.exists(fname_fail_log):
            os.remove(fname_fail_log)
        if os.path.exists(fname_stat_log):
            os.remove(fname_stat_log)
    stat_log = open(fname_stat_log, 'w')
    for username in os.listdir(submissions_dir):
        if username.startswith('.'):
            continue  # Ignore hidden directories, such as .svn.
        if uids != [] and username not in uids:
            continue
        if skipto is not None:
            print >> sys.stderr, "%s" % username
            m = re.search('[0-9]+$', username)
            if m is not None and skipto == int(m.group(0)):
                skipto = None
            else:
                continue
        base_dir = os.path.join(submissions_dir, username)
        if os.path.isdir(base_dir):
            grading_output = os.path.join(base_dir, '%s_graded.ml' % asst_name)
            try:
                print >> sys.stderr, 'Starting %s' % username
                grade(asst_name,
                      os.path.join(base_dir, '%s.ml' % asst_name),
                      grading_output,
                      stat_log)
                print >> sys.stderr, 'Graded'

            except SubmissionException, e:
                if os.path.exists(grading_output):
                    os.remove(grading_output)
                print >> sys.stderr, e.desc
                if fail_log is None:
                    fail_log = open(fname_fail_log, 'w')
                fail_log.write('%s: %s\n' % (username, e.desc))
                fail_log.flush()
                bad_submissions.append('%s: %s\n' % (username, e.desc))
    if fail_log is not None:
        fail_log.close()
    stat_log.close()
    return bad_submissions

def usage():
    print 'Usage: grade.py [-d|--debug]'
    sys.exit(255)

if __name__ == '__main__':
    my_path = os.path.dirname(os.path.realpath(__file__))
    parser = optparse.OptionParser()
    parser.add_option('-d', '--debug', action='store_true',
                      dest='debug', default=False,
                      help='Leave temp files around for debugging')
    parser.add_option('-u', '--uid', action='append',
                      dest='uids', default=[],
                      help='Grade submissions for specific uid(s)')
    parser.add_option('-s', '--skipto', action='store',
                      type='int', dest='skipto', default=None,
                      help='Grade submissions starting from specific uid')
    opts, args = parser.parse_args(sys.argv[1:])
    if len(args) != 0:
        usage()
    ps_number = "3"
    ps_name = 'ps' + ps_number
    # "Expects spring2013 with student work to be in "../../".
    # So, for instance
    # Desktop/cs51-materials/grading/grade.py
    # Desktop/spring2013/
    ps_grade_path = os.path.join(my_path, '..', '..', '..', 'grading', ps_number)
    ps_soln_path = os.path.join(my_path)
    ps_part_names_path = os.path.join(my_path, '..', 'hw', ps_number,
                                      'part_names')
    if os.path.exists(ps_part_names_path):
        parts = load(ps_part_names_path)
    else:
        # If the part-names file does not exist, assume there is only one part,
        # with the same name as the pset.
        parts = [ps_name]
    bad_submissions = {}
    for part in parts:
        print >> sys.stderr, '*** GRADING PART: %s' % part
        bad = grade_all(part,
                        ps_grade_path,
                        ps_soln_path,
                        os.path.join(ps_grade_path, '%s_fail_log.txt' % part),
                        os.path.join(ps_grade_path, '%s_stat_log.csv' % part),
                        uids=opts.uids, skipto=opts.skipto, debug=opts.debug)
        bad_submissions[part] = bad
    print >> sys.stderr, '*** BAD SUBMISSIONS ***'
    for part in parts:
        print >> sys.stderr, '* Part: %s *' % part
        for badsub in bad_submissions[part]:
            print >> sys.stderr, ' %s' % badsub

