#!/bin/sh

cd `dirname "$0"`
mkdir -p lectures

make_pdf () {
  if [ "lectures/$2.pdf" -nt "$2" ]; then
    true
  else
    echo "Building $2.pdf" >&2
    enscript -E$1 --style=modest -MLetter --color -o- "$2" |
      ps2pdf - "lectures/$2.pdf"
  fi
}

copy_src () {
  if [ "lectures/$1" -nt "$1" ]; then
    true
  else
    echo "Copying $1" >&2
    cp "$1" "lectures/$1"
  fi
}

visible () {
  if fgrep '[HIDE]' "$1" >/dev/null 2>&1; then
    echo "Skipping $1" >&2
    false
  else
    echo "Linking $1" >&2
    true
  fi
}

empty_td () {
    cat <<....EOF
            <td>&nbsp;</td>
....EOF
}

get_dates () {
  ls ??-??-* | sed 's/^\(..-..\).*/\1/' | sort -r | uniq
}

get_lecture_dates () {
  get_dates | grep -v supplement
}

# lecture count:
count=`get_lecture_dates | wc -l`

# open index.html for output:
rm lectures/index.html
exec 3>&1
exec >lectures/index.html

cat <<'EOF'
<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html
     PUBLIC "-//W3C//DTD XHTML 1.1//EN"
     "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>CS51 Spring 2014 – Lecture Notes</title>
    <style type="text/css" media="screen">
      body {
        font-family: sans-serif;
      }

      #main {
        width: 600px;
        margin: 1em auto;
      }

      #main table {
        margin: 1em auto;
      }

      td, th {
        text-align: center;
        padding: 0.25ex 0.5ex;
      }

      h1 {
        text-align: center;
      }
      /* @import url("../../ccs.css"); */
    </style>
  </head>
  <body>
    <div id="main">
      <h1>CS51 Spring 2014 — Lecture Notes</h1>
      <p>
        I’m going to post lecture notes here as they become available.
        The <em>preliminary notes</em> are the notes I write before lecture;
        these
        are subject to change, but might be helpful for following along.
        The <em>actual notes</em> are a transcript of the actual code written in
        lecture.
      </p>
      <table>
        <thead>
          <tr>
            <th></th>
            <th>Preliminary Notes</th>
            <th>Actual Notes</th>
            <th>Slides</th>
            <th>Videos</th>
          </tr>
        </thead>
        <tbody>
EOF

get_dates | while read date; do
  planned="$date-planned.ml"
  supplement="$date-supplement.ml"
  actualml="$date-actual.ml"
  actualtxt="$date-actual.txt"
  slides="$date-slides.pdf"
  video="$date-video.txt"

  month=`echo $date | sed 's/-..//'`
  day=`echo $date | sed 's/..-//'`

  if [ -f "$supplement" ]; then
    planned="$supplement"
    title="Extra video: $month/$day"
  else
    supplement=
    title="Lecture $count: $month/$day"
  fi

  cat <<..EOF
          <tr>
            <th>$title</th>
..EOF

  if [ -f $planned ]; then
    cp $planned lectures
    make_pdf ocaml $planned
    if visible $planned; then
      cat <<......EOF
            <td>
              <a href="$planned">ML file</a>
              or
              <a href="$planned.pdf">PDF</a>
            </td>
......EOF
    else empty_td
    fi
  else empty_td
  fi

  if [ -f "$actualml" ]; then
    copy_src "$actualml"
    make_pdf ocaml "$actualml"
    if visible "$actualml"; then
      cat <<......EOF
            <td>
              <a href="$actualml">ML file</a>
              or
              <a href="$actualml.pdf">PDF</a>
            </td>
......EOF
    else empty_td
    fi
  elif [ -f "$actualtxt" ]; then
    copy_src "$actualtxt"
    make_pdf c "$actualtxt"
    if visible "$actualtxt"; then
      cat <<......EOF
            <td>
              <a href="$actualtxt">text file</a>
              or
              <a href="$actualtxt.pdf">PDF</a>
            </td>
......EOF
    fi
  else empty_td
  fi

  if [ -f "$slides" ]; then
    copy_src "$slides"
    cat <<......EOF
            <td>
              <a href="$slides">PDF</a>
            </td>
......EOF
  else empty_td
  fi

  if [ -f "$video" ]; then
    code=`cat "$video"`
    cat <<....EOF
            <td>
              <a href="http://www.youtube.com/watch?v=$code&wide">
                YouTube
              </a>
            </td>
....EOF
  else empty_td
  fi

  cat <<"..EOF"
          </tr>
..EOF

  if [ -z "$supplement" ]; then
    count=`expr $count - 1`
  fi
done

chmod -R a+rX lectures

cat <<'EOF'
        </tbody>
      </table>
      <p>
        <a href="http://isites.harvard.edu/icb/icb.do?keyword=k99563">
          CS51 iSite
        </a>
      </p>
    </div>
  </body>
</html>
EOF

exec 1>&3

rsync -avz lectures tov@bowser.eecs.harvard.edu:public_html/courses/cs51

