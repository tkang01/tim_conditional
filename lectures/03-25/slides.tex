\documentclass[aspectratio=169]{beamer}

\RequirePackage{ucs}
\usepackage[quiet]{fontspec}
\usepackage{xunicode}
\usepackage{xltxtra}
\usepackage{graphicx}
\usepackage{forloop}
\usepackage{stmaryrd}
\usepackage{tikz}
\usepackage{mathtools}
\usepackage{fancyvrb}
\usepackage{amsmath,amssymb,amsthm}

\usetikzlibrary{chains,decorations.pathmorphing,positioning,fit}
\usetikzlibrary{decorations.shapes,calc,backgrounds}
\usetikzlibrary{decorations.text,matrix}
\usetikzlibrary{arrows,shapes.geometric,shapes.symbols,scopes}

\newcommand\bigO[1]{\ensuremath{\mathcal{O}(#1)}}
\newcommand\fun[1]{\ensuremath{\lambda #1.\,}}
\renewcommand\T[1]{\ensuremath{T_{\mathit{#1}}}}

%% %%% SETTINGS

\frenchspacing
\unitlength=0.01\textwidth
\thicklines
\urlstyle{sf}
\graphicspath{{images/}}

%%% VERBATIM SETUP

\DefineVerbatimEnvironment
  {code}{Verbatim}
  {commandchars=\\\‹\›,
   commentchar=\%,
   codes={\catcode`$=3\relax},
   baselinestretch=.8,
   formatcom=\relax}
\renewcommand\FancyVerbFormatLine[1]{#1\strut}
\fvset{commandchars=\\\‹\›,
       codes={\catcode`$=3\relax},
       formatcom=\relax}
\newcommand\exponent[1]{^{#1}}
\DefineShortVerb{\^}

\newcommand\ALT[3]{\alt<#1>{#2}{#3}}

%%% COLOR AND BEAMER SETUP

\setbeamercolor{frametitle}{fg=black}
\setbeamercolor{normal text}{fg=black}

\defaultfontfeatures{
    Mapping=tex-text,
    Scale=MatchLowercase,
}
\setmainfont{TeX Gyre Schola}
\setmonofont{Monaco}

\usefonttheme{serif}

\setbeamertemplate{frametitle}
  {\begin{centering}\medskip
   \insertframetitle\par
   \end{centering}}

\setbeamertemplate{itemize item}{$\bullet$}

\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[text line]{%
    \hfill\strut{%
        \small\color{black!75}%
        \texttt{\insertframenumber:\insertframeslidenumber}%
    }%
    \hfill%
}

\makeatletter
\newcount\frameslidetemp
\newcommand\insertframeslidenumber{%
  \frameslidetemp\c@page\relax
  \advance\frameslidetemp-\beamer@startpageofframe\relax
  \advance\frameslidetemp1\relax
  \the\frameslidetemp
}
\makeatother

%%% GENERALLY USEFUL MACROS

\newcommand<>\always[1]{#1}

\def\defgbox#1#2{%
  \expandafter\global\expandafter\newbox\csname#2box\endcsname%
  \expandafter\global\expandafter\def\csname#2\endcsname{%
    \expandafter\usebox\csname#2box\endcsname%
  }%
  \expandafter\global\expandafter\setbox\csname#2box\endcsname=#1%
}
\def\defhbox{\defgbox\hbox}
\def\defvbox{\defgbox\vbox}

\def\setgbox#1#2{%
  \expandafter\setbox\csname#2box\endcsname=#1%
}
\def\sethbox{\setgbox\hbox}
\def\setvbox{\setgbox\vbox}

%%%
%%% TIKZ
%%%

\newcommand\K[1]{\textcolor{orange!70!black}{#1}}
\newcommand\PR[1]{\textcolor{blue!70!black}{#1}}
\newcommand\REF{\textcolor{green!50!black}{ref}}

\newcommand\place[3][]{%
  \tikz[orp] \node[#1] at (#2) {#3};%
}

\newcommand\anchor[1]{%
  \tikz[orp] \coordinate (#1);%
}

\newcommand\nil[1]{
  \draw[nil] (#1.north east) -- (#1.south west)
}

\newcommand\cons[4][]{
  \node[cons,#1] (#2) {#3\kern3pt\anchor{#2 center}\kern3pt#4};
  \draw[thick] (#2 center |- #2.north) -- (#2 center |- #2.south)
}

\newcommand\genpointer[3][draw=blue!50!black,opacity=.5]{%
  \begin{tikzpicture}[rem,solid,#1]
    \node(pointer start) [circle, draw, inner sep=1.37pt] {};
    \draw[->, overlay] (pointer start) edge[bend right=#2] (#3);
  \end{tikzpicture}%
}

\newcommand\hpointer[2][30]{%
  \genpointer[draw=red!70!black]{#1}{#2}%
}

\newcommand\ppointer[2][30]{%
  \genpointer{#1}{#2}%
}

\newcommand\pointer[2][30]{%
  \ifhighlit\let\pointertemp\hpointer\else\let\pointertemp\ppointer\fi
  \pointertemp[#1]{#2}%
}


\newcount\reducetemp
\newcommand\reduce[3]{%
  \reducetemp#1\relax
  \advance\reducetemp -1\relax
  \alt<#1->{#3}{\alt<\the\reducetemp>{\highlight{#2}}{#2}}%
}

\tikzset{
  orp/.style={
    overlay,
    remember picture,
  },
  rem/.style={
    remember picture,
  },
  every node/.style={
    very thick,
    inner sep=3pt,
  },
  every path/.style={very thick},
  ref/.style={
    draw=red,
    densely dotted,
    font=\ttfamily,
  },
  cons/.style={
    draw=black,
    font=\ttfamily,
  },
  nil/.style={
    black!80!white,
    shorten >=2pt,
    shorten <=2pt,
  },
  highlight/.style={
    color=yellow!50!white!90!black,
    thick,
    draw,
    draw opacity=0.5,
    outer color=yellow!50,
    inner color=yellow!30,
    rounded corners=2pt,
  },
}

\newif\ifhighlit
\highlitfalse

\newcommand<>\highlight[2][\relax]{%
  \ifx#1\relax
    \begin{tikzpicture}
  \else
    \begin{tikzpicture}[remember picture]
  \fi
    \node[
      inner sep=0pt,
      outer sep=0pt,
      text depth=0pt,
      align=left,
    ] (highlight temp) {%
        \only#3{\highlittrue}%
        #2%
        \only#3{\highlitfalse}%
    };
    \begin{scope}[on background layer]
      \coordinate (highlight temp 1) at
        ($(highlight temp.north east) + (2pt,2pt)$);
      \coordinate (highlight temp 2) at
        ($(highlight temp.base west) + (-2pt,-3pt)$);
      \node#3 [
        overlay,
        highlight,
        inner sep=0pt,
        fit=(highlight temp 1) (highlight temp 2),
      ] {};
    \end{scope}
    \ifx#1\relax
      \relax
    \else
      \node (#1) [
        overlay,
        inner sep=0pt,
        fit=(highlight temp 1) (highlight temp 2),
      ] {};
    \fi
  \end{tikzpicture}%
}

\newcommand\HI[2]{\highlight<#1>{#2}}

\newcommand\huncover[3][\relax]{%
  \uncover<#2->{\highlight<#2>[#1]{#3}}%
}
\newcommand\honly[3][\relax]{%
  \only<#2->{\highlight<#2>[#1]{#3}}%
}

\def\S#1#2{#2<#1>}

\begin{document}

%%%
%%% TITLE PAGE
%%%

\begin{frame}\relax
  \thispagestyle{empty}
  \begin{center}
  {\Huge Infinite Data Structures!}

  \bigskip
  \Large
  CS 51 and CSCI E-51

  \medskip
  March 25, 2014
  \end{center}

  %% Need to initialize Tikz opacity inside a frame, not outside:
  \begin{tikzpicture}[opacity=0,overlay]\end{tikzpicture}
\end{frame}

\begin{frame}[fragile]{A first attempt}
  Consider this definition for infinite sequences:
  \begin{code}
  \K‹type› 'a stream = Cons of 'a * 'a stream
  \end{code}

  \pause
  \medskip
  We can extract the head and the tail of a stream:
  \begin{code}
  \K‹let› head (Cons(x, _) : 'a stream) : 'a        = x

  \K‹let› tail (Cons(_, r) : 'a stream) : 'a stream = r
  \end{code}

  \pause
  \medskip
  But how can we build one?
\end{frame}

\begin{frame}[fragile]{But how can we build one?}
  Recursion!
  \begin{code}
  \K‹let› \K‹rec› ones = Cons(1, ones)
  \end{code}
  \par
  \pause
  \begin{code}
  \PR‹utop[5]› head ones
  - : int = 1\pause
  \PR‹utop[6]› head (tail ones)
  - : int = 1\pause
  \PR‹utop[7]› head (tail (tail ones))
  - : int = 1
  \end{code}
\end{frame}

\begin{frame}[fragile]{What it means}
  Reduction-substitution model:
  \begin{code}
  \K‹let› \K‹rec› ones = Cons(1, ones)
  \end{code}
  \pause
  \begin{code}
  \K‹let› \K‹rec› ones = Cons(1, \K‹let› \K‹rec› ones = Cons(1, ones) in ones)
  \end{code}
  \pause
  \begin{code}
  \K‹let› \K‹rec› ones = Cons(1, Cons(1, \K‹let› \K‹rec› ones = Cons(1, ones) in ones))
  \end{code}
  \pause
  \begin{code}
  \K‹let› \K‹rec› ones = Cons(1, Cons(1, Cons(1, \K‹let› \K‹rec› ones = Cons(1, ones) in ones)))
  \end{code}
  \vspace{-.5\baselineskip}
  $\vdots$
  \par\medskip\pause
  Also, this isn't true:
  \begin{code}
  \PR‹utop[8]>› phys_equal ones (tail ones)
  - : bool = true
  \end{code}
\end{frame}

\begin{frame}[fragile]{What's really happening}
  \begin{code}
  \K‹let› \K‹rec› ones = Cons(1, ones)
  \end{code}
  \pause
  \begin{center}
  \begin{tikzpicture}[rem]
    \coordinate (ones);
    \cons[above right=1pt of ones, anchor=south west]
      {ones again} {\strut\alt<3->{1}{--}} {\alt<4->{\strut\pointer[-90]{ones}}{--}};
  \end{tikzpicture}
  \end{center}
  \par
  \pause[5]
  \begin{code}
  \K‹let› \K‹rec› onetwothrees = Cons(1, Cons(2, Cons(3, onetwothrees)))
  \end{code}
  \begin{center}
  \begin{tikzpicture}[rem]
    \coordinate[at={(0,0)}] (onetwothrees);
    \cons[at={(4,0)},anchor=south west] {c3} {3} {\pointer[-45]{onetwothrees}};
    \cons[at={(2,0)},anchor=south west] {c2} {2} {\pointer{c3}};
    \cons[at={(0,0)},anchor=south west] {c1} {1} {\pointer{c2}};
  \end{tikzpicture}
  \end{center}
  \par
  \pause
  \medskip
  How about a stream that doesn’t repeat, like the
  natural numbers?
\end{frame}

\begin{frame}[fragile]{A stream of natural numbers?}
  \begin{code}
  \K‹let› \K‹rec› count_from n = Cons(n, count_from (n + 1))
  \pause
  \K‹let› nats = count_from 0
  \end{code}
  \pause
  Unfortunately, this just recurs until it blows the stack
\end{frame}

\begin{frame}{\relax}
  \begin{center}
  --- To Emacs ---
  \end{center}
\end{frame}

\begin{frame}[fragile]{Take three: memoize the thunks}
  \begin{code}
  \K‹type› 'a promise = Unevaluated of (unit -> 'a) | Evaluated of 'a
  \pause
  \K‹type› 'a stream = 'a str promise ref
   \K‹and› 'a str = { hd : 'a; tl : 'a stream }
  \pause
  \K‹let› \K‹rec› ones = \REF (Unevaluated (\K‹fun› () -> { hd = 1; tl = ones }))
  \pause
  \K‹let› \K‹rec› head (s : 'a stream) : 'a =
    \K‹match› !s \K‹with›
    | Evaluated str -> str.hd
    | Unevaluated f -> \K‹let› str = f () \K‹in›
                       s := Evaluated str;
                       str.hd
  \pause
  \K‹let› \K‹rec› tail (s : 'a stream) : 'a stream =
    \K‹match› !s \K‹with›
    | Evaluated str -> str.tl
    | Unevaluated f -> \K‹let› str = f () \K‹in›
                       s := Evaluated str;
                       str.tl
  \end{code}
\end{frame}

\begin{frame}[fragile]{The laziness pattern}
  \begin{code}
   \K‹type› 'a promise   = Unevaluated of (unit -> 'a) | Evaluated of 'a
   \K‹type› 'a my_lazy_t = 'a promise ref
   \pause
   \K‹let› force (pr : 'a my_lazy_t) : 'a =
     \K‹match› !pr \K‹with›
     | Evaluated v -> v
     | Unevaluated f -> \K‹let› v = f () \K‹in›
                        pr := Evaluated v;
                        v
   \pause
   \K‹type› 'a stream = 'a str my_lazy_t
    \K‹and› 'a str = { hd : 'a; tl : 'a stream }
   \pause
   \K‹let› head (s : 'a stream) : 'a = (force s).hd
   \K‹let› tail (s : 'a stream) : 'a stream = (force s).tl
   \pause
   \K‹let rec› map (s : 'a stream) ~(f : 'a -> 'b) : 'b stream =
     \REF (Unevaluted (\K‹fun› () -> { hd = f (head s); tl = map (tail s) ~f }))
  \end{code}
\end{frame}

\begin{frame}[fragile]{OCaml's built-in laziness}
  \begin{code}
   \K‹type› 'a stream = 'a str lazy_t
    \K‹and› 'a str = { hd : 'a; tl : 'a stream }
   \pause
   \K‹let› head (s : 'a stream) : 'a = (Lazy.force s).hd
   \K‹let› tail (s : 'a stream) : 'a stream = (Lazy.force s).tl
   \pause
   \K‹let rec› map (s : 'a stream) ~(f : 'a -> 'b) : 'b stream =
     \K‹lazy› { hd = f (head s);
            tl = map (tail s) ~f }
  \end{code}
\end{frame}

\begin{frame}{\relax}
  \begin{center}
  --- To Emacs ---
  \end{center}
\end{frame}


\begin{frame}{The take-away}
  \begin{itemize}
    \item You can build infinite data structures \pause(but not really)
    \pause
    \item Lazy evaluation lets us delay computation until it's needed
    \pause
    \item Helps us separate concerns---decouples consumers from
    producers
  \end{itemize}
\end{frame}

\begin{frame}{(Thought) exercises}
  \begin{itemize}
  \item Other Taylor series, \emph{e.g.,}
  \[
    \cos x
      = \sum_{i = 0}\exponent{\infty} (-1)\exponent{i} \frac{x\exponent{2i}}{(2i)!}
      = \frac{x\exponent0}{0!} - \frac{x\exponent2}{2!} +
        \frac{x\exponent4}{4!} - \frac{x\exponent6}{6!} + \cdots
  \]
  \pause
  \item Boolean circuits as streams of booleans
    \begin{itemize}
      \item The \emph{not} circuit negates each value in its input stream
      \item The \emph{and} and \emph{or} circuits
      \item Define all of the above in terms of \emph{nand}
      \item For EEs: a J-K flip-flop
    \end{itemize}
  \pause
  \item How would you define infinite trees?
  \end{itemize}
\end{frame}

\end{document}
