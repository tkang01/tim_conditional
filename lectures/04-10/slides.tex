\documentclass[aspectratio=169]{beamer}

\RequirePackage{ucs}
\usepackage[quiet]{fontspec}
\usepackage{xunicode}
\usepackage{xltxtra}
\usepackage{graphicx}
\usepackage{forloop}
\usepackage{stmaryrd}
\usepackage{tikz}
\usepackage{mathtools}
\usepackage{fancyvrb}
\usepackage{listings}
\usepackage{amsmath,amssymb,amsthm}
\usepackage{cancel}

\usetikzlibrary{chains,decorations.pathmorphing,positioning,fit}
\usetikzlibrary{decorations.shapes,calc,backgrounds}
\usetikzlibrary{decorations.text,matrix}
\usetikzlibrary{arrows,shapes.geometric,shapes.symbols,scopes}

\newcommand\bigO[1]{\ensuremath{\mathcal{O}(#1)}}
\newcommand\fun[1]{\ensuremath{\lambda #1.\,}}
\renewcommand\T[1]{\ensuremath{T_{\mathit{#1}}}}

%% %%% SETTINGS

\frenchspacing
\unitlength=0.01\textwidth
\thicklines
\urlstyle{sf}
\graphicspath{{images/}}

%%% VERBATIM SETUP

\DefineVerbatimEnvironment
  {code}{Verbatim}
  {commandchars=\\\‹\›,
   commentchar=\%,
   codes={\catcode`$=3\relax},
   baselinestretch=.8,
   formatcom=\relax}
\renewcommand\FancyVerbFormatLine[1]{#1\strut}
\fvset{commandchars=\\\‹\›,
       codes={\catcode`$=3\relax},
       formatcom=\relax}
\newcommand\exponent[1]{^{#1}}
\DefineShortVerb{\^}

\newcommand\ALT[3]{\alt<#1>{#2}{#3}}
\newcommand\UNCOVER[2]{\uncover<#1>{#2}}

%%% COLOR AND BEAMER SETUP

\setbeamercolor{frametitle}{fg=black}
\setbeamercolor{normal text}{fg=black}

\defaultfontfeatures{
    Mapping=tex-text,
    Scale=MatchLowercase,
}
\setmainfont{TeX Gyre Schola}
\setmonofont{Monaco}

\usefonttheme{serif}

\setbeamertemplate{frametitle}
  {\begin{centering}\medskip
   \insertframetitle\par
   \end{centering}}

\setbeamertemplate{itemize item}{$\bullet$}

\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[text line]{%
    \hfill\strut{%
        \small\color{black!75}%
        \texttt{\insertframenumber:\insertframeslidenumber}%
    }%
    \hfill%
}

\makeatletter
\newcount\frameslidetemp
\newcommand\insertframeslidenumber{%
  \frameslidetemp\c@page\relax
  \advance\frameslidetemp-\beamer@startpageofframe\relax
  \advance\frameslidetemp1\relax
  \the\frameslidetemp
}
\makeatother

\newcommand\CenterFrame[1]{
  \begin{frame}{\relax}
    \thispagestyle{empty}
    \begin{center}
      \emph{#1}
    \end{center}
  \end{frame}
}


\newcommand\EmacsFrame{
  \CenterFrame{--- To Emacs---}
}

%%% GENERALLY USEFUL MACROS

\newcommand<>\always[1]{#1}

\def\defgbox#1#2{%
  \expandafter\global\expandafter\newbox\csname#2box\endcsname%
  \expandafter\global\expandafter\def\csname#2\endcsname{%
    \expandafter\usebox\csname#2box\endcsname%
  }%
  \expandafter\global\expandafter\setbox\csname#2box\endcsname=#1%
}
\def\defhbox{\defgbox\hbox}
\def\defvbox{\defgbox\vbox}

\def\setgbox#1#2{%
  \expandafter\setbox\csname#2box\endcsname=#1%
}
\def\sethbox{\setgbox\hbox}
\def\setvbox{\setgbox\vbox}

%%%
%%% TIKZ
%%%

\newcommand\K[1]{\textcolor{orange!70!black}{#1}}
\newcommand\TY[1]{\textcolor{blue!70!black}{#1}}
\newcommand\REF{\textcolor{green!50!black}{ref}}

\newcommand\place[3][]{%
  \tikz[orp] \node[#1] at (#2) {#3};%
}

\newcommand\anchor[1]{%
  \tikz[orp] \coordinate (#1);%
}

\newcommand\nil[1]{
  \draw[nil] (#1.north east) -- (#1.south west)
}

\newcommand\cons[4][]{
  \node[cons,#1] (#2) {#3\kern3pt\anchor{#2 center}\kern3pt#4};
  \draw[thick] (#2 center |- #2.north) -- (#2 center |- #2.south)
}

\newcommand\genpointer[3][draw=blue!50!black,opacity=.5]{%
  \begin{tikzpicture}[rem,solid,#1]
    \node(pointer start) [circle, draw, inner sep=1.37pt] {};
    \draw[->, overlay] (pointer start) edge[bend right=#2] (#3);
  \end{tikzpicture}%
}

\newcommand\hpointer[2][30]{%
  \genpointer[draw=red!70!black]{#1}{#2}%
}

\newcommand\ppointer[2][30]{%
  \genpointer{#1}{#2}%
}

\newcommand\pointer[2][30]{%
  \ifhighlit\let\pointertemp\hpointer\else\let\pointertemp\ppointer\fi
  \pointertemp[#1]{#2}%
}


\newcount\reducetemp
\newcommand\reduce[3]{%
  \reducetemp#1\relax
  \advance\reducetemp -1\relax
  \alt<#1->{#3}{\alt<\the\reducetemp>{\highlight{#2}}{#2}}%
}

\tikzset{
  orp/.style={
    overlay,
    remember picture,
  },
  rem/.style={
    remember picture,
  },
  every node/.style={
    very thick,
    inner sep=3pt,
  },
  every path/.style={very thick},
  ref/.style={
    draw=red,
    densely dotted,
    font=\ttfamily,
  },
  cons/.style={
    draw=black,
    font=\ttfamily,
  },
  nil/.style={
    black!80!white,
    shorten >=2pt,
    shorten <=2pt,
  },
  highlight/.style={
    color=yellow!50!white!90!black,
    thick,
    draw,
    draw opacity=0.5,
    outer color=yellow!50,
    inner color=yellow!30,
    rounded corners=2pt,
  },
  higherlight/.style={
    highlight,
    outer color=green,
  },
}

\newif\ifhighlit
\highlitfalse

\newcommand<>\highlight[2][\relax]{%
  \ifx#1\relax
    \begin{tikzpicture}
  \else
    \begin{tikzpicture}[remember picture]
  \fi
    \node[
      inner sep=0pt,
      outer sep=0pt,
      text depth=0pt,
      align=left,
    ] (highlight temp) {%
        \only#3{\highlittrue}%
        #2%
        \only#3{\highlitfalse}%
    };
    \begin{scope}[on background layer]
      \coordinate (highlight temp 1) at
        ($(highlight temp.north east) + (2pt,2pt)$);
      \coordinate (highlight temp 2) at
        ($(highlight temp.base west) + (-2pt,-3pt)$);
      \node#3 [
        overlay,
        highlight,
        inner sep=0pt,
        fit=(highlight temp 1) (highlight temp 2),
      ] {};
    \end{scope}
    \ifx#1\relax
      \relax
    \else
      \node (#1) [
        overlay,
        inner sep=0pt,
        fit=(highlight temp 1) (highlight temp 2),
      ] {};
    \fi
  \end{tikzpicture}%
}

\newcommand\HI[2]{\highlight<#1>{#2}}

\newcommand\huncover[3][\relax]{%
  \uncover<#2->{\highlight<#2>[#1]{#3}}%
}
\newcommand\honly[3][\relax]{%
  \only<#2->{\highlight<#2>[#1]{#3}}%
}

\def\S#1#2{#2<#1>}

\begin{document}

%%%
%%% TITLE PAGE
%%%

\begin{frame}[t,fragile]\relax
  \thispagestyle{empty}
  \vfill
  \begin{center}
  {\Huge Almost Done}
        \par
  \bigskip
  \Large
  CS 51 and CSCI E-51
        \par
  \medskip
  April 10, 2014
  \end{center}
        \par
  %% Need to initialize Tikz opacity inside a frame, not outside:
  \begin{tikzpicture}[opacity=0,overlay]\end{tikzpicture}
\end{frame}

\newcommand<>\emphsame[1]{%
  \begin{tikzpicture}
    \node[
      inner sep=0pt,
      outer sep=0pt,
      text depth=0pt,
      align=left,
    ] (option) {{#1}};
    \node#2[
      inner sep=1pt,
      overlay,
      fit=(option),
      circle,
      green!50!black,
      draw,
      thick,
    ] {};
  \end{tikzpicture}%
}

\begin{frame}{Question 6(a)}
  \alt<4->{
    \strut \highlight<5>{Whenever} a function calls itself, applying
    a tail recursion optimization\\saves space.
  }{
    \strut\\
    Tail recursion optimization saves
    \highlight<2-3>{\alt<3->{space}{time}} whenever a function calls
    itself.
  }
  \hfill
  T \quad \emphsame<6->{F}

  \par\bigskip
  \uncover<6>{
  \begin{center}
     Proper tail call optimization applies \emph{only} when a call is in
     \emph{tail position}.
  \end{center}
  }
\end{frame}

\begin{frame}{You Are Not Alone}
  \begin{quote}
  ``\ldots unfortunately Python's compiler cannot reliably determine whether any
  particular call is actually reference the current function, even if it
  appears to have the same name.''
  \hfill ---Guido van Rossum, Python BDFL
  \end{quote}
\end{frame}

\begin{frame}[fragile]{So What Is This ``Tail Position''?}
  It's the context of the expression whose value will be the result of
  the function.

  \par\bigskip
  \pause
  \begin{overprint}
  \onslide<2>
  \begin{code}
     \K‹fun› x -> \highlight‹\strut         ›
  \end{code}
  \onslide<3>
  \begin{code}
     \K‹fun› x -> \highlight‹\strut    6    ›
  \end{code}
  \onslide<4>
  \begin{code}
     \K‹fun› x -> \highlight‹\strut‹›f (x + 1)›
  \end{code}
  \onslide<5>
  \begin{code}
     \K‹fun› x -> \highlight‹\strut‹› g (f x) ›
  \end{code}
  \onslide<6>
  \begin{code}
     \K‹fun› x -> \highlight‹\strut‹›\K‹if›     \K‹then›         \K‹else›        ›
  \end{code}
  \onslide<7>
  \begin{code}
     \K‹fun› x -> \strut‹›\K‹if›     \K‹then› \highlight‹       \strut› \K‹else› \highlight‹       \strut›
  \end{code}
  \onslide<8>
  \begin{code}
     \K‹fun› x -> \strut‹›\K‹if› c x \K‹then› \highlight‹g (f x)\strut› \K‹else› \highlight‹h (k x)\strut›
  \end{code}
  \end{overprint}
\end{frame}

\begin{frame}{Question 6(d)}
   \vfill
   In lecture, we introduced laziness for streams in order to avoid
   recomputing the same stream element values repeatedly.
   \hfill
   \emphsame<4->T \quad F

   \par\bigskip
   \pause
   \begin{quote}
   ``I thought you introduced laziness to delay computation so the
   streams wouldn't just loop.''
   \end{quote}

   \par\medskip
   \pause
   \begin{quote}
   ``Do you mean memoization?''
   \pause
   \textcolor{blue!50!green!80!black}{\emph{Yes!}}
   \end{quote}
\end{frame}

\begin{frame}{Laziness Is Efficient (Sometimes)}
  Laziness is when you wait to do it until you absolutely have to\ldots%
  \pause\\\hfill but then you only do it \emph{once}.

  \par\pause
  \bigskip
  \begin{center}
  Memoization is how we \emph{implement} laziness.
  \end{center}
\end{frame}

\CenterFrame{Speaking of exams\ldots}

\begin{frame}{How to Grade Exams Quickly}
  \begin{center}
  Don't do it by yourself.
  \end{center}
\end{frame}

\begin{frame}{Word Problem Time\strut}
  \begin{center}
  If one CS 51 TF can grade 100 exams in 10 hours, \\
  how long does it take for 20 CS 51 TFs to grade 300 exams?
  \end{center}
  \par
  \pause
  $$
  \frac{10\;\textrm{TF}\cdot\textrm{hrs}}
       {100\;\textrm{exams}}
       \pause
  \times
  \frac{300\;\textrm{exams}}
       {20\;\textrm{TFs}}
       \pause
  =
  1.5\;\textrm{hrs}
  $$
\end{frame}

\begin{frame}{Let's Try Another\strut}
  \begin{center}
  If one CS 51 TF can grade 100 exams in 10 hours, \\
  how long does it take for 2,000 CS 51 TFs to grade 300 exams?
  \end{center}
  \par
  \pause
  $$
  \frac{10\;\textrm{TF}\cdot\textrm{hrs}}
       {100\;\textrm{exams}}
       \pause
  \times
  \frac{300\;\textrm{exams}}
       {2{,}000\;\textrm{TFs}}
       \pause
  =
  \alt<5>{\cancel{0.015\;\textrm{hrs}}}
         {0.015\;\textrm{hrs}}
         \mbox{\color{red!70!black}\fontspec{Spaghetti}%
               \rlap{~\uncover<5>{Hell if I know!}}}
  $$
\end{frame}

\begin{frame}{One More\strut}
  \begin{center}
  If one CS 51 TF can grade 100 exams in 10 hours, \\
  how long does it take for 900 CS 51 TFs to grade 300 exams?
  \end{center}
  \par
  \pause
  $$
  \frac{10\;\textrm{TF}\cdot\textrm{hrs}}
       {100\;\textrm{exams}}
       \pause[2]
  \times
  \frac{300\;\textrm{exams}}
       {900\;\textrm{TFs}}
       \pause[2]
  =
  0.0\overline{3}\;\textrm{hrs}
         \mbox{\rlap{\uncover<3>{${} + \textit{overhead}$}}}
  $$
\end{frame}

\begin{frame}{Parallel Speedup}
    Speedup $S_p$ is given by
    $$
        S_p = \frac{T_1}{T_p}
    $$
    where
    \begin{itemize}
      \item $p$ is the number of CS 51 TFs,
      \item $T_1$ is the time for one TF to do it alone, and
      \item $T_p$ is the time for $p$ TFs to do it.
    \end{itemize}
    \par\pause\bigskip
    Speedup is \emph{linear} (or ``\emph{optimal}'') when $S_p = p$.
\end{frame}

\begin{frame}{Linear Speedup is Rare}
  \centering
  \includegraphics[width=.8\textwidth]{speedup.png}
\end{frame}

\begin{frame}[t,fragile]\relax
  \thispagestyle{empty}
  \vfill
  \begin{center}
  {\Huge Parallelism}
        \par
  \bigskip
  \Large
  CS 51 and CSCI E-51
        \par
  \medskip
  April 10, 2014
  \end{center}
        \par
  %% Need to initialize Tikz opacity inside a frame, not outside:
  \begin{tikzpicture}[opacity=0,overlay]\end{tikzpicture}
\end{frame}

\begin{frame}{Topic for Today: Parallelism}
\begin{itemize}
\item What is it?
\item Why should we care?
\item How can we take advantage of it?
\item Why is it so gosh darn difficult?
\item Some linguistic constructs
  \begin{itemize}
  \item thread creation
  \item thread coordination: locks \& conditions
  \item thread coordination: communication channels
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{What Is It?}
\centering
Doing many things at the same time instead of sequentially
\end{frame}

\begin{frame}{Flavors of Parallelism}
\begin{itemize}
\item<1-> Data parallelism
\begin{itemize}
\item same computation being performed on a lot of data
\item \emph{e.g.,} adding two vectors of numbers
\end{itemize}
\item<2-> Task parallelism
\begin{itemize}
\item different computations/programs running at the same time
\item \emph{e.g.,} running web server and database
\end{itemize}
\item<3-> Pipeline parallelism
\begin{itemize}
\item assembly line
\item \emph{e.g.,} parse function, type-check function, translate
to intermediate representation, optimize representation, register
allocate, code generate
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Parallelism vs. Concurrency}
  \begin{itemize}
    \item<1-> No consistent definitions
    \item<2-> For our purposes:
    \begin{itemize}
      \item Parallelism is about speed
      \item Concurrency is about dealing with multiple things at once
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Why Is Parallelism (Increasingly) Important?}
  \centering
  In a word: power
\end{frame}

\begin{frame}{}
  \centering
  \includegraphics[height=.8\textheight]{CPUs.jpg}
\end{frame}

%%% EXTRA SLIDES HERE (this is between slides 48 and 49)

\begin{frame}{Why Is It Particularly Important (Today)?}
\begin{itemize}
\item Roughly every other year, a chip from Intel would:
\begin{itemize}
\item halve the feature size (size of transistors, wires, etc.)
\item double the number of transistors
\item double the clock speed
\end{itemize}
\item<2-> No longer able to double clock or cut voltage---a
      processor won’t get any faster!
\begin{itemize}
\item power and heat are limitations on the clock
\item errors, variability (noise) are limitations on the voltage
\item but we can still pack in a lot of transistors (at least for 10--15
more years)
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{So Here's What's Happening}
They can no longer to make your CPU go faster, so instead they'll
sell you more of them (packed into one chip)
\begin{itemize}
\item<2-> 2006: dual core
\item<3-> 2008: quad core
\item<4-> 2010--now: 6 cores
\item<5-> Intel is testing 48-core chips
\item<6-> Within 10 years, you’ll have \~1024 Intel CPUs on a chip
\end{itemize}
\pause[7]
\par\medskip
Already happening with graphics chips
\end{frame}

\begin{frame}{Sounds Great!}
  In 10 years, my programs will run 1024 times faster!
  \pause
  Well, not so fast:
  \begin{itemize}
    \item Not every program can be effectively parallelized
    \begin{itemize}
      \item Perfect speedups are rare
      \item We're limited by parts that have to be sequential
    \end{itemize}
    \item Parallel programs require coordination
  \end{itemize}
\end{frame}

\begin{frame}{But Wait, It Gets Worse}
  Parallel code is \emph{hard} to get right
  \begin{itemize}
    \item<2-> New kinds of errors
    \begin{itemize}
      \item Race conditions
      \item Deadlocks
    \end{itemize}
    \item<3-> New kinds of performance problems
    \begin{itemize}
      \item Busy waiting
      \item Contention
    \end{itemize}
  \end{itemize}
  \pause[4]
  Programmers tend to get all this stuff wrong
  \par\medskip
  \pause[5]
  Testing and debugging become a nightmare
\end{frame}

\begin{frame}{Solid Parallel Programming}
\begin{itemize}
  \item Good baseline programming skills
  \item Deep knowledge of the application
  \item Discipline in following safe programming patterns
  \item Careful engineering to ensure scaling
\end{itemize}
\end{frame}

%%% GREG SLIDES HERE (26 to end)

\end{document}
