(* Lecture 12 (03/11/2014): References and Mutation *)

let cell = ref true

let mystery _ =
  cell := not (!cell);
  if !cell then 0 else Int.max_value

(*
 * SPOILER SPACE
 *)

let rem_longer xs xss =
  List.filter xss ~f:(fun ys -> List.length ys <= List.length xs)

let rem_longer xs xss =
  let temp1 = List.length xs in
  List.filter xss ~f:(fun ys -> List.length ys <= temp1)

;;
rem_longer [0; 0] [[1]; [1; 2]; [1; 2; 3]; [1; 2; 3; 4]; [1; 2; 3; 4; 5]]

let rec rem_mystery xs =
  List.filter ~f:(fun ys -> List.length ys <= mystery xs)

let rec rem_mystery xs =
  let temp1 = mystery xs in
  List.filter ~f:(fun ys -> List.length ys <= temp1)

;;
rem_mystery [0; 0] [[1]; [1; 2]; [1; 2; 3]; [1; 2; 3; 4]]



let c = ref 8

let x = !c

let _ = c := 10

let y = !c



let c = ref 0

let next s =
  let v = !c in
  c := v + 1;
  v

let _ = next ()

let _ = next () + next ()



let next s =
  let v = !c in
  c := v + 1;
  print_string s;
  v

let _ = next "left\n" + next "right\n"



let c = ref 0
let x = c

let _ = c := 10

let _ = !x

let c = ref 1

let y = !c


let x = 3
let c = ref (8 + x)



module type IMP_STACK =
  sig
    type 'a t

    (* Make a new, empty stack *)
    val empty : unit ->'a t

    (* Push a value on a stack *)
    val push : 'a -> 'a t -> unit

    (* functional push : 'a -> 'a t -> 'a t *)

    (* Pop a value off a stack *)
    val pop : 'a t -> 'a option

    (* functional pop : 'a t -> ('a * 'a t) option *)
  end

module ImpStack : IMP_STACK =
  struct
    type 'a t = 'a list ref

    let empty () = ref []

    let push x stack =
      stack := x :: !stack

    let pop stack =
      match !stack with
      | [] -> None
      | x :: xs ->
         stack := xs;
         Some x
  end

module I = ImpStack

let s = I.empty ()

let _ = I.push 5 s

let _ = I.pop s

let s' = s

let _ = I.push 5 s'; I.push 6 s'; I.push 7 s'

let _ = I.pop s


module type IMP_QUEUE =
  sig
    type 'a t

    val empty : unit -> 'a t
    val enq : 'a -> 'a t -> unit
    val deq : 'a t -> 'a option
  end

module ImpQueue : IMP_QUEUE =
  struct
    type 'a t = { front : 'a list ref; rear : 'a list ref }

    let empty () = { front = ref []; rear = ref [] }

    let enq (x : 'a) (q : 'a t) : unit = q.rear := x :: !(q.rear)

    let deq q =
      match !(q.front) with
      | x :: xs ->
         q.front := xs;
         Some x
      | _ -> (match List.rev !(q.rear) with
              | x :: xs ->
                 q.front := xs;
                 q.rear := [];
                 Some x
              | _ -> None)
  end

module I = ImpQueue


let q = I.empty ()

let _ = I.enq 3 q
let _ = I.enq 4 q
let _ = I.enq 5 q

let _ = I.deq q

let _ = I.enq 6 q

let _ = I.deq q
let _ = I.deq q
let _ = I.deq q
let _ = I.deq q


module ImpQueue2 : IMP_QUEUE =
  struct
    type 'a mlist = Nil | Cons of 'a * 'a mlist ref
    type 'a t = { front : 'a mlist ref; back : 'a mlist ref }

    let empty () = { front = ref Nil; back = ref Nil }

    let enq x q =
      match !(q.back) with
      | Cons(_, r) ->
         assert( !r = Nil );
         r := Cons(x, ref Nil);
         q.back := !r
      | Nil ->
         assert( !(q.front) = Nil );
         q.front := Cons(x, ref Nil);
         q.back := !(q.front)

    let deq q =
      match !(q.front) with
      | Cons(x, r) ->
         q.front := !r;
         if !r = Nil then q.back := Nil;
         Some x
      | Nil -> None
  end

module I = ImpQueue2


let q = I.empty ()

let _ = I.enq 3 q
let _ = I.enq 4 q
let _ = I.enq 5 q

let _ = I.deq q

let _ = I.enq 6 q

let _ = I.deq q
let _ = I.deq q
let _ = I.deq q
let _ = I.deq q
