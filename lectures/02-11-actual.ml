(* Lecture 5: 02/11/2014 *)

(*

* Brace yourselves

  It's buzzword time


* OCaml is a *functional* language

  - small core based on lambda calculus

  - control done with recursion

  - makes it easy to specify (and learn?)

  - higher-order, lexically-scoped, first-class functions (like
    Scheme, Standard ML, Haskell, Closure, CoffeeScript)

* OCaml is "mostly" *pure*

  - as opposed to most imperative languages (e.g., C/C++, C#, Java,
    Python, FORTRAN, ...)

  - emphasis on values, not variables or state

    - (this can make it easier to reason about)

  - still has arrays, I/O, etc. if you need it

* OCaml is *(statically) typed*

  - type errors are caught at compile time

  - other statically typed languages: Java, C#, C/C++, Standard ML

  - some dynamically typed languages: Python, Ruby, JavaScript, Scheme

* OCaml expressions

  > ‹exp›   ::= ‹value›
  >           | ‹id›
  >           | ‹exp› ‹op› ‹exp›
  >           | ‹exp› ‹exp›
  >           | let ‹id› = ‹exp› in ‹exp›
  >           | if ‹exp› then ‹exp› else ‹exp›
  >           | ...







* OCaml expressions and values

  > ‹exp›   ::= ‹value›
  >           | ‹id›
  >           | ‹exp› ‹op› ‹exp›
  >           | ‹exp› ‹exp›
  >           | let ‹id› = ‹exp› in ‹exp›
  >           | if ‹exp› then ‹exp› else ‹exp›
  >           | ...

  > ‹value› ::= ‹number›
  >           | ‹string›
  >           | ‹boolean›
  >           | fun ‹id› -> ‹exp›
  >           | ...


  fun x -> fun y -> ... x ... y ...

  let x = (let x = 6 in x + x) in x + 2
* (Informal) reduction model

  let x = 3 in
  let y = 10 + 7 in
  x * y

  -->

  let y = 10 + 7 in
  3 * y

  -->

  let y = 17 in
  3 * y

  -->

  3 * 17

  -->

  51

* Function calls

  let inc = (fun x -> x + 1) in
  inc (2 * 25)

  (fun x -> x + 1) (2 * 25)

  (fun x -> x + 1) 50

  50 + 1

  51

* Another example

  let add x y = x + y in
  let inc = add 1 in
  let dec = add (-1) in
  dec (inc 51)

  ==desugar==>

  let add = fun x -> fun y -> x + y in
  let inc = add 1 in
  let dec = add (-1) in
  dec (inc 51)

  let inc = (fun x -> fun y -> x + y) 1 in
  let dec = (fun x -> fun y -> x + y) (-1) in
  dec (inc 51)

  let inc = (fun y -> 1 + y) in
  let dec = (fun x -> fun y -> x + y) (-1) in
  dec (inc 51)

  let dec = (fun x -> fun y -> x + y) (-1) in
  dec ((fun y -> 1 + y) 51)

  let dec = (fun y -> (-1) + y) in
  dec ((fun y -> 1 + y) 51)

  (fun y -> (-1) + y) ((fun y -> 1 + y) 51)

  (fun y -> (-1) + y) (1 + 51)

  (fun y -> (-1) + y) 52

  (-1) + 52

  51

* Variable renaming

  let x = 3 in
  let y = 10 + 7 in
  x * y

  let c = 3 in
  let b = 10 + 7 in
  c * b

* Careful!

  let a = 3 in
  let y = 10 + 7 in
  a * y

  let a = 3 in
  let a = 10 + 7 in
  a * a


* Substitution

  let a = 3 in
  let a = 10 + 7 in
  a * a

  let a = 10 + 7 in
  3 * 3      (**** WRONG!!!! ****)

  let a = 10 + 7 in
  a * a

  let a = 17 in
  a * a

  17 * 17

  289

* A revised *let* rule

  let x = e1 in e2

  1. evaluate e1 to a value v

  2. substitute v for the _corresponding uses_ of x in e2

  3. evaluate the resulting expression

* Scope

  let a1 = 30 in
  let a2 = (let a3 = 3 in a3 * 4) in
  a2 + a2

* Code time

*)

(** A simple arithmetic language with variables **)

type op = Plus | Minus | Times

type var = string

type value = IntV of int
(*           | FunV of var * expr *)

 and expr = ValE of value
          | OpE of expr * op * expr    (* OpE(e1, Minus, e2) means e1 - e2 *)
          | LetE of var * expr * expr  (* LetE("x", e1, e2) means let x = e1 in e2 *)
          | VarE of var
(*          | AppE of expr * expr *)

(* 3 *)
let three = ValE(IntV 3)

(* 3 + 1 *)
let four = OpE(ValE(IntV 3), Plus, ValE(IntV 1))

let intE (z : int) : expr = ValE(IntV z)

let four = OpE(intE 3, Plus, intE 1)

(* let two = 2 in two + (two + 1) *)
let five = LetE("two", intE 2,
                OpE(VarE "two", Plus,
                    OpE(VarE "two", Plus, intE 1)))

exception UnboundVariable of var

(* Evaluate an operator on two values *)
let eval_op (o : op) (v1 : value) (v2 : value) : value =
  match o, v1, v2 with
  | Plus, IntV z1, IntV z2 -> IntV (z1 + z2)
  | Minus, IntV z1, IntV z2 -> IntV (z1 - z2)
  | Times, IntV z1, IntV z2 -> IntV (z1 * z2)

(* Substitute v for x in e {UGLY VERSION} *)
let rec substitute (v : value) (x : var) (e : expr) : expr =
  match e with
  | ValE u -> ValE u
  | OpE(e1, op, e2) -> OpE(substitute v x e1, op, substitute v x e2)
  | LetE(y, e1, e2) -> LetE(y, substitute v x e1,
                            if x = y then e2 else substitute v x e2)
  | VarE y -> if y = x then ValE v else VarE y

(* Substitute v for x in e *)
let substitute (v : value) (x : var) (e : expr) : expr =
  let rec subst (e : expr) =
    match e with
    | ValE u -> ValE u
    | OpE(e1, op, e2) -> OpE(subst e1, op, subst e2)
    | LetE(y, e1, e2) -> LetE(y, subst e1, if x = y then e2 else subst e2)
    | VarE y -> if y = x then ValE v else VarE y
  in
  subst e

(* Evaluate an expression *)
let rec eval (e : expr) : value =
  match e with
  | ValE v -> v
  | OpE(e1, op, e2) -> eval_op op (eval e1) (eval e2)
  | LetE(x, e1, e2) -> let v = eval e1 in
                       eval (substitute v x e2)
  | VarE x -> raise (UnboundVariable x)

assert( eval three = IntV 3 )
assert( eval four = IntV 4 )
assert( eval five = IntV 5 )

assert( eval (LetE("x", intE 3,
                   LetE("x", intE 4,
                        VarE "x")))
             = IntV 4 )

assert( eval (LetE("x", intE 3,
                   LetE("y", intE 4,
                        VarE "x")))
             = IntV 3 )
