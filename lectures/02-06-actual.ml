(* Lecture notes for intro to OCaml, continued (2014-02-06)
 * (higher-order functions, polymorphism) *)

(* Increment every int in a list *)
let rec inc_list (xs : int list) : int list =
  match xs with
  | [] -> []
  | x :: xs' -> 1 + x :: inc_list xs'

(* Decrement every int in a list *)
let rec dec_list (xs : int list) : int list =
  match xs with
  | [] -> []
  | x :: xs' -> -1 + x :: dec_list xs'

(* Add 6 to every int in a list *)
let rec add6_list (xs : int list) : int list =
  match xs with
  | [] -> []
  | x :: xs' -> 6 + x :: add6_list xs'

(* Add an int to every int in a list *)
let rec add_to_list (n : int) (xs : int list) : int list =
  match xs with
  | [] -> []
  | x :: xs' -> n + x :: add_to_list n xs'

let inc_list xs = add_to_list 1 xs
let dec_list xs = add_to_list (-1) xs
let add6_list xs = add_to_list 6 xs

(* Square every int in a list *)
let rec square_list (xs : int list) : int list =
  match xs with
  | [] -> []
  | x :: xs' -> x * x :: square_list xs'

(* Map a function of every element of an int list *)
let rec map (f : int -> int) (xs : int list) : int list =
  match xs with
  | [] -> []
  | x :: xs' -> f x :: map f xs'

let inc x = x + 1
let add6 x = x + 6
let square x = x * x

let inc_list (xs : int list) : int list = map inc xs
let add6_list (xs : int list) : int list = map add6 xs
let square_list (xs : int list) : int list = map square xs

(* Add an int to every int in a list *)
let add_to_list (n : int) (xs : int list) : int list =
  let addn x = x + n in
  map addn xs

(* Add an int to every int in a list *)
let add_to_list (n : int) (xs : int list) : int list =
  map (fun x -> x + n) xs

let inc_list (xs : int list) : int list = map (fun x -> x + 1) xs
let add6_list (xs : int list) : int list = map (fun x -> x + 6) xs
let square_list (xs : int list) : int list =
  map (fun asdfjkl -> asdfjkl * asdfjkl) xs

(* To square an int and add it to another int *)
let square_and_add : int -> int -> int =
  fun (x : int) ->
    fun (y : int) ->
      x * x + y

let inc_list = map (fun x -> x + 1)
let dec_list = map (fun x -> x - 1)

let inc_list = map ((+) 1)
let dec_list = map (fun x -> x - 1)

(* To exchange the order of the arguments of a function *)
let flip f x y = f y x

let dec_list = map (flip (-) 1)

let add_to_list (n : int) = map ((+) n)

(* To add up the ints in a list *)
let rec sum (xs : int list) : int =
  match xs with
  | [] -> 0
  | x :: xs' -> x + sum xs'

(* To multiply the ints in a list *)
let rec prod (xs : int list) : int =
  match xs with
  | [] -> 1
  | x :: xs' -> x * prod xs'

(* To fold a function over a list, right-associatively *)
let rec fold_right (f : int -> int -> int) (z : int) (xs : int list) : int =
  match xs with
  | [] -> z
  | x :: xs' -> f x (fold_right f z xs')

let sum xs = fold_right (+) 0 xs

let inc = (+) 1

(* Convert a list of ints to a list of strings *)
let rec strings_of_ints (xs : int list) : string list =
  match xs with
  | [] -> []
  | x :: xs' -> string_of_int x :: strings_of_ints xs'

(* Map a function of every element of a list *)
let rec map (f : 'a -> 'b) (xs : 'a list) : 'b list =
  match xs with
  | [] -> []
  | x :: xs' -> f x :: map f xs'

let strings_of_ints (xs : int list) : string list =
  map string_of_int xs

(* Keep only the positive ints in a list *)
let rec keep_positive (xs : int list) : int list =
  match xs with
  | [] -> []
  | x :: xs' -> let rest = keep_positive xs' in
                if x > 0 then x :: rest
                else rest

(* To fold a function over a list, right-associatively *)
let rec fold_right (f : 'a -> 'b -> 'b) (z : 'b) (xs : 'a list) : 'b =
  match xs with
  | [] -> z
  | x :: xs' -> f x (fold_right f z xs')

let keep_positive (xs : int list) : int list =
  fold_right (fun x rest -> if x > 0 then x :: rest else rest) [] xs

(*
   fold_right f z (a :: (b :: (c :: [])))
    =
                  (f a  (f b  (f c  z))
 *)

let keep_positive (xs : int list) : int list =
  List.fold_right ~init:[] ~f:(fun x rest -> if x > 0 then x :: rest else rest) xs
