(* Lecture 14: Infinite data structures *)

type 'a stream = Cons of 'a * 'a stream

let head (Cons(x, _) : 'a stream) : 'a        = x
let tail (Cons(_, r) : 'a stream) : 'a stream = r

let rec ones = Cons(1, ones)

assert( head ones = 1 )
assert( head (tail ones) = 1 )
assert( head (tail (tail ones)) = 1 )

assert( phys_equal ones (tail ones) )
assert( phys_equal ones (tail (tail ones)) )

let rec ones' = Cons(1, Cons(1, ones'))

assert( phys_equal ones' (tail (tail ones')) )
assert( not (phys_equal ones' (tail ones')) )

let rec count_from (n : int) : int stream =
  Cons(n, count_from (n + 1))

(* This is gonna blow the stack: *)
let rec nats = count_from 0

let rec map (Cons(x, r) : 'a stream) ~(f : 'a -> 'b) : 'b stream =
  Cons(f x, map r ~f)

let twos = map ~f:((+) 1) ones

let x = printf "hello\n"; 2 + 3

let x = fun () -> printf "hello\n"; 2 + 3

(** TAKE TWO **)

(*
let rec map (Cons(x, r) : 'a stream) ~(f : 'a -> 'b) : 'b stream =
  Cons(f x, fun () -> map r ~f)
 *)

type 'a stream = unit -> 'a str
 and 'a str = Cons of 'a * 'a stream

type 'a stream = unit -> 'a str
 and 'a str = { hd : 'a; tl : 'a stream }

let rec ones : int stream =
  fun () -> { hd = 1; tl = ones }

let head (s : 'a stream) : 'a = (s ()).hd
let tail (s : 'a stream) : 'a stream = (s ()).tl

let rec count_from (n : int) : int stream =
  fun () -> { hd = n; tl = count_from (n + 1) }

let nats = count_from 0

let rec take (s : 'a stream) (n : int) : 'a list =
  if n <= 0 then []
  else head s :: take (tail s) (n - 1)

let rec map (s : 'a stream) ~(f : 'a -> 'b) : 'b stream =
  fun () -> { hd = f (head s);
              tl = map ~f (tail s) }

let twos = map ~f:((+) 1) ones

let rec nats =
  fun () -> { hd = 0;
              tl = map ~f:((+) 1) nats }

let rec map2 (s1 : 'a stream) (s2 : 'b stream)
             ~(f : 'a -> 'b -> 'c) : 'c stream =
  fun () -> { hd = f (head s1) (head s2);
              tl = map2 (tail s1) (tail s2) ~f }

let threes = map2 ~f:(+) ones twos

let rec fibs : int stream =
  fun () -> { hd = 0;
              tl = fun () -> { hd = 1;
                               tl = map2 ~f:(+) fibs (tail fibs) } }

(* AFTER EMACS *)

type 'a stream = 'a str lazy_t
 and 'a str = { hd : 'a; tl : 'a stream }

let head (s : 'a stream) : 'a = (Lazy.force s).hd

let tail (s : 'a stream) : 'a stream = (Lazy.force s).tl

let rec map (s : 'a stream) ~(f : 'a -> 'b) : 'b stream =
  lazy { hd = f (head s);
         tl = map (tail s) ~f }

let rec map2 (s1 : 'a stream) (s2 : 'b stream) ~(f : 'a -> 'b -> 'c) : 'c stream =
  lazy { hd = f (head s1) (head s2);
         tl = map2 (tail s1) (tail s2) ~f }

let rec take (s : 'a stream) (n : int) : 'a list =
  if n <= 0 then []
  else head s :: take (tail s) (n - 1)

let rec fibs =
  lazy { hd = 0;
         tl = lazy { hd = 1;
                     tl = map2 ~f:(+) fibs (tail fibs) } }

(* Please see 03-25-planned.ml for more *)
