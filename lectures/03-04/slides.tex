\documentclass{beamer}

\usepackage[quiet]{fontspec}
\usepackage{xunicode}
\usepackage{xltxtra}
\usepackage{graphicx}
\usepackage{forloop}
\usepackage{stmaryrd}
\usepackage{tikz}
\usepackage{almslides}
\usepackage{mathtools}

\usetikzlibrary{chains,decorations.pathmorphing,positioning,fit}
\usetikzlibrary{decorations.shapes,calc,backgrounds}
\usetikzlibrary{decorations.text,matrix}
\usetikzlibrary{arrows,shapes.geometric,shapes.symbols,scopes}
\usetikzlibrary{mindmap}
\usetikzlibrary{fadings}

\newcommand\bigO[1]{\ensuremath{\mathcal{O}(#1)}}
\newcommand\fun[1]{\ensuremath{\lambda #1.\,}}
\renewcommand\T[1]{\ensuremath{T_{\mathit{#1}}}}

%% %%% SETTINGS

\frenchspacing
\unitlength=0.01\textwidth
\thicklines
\urlstyle{sf}
\graphicspath{{images/}}

%% %%% VERBATIM SETUP
%% 
%% \DefineVerbatimEnvironment
%%   {code}{Verbatim}
%%   {commandchars=\\\{\},
%%    codes={\catcode`$=3\relax},
%%    formatcom=\small}
%% %\DefineShortVerb{v}
%% 
%% %%% COLOR AND BEAMER SETUP
%% 

\def\fern{407428}
\def\charcoal{4D4944}
\definecolor{DarkCharcoal}{HTML}{\charcoal}
\colorlet{Charcoal}{DarkCharcoal!85!white}
\colorlet{AlertColor}{orange!70!black}
\colorlet{DarkRed}{red!70!black}
\colorlet{DarkBlue}{blue!70!black}
\colorlet{DarkGreen}{green!50!black}

\colorlet{Client}{blue!60!black}
\colorlet{Server}{red!60!black}
\colorlet{Both}{violet!80!black}

\setbeamercolor{frametitle}{fg=black}
\setbeamercolor{normal text}{fg=black}

\defaultfontfeatures{
    Mapping=tex-text,
    Scale=MatchLowercase,
}
\setmainfont{TeX Gyre Schola}

\usefonttheme{serif}

\setbeamertemplate{frametitle}
  {\begin{centering}\medskip
   \insertframetitle\par
   \end{centering}}

\setbeamercolor{alerted text}{fg=AlertColor}
\setbeamertemplate{itemize item}{$\bullet$}

\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[text line]{%
    \hfill\strut{%
        \small\color{black!75}%
        \texttt{\insertframenumber}%
    }%
    \hfill%
}

%%% GENERALLY USEFUL MACROS

\def\LAM{\text{\textK{$\bm\lambda$}\,}}

\def\verysmall{%
    \fontsize{7.5pt}{8pt}\selectfont%
}

\newcommand\setPause[2][0]{%
  \setcounter{beamerpauses}{#2}%
  \addtocounter{beamerpauses}{#1}%
}

\newcommand<>\always[1]{#1}

\newcommand\aalt[4]{%
  \alt<#1#2>{\color<#1>{AlertColor}#3\color{Charcoal}\relax}{#4}%
}

\newbox\mytikzbox
\def\settikz{\setbox\mytikzbox=\hbox}
\def\usetikz{\usebox\mytikzbox}

\def\defgbox#1#2{%
  \expandafter\global\expandafter\newbox\csname#2box\endcsname%
  \expandafter\global\expandafter\def\csname#2\endcsname{%
    \expandafter\usebox\csname#2box\endcsname%
  }%
  \expandafter\global\expandafter\setbox\csname#2box\endcsname=#1%
}
\def\defhbox{\defgbox\hbox}
\def\defvbox{\defgbox\vbox}

\def\setgbox#1#2{%
  \expandafter\setbox\csname#2box\endcsname=#1%
}
\def\sethbox{\setgbox\hbox}
\def\setvbox{\setgbox\vbox}

\newcommand<>\EXP{\textcolor{Both}#1{\Exp}}

\newcommand\tikzinfer[3][\relax]{
  \begin{tikzpicture}
    \node(premiss) {#2};
    \node(conclusion)[at=(premiss.south),anchor=north] {#3};
    \node(infer rule)[fit=(premiss) (conclusion),inner sep=0] {};
    \draw[math line]
      (premiss.south -| infer rule.west) -- (premiss.south -| infer rule.east)
      ;
    \if#1\relax \else
      \node(infer label)[at=(infer rule.east),anchor=mid west] {(#1)};
    \fi
  \end{tikzpicture}
}

\def\mdef#1#2{\def#1{\ensuremath{#2}}}
\def\mnew@command#1[#2]#3{\newcommand#1[#2]{\ensuremath{#3}}}
\def\mnewcommand#1#2{%
  \ifthenelse{\equal{#2}{[}}
    {\mnew@command{#1}[}
    {\mdef{#1}{#2}}
}
\let\mnew\mnewcommand

\mdef\EnvD{
  \mathord{
    \tikz \draw[math line] (0,0) -- (.66em,0em) -- (.33em,1.3ex) -- cycle;
  }
}
\mdef\EnvG{
  \mathord{
    \tikz \draw[math line] (0,0) -- (0,1.4ex) -- +(.5em,0);
  }
}
\mdef\Proves{
  \mathrel{
    \tikz \draw[math tline] (0,0) -- (0,1.3ex) (0,.65ex) -- +(1ex,0);
  }
}
\mdef\Exp{
  \mathord{!\kern.5pt}
}
\mdef\pxE{
  \mathord{\text{\textexclamdown}\kern.5pt}
}

%%%
%%% TIKZ
%%%

\tikzfading[
  name=fade outer,
  inner color=transparent!0,
  outer color=transparent!25,
]

\tikzset{
  every node/.style={very thick},
  every path/.style={very thick},
  invisible state/.style={
    rectangle,
    minimum size=5mm,
    rounded corners=1mm,
    text height=1.75ex,
    text depth=0.5ex,
  },
  state coloring/.style={
    draw=#1,
    top color=white,
    bottom color=#1!20,
  },
  state/.style={
    invisible state,
    state coloring=#1,
  },
  state/.default={DarkGreen},
  info box/.style={
    draw=#1,
    inner color=white,
    outer color=#1!20,
    rounded corners=1pt,
    align=left,
    font=\ttfamily\color{#1!50!black},
  },
  info box/.default={DarkRed},
  trans/.style={
    ->,
    shorten >=0.5pt,
    rounded corners,
  },
  >=stealth,
  start here/.style={
    draw=black,
    append after command={\tikzlastnode -- +(-.1,-.1)},
  },
  math line/.style={line width=0.2ex},
  math tline/.style={line width=0.14ex},
  rstate/.style={
    circle,
    font=\scriptsize,
    inner sep=1.5pt,
    state coloring=#1,
  },
  rstate/.default={DarkGreen},
  final rstate/.style={
    rstate=#1,
    double,
    inner sep=2pt,
  },
  final rstate color/.default={DarkGreen},
  my concept/.style={
    circle,
    draw=#1!30,
    outer color=#1!30,
    inner color=#1!15,
    align=center,
    inner sep=0pt,
  },
  fade out/.style={
    orp,
    fit=(current page.south west) (current page.north east),
    inner color=white,
    outer color=#1,
    path fading=fade outer,
  },
  fade out/.default=DarkBlue!30,
}


%%% TITLE INFO

\title{Programming with Affine Types}
\author{Jesse A. Tov}
\institute{CS51}
\date{April 30, 2013}

\begin{document}

%%%
%%% REUSABLE PICTURES
%%%

\newcommand\testpattern{
  \begin{frame}[plain]{}
    \begin{tikzpicture}[
      orp,
      ultra thick
    ]
      \coordinate (ll) at ($ (current page.south west) + (.3pt,.3pt) $);
      \coordinate (ur) at ($ (current page.north east) - (.8pt,.8pt) $);
      \draw[red!80!black] (ll) rectangle (ur);
      \draw[green!80!black] ($ (ll) + (.5,.5) $) rectangle ($ (ur) - (.5,.5) $);
      \draw[blue] ($ (ll) + (1,1) $) rectangle ($ (ur) - (1,1) $);
      \path[minimum size=2cm,circle,opacity=.5]
        (current page.center)
        ++(-3.7,2.3) node (cyan) [fill=cyan] {}
        +(1,0) node (magenta) [fill=magenta] {}
        (intersection of cyan and magenta) node[fill=yellow] {}
      ;
      \path[minimum size=1.7cm,circle]
        (current page.center)
        ++(0,2.3) node[fill=Charcoal] {}
        ++(2,0) node[fill=DarkGreen] {}
        ++(2,0) node[fill=AlertColor] {}
        ;
      \path[minimum size=1.2cm,circle]
        (current page.center)
        ++(-.25,.25) node[fill=black] {}
        ++(1.5,0) node[fill=Client] {}
        ++(1.5,0) node[fill=Server] {}
        ++(1.5,0) node[fill=Both] {}
        ;
      \draw[line width=1ex,color=Charcoal]
        (current page.center) ++(-.85,-1) -- +(5.7,0) ;
      \draw[line width=.5ex,color=Charcoal]
        (current page.center) ++(-.85,-1.3) -- +(5.7,0) ;
      \draw[ultra thick,color=Charcoal]
        (current page.center) ++(-.85,-1.6) -- +(5.7,0) ;
      \draw[very thick,color=Charcoal]
        (current page.center) ++(-.85,-1.9) -- +(5.7,0) ;
      \draw[thick,color=Charcoal]
        (current page.center) ++(-.85,-2.2) -- +(5.7,0) ;
      \draw[semithick,color=Charcoal]
        (current page.center) ++(-.85,-2.5) -- +(5.7,0) ;
      \draw[thin,color=Charcoal]
        (current page.center) ++(-.85,-2.8) -- +(5.7,0) ;
      \draw[very thin,color=Charcoal]
        (current page.center) ++(-.85,-3.1) -- +(5.7,0) ;
      \draw[ultra thin,color=Charcoal]
        (current page.center) ++(-.85,-3.4) -- +(5.7,0) ;
      \path
        (ll) ++ (1.4,1.37)
        node[
          text width=3.5cm,
          anchor=south west,
          highlight,
          font=\scriptsize,
          text justified,
        ]
        {
          \color{black}
          {\normalsize Because propositions}
          {\small in such a logic}
          may no longer be freely copied or
          ignored,
          \color{AlertColor}
          this suggests understanding propositions in
          substructural logics as representing resources rather than
          truth.
        }
        ;
    \end{tikzpicture}
  \end{frame}
}

%%%
%%% TITLE PAGE
%%%

\begin{frame}\relax

  \begin{centering}
  {\Huge Algorithmic Complexity}

  \bigskip

  \Large
  CS 51 and CSCI E-51

  \medskip

  March 4, 2014

  \end{centering}

  %% Need to initialize Tikz opacity inside a frame, not outside:
  \begin{tikzpicture}[opacity=0,overlay]\end{tikzpicture}
\end{frame}

\begin{frame}{Complexity of algorithms}
  \begin{itemize}
    \item time \& space
    \item asymptotic growth
    \item big-O notation
    \item recurrences
    \item tail calls
  \end{itemize}
\end{frame}

\iftrue %%%% BEGIN IFFALSE

\begin{frame}[fragile]{Why?}
  To make comparisons:
  \begin{itemize}
    \item Why is mergesort ``better'' than insertion sort?
    \item Why is red-black tree ``better'' than an association list for
          representing sets?
    \item Is there ``better'' way to sum a list than using
          \verb|fold_right|?
    \item Why do I have to be careful using recursion to write loops in
          C and Java?
  \end{itemize}
\end{frame}

\begin{frame}{Problems with measuring}
  \begin{centering}
  \begin{tabular}{|l|l|}
    \hline
    {\Large \strut}\relax
    \textbf{Algorithm} & \textbf{Running time} \\
    \hline
    \hline
    {\Large \strut}\relax
    insertion sort & $c_1 n^2 + c_2$ \\
    \hline
    {\Large \strut}\relax
    mergesort & $d_1 n \log n + d_2$ \\
    \hline
  \end{tabular}
  \smallskip

  Table: Sorting a list of length $n$

  \end{centering}
\end{frame}

\begin{frame}{Asymptotic complexity}
    \begin{center}
    $\T{is}(n) = c_1 n^2 + c_2$ \qquad vs. \qquad
    $\T{ms}(n) = d_1 n \log n + d_2$

    \medskip
    \pause
    As $n$ grows to infinity, $\T{is}(n)$ grows faster than $\T{ms}(n)$
    \\ \pause
    \emph{regardless} of choice of constants.

    \medskip
    \pause
    What about $T(n) = h_1 n^2 + h_2 n + h_3$?
    \\ \pause
    It \emph{doesn't} grow faster than $\T{is}$ independent of
    constants.

    \bigskip
    Can we define ``faster''?

    \end{center}
\end{frame}

\begin{frame}{Goals}
  Examine code and determine worst case running time (or space)
  as a function of the inputs.
  \begin{itemize}
    \item no measuring
    \item symbolically!
  \end{itemize}

  \pause
  Be able to compare symbolic resource usage of functions to find which
  is asymptotically better.
  \begin{itemize}
    \item no plots
    \item big-O notation!
  \end{itemize}
\end{frame}

\begin{frame}{Big-O defined}
  Given $f : \textit{number} \to \textit{number}$,
  \\\pause
  \bigO{f} is the set of all functions $g$ such that:
  \begin{itemize}
    \pause
    \item there exist numbers $m$ and $c$ such that
    \pause
    \item for all $n > m$,
    \pause
    \item $g(n) \le c \cdot f(n)$.
  \end{itemize}

  \pause
  \medskip
  If $f \in \bigO g$ but $g \not\in \bigO f$, then $g$ grows faster than $f$.
  \begin{itemize}
    \item Write this $g \gg f$.
  \end{itemize}
\end{frame}

\begin{frame}{Example}
  I claim that
  \[
    \fun n {10n^2 + 3} \quad\in\quad \bigO{\fun n {n^2}}
  \]
  but not in
  \[
    \bigO{\fun n {n \log n}}
  \]
\end{frame}

\begin{frame}[t]{Example: Proving inclusion}
  \begin{centering}
  (\bigO{f} is the set of functions $g$ s.t.
    there exist\\ $m$ and $c$ s.t.
    for all $n > m$, $g(n) \le c \cdot f(n)$.)

  \end{centering}

  \vfill
  To prove: $\fun n {10n^2 + 3} \in \bigO{\fun n {n^2}}$.
  \pause\par\smallskip
  Choose $m = 1$.
  \pause\par\smallskip
  Need to find $c$ such that
  $10n^2 + 3 \le cn^2$ for $n > 1$.
  \begin{itemize}
    \pause
    \item When $n = 1$, $(\fun n {10n^2 + 3})(n) = 13$,
          \pause so need $c \ge 13$.
    \pause
    \item When $n = 2$, $(\fun n {10n^2 + 3})(n) = 43$,
          so need $c \ge \frac{43}{4}$.
    \pause
    \item When $n = 3$, $(\fun n {10n^2 + 3})(n) = 93$,
          so need $c \ge \frac{93}{9}$.
    \item When $n = 4$, $(\fun n {10n^2 + 3})(n) = 163$,
          so need $c \ge \frac{163}{16}$.
  \end{itemize}
  \pause
  Let's try 13 for $c$.
  \vfill
\end{frame}

\begin{frame}[t]{Example: Proving inclusion (2)}
  \begin{centering}
  (\bigO{f} is the set of functions $g$ s.t.
    there exist\\ $m$ and $c$ s.t.
    for all $n > m$, $g(n) \le c \cdot f(n)$.)

  \end{centering}

  \vfill
  To show: $10n^2 + 3 \le 13n^2$ for all $n > 1$.
  \pause\par\smallskip
  Let $k = n - 1$. Note that $k > 0$.
  \par\smallskip
  \begin{align*}
  \uncover<3->{
  10(k + 1)^2 + 3 &\le 13(k + 1)^2 \\
  }
  \uncover<4->{
  10(k^2 + 2k + 1) + 3 &\le 13(k^2 + 2k + 1) \\
  }
  \uncover<5->{
  10k^2 + 20k + 13 &\le 13k^2 + 26k + 13 \\
  }
  \uncover<6->{
  0                &\le 3k^2 + 6k
  }
  \end{align*}
  \uncover<6->{
  which is true, because $k > 0$.
  }
  \vfill
\end{frame}

%\begin{frame}[t]{Example: Proving exclusion}
%  \begin{centering}
%  (\bigO{f} is the set of functions $g$ s.t.
%    there exist\\ $m$ and $c$ s.t.
%    for all $n > m$, $g(n) \le c \cdot f(n)$.)
%
%  \end{centering}
%
%  \vfill
%  To prove: $\fun n {10n^2 + 3} \not\in \bigO{\fun n {n \log n}}$.
%  \pause\par\smallskip
%  Show: There are no suitable $m$ and $c$.
%  \pause\par\smallskip
%  Show (equivalently): For any $c$ and $m$, we can find an $n > m$ such that
%  $10n^2 + 3 > cn \log n$.
%  \vfill
%\end{frame}

\begin{frame}{Informal big-O}
  Mathematicians don't typically write the lambdas, so:
  \begin{itemize}
    \item \bigO{n} is shorthand for \bigO{\fun n n}.
    \item \bigO{m^2} is shorthand for \bigO{\fun m m^2}.
  \end{itemize}
  \pause\bigskip
  Sources of confusion:
  \begin{itemize}
    \item multiple variables
    \item \bigO{} is a higher-order function!
  \end{itemize}
\end{frame}

\begin{frame}{Some big-O facts}
  \begin{tabular}{ll}
    $\fun n {f(n) + k} \in \bigO f$
      & adding a constant doesn't matter \\
    \pause
    $\fun n {k \cdot f(n)} \in \bigO f$
      & multiplying by a constant doesn't matter \\
    \pause
    $\fun n {n^k + n^j} \in \bigO{\fun n {n^k}}$
      & bigger exponent wins
      \\\quad when $k > j$
  \end{tabular}

  \pause
  \bigskip
  So for instance, \[
    \fun n {4n^3 + 35n^2 + 782n^1 + 42} \quad\in\quad
      \bigO{\alt<5>{n^3}{?}}
  \]
\end{frame}

\begin{frame}{More big-O facts}
  \begin{tabular}{ll}
    $\fun n n^k \gg \fun n n^j$ when $k > j$
      & bigger exponents grow faster \\
    $\fun n n \gg \fun n \log n$
      & logarithms grow slower than linear \\
    $\bigO{\log_k n} = \bigO{\log_j n}$
      & logarithm bases don't matter \\
    $\fun n 2^n \gg \fun n n^k$
      & exponentials beat polynomials \\
    $\fun n 3^n \gg \fun n 2^n$
      & the base of an exponential matters
  \end{tabular}
\end{frame}

\begin{frame}{Common complexities}
  \begin{tabular}{lll}
    \bigO{1} & constant & \emph{e.g.,} array lookup \\
    \bigO{\log n} & log & \emph{e.g.,} binary search \\
    \bigO{n} & linear & \emph{e.g.,} linear search \\
    \pause
    \bigO{n\log n} & ``en log en'' & \emph{e.g.}, efficient sorting
            (mergesort, heapsort) \\
    \pause
    \bigO{n^2} & quadratic & \emph{e.g.,} quicksort, insertion sort \\
    \pause
    \bigO{n^k} & polynomial & \emph{e.g.,} Floyd-Warshall (\bigO{V^3}) \\
    \pause
    \bigO{2^n} & exponential & \emph{e.g.,} OCaml type checking!
  \end{tabular}
\end{frame}

\begin{frame}{Big-O summary}
  Big-O maps a function to a set of functions
  \begin{itemize}
    \item Intuitively: all functions with same growth rate (up to
    constants)
    \item Formally: difference is bounded by a constant factor
  \end{itemize}

  \pause
  \medskip
  Big-O gives us a technology-independent way to compare algorithms

  \pause
  \medskip
  How do we get from a program to there?
\end{frame}

\begin{frame}[fragile]{Time complexity of \textalms{append}}
  \begin{Alms}
let rec append xs ys =
  match xs with
  | [] -> ys
  | x :: xs' -> x :: append xs' ys
  \end{Alms}
  Want: $\T{append}$ as a function of the sizes of the arguments
  (\emph{i.e.,} lengths of \textalms{xs} and \textalms{ys}).

  \medskip
  \pause
  Follow the shape of the function:
  \begin{align*}
    \T{append}(0, m) &=
    \mathrlap{\only<1-2>{???}}
    \mathrlap{\only<4->{c_1}}
    \uncover<3>{\T{match} + \T{return}}
    \\
    \T{append}(n + 1, m) &=
    \mathrlap{\only<1-4>{???}}
    \mathrlap{\only<6>{c_2 + \T{append}(n, m)}}
    \uncover<5>{\T{match} + \T{cons} + \T{append}(n, m)}
    \\
  \end{align*}
\end{frame}

\begin{frame}{Solving the recurrence}
  \begin{align*}
    \T{append}(0, m) &= {c_1} \\
    \T{append}(n + 1, m) &= {c_2 + \T{append}(n, m)} \\[1em]
    \uncover<2->{
    \T{append}(0, m) &= c_1 \\
    }
    \uncover<3->{
    \T{append}(1, m) &= c_2 + c_1 \\
    }
    \uncover<4->{
    \T{append}(2, m) &= c_2 + c_2 + c_1 \\
    }
    \uncover<5->{
    \T{append}(n, m) &= \underbrace{c_2 + \cdots + c_2}_n + c_1 \\
    }
    \uncover<5->{
    \T{append}(n, m) &= c_2n + c_1 \\
    }
    \uncover<6->{
    \T{append}       &\in \bigO{n}
    }
  \end{align*}
\end{frame}

\begin{frame}[fragile]{Another example}
  \begin{Alms}
let rec rev xs =
  match xs with
  | [] -> []
  | x :: xs' -> append (rev xs') [x]
  \end{Alms}

  \medskip
  \pause
  Follow the shape of the function:
  \begin{align*}
    \T{rev}(0) &=
    \mathrlap{\only<1-2>{???}}
    \mathrlap{\only<4>{c_1}}
    \uncover<3>{\T{match} + \T{return}}
    \\
    \T{rev}(n + 1) &=
    \mathrlap{\only<1-2>{???}}
    \mathrlap{\only<4>{c_2 + \T{rev}(n) + c_3 n}}
    \uncover<3>{\T{match} + \T{cons} + \T{rev}(n) + \T{append}(n, 1)}
    \\
  \end{align*}
\end{frame}

\begin{frame}[t]{Solving the recurrence}
  \begin{align*}
    \T{rev}(0) &= {c_1} \\
    \T{rev}(n + 1) &= {c_2 + c_3 n + \T{rev}(n)} \\[1em]
    \uncover<2->{
    \T{rev}(0) &= c_1 \\
    }
    \uncover<3->{
    \T{rev}(0 + 1) &= c_2 + 0c_3 + c_1 \\
    }
    \uncover<4->{
    \T{rev}(1 + 1) &= c_2 + 1c_3 + c_2 + 0c_3 + c_1 \\
    }
    \uncover<5->{
    \T{rev}(2 + 1) &= c_2 + 2c_3 + c_2 + 1c_3 + c_2 + 0c_3 + c_1 \\
    }
    \uncover<6->{
    \T{rev}(n) &= \sum_{i = 0}^{n - 1}(c_2 + c_3 i) + c_1 \\
    }
  \end{align*}
\end{frame}

\begin{frame}[t]{Solving the recurrence}
  \begin{align*}
    \T{rev}(0) &= {c_1} \\
    \T{rev}(n + 1) &= {c_2 + c_3 n + \T{rev}(n)} \\[1em]
    \uncover<1->{
    \T{rev}(n) &=
      \mathrlap{\sum_{i = 0}^{n - 1}(c_2 + c_3 i) + c_1}
      \phantom{c_2 + 2c_3 + c_2 + 1c_3 + c_2 + 0c_3 + c_1} \\
    }
    \uncover<2->{
    \T{rev}(n) &= \sum_{i=0}^{n-1} c_2 + \sum_{i=0}^{n-1} c_3i + c_1 \\
    }
    \uncover<3->{
    \T{rev}(n) &= c_2n + \frac{c_3}{2}(n - 1)n + c_1 \\
    }
    \uncover<4->{
    \T{rev}(n) &= \frac{c_3}{2}n^2 + (c_2 - \frac{c_3}{2})n + c_1 \\
    }
    \uncover<5->{
    \T{rev}       &\in \bigO{n^2}
    }
  \end{align*}
\end{frame}

\fi %%% END IFFALSE

\begin{frame}[fragile]{Can we do better?}
\begin{Alms}
let rev xs0 =
  let rec revAppend xs ys =
    match xs with
    | [] -> ys
    | x :: xs' -> revAppend xs' (x :: ys) in
  revAppend xs0 []
\end{Alms}
  \pause
  \begin{align*}
    \T{rev}(n) &= c_1 + \T{revAppend}(n, 0) \\[4pt]
    \T{revAppend}(0, m) &= c_2 \\
    \T{revAppend}(n + 1, m) &= c_3 + \T{revAppend}(n, m + 1) \\[8pt]
    \uncover<3>{
    \T{rev} &\in \bigO{n}
    }
  \end{align*}
\end{frame}

\begin{frame}[fragile]{Example: \textalms{insert}}
\begin{Alms}
let rec insert xs y =
  match xs with
  | [] -> [y]
  | x :: xs' -> if y <= x then y :: xs
                else x :: insert xs' y
\end{Alms}
  \pause
  \begin{align*}
  \T{insert}(0) &= c_1 \\
  \T{insert}(n + 1) &= c_2 + \T{insert}(n) \\[8pt]
  \uncover<3>{
    \T{insert} &\in \bigO{n}
  }
  \end{align*}
\end{frame}

\begin{frame}[fragile]{Example: \textalms{fold_left}}
\begin{Alms}
let rec fold_left (xs : 'a list) ~(init : 'b)
                  ~(f : 'b -> 'a -> 'b) : 'b =
  match xs with
  | [] -> init
  | x :: xs' -> fold_left xs' ~init:(f init x) ~f
\end{Alms}
  \pause
  (We assume here that \textalms{init} and \textalms{xs} have the same
  size, and that the running time of \textalms{f} depends only on its
  first argument. These assumptions don't hold in general, but they do
  hold for our example.)
  \begin{align*}
  \T{fold\_left}(0, \T{f}) &= c_1 \\
  \T{fold\_left}(n+1, \T{f}) &= \T{f}(n) + \T{fold\_left}(n, \T{f}) \\[8pt]
  \uncover<3>{
    \T{fold\_left} &\in \bigO{n\T{f}(n)}
  }
  \end{align*}
\end{frame}

\begin{frame}[fragile]{Example: Insertion sort}
\begin{Alms}
let rec insertion_sort xs =
  fold_left xs ~init:[] ~f:insert
\end{Alms}
  \pause
  \begin{align*}
  \T{insertion\_sort}(n) &= \T{fold\_left}(n, \T{insert}) \\
  \uncover<3->{\T{insertion\_sort} &\in \bigO{n \T{insert}(n)} \\}
  \uncover<4->{\T{insertion\_sort} &\in \bigO{n^2}}
  \end{align*}
\end{frame}

\begin{frame}[fragile]{Example: \textalms{merge}}
\begin{Alms}
let rec merge xs ys =
  match xs, ys with
  | [], _ -> ys
  | _, [] -> xs
  | x :: xs', y :: ys' ->
      if x <= y then x :: merge xs' ys
      else y :: merge xs ys'
\end{Alms}
  \pause
  \begin{align*}
  \T{merge}(0, m) &= c_1 \\
  \T{merge}(n, 0) &= c_2 \\
  \T{merge}(n + 1, m + 1) &= c_3 +
        \mathrm{max}(\T{merge}(n, m + 1), \T{merge}(n + 1, m)) \\[8pt]
  \uncover<3->{
    \T{merge} &\in \bigO{n + m} \\
  }
  \end{align*}
\end{frame}

\begin{frame}[fragile]{Example: \textalms{split}}
\begin{Alms}
let rec split xs ys zs =
  match xs with
  | [] -> (ys, zs)
  | x :: xs' -> split xs' zs (x :: ys)
\end{Alms}
  \pause
  \begin{align*}
    \T{split} &\in \bigO{n}
  \end{align*}
\end{frame}

\begin{frame}[fragile]{Example: \textalms{mergesort}}
\begin{Alms}
let rec mergesort xs =
  match xs with
  | [] | [_] -> xs
  | _ -> let (xs1, xs2) = split xs [] [] in
         merge (mergesort xs1) (mergesort xs2)
\end{Alms}
  \begin{align*}
  \uncover<2->{\T{mergesort}(0) &= c_1} \qquad
  \uncover<2->{\T{mergesort}(1)  = c_2} \\
  \uncover<3->{\T{mergesort}(n) &= c_3 + \T{split}(n) + 2\T{mergesort}(n/2) + \T{merge}(n/2, n/2)} \\
  \uncover<4->{\T{mergesort}(n) &= c_3 + c_4 n + 2\T{mergesort}(n/2)} \\
  \uncover<5->{\T{mergesort}(n) &= c_3 + c_4 n + 2(c_3 + c_4 n/2 + 2\T{mergesort}(n/4))}
  \end{align*}
  \begin{align*}
  \uncover<6->{\T{mergesort} \in \bigO{n \log n}}
  \uncover<7->{&\quad\text{vs.}\quad\T{insertion\_sort} \in \bigO{n^2}} \\
  \uncover<8>{\T{mergesort} &\quad\ll\quad \T{insertion\_sort} }
  \end{align*}
\end{frame}

\begin{frame}[fragile]{Lookup}
\begin{Alms}
let rec list_mem y xs =
  match xs with
  | [] -> false
  | x :: xs' -> x = y || list_mem y xs'

let rec tree_mem y t =
  match t with
  | Leaf -> false
  | Branch(tl, x, tr) -> x = y ||
                         if x > y then tree_mem y tl
                         else tree_mem y tr
\end{Alms}
\pause
\medskip
This is why we balance trees.
\end{frame}

\begin{frame}{Common recurrence patterns}
  \begin{align*}
    T(n) &= c + T(n - 1) & T &\in \bigO{n} \\
    T(n) &= cn + T(n - 1) & T &\in \bigO{n^2} \\
    T(n) &= c + T(n/2) & T &\in \bigO{\log n} \\
    T(n) &= cn + 2T(n/2) & T &\in \bigO{n \log n} \\
  \end{align*}
\end{frame}

\end{document}
