(* Lecture 6: 02/13/2014 *)

(*
  Substitution example:

  let x = 5 in
  (let y = 12 in x + y) * (let x = x + 1 in x - 3)

  -->

  (let y = 12 in 5 + y) * (let x = 5 + 1 in x - 3)

  -->

  (5 + 12) * (let x = 5 + 1 in x - 3)

  -->

  17 * (let x = 5 + 1 in x - 3)

  -->

  17 * (let x = 6 in x - 3)

  -->

  17 * (6 - 3)

  -->

  17 * 3

  -->

  51

 *)

open Core.Std

(** Adding conditionals and functions **)

type op    = Plus | Minus | Times
type var   = string

type expr  = ValE of value
           | OpE of expr * op * expr
           | LetE of var * expr * expr
           | VarE of var
           | If0E of expr * expr * expr
           | AppE of expr * expr

 and value = IntV of int
           | FunV of var * expr

let intE (z : int) : expr = ValE (IntV z)

(* 3 *)
let three = intE 3

(* 3 + 1 *)
let four = OpE(intE 3, Plus, intE 1)

(* let two = 2 in two + (two + 1) *)
let five = LetE("two", intE 2,
                OpE(VarE "two", Plus,
                    OpE(VarE "two", Plus, intE 1)))

(* let square = fun x -> x * x in square (square 2) *)
let sixteen = LetE("square", ValE(FunV("x", OpE(VarE "x", Times, VarE "x"))),
                   AppE(VarE "square",
                        AppE(VarE "square", ValE (IntV 2))))
(*
  let add = (fun x -> (fun y -> x + y)) in
  let inc = add 1 in
  let dec = add (-1) in
  dec (inc 51)
 *)
let fiftyone =
  LetE("add", ValE(FunV("x", ValE(FunV("y", OpE(VarE "x", Plus, VarE "y"))))),
       LetE("inc", AppE(VarE "add", ValE(IntV 1)),
            LetE("dec", AppE(VarE "add", ValE(IntV(-1))),
                 AppE(VarE "dec", AppE(VarE "inc", ValE(IntV 51))))))

exception UnboundVariable of var
exception TypeError

(* Evaluate an arithemetic operation on two values *)
let rec eval_op (v1 : value) (op : op) (v2 : value) : value =
  match op, v1, v2 with
  | Plus, IntV z1, IntV z2 -> IntV (z1 + z2)
  | Minus, IntV z1, IntV z2 -> IntV (z1 - z2)
  | Times, IntV z1, IntV z2 -> IntV (z1 * z2)
  | _ -> raise TypeError

(* Substitute a value for a variable in an expression *)
let substitute (v : value) (x : var) (e : expr) : expr =
  let rec subst e =
    match e with
    | ValE v' -> ValE (subst_val v')
    | OpE(e1, op, e2) -> OpE(subst e1, op, subst e2)
    | LetE(y, e1, e2) ->
       let e1' = subst e1 in
       let e2' = if x = y then e2 else subst e2 in
       LetE(y, e1', e2')
    | VarE y -> if x = y then ValE v else e
    | If0E(e1, e2, e3) -> If0E(subst e1, subst e2, subst e3)
    | AppE(e1, e2) -> AppE(subst e1, subst e2)
  and subst_val v =
    match v with
    | IntV _ -> v
    | FunV(y, e1) ->
       let e1' = if x = y then e1 else subst e1 in
       FunV(y, e1')
  in subst e

(* Evaluate an expression *)
let rec eval : expr -> value = function
  | ValE v -> v
  | OpE(e1, op, e2) -> eval_op (eval e1) op (eval e2)
  | LetE(x, e1, e2) -> eval (substitute (eval e1) x e2)
  | VarE x -> raise (UnboundVariable x)
  | If0E(e1, e2, e3) ->
     (match eval e1 with
      | IntV n -> if n = 0 then eval e2 else eval e3
      | _ -> raise TypeError)
  | AppE(e1, e2) ->
     (match eval e1 with
      | FunV(x, e3) -> eval (substitute (eval e2) x e3)
      | _ -> raise TypeError)

assert( eval three = IntV 3 )
assert( eval four = IntV 4 )
assert( eval five = IntV 5 )
assert( eval sixteen = IntV 16 )
assert( eval fiftyone = IntV 51 )

assert( eval (LetE("x", intE 3,
                   LetE("x", intE 4,
                        VarE "x")))
             = IntV 4 )

assert( eval (LetE("x", intE 3,
                   LetE("y", intE 4,
                        VarE "x")))
             = IntV 3 )

(** CHALLENGE: Write the factorial function in the above language. **)

(* Factorial function in our mini-OCaml *)
let factorial =
  AppE(ValE(FunV("f",
                 LetE("helper",
                      ValE(FunV("x",
                                AppE(VarE "f",
                                     ValE(FunV("v", AppE(AppE(VarE "x", VarE "x"), VarE "v")))))),
                      AppE(VarE "helper", VarE "helper")))),
       ValE(FunV("fact",
                 ValE(FunV("n",
                           If0E(VarE "n", intE 1,
                                OpE(VarE "n", Times, AppE(VarE "fact",
                                                          OpE(VarE "n", Minus, intE 1)))))))))

(* Factorial function in OCaml (think of it as the spec) *)
let rec fact' (n : int) : int =
  if n = 0 then 1
  else n * fact' (n - 1)

(* Convenient way to use the interpreted factorial function *)
let fact (n : int) : value =
  eval (AppE(factorial, intE n))

assert( fact 4 = IntV (fact' 4) )
assert( fact 5 = IntV (fact' 5) )
assert( fact 10 = IntV (fact' 10) )

(* We can rewrite function calls into lets (or lets into function calls),
 * according to the rule:
 *
 *   let x = e1 in e2   ===   (fun x -> e2) e1
 *)

let rec eval : expr -> value = function
  | ValE v -> v
  | OpE(e1, op, e2) -> eval_op (eval e1) op (eval e2)
  | LetE(x, e1, e2) -> eval (substitute (eval e1) x e2)
  | VarE x -> raise (UnboundVariable x)
  | If0E(e1, e2, e3) ->
     (match eval e1 with
      | IntV z -> if z = 0 then eval e2 else eval e3
      | _ -> raise TypeError)
  | AppE(e1, e2) ->
     (match eval e1 with
      | FunV(x, e3) -> eval (LetE(x, e2, e3))
      | _ -> raise TypeError)

(** Other folds *)

type 'a tree = Leaf
             | Branch of 'a tree * 'a * 'a tree

let tree0 = Leaf
let tree1 = Branch(Leaf, 1, Leaf)
let tree2 = Branch(tree1, 3, Branch(Leaf, 5, Branch(Leaf, 7, Leaf)))

(*
let rec process_tree (t : 'a tree) : ... =
  match t with
  | Leaf -> ...
  | Branch(tl, x, tr) = ... process_tree tl ... process_tree tr ...
*)

(* Sum the ints in a tree *)
let rec sum_tree (t : int tree) : int =
  match t with
  | Leaf -> 0
  | Branch(tl, x, tr) -> x + sum_tree tl + sum_tree tr

assert( sum_tree tree0 = 0 )
assert( sum_tree tree1 = 1 )
assert( sum_tree tree2 = 16 )

let rec max_tree (t : int tree) : int option =
  let (\/) zo1 zo2 =
    match zo1, zo2 with
    | Some z1, Some z2 -> Some (max z1 z2)
    | None, _ -> zo2
    | _, None -> zo1 in
  match t with
  | Leaf -> None
  | Branch(tl, x, tr) -> Some x \/ max_tree tl \/ max_tree tr

assert( max_tree tree0 = None )
assert( max_tree tree1 = Some 1 )
assert( max_tree tree2 = Some 7 )

(* Collect the values in a tree as a list (in order) *)
let rec in_order_tree (t : 'a tree) : 'a list =
  match t with
  | Leaf -> []
  | Branch(tl, x, tr) -> in_order_tree tl @ x :: in_order_tree tr

assert( in_order_tree tree0 = [] )
assert( in_order_tree tree1 = [1] )
assert( in_order_tree tree2 = [1; 3; 5; 7] )

(* Collect the values in a tree as a list (pre order) *)
let rec pre_order_tree (t : 'a tree) : 'a list =
  match t with
  | Leaf -> []
  | Branch(tl, x, tr) -> x :: in_order_tree tl @ in_order_tree tr

assert( pre_order_tree tree0 = [] )
assert( pre_order_tree tree1 = [1] )
assert( pre_order_tree tree2 = [3; 1; 5; 7] )

(* Reduce a tree, given a value for leaves and a function to combine at branches. *)
let fold_tree (t : 'a tree) ~(init : 'b) ~(f : 'b -> 'a -> 'b -> 'b) : 'b =
  let rec loop t =
    match t with
    | Leaf -> init
    | Branch(tl, x, tr) -> f (loop tl) x (loop tr) in
  loop t

let sum_tree = fold_tree ~init:0 ~f:(fun al x ar -> al + x + ar)
assert( sum_tree tree0 = 0 )
assert( sum_tree tree1 = 1 )
assert( sum_tree tree2 = 16 )

let max_tree =
  let (\/) zo1 zo2 =
    match zo1, zo2 with
    | Some z1, Some z2 -> Some (max z1 z2)
    | None, _ -> zo2
    | _, None -> zo1 in
  fold_tree ~init:None ~f:(fun al x ar -> al \/ Some x \/ ar)
assert( max_tree tree0 = None )
assert( max_tree tree1 = Some 1 )
assert( max_tree tree2 = Some 7 )

let in_order_tree = fold_tree ~init:[] ~f:(fun al x ar -> al @ x :: ar)
assert( in_order_tree tree0 = [] )
assert( in_order_tree tree1 = [1] )
assert( in_order_tree tree2 = [1; 3; 5; 7] )

let pre_order_tree = fold_tree ~init:[] ~f:(fun al x ar -> x :: al @ ar)
assert( pre_order_tree tree0 = [] )
assert( pre_order_tree tree1 = [1] )
assert( pre_order_tree tree2 = [3; 1; 5; 7] )

let size_tree = fold_tree ~init:0 ~f:(fun al _ ar -> al + 1 + ar)

let depth_tree = fold_tree ~init:0 ~f:(fun al _ ar -> 1 + max al ar)

(* A more efficient version of in_order tree--O(n) instead of O(n^2) *)
let in_order_tree (t : 'a tree) =
  fold_tree t
            ~init:(fun lst -> lst)
            ~f:(fun al x ar -> fun lst -> al (x :: ar lst))
            []
assert( in_order_tree tree0 = [] )
assert( in_order_tree tree1 = [1] )
assert( in_order_tree tree2 = [1; 3; 5; 7] )
