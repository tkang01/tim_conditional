(* Lecture 8 (02/25): more abstract data types *)

open Core.Std

(*
 * Module types = signatures = interfaces
 *
 * Modules = structs = implementations
 *
 * Functors = functions from structs to structs
 *)

(* Queues (FIFOs):

   front { 2, 3, 4 } = 2
   dequeue { 2, 3, 4 } = { 3, 4 }

   enqueue 8 { 2, 3 } = { 2, 3, 8 }

   enqueue 8 { } = { 8 }

   front { } = ERROR
   dequeue { } = ERROR

   is_empty { } = true
   is_empty { 2, 3 } = false

 *)

module type QUEUE =
  sig
    type 'a t    (* the type of queues *)

    (* thrown on dequeue or front of empty *)
    exception Empty

    val empty : 'a t
    val enqueue : 'a -> 'a t -> 'a t
    val is_empty : 'a t -> bool
    val front : 'a t -> 'a
    val dequeue : 'a t -> 'a t
  end

module ListQueue : QUEUE =
  struct
    (* represent queue { 1, 2, 3 } as [1; 2; 3] *)
    type 'a t = 'a list

    exception Empty

    let empty = []
    let enqueue x q = q @ [x]
    let is_empty = List.is_empty
    let front = function
      | x :: _ -> x
      | _ -> raise Empty
    let dequeue = function
      | _ :: q' -> q'
      | _ -> raise Empty
  end

(* Suppose you want to enqueue N items is O(N^2) *)

module BankersQueue : QUEUE =
  struct
    (* { front = [ 2; 3 ]; back = [ 5; 4 ] }
        represents
       { 2, 3, 4, 5 } *)
    (* { front = xs; back = ys }
       represents
       xs @ reverse ys
    *)
    type 'a t = { front : 'a list; back : 'a list }

    exception Empty

    let empty = { front = []; back = [] }

    let is_empty = function
      | { front = []; back = [] } -> true
      | _ -> false

    let enqueue x q = { q with back = x :: q.back }

    let deq (q : 'a t) : 'a * 'a t =
      match q.front with
      | x :: xs -> (x, { q with front = xs })
      | _ -> match List.rev q.back with
             | x :: xs -> (x, { front = xs; back = [] })
             | _ -> raise Empty

    let front q = fst (deq q)
    let dequeue q = snd (deq q)
  end

module type CONTAINER =
  sig
    type 'a t
    exception Empty
    val empty : 'a t
    val insert : 'a -> 'a t -> 'a t
    val is_empty : 'a t -> bool
    val first : 'a t -> 'a
    val remove_first : 'a t -> 'a t
  end

(* MATRICES *)

module type RING =
  sig
    type t
    val zero : t
    val one : t
    val add : t -> t -> t
    val mul : t -> t -> t
    (* ... *)
  end

(* I got the next thing wrong in lecture. I wrote:
 *
 *   module IntRing : RING = struct ... end
 *
 * Then Matrix(IntRing) didn't work correctly because IntRing.t was
 * abstract. One fix suggested by a student is specify the concrete
 * representation of IntRing.t by including it in RING when sealing:
 *
 *   module IntRing : (RING with type t = int) = struct ... end
 *
 * This works, but it's unnecessary, because RING with type t = int is
 * already the type that OCaml infers for an unsealed IntRing, like so:
 *)
module IntRing =
  struct
    type t = int
    let zero = 0
    let one = 1
    let add = (+)
    let mul = ( * )
  end

module FloatRing =
  struct
    type t = float
    let zero = 0.
    let one = 1.
    let add = (+.)
    let mul = ( *. )
  end

module BoolRing =
  struct
    type t = bool
    let zero = false
    let one = true
    let add = (||)
    let mul = (&&)
  end

module type MATRIX =
  sig
    type elt
    type t
    val of_list : elt list list -> t
    val add : t -> t -> t
    val mul : t -> t -> t
    (* ... *)
  end

module DenseMatrix (R : RING) : MATRIX with type elt = R.t =
  struct
    type elt = R.t
    type t = elt list list
    let of_list rows = rows
    let add _ _ = failwith "unimplemented"
    let mul _ _ = failwith "fun exercise for later"
  end

module IntMatrix = DenseMatrix(IntRing)
module BoolMatrix = DenseMatrix(BoolRing)
