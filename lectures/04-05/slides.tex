\documentclass[aspectratio=53]{beamer}

\RequirePackage{ucs}
\usepackage[quiet]{fontspec}
\usepackage{xunicode}
\usepackage{xltxtra}
\usepackage{graphicx}
\usepackage{forloop}
\usepackage{stmaryrd}
\usepackage{tikz}
\usepackage{mathtools}
\usepackage{fancyvrb}
\usepackage{listings}
\usepackage{amsmath,amssymb,amsthm}
\usepackage{pdfpages}

\usetikzlibrary{chains,decorations.pathmorphing,positioning,fit}
\usetikzlibrary{decorations.shapes,calc,backgrounds}
\usetikzlibrary{decorations.text,matrix}
\usetikzlibrary{arrows,shapes.geometric,shapes.symbols,scopes}

\newcommand\bigO[1]{\ensuremath{\mathcal{O}(#1)}}
\newcommand\fun[1]{\ensuremath{\lambda #1.\,}}
\renewcommand\T[1]{\ensuremath{T_{\mathit{#1}}}}

%% %%% SETTINGS

\frenchspacing
\unitlength=0.01\textwidth
\thicklines
\urlstyle{sf}
\graphicspath{{images/}}

%%% VERBATIM SETUP

\DefineVerbatimEnvironment
  {code}{Verbatim}
  {commandchars=\\\‹\›,
   commentchar=\%,
   codes={\catcode`$=3\relax},
   baselinestretch=.8,
   formatcom=\relax}
\renewcommand\FancyVerbFormatLine[1]{#1\strut}
\fvset{commandchars=\\\‹\›,
       codes={\catcode`$=3\relax},
       formatcom=\relax}
\newcommand\exponent[1]{^{#1}}
\DefineShortVerb{\^}

\newcommand\ALT[3]{\alt<#1>{#2}{#3}}

%%% COLOR AND BEAMER SETUP

\setbeamercolor{frametitle}{fg=black}
\setbeamercolor{normal text}{fg=black}

\defaultfontfeatures{
    Mapping=tex-text,
    Scale=MatchLowercase,
}
\setmainfont{TeX Gyre Schola}
\setmonofont{Monaco}

\usefonttheme{serif}

\setbeamertemplate{frametitle}
  {\begin{centering}\medskip
   \insertframetitle\par
   \end{centering}}

\setbeamertemplate{itemize item}{$\bullet$}

\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[text line]{%
    \hfill\strut{%
        \small\color{black!75}%
        \texttt{\insertframenumber:\insertframeslidenumber}%
    }%
    \hfill%
}

\makeatletter
\newcount\frameslidetemp
\newcommand\insertframeslidenumber{%
  \frameslidetemp\c@page\relax
  \advance\frameslidetemp-\beamer@startpageofframe\relax
  \advance\frameslidetemp1\relax
  \the\frameslidetemp
}
\makeatother

\newcommand\EmacsFrame{
  \begin{frame}{\relax}
    \thispagestyle{empty}
    \begin{center}
      \emph{--- To Emacs ---}
    \end{center}
  \end{frame}
}

%%% GENERALLY USEFUL MACROS

\newcommand<>\always[1]{#1}

\def\defgbox#1#2{%
  \expandafter\global\expandafter\newbox\csname#2box\endcsname%
  \expandafter\global\expandafter\def\csname#2\endcsname{%
    \expandafter\usebox\csname#2box\endcsname%
  }%
  \expandafter\global\expandafter\setbox\csname#2box\endcsname=#1%
}
\def\defhbox{\defgbox\hbox}
\def\defvbox{\defgbox\vbox}

\def\setgbox#1#2{%
  \expandafter\setbox\csname#2box\endcsname=#1%
}
\def\sethbox{\setgbox\hbox}
\def\setvbox{\setgbox\vbox}

%%%
%%% TIKZ
%%%

\newcommand\K[1]{\textcolor{orange!70!black}{#1}}
\newcommand\TY[1]{\textcolor{blue!70!black}{#1}}
\newcommand\REF{\textcolor{green!50!black}{ref}}

\newcommand\place[3][]{%
  \tikz[orp] \node[#1] at (#2) {#3};%
}

\newcommand\anchor[1]{%
  \tikz[orp] \coordinate (#1);%
}

\newcommand\nil[1]{
  \draw[nil] (#1.north east) -- (#1.south west)
}

\newcommand\cons[4][]{
  \node[cons,#1] (#2) {#3\kern3pt\anchor{#2 center}\kern3pt#4};
  \draw[thick] (#2 center |- #2.north) -- (#2 center |- #2.south)
}

\newcommand\genpointer[3][draw=blue!50!black,opacity=.5]{%
  \begin{tikzpicture}[rem,solid,#1]
    \node(pointer start) [circle, draw, inner sep=1.37pt] {};
    \draw[->, overlay] (pointer start) edge[bend right=#2] (#3);
  \end{tikzpicture}%
}

\newcommand\hpointer[2][30]{%
  \genpointer[draw=red!70!black]{#1}{#2}%
}

\newcommand\ppointer[2][30]{%
  \genpointer{#1}{#2}%
}

\newcommand\pointer[2][30]{%
  \ifhighlit\let\pointertemp\hpointer\else\let\pointertemp\ppointer\fi
  \pointertemp[#1]{#2}%
}


\newcount\reducetemp
\newcommand\reduce[3]{%
  \reducetemp#1\relax
  \advance\reducetemp -1\relax
  \alt<#1->{#3}{\alt<\the\reducetemp>{\highlight{#2}}{#2}}%
}

\tikzset{
  orp/.style={
    overlay,
    remember picture,
  },
  rem/.style={
    remember picture,
  },
  every node/.style={
    very thick,
    inner sep=3pt,
  },
  every path/.style={very thick},
  ref/.style={
    draw=red,
    densely dotted,
    font=\ttfamily,
  },
  cons/.style={
    draw=black,
    font=\ttfamily,
  },
  nil/.style={
    black!80!white,
    shorten >=2pt,
    shorten <=2pt,
  },
  highlight/.style={
    color=yellow!50!white!90!black,
    thick,
    draw,
    draw opacity=0.5,
    outer color=yellow!50,
    inner color=yellow!30,
    rounded corners=2pt,
  },
}

\newif\ifhighlit
\highlitfalse

\newcommand<>\highlight[2][\relax]{%
  \ifx#1\relax
    \begin{tikzpicture}
  \else
    \begin{tikzpicture}[remember picture]
  \fi
    \node[
      inner sep=0pt,
      outer sep=0pt,
      text depth=0pt,
      align=left,
    ] (highlight temp) {%
        \only#3{\highlittrue}%
        #2%
        \only#3{\highlitfalse}%
    };
    \begin{scope}[on background layer]
      \coordinate (highlight temp 1) at
        ($(highlight temp.north east) + (2pt,2pt)$);
      \coordinate (highlight temp 2) at
        ($(highlight temp.base west) + (-2pt,-3pt)$);
      \node#3 [
        overlay,
        highlight,
        inner sep=0pt,
        fit=(highlight temp 1) (highlight temp 2),
      ] {};
    \end{scope}
    \ifx#1\relax
      \relax
    \else
      \node (#1) [
        overlay,
        inner sep=0pt,
        fit=(highlight temp 1) (highlight temp 2),
      ] {};
    \fi
  \end{tikzpicture}%
}

\newcommand\HI[2]{\highlight<#1>{#2}}

\newcommand\huncover[3][\relax]{%
  \uncover<#2->{\highlight<#2>[#1]{#3}}%
}
\newcommand\honly[3][\relax]{%
  \only<#2->{\highlight<#2>[#1]{#3}}%
}

\def\S#1#2{#2<#1>}

\begin{document}

%%%
%%% TITLE PAGE
%%%

\begin{frame}[t,fragile]\relax
  \thispagestyle{empty}
  \vfill
  \begin{center}
  \par {Supplementary Video:}\par\smallskip
  {\Huge More Events}

  \bigskip
  \Large
  CS 51 and CSCI E-51

  \medskip
  April 5, 2014
  \end{center}

  %% Need to initialize Tikz opacity inside a frame, not outside:
  \begin{tikzpicture}[opacity=0,overlay]\end{tikzpicture}
\end{frame}

\begin{frame}{Road map}
  \begin{itemize}
    \item The concept
    \item Using events
    \item Implementation of events
    \item UI events
  \end{itemize}
\end{frame}

{
\def\each#1{\onslide<#1>\includegraphics[width=10cm]{concept#1.png}}
\begin{frame}{The concept}
  \centering
  \begin{overprint}
  \each{1}
  \each{2}
  \each{3}
  \each{4}
  \each{5}
  \each{6}
  \each{7}
  \each{8}
  \each{9}
  \end{overprint}
\end{frame}
}

\newcommand\ONLY[2]{\only<#1>{#2}}

\begin{frame}[fragile]{Basic event primitives}
  \begin{code}
\K‹module› Event51 :
\K‹sig›
  \HI‹10-11›‹\ALT‹1-9›‹\color‹black!20!white›type id›‹\K‹type› id››
  \HI‹2›‹\K‹type› 'a event›

  \HI‹3›‹\K‹val› new_event : unit -> 'a event›
  \HI‹4›‹\K‹val› add_listener : \HI‹5›‹'a event› -> \HI‹5,8›‹('a -> unit)› -> \HI‹10-11›‹id››
  \HI‹6›‹\K‹val› fire_event : \HI‹7›‹'a event› -> \HI‹7-8›‹'a› -> unit›

  \HI‹9›‹\ALT‹1-8›‹\color‹black!20!white›val›‹\K‹val›› add_one_shot_listener : 'a event -> ('a -> unit) -> id›
  \HI‹11›‹\ALT‹1-10›‹\color‹black!20!white›val›‹\K‹val›› remove_listener : 'a event -> id -> unit›
  $\cdots$
\K‹end›
  \end{code}
\end{frame}

\EmacsFrame

\begin{frame}[fragile]{Event implementation}
\begin{code}
\HI‹2›‹\K‹type› 'a waiter = { id : id; action : 'a -> unit }›
\HI‹3›‹\K‹type› 'a event = 'a waiter list ref›

\HI‹4›‹\K‹let› new_event () : 'a event = ref []›

\K‹let› add_listener (\HI‹5›‹e : 'a event›) (\HI‹5›‹f : 'a -> unit›) : id =
  \HI‹6›‹\K‹let› i = new_id ()› \K‹in›
  \HI‹9›‹e := \HI‹8›‹\HI‹7›‹{ id = i; action = f }› :: !e››;
  \HI‹10›‹i›

\K‹let› fire_event (\HI‹11›‹e : 'a event›) (\HI‹11›‹v : 'a›) : unit =
  \HI‹12›‹List.iter !e ~f:(\HI‹13›‹\K‹fun› w -> w.action v›)›
\end{code}
\end{frame}

\begin{frame}[fragile]{UI events}
  \begin{code}
\K‹val› key_pressed  : char event\pause
\HI‹3›‹\K‹val› mouse_motion : (int * int) event›
\HI‹4›‹\K‹val› button_down  : (int * int) event›
\HI‹4›‹\K‹val› button_up    : (int * int) event›
\HI‹5›‹\K‹val› clock        : unit event›
  \end{code}
\end{frame}

\begin{frame}[fragile]{UI event implementation}
\newcommand\pseudo[1]{\textsf{\small\textcolor{violet}{#1}}}
\newcommand\V[1]{\emph{\textcolor{green!50!black}{#1}}}
\begin{code}
\K‹let› read_event () =
  \K‹if› \pseudo‹\V‹mouse position› has changed› \K‹then›
    fire_event mouse_motion \pseudo‹\V‹mouse position››;\pause
  \K‹if› \pseudo‹mouse button was up and is now down› \K‹then›
    fire_event button_down \pseudo‹\V‹mouse position››;\pause
  \K‹else› \K‹if› \pseudo‹mouse button was down and is now up› \K‹then›
    fire_event button_up \pseudo‹\V‹mouse position››;\pause
  \K‹if› \pseudo‹a key has been pressed› \K‹then›
    fire_event key_handler \pseudo‹\V‹the key››;\pause
  fire_event clock ()
\pause
\K‹let› \K‹rec› event_loop () =
  read_event ();\pause
  \pseudo‹redraw screen›;
  \pseudo‹wait a bit›;\pause
  event_loop ()
\end{code}
\end{frame}

\end{document}
