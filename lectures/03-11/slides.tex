\documentclass{beamer}

\usepackage[quiet]{fontspec}
\usepackage{xunicode}
\usepackage{xltxtra}
\usepackage{graphicx}
\usepackage{forloop}
\usepackage{stmaryrd}
\usepackage{tikz}
\usepackage{almslides}
\usepackage{mathtools}

\usetikzlibrary{chains,decorations.pathmorphing,positioning,fit}
\usetikzlibrary{decorations.shapes,calc,backgrounds}
\usetikzlibrary{decorations.text,matrix}
\usetikzlibrary{arrows,shapes.geometric,shapes.symbols,scopes}
\usetikzlibrary{mindmap}
\usetikzlibrary{fadings}

\newcommand\bigO[1]{\ensuremath{\mathcal{O}(#1)}}
\newcommand\fun[1]{\ensuremath{\lambda #1.\,}}
\renewcommand\T[1]{\ensuremath{T_{\mathit{#1}}}}

%% %%% SETTINGS

\frenchspacing
\unitlength=0.01\textwidth
\thicklines
\urlstyle{sf}
\graphicspath{{images/}}

%% %%% VERBATIM SETUP
%% 
%% \DefineVerbatimEnvironment
%%   {code}{Verbatim}
%%   {commandchars=\\\{\},
%%    codes={\catcode`$=3\relax},
%%    formatcom=\small}
%% %\DefineShortVerb{v}
%% 
%% %%% COLOR AND BEAMER SETUP
%% 

\def\fern{407428}
\def\charcoal{4D4944}
\definecolor{DarkCharcoal}{HTML}{\charcoal}
\colorlet{Charcoal}{DarkCharcoal!85!white}
\colorlet{AlertColor}{orange!70!black}
\colorlet{DarkRed}{red!70!black}
\colorlet{DarkBlue}{blue!70!black}
\colorlet{DarkGreen}{green!50!black}

\colorlet{Client}{blue!60!black}
\colorlet{Server}{red!60!black}
\colorlet{Both}{violet!80!black}

\setbeamercolor{frametitle}{fg=black}
\setbeamercolor{normal text}{fg=black}

\defaultfontfeatures{
    Mapping=tex-text,
    Scale=MatchLowercase,
}
\setmainfont{TeX Gyre Schola}

\usefonttheme{serif}

\setbeamertemplate{frametitle}
  {\begin{centering}\medskip
   \insertframetitle\par
   \end{centering}}

\setbeamercolor{alerted text}{fg=AlertColor}
\setbeamertemplate{itemize item}{$\bullet$}

\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[text line]{%
    \hfill\strut{%
        \small\color{black!75}%
        \texttt{\insertframenumber}%
    }%
    \hfill%
}

%%% GENERALLY USEFUL MACROS

\def\LAM{\text{\textK{$\bm\lambda$}\,}}

\def\verysmall{%
    \fontsize{7.5pt}{8pt}\selectfont%
}

\newcommand\setPause[2][0]{%
  \setcounter{beamerpauses}{#2}%
  \addtocounter{beamerpauses}{#1}%
}

\newcommand<>\always[1]{#1}

\newcommand\aalt[4]{%
  \alt<#1#2>{\color<#1>{AlertColor}#3\color{Charcoal}\relax}{#4}%
}

\newbox\mytikzbox
\def\settikz{\setbox\mytikzbox=\hbox}
\def\usetikz{\usebox\mytikzbox}

\def\defgbox#1#2{%
  \expandafter\global\expandafter\newbox\csname#2box\endcsname%
  \expandafter\global\expandafter\def\csname#2\endcsname{%
    \expandafter\usebox\csname#2box\endcsname%
  }%
  \expandafter\global\expandafter\setbox\csname#2box\endcsname=#1%
}
\def\defhbox{\defgbox\hbox}
\def\defvbox{\defgbox\vbox}

\def\setgbox#1#2{%
  \expandafter\setbox\csname#2box\endcsname=#1%
}
\def\sethbox{\setgbox\hbox}
\def\setvbox{\setgbox\vbox}

\newcommand<>\EXP{\textcolor{Both}#1{\Exp}}

\newcommand\tikzinfer[3][\relax]{
  \begin{tikzpicture}
    \node(premiss) {#2};
    \node(conclusion)[at=(premiss.south),anchor=north] {#3};
    \node(infer rule)[fit=(premiss) (conclusion),inner sep=0] {};
    \draw[math line]
      (premiss.south -| infer rule.west) -- (premiss.south -| infer rule.east)
      ;
    \if#1\relax \else
      \node(infer label)[at=(infer rule.east),anchor=mid west] {(#1)};
    \fi
  \end{tikzpicture}
}

\def\mdef#1#2{\def#1{\ensuremath{#2}}}
\def\mnew@command#1[#2]#3{\newcommand#1[#2]{\ensuremath{#3}}}
\def\mnewcommand#1#2{%
  \ifthenelse{\equal{#2}{[}}
    {\mnew@command{#1}[}
    {\mdef{#1}{#2}}
}
\let\mnew\mnewcommand

\mdef\EnvD{
  \mathord{
    \tikz \draw[math line] (0,0) -- (.66em,0em) -- (.33em,1.3ex) -- cycle;
  }
}
\mdef\EnvG{
  \mathord{
    \tikz \draw[math line] (0,0) -- (0,1.4ex) -- +(.5em,0);
  }
}
\mdef\Proves{
  \mathrel{
    \tikz \draw[math tline] (0,0) -- (0,1.3ex) (0,.65ex) -- +(1ex,0);
  }
}
\mdef\Exp{
  \mathord{!\kern.5pt}
}
\mdef\pxE{
  \mathord{\text{\textexclamdown}\kern.5pt}
}

%%%
%%% TIKZ
%%%

\tikzfading[
  name=fade outer,
  inner color=transparent!0,
  outer color=transparent!25,
]

\tikzset{
  every node/.style={very thick},
  every path/.style={very thick},
  invisible state/.style={
    rectangle,
    minimum size=5mm,
    rounded corners=1mm,
    text height=1.75ex,
    text depth=0.5ex,
  },
  state coloring/.style={
    draw=#1,
    top color=white,
    bottom color=#1!20,
  },
  state/.style={
    invisible state,
    state coloring=#1,
  },
  state/.default={DarkGreen},
  info box/.style={
    draw=#1,
    inner color=white,
    outer color=#1!20,
    rounded corners=1pt,
    align=left,
    font=\ttfamily\color{#1!50!black},
  },
  info box/.default={DarkRed},
  trans/.style={
    ->,
    shorten >=0.5pt,
    rounded corners,
  },
  >=stealth,
  start here/.style={
    draw=black,
    append after command={\tikzlastnode -- +(-.1,-.1)},
  },
  math line/.style={line width=0.2ex},
  math tline/.style={line width=0.14ex},
  rstate/.style={
    circle,
    font=\scriptsize,
    inner sep=1.5pt,
    state coloring=#1,
  },
  rstate/.default={DarkGreen},
  final rstate/.style={
    rstate=#1,
    double,
    inner sep=2pt,
  },
  final rstate color/.default={DarkGreen},
  my concept/.style={
    circle,
    draw=#1!30,
    outer color=#1!30,
    inner color=#1!15,
    align=center,
    inner sep=0pt,
  },
  fade out/.style={
    orp,
    fit=(current page.south west) (current page.north east),
    inner color=white,
    outer color=#1,
    path fading=fade outer,
  },
  fade out/.default=DarkBlue!30,
}


%%% TITLE INFO

\title{CS51 Lecture}
\author{Jesse A. Tov}
\institute{CS51}

\begin{document}

%%%
%%% REUSABLE PICTURES
%%%

\newcommand\testpattern{
  \begin{frame}[plain]{}
    \begin{tikzpicture}[
      orp,
      ultra thick
    ]
      \coordinate (ll) at ($ (current page.south west) + (.3pt,.3pt) $);
      \coordinate (ur) at ($ (current page.north east) - (.8pt,.8pt) $);
      \draw[red!80!black] (ll) rectangle (ur);
      \draw[green!80!black] ($ (ll) + (.5,.5) $) rectangle ($ (ur) - (.5,.5) $);
      \draw[blue] ($ (ll) + (1,1) $) rectangle ($ (ur) - (1,1) $);
      \path[minimum size=2cm,circle,opacity=.5]
        (current page.center)
        ++(-3.7,2.3) node (cyan) [fill=cyan] {}
        +(1,0) node (magenta) [fill=magenta] {}
        (intersection of cyan and magenta) node[fill=yellow] {}
      ;
      \path[minimum size=1.7cm,circle]
        (current page.center)
        ++(0,2.3) node[fill=Charcoal] {}
        ++(2,0) node[fill=DarkGreen] {}
        ++(2,0) node[fill=AlertColor] {}
        ;
      \path[minimum size=1.2cm,circle]
        (current page.center)
        ++(-.25,.25) node[fill=black] {}
        ++(1.5,0) node[fill=Client] {}
        ++(1.5,0) node[fill=Server] {}
        ++(1.5,0) node[fill=Both] {}
        ;
      \draw[line width=1ex,color=Charcoal]
        (current page.center) ++(-.85,-1) -- +(5.7,0) ;
      \draw[line width=.5ex,color=Charcoal]
        (current page.center) ++(-.85,-1.3) -- +(5.7,0) ;
      \draw[ultra thick,color=Charcoal]
        (current page.center) ++(-.85,-1.6) -- +(5.7,0) ;
      \draw[very thick,color=Charcoal]
        (current page.center) ++(-.85,-1.9) -- +(5.7,0) ;
      \draw[thick,color=Charcoal]
        (current page.center) ++(-.85,-2.2) -- +(5.7,0) ;
      \draw[semithick,color=Charcoal]
        (current page.center) ++(-.85,-2.5) -- +(5.7,0) ;
      \draw[thin,color=Charcoal]
        (current page.center) ++(-.85,-2.8) -- +(5.7,0) ;
      \draw[very thin,color=Charcoal]
        (current page.center) ++(-.85,-3.1) -- +(5.7,0) ;
      \draw[ultra thin,color=Charcoal]
        (current page.center) ++(-.85,-3.4) -- +(5.7,0) ;
      \path
        (ll) ++ (1.4,1.37)
        node[
          text width=3.5cm,
          anchor=south west,
          highlight,
          font=\scriptsize,
          text justified,
        ]
        {
          \color{black}
          {\normalsize Because propositions}
          {\small in such a logic}
          may no longer be freely copied or
          ignored,
          \color{AlertColor}
          this suggests understanding propositions in
          substructural logics as representing resources rather than
          truth.
        }
        ;
    \end{tikzpicture}
  \end{frame}
}

%%%
%%% TITLE PAGE
%%%

\begin{frame}\relax

  \begin{centering}
  {\Huge References and Mutation}

  \bigskip

  \Large
  CS 51 and CSCI E-51

  \medskip

  March 11, 2014

  \end{centering}

  %% Need to initialize Tikz opacity inside a frame, not outside:
  \begin{tikzpicture}[opacity=0,overlay]\end{tikzpicture}
\end{frame}

\begin{frame}{Purely Functional OCaml}
  \begin{itemize}
    \item<+-> We've considered purely functional subset
      \begin{itemize}
        \item (Exceptions: printing and exceptions)
      \end{itemize}
    \item<+-> Two reasons:
      \begin{itemize}
        \item Reasoning is easier
        \item To show you what's possible
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Alas!}
  \begin{itemize}
    \item<+-> Purely functional programs are ultimately pointless
      \begin{itemize}
        \item<+-> We run programs to have some effect on the world
        \item<+-> If a tree gets balanced in the woods\ldots
      \end{itemize}
    \item<+-> Some algorithms and data structures \emph{need} mutable state
      \begin{itemize}
        \item Example: Hash tables have constant-time access and update
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{But still:}
  \begin{itemize}
    \item<+-> Reasoning about state (and side effects) is hard
      \begin{itemize}
        \item Effects don't show up in interfaces
        \item<+-> If I insert $x$ into $S$ and then call a function $f$,
        will $\textit{member}\;S\;x$ then be true?
        \item<+-> Sharing state is perilous
      \end{itemize}
    \item<+-> Moral: use mutation only where necessary
      \begin{itemize}
        \item This is helpful in Java, C/C++, Python, \ldots
        \item But functional languages are designed for it
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{References}
  \begin{itemize}
    \item<+-> New type: \verb!t ref!
      \begin{itemize}
        \item A pointer to a \emph{box} holding a \verb!t!.
        \item Can be shared
        \item Can be read or written
      \end{itemize}
    \item<+-> To allocate: \verb!ref 8!
      \begin{itemize}
        \item Allocates a new box, initializes to \texttt{8}, and
          returns the pointer
      \end{itemize}
    \item<+-> To read: \verb|!r|
      \begin{itemize}
        \item Returns contents of box
        \item For example, if \verb|r| is a box containing
        \texttt{8}, then returns \texttt{8}
      \end{itemize}
    \item<+-> To write: \verb|r := 9|
      \begin{itemize}
        \item Updates the box to contain \texttt{9}
        \item After this, \verb|!r| will be \texttt{9}
      \end{itemize}
  \end{itemize}
\end{frame}

\end{document}
