\documentclass[aspectratio=169]{beamer}

\RequirePackage{ucs}
\usepackage[quiet]{fontspec}
\usepackage{xunicode}
\usepackage{xltxtra}
\usepackage{graphicx}
\usepackage{forloop}
\usepackage{stmaryrd}
\usepackage{tikz}
\usepackage{mathtools}
\usepackage{fancyvrb}
\usepackage{listings}
\usepackage{amsmath,amssymb,amsthm}

\usetikzlibrary{chains,decorations.pathmorphing,positioning,fit}
\usetikzlibrary{decorations.shapes,calc,backgrounds}
\usetikzlibrary{decorations.text,matrix}
\usetikzlibrary{arrows,shapes.geometric,shapes.symbols,scopes}

\newcommand\bigO[1]{\ensuremath{\mathcal{O}(#1)}}
\newcommand\fun[1]{\ensuremath{\lambda #1.\,}}
\renewcommand\T[1]{\ensuremath{T_{\mathit{#1}}}}

%% %%% SETTINGS

\frenchspacing
\unitlength=0.01\textwidth
\thicklines
\urlstyle{sf}
\graphicspath{{images/}}

%%% VERBATIM SETUP

\DefineVerbatimEnvironment
  {code}{Verbatim}
  {commandchars=\\\‹\›,
   commentchar=\%,
   codes={\catcode`$=3\relax},
   baselinestretch=.8,
   formatcom=\relax}
\renewcommand\FancyVerbFormatLine[1]{#1\strut}
\fvset{commandchars=\\\‹\›,
       codes={\catcode`$=3\relax},
       formatcom=\relax}
\newcommand\exponent[1]{^{#1}}
\DefineShortVerb{\^}

\newcommand\ALT[3]{\alt<#1>{#2}{#3}}

%%% COLOR AND BEAMER SETUP

\setbeamercolor{frametitle}{fg=black}
\setbeamercolor{normal text}{fg=black}

\defaultfontfeatures{
    Mapping=tex-text,
    Scale=MatchLowercase,
}
\setmainfont{TeX Gyre Schola}
\setmonofont{Monaco}

\usefonttheme{serif}

\setbeamertemplate{frametitle}
  {\begin{centering}\medskip
   \insertframetitle\par
   \end{centering}}

\setbeamertemplate{itemize item}{$\bullet$}

\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[text line]{%
    \hfill\strut{%
        \small\color{black!75}%
        \texttt{\insertframenumber:\insertframeslidenumber}%
    }%
    \hfill%
}

\makeatletter
\newcount\frameslidetemp
\newcommand\insertframeslidenumber{%
  \frameslidetemp\c@page\relax
  \advance\frameslidetemp-\beamer@startpageofframe\relax
  \advance\frameslidetemp1\relax
  \the\frameslidetemp
}
\makeatother

\newcommand\EmacsFrame{
  \begin{frame}{\relax}
    \thispagestyle{empty}
    \begin{center}
      \emph{--- To Emacs ---}
    \end{center}
  \end{frame}
}

%%% GENERALLY USEFUL MACROS

\newcommand<>\always[1]{#1}

\def\defgbox#1#2{%
  \expandafter\global\expandafter\newbox\csname#2box\endcsname%
  \expandafter\global\expandafter\def\csname#2\endcsname{%
    \expandafter\usebox\csname#2box\endcsname%
  }%
  \expandafter\global\expandafter\setbox\csname#2box\endcsname=#1%
}
\def\defhbox{\defgbox\hbox}
\def\defvbox{\defgbox\vbox}

\def\setgbox#1#2{%
  \expandafter\setbox\csname#2box\endcsname=#1%
}
\def\sethbox{\setgbox\hbox}
\def\setvbox{\setgbox\vbox}

%%%
%%% TIKZ
%%%

\newcommand\K[1]{\textcolor{orange!70!black}{#1}}
\newcommand\TY[1]{\textcolor{blue!70!black}{#1}}
\newcommand\REF{\textcolor{green!50!black}{ref}}

\newcommand\place[3][]{%
  \tikz[orp] \node[#1] at (#2) {#3};%
}

\newcommand\anchor[1]{%
  \tikz[orp] \coordinate (#1);%
}

\newcommand\nil[1]{
  \draw[nil] (#1.north east) -- (#1.south west)
}

\newcommand\cons[4][]{
  \node[cons,#1] (#2) {#3\kern3pt\anchor{#2 center}\kern3pt#4};
  \draw[thick] (#2 center |- #2.north) -- (#2 center |- #2.south)
}

\newcommand\genpointer[3][draw=blue!50!black,opacity=.5]{%
  \begin{tikzpicture}[rem,solid,#1]
    \node(pointer start) [circle, draw, inner sep=1.37pt] {};
    \draw[->, overlay] (pointer start) edge[bend right=#2] (#3);
  \end{tikzpicture}%
}

\newcommand\hpointer[2][30]{%
  \genpointer[draw=red!70!black]{#1}{#2}%
}

\newcommand\ppointer[2][30]{%
  \genpointer{#1}{#2}%
}

\newcommand\pointer[2][30]{%
  \ifhighlit\let\pointertemp\hpointer\else\let\pointertemp\ppointer\fi
  \pointertemp[#1]{#2}%
}


\newcount\reducetemp
\newcommand\reduce[3]{%
  \reducetemp#1\relax
  \advance\reducetemp -1\relax
  \alt<#1->{#3}{\alt<\the\reducetemp>{\highlight{#2}}{#2}}%
}

\tikzset{
  orp/.style={
    overlay,
    remember picture,
  },
  rem/.style={
    remember picture,
  },
  every node/.style={
    very thick,
    inner sep=3pt,
  },
  every path/.style={very thick},
  ref/.style={
    draw=red,
    densely dotted,
    font=\ttfamily,
  },
  cons/.style={
    draw=black,
    font=\ttfamily,
  },
  nil/.style={
    black!80!white,
    shorten >=2pt,
    shorten <=2pt,
  },
  highlight/.style={
    color=yellow!50!white!90!black,
    thick,
    draw,
    draw opacity=0.5,
    outer color=yellow!50,
    inner color=yellow!30,
    rounded corners=2pt,
  },
}

\newif\ifhighlit
\highlitfalse

\newcommand<>\highlight[2][\relax]{%
  \ifx#1\relax
    \begin{tikzpicture}
  \else
    \begin{tikzpicture}[remember picture]
  \fi
    \node[
      inner sep=0pt,
      outer sep=0pt,
      text depth=0pt,
      align=left,
    ] (highlight temp) {%
        \only#3{\highlittrue}%
        #2%
        \only#3{\highlitfalse}%
    };
    \begin{scope}[on background layer]
      \coordinate (highlight temp 1) at
        ($(highlight temp.north east) + (2pt,2pt)$);
      \coordinate (highlight temp 2) at
        ($(highlight temp.base west) + (-2pt,-3pt)$);
      \node#3 [
        overlay,
        highlight,
        inner sep=0pt,
        fit=(highlight temp 1) (highlight temp 2),
      ] {};
    \end{scope}
    \ifx#1\relax
      \relax
    \else
      \node (#1) [
        overlay,
        inner sep=0pt,
        fit=(highlight temp 1) (highlight temp 2),
      ] {};
    \fi
  \end{tikzpicture}%
}

\newcommand\HI[2]{\highlight<#1>{#2}}

\newcommand\huncover[3][\relax]{%
  \uncover<#2->{\highlight<#2>[#1]{#3}}%
}
\newcommand\honly[3][\relax]{%
  \only<#2->{\highlight<#2>[#1]{#3}}%
}

\def\S#1#2{#2<#1>}

\begin{document}

%%%
%%% TITLE PAGE
%%%

\begin{frame}[t,fragile]\relax
  \thispagestyle{empty}
  \vfill
  \begin{center}
  {\Huge Events}
  \par {\Large (after a little bit more about objects)}

  \bigskip
  \Large
  CS 51 and CSCI E-51

  \medskip
  April 3, 2014
  \end{center}

  %% Need to initialize Tikz opacity inside a frame, not outside:
  \begin{tikzpicture}[opacity=0,overlay]\end{tikzpicture}
\end{frame}

\begin{frame}<1-8>[fragile]{Polymorphic classes}
\begin{code}
\K‹class› \K‹type› \HI‹2-3›‹['a]› container =
\K‹object›
  \K‹method› insert : \HI‹3›‹'a› -> unit
  \K‹method› take   : \HI‹3›‹'a› option
  \K‹method› size   : int
\K‹end›
\pause[4]
\K‹class› \HI‹5›‹['a] stack : ['a] container› =
\K‹object›
  \HI‹6›‹\K‹val› \K‹mutable› contents = []›

  \HI‹7›‹\K‹method› insert x = contents <- x :: contents›
  \K‹method› take = \K‹match› contents \K‹with›
    | [] -> None
    | x :: xs -> contents <- xs;
                 Some x
  \K‹method› size = List.length contents
\K‹end›
\end{code}
\end{frame}

\begin{frame}[fragile]{Overriding for performance}
\begin{code}
\K‹class› \K‹type› ‹['a]› container =
\K‹object›
  \K‹method› insert : ‹'a› -> unit
  \K‹method› take   : ‹'a› option
  \K‹method› size   : int
\K‹end›
\pause
\K‹class› \HI‹3›‹['a] fast_size_stack : ['a] container› =
\K‹object›
  \HI‹4›‹\K‹inherit› ['a] stack› \HI‹5›‹\K‹as› super›
  \HI‹6›‹\K‹val› \K‹mutable› length = 0›

  \HI‹7›‹\K‹method!› size = length›
  \HI‹8›‹\K‹method!› insert x = length <- length + 1;›
                     \HI‹8›‹super\K‹#›insert x›
  \HI‹9›‹\K‹method!› take = length <- max (length - 1) 0;›
                 \HI‹9›‹super\K‹#›take›
\K‹end›
\end{code}
\end{frame}

%% MEH %%

\begin{frame}{Object wrap-up}
  \begin{itemize}
    \item<+-> Objects are like first-class modules
      \begin{itemize}
        \item<+-> Encapsulate abstract data type and operations
        \item<+-> Like modules, objects support subtyping (but OCaml requires
                  explicit coercions)
        \item<+-> Like modules, classes support implementation inheritance
        \item<+-> \emph{Unlike} modules, classes support (proper) overriding
      \end{itemize}
    \item<+-> Trade-off between datatypes and objects
      \begin{itemize}
        \item Datatypes: easy to add operations, hard to add variants
        \item Objects: easy to add variants, hard to add operations
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{On to events}
  \begin{itemize}
    \item<+-> We’ve focused on \emph{sequential, batch} computation
    \begin{itemize}
      \item sequential: computation is a sequence of steps
      \item batch: computation takes inputs, produces outputs
    \end{itemize}
    \item<+-> Why? History and difficulty
    \item<+-> There are more things in heaven and earth, Horatio
      \begin{itemize}
        \item users, devices, sensors, networks, evil printing robots, etc.
        \item multiple things happening at once\,\ldots
        \item \ldots\,and in multiple places
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Reactive programming}
  Many programs have to \emph{react} to incoming events:
  \begin{itemize}
    \item System events: clock, disks, etc.
    \item User events: keyboard, mouse, touch screen, microphone, etc.
    \item Network events: incoming packets, timeouts, closed connections, etc.
    \item Sensors: accelerometers, GPS, etc.
  \end{itemize}

  \par\medskip
  How can we make sense of this?
\end{frame}

\begin{frame}{Reactive programming issues}
  \begin{itemize}
    \item latency: how long is the delay before reacting?
    \item throughput: how many events can we handle per second?
    \item energy: are we burning battery?
    \item real-time: are there deadlines (and how can we know we’ll meet
    them)?
    \item synchronization: coordination among multiple events
    \item deadlock: failure to make progress
  \end{itemize}
\end{frame}

\begin{frame}{Reactive programming paradigms}
  \begin{itemize}
    \item events and listeners
    \item interrupts and interrupt handlers
    \item threads
    \item functional reactive programming (FRP)
  \end{itemize}

  We’ll start with events.
\end{frame}

\begin{frame}[fragile]{Basic event primitives}
  \begin{code}
\ALT‹11›‹\K‹type› \HI‹11›‹listener_id››‹›

\K‹class› \HI‹2-3›‹['a] event› :
  \K‹object›
    \K‹method› add_listener : \HI‹4›‹('a -> unit)› -> \ALT‹11›‹\HI‹11›‹listener_id››‹unit›
    $\cdots$
  \K‹end›
\pause[3]
\ALT‹8-›‹\K‹module› GraphicsEvents : ‹sig››‹›
  \K‹val› key_pressed  : \HI‹3›‹char event›\pause[5]
  \K‹val› button_down  : \HI‹6›‹(int * int) event›
  \K‹val› button_up    : (int * int) event
  \K‹val› mouse_motion : (int * int) event
  \K‹val› clock        : \HI‹7›‹unit event›
  \pause[8]
  \HI‹9›‹\K‹val› run          : (unit -> unit) -> unit›
  \HI‹10›‹\K‹val› terminate    : unit -> 'a›
\K‹end›
  \end{code}
\end{frame}

\begin{frame}[fragile]{A simple example}
\begin{code}
\K‹let› ball = \K‹new› circ ~color:Graphics.magenta (300, 225) 75
\pause
\K‹let› key_handler \HI‹3›‹(c : char)› : unit =
  (\K‹match› c \K‹with›
     | \HI‹4›‹‘r’ -> ball\K‹#›set_color Graphics.red›
     | ‘g’ -> ball\K‹#›set_color Graphics.green
     | ‘b’ -> ball\K‹#›set_color Graphics.blue
     | \HI‹5›‹‘q’ -> GraphicsEvents.terminate ()›
     | \HI‹6›‹_ -> ()›);
  \HI‹7›‹ball\K‹#›draw›
\pause[8]
\K‹let› simple () =
  \HI‹9›‹GraphicsEvents.key_pressed\K‹#›add_listener key_handler›;
  \HI‹10›‹GraphicsEvents.run (\K‹fun› () -> ball\K‹#›draw)›
\end{code}
\end{frame}

\EmacsFrame

\begin{frame}{Further examples}
  See \texttt{04-03-planned.ml}%
  \footnote{%
    \strut\url{http://www.eecs.harvard.edu/~tov/courses/cs51/lectures/}%
  } for:
  \begin{itemize}
    \item how events are implemented
    \item more examples using events
    \item more examples of classes
    \item some event \emph{combinators} (like \texttt{map} and
          \texttt{filter}!)
  \end{itemize}
\end{frame}

\begin{frame}{Implementing events}
  At the machine level, two basic strategies:
  \begin{itemize}
    \item Polling
      \begin{itemize}
        \item Periodically check if a device has sent a message
      \end{itemize}
    \item Interrupts
      \begin{itemize}
        \item The device suspends what the machine is doing to
        run an interrupt handler
        \item<2-> Sounds a lot like events!
      \end{itemize}
  \end{itemize}

  \medskip\pause[3]
  Sometimes we mix both approaches, \emph{e.g.,} poll other devices on
  clock interrupts
\end{frame}

\begin{frame}{Event implementation trade-offs}
  \begin{itemize}
    \item Polling
    \begin{itemize}
      \item You do a lot of busy waiting---bad for power consumption
      \item But you control when you poll
    \end{itemize}
    \item Interrupts
    \begin{itemize}
      \item You can sleep until interrupted
      \item But you might get interrupted at inconvenient times
    \end{itemize}
  \end{itemize}
  \pause
  Usually you don't get to choose and have to make do
\end{frame}

\EmacsFrame

\end{document}
