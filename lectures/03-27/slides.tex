\documentclass[aspectratio=169]{beamer}

\RequirePackage{ucs}
\usepackage[quiet]{fontspec}
\usepackage{xunicode}
\usepackage{xltxtra}
\usepackage{graphicx}
\usepackage{forloop}
\usepackage{stmaryrd}
\usepackage{tikz}
\usepackage{mathtools}
\usepackage{fancyvrb}
\usepackage{listings}
\usepackage{amsmath,amssymb,amsthm}

\usetikzlibrary{chains,decorations.pathmorphing,positioning,fit}
\usetikzlibrary{decorations.shapes,calc,backgrounds}
\usetikzlibrary{decorations.text,matrix}
\usetikzlibrary{arrows,shapes.geometric,shapes.symbols,scopes}

\newcommand\bigO[1]{\ensuremath{\mathcal{O}(#1)}}
\newcommand\fun[1]{\ensuremath{\lambda #1.\,}}
\renewcommand\T[1]{\ensuremath{T_{\mathit{#1}}}}

%% %%% SETTINGS

\frenchspacing
\unitlength=0.01\textwidth
\thicklines
\urlstyle{sf}
\graphicspath{{images/}}

%%% VERBATIM SETUP

\DefineVerbatimEnvironment
  {code}{Verbatim}
  {commandchars=\\\‹\›,
   commentchar=\%,
   codes={\catcode`$=3\relax},
   baselinestretch=.8,
   formatcom=\relax}
\renewcommand\FancyVerbFormatLine[1]{#1\strut}
\fvset{commandchars=\\\‹\›,
       codes={\catcode`$=3\relax},
       formatcom=\relax}
\newcommand\exponent[1]{^{#1}}
\DefineShortVerb{\^}

\newcommand\ALT[3]{\alt<#1>{#2}{#3}}

%%% COLOR AND BEAMER SETUP

\setbeamercolor{frametitle}{fg=black}
\setbeamercolor{normal text}{fg=black}

\defaultfontfeatures{
    Mapping=tex-text,
    Scale=MatchLowercase,
}
\setmainfont{TeX Gyre Schola}
\setmonofont{Monaco}

\usefonttheme{serif}

\setbeamertemplate{frametitle}
  {\begin{centering}\medskip
   \insertframetitle\par
   \end{centering}}

\setbeamertemplate{itemize item}{$\bullet$}

\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[text line]{%
    \hfill\strut{%
        \small\color{black!75}%
        \texttt{\insertframenumber:\insertframeslidenumber}%
    }%
    \hfill%
}

\makeatletter
\newcount\frameslidetemp
\newcommand\insertframeslidenumber{%
  \frameslidetemp\c@page\relax
  \advance\frameslidetemp-\beamer@startpageofframe\relax
  \advance\frameslidetemp1\relax
  \the\frameslidetemp
}
\makeatother

\newcommand\EmacsFrame{
  \begin{frame}{\relax}
    \thispagestyle{empty}
    \begin{center}
      \emph{--- To Emacs ---}
    \end{center}
  \end{frame}
}

%%% GENERALLY USEFUL MACROS

\newcommand<>\always[1]{#1}

\def\defgbox#1#2{%
  \expandafter\global\expandafter\newbox\csname#2box\endcsname%
  \expandafter\global\expandafter\def\csname#2\endcsname{%
    \expandafter\usebox\csname#2box\endcsname%
  }%
  \expandafter\global\expandafter\setbox\csname#2box\endcsname=#1%
}
\def\defhbox{\defgbox\hbox}
\def\defvbox{\defgbox\vbox}

\def\setgbox#1#2{%
  \expandafter\setbox\csname#2box\endcsname=#1%
}
\def\sethbox{\setgbox\hbox}
\def\setvbox{\setgbox\vbox}

%%%
%%% TIKZ
%%%

\newcommand\K[1]{\textcolor{orange!70!black}{#1}}
\newcommand\TY[1]{\textcolor{blue!70!black}{#1}}
\newcommand\REF{\textcolor{green!50!black}{ref}}

\newcommand\place[3][]{%
  \tikz[orp] \node[#1] at (#2) {#3};%
}

\newcommand\anchor[1]{%
  \tikz[orp] \coordinate (#1);%
}

\newcommand\nil[1]{
  \draw[nil] (#1.north east) -- (#1.south west)
}

\newcommand\cons[4][]{
  \node[cons,#1] (#2) {#3\kern3pt\anchor{#2 center}\kern3pt#4};
  \draw[thick] (#2 center |- #2.north) -- (#2 center |- #2.south)
}

\newcommand\genpointer[3][draw=blue!50!black,opacity=.5]{%
  \begin{tikzpicture}[rem,solid,#1]
    \node(pointer start) [circle, draw, inner sep=1.37pt] {};
    \draw[->, overlay] (pointer start) edge[bend right=#2] (#3);
  \end{tikzpicture}%
}

\newcommand\hpointer[2][30]{%
  \genpointer[draw=red!70!black]{#1}{#2}%
}

\newcommand\ppointer[2][30]{%
  \genpointer{#1}{#2}%
}

\newcommand\pointer[2][30]{%
  \ifhighlit\let\pointertemp\hpointer\else\let\pointertemp\ppointer\fi
  \pointertemp[#1]{#2}%
}


\newcount\reducetemp
\newcommand\reduce[3]{%
  \reducetemp#1\relax
  \advance\reducetemp -1\relax
  \alt<#1->{#3}{\alt<\the\reducetemp>{\highlight{#2}}{#2}}%
}

\tikzset{
  orp/.style={
    overlay,
    remember picture,
  },
  rem/.style={
    remember picture,
  },
  every node/.style={
    very thick,
    inner sep=3pt,
  },
  every path/.style={very thick},
  ref/.style={
    draw=red,
    densely dotted,
    font=\ttfamily,
  },
  cons/.style={
    draw=black,
    font=\ttfamily,
  },
  nil/.style={
    black!80!white,
    shorten >=2pt,
    shorten <=2pt,
  },
  highlight/.style={
    color=yellow!50!white!90!black,
    thick,
    draw,
    draw opacity=0.5,
    outer color=yellow!50,
    inner color=yellow!30,
    rounded corners=2pt,
  },
}

\newif\ifhighlit
\highlitfalse

\newcommand<>\highlight[2][\relax]{%
  \ifx#1\relax
    \begin{tikzpicture}
  \else
    \begin{tikzpicture}[remember picture]
  \fi
    \node[
      inner sep=0pt,
      outer sep=0pt,
      text depth=0pt,
      align=left,
    ] (highlight temp) {%
        \only#3{\highlittrue}%
        #2%
        \only#3{\highlitfalse}%
    };
    \begin{scope}[on background layer]
      \coordinate (highlight temp 1) at
        ($(highlight temp.north east) + (2pt,2pt)$);
      \coordinate (highlight temp 2) at
        ($(highlight temp.base west) + (-2pt,-3pt)$);
      \node#3 [
        overlay,
        highlight,
        inner sep=0pt,
        fit=(highlight temp 1) (highlight temp 2),
      ] {};
    \end{scope}
    \ifx#1\relax
      \relax
    \else
      \node (#1) [
        overlay,
        inner sep=0pt,
        fit=(highlight temp 1) (highlight temp 2),
      ] {};
    \fi
  \end{tikzpicture}%
}

\newcommand\HI[2]{\highlight<#1>{#2}}

\newcommand\huncover[3][\relax]{%
  \uncover<#2->{\highlight<#2>[#1]{#3}}%
}
\newcommand\honly[3][\relax]{%
  \only<#2->{\highlight<#2>[#1]{#3}}%
}

\def\S#1#2{#2<#1>}

\begin{document}

%%%
%%% TITLE PAGE
%%%

\begin{frame}[t,fragile]\relax
  \thispagestyle{empty}
  \vfill
  \begin{center}
  {\Huge Putting the “O” in OCaml}

  \bigskip
  \Large
  CS 51 and CSCI E-51

  \medskip
  March 27, 2014

  \vfill
  \small
  To install the graphics library on your appliance:
  \par\smallskip
  \color{red!66!black}
  ^curl -sL http://goo.gl/f8J4L6 | sh^
  \end{center}

  %% Need to initialize Tikz opacity inside a frame, not outside:
  \begin{tikzpicture}[opacity=0,overlay]\end{tikzpicture}
\end{frame}

\begin{frame}{But first, a word about final projects}
  Requirements:
  \begin{itemize}
    \item Teams of 2 to 4
    \item Any language you please
    \item Must be \emph{computationally interesting}
    \item Proposals due 4/4 (a week from tomorrow)
    \item More details will be forthcoming\ldots
  \end{itemize}
\end{frame}

\begin{frame}{Some example projects}
  \begin{itemize}
  \item A document search suite and plagarism detector (OCaml)
  \item A version control system, focusing on the rsync and compression
  algorithms (Java)
  \item Machine learning for games: genetic algorithm \& neural network to find
  optimum play for a strategy game (Java)
  \item A simplex solver with a complete matrix module (OCaml)
  \item Collaborative filtering using five algorithms (OCaml)
  \item Mini-WolframAlpha (OCaml)
  \item Comparison of dictionary performance with 6 different data
  structures (Java)
  \item SAT solver (OCaml)
  \item Content-aware image resizing (Python)
  \item Natural language parsing (Python)
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{A crude GUI}
\begin{code}
\K‹type› point = { x : int; y : int }

\K‹type› rect = { rect_ll : point; rect_width : int; rect_height : int }

\K‹type› circ = { circ_center : point; circ_radius : int }

\K‹type› text = { text_ll : point; text_value : string }
\pause
\K‹type› display_elt
  = Rectangle of rect
  | Circle of circ
  | Text of text
\pause
\K‹type› scene = display_elt list
\end{code}
\end{frame}

\begin{frame}[fragile]{Rendering}
\begin{code}
\K‹let› draw (elt : display_elt) : unit =
  \K‹match› elt \K‹with›
  | Rectangle r ->
     \HI‹2›‹fill_rect r.rect_ll.x r.rect_ll.y r.rect_width r.rect_height›
  | Circle c ->
     \HI‹3›‹fill_circle c.circ_center.x c.circ_center.y c.circ_radius›
  | Text t ->
     \HI‹4›‹moveto t.text_ll.x t.text_ll.y;›
     \HI‹5›‹draw_string t.text_value›
\pause[6]
\K‹let› draw_scene : scene -> unit = List.iter ~f:draw
\end{code}
\end{frame}

\EmacsFrame

\begin{frame}[fragile]{A different approach}
\begin{code}
\K‹type› display_elt = { draw : unit -> unit }
\pause
\K‹let› rect (ll : point) (width : int) (height : int) : display_elt =
  { draw = \K‹fun› () -> fill_rect ll.x ll.y width height }
\pause
\K‹let› circ (center : point) (radius : int) : display_elt =
  { draw = \K‹fun› () -> fill_circle center.x center.y radius }
\pause
\K‹let› text (ll : point) (value : string) : display_elt =
  { draw = \K‹fun› () ->
           moveto ll.x ll.y;
           draw_string value }
\pause
\K‹type› scene = display_elt list
\end{code}
\end{frame}

\EmacsFrame

\begin{frame}{This is OO}
  \begin{itemize}
    \item<1-> Objects
      \begin{itemize}
        \item Represented as OCaml record
        \item Collection of named values---\emph{instance variables}
        \item Collection of operations---\emph{methods}
      \end{itemize}
    \item<2-> Interfaces
      \begin{itemize}
        \item An OCaml record type
        \item Instance variables are hidden
        \item All interaction is via methods
      \end{itemize}
    \item<3-> Extensible types
      \begin{itemize}
        \item Easy to add new kinds of display elements
        \item Each object defines its own behavior
        \item<4-> But not so easy to add new methods\ldots
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Datatypes versus objects}
  \begin{itemize}
    \item Datatypes are \emph{closed}
      \begin{itemize}
        \item Hard to add new alternatives \uncover<2>{(production)}
        \item Easy to add new operations \uncover<2>{(consumption)}
      \end{itemize}
    \item Objects are \emph{open}
      \begin{itemize}
        \item Easy to add new alternatives \uncover<2>{(production)}
        \item Hard to add new operations \uncover<2>{(consumption)}
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Other OO concepts}
  \begin{itemize}
    \item<+-> \emph{Classes}
      \begin{itemize}
        \item Specify how to build objects
        \item Like our functions ^rect^, ^circ^, etc.
      \end{itemize}
    \item<+-> \emph{Inheritance} (implementation reuse)
      \begin{itemize}
        \item Base new classes on existing ones
        \item Can change or extend behavior
      \end{itemize}
    \item<+-> \emph{Subtyping} \uncover<+->{(is \emph{not} inheritance!)}
      \begin{itemize}
        \item Use an object where a smaller interface is expected
        \item<.-> C++, Java, and C\# confuse subtyping with inheritance
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{The “O” in OCaml}
\begin{code}
\K‹class› \K‹type› display_elt =
\K‹object›
  \K‹method› draw      : unit
  \K‹method› get_color : color
  \K‹method› set_color : color -> unit
  \K‹method› translate : dx:int -> dy:int -> unit
\K‹end›
\pause
\K‹class› \HI‹2›‹rect› \HI‹3›‹(ll : point) (width : int) (height : int)› : display_elt =
\K‹object›
  \HI‹4›‹\K‹val› \K‹mutable› ll = ll›
  \HI‹4›‹\K‹val› \K‹mutable› color = black›

  \HI‹5›‹\K‹method› get_color = color›
  \HI‹6›‹\K‹method› set_color c = color <- c›
  \HI‹7›‹\K‹method› draw = set_color color;›
                \HI‹7›‹fill_rect ll.x ll.y width height›
  \K‹method› translate ~dx ~dy = ll <- translate_point ll ~dx ~dy
\K‹end›
\end{code}
\end{frame}

\begin{frame}[fragile]{Another class}
\begin{code}
\K‹class› \K‹type› display_elt =
\K‹object›
  \K‹method› draw      : unit
  \K‹method› get_color : color
  \K‹method› set_color : color -> unit
  \K‹method› translate : dx:int -> dy:int -> unit
\K‹end›

\K‹class› circ (center : point) (radius : int) : display_elt =
\K‹object›
  \K‹val› \K‹mutable› center = center
  \K‹val› \K‹mutable› color = black

  \K‹method› get_color = color
  \K‹method› set_color c = color <- c
  \K‹method› draw = set_color color;
                fill_circle center.x center.y radius
  \K‹method› translate ~dx ~dy = center <- translate_point center ~dx ~dy
\K‹end›
\end{code}
\end{frame}

\EmacsFrame

\begin{frame}[fragile]{Comparison to Java (1/2)}
\begin{code}
\K‹public› \K‹interface› \TY‹DisplayElt› {
    \TY‹void› draw();
    \TY‹Color› getColor();
    \TY‹void› setColor(\TY‹Color› c);
    \TY‹void› translate(\TY‹int› dx, \TY‹int› dy);
}
\pause
\K‹public› \K‹class› \TY‹Shape› {
    \HI‹3›‹\K‹protected› \TY‹Point› pos;›
    \HI‹3›‹\K‹protected› \TY‹Color› color;›

    \HI‹4›‹\K‹public› Shape(\TY‹Point› p) { pos \K‹=› p; }›

    \HI‹5›‹\K‹public› \TY‹Color› getColor() { \K‹return› color; }›
    \HI‹5›‹\K‹public› \TY‹void› setColor(Color c) { color = c; }›
    \HI‹5›‹\K‹public› \TY‹void› translate(\TY‹int› dx, \TY‹int› dy) { pos = pos.translate(dx, dy) }›
}
\end{code}
\end{frame}

\begin{frame}<1-8>[fragile]{Comparison to Java (2/2)}
\begin{code}
\K‹public› \K‹interface› \TY‹DisplayElt› {
    \TY‹void› draw();
    \TY‹Color› getColor();
    \TY‹void› setColor(\TY‹Color› c);
    \TY‹void› translate(\TY‹int› dx, \TY‹int› dy);
}
\pause
\K‹public› \K‹static› \K‹class› \TY‹Rect› \HI‹3›‹\K‹extends› \TY‹Shape›› \HI‹4›‹\K‹implements› \TY‹DisplayElt››{
    \HI‹5›‹\K‹private› \K‹final› \TY‹int› width, height;›

    \K‹public› \HI‹6›‹Rect(\TY‹Point› ll, \TY‹int› w, \TY‹int› h)› {
        \HI‹7›‹\K‹super›(ll);› \HI‹8›‹width = w; height = h;›
    }

    \K‹public› \K‹void› draw() {
        gc.setColor(color);
        gc.fillRect(pos.x, pos.y, width, height);
    }
}
\end{code}
\end{frame}

\EmacsFrame

%- \begin{frame}<1-8>[fragile]{Polymorphic classes}
%- \begin{code}
%- \K‹class› \K‹type› \HI‹2-3›‹['a]› container =
%- \K‹object›
%-   \K‹method› insert : \HI‹3›‹'a› -> unit
%-   \K‹method› take   : \HI‹3›‹'a› option
%-   \K‹method› size   : int
%- \K‹end›
%- \pause[4]
%- \K‹class› \HI‹5›‹['a] stack : ['a] container› =
%- \K‹object›
%-   \HI‹6›‹\K‹val› \K‹mutable› contents = []›
%- 
%-   \HI‹7›‹\K‹method› insert x = contents <- x :: contents›
%-   \K‹method› take = \K‹match› contents \K‹with›
%-     | [] -> None
%-     | x :: xs -> contents <- xs;
%-                  Some x
%-   \K‹method› size = List.length contents
%- \K‹end›
%- \end{code}
%- \end{frame}
%- 
%- \begin{frame}[fragile]{Overriding for performance}
%- \begin{code}
%- \K‹class› \K‹type› ‹['a]› container =
%- \K‹object›
%-   \K‹method› insert : ‹'a› -> unit
%-   \K‹method› take   : ‹'a› option
%-   \K‹method› size   : int
%- \K‹end›
%- \pause
%- \K‹class› \HI‹3›‹['a] fast_size_stack : ['a] container› =
%- \K‹object›
%-   \HI‹4›‹\K‹inherit› ['a] stack› \HI‹5›‹\K‹as› super›
%-   \HI‹6›‹\K‹val› \K‹mutable› length = 0›
%- 
%-   \HI‹7›‹\K‹method› size = length›
%-   \HI‹8›‹\K‹method› insert x = length <- length + 1;›
%-                         \HI‹8›‹super#insert x›
%-   \HI‹9›‹\K‹method› take = length <- max (length - 1) 0;›
%-                     \HI‹9›‹super#take›
%- \K‹end›
%- \end{code}
%- \end{frame}
%- 
%- %% MEH %%
%- 
%- \begin{frame}{Wrap-up}
%-   \begin{itemize}
%-     \item<+-> Objects are like first-class modules
%-       \begin{itemize}
%-         \item<+-> Encapsulate abstract data type and operations
%-         \item<+-> Like modules, objects support subtyping (but OCaml requires
%-                   explicit coercions)
%-         \item<+-> Like modules, classes support implementation inheritance
%-         \item<+-> \emph{Unlike} modules, classes support (proper) overriding
%-       \end{itemize}
%-     \item<+-> Trade-off between datatypes and objects
%-       \begin{itemize}
%-         \item Datatypes: easy to add operations, hard to add variants
%-         \item Objects: easy to add variants, hard to add operations
%-       \end{itemize}
%-   \end{itemize}
%- \end{frame}

\end{document}
