public class Example {
    /* Begin of mock-up code */
    public static class Color { }

    public static class Point {
        public final int x, y;

        public Point(int nx, int ny) {
            x = nx;
            y = ny;
        }

        public Point translate(int dx, int dy) {
            return new Point(x + dx, y + dy);
        }
    }

    public static class Gc {
        public void setColor(Color c) { }
        public void fillRect(int x, int y, int w, int h) { }
    }

    public static Gc gc = new Gc();

    /* End of mock-up code */

    public static interface DisplayElt {
        void draw();
        Color getColor();
        void setColor(Color c);
        void translate(int dx, int dy);
    }

    public static class Shape {
        protected Point pos;
        protected Color color;

        public Shape(Point p) { pos = p; }
        public Color getColor() { return color; }
        public void setColor(Color c) { color = c; }
        public void translate(int dx, int dy) { pos = pos.translate(dx, dy); }
    }

    public static class Rect extends Shape implements DisplayElt {
        private final int width, height;

        public Rect(Point ll, int w, int h) {
            super(ll);
            width = w;
            height = h;
        }

        public void draw() {
            gc.setColor(color);
            gc.fillRect(pos.x, pos.y, width, height);
        }
    }
}
