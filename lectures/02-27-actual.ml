(* Lecture 9 (02/27): more abstract data types; intro to complexity *)

(* This is 02-27-actual.ml. If you want to start with the same code I
 * have and follow along, you can get it at:
 *
 *   http://goo.gl/uQAIqv
 *)

open Core.Std

(** Matrices *)

(* Matrices are defined over rings, which offer some operations: *)
module type RING =
  sig
    type t
    val zero : t
    val one : t
    val add : t -> t -> t
    val mul : t -> t -> t
    (* ... *)
  end

module IntRing =
  struct
    type t = int
    let zero = 0
    let one = 1
    let add = (+)
    let mul = ( * )
  end

module FloatRing =
  struct
    type t = float
    let zero = 0.
    let one = 1.
    let add = (+.)
    let mul = ( *. )
  end

module BoolRing =
  struct
    type t = bool
    let zero = false
    let one = true
    let add = (||)
    let mul = (&&)
  end

(* A signature for matrices *)
module type MATRIX =
  sig
    type elt
    type t
    val of_list : elt list list -> t
    val to_list : t -> elt list list
    val scale : elt -> t -> t
    val add : t -> t -> t
    val mul : t -> t -> t
  end

(* Make a matrix over any ring *)
module DenseMatrix (R : RING) : MATRIX with type elt = R.t =
  struct
    type elt = R.t
    type t = elt list list
    let of_list = ident
    let to_list = ident
    let scale s = List.map ~f:(List.map ~f:(R.mul s))
    let add = List.map2_exn ~f:(List.map2_exn ~f:R.add)
    let mul _ _ = failwith "not implemented"
  end

module DenseIntMatrix = DenseMatrix(IntRing)
module DenseFloatMatrix = DenseMatrix(FloatRing)
module DenseBoolMatrix = DenseMatrix(BoolRing)

(* Stuff after this IS gonna type. *)

let matrix1 = DenseIntMatrix.of_list [[2; 3]; [4; 5]]
let matrix2 = DenseIntMatrix.add matrix1 matrix1
assert( DenseIntMatrix.to_list matrix2 = [[4; 6]; [8; 10]] )

let matrix3 = DenseIntMatrix.scale (-1) matrix2
assert( DenseIntMatrix.to_list matrix3 = [[-4; -6]; [-8; -10]] )

(** SETS *)

(* A signature for sets *)
module type SET =
  sig
    (* The element type of the set *)
    type elt
    (* The type of sets *)
    type t

    (* The empty set *)
    val empty : t
    (* A singleton set *)
    val singleton : elt -> t
    (* The union of two sets *)
    val union : t -> t -> t
    val inter : t -> t -> t

    val is_empty : t -> bool
    val mem : t -> elt -> bool
    (* ... *)
  end

module SetTests(Set : SET with type elt = string) =
  struct
    open Set

    let s1 = singleton "one" (* { "one" } *)
    let s2 = singleton "two"
    let s3 = singleton "three"

    let s12 = union s1 s2  (* { "one", "two" } *)
    let s13 = union s1 s3
    let s123 = union s12 s3

    let s1' = inter s12 s13  (* { "one" } *)

    assert( mem s1 "one" )
    assert( mem s12 "one" )
    assert( not (mem s2 "one") )
    assert( mem s1' "one" )
    assert( not (mem s1' "two") )
  end

module type ELT =
  sig
    type t
    val compare : t -> t -> int
  end

(* If Elt.t = string list
   then SortedListSet(Elt).elt = string list *)

module SortedListSet (Elt : ELT) : (SET with type elt = Elt.t) =
  struct
    type elt = Elt.t
    type t = elt list

    let empty = []
    let singleton x = [x]

    let rec inter xs ys =
      match xs, ys with
      | x :: xs', y :: ys' ->
         (match Elt.compare x y with
          | -1 -> inter xs' ys
          | 1  -> inter xs ys'
          | _  -> x :: inter xs' ys')
      | _ -> []

    let rec union xs ys =
      match xs, ys with
      | [], _ -> ys
      | _, [] -> xs
      | x :: xs', y :: ys' ->
         (match Elt.compare x y with
          | -1 -> x :: union xs' ys
          | 1 ->  y :: union xs ys'
          | _ ->  x :: union xs' ys')

    let is_empty = List.is_empty
    let mem set x = List.mem set x ~equal:(fun y z -> Elt.compare y z = 0)
  end

module StringSet = SortedListSet(String)
module StringSet' = struct
  type elt = string
  include Set.Make(String)
end

module StringSetTested = SetTests(StringSet)
module StringSetTested' = SetTests(StringSet')

(** Resource usage *)

(* T(x :: xs') = C + T(xs')
   T([])       = D

   T(N) = C + T(N - 1)
   T(0) = D

   T(N) = D + CN

   append is O(N) where N is the length of xs
 *)

let rec append xs ys =
  match xs with
  | x :: xs' -> x :: append xs' ys
  | _ -> ys

let rec reverse xs =
  match xs with
  | x :: xs' -> append (reverse xs') [x]
  | _ -> []
