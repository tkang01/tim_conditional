(* Lecture 15: Putting the "O" in OCaml *)

(* If you're using the CS50 appliance with our standard OCaml installation, you
   can install the graphics library used during this lecture by typing the
   following command into the terminal in your appliance:

    curl -sL http://goo.gl/f8J4L6 | sh

 *)

#require "graphics"

(* See http://caml.inria.fr/pub/docs/manual-ocaml/libref/Graphics.html *)
open Graphics

(* Make sure the graphics window is open (and blank) *)
let reset () = close_graph (); open_graph ""

type point = { x : int; y : int }

type rect = { rect_ll : point; rect_width : int; rect_height : int }

type circ = { circ_center : point; circ_radius : int }

type text = { text_ll : point; text_value : string }

type display_elt
  = Rectangle of rect
  | Circle of circ
  | Text of text

type scene = display_elt list

let draw (elt : display_elt) : unit =
  match elt with
  | Rectangle r ->
     fill_rect r.rect_ll.x r.rect_ll.y r.rect_width r.rect_height
  | Circle c ->
     fill_circle c.circ_center.x c.circ_center.y c.circ_radius
  | Text t ->
     moveto t.text_ll.x t.text_ll.y;
     draw_string t.text_value

let draw_scene : scene -> unit = List.iter ~f:draw

let p1 = { x = 300; y = 225 }
let p2 = { x = 100; y = 100 }
let p3 = { x = 500; y = 350 }

let rect1 = { rect_ll = p2; rect_width = 200; rect_height = 100 }
let rect2 = { rect_ll = p3; rect_width = 50; rect_height = 50 }

let circ1 = { circ_center = p1; circ_radius = 150 }
let circ2 = { circ_center = p3; circ_radius = 25 }

let text1 = { text_ll = p2; text_value = "Hello" }
let text2 = { text_ll = p1; text_value = "CS 51 and CSCI E-51" }

let scene1 = [ Text text1; Text text2; Circle circ2 ]
let scene2 = [ Rectangle rect1; Rectangle rect2; Circle circ1; Circle circ2 ]

let _ = reset (); draw_scene scene1

let _ = reset (); draw_scene scene2

type tri = { tri_p1 : point; tri_p2 : point; tri_p3 : point }

let tri1 = { tri_p1 = { x = 50; y = 400 };
             tri_p2 = { x = 300; y = 400 };
             tri_p3 = { x = 50; y = 300 } }

type extended_display_elt = Triangle of tri
                          | Normal of display_elt

let extended_draw : extended_display_elt -> unit =
  function
  | Triangle t -> fill_poly (Array.map ~f:(fun { x; y } -> (x, y))
                                       [| t.tri_p1; t.tri_p2; t.tri_p3 |])
  | Normal elt -> draw elt

type extended_scene = extended_display_elt list

let extended_draw_scene : extended_scene -> unit =
  List.iter ~f:extended_draw

let _ = reset ();
        extended_draw_scene [ Triangle tri1;
                              Normal (Text text1);
                              Normal (Circle circ2) ]

let _ = () (* Don't scroll yet, Emacs *)

type display_elt = { draw : unit -> unit }

let rect (ll : point) (width : int) (height : int) : display_elt =
  { draw = fun () -> fill_rect ll.x ll.y width height }

let circ (center : point) (radius : int) : display_elt =
  { draw = fun () -> fill_circle center.x center.y radius }

let text (ll : point) (value : string) : display_elt =
  { draw = fun () ->
           moveto ll.x ll.y;
           draw_string value }

type scene = display_elt list

let rect1 = rect p2 200 100
let rect2 = rect p3 50 50

let circ1 = circ p1 150
let circ2 = circ p3 25

let text1 = text p2 "Hello"
let text2 = text p1 "CS 51 and CSCI E-51"

let scene1 = [rect1; rect2; circ1; circ2; text1]

let draw_scene : scene -> unit =
  List.iter ~f:fun elt -> elt.draw ()

let _ = reset ();
        draw_scene scene1

let tri (p1 : point) (p2 : point) (p3 : point) : display_elt =
  { draw = fun () ->
           fill_poly (Array.map ~f:(fun { x; y } -> (x, y))
                                [| p1; p2; p3 |]) }

let tri1 = tri { x = 50; y = 400 }
               { x = 300; y = 400 }
               { x = 50; y = 300 }

let _ = reset ();
        draw_scene (tri1 :: scene1)

type display_elt =
    { draw      : unit -> unit;
      get_color : unit -> color;
      set_color : color -> unit;
      translate : dx:int -> dy:int -> unit;
    }

let translate_point (pt : point) ~(dx : int) ~(dy : int) : point =
  { x = pt.x + dx; y = pt.y + dy }

let rect (ll : point) (width : int) (height : int) : display_elt =
  let rll    = ref ll in
  let rcolor = ref black in
  { draw = (fun () -> set_color !rcolor;
                      fill_rect !rll.x !rll.y width height);
    get_color = (fun () -> !rcolor);
    set_color = (fun c -> rcolor := c);
    translate = (fun ~dx ~dy -> rll := translate_point !rll ~dx ~dy);
  }

let circ (center : point) (radius : int) : display_elt =
  let rcenter = ref center in
  let rcolor  = ref black in
  { draw      = (fun () -> set_color !rcolor;
                           fill_circle !rcenter.x !rcenter.y radius);
    get_color = (fun () -> !rcolor);
    set_color = (:=) rcolor;
    translate = (fun ~dx ~dy -> rcenter := translate_point !rcenter ~dx ~dy);
  }

let _ = reset ();
        let r = rect p2 100 75 in
        let c = circ p3 50 in
        r.set_color red;
        r.draw ();
        r.translate ~dx:100 ~dy:100;
        r.set_color green;
        r.draw ();
        r.translate ~dx:100 ~dy:100;
        r.set_color blue;
        r.draw ();
        c.set_color (r.get_color ());
        c.draw ()

(*** TO SLIDES ***)

let _ = () (* Don't scroll yet, Emacs *)

class type display_elt =
object
  method draw      : unit
  method get_color : color
  method set_color : color -> unit
  method translate : dx:int -> dy:int -> unit
end

class rect (ll : point) (width : int) (height : int) : display_elt =
object
  val mutable ll = ll
  val mutable color = black

  method draw = set_color color;
                fill_rect ll.x ll.y width height

  method get_color = color

  method set_color c = color <- c

  method translate ~dx ~dy = ll <- translate_point ll ~dx ~dy
end

class circ (center : point) (radius : int) : display_elt =
object
  val mutable center = center
  val mutable color = black

  method draw = set_color color;
                fill_circle center.x center.y radius

  method get_color = color

  method set_color c = color <- c

  method translate ~dx ~dy = center <- translate_point center ~dx ~dy
end

let _ = reset ();
        let r = new rect p2 100 75 in
        let c = new circ p3 50 in
        r#set_color red;
        r#draw;
        r#translate ~dx:100 ~dy:100;
        r#set_color green;
        r#draw;
        r#translate ~dx:100 ~dy:100;
        r#set_color blue;
        r#draw;
        c#set_color r#get_color;
        c#draw

class shape (p : point) =
object
  val mutable pos = p
  val mutable color = black

  method get_color = color
  method set_color c = color <- c
  method translate ~dx ~dy = pos <- translate_point pos ~dx ~dy
end

class rect (ll : point) (width : int) (height : int) =
object
  inherit shape ll

  method draw = set_color color;
                fill_rect pos.x pos.y width height
end

class circ (center : point) (radius : int) =
object
  inherit shape center

  method draw = set_color color;
                fill_circle pos.x pos.y radius
end

let _ =
  reset ();
  let scene1 = [ new rect p2 50 50; new circ p2 50 ] in
  List.iter scene1 ~f:(fun elt -> elt#set_color red;
                                  elt#draw;
                                  elt#translate ~dx:150 ~dy:100;
                                  elt#set_color green;
                                  elt#draw;
                                  elt#translate ~dx:150 ~dy:100;
                                  elt#set_color blue;
                                  elt#draw)

(*** TO SLIDES ***)
















































(* Polymorphism *)

class type ['a] container =
object
  method insert : 'a -> unit
  method take   : 'a option
  method size   : int
end

class ['a] stack : ['a] container =
object
  val mutable contents = []

  method insert x = contents <- x :: contents
  method take = match contents with
    | [] -> None
    | x :: xs -> contents <- xs; Some x
  method size = List.length contents
end

class ['a] fast_size_stack : ['a] container =
object
  inherit ['a] stack as super
  val mutable length = 0

  method size = length
  method insert x = length <- length + 1; super#insert x
  method take = length <- max (length - 1) 0; super#take
end
