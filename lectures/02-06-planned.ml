(* Lecture 4: notes for intro to OCaml, continued (2014-02-06)
 * (higher-order functions, polymorphism) *)

(* Increment every int in a list *)
let rec inc_list (xs : int list) : int list =
  match xs with
  | [] -> []
  | x :: xs' -> x + 1 :: inc_list xs'

(* Decrement every int in a list *)
let rec dec_list (xs : int list) : int list =
  match xs with
  | [] -> []
  | x :: xs' -> x - 1 :: dec_list xs'

(* Add 6 to every int in a list *)
let rec add6_list (xs : int list) : int list =
  match xs with
  | [] -> []
  | x :: xs' -> x + 6 :: add6_list xs'

(** Let's abstract! **)

(* Add the given int to every int in a list *)
let rec add_to_list (n : int) (xs : int list) : int list =
  match xs with
  | [] -> []
  | x :: xs' -> x + n :: add_to_list n xs'

(** Now we can use our new abstraction: **)

let inc_list xs = add_to_list 1 xs
let dec_list xs = add_to_list (-1) xs
let add6_list xs = add_to_list 6 xs

(** Partial application: **)

let inc_list = add_to_list 1
let dec_list = add_to_list (-1)
let add6_list = add_to_list 6

(* Square every int in a list *)
let rec square_list (xs : int list) : int list =
  match xs with
  | [] -> []
  | x :: xs' -> x * x :: square_list xs'

(** We need to abstract the operation. **)

(* To transform a list by applying a function to each element *)
let rec map (f : int -> int) (xs : int list) : int list =
  match xs with
  | [] -> []
  | x :: xs' -> f x :: map f xs'

(** Using our new abstraction: **)

let inc x = x + 1
let add6 x = x + 6
let square x = x * x

let inc_list xs = map inc xs

let inc_list = map inc
let add6_list = map add6
let square_list = map square

(** A trickier case: **)

let add_to_list (n : int) (xs : int list) : int list =
  let addn x = x + n in
  map addn xs

(** We can use an anonymous function. **)

let add_to_list (n : int) (xs : int list) : int list =
  map (fun x -> x + n) xs

let inc_list = map (fun x -> x + 1)
let square_list = map (fun x -> x * x)

(** OCaml functions are "curried" -- always 1 argument, 1 result **)

let rec map : (int -> int) -> (int list -> int list) =
  fun (f : int -> int) ->
    fun (xs : int list) ->
      match xs with
      | [] -> []
      | x :: xs' -> f x :: map f xs'

let _ = (+)
let _ = ( * )

let inc_list = map ((+) 1)
let add_to_list n = map ((+) n)

(** Let's look at a different pattern *)

(* Add up the ints in a list *)
let rec sum (xs : int list) : int =
  match xs with
  | [] -> 0
  | x :: xs' -> x + sum xs'

(* Multiply the ints in a list *)
let rec prod (xs : int list) : int =
  match xs with
  | [] -> 1
  | x :: xs' -> x * prod xs'

(** Let's abstract! **)

let rec fold_right (f : int -> int -> int) (z : int)
                   (xs : int list) : int =
  match xs with
  | [] -> z
  | x :: xs' -> f x (fold_right f z xs')

(** Use our new abstraction **)

let sum = fold_right (+) 0
let prod = fold_right ( * ) 1

(** What if we want to do this? **)

(* Convert a list of ints to a list of strings *)
let rec strings_of_ints (xs : int list) : string list =
  match xs with
  | [] -> []
  | x :: xs' -> string_of_int x :: strings_of_ints xs'

(* Keep only the positive ints in a list *)
let rec keep_positive (xs : int list) : int list =
  match xs with
  | [] -> []
  | x :: xs' -> if x > 0
                then x :: keep_positive xs'
                else keep_positive xs'

(** These functions are more general **)

let rec map f xs =
  match xs with
  | [] -> []
  | x :: xs' -> f x :: map f xs'

let rec map (f : 'a -> 'b) (xs : 'a list) : 'b list =
  match xs with
  | [] -> []
  | x :: xs' -> f x :: map f xs'

(* Convert a list of ints to a list of strings *)
let strings_of_ints = map string_of_int

let rec fold_right (f : 'a -> 'b -> 'b) (z : 'b) (xs : 'a list) : 'b =
  match xs with
  | [] -> z
  | x :: xs' -> f x (fold_right f z xs')

(** Trick:
    fold_right f z (x1 :: (x2 :: (x3 :: [])))
                 = (f x1  (f x2  (f x3  z))) **)

let keep_positive =
  fold_right (fun x res -> if x > 0 then x :: res else res)
             []

(** The standard list library has map and fold_right *)

let _ = List.map

(** But it has a slightly different type *)

let add_to_list n xs = List.map xs ~f:((+) n)
let add_to_list n xs = List.map ~f:((+) n) xs
let add_to_list n = List.map ~f:((+) n)

let _ = List.fold_right

let sum = List.fold_right ~f:(+) ~init:0
let prod = List.fold_right ~init:1 ~f:( * )

let keep_positive =
  List.fold_right ~init:[]
                  ~f:fun x res -> if x > 0 then x :: res else res

(** For more:
  https://ocaml.janestreet.com/ocaml-core/latest/doc/core/#Core_list  **)
