(* Lecture 1: 2014-01-28 -- Intro to OCaml *)

5 + 7;;

2 + 5 * 8;;

6;;

"hello";;

"hello" ^ "world";;

"hello" + "world";;

"CS" ^ 51;;

"CS" ^ string_of_int 51;;

string_of_int;;

let favorite_dept = "CS";;

favorite_dept;;

let favorite_course = favorite_dept ^ string_of_int 51;;

favorite_course;;

(* To convert a Celsius temperature to a Fahrenheit temperature *)
let f_of_c celsius = 9 / 5 * celsius + 32;;

f_of_c 0;;

f_of_c 100;;

9 / 5 * 100 + 32;;

9.0;;

9.0 / 5.0;;

9.0 /. 5.0;;

(* To convert a Celsius temperature to a Fahrenheit temperature *)
let f_of_c celsius = 9. /. 5. *. celsius +. 32.;;

f_of_c 0.;;

f_of_c 100.;;

