(* Lecture 2, notes for intro to OCaml, continued, 2014-01-30 *)

(* Two 2-D points *)
let p = (3., 4.)
let q = (5., 5.)

;;
let (x, y) = p in x +. y

(* Square a float *)
let square x = x *. x

(* The distance between two cartesian points *)
let distance p1 p2 =
  let (x1, y1) = p1 in
  let (x2, y2) = p2 in
    sqrt (square (x1 -. x2) +. square (y1 -. y2))

;;
distance (0., 0.) p

;;
distance (1., 3.) (-4., 15.)

(* The distance between two cartesian points *)
let distance (x1, y1) (x2, y2) =
  sqrt (square (x1 -. x2) +. square (y1 -. y2))

(* The slope between two cartesian points, returned as an option. Returns
 * None if the slope is infinite. *)
let slope (x1, y1) (x2, y2) =
  if x2 -. x1 = 0.0
  then None
  else Some ((y2 -. y1) /. (x2 -. x1))

(* Double a slope (represented as a float option) *)
let double_slope a_slope =
  match a_slope with
  | None -> None
  | Some f -> Some (2.0 *. f)

assert( double_slope None = None )
assert( double_slope (Some 3.3) = Some 6.6 )

;;
double_slope (slope p q)

;;
double_slope (slope p p)

let cs51_staff = ["Henry"; "Jesse"; "Rob"; "Ben"]

let cs51_staff = "Henry" :: "Jesse" :: "Rob" :: "Ben" :: []

let cs51_staff = "Henry" :: ("Jesse" :: ("Rob" :: ("Ben" :: [])))

(* To sum a list of ints *)
let rec sum_list (xs : int list) : int =
  match xs with
  | [] -> 0
  | x::xs' -> x + sum_list xs'

assert( sum_list [] = 0 )
assert( sum_list [2] = 2 )
assert( sum_list [2; 3; 5] = 10 )

(* To square a list of ints *)
let rec square_list (xs : int list) : int list =
  match xs with
  | [] -> []
  | x::xs' -> x * x :: square_list xs'

assert( square_list [] = [] )
assert( square_list [2] = [4] )
assert( square_list [2;3;4] = [4;9;16] )

;;
square_list [0; 1; 2; 3]

(* To multiply the ints in each pair of an (int * int) list *)
let rec prods_list (xys : int list) : int =
  match xys with
  | [] -> []
  | (x, y) :: xs' -> x * y :: prods_list xs'

assert( prods_list [] = [] )
assert( prods_list [(2, 3); (5, 7)] = [6; 35] )
<<<<<<< HEAD

(* To zip two lists into a list of pairs. Return None if the lists have
   different lengths *)
let rec zip_lists (xs : int list) (ys : int list) : (int * int) list option =
  let cons_option z zso =
    match zso with
    | None -> None
    | Some zs -> Some (z :: zs) in
  match xs, ys with
  | [], [] -> Some []
  | x::xs', y::ys' -> cons_option (x, y) (zip_lists xs' ys')
  | _, _ -> None

assert( zip_lists [] [] = Some [] )
assert( zip_lists [] [2] = None )
assert( zip_lists [3] [] = None )
assert( zip_lists [3;4;5] [12;10;8] = Some [(3,12); (4,10); (5,8)] )
assert( zip_lists [3;4;5] [12;10] = None )

;;
zip_lists [] []
;;
zip_lists [1; 2] [3; 4]
;;
zip_lists [1; 2] [3]

(* A record type for CS51 staff *)
type programmer = { name : string; age : int; fav_lang : string }

let alice = { name = "Alice"; age = 32; fav_lang = "OCaml" }
let bob   = { name = "Bob"; age = 45; fav_lang = "LISP" }
let carol = { name = "Carol"; age = 60; fav_lang = "C" }

(* To find the difference in two programmers' ages *)
let age_diff p1 p2 =
  let { name = n1; age = a1; fav_lang = f1 } = p1 in
  let { name = n2; age = a2; fav_lang = f2 } = p2 in
    a1 - a2

let age_diff { age = a1 } { age = a2 } = a1 - a2

let age_diff p1 p2 = p1.age - p2.age

assert( age_diff alice bob = -13 )
assert( age_diff carol bob = 15 )

(* Insert a programmer into a list sorted by age *)
let rec insert_programmer x xs =
  match xs with
  | [] -> [x]
  | x'::xs' ->
     if x.age <= x'.age
     then x :: xs
     else x' :: insert_programmer x xs'

assert( insert_programmer alice [] = [alice] )
assert( insert_programmer alice [bob; carol] = [alice; bob; carol] )
assert( insert_programmer bob [alice; carol] = [alice; bob; carol] )
assert( insert_programmer carol [alice; bob] = [alice; bob; carol] )

(* Sort a list of programmers by age *)
let rec sort_programmers xs =
  match xs with
  | [] -> []
  | x::xs' -> insert_programmer x (sort_programmers xs')

assert( sort_programmers [] = [] )
assert( sort_programmers [bob; carol; alice] = [alice; bob; carol] )
assert( sort_programmers [alice; bob; carol] = [alice; bob; carol] )

type shape = Circle of float
           | Rectangle of float * float
           | Triangle of float * float * float

let pi = 4. *. atan 1.

let area_of_shape a_shape =
  match a_shape with
  | Circle radius -> pi *. square radius
  | Rectangle (width, height) -> width *. height
  | Triangle (a, b, c) ->
     let p = (a +. b +. c) /. 2.0 in
     sqrt (p *. (p -. a) *. (p -. b) *. (p -. c))

assert( area_of_shape (Circle 2.) = 4. *. pi )
assert( area_of_shape (Rectangle (4., 5.)) = 20. )
assert( area_of_shape (Triangle (3., 4., 5.)) = 6. )

let scale_shape factor a_shape =
  match a_shape with
  | Circle radius -> Circle (factor *. radius)
  | Rectangle (width, height) -> Rectangle (factor *. width, factor *. height)
  | Triangle (a, b, c) -> Triangle (factor *. a, factor *. b, factor *. c)

assert( scale_shape 3. (Circle 2.) = Circle 6. )
assert( scale_shape 3. (Rectangle (4., 5.)) = Rectangle (12., 15.) )
assert( scale_shape 3. (Triangle (3., 4., 5.)) = Triangle (9., 12., 15.) )

(* A list of ints *)
type int_list = Null | Cons of int * int_list

(* Like an int option *)
type int_maybe = Nothing | Just of int

(* A color *)
type color = Red | Green | Blue

(* Several types of polymorphic values *)
type value =
    Int of int
  | Str of string
  | Bool of bool
  | Pair of value * value

(* Some example values *)
let v1 = Int 5
let v2 = Str "hello"
let v3 = Pair (v1, v2)
let v4 = Pair (Pair (v3, Bool true),
	       Pair (Int 7, Str "world"))

(* To sum all the ints in a value *)
let rec sum_ints v =
  match v with
  | Int z -> z
  | Str _ -> 0
  | Bool _ -> 0
  | Pair (v1, v2) -> sum_ints v1 + sum_ints v2

assert( sum_ints (Int 7) = 7 )
assert( sum_ints (Str "hi") = 0 )
assert( sum_ints v4 = 12 )

;;
sum_ints v4

(* To gather all the strings in a value as a string list *)
let rec gather_strings v =
  match v with
  | Int _ -> []
  | Str s -> [s]
  | Bool _ -> []
  | Pair (v1, v2) -> gather_strings v1 @ gather_strings v2

assert( gather_strings (Int 8) = [] )
assert( gather_strings (Str "hi") = ["hi"] )
assert( gather_strings v4 = ["hello"; "world"] )

;;
gather_strings v4

let rec string_of_value v =
  match v with
  | Int z -> string_of_int z
  | Str s -> s
  | Bool b -> string_of_bool b
  | Pair (v1, v2) -> "(" ^ string_of_value v1 ^ ", "
                     ^ string_of_value v2 ^ ")"

assert( string_of_value v3 = "(5, hello)" )
assert( string_of_value v4 = "(((5, hello), true), (7, world))" )

;;
string_of_value v3
;;
string_of_value v4
