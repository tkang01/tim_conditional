\documentclass[aspectratio=169]{beamer}

\RequirePackage{ucs}
\usepackage[quiet]{fontspec}
\usepackage{xunicode}
\usepackage{xltxtra}
\usepackage{graphicx}
\usepackage{forloop}
\usepackage{stmaryrd}
\usepackage{tikz}
\usepackage{mathtools}
\usepackage{fancyvrb}
\usepackage{listings}
\usepackage{amsmath,amssymb,amsthm}

\usetikzlibrary{chains,decorations.pathmorphing,positioning,fit}
\usetikzlibrary{decorations.shapes,calc,backgrounds}
\usetikzlibrary{decorations.text,matrix}
\usetikzlibrary{arrows,shapes.geometric,shapes.symbols,scopes}

\newcommand\bigO[1]{\ensuremath{\mathcal{O}(#1)}}
\newcommand\fun[1]{\ensuremath{\lambda #1.\,}}
\renewcommand\T[1]{\ensuremath{T_{\mathit{#1}}}}

%% %%% SETTINGS

\frenchspacing
\unitlength=0.01\textwidth
\thicklines
\urlstyle{sf}
\graphicspath{{images/}}

%%% VERBATIM SETUP

\DefineVerbatimEnvironment
  {code}{Verbatim}
  {commandchars=\\\‹\›,
   commentchar=\%,
   codes={\catcode`$=3\relax},
   baselinestretch=.8,
   formatcom=\relax}
\renewcommand\FancyVerbFormatLine[1]{#1\strut}
\fvset{commandchars=\\\‹\›,
       codes={\catcode`$=3\relax},
       formatcom=\relax}
\newcommand\exponent[1]{^{#1}}
\DefineShortVerb{\^}

\newcommand\ALT[3]{\alt<#1>{#2}{#3}}

%%% COLOR AND BEAMER SETUP

\setbeamercolor{frametitle}{fg=black}
\setbeamercolor{normal text}{fg=black}

\defaultfontfeatures{
    Mapping=tex-text,
    Scale=MatchLowercase,
}
\setmainfont{TeX Gyre Schola}
\setmonofont{Monaco}

\usefonttheme{serif}

\setbeamertemplate{frametitle}
  {\begin{centering}\medskip
   \insertframetitle\par
   \end{centering}}

\setbeamertemplate{itemize item}{$\bullet$}

\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[text line]{%
    \hfill\strut{%
        \small\color{black!75}%
        \texttt{\insertframenumber:\insertframeslidenumber}%
    }%
    \hfill%
}

\makeatletter
\newcount\frameslidetemp
\newcommand\insertframeslidenumber{%
  \frameslidetemp\c@page\relax
  \advance\frameslidetemp-\beamer@startpageofframe\relax
  \advance\frameslidetemp1\relax
  \the\frameslidetemp
}
\makeatother

\newcommand\EmacsFrame{
  \begin{frame}{\relax}
    \thispagestyle{empty}
    \begin{center}
      \emph{--- To Emacs ---}
    \end{center}
  \end{frame}
}

%%% GENERALLY USEFUL MACROS

\newcommand<>\always[1]{#1}

\def\defgbox#1#2{%
  \expandafter\global\expandafter\newbox\csname#2box\endcsname%
  \expandafter\global\expandafter\def\csname#2\endcsname{%
    \expandafter\usebox\csname#2box\endcsname%
  }%
  \expandafter\global\expandafter\setbox\csname#2box\endcsname=#1%
}
\def\defhbox{\defgbox\hbox}
\def\defvbox{\defgbox\vbox}

\def\setgbox#1#2{%
  \expandafter\setbox\csname#2box\endcsname=#1%
}
\def\sethbox{\setgbox\hbox}
\def\setvbox{\setgbox\vbox}

%%%
%%% TIKZ
%%%

\newcommand\K[1]{\textcolor{orange!70!black}{#1}}
\newcommand\TY[1]{\textcolor{blue!70!black}{#1}}
\newcommand\REF{\textcolor{green!50!black}{ref}}

\newcommand\place[3][]{%
  \tikz[orp] \node[#1] at (#2) {#3};%
}

\newcommand\anchor[1]{%
  \tikz[orp] \coordinate (#1);%
}

\newcommand\nil[1]{
  \draw[nil] (#1.north east) -- (#1.south west)
}

\newcommand\cons[4][]{
  \node[cons,#1] (#2) {#3\kern3pt\anchor{#2 center}\kern3pt#4};
  \draw[thick] (#2 center |- #2.north) -- (#2 center |- #2.south)
}

\newcommand\genpointer[3][draw=blue!50!black,opacity=.5]{%
  \begin{tikzpicture}[rem,solid,#1]
    \node(pointer start) [circle, draw, inner sep=1.37pt] {};
    \draw[->, overlay] (pointer start) edge[bend right=#2] (#3);
  \end{tikzpicture}%
}

\newcommand\hpointer[2][30]{%
  \genpointer[draw=red!70!black]{#1}{#2}%
}

\newcommand\ppointer[2][30]{%
  \genpointer{#1}{#2}%
}

\newcommand\pointer[2][30]{%
  \ifhighlit\let\pointertemp\hpointer\else\let\pointertemp\ppointer\fi
  \pointertemp[#1]{#2}%
}


\newcount\reducetemp
\newcommand\reduce[3]{%
  \reducetemp#1\relax
  \advance\reducetemp -1\relax
  \alt<#1->{#3}{\alt<\the\reducetemp>{\highlight{#2}}{#2}}%
}

\tikzset{
  orp/.style={
    overlay,
    remember picture,
  },
  rem/.style={
    remember picture,
  },
  every node/.style={
    very thick,
    inner sep=3pt,
  },
  every path/.style={very thick},
  ref/.style={
    draw=red,
    densely dotted,
    font=\ttfamily,
  },
  cons/.style={
    draw=black,
    font=\ttfamily,
  },
  nil/.style={
    black!80!white,
    shorten >=2pt,
    shorten <=2pt,
  },
  highlight/.style={
    color=yellow!50!white!90!black,
    thick,
    draw,
    draw opacity=0.5,
    outer color=yellow!50,
    inner color=yellow!30,
    rounded corners=2pt,
  },
}

\newif\ifhighlit
\highlitfalse

\newcommand<>\highlight[2][\relax]{%
  \ifx#1\relax
    \begin{tikzpicture}
  \else
    \begin{tikzpicture}[remember picture]
  \fi
    \node[
      inner sep=0pt,
      outer sep=0pt,
      text depth=0pt,
      align=left,
    ] (highlight temp) {%
        \only#3{\highlittrue}%
        #2%
        \only#3{\highlitfalse}%
    };
    \begin{scope}[on background layer]
      \coordinate (highlight temp 1) at
        ($(highlight temp.north east) + (2pt,2pt)$);
      \coordinate (highlight temp 2) at
        ($(highlight temp.base west) + (-2pt,-3pt)$);
      \node#3 [
        overlay,
        highlight,
        inner sep=0pt,
        fit=(highlight temp 1) (highlight temp 2),
      ] {};
    \end{scope}
    \ifx#1\relax
      \relax
    \else
      \node (#1) [
        overlay,
        inner sep=0pt,
        fit=(highlight temp 1) (highlight temp 2),
      ] {};
    \fi
  \end{tikzpicture}%
}

\newcommand\HI[2]{\highlight<#1>{#2}}

\newcommand\huncover[3][\relax]{%
  \uncover<#2->{\highlight<#2>[#1]{#3}}%
}
\newcommand\honly[3][\relax]{%
  \only<#2->{\highlight<#2>[#1]{#3}}%
}

\def\S#1#2{#2<#1>}

\begin{document}

%%%
%%% TITLE PAGE
%%%

\begin{frame}[t,fragile]\relax
  \thispagestyle{empty}
  \vfill
  \begin{center}
  {\Huge Objects, Part 2}
  \par {\Large (and maybe some events, too)}

  \bigskip
  \Large
  CS 51 and CSCI E-51

  \medskip
  April 1, 2014

  \vfill
  \small
  To install the graphics library on your appliance:
  \par\smallskip
  \color{red!66!black}
  ^curl -s http://sites.fas.harvard.edu/~cs51/graphics.sh | sh^
  \end{center}

  %% Need to initialize Tikz opacity inside a frame, not outside:
  \begin{tikzpicture}[opacity=0,overlay]\end{tikzpicture}
\end{frame}

\begin{frame}{A note on paradigms}
  \begin{center}
    What do people mean when they say that a
    \\
    \{ \emph{language}, \emph{program}, \emph{API},
       \emph{data structure},
       \emph{programming style} \}
    \\
    is
    \\
    \{ \emph{functional}, \emph{object oriented}, \emph{imperative},
      \emph{declarative},\,\ldots{} \}?
    \par
    \vspace{1cm}\pause
    It’s about style.
  \end{center}
\end{frame}

\begin{frame}{Some possibilities}
  \begin{tabular}{ll}
    \color{blue!70!black}functional & might mean “higher-order” or “purely
                      functional” or both \\
    \pause
    \color{blue!70!black}purely functional & no side effects \\
    \pause
    \color{blue!70!black}imperative & lots of side effects \\
    \pause
    \color{blue!70!black}higher-order & computations are values \\
    \pause
    \color{blue!70!black}object oriented & objects encapsulate state and behavior
                 \pause (sounds higher-order!) \\
    \pause
    \color{blue!70!black}procedural & computation is defined separately
                                      from data, maybe \\
    \pause
    \color{blue!70!black}declarative & nobody knows
  \end{tabular}
  \par\pause\bigskip
  Which of these styles are compatible?
  \par\pause\bigskip
  Which of these styles does OCaml support?
\end{frame}

\begin{frame}[fragile]{The “O” in OCaml}
\begin{code}
\K‹class› \K‹type› \HI‹2›‹display_elt› =
\K‹object›
  \HI‹3›‹\K‹val› \K‹mutable› pos  : point›
  \HI‹4›‹\K‹method› draw      : unit›
  \HI‹5›‹\K‹method› get_color : color›
  \HI‹6›‹\K‹method› set_color : color -> unit›
  \HI‹7›‹\K‹method› translate : dx:int -> dy:int -> unit›
\K‹end›
\end{code}
\begin{overprint}
\onslide<8-15>
\begin{code}
\K‹class› \HI‹8›‹rect› \HI‹9›‹(ll : point) (width : int) (height : int)› : \HI‹10›‹display_elt› =
\K‹object›
  \HI‹11›‹\K‹val› \K‹mutable› pos = ll›
  \HI‹11›‹\K‹val› \K‹mutable› color = Graphics.black›

  \HI‹12›‹\K‹method› get_color = color›
  \HI‹13›‹\K‹method› set_color c = color <- c›
  \HI‹14›‹\K‹method› translate ~dx ~dy = pos <- translate_point pos ~dx ~dy›
  \HI‹15›‹\K‹method› draw = Graphics.set_color color;›
                \HI‹15›‹Graphics.fill_rect pos.x pos.y width height›
\K‹end›
\end{code}
\onslide<16-17>
\begin{code}
\K‹class› \HI‹17›‹circ› (center : point) \HI‹17›‹(radius : int)› : ‹display_elt› =
\K‹object›
  ‹\K‹val› \K‹mutable› pos = center›
  ‹\K‹val› \K‹mutable› color = Graphics.black›

  ‹\K‹method› get_color = color›
  ‹\K‹method› set_color c = color <- c›
  ‹\K‹method› translate ~dx ~dy = pos <- translate_point pos ~dx ~dy›
  ‹\K‹method› draw = Graphics.set_color color;›
                \HI‹17›‹Graphics.fill_circle pos.x pos.y radius›
\K‹end›
\end{code}
\onslide<18>
\begin{code}
\K‹class› ‹shape› ‹(p : point)› =
\K‹object›
  ‹\K‹val› \K‹mutable› pos = ‹p››
  ‹\K‹val› \K‹mutable› color = Graphics.black›

  ‹\K‹method› get_color = color›
  ‹\K‹method› set_color c = color <- c›
  ‹\K‹method› translate ~dx ~dy = pos <- translate_point pos ~dx ~dy›
\K‹end›
\end{code}
\onslide<19>
\begin{code}
\K‹class› ‹shape› ‹(p : point)› = \K‹object› $\ldots$ \K‹end›
\K‹end›
\end{code}
\onslide<20->
\begin{code}
\K‹class› ‹shape› \HI‹22›‹(p : point)› = \K‹object› $\ldots$ \K‹end›

\K‹class› rect \HI‹22›‹(ll : point)› (width : int) (height : int) : display_elt =
\K‹object›
  \HI‹21›‹\K‹inherit› shape \HI‹22›‹ll››
  \K‹method› draw = Graphics.set_color color;
                Graphics.fill_rect pos.x pos.y width height
\K‹end›
\end{code}
\end{overprint}
\end{frame}

\EmacsFrame

\begin{frame}[fragile]{Object subtyping}
  Subtyping allows you to use an object where a \emph{narrower} interace
  is expected.

  For example, if we define
  \begin{code}
  \K‹type› drawable = \K‹<› draw : unit \K‹>›
  \end{code}
  then
  \begin{code}
  circ   $≤$   drawable
  \end{code}
  \pause
  This implies that we can use a ^circ^ as a ^drawable^.
  \pause
  This is based on the structure:
  \begin{code}
  \K‹<› draw : unit;
    get_color : Graphics.color;
    set_color : Graphics.color -> unit;
    translate : dx:int -> dy:int -> unit \K‹>›   $≤$   \K‹<› draw : unit \K‹>›
  \end{code}
\end{frame}

\EmacsFrame

\begin{frame}[fragile]{Virtual classes (mixins)}
  Define functionality to add to future classes
  that depends on \emph{future} functionality:

  \def\percent{\%}

  \begin{code}
  \K‹class› \HI‹2›‹\K‹virtual›› greeter =
  \K‹object› \HI‹3›‹(self)›
    \K‹method› \HI‹4›‹\K‹virtual›› who : string
    \K‹method› greet = printf “Hello, \percent‹›s\textbackslash‹›n” \HI‹5›‹self\K‹#›who›
  \K‹end›
  \pause[6]
  \K‹class› cs51 =
  \K‹object›
    \HI‹7›‹\K‹inherit› greeter›
    \K‹method› who = “CS 51”
  \K‹end›

  \K‹class› csci_e51 =
  \K‹object›
    \HI‹7›‹\K‹inherit› greeter›
    \K‹method› who = “CSCI E-51”
  \K‹end›
  \end{code}
\end{frame}

\begin{frame}[fragile]{Another mixin example}
  \begin{code}
  \K‹class› \K‹virtual› erasable = \K‹object› (self)
    \K‹method› \K‹virtual› draw : unit
    \K‹method› \K‹virtual› get_color : Graphics.color
    \K‹method› \K‹virtual› set_color : Graphics.color -> unit

    \K‹method› erase =
      \K‹let› old_color = self\K‹#›get_color in
      self\K‹#›set_color Graphics.white;
      self\K‹#›draw;
      self\K‹#›set_color old_color
  \K‹end›
  \end{code}
  \begin{overprint}
  \onslide<2>
  \begin{code}
  \K‹class› rect (ll : point) (width : int) (height : int) = \K‹object›
    \K‹inherit› shape ll
    \K‹inherit› erasable
    \K‹method› draw = Graphics.set_color color;
                  Graphics.fill_rect pos.x pos.y width height
  \K‹end›
  \end{code}
  \onslide<3>
  \begin{code}
  \K‹class› erasable_rect (ll : point) (width : int) (height : int) = \K‹object›
    \K‹inherit› rect ll width height
    \K‹inherit› erasable
  \K‹end›
  \end{code}
  \end{overprint}
\end{frame}

\end{document}
