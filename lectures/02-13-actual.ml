(* Lecture 6: 02/13/2014 *)

(*
  Substitution example:

  let x = 5 in
  (let y = 12 in x + y) * (let x = x + 1 in x - 3)

  -->

  (let y = 12 in 5 + y) * (let x = 5 + 1 in x - 3)

  -->

  (5 + 12) * (let x = 5 + 1 in x - 3)

  -->

  17 * (let x = 5 + 1 in x - 3)

  -->

  17 * (let x = 6 in x - 3)

  -->

  17 * (6 - 3)

  -->

  17 * 3

  -->

  51

 *)

(* if0 e1 then e2 else e3 *)

(* if0 0 then e2 else e3  -->  e2 *)
(* if0 6 then e2 else e3  -->  e3 *)
(* if0 (fun x -> x) then e2 else e3  --- ERROR *)

open Core.Std

(** Adding conditionals and functions **)

type op    = Plus | Minus | Times
type var   = string

type expr  = ValE of value
           | OpE of expr * op * expr
           | LetE of var * expr * expr
           | VarE of var
           | If0E of expr * expr * expr
           | AppE of expr * expr

 and value = IntV of int
           | FunV of var * expr

(*
let rec p_expr (e : expr) =
  match e with
  | ValE v -> ... p_value v ...
  | OpE(e1, op, e2) -> ... p_expr e1 ... p_expr e2 ...
  | LetE(x, e1, e2) -> ... p_expr e1 ... p_expr e2 ...
  | VarE x -> ...
  | If0E(e1, e2, e3) -> ... p_expr e1 ... p_expr e2 ... p_expr e3 ...
  | AppE(e1, e2) -> ... p_expr e1 ... p_expr e2 ...

and p_value (v : value) =
  match v with
  | IntV z -> ...
  | FunV(x, e) -> ... p_expr e ...
                  *)

let intE (z : int) : expr = ValE (IntV z)

(* 3 *)
let three = intE 3

(* 3 + 1 *)
let four = OpE(intE 3, Plus, intE 1)

(* let two = 2 in two + (two + 1) *)
let five = LetE("two", intE 2,
                OpE(VarE "two", Plus,
                    OpE(VarE "two", Plus, intE 1)))

(* let square = fun x -> x * x in square (square 2) *)
let sixteen =
  LetE("square", ValE(FunV("x", OpE(VarE "x", Times, VarE "x"))),
       AppE(VarE "square", AppE(VarE "square", intE 2)))

(* let add = fun x -> fun y -> x + y in
   let inc = add 1 in
   let dec = add (-1) in
   dec (inc 51) *)
let fiftyone =
  LetE("add", ValE(FunV("x", ValE(FunV("y", OpE(VarE "x", Plus, VarE "y"))))),
       LetE("inc", AppE(VarE "add", intE 1),
            LetE("dec", AppE(VarE "add", intE (-1)),
                 AppE(VarE "dec", AppE(VarE "inc", intE 51)))))

(* let add = fun x -> fun y -> x + y in
   let inc = fun z -> add 1 z in
   let dec = add (-1) in
   dec (inc 51) *)
let fiftyone' =
  LetE("add", ValE(FunV("x", ValE(FunV("y", OpE(VarE "x", Plus, VarE "y"))))),
       LetE("inc", ValE(FunV("z", AppE(AppE(VarE "add", intE 1), VarE "z"))),
            LetE("dec", AppE(VarE "add", intE (-1)),
                 AppE(VarE "dec", AppE(VarE "inc", intE 51)))))

(* 3 + x -- Error! *)
let error_expr = OpE(intE 3, Plus, VarE "x")

exception UnboundVariable of var
exception TypeError

(* Evaluate an arithemetic operation on two values *)
let rec eval_op (v1 : value) (op : op) (v2 : value) : value =
  match op, v1, v2 with
  | Plus, IntV z1, IntV z2 -> IntV (z1 + z2)
  | Minus, IntV z1, IntV z2 -> IntV (z1 - z2)
  | Times, IntV z1, IntV z2 -> IntV (z1 * z2)
  | _ -> raise TypeError

(* Substitute a value for a variable in an expression *)
let substitute (v : value) (x : var) (e : expr) : expr =
  let rec subst e =
    match e with
    | ValE v -> ValE (subst_v v)
    | OpE(e1, op, e2) -> OpE(subst e1, op, subst e2)
    | LetE(y, e1, e2) ->
       let e1' = subst e1 in
       let e2' = if x = y then e2 else subst e2 in
       LetE(y, e1', e2')
    | VarE y -> if x = y then ValE v else VarE y
    | If0E(e1, e2, e3) -> If0E(subst e1, subst e2, subst e3)
    | AppE(e1, e2) -> AppE(subst e1, subst e2)
    and subst_v v =
      match v with
      | IntV z -> IntV z
      | FunV(y, e) -> if x = y then FunV(y, e)
                      else FunV(y, subst e)
  in subst e

(* Evaluate an expression *)
let rec eval : expr -> value = function
  | ValE v -> v
  | OpE(e1, op, e2) -> eval_op (eval e1) op (eval e2)
  | LetE(x, e1, e2) -> eval (substitute (eval e1) x e2)
  | VarE x -> raise (UnboundVariable x)
  | If0E(e1, e2, e3) ->
     (match eval e1 with
      | IntV z -> if z = 0 then eval e2 else eval e3
      | FunV(_, _) -> raise TypeError)
  | AppE(e1, e2) ->
      (match eval e1 with
       | IntV _ -> raise TypeError
       | FunV(x, e3) -> eval (substitute (eval e2) x e3))


assert( eval three = IntV 3 )
assert( eval four = IntV 4 )
assert( eval five = IntV 5 )

assert( eval sixteen = IntV 16 )
assert( eval fiftyone = IntV 51 )
assert( eval fiftyone' = IntV 51 )

assert( eval (LetE("x", intE 3,
                   LetE("x", intE 4,
                        VarE "x")))
             = IntV 4 )

assert( eval (LetE("x", intE 3,
                   LetE("y", intE 4,
                        VarE "x")))
             = IntV 3 )


type 'a tree = Leaf
             | Branch of 'a tree * 'a * 'a tree

let tree0 = Leaf

let tree1 = Branch(Leaf, 1, Leaf)

let tree2 = Branch(Branch(Leaf, 1, Leaf),
                   3,
                   Branch(Leaf, 5, Branch(Leaf, 7, Leaf)))

 (*
let rec p_tree (t : ... tree) =
  match t with
  | Leaf -> ...
  | Branch(tl, x, tr) -> ... p_tree tl ... p_tree tr ...
  *)

(* To sum the ints in an int tree *)
let rec sum_tree (t : int tree) : int =
  match t with
  | Leaf -> 0
  | Branch(tl, x, tr) -> x + sum_tree tl + sum_tree tr

assert( sum_tree tree0 = 0 )
assert( sum_tree tree1 = 1 )
assert( sum_tree tree2 = 16 )

(* To find the maximum int in an int tree *)
let rec max_tree (t : int tree) : int option  =
  let max_opt o1 o2 =
    match o1, o2 with
    | Some z1, Some z2 -> Some (max z1 z2)
    | None, Some z2 -> Some z2
    | Some z1, None -> Some z1
    | None, None -> None in
  match t with
  | Leaf -> None
  | Branch(tl, x, tr) -> max_opt (Some x) (max_opt (max_tree tl) (max_tree tr))

assert( max_tree tree0 = None )
assert( max_tree tree1 = Some 1 )
assert( max_tree tree2 = Some 7 )

let rec in_order_tree (t : 'a tree) : 'a list =
  match t with
  | Leaf -> []
  | Branch(tl, x, tr) -> in_order_tree tl @ x :: in_order_tree tr


let rec fold_tree (t : 'a tree) ~(init : 'b) ~(f : 'b -> 'a -> 'b -> 'b) : 'b =
  match t with
  | Leaf -> init
  | Branch(tl, x, tr) -> f (fold_tree tl ~init:init ~f:f)
                           x
                           (fold_tree tr ~init:init ~f:f)

let sum_tree (t : int tree) : int =
  fold_tree t ~init:0 ~f:(fun zl z zr -> zl + z + zr)

assert( sum_tree tree0 = 0 )
assert( sum_tree tree1 = 1 )
assert( sum_tree tree2 = 16 )

let in_order_tree : 'a tree -> 'a list =
  fold_tree ~init:[] ~f:(fun lstl x lstr -> lstl @ x :: lstr)

assert( in_order_tree tree2 = [1; 3; 5; 7] )

let max_tree : int tree -> int option =
  let max_opt o1 o2 =
    match o1, o2 with
    | Some z1, Some z2 -> Some (max z1 z2)
    | None, Some z2 -> Some z2
    | Some z1, None -> Some z1
    | None, None -> None in
  fold_tree ~init:None ~f:(fun ml z mr -> max_opt ml (max_opt (Some z) mr))

assert( max_tree tree2 = Some 7 )
assert( max_tree tree0 = None )
