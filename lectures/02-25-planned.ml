(* Lecture 8 (02/25): more abstract data types *)

open Core.Std

(*
 * Module types = signatures = interfaces
 * Modules = structs = implementations
 * Functors = functions from structs to structs
 *)

(* Rule of thumb: Use language mechanisms to enforce abstraction.
 * (Like all design rules, violate this when it sucks too much.)
 *)

module type QUEUE =
  sig
    type 'a t
    exception Empty
    val empty : 'a t
    val enqueue : 'a -> 'a t -> 'a t
    val is_empty : 'a t -> bool
    val dequeue : 'a t -> 'a t
    val front : 'a t -> 'a
  end

module ListQueue : QUEUE =
  struct
    type 'a t = 'a list
    exception Empty
    let empty = []
    let enqueue x q = q @ [x]
    let is_empty = List.is_empty
    let dequeue = function
      | [] -> raise Empty
      | _ :: q -> q
    let front = function
      | [] -> raise Empty
      | x :: _ -> x
  end

module BankersQueue : QUEUE =
  struct
    type 'a t = { front: 'a list; back: 'a list }
    (* q represents sequence q.front @ List.rev q.back *)

    exception Empty

    let empty = { front = []; back = [] }

    let enqueue x q = { q with back = x :: q.back }

    let is_empty = function
      | { front = []; back = [] } -> true
      | _ -> false

    let deq (q : 'a t) : 'a * 'a t =
      match q.front with
      | x :: xs -> (x, { q with front = xs })
      | [] -> match List.rev q.back with
              | x :: xs -> (x, { front = xs; back = [] })
              | [] -> raise Empty

    let dequeue q = snd (deq q)

    let front q = fst (deq q)
  end

(* Abstract containers (for abstract algorithms) *)
module type CONTAINER =
  sig
    type 'a t
    exception Empty
    val empty : 'a t
    val insert : 'a -> 'a t -> 'a t
    val is_empty : 'a t -> bool
    val remove_first : 'a t -> 'a t
    val first : 'a t -> 'a
  end

(** Matrices *)

(* Matrices are defined over rings, which offer a set of operations: *)
module type RING =
  sig
    type t
    val zero : t
    val one : t
    val add : t -> t -> t
    val mul : t -> t -> t
    (* ... *)
  end

module IntRing =
  struct
    type t = int
    let zero = 0
    let one = 1
    let add = (+)
    let mul = ( * )
  end

module FloatRing =
  struct
    type t = float
    let zero = 0.
    let one = 1.
    let add = (+.)
    let mul = ( *. )
  end

module BoolRing =
  struct
    type t = bool
    let zero = false
    let one = true
    let add = (||)
    let mul = (&&)
  end

(* A signature for matrices *)
module type MATRIX =
  sig
    type elt
    type t
    val of_list : elt list list -> t
    val add : t -> t -> t
    val mul : t -> t -> t
  end

(* Make a matrix over any ring *)
module DenseMatrix (R : RING) : MATRIX with type elt = R.t =
  struct
    type elt = R.t
    type t = elt list list
    let of_list rows = rows
    let add m1 m2 = List.map2_exn m1 m2
                                  ~f:(fun row1 row2 ->
                                      List.map2_exn row1 row2 ~f:R.add)
    let mul _ _ = failwith "not implemented"
  end

module DenseIntMatrix = DenseMatrix(IntRing)
module DenseFloatMatrix = DenseMatrix(FloatRing)
module DenseBoolMatrix = DenseMatrix(BoolRing)

