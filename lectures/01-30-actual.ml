(* Lecture 2: 01/30 *)

let p = (3.0, 4.0)
let q = (5.0, 5.0)

;;
let (x, y) = p in x +. y

(* To square a float *)
let square x = x *. x

(* The distance between two cartesian points *)
let distance p1 p2 =
  let (x1, y1) = p1 in
  let (x2, y2) = p2 in
    sqrt (square (x1 -. x2) +. square (y1 -. y2))

assert( distance (0., 0.) (3., 4.) = 5. )
assert( distance (1., 3.) (-4., 15.) = 13. )

let distance (x1, y1) (x2, y2) =
  sqrt (square (x1 -. x2) +. square (y1 -. y2))

assert( distance (0., 0.) (3., 4.) = 5. )
assert( distance (1., 3.) (-4., 15.) = 13. )

(* The slope between two cartesian points. Return Some
 * m when the slope is (finite) m, and None otherwise. *)
let slope (x1, y1) (x2, y2) =
  if x1 -. x2 = 0.0
  then None
  else Some ((y2 -. y1) /. (x2 -. x1))

assert( slope (2., 0.) (4., 0.) = Some 0. )
assert( slope (0., 0.) (3., 3.) = Some 1. )
assert( slope (1., -2.) (5., 0.) = Some 0.5 )
assert( slope (2., 5.) (2., 8.) = None )

(* To double a (possible None) slope *)
let double_slope (a_slope : float option) : float option =
  match a_slope with
  | Some f -> Some (2.0 *. f)
  | None -> None

assert( double_slope (Some 2.0) = Some 4.0 )
assert( double_slope None = None )
assert( double_slope (slope (0., 0.) (4., 8.)) = Some 4.0 )

let cs51_staff = ["Henry"; "Jesse"; "Mike"; "Rob"]

let cs51_staff = "Henry" :: ("Jesse" :: ("Mike" :: ("Rob" :: [])))

let cs51_staff = "Henry" :: "Jesse" :: "Mike" :: "Rob" :: []

(* A string list is either:
   - []
   - x :: xs where x is a string and xs is a string list
 *)

(*
 * DESIGN RECIPE
 *
 * 1. Thing about the problem and the data. (Define types.)
 * 2. Function header (name it, purpose statement)
 * 3. Examples.
 * 4. Pick a strategy (domain knowledge, composition, structural)
 * 5. Code.
 * 6. Tests (from examples)
 *)

(* TEMPLATE FOR PROCESSING LISTS

let rec process_list (xs : int list) : int =
  match xs with
  | [] -> ...
  | x :: xs' -> ... x ... process_list xs' ...

 *)

(* To sum a list of integers *)
let rec sum_list (xs : int list) : int =
  match xs with
  | [] -> 0
  | x :: xs' -> x + sum_list xs'

assert( sum_list [] = 0 )
assert( sum_list [3] = 3 )
assert( sum_list [2; 3; 5] = 10 )

(* To square each element of a list of ints *)
let rec square_list (xs : int list) : int list =
  match xs with
  | [] -> []
  | x :: xs' -> x * x :: square_list xs'

assert( square_list [] = [] )
assert( square_list [2] = [4] )
assert( square_list [3; 5; 7] = [9; 25; 49] )

(* To multiply each pair of ints in a list *)
let rec prods_list (xys : (int * int) list) : int list =
  match xys with
  | [] -> []
  | (x, y) :: xys' -> x * y :: prods_list xys'

assert( prods_list [] = [] )
assert( prods_list [(2,3)] = [6] )
assert( prods_list [(2,3); (4,5); (6,7)] = [6; 20; 42] )

