\documentclass{beamer}

\RequirePackage{ucs}
\usepackage[quiet]{fontspec}
\usepackage{xunicode}
\usepackage{xltxtra}
\usepackage{graphicx}
\usepackage{forloop}
\usepackage{stmaryrd}
\usepackage{tikz}
\usepackage{mathtools}
\usepackage{fancyvrb}
\usepackage{listings}
\usepackage{amsmath,amssymb,amsthm}
\usepackage{cancel}

\usetikzlibrary{chains,decorations.pathmorphing,positioning,fit}
\usetikzlibrary{decorations.shapes,calc,backgrounds}
\usetikzlibrary{decorations.text,matrix}
\usetikzlibrary{arrows,shapes.geometric,shapes.symbols,scopes}

\newcommand\bigO[1]{\ensuremath{\mathcal{O}(#1)}}
\newcommand\fun[1]{\ensuremath{\lambda #1.\,}}
\renewcommand\T[1]{\ensuremath{T_{\mathit{#1}}}}

%% %%% SETTINGS

\frenchspacing
\unitlength=0.01\textwidth
\thicklines
\urlstyle{sf}
\graphicspath{{images/}}

%%% VERBATIM SETUP

\DefineVerbatimEnvironment
  {code}{Verbatim}
  {commandchars=\\\‹\›,
   commentchar=\%,
   codes={\catcode`$=3\relax},
   baselinestretch=.8,
   formatcom=\relax}
\renewcommand\FancyVerbFormatLine[1]{#1\strut}
\fvset{commandchars=\\\‹\›,
       codes={\catcode`$=3\relax},
       formatcom=\relax}
\newcommand\exponent[1]{^{#1}}
\DefineShortVerb{\^}

\newcommand\ALT[3]{\alt<#1>{#2}{#3}}
\newcommand\UNCOVER[2]{\uncover<#1>{#2}}

%%% COLOR AND BEAMER SETUP

\setbeamercolor{frametitle}{fg=black}
\setbeamercolor{normal text}{fg=black}

\defaultfontfeatures{
    Mapping=tex-text,
    Scale=MatchLowercase,
}
\setmainfont{TeX Gyre Schola}
\setmonofont{Monaco}

\usefonttheme{serif}

\setbeamertemplate{frametitle}
  {\begin{centering}\medskip
   \insertframetitle\par
   \end{centering}}

\setbeamertemplate{itemize item}{$\bullet$}

\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[text line]{%
    \hfill\strut{%
        \small\color{black!75}%
        \texttt{\insertframenumber:\insertframeslidenumber}%
    }%
    \hfill%
}

\makeatletter
\newcount\frameslidetemp
\newcommand\insertframeslidenumber{%
  \frameslidetemp\c@page\relax
  \advance\frameslidetemp-\beamer@startpageofframe\relax
  \advance\frameslidetemp1\relax
  \the\frameslidetemp
}
\makeatother

\newcommand\CenterFrame[1]{
  \begin{frame}{\relax}
    \thispagestyle{empty}
    \begin{center}
      \emph{#1}
    \end{center}
  \end{frame}
}


\newcommand\EmacsFrame{
  \CenterFrame{--- To Emacs---}
}

%%% GENERALLY USEFUL MACROS

\newcommand<>\always[1]{#1}

\def\defgbox#1#2{%
  \expandafter\global\expandafter\newbox\csname#2box\endcsname%
  \expandafter\global\expandafter\def\csname#2\endcsname{%
    \expandafter\usebox\csname#2box\endcsname%
  }%
  \expandafter\global\expandafter\setbox\csname#2box\endcsname=#1%
}
\def\defhbox{\defgbox\hbox}
\def\defvbox{\defgbox\vbox}

\def\setgbox#1#2{%
  \expandafter\setbox\csname#2box\endcsname=#1%
}
\def\sethbox{\setgbox\hbox}
\def\setvbox{\setgbox\vbox}

%%%
%%% TIKZ
%%%

\newcommand\K[1]{\textcolor{orange!70!black}{#1}}
\newcommand\TY[1]{\textcolor{blue!70!black}{#1}}
\newcommand\REF{\textcolor{green!50!black}{ref}}

\newcommand\place[3][]{%
  \tikz[orp] \node[#1] at (#2) {#3};%
}

\newcommand\anchor[1]{%
  \tikz[orp] \coordinate (#1);%
}

\newcommand\nil[1]{
  \draw[nil] (#1.north east) -- (#1.south west)
}

\newcommand\cons[4][]{
  \node[cons,#1] (#2) {#3\kern3pt\anchor{#2 center}\kern3pt#4};
  \draw[thick] (#2 center |- #2.north) -- (#2 center |- #2.south)
}

\newcommand\genpointer[3][draw=blue!50!black,opacity=.5]{%
  \begin{tikzpicture}[rem,solid,#1]
    \node(pointer start) [circle, draw, inner sep=1.37pt] {};
    \draw[->, overlay] (pointer start) edge[bend right=#2] (#3);
  \end{tikzpicture}%
}

\newcommand\hpointer[2][30]{%
  \genpointer[draw=red!70!black]{#1}{#2}%
}

\newcommand\ppointer[2][30]{%
  \genpointer{#1}{#2}%
}

\newcommand\pointer[2][30]{%
  \ifhighlit\let\pointertemp\hpointer\else\let\pointertemp\ppointer\fi
  \pointertemp[#1]{#2}%
}


\newcount\reducetemp
\newcommand\reduce[3]{%
  \reducetemp#1\relax
  \advance\reducetemp -1\relax
  \alt<#1->{#3}{\alt<\the\reducetemp>{\highlight{#2}}{#2}}%
}

\tikzset{
  orp/.style={
    overlay,
    remember picture,
  },
  rem/.style={
    remember picture,
  },
  every node/.style={
    very thick,
    inner sep=3pt,
  },
  every path/.style={very thick},
  ref/.style={
    draw=red,
    densely dotted,
    font=\ttfamily,
  },
  cons/.style={
    draw=black,
    font=\ttfamily,
  },
  nil/.style={
    black!80!white,
    shorten >=2pt,
    shorten <=2pt,
  },
  highlight/.style={
    color=yellow!50!white!90!black,
    thick,
    draw,
    draw opacity=0.5,
    outer color=yellow!50,
    inner color=yellow!30,
    rounded corners=2pt,
  },
  higherlight/.style={
    highlight,
    outer color=green,
  },
}

\newif\ifhighlit
\highlitfalse

\newcommand<>\highlight[2][\relax]{%
  \ifx#1\relax
    \begin{tikzpicture}
  \else
    \begin{tikzpicture}[remember picture]
  \fi
    \node[
      inner sep=0pt,
      outer sep=0pt,
      text depth=0pt,
      align=left,
    ] (highlight temp) {%
        \only#3{\highlittrue}%
        #2%
        \only#3{\highlitfalse}%
    };
    \begin{scope}[on background layer]
      \coordinate (highlight temp 1) at
        ($(highlight temp.north east) + (2pt,2pt)$);
      \coordinate (highlight temp 2) at
        ($(highlight temp.base west) + (-2pt,-3pt)$);
      \node#3 [
        overlay,
        highlight,
        inner sep=0pt,
        fit=(highlight temp 1) (highlight temp 2),
      ] {};
    \end{scope}
    \ifx#1\relax
      \relax
    \else
      \node (#1) [
        overlay,
        inner sep=0pt,
        fit=(highlight temp 1) (highlight temp 2),
      ] {};
    \fi
  \end{tikzpicture}%
}

\newcommand\HI[2]{\highlight<#1>{#2}}

\newcommand\huncover[3][\relax]{%
  \uncover<#2->{\highlight<#2>[#1]{#3}}%
}
\newcommand\honly[3][\relax]{%
  \only<#2->{\highlight<#2>[#1]{#3}}%
}

\def\S#1#2{#2<#1>}

\begin{document}

%%%
%%% TITLE PAGE
%%%

\begin{frame}[t,fragile]\relax
  \thispagestyle{empty}
  \vfill
  \begin{center}
  {\Huge Parallel Programming}
        \par
  \bigskip
  \Large
  CS 51 and CSCI E-51
        \par
  \medskip
  April 15, 2014
  \end{center}
        \par
  %% Need to initialize Tikz opacity inside a frame, not outside:
  \begin{tikzpicture}[opacity=0,overlay]\end{tikzpicture}
\end{frame}

%%% GREG SLIDES HERE (25 to end)

\end{document}
