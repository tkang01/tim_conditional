(* Lecture 3: 02/04 *)

let pi = 4. *. atan 1.

(* Record type for a programmer *)
type programmer = { name : string; age : int; fav_lang : string }
(* kind of like string * int * string *)

let alice = { name = "Alice"; age = 32; fav_lang = "OCaml" }
let bob   = { name = "Bob"; age = 45; fav_lang = "LISP" }
let carol = { name = "Carol"; age = 60; fav_lang = "C" }

(* To find the difference in age between two programmers *)
let age_diff (p1 : programmer) (p2 : programmer) : int =
  let { name = _; age = a1; fav_lang = _ } = p1 in
  let { name = _; age = a2; fav_lang = _ } = p2 in
  a1 - a2

(* To find the difference in age between two programmers *)
let age_diff ({ age = a1 } : programmer) ({ age = a2 } : programmer) : int =
  a1 - a2

(* To find the difference in age between two programmers *)
let age_diff (p1 : programmer) (p2 : programmer) =
  p1.age - p2.age

assert( age_diff carol bob = 15 )
assert( age_diff alice bob = -13 )

(* To insert a programmer into a list sorted by age *)
let rec insert_programmer (p : programmer) (ps : programmer list)
        : programmer list =
  match ps with
  | [] -> [p]
  | p' :: ps' ->
     if p.age <= p'.age
     then p :: ps
     else p' :: insert_programmer p ps'

assert( insert_programmer alice [] = [alice] )
assert( insert_programmer alice [bob; carol] = [alice; bob; carol] )
assert( insert_programmer bob [alice; carol] = [alice; bob; carol] )
assert( insert_programmer carol [alice; bob] = [alice; bob; carol] )

(* Sort a list of programmers by age *)
let rec sort_programmers (ps : programmer list) : programmer list =
  match ps with
  | [] -> []
  | p' :: ps' -> insert_programmer p' (sort_programmers ps')

assert( sort_programmers [] = [] )
assert( sort_programmers [alice] = [alice] )
assert( sort_programmers [bob; carol; alice] = [alice; bob; carol] )
assert( sort_programmers [bob; alice; carol] = [alice; bob; carol] )
assert( sort_programmers [alice; bob; carol] = [alice; bob; carol] )


(* A type to represent shapes *)
type shape = Circle of float
           | Rectangle of float * float
           | Triangle of float * float * float

let unit_circle = Circle 1.0
let rectangle4by3 = Rectangle (4.0, 3.0)
let triangle = Triangle (3., 4., 5.)

(* To find the area of a shape *)
let area_of_shape (s : shape) : float =
  match s with
  | Circle r -> pi *. r *. r
  | Rectangle (w, h) -> w *. h
  | Triangle (a, b, c) ->
     let p = (a +. b +. c) /. 2.0 in
     sqrt (p *. (p -. a) *. (p -. b) *. (p -. c))

assert( area_of_shape (Circle 2.) = 4. *. pi )
assert( area_of_shape (Rectangle (4., 5.)) = 20. )
assert( area_of_shape (Triangle (3., 4., 5.)) = 6. )

(* To scale a shape by a factor *)
let scale_shape (factor : float) (s : shape) : shape =
  match s with
  | Circle r -> Circle (factor *. r)
  | Rectangle (w, h) -> Rectangle (factor *. w, factor *. h)
  | Triangle (a, b, c) -> Triangle (factor *. a, factor *. b, factor *. c)

assert( scale_shape 3. (Circle 2.) = Circle 6. )
assert( scale_shape 5. (Rectangle (4., 5.)) = Rectangle (20., 25.))
assert( scale_shape 2. (Triangle (2., 3., 3.)) = Triangle (4., 6., 6.) )

type int_option = Nothing
                | Just of int

type int_list = Empty
              | Cons of int * int_list

type color = Red | Green | Blue


(* Several different kinds of values *)
type value = Int of int
           | Str of string
           | Bool of bool
           | Prog of programmer
           | Pair of value * value

let v1 = Int 5
let v2 = Str "hello"
let v3 = Pair (v1, v2)
let v4 = Pair (Pair (v3, Bool true),
               Pair (Int 7, Str "world"))

(* To sum all the ints in a value *)
let rec sum_ints (v : value) : int =
  match v with
  | Int z -> z
  | Str s -> 0
  | Bool b -> 0
  | Pair (v1, v2) -> sum_ints v1 + sum_ints v2

assert( sum_ints v1 = 5 )
assert( sum_ints v2 = 0 )
assert( sum_ints v3 = 5 )
assert( sum_ints v4 = 12 )

(* To gather all the strings in a value into a list *)
let rec gather_strings (v : value) : string list =
  match v with
  | Int _ -> []
  | Str s -> [s]
  | Bool _ -> []
  | Pair (v1, v2) -> gather_strings v1 @ gather_strings v2

assert( gather_strings v1 = [] )
assert( gather_strings v2 = ["hello"] )
assert( gather_strings v3 = ["hello"] )
assert( gather_strings v4 = ["hello"; "world"] )

(* To format a value as a string *)
let rec string_of_value (v : value) : string =
  match v with
  | Int z -> string_of_int z
  | Str s -> s
  | Bool b -> string_of_bool b
  | Pair (v1, v2) -> "(" ^ string_of_value v1 ^ ", " ^
                       string_of_value v2 ^ ")"

assert( string_of_value v1 = "5" )
assert( string_of_value v2 = "hello" )
assert( string_of_value v3 = "(5, hello)" )
assert( string_of_value v4 = "(((5, hello), true), (7, world))" )

(* let value_of_string = ... *)

(* To append two int lists *)
let rec append_int_list (xs : int list) (ys : int list) : int list =
  match xs with
  | [] -> ys
  | x :: xs' -> x :: append_int_list xs' ys
  
assert( append_int_list [] [] = [] )
assert( append_int_list [] [2; 4] = [2; 4] )
assert( append_int_list [3; 4; 5] [] = [3; 4; 5] )
assert( append_int_list (3 :: 4 :: 5 :: []) (1 :: 2 :: []) =
                         3 :: 4 :: 5 :: (1 :: 2 :: []) )

(* To append two string lists *)
let rec append_string_list (xs : string list) (ys : string list) : string list =
  match xs with
  | [] -> ys
  | x :: xs' -> x :: append_string_list xs' ys

assert( append_string_list [] [] = [] )
assert( append_string_list [] ["ab"; "de"] = ["ab"; "de"] )
assert( append_string_list ["meh"; "repetition"] ["ab"; "de"] =
                           ["meh"; "repetition"; "ab"; "de"] )

(* To append two string lists *)
let rec append_list (xs : 'a list) (ys : 'a list) : 'a list =
  match xs with
  | [] -> ys
  | x :: xs' -> x :: append_list xs' ys

assert( append_list [] [] = [] )
assert( append_list [] [2; 4] = [2; 4] )
assert( append_list [3; 4; 5] [] = [3; 4; 5] )
assert( append_list (3 :: 4 :: 5 :: []) (1 :: 2 :: []) =
                    3 :: 4 :: 5 :: (1 :: 2 :: []) )
assert( append_list [] [] = [] )
assert( append_list [] ["ab"; "de"] = ["ab"; "de"] )
assert( append_list ["meh"; "repetition"] ["ab"; "de"] =
                    ["meh"; "repetition"; "ab"; "de"] )



