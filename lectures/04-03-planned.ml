(* Lecture 17: Events *)

(** EXAMPLE: POLYMORPHIC CLASSES **)

module PolymorphicStack =
struct
  class type ['a] container =
  object
    method insert : 'a -> unit
    method take   : 'a option
    method size   : int
  end

  class ['a] stack : ['a] container =
  object
    val mutable contents = []

    method size = List.length contents
    method insert x = contents <- x :: contents
    method take = match contents with
      | [] -> None
      | x :: xs -> contents <- xs; Some x
  end

  class ['a] fast_size_stack : ['a] container =
  object
    inherit ['a] stack as super

    val mutable length = 0

    method! size = length
    method! insert x = length <- length + 1; super#insert x
    method! take = length <- max (length - 1) 0; super#take
  end

  let _ =
    let s = new fast_size_stack in
    s#insert 0;
    s#insert 2;
    s#insert 4;
    assert(s#size = 3);
    assert(s#take = Some 4);
    assert(s#take = Some 2);
    s#insert 17;
    assert(s#size = 2);
    assert(s#take = Some 17);
    assert(s#take = Some 0);
    assert(s#take = None);
    assert(s#size = 0)
end

(*** SLIDES ***)

(** DISPLAY ELEMENTS **)

(* These have evolved slightly from the previous lecture. *)

;;
#require "graphics"

module DisplayElt =
struct
  (* 2-D cartesian points *)
  type point = int * int

  open Graphics

  (* Class type for display elements. This is the interface that display
     elements present not only to clients, but to subclasses (in particular,
     the inheritance interface includes the mutable variable point) *)
  class type display_elt =
  object
    method get_color : color
    method set_color : color -> unit
    method get_pos   : point
    method set_pos   : point -> unit
    method translate : int -> int -> unit
    method draw      : unit
    method erase     : unit
  end

  class virtual shape ?(color : color = black) (pos : point) =
  object (self : #display_elt)
    val mutable pos   = pos
    val mutable color = color

    method virtual draw : unit

    method get_pos     = pos
    method set_pos p   = pos <- p
    method get_color   = color
    method set_color c = color <- c

    method translate dx dy =
      pos <- (fst pos + dx, snd pos + dy)

    method erase =
      let old_color = self#get_color in
      self#set_color white;
      self#draw;
      self#set_color old_color
  end

  class rect ?(color : color option)
             (ll : point) (width : int) (height : int) : display_elt =
  object
    inherit shape ?color ll

    method draw = set_color color;
                  fill_rect (fst pos) (snd pos) width height
  end

  class circ ?(color : color option)
             (ll : point) (radius : int) : display_elt =
  object
    inherit shape ?color ll

    method draw = set_color color;
                  fill_circle (fst pos + radius) (snd pos + radius) radius
  end

  class square ?color ll width = rect ?color ll width width

  class text ?(color : color option)
             (ll : point) (value : string) : display_elt =
  object
    inherit shape ?color ll

    method draw = moveto (fst pos) (snd pos);
                  set_color color;
                  draw_string value
  end

  class textbox ?(color : color option) ?(bg : color = white)
                (ll : point) (value : string) : display_elt =
  object (self)
    inherit text ?color ll value as super

    method draw = let (width, height) = text_size value in
                  (new rect ~color:white (self#get_pos) width height)#draw;
                  super#draw
  end
end


(** EVENTS **)

(* A interface for events *)
module type EVENT =
sig
  type listener_id

  class ['a] event :
  object
    (* Adds a listener function to the event, returning a
       unique listener_id that can be used to later remove the
       listener. The function will be invoked each time
       the event is fired. There is no guarantee about
       the order in which the listeners are invoked, so the
       client should not assume a particular order. *)
    method add_listener : ('a -> unit) -> listener_id

    (* Similar to add_listener, except the function is only
       called the first time the event is fired, and then
       removed. *)
    method add_one_shot : ('a -> unit) -> listener_id

    (* For adding listeners when we don't need the id for later removal. *)
    method add_listener_ : ('a -> unit) -> unit
    method add_one_shot_ : ('a -> unit) -> unit

    (* Removes a listener identified from the event. *)
    method remove_listener : listener_id -> unit

    (* Removes all listeners from the event. *)
    method clear_listeners : unit

    (* Signal that the event has occurred. *)
    method fire : 'a -> unit
  end
end

(* A simple implementation of events. (You are not expected to
   understand this--but I bet you can.) *)
module Event : EVENT =
struct
  (* We use physical equality on unit refs to identify unique listeners *)
  type listener_id = unit ref

  (* We represent events as a mutable list of waiters, where
     a waiter is an id and a function to invoke when the event
     is fired. *)
  type 'a waiter = { id : listener_id; action : 'a -> unit }

  class ['a] event =
  object (self)
    val mutable waiters : 'a waiter list = []

    (* Add a listener to the event *)
    method add_listener action =
      let id = ref () in
      waiters <- { id; action } :: waiters;
      id

    (* Remove a listener *)
    method remove_listener i =
      waiters <- List.filter waiters ~f:fun w -> not (phys_equal w.id i)

    (* Add a one-shot listener.  We basically add a function
     that removes itself. *)
    method add_one_shot f =
      let id = ref () in
      let action v = self#remove_listener id; f v in
      waiters <- { id; action } :: waiters;
      id

    method add_listener_ f = ignore (self#add_listener f)
    method add_one_shot_ f = ignore (self#add_one_shot f)

    method clear_listeners = waiters <- []

  (* Fire an event -- simply iterate over the waiters and invoke
     their functions on the given value. *)
    method fire v =
      List.iter waiters ~f:fun w -> w.action v
  end
end

(* Interface for handling user interaction in the Graphics library using
   events. *)
module type GRAPHICS_EVENTS =
sig
  class ['a] event : ['a] Event.event

  (* Some built-in events from the Graphics package: *)
  val key_pressed  : char event
  val button_down  : (int * int) event
  val button_up    : (int * int) event
  val mouse_motion : (int * int) event
  val clock        : unit event

  (* Clears out any listeners from the above Graphics events. *)
  val clear_listeners : unit -> unit

  (* Given an initializer function f, sets up the graphics
     frame, invokes the initializer f, and then starts the
     event loop, which listens for user input and translates
     input into event firings. *)
  val run : ?frame_rate:int -> (unit -> unit) -> unit

  (* Called from a listener, terminates the event loop and
     returns from run. *)
  val terminate : unit -> 'a
end

module GraphicsEvents : GRAPHICS_EVENTS = struct
  class ['a] event = ['a] Event.event

  (* Little hack so that I can use the same names for my
     events as Graphics does, and still get to them when
     the Graphics module is open. *)
  module E = struct
    (* Events for the primitive Graphics actions. *)
    let key_pressed : char event = new event
    let button_down : (int * int) event = new event
    let button_up : (int * int) event = new event
    let mouse_motion : (int * int) event = new event
    let clock : unit event = new event
  end

  include E

  let clear_listeners () =
    key_pressed#clear_listeners;
    button_down#clear_listeners;
    button_up#clear_listeners;
    mouse_motion#clear_listeners;
    clock#clear_listeners

  (* The Graphics event interface doesn't really provide the right
     functionality so that we can do a blocking operation and determine what
     event occurred. (For instance, we can't tell whether the mouse moved, or
     the mouse button was released.) So we use a polling approach with a
     little bit of auxilliary state to figure out what event happened, and the
     corresponding value. We then fire the appropriate high-level event. *)
  let mouse_btn_down = ref false
  let old_pos = ref (0,0)

  let read_event () =
    let open Graphics in
    (* poll for the current mouse state: *)
    let new_pos  = mouse_pos () in
    let new_down = button_down () in
    (* if mouse position changed, fire mouse_motion *)
    if new_pos <> !old_pos then
      (old_pos := new_pos;
       mouse_motion#fire new_pos);
    (* if keys were pressed, fire key_pressed for each *)
    while key_pressed () do E.key_pressed#fire (read_key ()) done;
    (* if the old state of the mouse button is different than the current
       state, fire button_down or button_up as appropriate *)
    if !mouse_btn_down <> new_down then
      (mouse_btn_down := new_down;
       let e = if new_down then E.button_down else E.button_up in
       e#fire new_pos);
    (* finally, fire the clock event *)
    clock#fire ()

  (* Helper for restarting interrupted system calls (OY) *)
  let rec restart f arg =
    try f arg
    with Unix.Unix_error (Unix.EINTR, _, _) -> restart f arg

  (* Our basic event loop just calls read_event, which fires the appropriate
     events, then synchronizes the shadow graphics buffer with the screen,
     and then loops again. *)
  let rec event_loop frame_rate =
    read_event ();
    Graphics.synchronize ();
    restart Thread.delay (1.0 /. Float.of_int frame_rate);
    event_loop frame_rate

  (* Users can break out of event loop by calling the terminate function *)
  exception Terminate
  let terminate () = raise Terminate

  (* Run is used to run a graphics app. We clear out all old event handlers,
     open the graphics page, call the user's init function (which typically
     installs listeners for the events) and then enter the event loop. If
     the user calls terminate within the event loop, then the exception
     Terminated will be raised and cause the loop to be terminated. *)
  let run ?(frame_rate : int = 30) init =
    try
      Graphics.open_graph "";
      Graphics.auto_synchronize false;
      init ();
      event_loop frame_rate
    with e ->
      Graphics.auto_synchronize true;
      Graphics.close_graph ();
      match e with
      | Terminate -> print_endline "Terminating."
      | _ -> raise e
end

(** EXAMPLES **)

(* Here is a simple use of the GraphicsEvents which draws a ball
   on the screen and lets you select the color by pressing
   'r' for red, 'g' for green, and 'b' for blue (or 'q' to quit
   the application.) *)
module Simple =
struct
  open DisplayElt

  (* start off with a yellow color *)
  let ball = new circ ~color:Graphics.magenta (300, 225) 75

  (* called when a key is pressed and selects the appropriate
     color and re-draws the ball, or terminates the event loop. *)
  let key_handler (c : char) : unit =
    (match c with
       | 'r' -> ball#set_color Graphics.red
       | 'g' -> ball#set_color Graphics.green
       | 'b' -> ball#set_color Graphics.blue
       | 'q' -> GraphicsEvents.terminate ()
       | _ -> ());
    ball#draw

  (* The simple application calls Event.run telling it to add
     key_handler to the key_pressed event listeners, and then
     draws the initial version of the (yellow) ball. *)
  let simple () =
    GraphicsEvents.clear_listeners ();
    GraphicsEvents.key_pressed#add_listener_ key_handler;
    GraphicsEvents.run (fun () -> ball#draw)
end

let _ = Simple.simple ()

(* A more elaborate example which demonstrates all of the different primitive
 * events. You can use wasd (or hjkl, or WASD) to move the ball around, and
 * press the mouse button down to change the color, and release it to change
 * it back. We also display a clock tick count (i.e., frame count) in the
 * upper-right corner of the frame, and the mouse position in the lower-left.
 *)

(*** BEGIN WRITE ***)
module MovingBall =
struct
  open DisplayElt

  let delta = 10

  let ball = new circ (250, 250) 40
  let clock_ticks = ref 0

  let key_handler (c:char) =
    ball#erase;
    (match c with
      | 'a' | 'A' | 'h' -> ball#translate (-delta) 0
      | 's' | 'S' | 'j' -> ball#translate 0        (-delta)
      | 'd' | 'D' | 'l' -> ball#translate delta    0
      | 'w' | 'W' | 'k' -> ball#translate 0        delta
      | 'q' | 'Q' -> GraphicsEvents.terminate ()
      | _ -> ()) ;
    ball#draw

  let button_down_handler _ =
    ball#set_color Graphics.red;
    ball#draw

  let button_up_handler _ =
    ball#set_color Graphics.blue;
    ball#draw

  let mouse_handler (x, y) =
    (new textbox (10, 10) (Printf.sprintf "mouse @ (%4d, %4d)" x y))#draw

  let clock_handler () =
    let (width, height) = Graphics.text_size "frames: 000000000" in
    let x = Graphics.size_x () - width - 10 in
    let y = Graphics.size_y () - height - 10 in
    clock_ticks := !clock_ticks + 1;
    (new textbox (x, y) (Printf.sprintf "frames: %d" !clock_ticks))#draw

  let moving_ball () =
    let open GraphicsEvents in
    clear_listeners ();
    key_pressed#add_listener_ key_handler;
    button_down#add_listener_ button_down_handler;
    button_up#add_listener_ button_up_handler;
    mouse_motion#add_listener_ mouse_handler;
    clock#add_listener_ clock_handler;
    run button_up_handler
end

let _ = MovingBall.moving_ball ()

(*** END WRITE ***)
(*** SLIDES ***)

(* This example uses a list of objects that move around randomly
   and bounce off the edges of the window. Generate a new box by
   pressing 's' and a new ball by pressing 'c'. *)
module BouncingItems =
struct
  open DisplayElt
  let _ = Random.self_init ()

  type direction = Neg | Same | Pos

  let max_value = 500

  let random_position () = (Random.int max_value, Random.int max_value)

  let random_color () = Random.int 0xffffff

  let random_direction () =
    match Random.int 3 with
      | 0 -> Neg
      | 1 -> Same
      | _ -> Pos

  let delta (x : int) (d : direction) : int * direction =
    match d with
      | Neg  -> if x <= 0 then (0, Pos) else (x - 1, Neg)
      | Same -> (x, Same)
      | Pos  -> if x >= max_value then (max_value, Neg) else (x + 1, Pos)

  (* Both balls and boxes will inherit from this. It keeps track
     of the direction that we're moving and provides a move method
     to update the position and bounce off the edge if necessary. *)
  class virtual moving =
  object (self)
    method virtual get_pos : point
    method virtual set_pos : point -> unit

    val mutable dx = random_direction ()
    val mutable dy = random_direction ()

    method move =
      let (x, y)    = self#get_pos in
      let (x', dx') = delta x dx in
      let (y', dy') = delta y dy in
      dx <- dx';
      dy <- dy';
      self#set_pos (x', y')
  end

  class type item =
  object
    inherit display_elt
    method move : unit
  end

  class ball (color : Graphics.color) : item =
  object
    inherit circ ~color (random_position ()) 10
    inherit moving
  end

  class box (color : Graphics.color) : item =
  object
    inherit rect ~color (random_position ()) 20 20
    inherit moving
  end

  (* a list of all of the items we've generated. *)
  let items : item list ref = ref []

  (* on each clock tick, we clear the frame, move the items,
     and then draw them again. *)
  let clock_handler () =
    Graphics.clear_graph ();
    List.iter !items ~f:fun i -> i#move; i#draw

  let key_handler c =
    match c with
      | 'c' | 'C' -> items := new ball (random_color ()) :: !items
      | 's' | 'S' -> items := new box (random_color ()) :: !items
      | 'q' | 'Q' -> GraphicsEvents.terminate ()
      | _ -> ()

  let bounce () =
    let open GraphicsEvents in
    clear_listeners ();
    items := [];
    key_pressed#add_listener_ key_handler;
    clock#add_listener_ clock_handler;
    run ~frame_rate:60
        (fun () -> Graphics.resize_window (max_value + 20) (max_value + 20))
end

let _ = BouncingItems.bounce ()

(* This is a slightly different version of the BouncingItems demo
   where we use an event to notify the objects to move and draw
   themselves. *)
module BouncingItems2 =
struct
  open DisplayElt
  module B = BouncingItems

  class virtual animating (tick : unit Event.event) =
  object (self)
    inherit B.moving

    method virtual draw : unit

    initializer
      tick#add_listener_ (fun () -> self#move; self#draw)
  end

  let item_clock = new Event.event

  class ball : B.item =
  object
    inherit circ ~color:(B.random_color ()) (B.random_position ()) 10
    inherit animating item_clock
  end

  class box : B.item =
  object
    inherit rect ~color:(B.random_color ()) (B.random_position ()) 20 20
    inherit animating item_clock
  end

  let clock_handler () =
    Graphics.clear_graph ();
    item_clock#fire ()

  let key_handler c =
    match c with
      | 'c' | 'c' -> ignore (new ball)
      | 's' | 'S' -> ignore (new box)
      | 'q' | 'Q' -> GraphicsEvents.terminate ()
      | _ -> ()

  let bounce () =
    let open GraphicsEvents in
    clear_listeners ();
    key_pressed#add_listener_ key_handler;
    clock#add_listener_ clock_handler;
    run ~frame_rate:60
        (fun () -> Graphics.resize_window (B.max_value + 20) (B.max_value + 20))
end

let _ = BouncingItems2.bounce ()

(** MORE EVENT OPERARATIONS **)

(* Here is an interface for some derived operations on events. *)
module type EVENT_PLUS =
sig
  include EVENT with type listener_id = Event.listener_id

  (* If event e fires with values [v1,v2,v3,...] then
     the event (map ~f e) fires with values [f v1, f v2, f v3, ...]
  *)
  val map : 'a event -> f:('a -> 'b) -> 'b event

  (* Infix version of map *)
  val (>>) : 'a event -> ('a -> 'b) -> 'b event

  (* The event (filter ~f e) fires with those values v fired from
     e such that (f v) = true. *)
  val filter : 'a event -> f:('a -> bool) -> 'a event

  (* Interleaves the values fired from the two events into a single
     event.  For instance, if we fire e1 v1; fire e2 v2; fire e1 v3
     then on this event, we will see [v1,v2,v3] fired. *)
  val (++) : 'a event -> 'a event -> 'a event

  (* Waits for the first event to fire a value v1, and then waits
     for the second event to fire a value v2, and then fires with
     value (v1, v2). *)
  val ($) : 'a event -> 'b event -> ('a * 'b) event

  (* If the input event fires with (v1,v2) then the returned events
     fire with v1 and v2 respectively. *)
  val split : ('a * 'b) event -> 'a event * 'b event

  (* Similar to a "reduce".  If e fires with events [v1,v2,v3,...]
     then (reduce f u e) is an event that fires with
     [u, f v1 u, f v2 (f v1 u), f v3 (f v2 (f v1 u)), ...]. *)
  val integrate : 'a event -> init:'b -> f:('b -> 'a -> 'b) -> 'b event
end

(* This module demonstrates some combinators that can be used to
   build new kinds of events out of other events.  Many of the ideas
   should look familiar... *)
module EventPlus : EVENT_PLUS =
struct
  include Event

  let map e ~f =
    let e' = new event in
    e#add_listener_ (fun v -> e'#fire (f v));
    e'

  let (>>) e f = map e ~f

  (* For example: *)
  let mouse_moved : unit event =
    map GraphicsEvents.mouse_motion ~f:(const ())

  let filter e ~f =
    let e' = new event in
    e#add_listener_ (fun v -> if f v then e'#fire v);
    e'

  (* For example, suppose we want an event that corresponds to the 'a' key
     being pressed: *)
  let a_pressed : char event = filter ~f:((=) 'a') GraphicsEvents.key_pressed

  (* Or, suppose we only want to be notified when the mouse
     moves into our area... *)
  let mouse_moved_rect (x, y) w h : (int * int) event =
    filter GraphicsEvents.mouse_motion
           ~f:fun (x', y') -> x' >= x && x' <= x + w && y' >= y && y' <= y + h

  let (++) e1 e2 =
    let e = new event in
    e1#add_listener_ e#fire;
    e2#add_listener_ e#fire;
    e

  let any_a = filter ~f:((=) 'a') GraphicsEvents.key_pressed
              ++ filter ~f:((=) 'A') GraphicsEvents.key_pressed

  let ($) e1 e2 =
    let e = new event in
    e1#add_listener_ (fun v1 ->
      e2#add_one_shot_ (fun v2 ->
        e#fire (v1, v2)));
  e

   (* With the above definition, if I wait for (e1 $ e2) then both
      events have to fire (in order) before the resulting event fires
      and then I get the output event. For example, a full mouse click
      could be modeled as:  *)
  let mouse_up_down : ((int * int) * (int * int)) event =
    GraphicsEvents.button_down $ GraphicsEvents.button_up

  let split e = (e >> fst, e >> snd)

  let integrate (e : 'a event) ~init ~f =
    let e' = new event in
    let rec loop accum =
      e#add_one_shot_ (fun v ->
                       let accum' = f accum v in
                       e'#fire accum';
                       loop accum') in
    loop init;
    e'

   (* As you might expect, we can implement some of the other
      combinators using integrate (just as we could implement
      many list functions, such as map and filter, using reduce). *)
  let map' ~(f : 'a -> 'b) : 'a event -> 'b event =
    integrate ~init:() ~f:(fun x _ -> f x)

   (* As another simple example, we could count the number of clock
      ticks that have passed with this: *)
  let total_clock_ticks : int event =
    integrate GraphicsEvents.clock ~init:0 ~f:(fun count _ -> count + 1)

  (* A more interesting example is that we may want to know the
     average of all of the values that have been signaled on an
     event. For instance, if we have a network event that tracks
     the stock price for a company, then we can produce a new
     event that tracks the average stock price for that company
     over time as follows: *)
  let average (e : int event) : float event =
    integrate e ~init:(0, 0) ~f:(fun (sum, count) i -> (sum + i, count + 1))
    >> fun (sum, count) -> Float.of_int sum /. Float.of_int count
end


(** EXAMPLES **)

module TestEventPlus =
struct
  open Graphics
  open GraphicsEvents
  open EventPlus

  let delta = 10

  type ball_state = { ball_x : int;
                      ball_y : int;
                      ball_size : int;
                      ball_color : color }

  let initial_state = { ball_x = 250;
                        ball_y = 250;
                        ball_size = 40;
                        ball_color = magenta }

  let draw_ball (s : ball_state) =
    set_color s.ball_color;
    fill_circle s.ball_x s.ball_y s.ball_size

  let erase_ball (s : ball_state) =
    set_color white;
    fill_circle s.ball_x s.ball_y s.ball_size

  let delta_x (i: int) (s : ball_state) =
    { s with ball_x = s.ball_x + i }

  let delta_y (i : int) (s : ball_state) =
    { s with ball_y = s.ball_y + i }

  let delta_size (i : int) (s : ball_state) =
    { s with ball_size = max 0 (s.ball_size + i) }

  let new_color (c : color) (s : ball_state) =
    { s with ball_color = c }

  let setup () =
    clear_listeners ();

    let grow =
      filter ~f:((=) '+') key_pressed >> fun _ -> delta_size 10 in

    let shrink =
      filter ~f:((=) '-') key_pressed >> fun _ -> delta_size (-10) in

    let move_up =
      filter ~f:((=) 'k') key_pressed >> fun _ -> delta_y 10 in

    let move_down =
      filter ~f:((=) 'j') key_pressed >> fun _ -> delta_y (-10) in

    let move_left =
      filter ~f:((=) 'h') key_pressed >> fun _ -> delta_x (-10) in

    let move_right =
      filter ~f:((=) 'l') key_pressed >> fun _ -> delta_x 10 in

    let change_blue =
      button_down >> fun _ -> new_color blue in

    let change_red =
      button_up >> fun _ -> new_color red in

    let quit =
      filter ~f:((=) 'q') key_pressed >> fun _ _ -> terminate () in

    let ball_actions =
      grow ++ shrink ++ move_up ++ move_down ++ move_left
      ++ move_right ++ change_blue ++ change_red ++ quit in

    ignore (integrate ball_actions ~init:initial_state
                      ~f:fun old_state updater ->
                         let new_state = updater old_state in
                         erase_ball old_state;
                         draw_ball new_state;
                         new_state)

  let test () =
    setup ();
    run (fun _ -> draw_ball initial_state)
end

let _ = TestEventPlus.test ()
