(* Lecture 12 (03/11/2014): References and Mutation *)

let cell = ref true

let mystery _ =
  cell := not (!cell);
  if !cell then 0 else Int.max_value

(*
 * SPOILER SPACE
 *)

let rec rem_longer xs =
  List.filter ~f:(fun ys -> List.length ys <= List.length xs)

;;
rem_longer [0; 0] [[1]; [1; 2]; [1; 2; 3]; [1; 2; 3; 4]; [1; 2; 3; 4; 5]]

let rec rem_mystery xs =
  List.filter ~f:(fun ys -> List.length ys <= mystery xs)

;;
rem_mystery [0; 0] [[1]; [1; 2]; [1; 2; 3]; [1; 2; 3; 4]; [1; 2; 3; 4; 5]]


(** SLIDES *)


let c = ref 8

let x = !c

let _ = c := 9

let y = !c



let c = ref 0

let next () =
  let v = !c in
  c := v + 1;
  v

let _ = next ()
let _ = next ()
let _ = next ()
let _ = next ()
let _ = next () + next ()

(* equivalently *)
let next () =
  let v = !c in
  let _dummy : unit = c := v + 1 in
  v



let c = ref 0

let x = c

let _ = c := 10

let _ = !x


(** Imperative stacks *)

module type IMP_STACK =
  sig
    type 'a t

    (* Make a new, empty stack *)
    val empty : unit -> 'a t

    (* Push a value onto a stack *)
    val push : 'a -> 'a t -> unit

    (* Pop the top value from the stack *)
    val pop : 'a t -> 'a option
  end

(* Compare type of push to type of functional push:
 *
 *   'a -> 'a t -> 'a t
 *
 *)

module ImpStack : IMP_STACK =
  struct
    type 'a t = 'a list ref

    let empty () = ref []

    let push x stack =
      stack := x :: !stack

    let pop stack =
      match !stack with
      | [] -> None
      | x :: xs ->
         stack := xs;
         Some x
  end

let s = ImpStack.empty ()

let _ = ImpStack.push 5 s

let s' = s

let _ = ImpStack.push 7 s
let _ = ImpStack.push 9 s

let _ = ImpStack.pop s'
let _ = ImpStack.pop s
let _ = ImpStack.pop s'
let _ = ImpStack.pop s'

(** Imperative queues *)

module type IMP_QUEUE =
  sig
    type 'a t

    (* Make a new, empty queue *)
    val empty : unit -> 'a t

    (* Enqueue a value *)
    val enq : 'a -> 'a t -> unit

    (* Dequeue a value *)
    val deq : 'a t -> 'a option
  end

module ImpQueue : IMP_QUEUE =
  struct
    type 'a t = { front : 'a list ref; rear : 'a list ref }

    let empty () = { front = ref []; rear = ref [] }

    let enq x q = q.rear := x :: !(q.rear)

    let deq q =
      match !(q.front) with
      | x :: xs ->
         q.front := xs;
         Some x
      | [] -> (match List.rev !(q.rear) with
               | x :: xs ->
                  q.front := xs;
                  q.rear := [];
                  Some x
               | [] -> None)
  end

let q = ImpQueue.empty ()

let _ = ImpQueue.enq 5 q

let q' = q

let _ = ImpQueue.enq 7 q'
let _ = ImpQueue.enq 9 q'

let _ = ImpQueue.deq q
let _ = ImpQueue.deq q
let _ = ImpQueue.deq q
let _ = ImpQueue.deq q

module ImpQueue2 : IMP_QUEUE =
  struct
    type 'a mlist = Nil | Cons of 'a * 'a mlist ref
    type 'a t = { front : 'a mlist ref; rear : 'a mlist ref }

    let empty () = { front = ref Nil; rear = ref Nil }

    let enq x q =
      match !(q.rear) with
      | Cons(y, r) ->
         assert( !r = Nil );
         r := Cons(x, ref Nil);
         q.rear := !r
      | Nil ->
         assert( !(q.front) = Nil );
         q.front := Cons(x, ref Nil);
         q.rear := !(q.front)

    let deq q =
      match !(q.front) with
      | Cons(x, r) ->
         q.front := !r;
         if !r = Nil then q.rear := Nil;
         Some x
      | Nil -> None
  end

let q = ImpQueue2.empty ()

let _ = ImpQueue2.enq 5 q

let q' = q

let _ = ImpQueue2.enq 7 q'
let _ = ImpQueue2.enq 9 q'

let _ = ImpQueue2.deq q
let _ = ImpQueue2.deq q
let _ = ImpQueue2.deq q
let _ = ImpQueue2.deq q

(** Mutable lists *)

type 'a mlist = Nil | Cons of 'a * 'a mlist ref

let mcons (x : 'a) (xs : 'a mlist) : 'a mlist = Cons(x, ref xs)

let xs = mcons 1 (mcons 2 (mcons 3 Nil))
let ys = mcons 4 (mcons 5 (mcons 6 Nil))

let rec mlength (xs : 'a mlist) : int =
  match xs with
  | Nil -> 0
  | Cons(_, r) -> 1 + mlength !r

assert( mlength lst = 3 )

let r = ref Nil
let m = Cons(1, r)
let _ = r := m
let _ = mlength m

let mlength : 'a mlist -> int =
  let rec loop acc xs =
    match xs with
    | Nil -> acc
    | Cons(_, r) -> loop (acc + 1) !r
  in loop 0

let _ = mlength m


let rec mappend (xs : 'a mlist) (ys : 'a mlist) : unit =
  match xs with
  | Nil -> ()
  | Cons(_, r) ->
     (match !r with
      | Nil -> r := ys
      | xs' -> mappend xs' ys)
