(* Lecture 9 (02/27): more abstract data types; intro to complexity *)

open Core.Std

(* Rule of thumb: Use language mechanisms to enforce abstraction.
 * (Like all design rules, violate this when it sucks too much.)
 *)

(** Matrices *)

(* Matrices are defined over rings, which offer some operations: *)
module type RING =
  sig
    type t
    val zero : t
    val one : t
    val add : t -> t -> t
    val mul : t -> t -> t
    (* ... *)
  end

module IntRing =
  struct
    type t = int
    let zero = 0
    let one = 1
    let add = (+)
    let mul = ( * )
  end

module FloatRing =
  struct
    type t = float
    let zero = 0.
    let one = 1.
    let add = (+.)
    let mul = ( *. )
  end

module BoolRing =
  struct
    type t = bool
    let zero = false
    let one = true
    let add = (||)
    let mul = (&&)
  end

(* A signature for matrices *)
module type MATRIX =
  sig
    type elt
    type t
    val of_list : elt list list -> t
    val to_list : t -> elt list list
    val scale : elt -> t -> t
    val add : t -> t -> t
    val mul : t -> t -> t
  end

(* Make a matrix over any ring *)
module DenseMatrix (R : RING) : MATRIX with type elt = R.t =
  struct
    type elt = R.t
    type t = elt list list
    let of_list = ident
    let to_list = ident
    let scale s = List.map ~f:(List.map ~f:(R.mul s))
    let add = List.map2_exn ~f:(List.map2_exn ~f:R.add)
    let mul _ _ = failwith "not implemented"
  end

module DenseIntMatrix = DenseMatrix(IntRing)
module DenseFloatMatrix = DenseMatrix(FloatRing)
module DenseBoolMatrix = DenseMatrix(BoolRing)

let matrix1 = DenseIntMatrix.of_list [[2; 3]; [4; 5]]
let matrix2 = DenseIntMatrix.add matrix1 matrix1
assert( DenseIntMatrix.to_list matrix2 = [[4; 6]; [8; 10]] )

let matrix3 = DenseIntMatrix.scale (-1) matrix2
assert( DenseIntMatrix.to_list matrix3 = [[-4; -6]; [-8; -10]] )


(** Another functor example *)

(* A signature for sets *)
module type SET =
  sig
    type elt
    type t
    val empty : t
    val singleton : elt -> t
    val union : t -> t -> t
    val inter : t -> t -> t
    val is_empty : t -> bool
    val mem : elt -> t -> bool
    (* ... *)
  end

(* Tests for sets *)
module SetTests(Set : SET with type elt = string) =
  struct
    open Set
    let s1 = singleton "one"
    let s2 = singleton "two"
    let s3 = singleton "three"
    let s4 = singleton "four"
    let s12 = union s1 s2
    let s13 = union s1 s3
    let s123 = union s12 s3
    let s1' = inter s12 s13

    assert( mem "one" s1 )
    assert( not (mem "two" s1) )
    assert( mem "one" s12 )
    assert( mem "two" s12 )
    assert( not (mem "three" s12) )
    assert( mem "one" s123 )
    assert( mem "two" s123 )
    assert( mem "three" s123 )
    assert( not (mem "four" s12) )
    assert( mem "one" s1' )
    assert( not (mem "two" s1') )
    assert( not (mem "three" s1') )
    assert( not (mem "four" s1') )
    (* Probably need to test with larger sets. *)
  end

(* A signature for element types *)
module type ELT =
  sig
    type t
    val compare : t -> t -> int
  end

(* Make a set module, with sets represented as sorted lists *)
module SortedListSet (Elt : ELT) : (SET with type elt = Elt.t) = struct
    type elt = Elt.t
    type t = elt list

    let empty = []
    let singleton x = [x]

    let rec inter (xs : 'a list) (ys : 'a list) : 'a list =
      match xs, ys with
      | x :: xs', y :: ys' ->
         (match Elt.compare x y with
          | -1 -> inter xs' ys
          | 1  -> inter xs ys'
          | _  -> x :: inter xs' ys')
      | _ -> []

    let rec union (xs : 'a list) (ys : 'a list) : 'a list =
      match xs, ys with
      | [], _ -> ys
      | _, [] -> xs
      | x :: xs', y :: ys' ->
         (match Elt.compare x y with
          | -1 -> x :: union xs' ys
          | 1  -> y :: union xs ys'
          | _  -> x :: union xs' ys')

    let is_empty = List.is_empty
    let mem x set = List.mem set x ~equal:(fun x y -> Elt.compare x y = 0)
  end

(* Make a set module, with sets represented as binary search trees lists *)
module BinTreeSet (Elt : ELT) : (SET with type elt = Elt.t) =
  struct
    type elt = Elt.t
    type t =
      | Leaf
      | Branch of t * elt * t

    let empty = Leaf
    let singleton x = Branch(Leaf, x, Leaf)
    let union _ _ = failwith "unimplemented"
    let inter _ _ = failwith "unimplemented"
    let is_empty = function Leaf -> true | _ -> false
    let mem _ _ = failwith "unimplemented"
end

module StringElt =
  struct
    type t = string
    let compare = String.compare
  end

module StringSet = SortedListSet(String)

module TestStringSet = SetTests(StringSet)

(** Resource usage *)

(*
 * T(x :: xs', ys) = 3 + T(xs', ys)
 * T([], ys) = 1
 *
 * T(N) = C + T(N - 1)
 * T(0) = D
 *
 * T(N) = CN + D
 *)
let rec append xs ys =
  match xs with
  | [] -> ys
  | x :: xs' -> x :: append xs' ys

(*
 * T(N) = 4 + (N - 1) + T(N - 1)
 *
 * T(N) = C + N + T(N - 1)
 *
 * T(N) = C + N + C + (N - 1) + C + (N - 2) + ... + C + 1
 *
 * T(N) = CN + N(N + 1)/2
 *)
let rec reverse xs =
  match xs with
  | [] -> []
  | x :: xs' -> append (reverse xs') [x]

(*
 * T(N) = 3 + T(N - 1)
 *
 * T(N) = CN
 *)
let reverse' xs0 =
  let rec loop xs result =
    match xs with
    | [] -> result
    | x :: xs' -> loop xs' (x :: result) in
  loop xs0 []

(* How do we know that reverse' is correct? A loop invariant: We show that
 * the following is true each time through the loop:

     append (reverse xs) result = reverse xs0

   At the start, we call loop xs0 [], so

     xs = xs0, result = []

   then append (reverse xs0) [] = reverse xs0.

   In the recursive case, assuming that

     append (reverse xs) result = reverse xs0
      = append (reverse (x :: xs')) result
      = append (append (reverse xs') [x]) result
      = append (reverse xs') (append [x] result)
      = append (reverse xs') (x :: result) = reverse xs0

   That is, the recursive call maintains the invariant. Finally,
   we return result when xs is []:

     append (reverse []) result = reverse xs0

     result = reverse xs0

     [QED]
 *)
