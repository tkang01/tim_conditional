\documentclass{beamer}

\RequirePackage{ucs}
\usepackage[quiet]{fontspec}
\usepackage{xunicode}
\usepackage{xltxtra}
\usepackage{graphicx}
\usepackage{forloop}
\usepackage{stmaryrd}
\usepackage{tikz}
\usepackage{mathtools}
\usepackage{fancyvrb}
\usepackage{amsmath,amssymb,amsthm}

\usetikzlibrary{chains,decorations.pathmorphing,positioning,fit}
\usetikzlibrary{decorations.shapes,calc,backgrounds}
\usetikzlibrary{decorations.text,matrix}
\usetikzlibrary{arrows,shapes.geometric,shapes.symbols,scopes}

\newcommand\bigO[1]{\ensuremath{\mathcal{O}(#1)}}
\newcommand\fun[1]{\ensuremath{\lambda #1.\,}}
\renewcommand\T[1]{\ensuremath{T_{\mathit{#1}}}}

%% %%% SETTINGS

\frenchspacing
\unitlength=0.01\textwidth
\thicklines
\urlstyle{sf}
\graphicspath{{images/}}

%%% VERBATIM SETUP

\DefineVerbatimEnvironment
  {code}{Verbatim}
  {commandchars=\\\‹\›,
   commentchar=\%,
   codes={\catcode`$=3\relax},
   baselinestretch=.8,
   formatcom=\relax}
\renewcommand\FancyVerbFormatLine[1]{#1\strut}
\fvset{commandchars=\\\‹\›,
       codes={\catcode`$=3\relax},
       formatcom=\relax}
\DefineShortVerb{\^}

\newcommand\ALT[3]{\alt<#1>{#2}{#3}}

%%% COLOR AND BEAMER SETUP

\setbeamercolor{frametitle}{fg=black}
\setbeamercolor{normal text}{fg=black}

\defaultfontfeatures{
    Mapping=tex-text,
    Scale=MatchLowercase,
}
\setmainfont{TeX Gyre Schola}
\setmonofont{Monaco}

\usefonttheme{serif}

\setbeamertemplate{frametitle}
  {\begin{centering}\medskip
   \insertframetitle\par
   \end{centering}}

\setbeamertemplate{itemize item}{$\bullet$}

\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[text line]{%
    \hfill\strut{%
        \small\color{black!75}%
        \texttt{\insertframenumber:\insertframeslidenumber}%
    }%
    \hfill%
}

\makeatletter
\newcount\frameslidetemp
\newcommand\insertframeslidenumber{%
  \frameslidetemp\c@page\relax
  \advance\frameslidetemp-\beamer@startpageofframe\relax
  \advance\frameslidetemp1\relax
  \the\frameslidetemp
}
\makeatother

%%% GENERALLY USEFUL MACROS

\newcommand<>\always[1]{#1}

\def\defgbox#1#2{%
  \expandafter\global\expandafter\newbox\csname#2box\endcsname%
  \expandafter\global\expandafter\def\csname#2\endcsname{%
    \expandafter\usebox\csname#2box\endcsname%
  }%
  \expandafter\global\expandafter\setbox\csname#2box\endcsname=#1%
}
\def\defhbox{\defgbox\hbox}
\def\defvbox{\defgbox\vbox}

\def\setgbox#1#2{%
  \expandafter\setbox\csname#2box\endcsname=#1%
}
\def\sethbox{\setgbox\hbox}
\def\setvbox{\setgbox\vbox}

%%%
%%% TIKZ
%%%

\newcommand\K[1]{\textcolor{orange!70!black}{#1}}
\newcommand\REF{\textcolor{green!50!black}{ref}}

\newcommand\place[3][]{%
  \tikz[orp] \node[#1] at (#2) {#3};%
}

\newcommand\anchor[1]{%
  \tikz[orp] \coordinate (#1);%
}

\newcommand\nil[1]{
  \draw[nil] (#1.north east) -- (#1.south west)
}

\newcommand\cons[4][]{
  \node[cons,#1] (#2) {#3\kern3pt\anchor{#2 center}\kern3pt#4};
  \draw[thick] (#2 center |- #2.north) -- (#2 center |- #2.south)
}

\newcommand\genpointer[3][draw=blue!50!black,opacity=.5]{%
  \begin{tikzpicture}[rem,solid,#1]
    \node(pointer start) [circle, draw, inner sep=1.37pt] {};
    \draw[->, overlay] (pointer start) edge[bend right=#2] (#3);
  \end{tikzpicture}%
}

\newcommand\hpointer[2][30]{%
  \genpointer[draw=red!70!black]{#1}{#2}%
}

\newcommand\ppointer[2][30]{%
  \genpointer{#1}{#2}%
}

\newcommand\pointer[2][30]{%
  \ifhighlit\let\pointertemp\hpointer\else\let\pointertemp\ppointer\fi
  \pointertemp[#1]{#2}%
}


\newcount\reducetemp
\newcommand\reduce[3]{%
  \reducetemp#1\relax
  \advance\reducetemp -1\relax
  \alt<#1->{#3}{\alt<\the\reducetemp>{\highlight{#2}}{#2}}%
}

\tikzset{
  orp/.style={
    overlay,
    remember picture,
  },
  rem/.style={
    remember picture,
  },
  every node/.style={
    very thick,
    inner sep=3pt,
  },
  every path/.style={very thick},
  ref/.style={
    draw=red,
    densely dotted,
    font=\ttfamily,
  },
  cons/.style={
    draw=black,
    font=\ttfamily,
  },
  nil/.style={
    black!80!white,
    shorten >=2pt,
    shorten <=2pt,
  },
  highlight/.style={
    color=yellow!50!white!90!black,
    thick,
    draw,
    draw opacity=0.5,
    outer color=yellow!50,
    inner color=yellow!30,
    rounded corners=2pt,
  },
}

\newif\ifhighlit
\highlitfalse

\newcommand<>\highlight[2][\relax]{%
  \ifx#1\relax
    \begin{tikzpicture}
  \else
    \begin{tikzpicture}[remember picture]
  \fi
    \node[
      inner sep=0pt,
      outer sep=0pt,
      text depth=0pt,
      align=left,
    ] (highlight temp) {%
        \only#3{\highlittrue}%
        #2%
        \only#3{\highlitfalse}%
    };
    \begin{scope}[on background layer]
      \coordinate (highlight temp 1) at
        ($(highlight temp.north east) + (2pt,2pt)$);
      \coordinate (highlight temp 2) at
        ($(highlight temp.base west) + (-2pt,-3pt)$);
      \node#3 [
        overlay,
        highlight,
        inner sep=0pt,
        fit=(highlight temp 1) (highlight temp 2),
      ] {};
    \end{scope}
    \ifx#1\relax
      \relax
    \else
      \node (#1) [
        overlay,
        inner sep=0pt,
        fit=(highlight temp 1) (highlight temp 2),
      ] {};
    \fi
  \end{tikzpicture}%
}

\newcommand\HI[2]{\highlight<#1>{#2}}

\newcommand\huncover[3][\relax]{%
  \uncover<#2->{\highlight<#2>[#1]{#3}}%
}
\newcommand\honly[3][\relax]{%
  \only<#2->{\highlight<#2>[#1]{#3}}%
}

\def\S#1#2{#2<#1>}

\begin{document}

%%%
%%% TITLE PAGE
%%%

\begin{frame}\relax
  \thispagestyle{empty}
  \begin{center}
  {\Huge References and Mutation, Continued}

  \bigskip
  \Large
  CS 51 and CSCI E-51

  \medskip
  March 13, 2014
  \end{center}

  %% Need to initialize Tikz opacity inside a frame, not outside:
  \begin{tikzpicture}[opacity=0,overlay]\end{tikzpicture}
\end{frame}

\begin{frame}[fragile]{References}
  \begin{itemize}
    \item<+-> New type: ^t ref^
      \begin{itemize}
        \item A pointer to a \emph{box} holding a ^t^
        \item Like ^t*^ in C
      \end{itemize}
    \item<+-> To allocate: ^\REF 8^
      \begin{itemize}
        \item Allocates a new box, initializes to \texttt{8}, and
          returns the pointer
        \item Like C:
\begin{code}
  int *r = malloc(sizeof(int));
  *r = 8;
  return r;
\end{code}
      \end{itemize}
    \item<+-> To read: ^!r^
      \begin{itemize}
        \item Returns contents of box
        \item Like ^*r^ in C.
      \end{itemize}
    \item<+-> To write: ^r := 9^
      \begin{itemize}
        \item Updates the box to contain \texttt{9}
        \item Like ^*r = 9;^ in C
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Reduction semantics for references}
  \begin{tikzpicture}[orp]
    \uncover<3->{
      \node[ref] (r) at ($ (current page.east)!.5!(current page.center) $)
          {\reduce{11}{8}{9}};
    }
  \end{tikzpicture}
\begin{code}
\ALT‹5-›‹›‹\K‹let› r = \reduce‹3›‹\REF 8›‹\pointer‹r›› \K‹in››
\ALT‹12-›‹›‹\reduce‹11›‹\reduce‹5›‹r›‹\pointer‹r›› := \reduce‹9›‹\reduce‹7›‹!\reduce‹5›‹r›‹\pointer‹r›››‹8› + 1›‹9››‹()›;›
\reduce‹14›‹!\reduce‹5›‹r›‹\pointer‹r›››‹9›
\end{code}
\end{frame}

\begin{frame}[fragile]{Mutable lists}
  \place{$ (current page.south) !.5! (current page.center) $}{
    \begin{tikzpicture}[rem,node distance=15pt]
      \uncover<4->{
      \node[ref] (rnil) {\phantom{X}};
      \nil{rnil};
      }
      \uncover<6->{
      \cons[left=of rnil] {c4} {4} {\pointer{rnil}};
      }
      \uncover<8->{
      \node[ref,left=of c4] (r4) {\pointer{c4}};
      }
      \uncover<10->{
      \cons[left=of r4] {c3} {3} {\pointer{r4}};
      }
      \uncover<12->{
      \node[ref,left=of c3] (r3) {\pointer{c3}};
      }
      \uncover<14->{
      \cons[left=of r3] {c2} {2} {\pointer{r3}};
      }
    \end{tikzpicture}
  }
\begin{code}
\K‹type› 'a mlist = Nil | Cons of 'a * 'a mlist ref
\end{code}
\par\bigskip\pause
\begin{code}
\K‹let› lst = \reduce‹14›‹Cons(2, \reduce‹12›‹\REF \reduce‹10›‹(Cons(3, \reduce‹8›‹\REF \reduce‹6›‹(Cons(4, \reduce‹4›‹\REF Nil›‹\ppointer‹rnil››))›‹\ppointer‹c4›››‹\ppointer‹r4››))›‹\ppointer‹c3›››‹\ppointer‹r3››)›‹\ppointer‹c2››
\end{code}
\end{frame}

\begin{frame}[fragile]{(A readability fix)}
  \begin{code}
\K‹type› 'a mlist = Nil | Cons of 'a * 'a mlist ref
\relax
\K‹let› lst = Cons(2, \REF (Cons(3, \REF (Cons(4, \REF Nil)))))
\pause
\K‹let› mcons x xs = Cons(x, \REF xs)
\pause
\K‹let› lst = mcons 2 (mcons 3 (mcons 4 Nil))
\pause
\K‹let› (^:) x xs = Cons(x, \REF xs)
\pause
\K‹let› lst = 2 ^: 3 ^: 4 ^: Nil
  \end{code}
\end{frame}

\begin{frame}[fragile,t]{Destructive append}
  \place[anchor=south]{$ (current page.south) + (0,2em) $}{
    \begin{tikzpicture}[rem,node distance=25pt and 15pt]
      \uncover<4->{
        \node[ref] (ysnil) {\phantom{X}};
        \nil{ysnil};
        \cons[left=of ysnil] {c6} {6} {\pointer{ysnil}};
        \node[ref,left=of c6] (r6) {\pointer{c6}};
        \cons[left=of r6] {c5} {5} {\pointer{r6}};
        \node[ref,left=of c5] (r5) {\pointer{c5}};
        \cons[left=of r5] {c4} {4} {\pointer{r5}};
      }
      \uncover<3->{
        \node[ref,above=of ysnil] (xsnil) {\phantom{X}};
        \nil{xsnil};
        \cons[left=of xsnil] {c3} {3} {\pointer{xsnil}};
        \node[ref,left=of c3] (r3) {\pointer{c3}};
        \cons[left=of r3] {c2} {2} {\pointer{r3}};
        \node[ref,left=of c2] (r2) {\pointer{c2}};
        \cons[left=of r2] {c1} {1} {\pointer{r2}};
      }
    \end{tikzpicture}
  }
\vspace{-\baselineskip}
\begin{code}
\K‹let› mappend (xs : 'a mlist) (ys : 'a mlist) : 'a mlist =
  \K‹let› \K‹rec› loop r =
    \K‹match› !r \K‹with›
    | Nil -> r := ys
    | Cons(_, next) -> loop next \K‹in›
  \K‹match› xs \K‹with›
  | Nil -> ys
  | Cons(_, r) -> loop r; xs
\end{code}
\begin{overprint}
\onslide<2>
\begin{code}
\K‹let› xs = 1 ^: 2 ^: 3 ^: Nil
\K‹let› ys = 4 ^: 5 ^: 6 ^: Nil
\relax
\K‹let› zs = mappend xs ys
\end{code}
\onslide<3>
\begin{code}
\K‹let› xs = \pointer[330]‹c1›
\K‹let› ys = 4 ^: 5 ^: 6 ^: Nil
\relax
\K‹let› zs = mappend xs ys
\end{code}
\onslide<4>
\begin{code}
\K‹let› xs = \pointer[330]‹c1›
\K‹let› ys = \pointer‹c4›
\relax
\K‹let› zs = mappend xs ys
\end{code}
\onslide<5>
\begin{code}
\K‹let› xs = \pointer[330]‹c1›
\K‹let› ys = \pointer‹c4›
\relax
\K‹let› zs = mappend \pointer[330]‹c1› \pointer[345]‹c4›
\end{code}
\end{overprint}
\end{frame}

\begin{frame}[fragile,t]{Destructive append}
  \place[anchor=south]{$ (current page.south) + (0,2em) $}{
    \begin{tikzpicture}[rem,node distance=25pt and 15pt]
      \node[ref] (ysnil) {\phantom{X}};
      \nil{ysnil};
      \cons[left=of ysnil] {c6} {6} {\pointer{ysnil}};
      \node[ref,left=of c6] (r6) {\pointer{c6}};
      \cons[left=of r6] {c5} {5} {\pointer{r6}};
      \node[ref,left=of c5] (r5) {\pointer{c5}};
      \cons[left=of r5] {c4} {4} {\pointer{r5}};
      \node[ref,above=of ysnil] (xsnil)
                {\reduce{40}{\phantom{X}}{\hpointer[70]{c4}}};
      \only<-29>{\nil{xsnil};}
      \cons[left=of xsnil] {c3} {3} {\pointer{xsnil}};
      \node[ref,left=of c3] (r3) {\pointer{c3}};
      \cons[left=of r3] {c2} {2} {\pointer{r3}};
      \node[ref,left=of c2] (r2) {\pointer{c2}};
      \cons[left=of r2] {c1} {1} {\pointer{r2}};
    \end{tikzpicture}
  }
\vspace{-\baselineskip}
\begin{code}
\K‹let› xs = \pointer[75]‹c1.west›
\K‹let› ys = \pointer[60]‹c4.west›
\end{code}
\par
\begin{overprint}
\onslide<1>
\begin{code}
\K‹let› zs = \highlight‹mappend› \ppointer[345]‹c1› \ppointer[330]‹c4›
\end{code}
\onslide<2-4>
\begin{code}
\K‹let› zs =
  (\K‹fun› \HI‹3›‹xs› \HI‹3›‹ys› ->
     \K‹let› \K‹rec› loop r =
       \K‹match› !r \K‹with›
       | Nil -> r := \reduce‹4›‹ys›‹\pointer[15]‹c4››
       | Cons(_, next) -> loop next \K‹in›
     \K‹match› \reduce‹4›‹xs›‹\pointer‹c1›› \K‹with›
     | Nil -> \reduce‹4›‹ys›‹\pointer‹c4››
     | Cons(_, r) -> loop r; \reduce‹4›‹xs›‹\pointer‹c1››) \ppointer[355]‹c1› \ppointer[330]‹c4›
\end{code}
\onslide<5-8>
\begin{code}
\K‹let› zs =
  \K‹let› \K‹rec› loop r =
    \K‹match› !r \K‹with›
    | Nil -> r := \pointer[15]‹c4›
    | Cons(_, next) -> loop next \K‹in›
  \K‹match› \HI‹6-›‹\pointer‹c1›› \K‹with›
  | Nil -> \pointer‹c4›
  | \HI‹6-›‹Cons(_, r)› -> loop \reduce‹8›‹r›‹\pointer‹r2››; \pointer‹c1›
\end{code}
\onslide<9-10>
\begin{code}
\K‹let› zs =
  \K‹let› \K‹rec› loop r =
    \K‹match› !r \K‹with›
    | Nil -> r := \pointer[15]‹c4›
    | Cons(_, next) -> loop next \K‹in›
  \HI‹10›‹loop› \pointer‹r2›; \pointer‹c1›
\end{code}
\onslide<11-13>
\begin{code}
\K‹let› zs =
  \K‹let› \K‹rec› loop r =
    \K‹match› !r \K‹with›
    | Nil -> r := \pointer[15]‹c4›
    | Cons(_, next) -> loop next \K‹in›
  (\K‹fun› \HI‹12›‹r› -> 
    \K‹match› !\reduce‹13›‹r›‹\pointer‹r2›› \K‹with›
    | Nil -> \reduce‹13›‹r›‹\pointer‹r2›› := \pointer[15]‹c4›
    | Cons(_, next) -> loop next) \pointer‹r2›; \pointer‹c1›
\end{code}
\onslide<14-18>
\begin{code}
\K‹let› zs =
  \K‹let› \K‹rec› loop r =
    \K‹match› !r \K‹with›
    | Nil -> r := \pointer[15]‹c4›
    | Cons(_, next) -> loop next \K‹in›
  (\K‹match› \reduce‹16›‹!\pointer‹r2››‹\HI‹17-›‹\pointer‹c2››› \K‹with›
   | Nil -> ‹\pointer‹r2›› := \pointer[15]‹c4›
   | \HI‹17-›‹Cons(_, next)› -> loop \reduce‹18›‹next›‹\pointer‹r3››); \pointer[355]‹c1›
\end{code}
\onslide<19-20>
\begin{code}
\K‹let› zs =
  \K‹let› \K‹rec› loop r =
    \K‹match› !r \K‹with›
    | Nil -> r := \pointer[15]‹c4›
    | Cons(_, next) -> loop next \K‹in›
  \HI‹20›‹loop› \pointer‹r3›; \pointer‹c1›
\end{code}
\onslide<21-23>
\begin{code}
\K‹let› zs =
  \K‹let› \K‹rec› loop r =
    \K‹match› !r \K‹with›
    | Nil -> r := \pointer[15]‹c4›
    | Cons(_, next) -> loop next \K‹in›
  (\K‹fun› \HI‹22›‹r› -> 
    \K‹match› !\reduce‹23›‹r›‹\pointer‹r3›› \K‹with›
    | Nil -> \reduce‹23›‹r›‹\pointer‹r3›› := \pointer[15]‹c4›
    | Cons(_, next) -> loop next) \pointer‹r3›; \pointer[355]‹c1›
\end{code}
%
\onslide<24-28>
\begin{code}
\K‹let› zs =
  \K‹let› \K‹rec› loop r =
    \K‹match› !r \K‹with›
    | Nil -> r := \pointer[15]‹c4›
    | Cons(_, next) -> loop next \K‹in›
  (\K‹match› \reduce‹26›‹!\pointer‹r3››‹\HI‹27-›‹\pointer‹c3››› \K‹with›
   | Nil -> ‹\pointer‹r3›› := \pointer[15]‹c4›
   | \HI‹27-›‹Cons(_, next)› -> loop \reduce‹28›‹next›‹\pointer‹xsnil››); \pointer[355]‹c1›
\end{code}
\onslide<29-30>
\begin{code}
\K‹let› zs =
  \K‹let› \K‹rec› loop r =
    \K‹match› !r \K‹with›
    | Nil -> r := \pointer[15]‹c4›
    | Cons(_, next) -> loop next \K‹in›
  \HI‹30›‹loop› \pointer‹xsnil›; \pointer‹c1›
\end{code}
\onslide<31-33>
\begin{code}
\K‹let› zs =
  \K‹let› \K‹rec› loop r =
    \K‹match› !r \K‹with›
    | Nil -> r := \pointer[15]‹c4›
    | Cons(_, next) -> loop next \K‹in›
  (\K‹fun› \HI‹32›‹r› -> 
    \K‹match› !\reduce‹33›‹r›‹\pointer‹xsnil›› \K‹with›
    | Nil -> \reduce‹33›‹r›‹\pointer‹xsnil›› := \pointer[15]‹c4›
    | Cons(_, next) -> loop next) \pointer‹xsnil›; \pointer[355]‹c1›
\end{code}
%
\onslide<34-37>
\begin{code}
\K‹let› zs =
  \K‹let› \K‹rec› loop r =
    \K‹match› !r \K‹with›
    | Nil -> r := \pointer[15]‹c4›
    | Cons(_, next) -> loop next \K‹in›
  (\K‹match› \reduce‹36›‹!\pointer‹xsnil››‹\HI‹37-›‹Nil›› \K‹with›
   | \HI‹37-›‹Nil› -> \pointer‹xsnil› := \pointer[15]‹c4›
   | Cons(_, next) -> loop next); \pointer[355]‹c1›
\end{code}
\onslide<38-41>
\begin{code}
\K‹let› zs =
  \K‹let› \K‹rec› loop r =
    \K‹match› !r \K‹with›
    | Nil -> r := \pointer[15]‹c4›
    | Cons(_, next) -> loop next \K‹in›
   \ALT‹41›‹›‹\reduce‹40›‹\pointer‹xsnil› := \pointer‹c4››‹()›; ›\pointer‹c1›
\end{code}
\onslide<42>
\begin{code}
\K‹let› zs = \pointer‹c1›
\end{code}
\end{overprint}
\end{frame}

\begin{frame}[fragile,t]{Caution!}
  \place[anchor=south]{$ (current page.south) + (0,2em) $}{
    \begin{tikzpicture}[rem,node distance=25pt and 15pt]
      \node[ref] (ysnil) {\phantom{X}};
      \nil{ysnil};
      \cons[left=of ysnil] {c6} {6} {\pointer{ysnil}};
      \node[ref,left=of c6] (r6) {\pointer{c6}};
      \cons[left=of r6] {c5} {5} {\pointer{r6}};
      \node[ref,left=of c5] (r5) {\pointer{c5}};
      \cons[left=of r5] {c4} {4} {\pointer{r5}};
      \node[ref,above=of ysnil] (xsnil)
                {\reduce{4}{\phantom{X}}{\hpointer[70]{c1}}};
      \only<-3>{\nil{xsnil};}
      \cons[left=of xsnil] {c3} {3} {\pointer{xsnil}};
      \node[ref,left=of c3] (r3) {\pointer{c3}};
      \cons[left=of r3] {c2} {2} {\pointer{r3}};
      \node[ref,left=of c2] (r2) {\pointer{c2}};
      \cons[left=of r2] {c1} {1} {\pointer{r2}};
    \end{tikzpicture}
  }
\vspace{-\baselineskip}
\begin{code}
\K‹let› mappend (xs : 'a mlist) (ys : 'a mlist) : 'a mlist =
  \K‹let› \K‹rec› loop r =
    \K‹match› !r \K‹with›
    | Nil -> r := ys
    | Cons(_, next) -> loop next \K‹in›
  \K‹match› xs \K‹with›
  | Nil -> ys
  | Cons(_, r) -> loop r; xs
\end{code}
\begin{overprint}
\onslide<1>
\begin{code}
\K‹let› xs = 1 ^: 2 ^: 3 ^: Nil
\K‹let› ys = 4 ^: 5 ^: 6 ^: Nil
\relax
\K‹let› zs = mappend xs xs
\end{code}
\onslide<2-4>
\begin{code}
\K‹let› xs = \pointer[330]‹c1›
\K‹let› ys = \pointer‹c4›
\relax
\K‹let› zs = \reduce‹4›‹mappend \pointer[330]‹c1› \pointer[345]‹c1››‹\pointer[330]‹c1››
\end{code}
\end{overprint}
\end{frame}

\begin{frame}[fragile]{Imperative queues, revisited}
\begin{code}
\K‹module type› IMP_QUEUE =
  \K‹sig›
    \K‹type› 'a t
    \relax
    \K‹val› empty : unit -> 'a t
    \K‹val› enq   : 'a -> 'a t -> unit
    \K‹val› deq   : 'a t -> 'a option
  \K‹end›
\end{code}
\end{frame}

\begin{frame}[fragile]{Imperative queue implementation}
\small
\begin{code}
\K‹module› ImpQueue : IMP_QUEUE =
  \K‹struct›
    \K‹type› 'a t = { front : 'a mlist ref; rear : 'a mlist ref }
\relax
    \K‹let› empty () = { front = \REF Nil; rear = \REF Nil }
\relax
    \K‹let› enq x q =
      \K‹match› !(q.rear) \K‹with›
      | Cons(_, r) ->
         r := Cons(x, \REF Nil);
         q.rear := !r
      | Nil ->
         q.front := Cons(x, \REF Nil);
         q.rear := !(q.front)
\relax
    \K‹let› deq q =
      \K‹match› !(q.front) \K‹with›
      | Cons(x, r) ->
         q.front := !r;
         \K‹if› !r = Nil \K‹then› q.rear := Nil;
         Some x
      | Nil -> None
  \K‹end›
\end{code}
\end{frame}

\begin{frame}[fragile,t]{Imperative queues: empty}
  \place[anchor=south]{$ (current page.south) + (0,2em) $}{
    \begin{tikzpicture}[rem,node distance=15pt]
      \uncover<0>{
        \node[ref] (rnil) {\phantom{X}};
        \nil{rnil};
        \cons[left=of rnil] {c3} {3} {\pointer{rnil}};
        \node[ref,left=of c3] (r3) {\pointer{c3}};
        \cons[left=of r3] {c2} {2} {\pointer{r3}};
        \node[ref,left=of c2] (r2) {\pointer{c2}};
        \cons[left=of r2] {c1} {1} {\pointer{r2}};
      }
      \uncover<7->{
        \node[ref,above=of c1] (front) {\phantom{X}};
        \nil{front};
      }
      \uncover<9->{
        \node[ref,right=of front] (rear) {\phantom{X}};
        \nil{rear};
      }
      \uncover<11->{
        \cons[above=of front] {q} {\pointer{front}} {\pointer{rear}};
      }
    \end{tikzpicture}
  }
  \begin{code}
\ALT‹13-›‹›‹let \HI‹12›‹q› = \reduce‹11›‹\reduce‹5›‹\reduce‹3›‹empty›‹(\K‹fun› () -> { front = \REF Nil; rear = \REF Nil })› ()›‹{ front = \reduce‹7›‹\REF Nil›‹\ppointer‹front››; rear = \reduce‹9›‹\REF Nil›‹\ppointer‹rear›› }››‹\pointer‹q›› in›
\HI‹14›‹enq› 1 \reduce‹13›‹q›‹\pointer[330]‹q››;
enq 2 \reduce‹13›‹q›‹\pointer[330]‹q››;
enq 3 \reduce‹13›‹q›‹\pointer[330]‹q››;
deq \reduce‹13›‹q›‹\pointer‹q››
  \end{code}
\end{frame}

\begin{frame}[fragile,t]{Imperative queues: enqueue to empty}
  \place[anchor=south]{$ (current page.south) + (0,2em) $}{
    \begin{tikzpicture}[rem,node distance=15pt]
      \uncover<0>{
        \node[ref] (rnil) {\phantom{X}};
        \nil{rnil};
        \cons[left=of rnil] {c3} {3} {\pointer{rnil}};
        \node[ref,left=of c3] (r3) {\pointer{c3}};
        \cons[left=of r3] {c2} {2} {\pointer{r3}};
        \node[ref,left=of c2] (r2) {\pointer{c2}};
        \cons[left=of r2] {c1} {1} {\pointer{r2}};
      }
      \node[ref,above=of c1] (front) {\phantom{X}};
      \nil{front};
      \node[ref,right=of front] (rear) {\phantom{X}};
      \nil{rear};
      \cons[above=of front] {q} {\pointer{front}} {\pointer{rear}};
    \end{tikzpicture}
  }
  \newcommand\vx{\reduce{3}{x}{1}}
  \newcommand\vq{\reduce{3}{q}{\pointer[330]{q}}}
  \begin{code}
(\ALT‹4-›‹›‹\K‹fun› \HI‹2›‹x› \HI‹2›‹q› -> ›\K‹match› \reduce‹8›‹!\reduce‹6›‹(\vq.rear)›‹\pointer[330]‹rear›››‹\HI‹9›‹Nil›› \K‹with›
 | Cons(_, r) ->
    r := Cons(\vx, \REF Nil);
    \vq.rear := !r
 | \HI‹9›‹Nil› ->
    \vq.front := Cons(\vx, \REF Nil);
    \vq.rear := !(\vq.front))\ALT‹4-›‹›‹ 1 \pointer[330]‹q››;
enq 2 \pointer[330]‹q›;
enq 3 \pointer[330]‹q›;
deq \pointer‹q›
  \end{code}
\end{frame}

\begin{frame}[fragile,t]{Imperative queues: enqueue to empty}
  \place[anchor=south]{$ (current page.south) + (0,2em) $}{
    \begin{tikzpicture}[rem,node distance=15pt]
      \uncover<0>{
        \node[ref] (rnil) {\phantom{X}};
        \nil{rnil};
        \cons[left=of rnil] {c3} {3} {\pointer{rnil}};
        \node[ref,left=of c3] (r3) {\pointer{c3}};
        \cons[left=of r3] {c2} {2} {\pointer{r3}};
      }
      \uncover<3->{
        \node[ref,left=of c2] (r2) {\phantom{X}};
        \nil{r2};
      }
      \uncover<5->{
        \cons[left=of r2] {c1} {1} {\pointer{r2}};
      }
      \node[ref,above=of c1] (front) {\reduce{9}{\phantom{X}}{\pointer{c1}}};
      \uncover<-8>{
        \nil{front};
      }
      \node[ref,right=of front] (rear) {\reduce{18}{\phantom{X}}{\pointer{c1}}};
      \uncover<-17>{
        \nil{rear};
      }
      \cons[above=of front] {q} {\pointer{front}} {\pointer{rear}};
    \end{tikzpicture}
  }
  \begin{code}
\ALT‹10-›‹›‹\reduce‹9›‹\reduce‹7›‹\pointer[60]‹q›.front›‹\pointer[60]‹front›› := \reduce‹5›‹Cons(1, \reduce‹3›‹\REF Nil›‹\ppointer[310]‹r2››)›‹\pointer[225]‹c1›››‹()›;›
\ALT‹19-›‹›‹\reduce‹18›‹\reduce‹16›‹\pointer[45]‹q›.rear›‹\pointer‹rear›› := \reduce‹14›‹!\reduce‹12›‹(\pointer[315]‹q›.front)›‹\pointer[315]‹front›››‹\pointer‹c1›››‹()›;›
\HI‹20›‹enq› 2 \pointer[330]‹q›;
enq 3 \pointer[330]‹q›;
deq \pointer‹q›
  \end{code}
\end{frame}

\begin{frame}[fragile,t]{Imperative queues: enqueue to non-empty}
  \place[anchor=south]{$ (current page.south) + (0,2em) $}{
    \begin{tikzpicture}[rem,node distance=15pt]
      \uncover<0>{
        \node[ref] (rnil) {\phantom{X}};
        \nil{rnil};
        \cons[left=of rnil] {c3} {3} {\pointer{rnil}};
        \node[ref,left=of c3] (r3) {\pointer{c3}};
        \cons[left=of r3] {c2} {2} {\pointer{r3}};
      }
      \node[ref,left=of c2] (r2) {\phantom{X}};
      \nil{r2};
      \cons[left=of r2] {c1} {1} {\pointer{r2}};
      \node[ref,above=of c1] (front) {\pointer{c1}};
      \node[ref,right=of front] (rear) {\pointer{c1}};
      \cons[above=of front] {q} {\pointer{front}} {\pointer{rear}};
    \end{tikzpicture}
  }
  \newcommand\vx{\reduce{3}{x}{2}}
  \newcommand\vq{\reduce{3}{q}{\pointer[330]{q}}}
  \begin{code}
(\ALT‹4-›‹›‹\K‹fun› \HI‹2›‹x› \HI‹2›‹q› -> ›\K‹match› \reduce‹8›‹!\reduce‹6›‹(\vq.rear)›‹\pointer[330]‹rear›››‹\HI‹9-11›‹\pointer[60]‹c1››› \K‹with›
 | \HI‹9-›‹Cons(_, r)› ->
    \reduce‹11›‹r›‹\pointer‹r2›› := Cons(\vx, \REF Nil);
    \vq.rear := !\reduce‹11›‹r›‹\pointer[315]‹r2››
 | Nil ->
    \vq.front := Cons(\vx, \REF Nil);
    \vq.rear := !(\vq.front))\ALT‹4-›‹›‹ 2 \pointer[330]‹q››;
enq 3 \pointer[330]‹q›;
deq \pointer‹q›
  \end{code}
\end{frame}

\begin{frame}[fragile,t]{Imperative queues: enqueue to non-empty}
  \place[anchor=south]{$ (current page.south) + (0,2em) $}{
    \begin{tikzpicture}[rem,node distance=15pt]
      \uncover<17->{
        \node[ref] (rnil) {\phantom{X}};
        \nil{rnil};
        \cons[left=of rnil] {c3} {3} {\pointer{rnil}};
      }
      \uncover<3->{
        \node[ref,left=of c3] (r3) {\reduce{17}{\phantom{X}}{\pointer{c3}}};
      }
      \uncover<3-16>{
        \nil{r3};
      }
      \uncover<5->{
        \cons[left=of r3] {c2} {2} {\pointer{r3}};
      }
      \node[ref,left=of c2] (r2) {\reduce{7}{\phantom{X}}{\pointer{c2}}};
      \uncover<-6>{
        \nil{r2};
      }
      \cons[left=of r2] {c1} {1} {\pointer{r2}};
      \node[ref,above=of c1] (front) {\pointer{c1}};
      \node[ref,right=of front] (rear)
            {\reduce{17}{\reduce{14}{\pointer{c1}}{\pointer[330]{c2}}}{\pointer[330]{c3}}};
      \cons[above=of front] {q} {\pointer{front}} {\pointer{rear}};
    \end{tikzpicture}
  }
  \begin{code}
\ALT‹8-›‹›‹\reduce‹7›‹\pointer[60]‹r2› := \reduce‹5›‹Cons(2, \reduce‹3›‹\REF Nil›‹\ppointer[315]‹r3››)›‹\pointer[300]‹c2›››‹()›;›
\ALT‹15-›‹›‹\reduce‹14›‹\reduce‹12›‹\pointer‹q›.rear›‹\pointer[225]‹rear›› := \reduce‹10›‹!\pointer[315]‹r2››‹\pointer[315]‹c2›››‹()›;›
\ALT‹18-›‹›‹\reduce‹17›‹enq 3 \pointer[330]‹q››‹()›;›
\HI‹19›‹deq› \pointer‹q›
  \end{code}
\end{frame}

\begin{frame}[fragile,t]{Imperative queues: dequeue}
  \place[anchor=south]{$ (current page.south) + (0,2em) $}{
    \begin{tikzpicture}[rem,node distance=15pt]
      \node[ref] (rnil) {\phantom{X}};
      \nil{rnil};
      \cons[left=of rnil] {c3} {3} {\pointer{rnil}};
      \node[ref,left=of c3] (r3) {\pointer{c3}};
      \cons[left=of r3] {c2} {2} {\pointer{r3}};
      \node[ref,left=of c2] (r2) {\pointer{c2}};
      \cons[left=of r2] {c1} {1} {\pointer{r2}};
      \node[ref,above=of c1] (front) {\pointer{c1}};
      \node[ref,right=of front] (rear) {\pointer[330]{c3}};
      \cons[above=of front] {q} {\pointer{front}} {\pointer{rear}};
    \end{tikzpicture}
  }
  \begin{code}
\ALT‹4-›‹ ›‹(\K‹fun› q -> ›\K‹match› \reduce‹8›‹!\reduce‹6›‹(\reduce‹3›‹q›‹\pointer‹q››.front)›‹\pointer[15]‹front›››‹\HI‹9-›‹\pointer[90]‹c1››› \K‹with›
 | \HI‹9-›‹Cons(x, r)› ->
   \reduce‹3›‹q›‹\pointer[60]‹q››.front := !\reduce‹11›‹r›‹\pointer‹r2››;
   \K‹if› !\reduce‹11›‹r›‹\pointer[60]‹r2›› = Nil \K‹then› \reduce‹3›‹q›‹\pointer[330]‹q››.rear := Nil;
   Some \reduce‹11›‹x›‹1›
 | Nil -> None\ALT‹4-›‹›‹) \pointer‹q››
  \end{code}
\end{frame}

\begin{frame}<1-16>[fragile,t]{Imperative queues: dequeue}
  \place[anchor=south]{$ (current page.south) + (0,2em) $}{
    \begin{tikzpicture}[rem,node distance=15pt]
      \node[ref] (rnil) {\phantom{X}};
      \nil{rnil};
      \cons[left=of rnil] {c3} {3} {\pointer{rnil}};
      \node[ref,left=of c3] (r3) {\pointer{c3}};
      \cons[left=of r3] {c2} {2} {\pointer{r3}};
      \uncover<-15>{
        \node[ref,left=of c2] (r2) {\pointer{c2}};
        \cons[left=of r2] {c1} {1} {\pointer{r2}};
      }
      \node[ref,above=of c1] (front) {\reduce{7}{\pointer{c1}}{\pointer{c2}}};
      \node[ref,right=of front] (rear) {\pointer[330]{c3}};
      \cons[above=of front] {q} {\pointer{front}} {\pointer{rear}};
    \end{tikzpicture}
  }
  \begin{code}
\ALT‹8-›‹›‹\reduce‹7›‹\reduce‹5›‹\pointer[60]‹q›.front›‹\pointer[60]‹front›› := \reduce‹3›‹!\pointer[15]‹r2››‹\pointer[345]‹c2›››‹()›;›
\ALT‹15-›‹›‹\reduce‹14›‹\K‹if› \reduce‹12›‹\reduce‹10›‹!\pointer[60]‹r2››‹\pointer‹c2›› = Nil›‹false› \K‹then› \pointer[300]‹q›.rear := Nil›‹()›;›
Some 1
  \end{code}
\end{frame}

\begin{frame}{The moral}
  \begin{itemize}
    \item<+-> Mutable data structures can lead to asymptotic efficiency
    improvements
    \item<+-> But hard to get right
      \begin{itemize}
        \item<+-> Have to think about aliasing
        \item<+-> Effects are non-local
        \item<+-> Invariants increase in importance
        \item<+-> Now we can have cycles
      \end{itemize}
    \item<+-> Use when you must; avoid when you can
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Is it possible to just avoid state?}
  \begin{itemize}
    \item<+-> Yes (if single-threaded)
    \item<+-> Just pass old values of references and return new:
      \begin{itemize}
        \item ^FunQueue.enq : 'a -> 'a queue -> 'a queue^
        \item ^ImpQueue.enq : 'a -> 'a queue -> unit^
      \end{itemize}
    \item<+-> In general we can pass a dictionary around (but efficiency)
  \end{itemize}
\end{frame}

\begin{frame}{When to use state}
  \begin{itemize}
    \item Try functional first
    \item Sometimes that's not good enough (maybe big-O)
    \item Encapsulate it behind an interface
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Arrays and mutable fields}
  \uncover<+->{
  In addition to refs, OCaml provides mutable record fields and arrays.
  }
  \begin{itemize}
    \item<+-> ^\K‹type› 'a ref = { \K‹mutable› contents : 'a }^
      \begin{itemize}
        \item<+-> ^!r^ is sugar for ^r.contents^
        \item<+-> ^r := v^ is sugar for ^r.contents <- v^
      \end{itemize}
    \item<+-> ^\K‹type› 'a array^
      \begin{itemize}
        \item<+-> ^Array.create : len:int -> 'a -> 'a array^
        \item<+-> ^a.(i)^
        \item<+-> ^a.(i) <- 9^
      \end{itemize}
  \end{itemize}
\end{frame}

\end{document}
