# CS51 Section 4 Notes

This section reviews Problem Set 3 and looks forward to Problem Set 4. 

## Problem Set 3 Talking Points

Problem set 3 asked students to implement `Bignum`, teaching them about pattern matching on records and record manipulation. Because there's a fair amount to cover for Problem Set 4 prep, this code review reviews the `times` function from Problem Set 3. 

#### `times` version 1

This solution is actually incorrect, in addition to being poorly designed. Potential problems that students might/should note:

- Reliance on Int module functions, when the whole point of bignums is that the numbers involved will be too larger for the Int module to handle.
- Exposing helper functions that exists only to power `times1` at the toplevel (i.e. `times_single1` and `times_double1`)


#### `times` version 2

This version of `times` is functionally correct, but very hard to read. Guide your students through a discussion of how/if it can be simplified and try to write comments together. 

A couple of things that would be easily correct:
	- removing the placeholder variables
	- `part`, `sum`, `exec` and `zeroes_lst` can be combined, as in the solution below, which makes the entire solution much more concise and easy to follow. 

For reference, this is what the Staff solution looks like. The approach take in `times2` is very similar algorithmically, but the student divides the code and wraps it much less cleverly. 

	let times (b1: bignum) (b2: bignum) : bignum =
	  let rec times_int (b: int list) (n: int) (carry: int) : int list =
	    match b with
	      | [] -> [carry]
	      | h::t -> 
		let prod = h * n + carry in
		(prod mod base)::(times_int t n (prod / base))
	  in
	  let rec times_helper l1 l2 (zeroes: int list) (r: bignum) : bignum = 
	    match l2 with
	      | [] -> r
	      | h::t -> 
		let term = List.rev (zeroes @ (times_int l1 h 0)) in
		times_helper l1 t (0::zeroes) (plus r {neg = false; coeffs = term})
	  in
	  let sign = not (b1.neg = b2.neg) && not (b1.coeffs = [] || b2.coeffs = []) in
	  let rev1, rev2 = (List.rev b1.coeffs, List.rev b2.coeffs) in
	  {neg = sign; coeffs = (times_helper rev1 rev2 [] (fromInt 0)).coeffs}
	;;
 

## Problem Set 4 Talking Points

In Problem Set 4, students face their first encounter with Modules, Interfaces, and Functors. The OCaml syntax for these constructs should have been introduced in last week's Lecture and Lab, and as a result, they should hopefully feel comfortable with the syntax and the underlying ideas on why we use modules, interfaces, and functors. Students may have some questions on the intracacies of Functors, which have not yet been fully introduced but will be covered in more detail this in advance of Problem Set 5, when they are expected to write their own. For now, they should just understand the idea.

The motivating example in the problem set that is not thoroughly covered in lecture or lab relates to the data structures that are used. As such, you should spend time discussing:

### Priority Queues

- Priority queues are data structures that assign both a priority ordering and a first-in first-out ordering to elements that are added to it.
- A possible explanation or motivating example is sorting/handling passengers waiting to board a plane.
- When passengers arrive to board an airplane, we assume that the airline wants to let the passenger who arrived in line first board first. Clearly this would be handled by a simple Queue (let passengers stand in line, and move through).
- Complexity arises, however, from having different classes of passengers: 
	- First class passengers have boarding priority over all passengers except other first class passengers, with whom they are "tied" in priority.
	- Business class passengers have boarding priority over all passengers except other business class passengers, with whom they are "tied", and first class passengers, who have higher priority.
	- Economy class passengers have lower boarding priority than first class or business class passengers.
- In this case, a priority queue models the problem
	- elements that are added to a priority queue have both a priority ordering and a ordering in time.
- Priority Queues can be implemented using a variety of data structures.
	- This week's problem set asks students to create several different implementations, built on top of a list, a binary tree, and a binary min-heap.

### List-based Priority queue

- Adding to the priority queue in a list is `O(n)`, since the element might have to become first based on its priority.
- "Taking" from a list-based priority queue is `O(1)`, since the element can be immediately popped from the front of the list.

### Binary Search Tree-based priority queue

- In the problem set, students create a version of a binary search tree that stores lists of elements as nodes in a binary tree.
- Reminder: binary search trees are ordered such that, for any node in the tree, all of the elements to the left and below are smaller than that node, and all of the elements to the right and below are larger than that element. 
- This allows each priority level to be node, and then for an separate queue to exist at each level of the tree, creating a priority queue.
- What are the running times here? Well, finding the correct node position is `O(log n)`, assuming that that tree is balanced, but could actually devolve in `O(n)`, since a naive binary tree doesn't necessarily stay balanced. Even after that, either insertions or pops from the queue will take `O(n)`, since the internal nodes are simply queues based on lists -- and lists in OCaml are only constant time on one end! 

### Binary Heap

The priority queue problem for this problem set is therefore best addressed by a more complex data structure, called a binary (min)heap, that enforces invariants on the data structure to make to efficient for use as a priority queue.

Like a binary search tree, a binary heap is a special case of a binary tree, with two special properties:
	1. The _weak_ invariant according to the problem set specification, or the _shape_ property from Wikipedia
		- The heap is a complete binary tree. All levels of the tree are fully filled, except possibly the lowest level, which is always filled from left to right.
	2. The _strong_ invariant according to the problem set specification, or the _heap_ property according to Wikipedia
		- For a min heap, every node is less than or equal to all of its children.
		- For a max heap, every node is greater than or equal to all of its children.

The effect is that a binary minheap can be used to implement a priority queue, since we assume that highest priority entries into the queue will have the lowest number (i.e. priority number 1 is the highest), and the data structure also preserves the ordering of entries that have the same priority. 

### Binary Heap Operations

Like any priority queue, the operations we're concerned with for a binary heap are `put` and `pop`. This operations are reasonably more complicated, however, since there are rules for how the structure needs to change after additions or removals depending on the current state. 

Imagine that we have the numbers `10, 6, 6, 5, 11, 1` that we want to add to a binary heap. Imagine that we add all of these to the binary heap. Here's what this series of additions would look like. 

![Step 1](stage1.png)
![Step 2](stage2.png)
![Step 3](stage3.png)
![Step 4](stage4.png)
![Step 5](stage5.png)
![Step 6](stage6.png)

Here's an example of one "pop and fix" loop. 

![Step 7](stage7.png)

Since the `1` node has the highest priority (i.e. is sitting at the top of the tree), it is removed. The rest of the tried is then "fixed" so that the `5` moves upwards, which happens because `5` is smaller than `6` and moving up `6` would violate the invariant. The 6 then moves up, as well. Ties between siblings are broken to the left, so that the element that was added first always moves to the top of the heap first. 

There are more cases to handle but this should help students get comfortable with the operations that they are expected to take on with Binary Heaps in Problem Set 4. 

















