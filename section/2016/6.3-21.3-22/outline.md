# CS51 Section 5 Notes

This section reviews Problem Set 4 and looks forward to Problem Set 5. 
* Finding the right balance between review and pset overview is very important.  Do your best to do a good introduction of 2-3 trees with the students! *

## Problem Set 4 Talking Points
A lot of the code is understanding the data structure and the solutions fall out from there.  Emphasize that understanding conceptually the pset before putting "code onto paper" often made the pset easier.

### Review Notes `BinSTree`

#### Example 1: `search`

* `search1`
	* Not understanding that just because an element is comparably `EQUAL` that is not the same thing as finding the actual element.  That's why we chose to store elements in lists. 
	* A potential example is `Comparable` module counting the length of strings, but when searching for an element, the entire string has to match.
* `search2`
	* Searches the entire list, but can be done a bit more cleanly.  
* `search3`
	* Using List.mem is a more clean implementation for finding an element.

#### Example 2: `get_min` and `get_max`

* `get_min1` and `get_max1`
	* Correct implementation, but lots of repeat code.
* `get_min2` and `get_max2`
	* Factors out repeating the reversing of the list.
	* Also note that get_min relies on pull_min, which was written for the student.

### Review Notes `PRIORQUEUE`

#### Example 3: `add` and `take`

* `add1` and `take1`
	* Have the students talk about the time complexity of this implementation.
	* It is O(n) for both functions.
* `add2` and `take2` 
	* Fixed implementation.  
	* Think about the running time for the algorithms that you're implementing.

### Review Notes `Binary Heap`

#### Example 4: `fix`

There are not a lot of design decisions that existed within this part of the pset as long as the students understood the way the data structure works.  Show how fix maintains the invariants of the tree.

* `fix`
	* Note the use of `get_top` and the factoring out of code to `replace_root`.
	* Ask the students how well they understood the data structure.
  * As an exercise, guide the students from the code provided in the review, to a potentially cleaner solution where you write a compare3 function as show below.  This removes the need for the nested match statements.  Do no just show this code to the students, as it is directly from the pset solution.

``` ocaml
  type dir = Left | Right | Neither

  let compare3 (e1 : elt) (e2 : elt) (e3 : elt) =
    match C.compare e2 e3 with
    | Less | Equal ->
      (match C.compare e1 e2 with
      | Less | Equal -> Neither
      | Greater -> Left)
    | Greater ->
      match C.compare e1 e3 with
      | Less | Equal -> Neither
      | Greater -> Right

  let swap (e : elt) (t : tree) =
    match t with
    | Leaf _ -> Leaf e
    | OneBranch (_,e1) -> OneBranch (e,e1)
    | TwoBranch (b,_,t1,t2) -> TwoBranch (b,e,t1,t2)

  let rec fix (t : tree) : tree =
    match t with
    | Leaf _ -> t
    | OneBranch (e1,e2) ->
      (match C.compare e1 e2 with
      | Less | Equal -> t
      | Greater -> OneBranch(e2,e1))
    | TwoBranch (b,e,t1,t2) ->
      let top1, top2 = get_top t1, get_top t2 in
      match compare3 e top1 top2 with
        | Neither -> t
        | Left -> TwoBranch(b, top1, fix (swap e t1), t2)
        | Right -> TwoBranch(b, top2, t1, fix (swap e t2))
```

## Problem Set 5 Talking Points

Also make sure that you understand and can explain the crawl function, as that helps explain the larger picture of the pset.  Emphasize when looking at crawl, the assistence that abstraction of `WordDict` and `LinkSet` provide.  This will probably take most of section time.

N.B: this outline was edited to remove 2-3 trees, since they are no longer covered on the problem set. 

#### Quick Overview of `crawl` (remember these notes are for TF's eyes)

Look over the solution for crawl:
``` ocaml
let rec crawl (n:int) (frontier: LinkSet.set)
    (visited : LinkSet.set) (d:WordDict.dict) : WordDict.dict =
  if n <= 0 then d
  else
    match LinkSet.choose frontier with
      | None -> d
      | Some (link, new_frontier) ->
        if LinkSet.member visited link then
          crawl n new_frontier visited d
        else
          let visited = LinkSet.insert link visited in
          match get_page link with
            | None -> crawl n new_frontier visited d
            | Some page ->
              let new_new_frontier =
                List.fold_left page.links ~f:(fun s x -> LinkSet.insert x s) ~init:new_frontier in
              let insert_word d' w =
                (match WordDict.lookup d' w with
                  | None ->
                    WordDict.insert d' w (LinkSet.singleton page.url)
                  | Some s ->
                    WordDict.insert d' w (LinkSet.insert page.url s)) in
              let d' = List.fold_left page.words ~f:insert_word ~init:d in
              crawl (n - 1) new_new_frontier visited d'
```
Things to discuss with the students.  Web crawling looks through all the links referenced in an initial page.  All the links in that page are added to a set that is the frontier for further crawling.  We also pass in a visit set of links so that the same link is not visited multiple times.  Once a new page is reached, the student must 1) add all the links on that page to the frontier and 2) add all the words on the page to the `WordDict`.  If the limit of pages to visit has not be exhausted, and the frontier is not empty, comtinue crawling.  Allow the students to ask questions and work their way through this process themselves.  But make sure they understand the general process of webcrawling.  Perhaps map out a sample network for them as well.

Based on this picture, for example, starting at the website goog, all websites can be crawled.  Starting at the website grass, there are no links to other sites.  Additionally, if you associate a small text file with each of these sites, you can sketch out the format of the returned WordDict.

![Sample Network](sample_web.png)