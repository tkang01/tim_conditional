# CS51 Section 7 Notes

This section reviews references from lab 5 and also goes a little bit more in depth about problem set 5.  We give you a little more leeway in this section to go over what you think is important, because there is not a review of a previous pset, and you should have talked a little about pset5 in the previous weeks.

## Lab 5 Talking Points
 
### `rememeber`

Note that this is a concept that Stuart introduced in lecture about creating a reference only one time, and then having remember return a function that then uses that reference.  Some good talking points are the problems with scoping in the first example, and talking about how dangerous it could be to leave these refs as globally scoped.

### `gensym`

Talk about the fact that you can still use the same `let <var> = <exp>` syntax with references that you have used in the past.  Also demostrate more clearly the way that `;` usage works by having the students modify the `ctr` variable multiple times.  Make it clear that whatever functions precede a semicolon must return unit.

### `mappend`

Talk about an implementation that could be done in a more functional way.  In that the function would return a new mlist that was the two concatenated onto one another.  Talk about the advantages of that and disadvantages.  Also mention that this setup makes it so cycles can appear. Draw a visual representation of a cycle, and begin coaxing people to the idea of infinite data structures (potentially). 
 
## Problem Set 5 Talking Points

You should have already gone over crawl in code review last week.  

This week spend some time talking about the various abstraction layers within dictionaries and sets in this pset.  For example, in the pset we are using BSTrees to implement dictionaries, but we could change that implementation without anyone realizing it.  (use the fact that * we did* change the implementation of dicts for this pset, but it didn't matter in their code as an example).  Then talk about the fact that there's another abstraction layer with crawl, that you can do the same changing of implementation for sets, and it makes no difference for the rest of the code.

Also, to reiterate a point that people may be forgetting, as the psets get larger, testing is really important.  Emphasize the guarantees that testing delivers to students in part 2.  If we know that dictionaries have been unit tested (assumed), then we can do all of our unit tests for sets knowing that the functions for the D module are water tight.

Talk a little bit about part 3, but remain vague.  Point out the relevant code in moogle.ml that times crawl and query.