# CS51 Section 2: 2/14-2/16

This section reviews Problem Set 1 and looks forward to Problem Set 2.

## Problem Set 1 Talking Points

Since higher order functions are being introduced in lecture, we will look to see where code can be cleaned up from Problem Set 1.  We will also look at some places where code is redundant 

### Review Notes
These examples are all adequate solutions to the problems. Oftentimes, since these examples are new to most of the students, they may see a new form of syntax or think about a problem in a different way.  Emphasize discussion about the following examples, and try to let the students do a lot of their own analysis.


#### Example 1 `variance`

This example features three different ways to implement variance.  Things to discuss are:

* `variance1`
	* Splitting the code into bite sized chunks, easy to understand
	* Must iterate throught the list twice, first to get the sum and then to get the count.
	* _Exercise 1_: Redo the average function so that the code iterates through the list one time to get both the sum and the length of the list.
		* This is useful because the code is more efficient.
		* This would be less useful if you thought that you would use a length or count function multiple times, independent of one another.
* `variance2`
	* Very similar to `variance1`, but the change to the order of the smaller chunks makes scoping more clear.  
	* Talk about which functions you would want to do this with, and when you would not.
* `variance3`
	* Uses higher order functions to make the code much more compact.
	* Still not incredibly efficent.
	* _Exercise 2_: Rewrite `variance3` using the List modules's higher order functions. 

#### Example 2 `to_run_length`

This example mostly shows that there are two ways of doing the problem that are the same complexity, but may demonstrate a different way of thinking than other students considered.  Open this up to discussion.  Some talking points

* `to_run_length1`
	* Helper function is called every time a new value is encountered, and then continues to iterate through the list.
* `to_run_length2`
	* Looks at the return result from its own recursive call.  Then the matching is a little bit safer.

## Problem Set 2 Talking Points
Problem Set 2 contains two parts, mapfold and expressions.  In mapfold they have to understand higher order functions and how to apply them.  Go over how to write fold and map so people get a sense of what the functions actually do.  You can use the code in the code review as reference.  Expressions allows the students to write a derivative function.  Go over the general structure of the release code. Emphasize the built-in functions that already exist, like `sin` and `cos` as some students might be unaware of them.  For `Binop` and `Unop` types, go into how they are defined if the students are confused.  Perhaps use the board to write a sample expression, and then convert that expression into the expressions types we have defined for them.
