# CS51 Section 9

## Problem Set 6 Code Review

Problem Set 6 introduces students to mutable state and to streams/infinite data structures. 

1. Have students read over `has_cycle`, `flatten`, and `mlength`. 
	- They should notice that identical `List.fold_right` calls are included in all 3 and could be factored into a separate function
	- Have them discuss the structural similarities in the 3 functions, and talk about if and how code reuse could be minimized. 
	- The idea here is that all of these functions have a `fold`-like quality about them -- writing fold so that the necessary cycle-finding properties are maintained is a little too convoluted an exercise for section, but having the students talk about the structural similarities is a useful exercise.

2. For exercise 2, discuss simplification of the example merge function. Here's the staff solution as a reference implementation. 

	```{ocaml}
	let merge (s1:int stream) (s2:int stream) : int stream =
  	let rec helper (s1:int stream) (s2:int stream) (v:int) : int stream =
    	let (Cons(v1,t1),Cons(v2,t2)) = (s1,s2) in
      	if v1 < v2 then
	if v1 = v then helper (Lazy.force t1) s2 v
	else Cons(v1, lazy(helper (Lazy.force t1) s2 v1))
      else
	if v2 = v then helper s1 (Lazy.force t2) v
	else Cons(v2, lazy(helper s1 (Lazy.force t2) v2)) in
  	let (Cons(v1,_),Cons(v2,_)) = (s1,s2) in
  	let v = (min v1 v2) - 1 in
    	helper s1 s2 v
	;;
	```

## Lab 7 Review/Post-mortem