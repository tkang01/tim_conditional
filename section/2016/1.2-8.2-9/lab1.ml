(* 

			      CS51 Lab 1
		     Basic Functional Programming

Objective:

This lab is intended to get you thinking about core concepts
introduced in class, including: 

    concrete versus abstract syntax
    basic and complex types
    functional programming, both first-order and higher-order

*)

(*====================================================================
Part 0: Testing your Vocareum interaction

Labs and problem sets in CS51 are submitted using the Vocareum
system. By now, you should be set up with Vocareum.

......................................................................
Exercise 1: To make sure that the setup works, submit this file,
just as is, under the filename "lab1.ml" to the Lab 1 assignment on
the CS51 Vocareum web site.
......................................................................

When you submit labs (including this one) Vocareum will check that the
submission compiles cleanly, and if so, will run a set of unit tests
on the submission. For this part 0 submission, most of the unit tests
will fail (as you haven't done the exercises yet). But that's
okay. You still got 100% of the points! Congratulations! (That's what
we mean when we say "labs are graded on effort, not result.")

Now let's get back to doing the remaining exercises so that more of
the unit tests pass.

......................................................................
Exercise 2: So that you can see how the unit tests in labs work,
replace the "failwith" expression below with the integer 42, so that
exercise2 is a function that returns 42 (instead of failing). When you
submit, the Exercise 2 unit test should then pass.
......................................................................  *)

let exercise2 () = 42;;

(*
Note that your grade on labs is not dependent on your passing the unit
tests. You'll see that even when a unit test is marked as passed, it
is still given 0 out of 0 points. The points on labs come from making
a good faith effort to do the labs (as evidenced by the submission
itself), for which you'll see points being supplied.

(Of course, the situation is different for problem sets. There, we
grade both on correctness of the code (as evidenced by passing the
unit tests) and the quality of the code (based on code review from the
instructional staff).)
*)

(* From here on, you'll want to test your lab solutions locally before
submitting them at the end of lab to Vocareum. A simple way to do that
is to cut and paste the exercises into an OCaml interpreter loop. Or
you can feed the whole file to OCaml with the command:

    % ocaml < lab1.ml

to see what happens.
*)

(*====================================================================
Part 1: Concrete versus abstract syntax

In class, we distinguished concrete from abstract syntax. Abstract
syntax corresponds to the substantive tree structuring of expressions,
concrete syntax to the particulars of how those structures are made
manifest in the language's textual notation.

......................................................................
Exercise 3: Consider the following abstract syntax tree:

      -
      |
      |
      -
      ^
     / \
    /   \
   5     3

(that is, the negation of the result of subtracting 3 from 5). How
might this be expressed in the concrete syntax of OCaml using the
fewest parentheses? Replace the "failwith" expression with the
appropriate OCaml expression to assign the value to the variable
exercise1 below.
......................................................................
*)

(*
let exercise3 = 2;;
*)

(* The OCaml concrete expression

   - 5 - 3

does *not* correspond to the abstract syntax above.

......................................................................
Exercise 4: Draw the tree that it does correspond to. Check it with a
member of the course staff if you'd like.
	+
	|
	|
	-
	^
   / \
  /	  \	
 3     5
 
......................................................................
*)

(*====================================================================
Part 2: Types and type inference

......................................................................
Exercise 5: What are appropriate types to replace the ??? in the
expressions below? Test your solution by uncommenting the examples and
verifying that no typing error is generated.
........................................................................
*)


let exercise5a : int = 42 ;;

let exercise5b : string =
  let greet y = "Hello " ^ y
  in greet "World!";;

let exercise5c : int * float -> int  =
  fun (x, y) -> x + int_of_float y ;;

let exercise5d : int -> bool =
  fun x -> x < x + 1 ;;

let exercise5e : bool -> bool list =
  fun x -> if x then [x] else [] ;;

let exercise5f : int option list =
  [Some 4; Some 2; None; Some 3] ;;

let exercise5g : (int option * float option) * bool = 
  ((None, Some 42.0), true) ;;

(* Part 3: First-order functional programming

We'll start with some simple "finger exercises" defining simple
functions like those from class before moving onto more complex
problems. As a reminder, here's the definition for the length function
of type int list -> int:

let rec length (lst : int list) : int =
  match lst with 
  | [] -> 0
  | head :: tail -> 1 + length tail ;;

......................................................................
Exercise 6: Define a function square_all that squares all of the
elements of an integer list by editing the template code below. Then
apply that function to a list containing the elements 3, 4, and 5,
calling the result exercise6.
......................................................................

Can't get this function to work.

let rec square_all (lst : int list) : int list =
  match lst with 
  | [] -> []
  | h::t -> (h*h) :: square_all t;;


let exercise6 =
  square_all[3,4,5] ;;

*)
(*
......................................................................
Exercise 7: Define a recursive function that sums an integer
list. (What's a sensible return value for the empty list?)
......................................................................
 *)

let rec sum (lst : int list) : int =
  match lst with
  |[] -> 0
  |h::t -> h + sum t;;
  
(*
......................................................................
Exercise 8: Define a recursive function that returns the maximum
element in an integer list, or None if there are no elements in the
list.
......................................................................
 *)

let max_list (lst : int list) : int option =
  match lst with
  |[] -> None
  |h::t -> Some[h + max_list t];;
(*
......................................................................
Exercise 9: Define a function zip, that takes two int lists and
returns a list of pairs of ints, one from each of the two argument
lists. Make sure that the function handles cases of mismatched list
lengths by returning None in that case. For example, zip [1;2;3]
[4;5;6] should evaluate to Some [(1, 4); (2, 5); (3, 6)].

Why wouldn't it be possible, in cases of mismatched length lists, to
just pad the shorter list with, say, false values, so that,
zip [1] [2; 3; 4] = [(1, 2); (false, 3); (false, 4)]?
......................................................................  
*)

let zip (x : int list) (y : int list) : ((int * int) list) option =
   ;;

(*
......................................................................
Exercise 10: Recall the definition of the function prods from lecture
(duplicated below).

Using sum, prods, and zip, define a function dotprod that
takes the dot product of two integer lists (that is, the sum of the
products of corresponding elements of the lists). For example, you
should have:

# dotprod [1;2;3] [0;1;2] ;;
- : int option = Some 8
# dotprod [1;2;3] [1;2;3;4] ;;
- : int option = None

Even without looking at the code for the functions, carefully looking
at the type signatures for zip, prods, and sum should give a good idea
of how you might combine these functions to implement dotproduct.
......................................................................
*)

let rec prods (lst : (int * int) list) : int list =
  match lst with
  | [] -> []
  | (x, y) :: tail -> (x * y) :: (prods tail) ;;

let dotprod (a : int list) (b : int list) : int option =
  failwith "dotprod not implemented" ;;

(*====================================================================
Part 3: High-order functional programming with map, filter, and fold

In these exercises, you should use the built-in functions map, filter,
and fold_left and fold_right provided in the OCaml Pervasives module
to implement these simple functions.

......................................................................
Exercise 11: Reimplement sum using fold_left.
......................................................................
*)

let sum_ho (lst : int list) : int =
  failwith "sum_ho not implemented" ;;

(*
......................................................................
Exercise 12: Reimplement prods using map.
......................................................................
*)

let prods_ho (lst : (int * int) list) : int list =
  failwith "prods_ho not implemented" ;;
  
(*
......................................................................
Exercise 13: The OCaml List module provides, in addition to the map,
fold_left, and fold_right higher-order functions, several other useful
higher-order list manipulation functions. For instance, map2 is like
map, but takes two lists instead of one along with a function of two
arguments and applies the function to corresponding elements of the
two lists to form the result list. Use map2 to reimplement zip.
......................................................................
*)

let zip_ho (x : int list) (y : int list) : ((int * int) list) option =
  failwith "sum_ho not implemented" ;;

(*
......................................................................
Exercise 14: Define a function evens that returns a list of all of the
even numbers in its argument list.
......................................................................  
 *)

let evens : int list -> int list =
  fun _ -> failwith "evens not implemented" ;;
