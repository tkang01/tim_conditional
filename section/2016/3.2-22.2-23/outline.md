# CS51 Section 3 Notes

This section reviews Problem Set 2 and looks forward to Problem Set 3. 

## Problem Set 2 Talking Points

Problem Set 2 introduces students to the List module, whose functions will play a consistent role in the course, as well as has students experiment for the first time with reading and using composed algebraic data types. The pset is divided into 2 parts, `mapfold.ml` and `expression.ml`, and therefore so are the notes.

### `mapfold` Review Notes

The goal of this review is to help students learn to identify when fold, map, and filter have been inefficiently used. Examples of problems in the source code include using map and filter when fold would make the code cleaner or more efficient, writing unnecessary helper functions to wrap binary operators or binary functions for folds, not reusing previously written functions, or having to throw exceptions as a result of not using fold in favor of map and filter. 

#### Example 1: `deoptionalize`
This example features 3 different implementations of the `deoptionalize` function, which takes `'a option list` and returns a `'a list`. Things to discuss in these examples:

- `deoptionalize1`
	- Requires raising an exception under some path.
	- Requires multiple passes over the list (uses filter + map rather than fold)
- `deoptionalize2`
	- Seems like it would be better, since it uses anonymous functions and condenses the solution to one line, but actually follows the same approach as `deoptionalize1`.
	- Uses a `match` statement instead of a `let`, even though the invariant it expects at that point only allows 1 case (this should be a hint to students that something is wrong).
	- Compiles with a warning because of the incomplete match.
- `deoptionalize3`
	- This shares many of the shortcomings of deoptionalize1, only masked in anonymous functions.
	- Still have to raise an Exception that will never be call in order to make the compiler not complain.
- `deoptionalize4` 
	- This is the solution that students should consider correct. 


#### Example 2: `concat`, `sum`, and `sum_rows`

This example illustrates how to use binary operators without wrapping them inside a helper function.

- `concat1` and `concat2` show how to use the (@) operator
- For `sum` and `sum_rows`, help the students generalize to remove the helper functions from `sum` and then from `sum_rows`

Exercises
	1. Have students write `sum2` based on what they've just said about `concat` and `sum1`
	2. Have students write `sum_rows2` based on same. 


#### Example 3: `num_occurs`, `filter_range`, and `filter_by_year`

- `num_occurs` 
	-  This is a similar `filter` + `map` vs `fold` conflict -- the `num_occurs` examples should illustrate how much cleaner a `fold` based solution can be. 
- `filter_range` 
	- The two implementations here have separate inefficiencies that complement each other.
	- In `filter_range1`, the implemenation uses 2 separate functions, which is clearly more code than is required.
	- In `filter_range2`, the solution doesn't _seem_ to be doing anything too wrong -- but when the condition is compared to the invariant that the `range` tuple will always be in the form `(min,max)`, the condition before the `||` is never necessary. 

### `expresion` notes 

Not a lot to review in `expression`, but a few points: 

- `contains_var`
	- This implementation has unused variables.
	- There is unnecessary `if-else` structure, which is the important takeaway. 
	- Otherwise, the function relies on the structure of the type to determine what to do, which is the correct logical structure and the idea we're trying to communicate here.
- `evaluate` 
	- This example is correct. It's presented so that the students can discuss how it would be properly tested.
	- There are three tests: is that adequate? What's the benchmark supposed to be? 

## Problem Set 3 Talking Points

Problem Set 3, bignums, introduces students to record-based ADTs. You should review record syntax, and then talk through some of the mathematical concepts (especially for multiplication) and some common pitfalls or challenges you encountered with this problem set. 