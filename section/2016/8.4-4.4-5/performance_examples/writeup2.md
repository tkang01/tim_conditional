Crawler Timing Report
=====================

To test our web crawler, we created a new module named Timing (in timing.ml).
This module contains six functions:

> - **time (f : unit -> unit) : float**
> takes in a function and returns the amount of time it took to execute it.
> - **test_crawler (link: link) () : unit**
> runs the crawler with the given link as the starting point when executed.
> - **run_corpora_timing () : unit**
> tests the crawler on the three corpora (simple-html, html, and wiki) and prints out the run time of each trial.
> - **run_count_timing () : unit**
> runs the crawler on the Wikipedia corpus multiple times while varying
>   the value of max_links from 1 up to 60.
> - **test_eval_query (i:index) (q:query) : set**
> looks for the given query in the given index .
> - **run_query_timing () : unit**
 runs query evaluation on a list of queries and prints out the run time of each trial.

Crawler Results
---------------

Here's how long it took to run the crawler on each of the corpora via run_corpora_timing, using the ListSet implementation, and then the DictSet implementation. We switched implementations by switching the comments in myset.ml lines 409 and 410.  All times in seconds.

<table>
	<thead>
		<th></th>
		<th>simple-html</th>
		<th>html</th>
		<th>wiki</th>
	</thead>
		<tr>
				<td># pages</td>
				<td>7</td>
				<td>21</td>
				<td>224</td>
		</tr>
    <tr>
        <td>ListSet</td>
        <td>0.003033</td>
        <td>0.483667</td>
        <td>1133.806688</td>
    </tr>
    <tr>
        <td>DictSet</td>
        <td>0.003515</td>
        <td>0.466974</td>
        <td>334.504242</td>
    </tr>
</table>

Here's a graph for your viewing pleasure:

![Timing Results](graph1.png)

As you can see, the DictSet implementation ran in about the same time as the ListSet implementation for both the simple-html and html corpora.  DictSet was significantly faster on the wiki corpus (almost by and order of 4).  


To get some more helpful information on the relative performances of the two implementations, we used the run_count_timing to see how the two sets worked for different numbers of links.  Graph below:

![Timing Results](graph2.png)

These data show more clearly that the two implementations stay at a similar time until the link count reaches about 27.  After that, the ListSet implementation takes longer and longer to complete.  

This difference can be explained by the run time complexities of the functions in ListSet and DictSet.  As the graphs show, the time that ListSet takes grows at a greater rate than that of DictSet.  


Query Results
-------------

<table>
<thead><tr><th>Query</th><th>DictSet</th><th>ListSet</th></tr></thead><tbody>
 <tr><td>the</td><td>0.000226</td><td>0.000240</td></tr>
 <tr><td>a</td><td>0.000002</td><td>0.000002</td></tr>
 <tr><td>Teenage</td><td>0.000002</td><td>0.000003</td></tr>
 <tr><td>Mutant</td><td>0.000008</td><td>0.000002</td></tr>
 <tr><td>Disney OR figurine</td><td>0.000013</td><td>0.000031</td></tr>
 <tr><td>Peter OR video</td><td>0.000127</td><td>0.000116</td></tr>
 <tr><td>Teenage AND Mutant</td><td>0.000102</td><td>0.000009</td></tr>
 <tr><td>people AND of</td><td>0.000415</td><td>0.000024</td></tr>
</tbody></table>

For all of the single word queries DictSet took about the same time as ListSet (the slight differences can be explained by exogenous background processes running on the computer).  However, for AND and OR queries DictSet was slower, because the intersection and union functions for DictSet contained tree lookups and insertions (O(log n)) nested within a fold function (O(n)).  In contrast, ListSet implemented union and intersection by folding over both of the lists (O(n)).  



