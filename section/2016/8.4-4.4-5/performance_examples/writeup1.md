File Edit Options Buffers Tools Help
ListSet vs DictSet Performance Tests
====================================

In order to compare the efficiency of the ListSet and DictSet implementations I measured \
the time taken for both to index the three given corpora.
Given that DictSet is a binary search tree it was expected that the DictSet implementatio\
n be faster than the ListSet implementation.
I decided to also test the time taken for both ListSet and DictSet to execute on interval\
s of page numbers  as a means of gathering more information on the rate at which \
the time taken to execute increases as the index increases.


### Implementation of Crawl Timer ###
I implemented two timing functions in a	file called 'performance_test.ml', using code very similar to the timing func
tion given in moogle.ml.
To test crawl I called the crawl timer multiple times and passed in the arguments at the command line. I took the average of ten values for the crawl times on simple-html and html, but only took the average of three crawl times for the wiki corpus as it took much longer to index all 224 pages.


### Implementation of Query Timer ###
In order to time query I hardcoded in a several strings and passed in the index at the command line, so the query function would time how long it took to query a given word on a given corpora. I did this for each corpus and set implementation, and then subtracted the times of the DictSet query from the ListSet query and found the average difference in querying time between the two set implementations.


### Results ###

Average time (seconds) taken to crawl (to 6 d.p)

set 	      simple-html	    html	       wiki	
-------	    ---------------	--------------	   --------------	
DictSet        0.000756		  0.272135	     386.853607	

ListSet        0.001153		  0.392135	     977.134890

Average time (seconds) to index intervals of wiki pages (to 6 d.p)

number of pages        DictSet	          ListSet
---------------	    --------------    --------------
20		    2.168774	      0.517259
40		    5.140051	      19.796313
60		    11.766765	      46.183662
80		    17.472356	      92.526293
100		    31.138424	      158.739552
120		    55.960027	      253.633755
140		    102.515009	      386.025268
160		    165.343471	      473.878694
180		    239.770818	      632.763097
200		    307.605570	      729.658515
220		    317.617999        925.813132
240		    511.116477        980.199134



Average difference in query time was 208.109431 (x10^-6) (with ListSet having the typically higher query time)

###Conclusions###


The results for crawl time seem accurate. Initially, when the index is quite small, the difference in crawl time isn't huge and in fact from the results that compares the time taken to crawl wiki pages at intervals, ListSet is initially slightly faster. However, crawl time for ListSet increases exponentially as the index increases which is what we would expect. 

I don't think the results for the query test are as accurate or reliable as the time difference was miniscule. This very may well be a problem with the query timer I wrote. Alternatively, it may be that I wasn't testing it on a large enough corpus (I tried querying over about 80 pages of wiki) and so the search time was very small. DictSet being a binary search tree, is O(log(n)) whilst the ListSet is O(n) and so I had expected DictSet to be significantly fast in both crawl and query time.
