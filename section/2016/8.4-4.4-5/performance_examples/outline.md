## Timing

`timing1.ml` and `timing2.ml` are two good examples of performance testing that are a step above running Moogle in the terminal a couple dozen times with different arguments and writing down the results (or testing query timing by using the web interface). The reports from the two timing examples are also included as examples of what sort of analysis the performance enabled. A strongpoint of both of these examples is that the students implemented their timing code in a _separate file_ rather than needing to mess around with the `main ()` function in `moogle.ml`. 

Be sure to emphasize to students not to panic if they, for example, took the brute-force approach of running `moogle.byte` and sticking the results in an excel document. Instead, point out why the approaches below might be preferable.

####`timing1`

This implementation nicely automates query timing through `query_get_time` function, although the way the queries to test are written out is slightly clunky (it would be cleaner, for example, to iterate the timing function over a list of words). To run the crawler on different corpora or with different page counts, the students still need to re-run their testing code with different arguments. The main automation of the crawl testing is that each time the testing code is run, it crawls with the same arguments three times and prints out the results.

####`timing2`

This timing code does a lot to automate testing, and the students used the data to do some very solid analysis (see `writeup2.md` ). In particular, the students used their `run_count_timing` to find the running times of the two set implementations as a function of the number of pages to crawl, which shows the asymptotic differences between the interpretations (see `graph2.png`)  This went beyond most student's submission, most submissions, which crawled through three corpora completely and reported the absolute difference in times at these three points.

The `run_corpora_timing` and `run_query_timing` also nicely automate testing crawling over the three corpora and testing multiple queries.