open Moogle
open Crawl


(* function to time query *)
       
let querying_time time stringword=
  Printf.printf "Time to query '%s' took %f(x10^-6)seconds...\n" stringword (time *. 1000000.) ;;

let query_get_time word index stringword =
  let start = Unix.gettimeofday () in
  let query = Q.parse_words word in
  let finish = Unix.gettimeofday () in
  let time = finish -. start in
      (querying_time time stringword) ;;

(* function to time crawl *)
  
let time_crawler crawler arg =
  let start = Unix.gettimeofday () in
  let res = crawler arg in
  let finish = Unix.gettimeofday () in
  Printf.printf "Crawling took %f seconds...\n" (finish -. start);
  res ;;

(* calling functions and printing times *)
  (*
let main () =
  (* Want different random numbers every time. *)
  let _ = Random.self_init () in
  (* Construct the index to pass to the server *)
  let _ = flush_all () in
  let test1 = Printf.printf "First crawl: " in
  let print_test = time_crawler crawler () in
  let test2 = Printf.printf "Second crawl: " in
  let print_test = time_crawler crawler () in
  let test3 = Printf.printf "Third crawl: " in
  let print_test = time_crawler crawler () in
  print_test
;;
  *)
(* uncomment for query test mode *)
let main () =
  (* Want different random numbers every time. *)
  let _ = Random.self_init () in
  let _ = flush_all () in
  let querying = query_get_time ["the"] (crawler ())  "the"  in
  let querying = query_get_time ["moo"] (crawler ()) "moo" in
  let querying = query_get_time ["cow"] (crawler ()) "cow" in
  let querying = query_get_time ["Turtles"] (crawler ()) "Turtle" in
  let querying = query_get_time ["and"] (crawler ()) "and" in
  let querying = query_get_time ["the"] (crawler ()) "the" in
  let querying = query_get_time ["cartoon"] (crawler ()) "cartoon" in
  let querying = query_get_time ["Ocaml"] (crawler ()) "Ocaml" in
  querying
    ;;
main()
