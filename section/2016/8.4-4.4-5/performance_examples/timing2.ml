open Crawl ;;
open Crawler_services ;;
open Moogle ;;
open Q ;;
module PR = Pagerank ;;

let max_pages_to_search = 224

let time (f : unit -> unit) : float =
	let start_time = Unix.gettimeofday () in
	let _ = f () in
	(Unix.gettimeofday ()) -. start_time
;;

let test_crawler (link: link) (n:int) () =
  	let _ = crawl n
	(PR.LinkSet.singleton link)
	PR.LinkSet.empty
	WordDict.empty in
	()
;;

let test_eval_query (i) (q: query) () =
    let _ = eval_query i q in
		() ;;

let run_corpora_timing () : unit =
	let urls  = ["simple-html/index.html";
	  "html/index.html";"wiki/Teenage_Mutant_Ninja_Turtles";] in
	let links = List.map (fun x -> {host=""; port=80; path=x}) urls in
	let _     = print_endline "starting timing..." in
	let times = List.map (fun x -> print_endline "Testing a link...";
	  time (test_crawler x max_pages_to_search)) links in
	    List.iter (fun x -> Printf.printf "%f\n" x) times
;;

let run_query_timing () : unit =
  let querys = [Word "the"; Word "a"; Word "Teenage"; Word "Mutant";
	  Or (Word "Disney", Word "figurine"); Or (Word "Peter", Word "video");
		And (Word "Teenage", Word "Mutant"); And (Word "people", Word "of")] in
	let index = crawl 60 (PR.LinkSet.singleton
		{host=""; port=80; path="wiki/Teenage_Mutant_Ninja_Turtles"})
		  (PR.LinkSet.empty) WordDict.empty in
	let times = List.map (fun x -> print_endline "Testing a query...";
	  time (test_eval_query index x)) querys in
      List.iter (fun x -> Printf.printf "%f\n" x) times
;;

let run_count_timing () : unit =
	let link = {host=""; port=80; path="wiki/Teenage_Mutant_Ninja_Turtles"} in
	for i = 1 to max_pages_to_search do
		let time = time (test_crawler link i) in
		(* let _ = Printf.printf "%f\n" time in *)
		print_endline (string_of_float time);
	done;
;;

(* run_corpora_timing () ;; *)
(* run_count_timing   () ;; *)
run_query_timing () ;;
