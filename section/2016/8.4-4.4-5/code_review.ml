(* 
	Section 8
	Problem Set 5 Review 
*)

let intersect s1 s2 =
    D.foldl (fun s x _ -> if member s2 x then insert x s else s)
      empty s1

let choose s =
  match D.choose s with
    | None -> None
    | Some (k,_,s') -> Some (k,s')


let fold f u s = D.foldl (fun a x _ -> f a x) u s


