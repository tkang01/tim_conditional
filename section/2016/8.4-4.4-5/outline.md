# CS51 Section 8 Notes

This section reviews Problem Set 5 and looks forward to Problem Set 6. 

## Problem Set 5 Talking Points

### Part 2

The problem set was radically changed from last year to be easier, but also a little more loose in terms of interpretation.  You should have covered part 1 and 2 in previous code reviews.  However, many students still expressed confusion during office hours about the conceptual details of part 2.  Go into some detail about the concepts introduced in that part of the pset.  A few things you might want to emphasizse.

* Sets and dictionaries have the implicit similarities of storing unique keys, and dictionaries are extended to associate values with those keys.  Therefore, if you have a working implementation for dictionaries, it makes sense to use a similar implementation for sets.  
* If students are confused about the general structure of athe abstraction barrier in this part of the pset, a picture may help to clarify things for them.  This is one possible way to represent how the different modules are interact.

![example drawing](barrier.png)

* Many of the functions for sets are direct translations to dictionaries, like empty.  Functions like fold or insert are fairly straightforward translations.
* Go over the functions in code_review.ml for clarification.

### Timing

The most intersting design decision that students had to make in this pset was how to do the automated timing of crawling and queries.  Some solutions that students came up with are highlighted below.

Note that there were many different ways to do this part of the pset:

* Just toggle the different implementations for sets, and manually run the `moogle` command given in the spec.  Copy the timing information automatically outputted, and analyze it by hand.

Another possible solution:
Things to point out, firstly that this is a very good benchmarking, and was able to produce results like this:

![results](output.png)

```
#!/bin/bash
for i in `seq 1 10`;
do
  make clean crawl crawlperf > /dev/null 2>&1 && ./crawlperf.byte 8080 1000 wiki/Teenage_Mutant_Ninja_Turtles
done
```

``` ocaml
(**********************************************************************
 * searchperf.ml: a performance test of search functionality that 
 *                outputs comma-separated values that report results.
 **********************************************************************)

open Crawl;;
open Crawler_services;;
module CS = Crawler_services;;

let read_file (fname : string) : string list =
  let chan = open_in fname in
  let rec aux (acc : string list) : string list =
    try
      aux ((input_line chan) :: acc)
    with
    | End_of_file -> close_in chan; List.rev acc in
  aux []
;;

(* run : execute a given number of search runs with the test queries *)
let run (n : int) (ndx : WordDict.dict) (words : string list) : unit =
  let len = List.length words in
  let gen_query (_ : unit) : string =
    "?q=" ^ 
      (let w = List.nth words (Random.int len) in
        (* 20% of the time, create a compound \query [AND or OR, even odds]*)
        if (Random.int 10) > 1 then
          let joiner = (if (Random.int 2) = 0 then " AND " else " OR ") in
          w ^ joiner ^ (List.nth words (Random.int len))
        else w) in
  let rec run' (c : int) : unit =
    if (c <= 0) then ()
    else 
      let _ = Q.eval_query ndx (Q.parse_query (gen_query ())) in
      run' (c - 1) in
  run' n
;;

let _ = Random.init 123456 in (* Always use the same seed *)
let index = (crawler ()) in
let words = (read_file "words.txt") in 
let _ =
  for _ = 1 to 1000 do
    let runstart = Unix.gettimeofday () in
    let _ = run 1000000 index words in
    let runend = Unix.gettimeofday () in
    let _ = (print_string ((string_of_float (runend -. runstart)))) in
    let _ = print_endline "" in
    let _ = flush_all () in ()
  done in 
()
;;

```



## Problem Set 6 Talking Points
Students will need to understand infinite data structures like streams and the way that refs differ from functional programming.  

**Update this section more as problem set 6 is developed.**


