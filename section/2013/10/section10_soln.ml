(* CS51 Section 10.  
 * Spring 2011 *)

(* This is a set of exercises meant to resemble the sorts of things
   we'll ask on final.  

  These exercises cover the following topics, among others: currying, partial application, higher order functions, variable scope in let, in let rec, recursion, refs, closures *)

(*============= Part 1 ===========================================*)

(* For each of the following Ocaml expressions, explain what value we
get when we evaluate the expression, or why the expression makes no
sense.  Hint: first type-check, then evaluate.*)

(* Soln: just run these *)

let rec f = fun x -> if x < 0 then -x else f (12 - x)
in f 15

let rec rex = 
  fun x -> 
    if x <= 0 then 0 else
      if x = 1 then 1 else
        let hex = rex (x-2) in
        let rex = rex (x-1) in
          hex + rex in
  List.map rex [0; 1; 2; 3; 4; 5];;

let f = fun x -> x * x in f 4

let f = fun x -> x+1 in let g = 41 in f g

let f = fun x -> fun y -> x + y + 2 in f

let x = fun f x -> x f in x 41 (fun f -> f+1)

let f x y = x * y in 
let a = [3; 5; 2; 7] in 
let b = List.map f a in
  List.map (fun g -> g 6) b

let f g h = fun x -> h (g x) in
let a = f (fun x -> 2*x + 3) in
let q = a (fun x -> (x, x+1)) in
  q 3

let c f g x = f g x in c ((+) 1) (( * ) 2) 20

let f = fun x f -> f x in
  f "4" (fun x -> "2" ^ x)   (* Reminder: ^ is string concat *)

List.filter ((=) 3) [1;2;3;4;5]

let lion = (5,6) in
let tiger = (1,2) in
let bear = (3,4) in
  List.map (fun zebra cow -> cow + 1) [lion; tiger; bear]

let f x = 
  match x with
  | None -> None
  | Some x -> if (x mod 2 == 0) then (x+1) else Some (x-1)
in
f (Some 41) 

reduce (+) 42

(* Time to invade England? *)
let r = ref 65 in
let f = fun a -> r := !r + a; string_of_int a in
let s = (f 1) ^ (f 0) in
  s ^ (string_of_int !r) 

let poof = ref ((+) 2) in
  List.map !poof [1;2;3]

let poof = ref (fun x y -> x y) in
  List.map !poof (List.map !poof [(+) 1; (+) 2; (+) 3])

let x = ref 42 in
let y = !x in
let _ = x := 51 in
  (y, !x)


 let x = ref 42 in 
 let f() = !x in
 x := 51 ; f()  


(*============ Part 2 ==========================================*)
(* For each of the following functions, state whether it could be used
   as an argument to reduce, map, filter or none of these. *)

fun (n: int) -> 2 * n
(* map *)

fun (n: int) -> (2 * n = 4)
(* map, filter *)

fun (x: string) (y: string) -> (String.length x) + (String.length y)
(* map----don't forget about currying *)

( * )
(* map----don't forget about currying, reduce *)

( * ) 4
(* map *)


(*============ Part 3  =========================================*)

(* Recall the definition of reduce as defined in class (similar to Ocaml’s List.fold_right): *)

let rec reduce f u xs = 
  match xs with 
  | [] -> u
  | h::t -> f h (reduce f u t)

(* For each of the following, rewrite the function using reduce.  You should not need to define or use auxiliary functions (i.e., you should be able to do this with just anonymous functions.) *)

let rec g xs = 
     match xs with 
     | [] -> ""
     | (n,s)::t -> n ^ ": " ^ string_of_int s ^ "\n" ^ (g t)

let g = reduce (fun (n,s) r -> n ^ ": " ^ string_of_int s ^ "\n" ^ r) "";;


let rec g xs = 
    match xs with 
    | [] -> None
    | h::t -> (match g t with 
               | None -> Some h
               | Some x -> if h > x then Some (h - 1) else Some x)

let g = reduce (fun x r -> match r with
		  | None -> Some x
		  | Some y -> if x > y then Some (x-1) else Some y) None;;


let rec g xs = 
    match xs with
    | [] -> (fun yarr -> yarr)
    | h::t -> fun x -> (g t) (h x)

let g = reduce (fun f r -> fun x -> r (f x)) (fun x -> x);;


(*============= Part 4 ===========================================*)

(* Define trinary trees as follows:

   An  trinary tree is either empty or a node.  If it’s a node, it
   can either be 

   (1) a binary node with one ‘a value x and two sub-trees left and
   right such that all of the ‘a values in left are less than x, and
   all of the ‘a values in right are greater than x, or else

   (2) a trinary node with two values x and y and three sub-trees
   left, middle, and right with the properties that x < y, all of the
   ‘a values in left are less than x, all of the ‘a values in right
   are greater than y, and all of the ‘a values in middle are between
   x and y.

   Here is an ML type definition for trinary trees:
*)

type 'a tree = Empty | BNode of ('a tree * 'a * 'a tree) 
	       | TNode of ('a tree * 'a * 'a tree * 'a * 'a tree);;


(* Write a fold for trinary trees. *)


(* you want your fold on a 'a tree to have type:
('b -> 'a -> 'b -> 'b) -> ('b -> 'a -> 'b -> 'a -> 'b -> 'b) -> 'b -> 'a tree -> 'b 

So, it takes 2 functions: a combinator for BNode, and a combinator for TNodes,
as well as the trivial combinator (base case) for Empty trees.

Then fold is:  *)

let rec foldt f g u t = 
  (* Define a helper to save some typing *)
  let loop = foldt f g u in
    match t with 
      | Empty -> u
      | BNode (l, v, r) -> f (loop l) v (loop r)
      | TNode (l, v1, c, v2, r) -> 
          g (loop l) v1 (loop c) v2 (loop r)



(* Use your fold to write a function that would add
   all the values in a trinary tree of ints.  *)

let add_all (t: int tree) : int =
  let add3 = fun x y z -> x + y + z in
  let add5 = fun x y z w q -> x + y + z + w + q in
  foldt add3 add5 0 t

;;

(* Use your fold to write map for trinary trees *)
let mapt f t =
  let map_bnode = fun l v r -> BNode(l, (f v), r) in
  let map_tnode = fun l v1 c v2 r -> TNode(l, (f v1), c, (f v2), r) in
    foldt map_bnode map_tnode Empty t



(*============== Part 5 =======================================*)

(* Here is the definition of streams from lecture, along with some
   useful functions: *)

type 'a str = Cons of 'a * 'a stream
and 'a stream = unit -> ('a str);;

let rec ones : int stream = fun () -> (Cons (1,ones));;

let head (s:'a stream) : 'a = 
  match s() with 
    | Cons (h,_) -> h
;;

let tail (s:'a stream) : 'a stream = 
  match s() with 
    | Cons (_,t) -> t
;;

let rec take(n:int) (s:'a stream) : 'a = 
  if n <= 0 then head s else take (n-1) (tail s)
;;

let rec first(n:int) (s:'a stream) : 'a list = 
  if n <= 0 then [] else (head s)::(first (n-1) (tail s))
;;

let rec map(f:'a -> 'b) (s:'a stream) : 'b stream = 
  fun () -> (Cons (f (head s), map f (tail s)))
;;

let inc x = 1 + x ;;

let twos = map inc ones ;;

let rec nats = fun () -> (Cons (0, map inc nats)) ;;

let rec zip (f:'a -> 'b -> 'c)  
    (s1:'a stream) (s2:'b stream) : 'c stream = 
  fun () -> (Cons (f (head s1) (head s2), 
                   zip f (tail s1) (tail s2))) ;;

let threes = zip (+) ones twos ;;

(* Write a function to find the maximum of a float stream, assuming
   that the stream will have a single maximum--the values will go up and
   then start going down. *)

let maximum (s: float stream) : float = 
  let rec loop (str: float stream) (prev: float) : float = 
    let curr = head str in
      if curr < prev then prev else loop (tail str) curr
  in
    loop (tail s) (head s)



;;

(* Write a function to compute successive approximation of e^x using the
formula:

  e^x = sum x^i / (i!)
*)

let exp_stream (x: float) : (float stream) = 
  let e_seq = 
    let rec fact n = 
      if n <= 0 then 1 else n * (fact (n-1)) in
    let numer = map (fun i -> x ** (float i)) nats in
    let denom = map (fun i -> float (fact i)) nats in
      zip (/.) numer denom in
  let rec sum_stream_to_n str n =
    if n = 0 then 0. else (head str) +. (sum_stream_to_n (tail str) (n-1)) in
    map (sum_stream_to_n e_seq) nats
;;
(* this is extremely inefficient; we could avoid recomputing values using  
 * memoization *)

(* What does this evaluate to? *)
let f = fun x y -> (x,y) in
  map (fun p -> (fst p) + (snd p)) (zip f nats nats)



(*********************************************************************)


(* Challenge: write reverse using just reduce and lambdas *)

let reverse (xs: 'a list) : 'a list = 
  let id = fun x -> x in     (* you'll find this useful *) 
  let f = 

(* Leaving this as a challenge.  Ask Greg or the staff list for hints. *)

  in
    (* Use a single call to reduce here *)
