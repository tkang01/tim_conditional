
public interface FoldStrategy<E, O> {
    public O getBaseCase();
    public O apply(E element, O accumulator);
}
