import java.util.ArrayList;
import java.util.Random;

public class DoublyLinkedList<T extends Comparable<T>> implements
        SortableList<T> {
    // Why might we want to hide the definition of Node?
    private static class Node<T> {
        T value;
        Node<T> prev;
        Node<T> next;

        Node(T value, Node<T> prev, Node<T> next) {
            this.value = value;
            this.prev = prev;
            this.next = next;
        }
    }

    private Node<T> first;
    private Node<T> last;

    DoublyLinkedList() {
        // You should be able to get an empty list by default.
        // We can overload constructors- what other types of constructors might
        // we want?
    }

    @Override
    public void cons(T element) {
        // Cons-ing an element should put it at the front of the list
    }

    @Override
    public T getNth(int index) throws SortableList.IllegalIndex {
        // Fill in implementation to get the nth element. Indices start at 0.
        throw new SortableList.IllegalIndex();
    }

    @Override
    public void deleteNth(int index) throws SortableList.IllegalIndex {
        // Be careful not to get into infinite loops and to update first and
        // last properly.
        throw new SortableList.IllegalIndex();
    }

    @Override
    public void bubbleSort() {
        // Who remembers bubble sort? What's the best case run time?
        // swap might be helpful (see below)
    }

    private void swap(Node<T> n1, Node<T> n2) {
        // Swap 2 nodes in place. Be sure all your pointers get updated!
    }

    @Override
    public <M extends Comparable<M>> SortableList<M> map(MapStrategy<T, M> m) {
        // Replace this with an implementation of map. Be careful about order.
        return null;
    }

    @Override
    public <N> N fold(FoldStrategy<T, N> f) {
        // Replace this with an implementation of fold.
        return null
    }

    @Override
    public void reverse() {
        // reverse the list
    }

    @Override
    public String toString() {
        String output = "[ ";
        for (Node<T> n = first; n != null; n = n.next) {
            output += n.value.toString() + " ";
        }
        return output + "]";
    }

    public ArrayList<T> toArrayList() {
        ArrayList<T> output = new ArrayList<T>();
        for (Node<T> n = first; n != null; n = n.next) {
            output.add(n.value);
        }
        return output;
    }

    public static void main(String[] args) {
     // some setup code to initialize 15 random integers
        Random r = new Random();
        DoublyLinkedList<Integer> randomInts = new DoublyLinkedList<Integer>();
        for (int i = 0; i < 15; i++) {
            randomInts.cons(r.nextInt(5));
        }

        // ##### Problem 1a, convert a DoublyLinkedList<Integer> to a
        // SortableList<String>
        SortableList<String> randomIntStrings = randomInts.map(
                new MapStrategy<Integer, String>() {
                    // This is the body of an anonymous class that implements
                    // MapStrategy
                    @Override
                    public String apply(Integer i) {
                        return Integer.toString(i);
                    }

                });
        System.out.println(randomInts);
        System.out.println(randomIntStrings);

        // ##### Problem 1b, some thoughts on types
        // Could we have asked for a DoublyLinkedList<String> out of the call to
        // map?
        // Let's say Animal is a supertype of Cow. Is DoublyLinkedList<Cow> a
        // subtype of DoublyLinkedList<Animal>?

        // ##### Problem 2, count the number of times a given String appears in
        // a DoublyLinkedList<String> using fold
        int nThrees = randomIntStrings.fold(new FoldStrategy<String, Integer>() {
            // some methods need to go here...
        });
        System.out.println("# 3s in " + randomIntStrings.toString() + ":" + nThrees);
    }
}