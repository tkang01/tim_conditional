import java.util.ArrayList;
import java.util.Random;

public class DoublyLinkedList<T extends Comparable<T>> implements
        SortableList<T> {
    private static class Node<T> {
        T value;
        Node<T> prev;
        Node<T> next;

        Node(T value, Node<T> prev, Node<T> next) {
            this.value = value;
            this.prev = prev;
            this.next = next;
        }
    }

    private Node<T> first;
    private Node<T> last;

    DoublyLinkedList() {
        first = null;
        last = null;
    }

    @Override
    public void cons(T element) {
        if (first == null) {
            first = new Node<T>(element, null, null);
            last = first;
        } else {
            first.prev = new Node<T>(element, null, first);
            first = first.prev;
        }
    }

    @Override
    public T getNth(int index) throws SortableList.IllegalIndex {
        int position = 0;
        for (Node<T> n = first; n != null; n = n.next) {
            if (position == index) {
                return n.value;
            }
            position++;
        }
        throw new SortableList.IllegalIndex();
    }

    @Override
    public void deleteNth(int index) throws SortableList.IllegalIndex {
        int position = 0;
        for (Node<T> n = first; n != null; n = n.next) {
            if (position == index) {
                if (n == first) {
                    first = n.next;
                    if (first != null) {
                        first.prev = null;
                    } else {
                        // this happens if we just removed the last element
                        last = null;
                    }
                } else if (n == last) {
                    last = n.prev;
                    last.next = null;
                } else {
                    n.prev.next = n.next;
                    n.next.prev = n.prev;
                }
                return;
            }
            position++;
        }
        throw new SortableList.IllegalIndex();
    }

    @Override
    public void bubbleSort() {
        if (first == null || first.next == null) {
            return;
        }
        boolean sorted = false;
        while (!sorted) {
            sorted = true;
            Node<T> n1 = first;
            Node<T> n2 = first.next;
            boolean checkingFirst = true;
            while (n2 != null) {
                int comparison = n1.value.compareTo(n2.value);
                if (comparison <= 0) {
                    n1 = n2;
                    n2 = n2.next;
                } else {
                    sorted = false;
                    swap(n1, n2);
                    if (checkingFirst) {
                        first = n2;
                    }
                    n2 = n1.next;
                }
                checkingFirst = false;
            }
            last = n1;
        }
    }

    private void swap(Node<T> n1, Node<T> n2) {
        n2.prev = n1.prev;
        n1.next = n2.next;
        n1.prev = n2;
        n2.next = n1;
        if (n2.prev != null) {
            n2.prev.next = n2;
        }
        if (n1.next != null) {
            n1.next.prev = n1;
        }
    }

    @Override
    public <M extends Comparable<M>> SortableList<M> map(MapStrategy<T, M> m) {
        SortableList<M> output = new DoublyLinkedList<M>();
        for (Node<T> n = last; n != null; n = n.prev) {
            output.cons(m.apply(n.value));
        }
        return output;
    }

    @Override
    public <N> N fold(FoldStrategy<T, N> f) {
        N accumulator = f.getBaseCase();
        for (Node<T> n = first; n != null; n = n.next) {
            accumulator = f.apply(n.value, accumulator);
        }
        return accumulator;
    }

    @Override
    public void reverse() {
        Node<T> n = first;
        first = last;
        last = n;
        while (n != null) {
            Node<T> visitNext = n.next;
            n.next = n.prev;
            n.prev = visitNext;
            n = visitNext;
        }
    }

    @Override
    public String toString() {
        String output = "[ ";
        for (Node<T> n = first; n != null; n = n.next) {
            output += n.value.toString() + " ";
        }
        return output + "]";
    }

    public ArrayList<T> toArrayList() {
        ArrayList<T> output = new ArrayList<T>();
        for (Node<T> n = first; n != null; n = n.next) {
            output.add(n.value);
        }
        return output;
    }

    public static void main(String[] args) {
        // some setup code to initialize 15 random integers
        Random r = new Random();
        DoublyLinkedList<Integer> randomInts = new DoublyLinkedList<Integer>();
        for (int i = 0; i < 15; i++) {
            randomInts.cons(r.nextInt(5));
        }

        // ##### Problem 1a, convert a DoublyLinkedList<Integer> to a
        // SortableList<String>
        SortableList<String> randomIntStrings = randomInts.map(
                new MapStrategy<Integer, String>() {
                    // This is the body of an anonymous class that implements
                    // MapStrategy
                    @Override
                    public String apply(Integer i) {
                        return Integer.toString(i);
                    }

                });
        System.out.println(randomInts);
        System.out.println(randomIntStrings);

        // ##### Problem 1b, some thoughts on types
        // Could we have asked for a DoublyLinkedList<String> out of the call to
        // map?
        // Let's say Animal is a supertype of Cow. Is DoublyLinkedList<Cow> a
        // subtype of DoublyLinkedList<Animal>?

        // ##### Problem 2, count the number of times a given String appears in
        // a DoublyLinkedList<String> using fold
        int nThrees = randomIntStrings.fold(new FoldStrategy<String, Integer>() {

            @Override
            public Integer getBaseCase() {
                return 0;
            }

            @Override
            public Integer apply(String element, Integer accumulator) {
                if (element.equals(Integer.toString(3))) {
                    return accumulator + 1;
                } else {
                    return accumulator;
                }
            }

        });
        System.out.println("# 3s in " + randomIntStrings.toString() + ":" + nThrees);
    }
}