public interface SortableList<T extends Comparable<T>> {
    public static class IllegalIndex extends Exception {};

    public void cons(T element);
    public T getNth(int index) throws IllegalIndex;
    public void deleteNth(int index) throws IllegalIndex;
    public void bubbleSort();
    public void reverse();

    public <M extends Comparable<M>> SortableList<M> map(MapStrategy<T, M> m);
    public <N> N fold(FoldStrategy<T, N> f);
}