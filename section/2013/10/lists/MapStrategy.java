
public interface MapStrategy<I, O> {
    public O apply(I input);
}
