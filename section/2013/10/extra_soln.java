/*
 * T(n) = (n/3) * T(n/4) + c_1 n + c_2
 * Just assume T(0) = 1 */


/* 
 * To quote the email thread:
 * For Part 3, ack.  Gideon put this into the final exam review last year and no
 * one knew what to do with it because it's not really something that's
 * naturally described in terms of recurrence relations. You also sort of need
 * to understand what the function is doing to be able to make sense of what's
 * going on.
 *
 * I committed a huge block of assumptions, intended to trivialize it down to
 * T(n) = (n/3) T(n/4) + c . There's no base case, really, but you can just say
 * that T(1) = 0.  Since we have a constant term, imagining infinite recursion
 * won't go very well.
 */
