/* This is a set of exercises meant to resemble the sorts of things
   we'll ask on final.  */

/* ============= PART 1 ===================================

  Define the following terms:
  class
  object
  overriding
  inheritance
  subtyping
  interfaces
  abstract base class
  pattern
  generics/polymorphism
  garbage collection

  algebraic data type
  functor
  signature
  cow
  fun
  Jim Danz
  
  - What's the difference between inheritance and subtyping?
 




   
  - What is the difference between an interface and an abstract base
  class?  Why might you want to use one or the other?  Does it make
  sense to use both at the same time?
     




  *** These next questions could be (and have been) the subject of books,
  so we wouldn't ask them quite like this on a real final, but we do
  want you to understand some of the pros and cons
  ****
  - Why is object oriented programming a great idea?

   
     

 
  - Why is object oriented programming a terrible idea?  
     




  - Why is functional programming a great idea?

   
     

 
  - Why is functional programming a terrible idea?  



  
 
*/

/* ================ PART 2 ================================ */

/* */

interface Animal {
    abstract String getSpecies();
    abstract int getAge();
    abstract int getCoolnessLevel();
}

class Dragon implements Animal {
    private int age;
    private int numPeopleEaten;
    public Dragon(int age) {
        this.age = age;
        numPeopleEaten = 0;
    }
    public String getSpecies() { return "DRAGON"; }
    public int getAge() { return this.age; }
    public int getCoolnessLevel() { return 800 + (100 * numPeopleEaten); }
    public void eatPerson(Person p) { numPeopleEaten++; }
}

class Puppy implements Animal {
    private int age;
    private float numPeopleEaten;
    public Puppy(int age) {
        this.age = age;
        numPeopleEaten = 0;
    }
    public String getSpecies() { return "Puppy"; }
    public int getAge() { return this.age; }
    public int getCoolnessLevel() { return 5000 - (100 * age); }

    /* Puppies only take little nibbles out of people */
    public void eatPerson(Person p) { this.numPeopleEaten += 0.2;  }
}


abstract class Person implements Animal {
    protected String name;
    protected int age;
    protected int baseCoolness;

    Person(String name, int age) {
        this.name = name;
        this.age = age;
        baseCoolness = 30;
    }
    public String getName() { return name; }
    public String getSpecies() { return "Homo sapiens"; }
    public int getAge() { return age; }
    abstract public int getCoolnessLevel();
}

/* You want to implement Hippogriff.  Declare the interface(s) you want to use.  Make
 * sure to name/document your methods so that it is obvious how it
 * will actually work.  Hippogriffs can fly very fast, so you will want to
 * calculate their coolness based on how fast they can fly.
 */

class Hippogriff implements Animal {
	
	
	
    public Hippogriff(int age, int speed) {
        
    }
	
    public String getSpecies(){

    }

    public int getAge() {


    }

    public int getSpeed() {


    }

    public int getCoolnessLevel() {


    }
}

/* Notice that Dragon and Hippogriff both keep track of age.  Factor out
 * their common code using an abstract class, MythicalCreature.  What
 * methods does MythicalCreature need to implement?
 */

abstract class MythicalCreature implements Animal {
	
	
	
	
	
	
	
}

/* What changes would you make to Dragon and Hippogriff to 
 * have them extend MythicalCreature?
 */



/* Now that you've written MythicalCreature, it shouldn't be too difficult to
 * write another! 
 */

class Toruk extends MythicalCreature {
	



    /* What methods need to be implemented here? */







    /* The functionality below is unique to a Toruk. */
	
    public void linkWith(Person p) {


    }

    public Person getRider(){


    }
}

/* What improvements could you make?  There is still some redundancy--see if
 * you can spot it.
 */
