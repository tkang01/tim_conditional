(*
 * CS 51 Section 9
 * Intro to Concurrency
 *)


(* Define the following terms:

    - Concurrency

    See parallelism


    - Parallelism

    Both parallelism and concurrency involve doing multiple things at the 
    same time. For the purposes of this class, with parallelism, we do 
    multiple things for the purposes of maximizing *throughput*, while for 
    concurrency we are trying to minimize *latency*

   
    - Thread

    Anytime you run a program, you are running a whole *process*. But a single
    process may have multiple threads; for example, in Microsoft Word, one
    thread may be constantly spell-checking and grammar-checking, while another
    thread is always waiting for further user input. A thread is the smallest
    context of execution that an operating system handles; multiple threads may
    be scheduled on entirely different processers. A process that does not fork
    any new threads is implicitly single-threaded.

   
    - Critical section

    A critical section is the actual code where a shared resource, such as a
    global variable (or a ref in OCaml), is being accessed and should not be
    accessed by any other thread until the critical section is done executing. 


    - Lock / Mutex

    A mechanism for coordinating threads such that no two threads are in a 
    critical section for a shared resource at the same time. Only one thread
    can hold a mutex at any given time, and so any other thread wanting the 
    mutex must wait for the first thread to release it for them to be able 
    to acquire it.


    - Deadlock

    The situation where two threads hold separate locks, and then thread 1 wants the lock thread 2 holds, and vice-versa, and so neither of them will be able to make any progress.
    
    - Atomic

    A set of instructions is atomic if, to the rest of the system, those 
    instructions happen instantaneously. A simple increment is importantly 
    *not* atomic. Mutexes are generally used in order to make critical 
    sections atomic.


*)





(* Exercise 0.1
 *
 * Threads make life complicated!  Why do we bother?
 *)

(*
 *  For one, Multi-core is the Future (tm). We want to add more processors and
 * have our code run faster as a result.
 * Also, even if we have only one core, we want preemptive multitasking.
 *)


(* Exercise 0.2
 *
 * What are futures? How do they differ from threads? When might you use futures?
 *)

(*
 * Futures are a design pattern for wrapping up some computation in a function
 * and "sending it off", with the expectation of completing other tasks and 
 * then checking back later to see what the computation returned. Generally, 
 * a thread is forked to complete the future as the current thread continues
 * about its business.
 * You might use futures in many cases where the forked thread doesn't 
 * require any synchronization with the current threads. For example, you may
 * use a future to calculate the length of a list while the current thread
 * continues to process does some other work, only needing the length "in the
 * future" and not right now.
 *)

(* In OCaml, a concurrent program uses threads to interleave execution
 * of different parts of the program. Threads are created using Thread.create
 * of the Thread module. In the programs you've written so far, all your code
 * has run in a main thread that was created by default. If a Thread is
 * created, it begins executing the code in the function you pass to
 * Thread.create.  Calling Thread.join with the thread id that Thread.create
 * returns causes the main thread to wait until the other thread finishes.
 *
 * We may reason about order of execution more easily by defining
 * "happens-before" relationships. Given a Thread t,
 *   - A call to Thread.create happens before any statement executed by 
 *     the thread
 *   - If Thread.join is called on thread t, all statements executed by 
 *     t happen before Thread.join returns
 *     
 *)


(* Exercise 1. *)

(* What's wrong with the following code? *)

(* In the function g, the inc function is not atomic, which means that 
 * the final result may not be the expected 2000000
 *) 


let f () = 
    let x = ref 0 in
    let inc x = x := !x + 1 in

    let rec g repeat () =
        if repeat <= 0 then () else
        let _ = inc x in
        g (repeat-1) () in
    
    let thread = Thread.create (fun _ -> g 1000000 ()) () in 
    let _ = g 1000000 () in 
    Thread.join thread; 
    print_int !x; print_string "\n"

let _ = f ()


(* What's one way of fixing this program? Insert the fix into the program.
 * What does this do to the parallelism of our code? 
 *)

(* One fix is to have a mutex for the variable x. Unfortunately, if we 
 * are going to use a mutex, then we won't be able to do much in parallel
 * at all, since only one thread will be able to increment at a time. 
 * Furthermore, the overhead of the mutex synchronization will only bring
 * down performance.
 *)


(* Try to reason about all of the possible interleavings of the functions 
 * x and y below, and the possible final values of num (assuming that only 
 * valid interleavings can produce final values, which may not be the case,
 * and in fact probably isn't the case on your own computer).
 *)

(*
 * Possible values: 1, 2, 3, 4, 5
 * Some ways to get some of these numbers:
 * 1 - in x, num is 0, and so begin to increment. Switch to y, who finishes,
 * incrementing num to 2. Switch back to x, who finishes its increment from 0
 * to 1, setting num to 1.
 * 2 - in x, num is 0, begin to increment. Switch to y, increment, num
 * is 1, so begin to increment again. x increments num to 1, switch back to y,
 * y increments it to 2 (although it read in 1 at a different time).
 * 3 - x reads in num = 0, switch to y. y completes, incrementing to 2. x 
 * continues and completes, incrementing to 3.
 * 4 - y completes fully, and then x completes fully
 * 5 - y increments once, get to the increment but don't start it. switch to x,
 * who increments 3 times and finishes. Finally, y increments, setting it to 5
 *)

let f () = 
    let num = ref 0 in
    let inc () = num := !num + 1 in

    let x () =
        let _ = 
            if (!num = 0) then inc ()
            else if (!num = 2) then (inc (); inc ())
            else inc(); inc(); inc() in
        () in

    let y () = 
        inc ();
        let _ = if (!num = 1) then inc () in
        () in

    let thread = Thread.create y () in 
    let _ = x () in 
    Thread.join thread;





(* Exercise 2: Linked Lists *)

(* 
 * Remember from lecture that divide and conquer algorithms are not very
 * effective when dealing with lists, since there is no constant-time way
 * of dividing the list into parts. However, we may still want to be able 
 * to access and modify a list safely from multiple threads. Here we'll define
 * such a list data structure.
 *)

class type ['a] linked_list =
object
    (* Adds an element to the front of the list *)
    method push : 'a -> unit

    (* Removes and returns the element from the from of the list *)
    method pop : 'a option

    (* Returns true if the given element is in the list, false otherwise *)
    method search : 'a -> bool

    (* Removes the first occurrence of the given element in the list.
     * Returns the original list if such an element is not found *)
    method remove : 'a -> unit

end

(* If we wanted, we could keep a lock on the entire list, in which case all 
 * list operations would readily be atomic. Why might we not want to do that? 
 *)

(*
 * We used new_id in order to establish an absolute ordering on the locks. Here
 * we can just use the order of the elements in the list as that ordering.
 *
 *)
    
(* Instead, associate a mutex with each element in the list *)
type 'a node = Nil | Cons of 'a * 'a list
and 'a list = {lock : Mutex.t; mutable node : 'a node}

(* The following functions from lecture might be useful. *)
let with_lock (l:Mutex.t) (f:unit -> 'a) : 'a =
    let _ = Mutex.lock l in
    let res = try f () with exn -> (Mutex.unlock l ; raise exn) in
    let _ = Mutex.unlock l in
    res

let new_id : unit -> int = 
    let c = ref 0 in 
    let l = Mutex.create() in
    (fun _ -> with_lock l (fun () -> (c := (!c) + 1; !c)))



(* 
 * Do we actually need new_id? What did we originally use it for? Why might 
 * that not matter in this case?
 *)

class ['a] llist : ['a] linked_list =
object (this)

    
    (* Why do we still need a super lock? *)
    val super_lock : Mutex.t = Mutex.create()
    val mutable contents : 'a list = {lock = Mutex.create(); node =  Nil}
    
    method push x = 
        let new_lock = Mutex.create() in
        with_lock super_lock (fun () ->
                contents <- {lock = new_lock; node = Cons(x, contents)})
    
    (* Why didn't we have to lock new_lock? Because no one will be 
     * able to access contents until the super_lock is released anyway *)


    method pop = 
        with_lock super_lock (fun () ->
            match contents.node with
            | Cons(x, contents') -> contents <- contents'; Some x
            | Nil -> None)

    (* Why can't we use "with_lock" here? 
       Answer: it assumes we want to acquiring and releasing locks 
               nests perfectly, which isn't the case here *)
    method search x = 

        (* Atomically grab the lock at the head of contents *)
        let _ = Mutex.lock super_lock in
        let contents' = contents in
        let lock = contents.lock in
        let _ = Mutex.lock lock in
        let _ = Mutex.unlock super_lock in

        (* Assumes the list is already locked! *)
        let rec search_helper list = 
            match list.node with
            | Nil -> (Mutex.unlock list.lock; false)
            | Cons(x', contents') ->
                if x = x' then 
                   (Mutex.unlock list.lock; true)

                (* We must lock the next element in the list before 
                 * we release the lock on this element, or it could be 
                 * removed from underneath us *)
                else
                    (Mutex.lock contents'.lock;
                    Mutex.unlock list.lock;
                    search_helper contents') in
        (* We cannot just call search_helper on contents, because the list
         * might change between the time we release the super_lock and the
         * time we read contents here. *)
        search_helper contents'
    
    (* For remove, we must look ahead one node to see if that's the node
     * we want to remove, since we want to update the "link" to the 
     * removed node to the subsequent node *)
    method remove x = 
        let _ = Mutex.lock super_lock in

        (* Special case removal of the first node *)
        match contents.node with 
        (* We need to still hold on to the super lock, since we might update
         * the head of contents *)
        | Nil -> Mutex.unlock super_lock
        | Cons(x', contents') ->
            if x' = x then 
                (contents <- contents'; Mutex.unlock super_lock)
            else
                (* Have to lock the head before unlocking super! *)
                let _ = Mutex.lock contents.lock in
                let _ = Mutex.unlock super_lock in

                (* Assumes the list is already locked! *)
                let rec remove_helper node = 
                    match node.node with
                    | Cons(v, n) ->
                        (match n.node with 
                        (* If next node is Nil, then the element wasn't in the list *)
                        | Nil ->  Mutex.unlock node.lock
                        | Cons(x', next) ->
                            let _ = Mutex.lock n.lock in
                            (if x = x' then
                                (Mutex.lock next.lock;
                                node.node <- next.node; 
                                Mutex.unlock node.lock;
                                Mutex.unlock n.lock)
                            else
                                (Mutex.unlock node.lock;
                                remove_helper n)))
                    | _ -> raise (Failure "Current node can't be Nil") in
                remove_helper contents
   
end

(* Summary:
 *
 * Reasoning about concurrent programming is hard.  You should now
 * have a sense of why, and should be able to see concurrency problems
 * in toy examples.  If you want to learn more, take CS61.  If you
 * want to really understand concurrency, take CS161.
 *)
