(* CS51 Section 2 Solutions *)

(* ********************** Part 0 - A Beautiful Times *************** *)

(* Returns a bignum representing n1*n2 *)
let times (n1: bignum) (n2: bignum) : bignum =
  (* group pairs numbers on the bottom_row with the entire top row *)
  let rec group (top_row:int list) (bottom_row:int list) : (int * int list) list =
    match bottom_row with
    | [] -> []
    | hd :: tl -> (hd, top_row) :: group top_row tl
  in
  (* multiply_row takes a number and multiplies it to each element in a row *)
  let rec multiply_row (number : int) (row : int list) : int list =
    match row with
    | [] -> []
    | hd :: tl -> hd * number :: multiply_row number tl
  in
  (* multiply_rows performs multiply_row on a list of groupings *)
  let rec multiply_rows (groupings : (int * int list) list) : int list list =
    match groupings with
    | [] -> []
    | (n, row) :: tl -> multiply_row n row :: multiply_rows tl
  in
  (* stagger takes a list of rows and returns a list of staggered columns. *)
  let rec stagger (rows : int list list) : int list list =
    match rows with
    | [] -> []
    | row_hd :: row_tl -> splice row_hd ([] :: stagger row_tl)
  (* splice prepends a list element-wise onto a list of lists *)
  and splice (to_splice : int list) (target : int list list) : int list list =
    match to_splice, target with
    | [], _ -> target
    | (hd :: tl), [] -> [hd] :: splice tl []
    | (hd :: tl), (t_hd :: t_tl) -> (hd :: t_hd) :: splice tl t_tl
  in
  (* combine_multiplications sums a list of number columns with a carry *)
  let rec combine_multiplications (ms : int list list) (carry : int) : int list =
    match ms with
    | [] -> if carry = 0 then [] else 
        carry mod base :: combine_multiplications [] (carry / base)
    | hd :: tl ->
        let s = sum hd + carry in
        s mod base :: combine_multiplications tl (s / base)
  and sum (ns : int list) : int =
    match ns with
    | [] -> 0
    | hd :: tl -> hd + sum tl
  in 
  let multiplications = 
    multiply_rows (group (List.rev n1.coeffs) (List.rev n2.coeffs)) 
  in
  let new_coeffs = combine_multiplications (stagger multiplications) 0 in
  { coeffs = List.rev new_coeffs; neg = not (n1.neg = n2.neg) }
;;

(* ************************ Part 1 - Style ********************** *)

(*
 * mystery [] -> []
 * mystery []::tl -> mystery tl
 * mystery [x]::tl -> x :: mystery tl
 * mystery (a::b)::tl -> a :: (mystery (b::tl))
 * So flatten :)
 *)

let rec mystery (lists : 'a list list) =
  if List.length lists = 0 then []
  else if List.length (List.hd lists) = 0 
  then mystery (List.tl lists)
  else if List.length (List.hd lists) = 1
  then let hd = List.hd lists in 
	((List.hd) hd) :: mystery (List.tl lists)
  else let hd = List.hd lists in
	(List.hd) hd :: (mystery ((List.tl hd)::(List.tl lists)))
;;
 
let rec mystery' lists = 
  match lists with
    | [] -> []
    | [] :: tl -> mystery' tl
    | [x] :: tl -> x :: mystery' tl
    | (a::b) :: tl -> a :: (mystery' (b::tl))
  ;;

(* Make sure this behaves the same *)
assert (let x = [[];[]] in mystery x = mystery' x);;
assert (let x = [[1];[1;3;2]] in mystery x = mystery' x);;
assert (let x = [[1;2;4];[1;3;2];[7;8;9]] in mystery x = mystery' x);;

(* Now change to a better name, and make even shorter *)
let flatten lists = reduce List.append [] lists;;

(* Make sure this behaves the same too *)
assert (let x = [[];[]] in mystery x = flatten x);;
assert (let x = [[1];[1;3;2]] in mystery x = flatten x);;
assert (let x = [[1;2;4];[1;3;2];[7;8;9]] in mystery x = flatten x);;

(* ***************** Part 2 - Map and reduce ************************ *)

(* Map and reduce *)
(* Exercise 1 *)
let rec reduce f u xs =
  match xs with
    | [] -> u
    | hd::tl -> f hd (reduce f u tl);;

let rec map f xs =
  match xs with
    | [] -> []
    | hd::tl -> (f hd) :: (map f tl);;
	
(* let map = List.map;; *)

(* 1a. Implement length in terms of reduce. 
 * length lst returns the length of lst. length [] = 0. *)
let length (lst: int list) : int =
  reduce (fun x r -> r + 1) 0 lst
;;

(* 1b. Write a function that takes an int list and multiplies every int by 3.
 * Use map. *)
let times_3 (lst: int list): int list =
  map (fun a -> a * 3) lst
;;

(* 1c. Write a function that takes an int and an int list and multiplies every
 * entry in the list by the int. Use map. *)
let times_x (x : int) (lst: int list): int list =
  map (fun a -> a * x) lst
;;

(* 1d. Rewrite times_3 in terms of times_x.
 * This should take very little code. *)
let times_3_shorter = times_x 3 ;;

(* 1e. Write a function that takes an int list and generates a "multiplication
 * table", a list of int lists showing the product of any two entries in the
 * list. *)
let mult_table (lst: int list) : int list list =
  map (fun a -> times_x a lst) lst
;;
	
(* 1f. Write a function that takes a list of boolean values
 * [x1; x2; ... ; xn] and returns x1 AND x2 AND ... AND xn.
 * For simplicity, assume and_list [] is TRUE. Use reduce. *)
let and_list (lst: bool list) : bool =
  reduce (&&) true lst
;;

(* 1g. Do the same as above, with OR.
 * Assume or_list [] is FALSE. *)
let or_list (lst: bool list) : bool =
  reduce (||) false lst
;;
	
(* 1h.	 Write a function that takes a bool list list and returns
 * its value as a boolean expression in conjunctive normal form (CNF).
 * A CNF expression is represented as a series of OR expressions joined
 * together by AND.
 * e.g. (x1 OR x2) AND (x3 OR x4 OR x5) AND (x6).
 * Use map and/or reduce.
 * You may find it helpful to use and_list and or_list. *)
let cnf_list (lst: bool list list) : bool =
  and_list (map or_list lst)
;;
	
(* 1i. Write a function that takes an expr list and returns true if and only if
 * every expr in the list represents a true Boolean expression. *)
let all_true (lst: expr list) : bool =
  reduce (&&) true (map eval lst)
;;

(* You may find these helper functions from section 1 exercise 3 helpful. *)

let calc_option (f: 'a->'a->'a) (x: 'a option) (y: 'a option) : 'a option =
  match (x, y) with
  | (Some x', Some y') -> Some (f x' y')
  | (_, None) -> x
  | (None, _) -> y ;;
 
let min_option x y = calc_option min x y ;;

let max_option x y = calc_option max x y ;;

let and_option x y = calc_option (&&) x y ;;
	
(* 1j. Write and_list to return a bool option,
 * where the empty list yields None. Use reduce. *)
let and_list_smarter (lst: bool list) : bool option =
  reduce (fun x base -> and_option (Some x) base) None lst
;;

let and_list_smartest (lst: bool list) : bool option =
  reduce and_option None (map (fun x -> Some x) lst)
;;

(* 1k. Write max_of_list from section 0:
 * Return the max of a list, or None if the list is empty. *)
let max_of_list (lst:int list) : int option =
  match lst with
    | hd::tl -> Some (reduce max hd tl)
    | [] -> None
;;

(* 1l. Write bounds from section 0:
 * Return the min and max of a list, or None if the list is empty. *)
let bounds (lst:int list) : (int option * int option) =
  reduce (fun x (small, big) -> (min_option x small, max_option x big))
    (None, None)
    (map (fun x -> Some x) lst)
;;

(* ************************ Part 3 ****************************** *)

let group (top_row:int list) (bottom_row:int list) : (int * int list) list =
  List.map (fun x -> (x, top_row)) bottom_row
;;
let multiply_row (number : int) (row : int list) : int list =
  List.map (fun x -> x * number) row (* or times_x from earlier *)
;;
let multiply_rows (groupings : (int * int list) list) : int list list =
  List.map (fun (n, l) -> multiply_row n l) groupings
;;
let stagger (rows : int list list) : int list list =
  reduce (fun r staggered -> splice r ([] :: staggered)) [] rows
;;
let sum (ns : int list) : int =
  reduce (+) 0 ns
;;

(*
 * The function should: filter out items that make pred false, and
 * return the result of applying f on each element of the remaining
 * list.
 *)
let filtermap (pred: 'a -> bool) (f: 'a -> 'b) (lst: 'a list) : 'b list = 
    reduce (fun x r -> if pred x then (f x)::r else r) [] lst;;

(* 2c.  Use filtermap to write the deoptionalize function from PS2. As a reminder:
   deoptionalize [None; Some 2; None; Some 3; Some 4; None] = [2;3;4] *)
let deoptionalize lst = filtermap (fun x -> x != None) 
  (fun o -> match o with 
     | Some x -> x
     | None -> raise (Failure("Impossible"))) 
  lst;;

(* You may have noticed that you needed to raise an exception to make
   deoptionalize work properly with arbitrary option types.  Here is
   an alternative way to define filter_map that avoids that
   problem. Try filling in the code (use reduce here too) *)

let filter_map (f: 'a -> 'b option) (lst: 'a list) : 'b list = 
  reduce (fun x r -> match f x with
            | Some a -> a :: r
            | None -> r) [] lst;;


let deoptionalize' lst = filter_map (fun x -> x) lst;;


(* ************************ Part 4 ****************************** *)

let reduce f u xs = List.fold_right f xs u;;

(* 42.1 *)
let f = (fun (x,y) -> x) in
  f (42, 24);;

(* 42.2 *)
(* A good intermediate step would be: *)
let x = ??? in
let true_dat y = true in
let f = (fun a -> let b = a * 2 in b) in
  reduce (+) 0 (List.filter true_dat [f x; x])
		;;
(* answer: *)
let x = 42/3 in
  let x' = (fun x -> let x = x * 2 in x) in
	  reduce (+) 0 (List.filter (fun x -> x = x) [x' x; x])
		;;


(* 42.3 *)
let f = (fun x y -> x + y) in
  let g = f 21 in
	  g 21
		;;

(* 42.5 *)
let f = (fun (x,y) -> x + y) in
  let g = f (???) in
	  g 21
		;;
(* Impossible -- no way to make the relevant value of g anything but
   an int, so it doesn't typecheck *)

(* 42.6.1 *)
let f = (fun x -> fun x -> 42) in
f f (f f)
;;


(* 42.6.2 *)
(* Trivial if you think correctly about currying, but hopefully
 * is the source of some confusion *)
let f = (fun x y -> fun x -> 42) in
  (f f) f f
;;

(* 42.7 *)
let f = (fun x y -> 2 * y) in
  reduce f 21 [f]
;;

(* 42.x: Bonus *)
let thequestion = 9 in
  6 * thequestion  (* reference: see The Hitchhiker's Guide to the Galaxy, by Douglas Adams *)
	;;



