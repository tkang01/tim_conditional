(* CS51 Section 2 Solutions *)

(* ************************ Part 1 - Clean it up. ********************** *)

(*
 * mystery [] -> []
 * mystery []::tl -> mystery tl
 * mystery [x]::tl -> x :: mystery tl
 * mystery (a::b)::tl -> a :: (mystery (b::tl))
 * So flatten :)
 *)

let rec mystery (lists : 'a list list) =
  if List.length lists = 0 then []
  else if List.length (List.hd lists) = 0 
  then mystery (List.tl lists)
  else if List.length (List.hd lists) = 1
  then let hd = List.hd lists in 
	((List.hd) hd) :: mystery (List.tl lists)
  else let hd = List.hd lists in
	(List.hd) hd :: (mystery ((List.tl hd)::(List.tl lists)))
;;
 
let rec mystery' lists = 
  match lists with
    | [] -> []
    | [] :: tl -> mystery' tl
    | [x] :: tl -> x :: mystery' tl
    | (a::b) :: tl -> a :: (mystery' (b::tl))
  ;;

(* Make sure this behaves the same *)
assert (let x = [[];[]] in mystery x = mystery' x);;
assert (let x = [[1];[1;3;2]] in mystery x = mystery' x);;
assert (let x = [[1;2;4];[1;3;2];[7;8;9]] in mystery x = mystery' x);;

(* Now change to a better name, and make even shorter. *)
let flatten lists = reduce List.append [] lists;;
(* Note: currently, reduce is implemented below, however for our 
 * implementation of flatten to work, it would have had to have been
 * implemented above this function *)

(* With currying? *)
let flatten = reduce List.append [];;

(* Make sure this behaves the same too *)
assert (let x = [[];[]] in mystery x = flatten x);;
assert (let x = [[1];[1;3;2]] in mystery x = flatten x);;
assert (let x = [[1;2;4];[1;3;2];[7;8;9]] in mystery x = flatten x);;

(* ***************** Part 2 - Map and reduce ************************ *)

(* Map and reduce *)
(* Exercise 1 *)
let rec reduce f u xs =
  match xs with
    | [] -> u
    | hd::tl -> f hd (reduce f u tl);;

let rec map f xs =
  match xs with
    | [] -> []
    | hd::tl -> (f hd) :: (map f tl);;
	
(* let map = List.map;; *)

(* 1a. Implement length in terms of reduce. 
 * length lst returns the length of lst. length [] = 0. *)
let length (lst: int list) : int =
  reduce (fun x r -> r + 1) 0 lst
;;

(* 1b. Write a function that takes an int list and multiplies every int by 3.
 * Use map. *)
let times_3 (lst: int list): int list =
  map (fun a -> a * 3) lst
;;

(* 1c. Write a function that takes an int and an int list and multiplies every
 * entry in the list by the int. Use map. *)
let times_x (x : int) (lst: int list): int list =
  map (fun a -> a * x) lst
;;

(* 1d. Rewrite times_3 in terms of times_x.
 * This should take very little code. *)
let times_3_shorter = times_x 3 ;;

(* 1e. Write a function that takes an int list and generates a "multiplication
 * table", a list of int lists showing the product of any two entries in the
 * list. *)
let mult_table (lst: int list) : int list list =
  map (fun a -> times_x a lst) lst
;;

(* 1f. Write a function that takes a list of boolean values
 * [x1; x2; ... ; xn] and returns x1 AND x2 AND ... AND xn.
 * For simplicity, assume and_list [] is TRUE. Use reduce. *)
let and_list (lst: bool list) : bool =
  reduce (&&) true lst
;;

(* 1g. Do the same as above, with OR.
 * Assume or_list [] is FALSE. *)
let or_list (lst: bool list) : bool =
  reduce (||) false lst
;;
	
(* 1h.	 Write a function that takes a bool list list and returns
 * its value as a boolean expression in conjunctive normal form (CNF).
 * A CNF expression is represented as a series of OR expressions joined
 * together by AND.
 * e.g. (x1 OR x2) AND (x3 OR x4 OR x5) AND (x6).
 * Use map and/or reduce.
 * You may find it helpful to use and_list and or_list. *)
let cnf_list (lst: bool list list) : bool =
  and_list (map or_list lst)
;;
	
(* 1i. Write a function that takes an expr list and returns true if and only if
 * every expr in the list represents a true Boolean expression. *)
let all_true (lst: expr list) : bool =
  reduce (&&) true (map eval lst)
;;

(* You may find these helper functions from section 1 exercise 3 helpful. *)

let calc_option (f: 'a->'a->'a) (x: 'a option) (y: 'a option) : 'a option =
  match (x, y) with
  | (Some x', Some y') -> Some (f x' y')
  | (_, None) -> x
  | (None, _) -> y ;;
 
let min_option x y = calc_option min x y ;;

let max_option x y = calc_option max x y ;;

let and_option x y = calc_option (&&) x y ;;
	
(* 1j. Write and_list to return a bool option,
 * where the empty list yields None. Use reduce. *)
let and_list_smarter (lst: bool list) : bool option =
  reduce (fun x base -> and_option (Some x) base) None lst
;;

let and_list_smartest (lst: bool list) : bool option =
  reduce and_option None (map (fun x -> Some x) lst)
;;

(* 1k. Write max_of_list from section 0:
 * Return the max of a list, or None if the list is empty. *)
let max_of_list (lst:int list) : int option =
  match lst with
    | hd::tl -> Some (reduce max hd tl)
    | [] -> None
;;

(* 1l. Write bounds from section 0:
 * Return the min and max of a list, or None if the list is empty. *)
let bounds (lst:int list) : (int option * int option) =
  reduce (fun x (small, big) -> (min_option x small, max_option x big))
    (None, None)
    (map (fun x -> Some x) lst)
;;

(* ************************ Part 3 ****************************** *)

(* 3a. Reduce reduce reduce
 * Implement map, filter, and map2 using reduce. All three of these functions
 * appear in the List module.
 *)
let map f lst = 
  reduce (fun x r -> (f x)::r) [] lst
;;

let filter f lst = 
  reduce (fun x r -> if f x then x::r else r) [] lst
;;

(*
 * It is necessary to define a helper funciton to zip the two lists together.
 * This allows reduce to deal with the given lists at the same time. Note that
 * we also raise an Invalid_argument exception if the lists are not of the same
 * length.
 *)
let map2 f lst1 lst2 = 
  let rec zip (l1: 'a list) (l2: 'b list) : ('a * 'b) list =
    match l1, l2 with
      | [], [] -> []
      | [], _ 
      | _, [] -> raise (Invalid_argument "map2")
      | h1::t1, h2::t2 -> (h1, h2)::(zip t1 t2)
  in
  reduce (fun (x1, x2) r -> (f x1 x2)::r) [] (zip lst1 lst2)
;;

(*
 * The function should filter out items that make pred false, and
 * return the result of applying f on each element of the remaining
 * list.
 *)
let filtermap (pred: 'a -> bool) (f: 'a -> 'b) (lst: 'a list) : 'b list = 
    reduce (fun x r -> if pred x then (f x)::r else r) [] lst
;;

(* 3c.  Use filtermap to write the deoptionalize function from PS2. As a reminder:
   deoptionalize [None; Some 2; None; Some 3; Some 4; None] = [2;3;4] *)
let deoptionalize lst = 
  filtermap (fun x -> x != None) 
    (fun o -> match o with 
      | Some x -> x
      | None -> raise (Failure("Impossible"))) 
    lst
;;

(* You may have noticed that you needed to raise an exception to make
   deoptionalize work properly with arbitrary option types.  Here is
   an alternative way to define filter_map that avoids that
   problem. Try filling in the code (use reduce here too) *)

let filter_map (f: 'a -> 'b option) (lst: 'a list) : 'b list = 
  reduce (fun x r -> match f x with
            | Some a -> a :: r
            | None -> r) [] lst
;;


let deoptionalize' lst = filter_map (fun x -> x) lst;;


(* ************************ Part 4 ****************************** *)

let reduce f u xs = List.fold_right f xs u;;

(* 42.1 *)
let f = (fun (x,y) -> x) in
  f (42, 24)
;;

(* 42.2 *)
(* A good intermediate step would be: *)
let x = ??? in
  let true_dat y = true in
    let f = (fun a -> let b = a * 2 in b) in
      reduce (+) 0 (List.filter true_dat [f x; x])
;;

(* answer: *)
let x = 42/3 in
  let x' = (fun x -> let x = x * 2 in x) in
    reduce (+) 0 (List.filter (fun x -> x = x) [x' x; x])
;;


(* 42.3 *)
let f = (fun x y -> x + y) in
  let g = f 21 in
    g 21
;;

(* 42.4 *)
let f = (fun (x,y) -> x + y) in
  let g = f (???) in
    g 21
;;
(* Impossible -- no way to make the relevant value of g anything but
   an int, so it doesn't typecheck *)

(* 42.5.1 *)
let f = (fun x y -> 42) in
  f f (f f)
;;

(* 42.5.2 *)
(* Trivial if you think correctly about currying, but hopefully
 * is the source of some confusion *)
let f = (fun x y z -> 42) in
  (f f) f f
;;

(* 42.6 *)
let f = (fun x y -> 2 * y) in
  reduce f 21 [f]
;;

(* 42.x: Bonus *)
let thequestion = 9 in
  6 * thequestion  (* reference: see The Hitchhiker's Guide to the Galaxy, by Douglas Adams *)
;;


(* ******************* Part 5 - RSA Encryption ******************* *)
(* RSA Encryption - You've used it with git, you might use it with ssh,
 * you will implement a version of it on ps3, but what is it exactly?
 * 
 * Let's start with a more general question: What is a cryptographic system?
 * A cryptographic system is a system designed to keep certain data/information
 * private by encoding that data/information and only allowing certain
 * individuals/groups to decode that data/information. A simple example
 * seen by many of you in CS51 is Caesar's cipher implemented below over
 * the ASCII character set: 
 *)
let caesar_encrypt (key: int) (ptext: string) : string =
  String.map (fun c -> Char.chr ((Char.code c + key) mod 256)) ptext
;;

let caesar_decrypt (key: int)  (ctext: string) : string =
  caesar_encrypt (256 - (key mod 256)) ctext;;
;;
(* See how much simpler this in OCaml than C! Higher order functions FTW! *)

(* Caesar's cypher requires the same key for both encryption and decryption. Can
 * you see any scenario when this might be a problem? What if we used
 * different keys for encryption and decryption? This is the concept behind 
 * public key encryption, an idea pioneered by Diffee and Hellman in 1976. In
 * public key encryption, the encryption key is made public and the decryption
 * key is kept private. Furthermore, knowing the encryption key cannot help
 * you find the decryption key. Therefore, someone can encode a message with
 * your public encryption key, send it to you, and know that only you can read 
 * it!
 * 
 * Is it possible to implement Caesar's cipher as the algorithm behind
 * a public key encryption system? Why?
 *
 * RSA encryption is one of the most popular types of public key encryption.
 * Aside from being a public key encryption system, however, what is it
 * that makes RSA encryption effective? RSA encryption takes advantage
 * of the fact that there is no known algorithm for factoring large numbers
 * efficiently. How you might ask? Check out the pset! *)

(* Note: on the pset we have provided a lot of boiler plate code to get you
 * started. At first this may be overwhelming, but we didn't name our
 * functions "mystery" for a reason. Our comments might also prove quite
 * useful! *)


