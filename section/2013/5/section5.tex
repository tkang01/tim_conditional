\documentclass[12pt]{article}

\usepackage{fullpage}
\usepackage{amsmath,amsthm}

\theoremstyle{definition}
\newtheorem{exo}{Exercise}

\title{Computer Science 51, Section 5 \\ Tail Calls and PageRank}
\author{}
\date{}

\begin{document}

\maketitle

\section{Tail Calls}

\indent In languages like ML, tail calls allow us to allocate stack space more
efficiently. A function that returns the value of its recursive call is said to be tail recursive.  Essentially, when the ``last" thing a function does is call itself recursively, we say the function is tail recursive. When a function calls itself recursively and then does something to that result other than just returning it, it is not tail recursive.

Normally, space is allocated for every function that is called
and is not deallocated until that function has fully evaluated. These
function calls are very expensive in terms of both time and space. In the
particular case where the function returns another function call, there is
no need to keep the information from original function around; we've already
finished using all of the information we need. Thus, if we plan ahead we can
write recursive functions that use constant space and spend much less time
switching between functions (shuffling frame pointers) on the stack!

Here we see two versions of ``reduce": foldl, which utilizes this tail call
optimization, and foldr, which does not. Which one is our usual reduce?

\begin{verbatim}
let rec foldr f u xs =
  match xs with
  | [] -> u
  | x::rest -> f x (foldr f u rest) ;;
\end{verbatim}

\begin{verbatim}
let rec foldl f u xs =
  match xs with
  | [] -> u
  | x::rest -> foldl f (f x u) rest ;;
\end{verbatim}

\begin{exo} Using the following definitions, what would expressions 1 and 2 evaluate to? \\\\
\tt{let nums = [1;2;3;4] ;;}\\
\tt{let cons hd tl = hd::tl ;;}\\
\tt{let more\_nums = [[5;8;13];[2;3];[];[1;1]] ;;}\\\\

\begin{tabular}{ c | c }
Expression 1 & Expression 2 \\
\hline\\
\tt{foldr (+) 0 nums} & \tt{foldl (+) 0 nums} \\\\\\\\
\tt{foldr ( * ) 1 nums} & \tt{foldl ( * ) 0 nums} \\\\\\\\
\tt{foldr cons [] nums} & \tt{foldl cons [] nums} \\\\\\\\
\hspace{7mm}\tt{foldr (@) [] more\_nums}\hspace{7mm} &
\hspace{7mm}\tt{foldl (@) [] more\_nums}\hspace{7mm} \\\\\\\\
\end{tabular}\\
\end{exo}

\begin{exo}
What property should functions have for the result to be the same for both
foldr and foldl? 
\end{exo}

\section{Analyzing functions}

For the following functions:
\begin{itemize}
\item What is the asymptotic running time?
\item Write the recurrence relation for its running time.
\item Does the function utilize tail-calls? If not, could we easily rewrite it so it does?
\end{itemize}
\begin{enumerate}
\item 
{\tt let split lst = foldl (fun x (a,b) -> (x::b,a)) ([],[]) lst}\vspace{20mm}


\item Remember {\tt merge} from problem set 1? It takes two ordered lists and combines them into one ordered list. Its runtime is $O(n + m)$ where
$n$ and $m$ are the length of the two lists being merged.
\begin{verbatim}
let rec mergesort (lst:int list) =
  match lst with
    | x::y::rest -> let (a,b) = split lst in merge (mergesort a) (mergesort b)
    | _ -> lst
\end{verbatim}
\vspace{20mm}


\item Remember {\tt partition} from problem set 1? It takes one list and splits it into two. The first list contains only elements smaller than some numbmer $x$, and the second list contains only elements greater than or equal to $x$. Its runtime is $O(n)$ in the length of the list.
\begin{verbatim}
let rec quicksort (lst:int list) =
  match lst with
    | [] -> []
    | x::rest -> let (lows,highs) = partition x rest in
	           quicksort lows @ (x :: quicksort highs)
\end{verbatim}
\vspace{20mm}
\end{enumerate}

\section{PageRank}

This section describes the QuantumRanker version of the PageRank algorithm you're implementing in Moogle. The key insight is that the links to a page can indicate the importance of that page. Furthermore, links from important pages are better indicators than links from less important pages. This algorithm captures all of this information.

PageRank can be thought of as a model of user
behavior. We assume there is a ``random surfer"
who is given a web page at random and keeps
clicking on links, never hitting ``back" but eventually
gets bored and starts on another random page.
The probability that the random surfer visits a page
is its PageRank. 

To clarify, each page is associated with the probability that it will be accessed by the random surfer.
This can serve as a measure of the page's relative importance because the random surfer would be more likely to 
stumble upon a page that has lots of links from other important sites.
\\\\
Overview of the algorithm:
\begin{enumerate}
\item Every page starts with equal probability of being reached. At this point we have not looked at any links! If there are ten pages on the internet then each one will start with the initial probability of $\frac{1}{10}$.
\item The random surfer follows a link with probability $(1-\alpha) $ and jumps to a random new page with probability $\alpha $. Thus, the probability of arriving at a particular page is: \\ $\alpha \cdot P($arrive randomly$) + (1-\alpha) \cdot P($arrive through a link$)$
\item To handle the case of nodes that have no outgoing edges, assume that each node has an implicit edge to itself.
\end{enumerate}

The iterative algorithm in equations:

\begin{align} 
P(p_i; 0) &= \frac{1}{N} \\
P(p_i; t+1) &= \frac{\alpha}{N} + (1- \alpha) \displaystyle\sum_{p_j \in M(p_i)} \frac{P(p_j;t)}{L(p_j)}
\end{align}

Keep repeating step 2 until all of the probabilities converge to something close to their true value. It shouldn't take too many iterations for the probabilities to become extremely stable.\\\\
Definition of terms:
\begin{itemize}
\item $p_i$ and $p_j$ are pages on the internet.
\item $P(p_i; t)$ is the probibility of stumbling upon page $p_i$ after $t$ iterations of this algorithm.
\item $N$ is the number of pages on the internet (i.e. the number of pages you have crawled).
\item $L(p_j)$ is the number of outgoing links from page $p_j$, including implicit link to itself. 
\item $M(p_i)$ is the set of pages that point to page $p_i$, again including itself.
\item $\alpha$ is the damping factor that makes this algorithm converge. It is generally agreed upon that $\alpha$ should be around 0.15.
\end{itemize}

\end{document} 
