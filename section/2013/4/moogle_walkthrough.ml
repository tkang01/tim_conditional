(* Makefile: used to build the project -- type "make all" at the
  command line to build the project. *)

(* order.ml: definitions for an order datatype used to compare values. *)

type order = Less | Eq | Greater ;;

let string_compare x y = 
  let i = String.compare x y in
    if i = 0 then Eq else if i < 0 then Less else Greater ;;

let int_compare x y = 
  let i = x - y in 
if i = 0 then Eq else if i < 0 then Less else Greater ;;

(* myset.ml: an interface and simple implementation of a set abstract
  datatype.*)

(* An interface for set modules *)
module type SET = 
sig
  type elt  (* type of elements in the set *)
  type set  (* abstract type for the set *)

  val empty : set

  val is_empty : set -> bool

  val insert : elt -> set -> set

  (* same as insert x empty *)
  val singleton : elt -> set

  val union : set -> set -> set
  val intersect : set -> set -> set

  (* remove an element from the set -- if the
   * element isn't present, does nothing. *)
  val remove : elt -> set -> set

  (* returns true iff the element is in the set *)
  val member : set -> elt -> bool

  (* chooses some member from the set, removes it 
   * and returns that element plus the new set.  
   * If the set is empty, returns None. *)
  val choose : set -> (elt * set) option

  (* fold a function across the elements of the set
   * in some unspecified order. *)
  val fold : (elt -> 'a -> 'a) -> 'a -> set -> 'a

  val string_of_set : set -> string
  val string_of_elt : elt -> string
end

(* parameter to Set modules -- we must pass in some 
 * type for the elements of a set, a comparison
 * function, and a way to stringify it.
 *)
module type COMPARABLE = 
  sig
    type t
    val compare : t -> t -> Order.order
    val string_of_t : t -> string
end


(* dict.ml: an interface and simple implementation of a dictionary
abstract datatype. *)

module type DICT = 
sig
  type key   
  type value 
  type dict
  val empty : dict 
  val insert : dict -> key -> value -> dict
  val lookup : dict -> key -> value option
  val remove : dict -> key -> dict
  val member : dict -> key -> bool

  (* Return an arbitrary key, value pair along with a new dict with that
   * pair removed.  Return None if the input dict is empty *)
  val choose : dict -> (key * value * dict) option
  val fold : (key -> value -> 'a -> 'a) -> 'a -> dict -> 'a
  val string_of_key: key -> string
  val string_of_value : value -> string
  val string_of_dict : dict -> string
end

(* Arguments to the AssocListDict functor *)
module type DICT_ARG = 
sig
  type key
  type value
  val compare : key -> key -> Order.order
  val string_of_key: key -> string
  val string_of_value : value -> string
end


(* query.ml: a datatype for Moogle queries and a function for
  evaluating a query given a web index. *)

module type QUERY_ARG = 
sig
  module S : Myset.SET with type elt = Util.CrawlerServices.link
  module D : Dict.DICT with type key = string
                       with type value = S.set
end

module Query(A : QUERY_ARG) = 
struct
  open A ;;
 (* util.ml: includes an interface and the implementation of crawler
 services needed to build the web index. This includes definitions of
 link and page datatypes, a function for fetching a page given a link,
 and the values of the command line arguments (e.g., the initial link,
 the number of pages to search, and the server eport.) *)

module type CRAWLER_SERVICES = 
  sig 
    (* links are used to describe a web address *)
    type link = { host : string ;  (* e.g., "www.eecs.harvard.edu" *)
                  port : int ;     (* e.g., 80 *)
                  path : string    (* e.g., "/~greg/index.html" *)
                } 

    val string_of_link : link -> string
    val href_of_link: link -> string
    val link_compare : link -> link -> order 

    (* pages are used to describe the contents of web pages *)
    type page = { url : link ;          (* see above *)
                  links : link list ;   (* all of the links on the page *)
                  words : string list   (* all of the words on the page *)
                }

    val string_of_page : page -> string
    (* given the link, returns the page -- should specify what 
     * exceptions get raised. *)
    val get_page : link -> page

    (* the initial link to be used by the crawler *)
    val initial_link : link 
    (* The root directory of the server. "" if crawling the web, 
    (dirname initial_link) otherwise *)
    val root_dir : string
    (* the number of (distinct) pages the crawler should process *)
    val num_pages_to_search : int
    (* the port on which to listen for query requests *)
    val server_port : int
  end
    
 (* moogle.ml: the main code for the Moogle server. Includes a stub
 for the crawler that you will need to complete, and the simple
 web-server code. *)
