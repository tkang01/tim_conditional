(* CS51 Section 4 *)
(* Week of 2/28/10 *)

(* The purpose of this section is to give you the tools to understand and
 * compare the time and space requirements for different algorithms. Having a
 * deeper understanding of these trade-offs will help you make better design
 * choices.
 *)

(******************  Part 1: Tail-calls  ******************)
(* In languages like ML, tail calls allow us to allocate stack space more
 * efficiently. Normally, space is allocated for every function that is called
 * and is not deallocated until that function has fully evaluated. These
 * function calls are very expensive in terms of both time and space. In the
 * particular case where the function returns another function call, there is
 * no need to keep the information from original function around; we've already
 * finished using all of the information we need. Thus, if we plan ahead we can
 * write recursive functions that use constant space and spend much less time
 * switching between functions (shuffling frame pointers) on the stack! *)

(* Here we see two versions of "reduce": foldl, which utilizes this tail call
 * optimization, and foldr, which does not. *)
let rec foldr f u xs =
  match xs with
  | [] -> u
  | x::rest -> f x (foldr f u rest) ;;

let rec foldl f u xs =
  match xs with
  | [] -> u
  | x::rest -> foldl f (f x u) rest ;;

(* Which one is our usual reduce? foldr (i.e., List.fold_right). *)

(* In the following examples, how would expressions 1 and 2 evaluate? *)
let nums = [1;2;3;4] ;;

(* Expression 1 *)
foldr (+) 0 nums ;;  (* 10 *)

(* Expression 2 *)
foldl (+) 0 nums ;;  (* 10 *)

(* In that example the result was the same in both cases, but this is not
 * always the case. Determine the result of the following expressions! What 
 * are the functions doing and is the result the same in both cases? *)

(* 1.1 *)
foldr ( * ) 1 nums ;;  (* 24 *)
foldl ( * ) 1 nums ;;  (* 24 *)

(* Result: 10
 * Finds the product of all of the elements in a list *)

(* 1.2 *)
let cons hd tl = hd::tl ;;

foldr cons [] nums ;;
foldl cons [] nums ;;

(* Not the same:
 *   foldr returns a list in with its elements in the original order
 *   foldl reverses the list *)

(* 1.3 *)
let more_nums = [[5;8;13];[2;3];[];[1;1]] ;;

foldr (@) [] more_nums ;;
foldl (@) [] more_nums ;;

(* Not the same:
 *   foldr flattens the list
 *   foldl flattens the list in reverse order (giving the fibonacci sequence!)
 *)

(* What property should functions have for the result to be the same for both
 * foldr and foldl? *)

(*
  It's sufficient for the first argument (the combining operation) to commute
  with itself. For example:
    -- a + b + c = b + a + c
    -- set_insert a (set_insert b s) = set_insert b (set_insert a s)
  but:
    -- it is not true in general that a :: b :: l = b :: a :: l.
*)

(**************  Part 2: Complexity and Big-O notation  **************)
(* The runtime of algorithms is often complicated and hard to
 * express. Big-O notation provides a way for us to simplify the way we
 * think about the complexity of algorithms and allows us to easily
 * compare across different algorithms. We can group algorithms with
 * the same Big-O complexity into time-complexity classes, which are
 * broad categorizations of the time it takes an algorithm to run.  *)

(* For example, take this simple algorithm for sorting a list:
 * 1) Count the number of items in the unsorted list. (n)
 * 2) Create a new list to hold sorted items. (c)
 * 3) Starting from the first item:
 *    a. Take the item at the beginning of the unsorted list. (n)
 *    b. Find the place where it should go in the sorted list. (n)
 * 
 * Thus, the runtime of this algorithm is T(n) = n^2 + n + c.
 * What does "Big-O of T(n)" mean? *)

(* Big-O is an estimate of the asymptotic time bound of a function. That is,
 * what does the running time of the algorithm tend to as the inputs become
 * very large?
 * Because we are only concerned with asymptotic running time, we can
 * disregard terms of lower orders; these terms have diminishing influence when
 * the size of inputs increase. In addition, we can disregard the coefficients
 * in front of terms, since constant multiples depend on the speed of the
 * processor and do not affect the underlying efficiency of the algorithm.
 *
 * For example, T(n) can be reduced in this fashion to n^2;
 * therefore, we could say that T(n) is in O(n^2)
 * (or, colloquially, T(n) = O(n^2)).
 * Note that we could also say T(n) is in O(n^3): big-O notation provides
 * an upper bound, not a precise characterization, of the asymptotic
 * running time (see the supplementary handout (you are not responsible
 * for the material on it!) for more details).
 * However, we'll usually use it to denote the precise asymptotic running time
 * (and this is what we're looking for when we ask questions about it).
 *)

(* The broad time-complexity classes we're interested in are:
 *
 *   constant time      O(c)
 *   logarithmic time   O(log n)
 *   polynomial time    O(n^x)
 *   exponential time   O(x^n)
 *
 * These are ordered from most time-efficient to least time-efficient. Why is
 * this so? Imagine graphing equations of these types, and you can see which
 * ones grow at a higher rate and eventually surpass the other equations. *)

(* Which of the following is the most efficient? Can you think of any examples
 * of algorithms that run in these times?
 *
 *   2^n
 *   log(n)
 *   n^3
 *   4n^3
 * 
 *)


(* In a binary search, we take as input a sorted array of ints and seek
 * a certain number within it. The strategy is intuitive: first you
 * check the middle element (arbitrarily choosing one of the middle
 * two elements if the array is of even length). If the middle element
 * is lower than the element you seek, you throw away the bottom half
 * of the array and recurse on the top half until you find the element
 * (or vice versa if the middle element is higher).
 *)


(* In how many steps would the binary search algorithm halt if it were to
 * search for the value 17, in the array 
 * L={2,3,5,7,11,13,17,19,23}? *)

(* What is the asymptotic running time of the binary search algorithm? *)

(* O(log n) *)



(******************  Part 3: Recurrence Relations  ******************)
(* To extract a recurrence relation from a function that describes its runtime,
 * we often break it down according to its cases. Each case can typically be
 * represented with its own equation, where we will map constant-time operations
 * to constant values, and function calls (recursive or otherwise) to applying 
 * mathematical functions. 
 *)

(* Useful recurrence relations and their solutions:

  T(n) = c                  O(1)

  T(n) = c + T(n-1)         O(n)

  T(n) = kn + T(n-1)       O(n^2)

  T(n) = c + T(n/2)         O(log n)

  T(n) = kn + T(n/2)       O(n)

  T(n) = kn + 2T(n/2)      O(n log n)

*)

(* Make sure that you have at least an intuitive sense of why these statements
 * are true. You will need to be comfortable using these facts in analyzing both
 * programs that you write and those that you might be presented as part of an
 * exam or problem set question. *)

(* Answer these questions for the following examples:
 *   What is the recurrence relation for this function's running time?
 *   What is the asymptotic running time?
 *   Does the function utilize tail-calls?
 *   If not, could we easily rewrite it so it does? *)

(* 3.1 *)
let sum (lst:int list) = foldr (+) 0 lst ;;

(* T(n) = c + T(n-1)
 * O(n)
 * No
 * foldl (+) 0 lst *)

(* 3.2 *)
let rec merge xlst ylst =
  match (xlst,ylst) with
    | (x::xtl,y::ytl) ->
	if x < y then x :: merge xtl ylst else y :: merge xlst ytl
    | (_,[]) -> xlst
    | ([],_) -> ylst ;;

(* T(m,n) = c(m+n) + d
 * or, if both are the same length, T(n) = cn + d
 * No

Tail-recursive version:

let rec merge_h alst blst lst =
  match (alst,blst) with
    | (_,[]) -> foldl cons lst alst
    | ([],_) -> foldl cons lst blst
    | (a::atl,b::btl) -> if a < b then merge_h atl blst (a::lst)
                                  else merge_h alst btl (b::lst) ;;
let merge alst blst =
  List.rev (merge_h alst blst []) ;;

 *)

(* 3.3 *)
let split lst =
  foldl (fun x (a,b) -> (x::b,a)) ([],[]) lst ;;

(* T(n) = c + T(n-1)
 * O(n)
 * Yes! *)

(* 3.4 *)
let rec mergesort (lst:int list) =
  match lst with
    | x::y::rest -> let (a,b) = split lst in merge (mergesort a) (mergesort b)
    | _ -> lst ;;

(* T(n) = kn + 2 * T(n/2) + c
 * O(n log n)
 * No
 * No *)

(* 3.5 *)
let partition (pivot:int) (lst:int list) =
  foldl (fun x (less,more) -> if x < pivot then (x::less,more)
	                                   else (less,x::more)) ([],[]) lst ;;

(* T(n) = c + T(n-1)
 * O(n)
 * Yes *)

(* 3.6 *)
let rec quicksort (lst:int list) =
  match lst with
    | [] -> []
    | x::rest -> let (lows,highs) = partition x rest in
	           quicksort lows @ (x :: quicksort highs) ;;

(* T(n) = kn + T(n-1-x) + T(x) + c for some x < n
 * O(n^2)  (conservative bound; usually quicker;
 *          can develop some intuition by looking at extreme cases:
 *          T(n) = kn + c + 2T(n/2) (always a good split);
 *          T(n) = kn + c + T(n-1) (always a bad split))
 * No
 * No *)

(* If you still feel uncomfortable with tail-calls, try rewriting the functions
 * above that were not tail-recursive. *)

(****************************** Part 4: Graphs ******************************)

type node=int;;

module type UNDIRECTED_GRAPH =
  sig
    type graph
    val empty: graph
    val isempty: graph->bool
    val neighbors: graph -> node -> node list
    val choose: graph -> node
    val remove: graph -> node -> graph
  end
;;

(* Here's one implementation of the signature above. This module
 * represents a graph as a list of tuples, each of which contains a node
 * and a list of its neighbors in numerical order. *)
module UndirectedIntGraph : UNDIRECTED_GRAPH =
  struct
    type graph = (node * node list) list
	(* N.B.: parsed as (node * (node list)) list. *)
    type node = int

    exception EmptyGraph
    exception IllFormedGraph

    let remove_dupls (l:'a list) : 'a list =
      reduce (fun x r -> if List.mem x r then r else x::r) [] l ;;

    let empty : graph = [] ;;
      
    let isempty (g:graph) : bool =
      match g with
	| [] -> true
	| _ -> false ;;
    
    let neighbors (g:graph) (n:node) : node list =
      match List.filter (fun (n',_) -> n' = n) g with
	| [] -> raise EmptyGraph
	| [(_,nbrs)] -> nbrs
	| _ :: _ :: _ -> raise IllFormedGraph ;;

    let choose (g:graph) : node =
      match g with
	| [] -> raise EmptyGraph
	| (n,_) :: _ -> n ;;

    let rec remove (g:graph) (n:node) : graph =
      match g with
	| [] -> []
	| (n',nbrs)::g' -> if n' = n then remove g' n
	    else (n', List.filter (fun x -> x != n) nbrs) :: remove g' n ;;
  end
;;

(* Exercise 4.1 *)
(* Write the recurrence relations for the remove function implemented above.
 * What is its Big-O running time ?*)

(*** SOLUTION ***
T_filter(n) = c + T_filter(n) = c + ... + c = cn = O(n)
T_graph(n) = k + T_filter(x) + T_graph(n-1) = (k + cx) + ... + (k + cx)
     = n (k + cx)
This is O(n*d) where n is the number of nodes and d is an upper bound on x, the
number of neighbors a node has. In the worst case, d = n-1 and remove runs
in time O(n^2).
*)

(* Now, we're going to write some searches on undirected graphs.
 * We want a way of representing the (ordered) set of nodes
 * we're scheduled to visit next in the search;
 * we should be able to "queue up" ("put") a node into the schedule,
 * (though it's not necessarily a queue),
 * and "get" the next scheduled node.
 *
 * We'll assume two implementations of this signature,
 * one which keeps the schedule as a stack (i.e., "put" is just cons,
 * and "get" is hd), and one as a queue (the two-list queue,
 * as shown in lecture).
 * 
 * We'll also assume we have an efficient implementation of sets,
 * in a suitable module called Set.
 *)

module type NODE_SCHEDULE = sig
  type schedule
  val empty : schedule
  val isempty : schedule -> bool
  exception EmptySet
  val get : schedule -> node * schedule
  val put : node -> schedule -> schedule
end
;;

open UndirectedIntGraph;;
module NodeSchedule = (* StackSchedule or QueueSchedule *);;
(* module NodeSet = Set(IntComparator) ;;  (e.g.) *)


(* Below is a general search function that takes a graph, a starting node,
 * a node to search for, and a nodeset (initially containing only one
 * element, the starting point of the search). It will begin at the
 * starting node and travel through the graph looking for the node
 * it's been asked to search for. Take a look at this code,
 * and reason through what it does. *)

let rec search' (g:graph) (goal:node) (to_visit:NodeSchedule.schedule)
    (visited:NodeSet.set) : bool =
  if Set.isempty to_visit then false
  else
    let (curr,remaining) = NodeSchedule.get to_visit in
      curr = goal ||
(let curr_neighbors =
        List.filter (fun n' -> not (NodeSet.member visited n))
	     (neighbors g curr) in
	      let to_visit' = reduce NodeSchedule.put to_visit curr_neighbors in
	         search' g goal to_visit' (NodeSet.put curr visited)) ;;

let search (g:graph) (goal:node) (start:node) =
  search' g goal (NodeSchedule.put start NodeSchedule.empty) NodeSet.empty ;;

(* Exercise 4.2
 * In what order does search visit the nodes in the graph below if it
 * is starting at node 1 and looking for node 4, if we use the StackSet
 * module as our nodeset implementation? What if we use the QueueSet
 * implementation? What general search strategies do these represent?
 *
 *          1--2--10--11
 *         /    \    /
 *        3      4--5
 *       / \    /   |
 *       6--7--8    9--14
 *         / \
 *        12  13
 *)

(*** SOLUTION ***
 * Stack: 1, 3, 7, 13, 12, 8, 4 (Depth First Search)
 * Queue: 1, 2, 3, 4 (Breadth First Search)
 *)

(* Exercise 4.3
 * Based on what you know about the search patterns of both search
 * implementations and the running times of the functions used, which
 * implementation seems more efficient in most cases, if either? What
 * about in the worst case? Which implementation would you use and why? *)

(*** SOLUTION ***
 * Stacks seem more efficient because both get and put run in O(1) time,
 * whereas for queues put runs in O(n) time. However, stacks don't perform
 * so well when the goal node is close to the start node. Consider a binary tree
 * with an infinitely long branch right of the root and the goal node left of
 * the root. Depth-first search may choose to search the right child first,
 * and will never reach the goal node. 
 * The principle of depth-first search is to search nodes that were encountered
 * most recently. The principle of breadth-first search is to search nodes in
 * order of increasing distance from the start node. 
 *)

(* Exercise 4.4
 * What's the bottleneck in this code that's most affecting its
 * asymptotic performance? Can you find a way to rewrite this code
 * to make it asymptotically faster?  *)

(*** SOLUTION ***
 * To remove already searched nodes from the graph, the search function calls
 * remove, which runs in time O(n^2). We may alleviate the bottleneck by
 * flagging nodes (using some boolean field) as searched or not, rather than
 * pulling searched nodes out of the graph.
 *)
