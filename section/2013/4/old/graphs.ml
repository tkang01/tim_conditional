let reduce f u x = List.fold_right f x u;;
(* Here's a partial signature for an Undirected Graph module. We
 * saw some useful graph functions last week, as well as two different
 * implementations of this signature. *)

type node=int
module type UNDIRECTED_GRAPH =
  sig
    type graph
    val empty: graph
    val isempty: graph->bool
    val neighbors: graph -> node -> node list
    val choose: graph -> node
    val remove: graph -> node -> graph
      
    val g: graph

  end
;;

(* Here's one implementation of the signature above. This module
 * represents a graph as a list of tuples, each of which contains a node
 * and a list of its neighbors in numerical order. *)
module UndirectedIntGraph : UNDIRECTED_GRAPH =
  struct
    type graph = (node * (node list)) list;;
    exception EmptyG of graph;;
    let remove_dupls l =
      reduce (fun x r -> if List.mem x r then r else x::r) [] l
	
    (* The methods and type definitions below go here! *)
    let empty = []
      
    let isempty g = (g=[]);;
    
    let neighbors (g:graph) (n:node) =
      let l = (List.filter (fun x -> (fst x) = n) g) in
	if l=[] then raise (EmptyG g) else
          snd (List.hd l)
    ;;

    let choose (g: graph) =
      fst (List.hd g)
    ;;

    let rec remove (g: graph) (n: node) =
      match g with
	| [] -> []
	| (gn, gl)::tl -> if gn=n then remove tl n
	  else (gn, List.filter (fun x -> x!=n) gl)::(remove tl n)
    ;; 
    let g = [(1, [2; 3]); (2, [1; 3; 4]); (3, [1; 2]); (4, [2; 5]); (5, [4])];;
  end
;;

(* Exercise 2.1 *)
(* Write the recurrence relations for the remove function implemented above.
 * What is its Big-O running time ?*)

(*** SOLUTION ***
T_filter(n) = c + T_filter(n) = c + ... + c = c = O(n)
T_graph(n) = c + T_filter(x) + T_graph(n-1) = (c + x) + ... + (c + x)
This is O(n*e) where n is the number of nodes and e is the number of edges.
*)

(* Now, we want a way of representing a set of nodes that allows us
 * to "get" a node from the set and "put" a node into the set.
 * We provide two implementations of this signature, one which uses
 * a stack, and one which uses the two-list queue Greg demonstrated
 * in lecture.
 * As discussed in lecture, get is constant time for both of these
 * implementations (although this is an amortized running time for the
 * queue implementation). For both implementations, put first checks if
 * the node is already in the set to avoid duplicates. This makes the
 * put function run in linear time in the number of nodes in the set. *)

module type NODESET = sig
  type nodeset
  val empty : nodeset
  val isempty : nodeset -> bool
  exception EmptySet
  val get : nodeset -> (node * nodeset)
  val put : node -> nodeset -> nodeset
end
;;

module StackSet : NODESET =
  struct
    type nodeset = node list
    let empty = []
    let isempty s = (s=[])
    exception EmptySet
    let get s = 
      match s with
	| [] -> raise EmptySet
	| h::t -> (h, t)
    let put (x: node) (s: nodeset) = 
      if List.mem x s then s else x::s
  end;;

module QueueSet : NODESET =
  struct
    type nodeset = {front: node list; rear: node list}
    let empty = {front=[]; rear=[]}
    let put x q = 
      if (List.mem x q.front) || (List.mem x q.rear) then
	q else {front=q.front; rear=x::q.rear}
    let isempty q =
      match q.front, q.rear with
        | [], [] -> true
        | _, _ -> false
    exception EmptySet
    let get (q: nodeset) : node * nodeset =
      match q.front with
        | h::t -> (h, {front=t; rear=q.rear})
        | [] -> (match List.rev q.rear with
		   | h::t -> (h, {front=t; rear=[]})
		   | [] -> raise EmptySet)
	    
  end
;;

open UndirectedIntGraph;;
module Set = StackSet;;
open Set;;

(* Below is a general search function that takes a graph, a starting node,
 * a node to search for, and a nodeset (initially empty). Take a look
 * at this code, and reason through what it does. *)

let rec search (g: graph) (r: node) (n: node) (s: nodeset) : bool =
  if r=n then true
  else 
    if g=UndirectedIntGraph.empty then false else
      let news = reduce Set.put s (UndirectedIntGraph.neighbors g r) in
	if(Set.isempty news) then false else
	  let (newr, news2) = (Set.get news) in
	    search (UndirectedIntGraph.remove g r) newr n news2
;;

(* Exercise 2.2
 * In what order does search visit the nodes in the graph below if it
 * is starting at node 1 and looking for node 4, if we use the StackSet
 * module as our nodeset implementation? What if we use the QueueSet
 * implementation? What general search strategies do these represent?
 *
 *          1--2--10--11
 *         /    \    /
 *        3      4--5
 *       / \    /   |
 *       6--7--8    9--14
 *         / \
 *        12  13
 *)

(*** SOLUTION ***
 * Stack: 1, 3, 7, 13, 12, 8, 4 (Depth First Search)
 * Queue: 2, 3 (Breadth First Search)
 *)

(* Exercise 2.3
 * Based on what you know about the search patterns of both search
 * implementations and the running times of the functions used, which
 * implementation seems more efficient in most cases, if either? What
 * about in the worst case? Which implementation would you use and why? *)

(* Exercise 2.4
 * What's the bottleneck in this code that's most affecting its
 * asymptotic performance? Can you find a way to rewrite this code
 * to make it asymptotically faster?  *)
