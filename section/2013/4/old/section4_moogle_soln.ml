(*** SECTION 4 ***)

(* Goal: Get you ready for Moogle
 * 
 * 1. Practice writing and using modules and functors
 * 2. Walk through A couple data types: sets, graphs, dictionaries
 * 3. An overview of Moogle and some tips
 * 
 * Some of the code presented here will be similar to the stuff you'll see in
 * Moogle. But there will be substantive differences! 
 *)

(****************************** Helpers *********************************)

(* The type order is used for comparison operations *)
type order = Less | Eq | Greater ;;

let string_compare x y = 
  let i = String.compare x y in
  if i = 0 then Eq else if i < 0 then Less else Greater ;;

let int_compare x y = 
  let i = x - y in 
  if i = 0 then Eq else if i < 0 then Less else Greater ;;

module type NODE = 
sig 
  type node
  val compare : node -> node -> order
end
;;

(****************** Part 1: Module Fun with Sets  ********************)

(* What is the difference between sets and lists? *)

(* Wiki: "A set is an abstract data structure that can store certain values, 
 * without any particular order, and no repeated values. It is a computer 
 * implementation of the mathematical concept of a finite set." *)

(* Here is the signature for a set of nodes *)

module type NODESET = 
sig
  module N : NODE
  type node = N.node
  type nodeset
  val empty : nodeset
  val isempty : nodeset -> bool
  (*  We cannot depend upon choose to return a specific element--
   *  the selection is random. If the set is empty, use None.*)
  val choose : nodeset -> (node * nodeset) option
  (*  Add a node to the set, but if node is repeated, do nothing. *)
  val put : node -> nodeset -> nodeset
end
;;


(* Problem 1.1
 * Implement a module SortedSet that takes a NODE. This set is sorted 
 * in ascending order according to NODE.compare.
 *)

(* Point here is to practice writing functors and using a val from 
 * the module that is passed in. 
 *)

module SortedSet(NA: NODE) : (NODESET with module N = NA) =
struct
  module N = NA
  type node = N.node
  type nodeset = node list
  let empty = []
  let isempty s = (s=[])
  exception EmptySet
  let choose s = 
    match s with
      | [] -> None
      | h::t -> Some (h, t)
  let rec put (x: node) (s: nodeset) = 
    match s with
      | [] -> [x]
      | h :: t -> 
        match N.compare h x with
          | Less -> h :: put x t
          | Eq -> s
          | Greater -> x :: s
end
;;

(* Problem 1.2
 * a) Define a StringNode module and pass it into the SortedSet functor to
 *    get a new module StringNodeSortedSet.
 * b) Without defining an IntNode module, define a queueset that consists of
 *    ints as nodes. 
 *
 * Hint: Take a look at module NODE in helpers. What does the signature 
 * require?
 *)

(* The point here is to practice using functors. *)

module StringNode : NODE = 
struct
  type node = string
  let compare = string_compare
end
;;

module StringSortedSet = SortedSet(StringNode);;

module IntSortedSet = SortedSet(struct
  type node = int
  let compare = int_compare
end);;




(**************************** Part 3: Graphs ******************************)

(* What is a graph? 
 *
 * Wikipedia: "A graph is an abstract representation of a set of objects where
 * some pairs of the objects are connected by links. The interconnected objects
 * are represented by mathematical abstractions called vertices [or nodes], and
 * the links that connect some pairs of vertices are called edges. Typically, a
 * graph is depicted in diagrammatic form as a set of dots for the vertices, 
 * joined by lines or curves for the edges."
 *
 * Two kinds of graphs: Directed vs. undirected. Examples: Facebook's
 * social graph is a undirected graph. Google's (or Moogle's) webpage
 * link graph is directed.
 *)

module type GRAPH =
sig
  module N : NODE
  type node = N.node
  type graph
  val empty : graph
  val isempty : graph -> bool
  val neighbors : graph -> node -> node list
  val choose : graph -> node option
  val remove : graph -> node -> graph
end
;;

(* Here is one implementation. *)

module UndirectedGraph (NA: NODE) : GRAPH =
struct
  module N = NA
  type node = N.node

  type graph = (node * node list) list
    (* N.B.: parsed as (node * (node list)) list. *)

  exception EmptyGraph
  exception IllFormedGraph

  let remove_dupls (l:'a list) : 'a list =
    List.fold_right (fun x r -> if List.mem x r then r else x::r) [] l ;;

  let empty : graph = [] ;;
  
  let isempty (g:graph) : bool =
    match g with
      | [] -> true
      | _ -> false ;;
  
  let neighbors (g:graph) (n:node) : node list =
    match List.filter (fun (n',_) -> n' = n) g with
      | [] -> raise EmptyGraph
      | [(_,nbrs)] -> nbrs
      | _ :: _ :: _ -> raise IllFormedGraph ;;

  let choose (g:graph) : node option =
    match g with
      | [] -> None
      | (n,_) :: _ -> Some n ;;

  let rec remove (g:graph) (n:node) : graph =
    match g with
      | [] -> []
      | (n',nbrs)::g' -> 
        if n' = n then remove g' n
        else (n', List.filter (fun x -> x != n) nbrs) :: remove g' n ;;
end
;; 

(* Problem 3.1 
 * Here we practice thinking about graphs. This is similar to when 
 * you implement crawler, which also requires you to traverse a graph. 
 *
 * Implement a search function using the module QueueSet that takes a graph,
 * a node to search for, and a nodeset (initially containing only one 
 * element, the starting point of the search). It returns whether the 
 * goal node is found or not. 
 *)

(* Recall the modules we wrote last section for implementing a queue and a stack,
 * using a SCHEDULER.
 *)

module type SCHEDULER =
sig
  type t
  type elem
  val empty : t
  val is_empty : t -> bool
  val take : t -> (elem * t) option
  val put : t -> elem -> t
end

module type QUEUE_ARG =
sig
  type t
end

module QueueStrategy (D : QUEUE_ARG) : (SCHEDULER with type elem = D.t) =
struct 
  type t = D.t list * D.t list
  type elem = D.t
  let empty = ([],[])
  let is_empty (a,b) = a = [] && b = []
  let take (a,b) = 
    match a with
      | [] ->
        (match List.rev b with
          | [] -> None 
          | hd :: tl -> Some (hd, (tl,[])))
      | hd :: tl -> Some (hd, (tl, b))
  let put (a,b) x = (a, x :: b)
end     

(*  Now write the pseudocode for the GraphSearch module. *)

module GraphSearch (G : GRAPH) (S : SCHEDULER with type elem = G.node) =
struct
  let search (g : G.graph) (start : G.node) (goal : G.node) : bool =
    let rec search_rec (g : G.graph) (to_visit : S.t) : bool =
      match S.take to_visit with
        | None -> false
        | Some (curr, rem) -> 
          (curr = goal ||
              (let to_visit' = List.fold_right (fun a b -> S.put b a) (G.neighbors g curr) rem in
               search_rec (G.remove g curr) to_visit'))
    in
    search_rec g (S.put S.empty start)
end
;;

(* Bonus: Why use sets and not lists? *)

(* Problem 3.2
 *
 * In what order does search visit the nodes in the graph below if it
 * is starting at node 1 and looking for node 4, if as above we use the 
 * QueueSet module as our nodeset implementation? What if we use the 
 * StackSet implementation? What general search strategies do these represent?
 *
 *          1--2--10--11
 *         /    \    /
 *        3      4--5
 *       / \    /   |
 *       6--7--8    9--14
 *         / \
 *        12  13
 *)

(*** SOLUTION ***
     * Stack: 1, 3, 7, 13, 12, 8, 4 (Depth First Search)
     * Queue: 2, 3 (Breadth First Search)
*)

(******************** Part 4: Dictionaries  *********************)

(* What is a dictionary? 
 *
 * Wiki: "a collection of unique keys and a collection of values, where each
 * key is associated with one value."
 * 
 * Examples: an actual dictionary, phonebook.
 *)

(* Problem 4.1
 * Write a signature for a dictionary. What would you include?
 *)

module type DICT = 
sig
  type key   
  type value 
  type dict
  val empty : dict 
  val insert : dict -> key -> value -> dict
  val lookup : dict -> key -> value option
  val remove : dict -> key -> dict
  val member : dict -> key -> bool
  val choose : dict -> (key * value * dict) option
  val fold : (key -> value -> 'a -> 'a) -> 'a -> dict -> 'a
end
;;

(* Problem 4.2
 * What are some ways to implement a dictionary? No need to write code. 
 *)

(* EXAMPLE SOLUTION: 
   type dict = (key * value) list;;
   type dict = (key * value) tree;;
   using Map library in OCaml
   etc.
*)

(* Problem 4.3
 * How can we write set in terms of dictionary? No need to write code. 
 *)

(* 
   module DictSet(D : Dict.DICT with type value = unit) 
   : (SET with type elt = D.key) = 
   struct
   ...
   end
*)

(* Problem 4.4
 * How can we write graph in terms of dictionary? No need to write code. 
 *)

(* EXAMPLE SOLUTION: 
   We can represent a graph as an edge dictionary:
   dictionary: node -> neighbor set
   Every node in the graph must be a key in the dictionary.
*)


(****************** Part 5: Getting started with Moogle *******************)

(* Moogle walk through. 
 * moogle.ml is the main file -- Start at the bottom and walk through the
 * calls there. Point out that the query engine is in many ways very similar to 
 * the expression evaluation they did in PS2.
 *)

(* Discussion:
 * Question 5.1: What's a good strategy for tackling a "large" codebase?
 * - Look over all the files.  Look at signatures and types, not 
 * implementations.
 * - Figure out what are the abstractions being used.
 * - Draw a picture of the main pieces, and what depends on what?

 * Question 5.2: How do I debug a "large" codebase?
 * - Unfortunately, we're too big for toplevel now.
 * - Use printing, asserts.  
 * - Write a test function which they should call on startup to test 
 * their code and print out their sets.  (Invoke your test as part 
 * of startup, and then use the command-line tools for compiling your
 * code instead of the interactive loop.)
 * 
 * Something to mention:
 * - The code will be patched over the next couple days. Look for updates.
 *)

