(*** SECTION 4 ***)

(* Goal: Get you ready for Moogle
 * 
 * 1. Practice writing and using modules and functors
 * 2. Walk through A couple data types: sets, graphs, dictionaries
 * 3. An overview of Moogle and some tips
 *)
 
(*RECOMMENDED
  ~15 minutes per part
 *)

(****************************** Helpers *********************************)

(* The type order is used for comparison operations *)
type order = Less | Eq | Greater ;;

let string_compare x y = 
  let i = String.compare x y in
    if i = 0 then Eq else if i < 0 then Less else Greater ;;

let int_compare x y = 
  let i = x - y in 
    if i = 0 then Eq else if i < 0 then Less else Greater ;;

module type NODE = 
sig 
  type node
  val compare : node -> node -> order
end
;;

(****************** Part 1: Module Fun with Sets  ********************)

(* Here is the signature for a set of nodes *)

module type NODESET = 
  sig
    module N : NODE
    type node = N.node
    type nodeset
    val empty : nodeset
    val isempty : nodeset -> bool
    exception EmptySet
    val get : nodeset -> (node * nodeset)
    val put : node -> nodeset -> nodeset
  end
;;

(* Problem 1.1
 * Remember queues from last section? Try implementing module QueueSet.
 * This implementation uses two lists instead of one. 
 *
 * Note: This is a functor! 
 *)

module QueueSet(NA: NODE) : (NODESET 
                              with module N = NA
                            ) =
  struct
    module N = NA
    type node = N.node
    type nodeset = {front: node list; rear: node list}
    let empty = {front=[]; rear=[]}
    let isempty q =
      match q.front, q.rear with
        | [], [] -> true
        | _, _ -> false
    exception EmptySet
    let put x q = 
      
      
      
      
      
    let get (q: nodeset) : node * nodeset =
      
      
      
      
      
  end
;;

(* Bonus question: Why might we use two lists instead of one? *)

(* Problem 1.2
 * Implement a module SortedSet that takes a NODE. This set is sorted according
 * to NODE.compare. 
 *)

module SortedSet(NA: NODE) : (NODESET with module N = NA) =
  struct
    module N = NA
    type node = N.node
    type nodeset = node list
    let empty = []
    let isempty s = (s=[])
    exception EmptySet
    let get s = 
      match s with
      | [] -> raise EmptySet
      | h::t -> (h, t)
    let put (x: node) (s: nodeset) = 
      
      
      
      
      
      
  end
;;

(* Problem 1.3
 * a) Define a StringNode module and pass it into a module StringSortedSet.
 * b) Without defining an IntNode module, define a queueset that consists of
 *    ints as nodes. 
 *
 * Hint: Take a look at module NODE in helpers. What does the signature 
 * require?
 *)

module StringNode : NODE = 






module StringSortedSet = 






module IntQueueSet = 










(**************************** Part 3: Graphs ******************************)

(* What is a graph? 
 *
 * Wikipedia: "A graph is an abstract representation of a set of objects where
 * some pairs of the objects are connected by links. The interconnected objects
 * are represented by mathematical abstractions called vertices [or nodes], and
 * the links that connect some pairs of vertices are called edges. Typically, a
 * graph is depicted in diagrammatic form as a set of dots for the vertices, 
 * joined by lines or curves for the edges."
 *
 * Two kinds of graphs: Directed vs. undirected. Examples: Facebook's social
 * graph is a undirected graph. Google's (or Moogle's) page rank is directed.
 *)

module type GRAPH =
  sig
    module N : NODE
    type node = N.node
    type graph
    val empty : graph
    val isempty : graph -> bool
    val neighbors : graph -> node -> node list
    val choose : graph -> node
    val remove : graph -> node -> graph
  end
;;

(* Here is one implementation. *)

module UndirectedGraph (NA: NODE) : GRAPH =
  struct
    module N = NA
    type node = N.node

    type graph = (node * node list) list
  (* N.B.: parsed as (node * (node list)) list. *)

    exception EmptyGraph
    exception IllFormedGraph

    let remove_dupls (l:'a list) : 'a list =
      reduce (fun x r -> if List.mem x r then r else x::r) [] l ;;

    let empty : graph = [] ;;
      
    let isempty (g:graph) : bool =
      match g with
        | [] -> true
        | _ -> false ;;
    
    let neighbors (g:graph) (n:node) : node list =
      match List.filter (fun (n',_) -> n' = n) g with
        | [] -> raise EmptyGraph
        | [(_,nbrs)] -> nbrs
        | _ :: _ :: _ -> raise IllFormedGraph ;;

    let choose (g:graph) : node =
      match g with
        | [] -> raise EmptyGraph
        | (n,_) :: _ -> n ;;

    let rec remove (g:graph) (n:node) : graph =
      match g with
        | [] -> []
        | (n',nbrs)::g' -> 
            if n' = n then remove g' n
            else (n', List.filter (fun x -> x != n) nbrs) :: remove g' n ;;
  end
;; 

(* Problem 3.1 
 * Here we practice thinking about graphs. This problem is similar to  
 * implementing crawler in Moogle, which requires you to traverse a graph. 
 *
 * Implement a search function using the module QueueSet that takes a graph,
 * a node to search for, and a nodeset (initially containing only one 
 * element, the starting point of the search). It returns whether the 
 * goal node is found or not. 
 *)

let rec search (g: UndirectedGraph.graph) (goal: UndirectedGraph.node) 
        (to_visit: QueueSet.nodeset) : bool =







(* Bonus: Why use sets and not lists? *)

(* Problem 3.2
 *
 * In what order does search visit the nodes in the graph below if it
 * is starting at node 1 and looking for node 4, if as above we use the 
 * QueueSet module as our nodeset implementation? What if we use the 
 * StackSet implementation? What general search strategies do these represent?
 *
 *          1--2--10--11
 *         /    \    /
 *        3      4--5
 *       / \    /   |
 *       6--7--8    9--14
 *         / \
 *        12  13
 *)







(******************** Part 4: Dictionaries  *********************)

(* What is a dictionary? 
 *
 * Wiki: "a collection of unique keys and a collection of values, where each
 * key is associated with one value."
 * 
 * Examples: an actual dictionary, phonebook.
 *)

(* Problem 4.1
 * Write a signature for a dictionary. What would you include?
 *)








(* Problem 4.2
 * What are some ways to implement a dictionary? No need to write code. 
 *)







(* Problem 4.3
 * How can we write set in terms of dictionary? No need to write code. 
 *)
 





(* Problem 4.4
 * How can we write graph in terms of dictionary? No need to write code. 
 *)
 






(****************** Part 5: Getting started with Moogle *******************)

(* Moogle walk through. *)

(* Discussion:
 * Question 5.1: What's a good strategy for tackling a "large" codebase?







 * Question 5.2: How do I debug a "large" codebase?







 *)

