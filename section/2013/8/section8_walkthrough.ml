(******************************************************************************)
(*          PART 1 : PS7 (OMGBees) Walkthrough                                *)
(******************************************************************************)

(* In problem set 7, you will be building a virtual world in which a number of 
   animals and other objects move around and interact with each other in
   various ways. The problem set writeup will walk you through each step in 
   creating the world and making objects interact in interesting ways. Here
   in section we will talk about the structure of the program as a whole  

Interfaces
- world_object_i
- world_object_t

+--------------------+
|                    |
| world_object_i (I) |
|                    |
+---------^----------+
          |
          |
          |
+---------+----------+    +---------------+    +---------------+     +-----------+
|                    |inh.|               |inh.|               |inh. |           |
+ world_object_t (I) <----+ movable_t (I) <----+ ageable_t (I) <-----+ bee_t (I) |
|                    |    |               |    |               |     |           |
+---------^----------+    +-------^-------+    +--------^------+     +------^----+
          |                       |                     |                   |
          | implements            | implements          | implements        |
          |                       |                     |                   |
 +--------+---------+      +------+------+       +------+------+            |
 |                  | inh. |             | inh.  |             |            |
 | world_object (C) <------+ movable (C) <-------+ ageable (C) |            |
 |                  |      |             |       |             |            | implements
 +--------^---------+      +------^------+       +------^------+            |
          |                       |                     |                   |
          |inherits               |inherits             |inherits           |
          |                       |                     |                   |
   +------+------+           +----+----+      +---------+--------+     +----+----+
   |             |           |         |      |                  | inh.|         |
   |   hive (C)  |           | cow (C) |      | carbon_based (C) <-----+ bee (C) |
   |             |           |         |      |                  |     |         |
   +-------------+           +---------+      +---------^--------+     +---------+
                                                        |
                                                        |inherits
                                                        |
                                                  +-----+------+
                                                  |            |
                                                  | flower (C) |
                                                  |            |
                                                  +------------+
- moveable_t

Create Heirarchy

The Event Handler


