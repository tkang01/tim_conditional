(* CS51
 * Section 8 - Events *)

(******************************************************************************)
(*          PART 0 : Events and Listeners                                     *)
(******************************************************************************)

(* Often, we want a program to react to things happening in our environment
 * rather than just execute a set of sequential instructions.  This paradigm
 * of "reacting to things happening in our environment" is called reactive
 * programming. Another way to think of reactive programming is to think of "a
 * thing happening" as a means for one thread to communicate to another thread.
 *
 * For example, consider a multi-threaded graphics-drawing application.  If one
 * of the threads has finished drawing an outline of a circle in the graphic, it
 * may want to tell other threads that the circle has been drawn so that those
 * other threads can color the circle. The first thread can send out a signal
 * to the application saying "Hey guys, I'm done drawing the circle!"  In other
 * words, the given thread has just communicated to other threads that it has
 * done something, so other threads should now react appropriately (hopefully
 * by coloring the circle). The coloring threads will be waiting for this
 * signal to occur; once they get the "Hey, I'm done" signal, the coloring
 * threads then color the circle.
 *
 * The idea of interpreting reactive programming as inter-thread communication
 * is at the heart of events. EVENTS are defined as the signals that threads
 * send out to an application, indicating to other threads that something has
 * happened and that they should react appropriately.  In order to send out
 * a signal, a thread creates a new event and "fires it," meaning that it
 * broadcasts the signal to the rest of the application.
 *
 * In our graphics example, not all of the threads need to hear that a thread
 * has finished drawing a circle.  (For example, do the square-coloring threads
 * care if some thread has drawn a circle?)  Instead, only the threads that need
 * to know that a circle has been drawn should be notified.
 *
 * In Ocaml, threads can tell the application that they would like to be
 * notified when a particular type of event has been fired, and what actions
 * they would like to take in response to that event.  This is where LISTENERS
 * come in.  Threads can add event listeners to an event, specifying for each
 * listener an EVENT HANDLER, or a method that should be executed when the event
 * is fired.  Whenever an events is fired, the threads that added listeners to
 * it are notified and execute the event handler immediately as a result.
 *
 * The three operations of events (creating events, firing events, and adding
 * listeners for events) make inter-thread communication easy and clean.  Much
 * more complex communication can be achieved by combining these operations.
 * See lecture notes for some examples of this.
 *)

(* StockMarket (adapted from section notes inspired by javaworld.com) *)

module type EVENT =
sig
  type id
  type 'a event
  type headline = Recession of string | IPO of string
                   | Bubble of string | InsiderTradingScandal of string
  type investment = Gold | Silver
                     | CallOption of string (* a bet that a stock will rise *)
                     | PutOption of string (*a bet that a stock will fall *)
                     | ForeignCurrency of string
                     | RealEstate of string
  type response = Invest of investment | InsiderTrade

  val new_event : unit -> 'a event
  val add_listener : 'a event -> ('a -> unit) -> id

  (* Similar to add_listener, but the handler is only called the first time the
   * event is fired. *)
  val add_one_shot : 'a event -> ('a -> unit) -> id

  (* Removes a listener identified by id from the event. *)
  val remove_listener : 'a event -> id -> unit

  (* Signals that the event has occurred *)
  val fire_event : 'a event -> 'a -> unit

  (* Some built-in events for the stock market. *)
  val headline_event : headline event
  val two_headline_event : (headline * headline) event
  val response_event : response event

  val terminate : unit -> 'a

  val run : (unit -> unit) -> unit
end

(* We're not going to spend time in section going over a concrete implementation
 * of the EVENT signature, but one implementation of a similar signature can be
 * found in lecture *)

module Event : EVENT =
...
end

open Event;;

(* Caricatured Stock Market
 * This model of the stock market should use events to describe the reactions of
 * investors when they learn of different types of news. *)

module StockMarket =
struct

  (* Passing the function stock_market a unit value will set off a chain of events:
   * we add listeners to headline_event, two_headline_event, and response_event.
   * Then, we fire a first headline event. *)

  let stock_market () =
    run
      (fun () ->
        (
          ignore (add_listener headline_event optimistInvestor);
          ignore (add_listener headline_event pessimistInvestor);
          ignore (add_listener headline_event hideboundInvestor);
          ignore (add_listener headline_event unclePennybags);
          ignore (add_listener two_headline_event insideInvestor);
          ignore (add_listener response_event newsAnchor);

          fire_event headline_event
            (IPO ("David Malan to take CS50 public at $51/share"))
        )
      )

  (* Exercise 1 *)
  (* Write the event handlers for each of the given players in the stock market,
     according to their description *)

  (* Exercise 1.1 *)
  (* The News Anchor should send out a new headline every time they hear
     learn about an investment made by wiseInvestor, foolInvestor,
     hideboundInvestor, unclePennybags, or insideInvestor. If they learn
     that insideInvestor has been conducting illegal insider trades, they
     should send out an InsiderTradingScandal headline. *)

  (* THERE CAN BE MANY IMPLEMENTATIONS OF THIS *)

  let newsAnchor (r: response) =
    match r with
    | InsiderTrade -> fire_event headline_event ???
    | Invest i ->
        match i with
        | PutOption _ ->
          fire_event headline_event (Bubble "DotCom")
        (* ???: fill out the remaining match cases!
        |
        |
        *)

  (* Exercise 1.2 *)
  (* If hideboundInvestor hears about a Recession in the news, he invests
     in gold and silver. Otherwise, he doesn't invest in anything. *)

  let hideboundInvestor (h: headline) = ???

  (* Exercise 1.3 *)

  (* optimistInvestor follows the news carefully and expects things to go well.
     If optimistInvestor hears about an IPO, she invests in call options on
     the relevant company's stock, expecting it to go up. If she hears about
     insider trading, she invests in foreign currency exchanges. If she learns of
     a recession, she invests in real estate. If she hears about a bubble, she does
     nothing. *)

  let optimistInvestor (h: headline) = ???

  (* Exercise 1.4 *)

  (* pessimistInvestor follows the news a bit and expects things to go badly.
     If pessimistInvestor hears about an IPO, she invests in put options on
     the relevant company's stock, expecting it to go down. If she hears about
     insider trading, she does nothing (what's the use???). If she learns of
     a recession or a bubble, she buys gold. *)

  let pessimistInvestor (h: headline) = ???

  (* Exercise 1.5 *)
  (* Uncle Pennybags** is unfailingly prosperous. Using his lucky coin,
     which he has for some reason named "random.bool", he unfailingly makes
     correct predictions about the successes and failures of IPOs and buys put
     or call options accordingly. As he is fabulously wealthy and allergic to
     taxes, he is otherwise prone to trading in foreign currency exchanges. *)

  let unclePennybags (h: headline) = ???

  (* Exercise 1.6 *)
  (* Whenever insideInvestor receives a headline about a Bubble, he
     polls his inside contacts and conducts an illegal InsiderTrade.

     Otherwise, he depends on you to provide him with a sound investment
     strategy. *)

  let insideInvestor (hs: headline * headline) = ???

end


(*  **

            .---.
            |#__|
           =;===;=
           / - - \
          ( _'.'_ )
         .-`-'^'-`-.
        |   `>o<'   |
        /     :     \
       /  /\  :  /\  \
     .-'-/ / .-. \ \-'-.
      |_/ /-'   '-\ \_|
         /|   |   |\
        (_|  /^\  |_)
          |  | |  |
      jgs |  | |  |
        '==='= ='==='

The character you may know as Mr. Monopoly is actually named "Rich Uncle
Pennybags," and first appeared on Monopoly boards in 1936.

(see: http://monopoly.wikia.com/wiki/Mr._Monopoly
      http://www.retrojunkie.com/asciiart/sports/monopoly.htm)

*)
