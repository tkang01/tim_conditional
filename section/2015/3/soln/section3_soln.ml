(* CS51 Section 3 *)

open Core.Std

(* ********************** Part 1 - Clean it up. ******************** *)

(* Over the course of your programming career, you will undoubtedly encounter
 * some poorly written code that is hard to read, hard to reason about, and
 * hard to modify.  With high probability, some of it will be your own code
 * that you're returning to six months after writing it :)  This exercise
 * is here to practice rewriting code to make it better, while at the same time
 * making sure that your new code has the same behavior as the old.
 *)

(* John Hacker recently learned OCaml, but has not taken CS51, and so
 * he doesn't know about map, fold_right or proper style. As a result, he
 * isn't aware that this horribly convoluted function he invented can
 * be written in one short, elegant line of code.
 *
 * Write a function that behaves identically but is simpler and written with
 * correct style.
 *
 * Hint: work by generating a series of simpler and simplerer "mystery"
 * functions. Don't try to jump to the "one short, elegant line of code"
 * in a single step. At each stage ensure (via testing) that
 * your new version of "mystery" works the same as the previous.
 *)

let rec mystery (lists : 'a list list) =
  if List.length lists = 0 then []
  else if List.length (List.hd_exn lists) = 0
  then mystery (List.tl_exn lists)
  else if List.length (List.hd_exn lists) = 1
  then let hd = List.hd_exn lists in
    ((List.hd_exn) hd) :: mystery (List.tl_exn lists)
  else let hd = List.hd_exn lists in
    (List.hd_exn) hd :: (mystery ((List.tl_exn hd)::(List.tl_exn lists)))
;;

let rec mystery' lists =
  match lists with
  | [] -> []
  | [] :: tl -> mystery' tl
  | [x] :: tl -> x :: mystery' tl
  | (a::b) :: tl -> a :: (mystery' (b::tl))
;;



(* Here's one test. Is this really enough? *)
assert (let x = [[];[]] in mystery x = mystery' x);;
assert (let x = [[1];[1;3;2]] in mystery x = mystery' x);;
assert (let x = [[1;2;4];[1;3;2];[7;8;9]] in mystery x = mystery' x);;

(* Now change to a better name, and make even shorter. *)
let flatten lists = List.fold_right ~f:List.append ~init:[] lists;;

(* With currying? *)
let flatten = List.fold_right ~f:List.append ~init:[];;

(* Make sure this behaves the same too *)
assert (let x = [[];[]] in mystery x = flatten x);;
assert (let x = [[1];[1;3;2]] in mystery x = flatten x);;
assert (let x = [[1;2;4];[1;3;2];[7;8;9]] in mystery x = flatten x);;


(* ***************** Part 2 - Map and fold_right ************************ *)
(* Exercises from section 2 for those sections that didn't get
 * a chance to cover map and fold_right thoroughly. The subsections match
 * the subsection from section one (i.e. subsection 1c. corresponds to 4c.
 * from section 2 notes).
 *)

(* Map and fold_right *)
(* Exercise 1 *)
let rec fold_right f u xs =
  match xs with
  | [] -> u
  | hd::tl -> f hd (fold_right f u tl);;

let rec map f xs =
  match xs with
  | [] -> []
  | hd::tl -> f hd :: map f tl;;

(* 1a. Implement length in terms of fold_right.
 * length lst returns the length of lst. length [] = 0. *)
let length (lst: int list) : int =
    fold_right (fun _ r -> r + 1) 0 lst
;;

(* 1b. Write a function that takes an int list and multiplies every int by 3.
 * Use map. *)
let times_3 (lst: int list): int list =
    map (( * ) 3) lst
;;

(* 1c. Write a function that takes an int and an int list and multiplies every
 * entry in the list by the int. Use map. *)
let times_x (x : int) (lst: int list): int list =
    map (( * ) x) lst
;;

(* 1d. Rewrite times_3 in terms of times_x.
 * This should take very little code. *)
let times_3_shorter = times_x 3 ;;

(* 1e. Write a function that takes an int list and generates a "multiplication
 * table", a list of int lists showing the product of any two entries in the
 * list.  e.g. mult_table [1;2;3] => [[1; 2; 3]; [2; 4; 6]; [3; 6; 9]] *)
let mult_table (lst: int list) : int list list =
    map (fun x -> times_x x lst) lst

;;

(* 1f. Write a function that takes a list of boolean values
 * [x1; x2; ... ; xn] and returns x1 AND x2 AND ... AND xn.
 * For simplicity, assume and_list [] is TRUE. Use fold_right. *)
let and_list (lst: bool list) : bool =
    fold_right (&&) true lst
;;

(* 1g. Do the same as above, with OR.
 * Assume or_list [] is FALSE. *)
let or_list (lst: bool list) : bool =
    fold_right (||) false lst
;;

(* 1h.	 Write a function that takes a bool list list and returns
 * its value as a boolean expression in conjunctive normal form (CNF).
 * A CNF expression is represented as a series of OR expressions joined
 * together by AND.
 * e.g. (x1 OR x2) AND (x3 OR x4 OR x5) AND (x6).
 * Use map and/or fold_right.
 * You may find it helpful to use and_list and or_list. *)
let cnf_list (lst: bool list list) : bool =
    and_list (map or_list lst)
;;


(* 1i. Write a function that takes an expr list and returns true if and only if
 * every expr in the list represents a true Boolean expression. *)

(* From section 2 *)

type expr = Value of bool | Not of expr | And of expr * expr | Or of expr * expr

let rec eval (a:expr) : bool =
  match a with
    | Value v -> v
    | Not e -> not (eval e)
    | And (e1, e2) -> eval e1 && eval e2
    | Or (e1, e2) -> eval e1 || eval e2
;;


let all_true (lst: expr list) : bool =
    and_list (map eval lst)
;;

(* You may find these helper functions from section 1 helpful. *)

let calc_option (f: 'a->'a->'a) (x: 'a option) (y: 'a option) : 'a option =
  match (x, y) with
  | (Some x', Some y') -> Some (f x' y')
  | (_, None) -> x
  | (None, _) -> y
;;

let min_option x y = calc_option min x y ;;

let max_option x y = calc_option max x y ;;

let and_option x y = calc_option (&&) x y ;;

(* 1j. Write and_list to return a bool option,
 * where the empty list yields None. Use fold_right. *)
let and_list_smarter (lst: bool list) : bool option =
    fold_right (fun x y -> and_option (Some x) y) None lst
;;

let and_list_smarter' (lst: bool list) : bool option =
    fold_right and_option None (map (fun x -> Some x) lst)
;;

(* 1k. Write max_of_list from section 1:
 * Return the max of a list, or None if the list is empty. *)
let max_of_list (lst:int list) : int option =
    match lst with
    | [] -> None
    | hd :: tl -> Some (fold_right max hd tl)
;;

(* 1l. Write bounds from section 1:
 * Return the min and max of a list, or None if the list is empty. *)
let bounds (lst:int list) : (int option * int option) =
    fold_right (fun x (min, max) -> (min_option x min, max_option x max))
        (None, None) (map (fun x -> Some x) lst)
;;

(* **************** Part 3 - More Map/Fold ******************* *)
(* For more practice writing and using higher order functions. *)

(* 3a. Fold fold fold
 * Higher order functions are a powerful tool, and fold_right in particular
 * is extremely powerful. In fact many higher order functions including
 * map, filter, and others can be written in terms of fold_right. Let's
 * implement several ourselves.
 *
 * Implement map, filter, and map2 using fold_right.
 *)
let map f lst =
    fold_right (fun x y -> (f x)::y) [] lst
;;

let filter f lst =
    fold_right (fun x y -> if f x then x::y else y) [] lst
;;

(* Challenge: map2 takes as input a two-argument function and two lists
 * and recurses through each list simultaneously, passing the hd of each list
 * as the arguments to the function, and spits out a new list with the return
 * values of the function.
 *
 * Hint: fold_right only works on one list at a time. What might you do to the
 * two lists so that fold_right can do its magic?
 *
 * Note: how should we handle lists of different lengths?
 *)
let map2 f lst1 lst2 =
    let rec zip (l1 : 'a list) (l2: 'b list) : ('a * 'b) list =
        match l1, l2 with
        | [], [] -> []
        | [], _
        | _, [] -> failwith "List lengths don't match"
        | h1::t1, h2::t2 -> (h1, h2) :: zip t1 t2
    in
    fold_right (fun (x1, x2) y -> (f x1 x2)::y) [] (zip lst1 lst2)
;;

(* 3b. filtermap
 * Write a function that takes
 *    -> a predicate, pred
 *    -> a one argument function f with argument type 'a
 *    -> a list of ('a)s, lst
 * The function should filter out items that make pred false, and
 * return the result of applying f on each element of the remaining
 * list.
 *
 * Your solution should use fold_right.
 *)
let filtermap (pred: 'a -> bool) (f: 'a -> 'b) (lst: 'a list) : 'b list =
    fold_right (fun x r -> if pred x then (f x)::r else r) [] lst
;;

(* 2c.  Use filtermap to write the deoptionalize function from PS2.
   As a reminder:
   deoptionalize [None; Some 2; None; Some 3; Some 4; None] = [2;3;4] *)
let deoptionalize lst =
    filtermap (fun x -> x <> None)
        (fun a -> match a with
            | Some x -> x
            | None -> failwith "Impossible") lst
;;

(* You may have noticed that you needed to raise an exception to make
   deoptionalize work properly with arbitrary option types. There are
   very few situations where you shouldn't be doing a complete match,
   and where you should fall back on exceptions. Here is an alternative
   (much better) way to define filter_map that avoids this problem. Try
   filling in the code for this definition (use fold_right here too) *)

let filter_map (f: 'a -> 'b option) (lst: 'a list) : 'b list =
    fold_right (fun x y ->
        match f x with
            | Some a -> a :: y
            | None -> y) [] lst
;;


(* Now write deoptionalize using this new filter_map *)
let deoptionalize' lst = filter_map (fun x -> x) lst ;;

(* ******************* Part 42 - Substitution Model ******************* *)
(* The purpose of these exercises is to help you develop a more formal
 * model of how ocaml code evaluates.  This is useful both when writing
 * and reading programs.  For some relevant examples, see the entries
 * at the Underhanded C contest:
 * http://underhanded.xcott.com/?page_id=5
 *)

(* For each of these, replace ??? with code that will make the snippet
 * evaluate to the integer 42.  The code should be properly
 * parenthesized--things like ") in blah blah in blah (" are not allowed.
 *
 * If this is not possible, justify why not.  "I couldn't figure out how" isn't
 * a valid justification.  "No matter what you replace ??? with, this expression
 * cannot possibly evaluate to 42 because..."  is a good start.
 *
 * (Note: the style of indentation here is meant to emphasize the concept
 * of scope within the subsitution model, and does not necessarily match
 * standard styling)
 *)

(* 42.1 *)
let f = fst in (* or "fun (x,_) -> x" *)
  f (42, 24)
;;


(* 42.2 *)
let x = 14 in
  let x' = (fun x -> let x = x * 2 in x) in
    List.fold_right ~f:(+) ~init:0 (List.filter ~f:(fun x -> x = x) [x' x; x])
;;

(* 42.3 *)
let f = (fun x y -> x + y) in
  let g = f 21 in
    g 21
;;

(* 42.4 *)

(* Impossible. g can't possibly be a function
 * that takes an integer *)
(*
let f = (fun (x,y) -> x + y) in
  let g = f (???) in
    g 21
;;
*)

(* 42.5.1 *)
let f = fun _ _ -> 42 in
  f f (f f)
;;

(* 42.5.2 *)
(* Trivial if you think about currying correctly *)
let f = fun _ _ _ -> 42 in
  (f f) f f
;;

(* From now on, your definitions cannot contain the value "42"! *)

(* 42.6 *)
let f = (fun _ y -> 2 * y) in
  List.fold_right ~f:f ~init:21 [f]
;;

(* 42.x: Bonus *)
let thequestion = 7 in
  6 * thequestion
;;

(* ******************* Part 5 - RSA Encryption ******************* *)
(* RSA Encryption - You've used it with git, you might use it with ssh,
 * you will implement a version of it on ps3, but what is it exactly?
 *
 * Let's start with a more general question: What is a cryptographic system?
 * A cryptographic system is a system designed to keep certain data/information
 * private by encoding that data/information and only allowing certain
 * individuals/groups to decode that data/information. A simple example
 * seen by many of you in CS51 is Caesar's cipher implemented below over
 * the ASCII character set:
 *)
let caesar_encrypt (key: int) (ptext: string) : string =
  String.map ~f:(fun c -> Char.of_int_exn ((Char.to_int c + key) mod 256)) ptext
;;

let caesar_decrypt (key: int)  (ctext: string) : string =
  caesar_encrypt (256 - (key mod 256)) ctext
;;
(* See how much simpler this in OCaml than C! Higher order functions FTW! *)

(* Caesar's cypher requires the same key for both encryption and decryption. Can
 * you see any scenario when this might be a problem? What if we used
 * different keys for encryption and decryption? This is the concept behind
 * public key encryption, an idea pioneered by Diffee and Hellman in 1976. In
 * public key encryption, the encryption key is made public and the decryption
 * key is kept private. Furthermore, knowing the encryption key cannot help
 * you find the decryption key. Therefore, someone can encode a message with
 * your public encryption key, send it to you, and know that only you can read
 * it!
 *
 * Is it possible to implement Caesar's cipher as the algorithm behind
 * a public key encryption system? Why or why not?
 *
 * RSA encryption is one of the most popular types of public key encryption.
 * Aside from being a public key encryption system, however, what is it
 * that makes RSA encryption effective? RSA encryption takes advantage
 * of the fact that there is no known algorithm for factoring large numbers
 * efficiently. How you might ask? Check out the pset! *)

(* Note: on the pset we have provided a lot of boiler plate code to get you
 * started. At first this may be overwhelming, but we didn't name our
 * functions "mystery" for a reason. Our comments might also prove quite
 * useful! *)
