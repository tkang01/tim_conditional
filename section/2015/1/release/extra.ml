(* Return the min and max of a list, or None if the list is empty. *)
let rec bounds (x : int list) : (int * int) option =

;;

(* From a list of pairs, retain only those that are in order. *)
let rec proper_pairs (l : (int * int) list) : (int * int) list =

;;

(* Given a matrix (list of lists), return the transpose.
 * The transpose of a matrix interchanges the rows and columns.
 * For example, transpose [[1;2;3];[4;5;6]];;
 * where [1;2;3] and [4;5;6] are the rows,
 * should return [[1;4];[2;5];[3;6]].
 *
 * Hint: write an auxiliary function, split, that
 * returns the first column of a matrix as a list
 * and the rest of the matrix as a list of rows.
 *
 * For now, don't worry about doing anything smart if the input
 * isn't a valid matrix.
 *)

let rec split (m : int list list) : (int list * int list list) option =

;;

let rec transpose (m : int list list) : int list list =

;;
