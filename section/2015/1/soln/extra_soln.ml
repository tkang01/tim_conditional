(* Return the min and max of a list, or None if the list is empty. *)
let rec bounds (x:int list) : (int * int) option =
  match x with
  | [] -> None
  | hd :: tl ->
    (match bounds tl with
    | None -> Some (hd, hd)  (* xtl is empty, so xhd is the only element;
			          it's the min and the max of x. *)
    | Some (min, max) ->
      Some ((if hd < min then hd else min),
            (if hd > max then hd else max))) ;;

(* Note the double parentheses around the elements in the pair:
     ((if xhd < min then . . .
   Some people (especially those who have used other functional languages)
   might be tempted to leave them out.
   However, "," binds tighter than if-then-else.
   High jinks may ensue. *)

(* From a list of pairs, retain only those that are in order. *)
let rec proper_pairs0 (lst : (int * int) list) : (int * int) list =
    match lst with
    | [] -> []
    | ((x1, x2) as hd) :: tl -> if x1 <= x2 then hd :: proper_pairs0 tl
                                else proper_pairs0 tl

let rec proper_pairs (l:(int * int) list) : (int * int) list =
  match l with
  | [] -> []
  | (x1, x2) :: ps -> let pps = proper_pairs ps in
                      if x1 <= x2 then ((x1, x2) :: pps) else pps ;;

(* Can also introduce abbreviated syntax for proper_pairs: *)
let rec proper_pairs' (l:(int * int) list) : (int * int) list =
  match l with
  | [] -> []
  | ((x1, x2) as p) :: ps -> let pps = proper_pairs' ps in
                             if x1 <= x2 then (p :: pps) else pps ;;

(* Given a matrix (list of lists), return the transpose.
 * The transpose of a matrix interchanges the rows and columns.
 * For example, transpose [[1;2;3];[4;5;6]];;
 * where [1;2;3] and [4;5;6] are the rows,
 * should return [[1;4];[2;5];[3;6]].
 *
 * Hint: write an auxiliary function, split, that
 * returns the first column of a matrix as a list
 * and the rest of the matrix as a list of rows.
 *
 * Behavior of solution is rather odd if m is not a valid matrix.
 *)

let rec split (m:int list list) : (int list * int list list) option =
  match m with
  | [] -> None
  | [] :: ls -> split ls
  | (x :: xs) :: ls ->
    match split ls with
    | Some (c, ls_rest) -> Some (x :: c, xs :: ls_rest)
    | None -> Some ([x], [xs]) ;;

let rec transpose (m:int list list) : int list list =
  match split m with
  | Some (c, m_rest) -> c :: transpose m_rest
  | None -> [] ;;
