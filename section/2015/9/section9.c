/* C program with observable race condition, corresponding to exercise 1. */
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

/* Compile with gcc -O -pthread section9.c
 *
 * Without the -O you don't get any race condition behavior, but
 * turn on any level of optimization and it shows up.
 *
 * Also, x must be declared volatile or you get weird behavior:
 * without volatile on -O it returns either 1000000 or 2000000,
 * but on higher optimization levels it's always the "correct"
 * 2000000.
 * */

#define NUM_THREADS 2

volatile int x;

void *g(void *tid) {
  (void)tid;
  int repeat;
  for (repeat = 1000000; repeat > 0; repeat--) {
    x++;
  }
  pthread_exit(NULL);
}

void main(int argc, char *argv)
{
  x = 0;
  pthread_t threads[NUM_THREADS];
  int problem;
  long t;
  for(t=0;t<NUM_THREADS;t++){
    problem = pthread_create(&threads[t], NULL, g, (void *)t);
    if (problem){
      printf("ERROR; return code from pthread_create() is %d\n", problem);
      exit(-1);
      }
    }

  for(t=0;t<NUM_THREADS;t++){
    pthread_join(threads[t], NULL);
  }

  printf("%d\n", x);
  pthread_exit(NULL);
}
