/*
 * CS 51 Section 7
 * Week of 3/29/10
 *
 * The Official "Welcome to Java" Section
 * 
 */

import java.util.*; // See below for explanation.

/* ************************** PART 1: INTERFACES **************************
 * An interface defines a set of functions that must exist in a particular
 * implementation, but it doesn't specify any of the lower level details.
 * This gives us the freedom to implement
 * functionality prescribed by the interface in any way we please, and, perhaps
 * more importantly, make changes down the line without breaking other
 * parts of a codebase.
 *
 * As an example, you've probably heard the term "API" -- this stands for
 * application programming interface, and broadly it means a
 * (documented) way of allowing two different pieces of software to
 * interact with each other.  For instance, the Java API specification
 * describes all of the packages that you have access to as a Java
 * developer.  
 * Notice above how we "import java.util.*".  This 'means' "give this code
 * that we are writing access to everything that is 'in' the java.util
 * package."  The java.util package includes all kinds of useful interfaces,
 * like Collection, List, and Set, and classes like HashSet that provide
 * concrete implementations of these interfaces.  The point is that you,
 * as the programmer of whatever cool functionality you are currently
 * working on, shouldn't have to to worry about the mechanics of how
 * things like set and list operations work.  In particular, you should
 * be able to switch which implementation of an interface you are using,
 * for instance from HashSet to TreeSet, without having to change
 * any of the rest of your code.
 * 
 * If this sounds a lot like the points that we were trying to drive
 * home with the module system in Ocaml, that's because it is.
 * It's simply another way of approaching the general issue of trying
 * to allow for modularized code and abstraction barriers.  Both approaches
 * carry important advantages and disadvantages (some may say that Java
 * in fact has no abstraction), and we will be discussing these issues
 * more in the coming weeks.
 * 
 */

// Here is a basic example of an interface:
interface Animal {
    abstract String getSpecies();
    abstract int getAge();
    // returns an animal's relative level of coolness
    abstract int getCoolnessLevel();
}

/* Let's implement two types of animals (Dragons and Humans) to get more
 * familiar with Java syntax and practice making classes.
 */

/* Part 1.1: IMPLEMENTING INTERFACES
 * To implement an interface, we MUST implement all of the functions it
 * describes. In a class, we can define our own variables and any extra
 * functions that we will need.
 *
 * The "private" keyword makes a variable inaccesible to outside users.
 * Why might we want to do this?
 */

class Dragon implements Animal {
    private int age;
    private int numPeopleEaten;
    public Dragon(int age) {
        this.age = age;
        numPeopleEaten = 0;
    }
    public String getSpecies() { return "DRAGON"; }
    public int getAge() { /* implement me! */ }
    public int getCoolnessLevel() { return 800 + (100 * numPeopleEaten); }
    public void eatPerson(Person p) { /* implement me */ }
}

/* Question 1.1.1: Fill in the getAge method */




/* Question 1.1.2: Fill in the eatPerson method */





/* ************************** PART 2: ABSTRACT CLASSES ************************
 * An abstract class cannot actually be instantiated, but it gives the framework
 * for its subclasses. It allows us to factor out functions and variables that
 * will appear in all of its subclasses to make our code more concise and 
 * manageable.
 *
 * The "protected" keyword allows us to use a variable in a class and ALL of its
 * subclasses whereas "private" only allows us to use it in the current class.
 * Protected variables don't need to be in an abstract class, but they only
 * really make sense if you are going to be extending the class.
 */

abstract class Person implements Animal {
    protected String name;
    protected int age;
    protected int baseCoolness;

    Person(String name, int age) {
        this.name = name;
        this.age = age;
        baseCoolness = 30;
    }
    public String getName() { return name; }
    public String getSpecies() { return "Homo sapiens"; }
    public int getAge() { return age; }
    abstract public int getCoolnessLevel();
}

/* Question 2.1.1:
 * What is the difference between abstract classes and interfaces? */




/* Question 2.1.2: When would you use each of them? */




/* Part 2.2: EXTENDING ABSTRACT CLASSES
 * Since we defined most of the functions for the Animal interface in Person,
 * we only need to worry about the function getCoolnessLevel which depends on
 * the particular person.
 *
 * The function "super" passes variables to the constructor of the super class
 * so that we are not rewritting the same code in every subclass.
 */

class CSstudent extends Person {
    public CSstudent(String name, int age) {
        super(name, age);
    }
    public int getCoolnessLevel() { return baseCoolness * 3; }
    public void takeCSclass(Person headTF) {
        if (headTF.getName().equals("Victor")) {
            baseCoolness++;
        }
    }
}

// This will allow us to make multiple instances of Victor so that he can be
// the head TF of every single CS class at Harvard.
class Victor extends CSstudent {
    Victor() {
        super("Victor", 27);
        baseCoolness += 3;
    }
}

/* Question 2.2.1:
 * Can we pass a Victor to any function that expects a CSstudent? */




/* Question 2.2.2:
 * Can we pass a CSstudent to any function that expects a Victor? */




// Now, here's a short example from a school very similar to Harvard.

/* 'enum' stands for enumeration: like ML's algebraic data types,
 * but much less powerful. */
enum Specialty { DIVINATION, ANCIENT_RUNES, TRANSFIGURATION, POTIONS, DARK_ARTS }

class Wizard extends Person {
    private Specialty specialty;
    private Set<Dragon> pets;
    public Wizard(String name, int age, Specialty s, Collection<Dragon> pets) {
        super(name, age);
        specialty = s;
        this.pets = new TreeSet<Dragon>(pets);
    }
    public void getNewPet(Dragon d) { pets.add(d); }
    public int getCoolnessLevel() {
        int dangerLevel = 5 + pets.size();
        if (specialty == Specialty.DARK_ARTS) dangerLevel += 3;
        return baseCoolness * dangerLevel;
    }
}

class WizardTest {
    public static void main() {
        Wizard snape = new Wizard("Severus", 50, Specialty.POTIONS, 
                                  new LinkedList<Dragon>());
    }
}

/* Question 2.2.3: What is a major difference between snape and Victor, other
 * than that Victor is a much better teacher?
 * (Notably, why is one capitalized and the other not?) */





/* **************** PART 3: LARGER INTERFACE EXAMPLE ******************/

class IntCollectionFullException extends Exception { }

interface IntCollection {
    abstract boolean isEmpty();
    abstract boolean contains(int i);
    abstract int size();
    abstract void add(int i) throws IntCollectionFullException;
    abstract void clear();
    abstract int[] toArray();
}

/* IntFixQueue is a queue that has a limited amount of space. This can be
 * helpful if we know we will never have more than n things.
 *
 * Note that this is an /imperative/, not a functional queue --
 * in ML-speak, our IntCollection 'add' (enqueue) has the type:
 *   int -> queue -> unit
 * rather than the type one might expect:
 *   int -> queue -> queue
 * Without side effects, an operation having this type would be useless! (Why?)
 */

class QueueEmptyException extends Exception { }
class QueueFullException extends IntCollectionFullException { }

class IntFixQueue implements IntCollection {
    private int maxSize, size, start;
    private int[] q;

    IntFixQueue(int max) {
        size = 0;
        start = 0;
        maxSize = max;
        q = new int[maxSize];
    }
    public boolean isEmpty() { return size == 0; }
    public boolean contains(int x) {
        for (int i : this.toArray()) {
            if (i == x) return true;
        }
        return false;
    }
    public int size() { return size; }
    public void enqueue(int i) throws QueueFullException {
        ++size;
        if (size > maxSize) {
            throw new QueueFullException();
        }
        q[(start + size) % maxSize] = i;
    }
    public int dequeue() throws QueueEmptyException {
      /* Implement me! */












    }
    public void add(int i) throws QueueFullException {
        enqueue(i);
    }
    public void clear() {
        start = size = 0;
    }
    public int[] toArray() {
        int[] out = new int[size];
        for (int i = 0; i < size; i++) {
            out[i] = q[(start + i) % maxSize];
        }
        return out;
    }
}


// Question 3.0:
//
// Fill in the 'dequeue' method above.


// Question 3.1:
//
// Explain why the 'clear' method works.







// Question 3.2:
//
// The 'enqueue' method has a bug.
// What is it, and how do we fix it?








// Question 3.3:
//
// Note the first line of toArray: it uses new
// (analogous to malloc, for C hackers).
// This allocates memory.
// When can this memory be freed?
// What code is responsible for freeing it?







// Question 3.4:
//
// The 'contains' method is inefficient. Why?
// How might we rewrite it to be more efficient?
// Would this duplicate code with another method?
// Suggest a way we could factor this code out if we were writing in ML
// (it's not so easy to do in Java...).








// Question 3.5:
//
// Why is it somewhat unappealing to have a 'IntCollection' interface?
// What is ML's solution to this problem?
// Java has a similar solution ('generics'), although the implementation is
// somewhat different and the type system doesn't allow quite as much
// flexibility.

// In fact, the actual Java standard library has an interface called Collection
// implemented in terms of generics:
//
// public interface Collection<E> { 
//   public boolean add(E e) { ... }
//   ...
// }
//
// (read "collection of E")
//
// (ArrayList, LinkedList, TreeSet and other classes implement this interface,
// and their use is highly encouraged (we've already seen how painful it can be
// to reinvent the wheel, particularly when the wheel is a balanced tree!).)
//
// We might want Collection to have a method:
//
//   public E[] toArray() { ... }
//
// But there's no clear way to write this
// (and indeed the Java developers didn't, opting instead to have the method
// take the destination array as an argument).
// What goes wrong?
