/*
 * CS 51 Section 7
 * Week of 3/29/10
 *
 * The Official "Welcome to Java" Section
 * 
 */

import java.util.*; // See below for explanation.

/* ************************** PART 1: INTERFACES **************************
 * An interface defines a set of functions that must exist in a particular
 * implementation, but it doesn't specify any of the lower level details.
 * This gives us the freedom to implement
 * functionality prescribed by the interface in any way we please, and, perhaps
 * more importantly, make changes down the line without breaking other
 * parts of a codebase.
 *
 * As an example, you've probably heard the term "API" -- this stands for
 * application programming interface, and broadly it means a
 * (documented) way of allowing two different pieces of software to
 * interact with each other.  For instance, the Java API specification
 * describes all of the packages that you have access to as a Java
 * developer.  
 * Notice above how we "import java.util.*".  This 'means' "give this code
 * that we are writing access to everything that is 'in' the java.util
 * package."  The java.util package includes all kinds of useful interfaces,
 * like Collection, List, and Set, and classes like HashSet that provide
 * concrete implementations of these interfaces.  The point is that you,
 * as the programmer of whatever cool functionality you are currently
 * working on, shouldn't have to to worry about the mechanics of how
 * things like set and list operations work.  In particular, you should
 * be able to switch which implementation of an interface you are using,
 * for instance from HashSet to TreeSet, without having to change
 * any of the rest of your code.
 * 
 * If this sounds a lot like the points that we were trying to drive
 * home with the module system in Ocaml, that's because it is.
 * It's simply another way of approaching the general issue of trying
 * to allow for modularized code and abstraction barriers.  Both approaches
 * carry important advantages and disadvantages (some may say that Java
 * in fact has no abstraction), and we will be discussing these issues
 * more in the coming weeks.
 * 
 */

// Here is a basic example of an interface:
interface Animal {
    abstract String getSpecies();
    abstract int getAge();
    // returns an animals relative level of coolness
    abstract int getCoolnessLevel();
}

/* Let's implement two types of animals (Dragons and Humans) to get more
 * familiar with Java syntax and practice making classes.
 */

/* Part 1.1: IMPLEMENTING INTERFACES
 * To implement an interface, we MUST implement all of the functions it
 * describes. In a class, we can define our own variables and any extra
 * functions that we will need.
 *
 * The "private" keyword makes a variable inaccesible to outside users.
 * Why might we want to do this?
 */

class Dragon implements Animal {
    private int age;
    private int numPeopleEaten;
    public Dragon(int age) {
        this.age = age;
        numPeopleEaten = 0;
    }
    public String getSpecies() { return "DRAGON"; }
    public int getAge() { return age; } 
    public int getCoolnessLevel() { return 800 + (100 * numPeopleEaten); }
    public void eatPerson(Person p) { numPeopleEaten++; } 
}
/* Question 1.1.1: Fill in the getAge method */
// (Solution provided inline) 


/* Question 1.1.2: Fill in the eatPerson method */
// (Solution provided inline) 


/* ************************** PART 2: ABSTRACT CLASSES ************************
 * An abstract class cannot actually be instantiated, but it gives the framework
 * for its subclasses. It allows us to factor out functions and variables that
 * will appear in all of its subclasses to make our code more concise and 
 * manageable.
 *
 * The "protected" keyword allows us to use a variable in a class and ALL of its
 * subclasses whereas "private" only allows us to use it in the current class.
 * Protected variables don't need to be in an abstract class, but they only
 * really make sense if you are going to be extending the class.
 */

abstract class Person implements Animal {
    protected String name;
    protected int age;
    protected int baseCoolness;

    Person(String name, int age) {
        this.name = name;
        this.age = age;
        baseCoolness = 30;
    }
    public String getName() { return name; }
    public String getSpecies() { return "Homo sapiens"; }
    public int getAge() { return age; }
    abstract public int getCoolnessLevel();
}

/* 2.1.1:  What is the difference between abstract classes and interfaces? */
/*
 * The basic difference is that abstract classes can contain instance fields
 * and can contain concrete implementations of methods.  That is, in an
 * abstract class you get to choose what is abstract (by puttting the 
 * keyword "abstract" before it) and what is concrete (by implementing it).
 * On the contrary, in an interface, /every/ method must be abstract,
 * and there is no notion of instance fields.
 *
 * One point to note here is that while we can (and do) provide a
 * constructor for the abstract class Person, we cannot instantiate
 * a Person directly because Person is an abstract class.  The constructor
 * that we have provided will (must) be invoked by constructors in
 * subclasses of Person, and therefore serves to factor out the portion
 * of instantiation that is common to all Persons.
 */



/* 2.1.2: When would you use each of them? */
/*
 * There is no one answer to this question, but one starting point to note
 * is that interfaces allow you to provide a list of operations that
 * are available regardless of representation, whereas on the other hand
 * abstract classes allow you define an (incomplete) picture of the 
 * representation.
 *
 * One important immediate implication of this is that an abstract
 * class allows you to factor out code that will be common to all subclassses.
 * For instance, in our trivial example here, we have realized that
 * all humans implement getName(), getSpecies(), and getAge() in the exactly same
 * way, simply by returning these values.  If Person were an interface,
 * then all classes that implemented Person would have to re-implement
 * these identical methods, and the identical instance fields that allow
 * for them.
 * 
 * However, one caveat is that by over-implementing, or even over-prescribing
 * at the level of the superclass, you may be making too many predictions
 * about how all possible subclasses will behave.  An important design
 * decision, then, is where to draw the line with regard to 1) what abstract
 * operations will be implemented by all concrete classes that extend a given
 * abstract class or implement a given interface, and 2) in the case of
 * abstract classes, which of these operations will have a common concrete
 * implementation.
 */



/* Part 2.2: EXTENDING ABSTRACT CLASSES
 * Since we defined most of the functions for the Animal interface in Person,
 * we only need to worry about the function getCoolnessLevel which depends on
 * the particular person.
 *
 * The function "super" passes variables to the constructor of the super class
 * so that we are not rewritting the same code in every subclass.
 */

class CSstudent extends Person {
    public CSstudent(String name, int age) {
        super(name, age);
    }
    public int getCoolnessLevel() { return baseCoolness * 3; }
    public void takeCSclass(Person headTF) {
        if (headTF.getName().equals("Victor")) {
            baseCoolness++;
        }
    }
}

// This will allow us to make multiple instances of Victor so that he can be
// the head TF of every single CS class at Harvard.
class Victor extends CSstudent {
    Victor() {
        super("Victor", 27);
        baseCoolness += 3;
    }
}

/* 2.2.1: Can we pass a Victor to any function that expects a CSstudent? */
/* Yes.  a instanceof Victor implies a instanceof CSstudent, because
 * Victor is a subclass of CSstudent. */


/* 2.2.2: Can we pass a CSstudent to any function that expects a Victor? */
/* No.  The converse of the implication stated above does not hold, because
 * CSstudent is a superclass of Victor.  Loosely speaking, one way to
 * think about this is that Victor is "more specific" than CSstudent.
 * In English, "A Victor is a CSstudent, but a CSstudent is not
 * necessarily a Victor."
 *
 * How could you check if a CSstudent is a Victor?
 * How could you force Java to cast a CSstudent to a Victor, or alternatively
 * throw an exception is this is impossible?
 * (Ask us if you don't know!).
 */


// Now, here's a short example from a school very similar to Harvard.

/* 'enum' stands for enumeration: like ML's algebraic data types,
 * but much less powerful. */
enum Specialty { DIVINATION, ANCIENT_RUNES, TRANSFIGURATION, POTIONS, DARK_ARTS }

class Wizard extends Person {
    private Specialty specialty;
    private TreeSet<Dragon> pets;
    public Wizard(String name, int age, Specialty s, Collection<Dragon> pets) {
        super(name, age);
        specialty = s;
        this.pets = new TreeSet<Dragon>(pets);
    }
    public void getNewPet(Dragon d) { pets.add(d); }
    public int getCoolnessLevel() {
        int dangerLevel = 5 + pets.size();
        if (specialty == Specialty.DARK_ARTS) dangerLevel += 3;
        return baseCoolness * dangerLevel;
    }
}

class WizardTest {
    public static void main() {
        Wizard snape = new Wizard("Severus", 50, Specialty.POTIONS, 
                                  new LinkedList<Dragon>());
    }
}

/* 2.2.3: What is a major difference between snape and Victor, other than
 * that Victor is a much better teacher?
 * (Notably, why is one capitalized and the other not?) */
/* snape is an object of type Wizard, while Victor is a class.
 * That is, we can use the class Victor to instantiate objects, whereas
 * snape is already an object.
 *
 * We apologize if this example is confusing.  Yes, in real life, Victor
 * is just one person, so it would make sense to have an object victor
 * of class CSstudent.  However, at Harvard, Victor is truly in a class of
 * his own.  We must have multiple Victors to have all of the TFs that
 * we need, so instead of just creating one Victor, we create a rule for
 * making multiple Victors -- a class Victor.
 */




/* **************** PART 3: LARGER INTERFACE EXAMPLE ******************/

class IntCollectionFullException extends Exception { }

interface IntCollection {
    abstract boolean isEmpty();
    abstract boolean contains(int i);
    abstract int size();
    abstract void add(int i) throws IntCollectionFullException;
    abstract void clear();
    abstract int[] toArray();
}

/* IntFixQueue is a queue that has a limited amount of space. This can be
 * helpful if we know we will never have more than n things.
 *
 * Note that this is an /imperative/, not a functional queue --
 * in ML-speak, our IntCollection 'add' (enqueue) has the type:
 *   int -> queue -> unit
 * rather than the type one might expect:
 *   int -> queue -> queue
 * Without side effects, an operation having this type would be useless! (Why?)
 */

class QueueEmptyException extends Exception { }
class QueueFullException extends IntCollectionFullException { }

class IntFixQueue implements IntCollection {
    private int maxSize, size, start;
    private int[] q;

    IntFixQueue(int max) {
        size = 0;
        start = 0;
        maxSize = max;
        q = new int[maxSize];
    }
    public boolean isEmpty() { return size == 0; }
    public boolean contains(int x) {
        for (int i : this.toArray()) {
            if (i == x) return true;
        }
        return false;
    }
    public int size() { return size; }
    public void enqueue(int i) throws QueueFullException {
        ++size;
        if (size > maxSize) {
            throw new QueueFullException();
        }
        q[(start + size) % maxSize] = i;
    }
    public int dequeue() throws QueueEmptyException {
        int ret;
        if (size == 0) {
            throw new QueueEmptyException();
        } else {
            ret = q[start];
            start = (start + 1) % maxSize;
            --size;
        }
        return ret;
    }
    public void add(int i) throws QueueFullException {
        enqueue(i);
    }
    public void clear() {
        start = size = 0;
    }
    public int[] toArray() {
        int[] out = new int[size];
        for (int i = 0; i < size; i++) {
            out[i] = q[(start + i) % maxSize];
        }
        return out;
    }
}


// Question 3.0:
//
// Fill in the 'dequeue' method.
/* See above. */


// Question 3.1:
//
// Explain why the 'clear' method works.
/* 
 * The point of this question is that it might initially seem
 * counterintuitive that we could "clear" our queue without actually modifying
 * any of the values that are in it.  What you need to notice is that all
 * of the computation about what values are in the queue is based on start
 * and size.  First of all, note that isEmpty() is guaranteed to return
 * true immediately after a call to clear().  Then, note that the value
 * returned by dequeue is based on the value of start, and that enqueue
 * uses both start and size to determine where the next "free" location
 * in the queue is, if it exists.  
 *
 * The bottom line is that the array "q" is not simply our queue.  Rather,
 * we use it as a container that will be partly filled with our queue,
 * and start and size fully specify the indexes in q that hold values in
 * our queue.
 *
 * If you are stuck on the intution here, we encourage you to draw out 
 * q for a small example, and step through a few calls to enqueue and
 * dequeue.
 */



// Question 3.2:
//
// The 'enqueue' method has a bug.
// What is it, and how do we fix it?
/*
 * size is incremented before the throw block.
 * This has 2 nasty implications:
 * 1) If the size > maxSize condition is executed, you may exit
 *    the method without having added anything to the queue,
 *    but having nevertheless incremented the "size" of it.
 *    For instance, if you could hold 2 values and you tried
 *    to add a third, "size" would be 3 even though you couldn't
 *    actually add this value.  Then, subsequent operations would either
 *    break or at least report size incorrectly.
 * 2) Likewise, (start + size) % maxSize gives the wrong index if
 *    size has been incremented a priori (again, if this is not clear to you,
 *    write out an example to prove it to yourself).
 * With these two considerations in mind, we could solve all of our
 * prblems by moving the "++size;" line to the very last line of enqueue.
 */



// Question 3.3:
//
// Note the first line of toArray: it uses new
// (analogous to malloc, for C hackers).
// This allocates memory.
// When can this memory be freed?
/* The memory for an object (or simply array, in this case) can be freed
 * when there are no longer any references to that object in scope.
 * 
 * In the case of toArray(),  this is not as simple as "the memory can
 * be freed when the function exists," because toArray() returns the
 * int array that it allocated with new (out).  Thus, when the memory
 * can be freed depends on the function that invoked toArray(), and in
 * particular on what that function does with the return value that it
 * gets from toArray().
 *
 * As an example, in the current implementation of contains, the return
 * value of toArray() is simply passed as the iterable to a for loop
 * (ask us or see
 * http://java.sun.com/j2se/1.5.0/docs/guide/language/foreach.html
 * for help understanding Java's new "for each" arrays).
 * Thus, the array becomes out of scope as soon as the for loop stops
 * executing, which either occurs because the function returns true,
 * or because the loop steps through all values.  After either of those
 * events occurs, the memory can be freed.
 */

// What code is responsible for freeing it?
/*
 * Those of you that are C hackers are familiar with (and good at,
 * we trust!) the fact that in C, you must always remember to free
 * memory that you have allocated.  Java is different, because it has
 * what is called "garbage collection."  With garbage collection, there is
 * a daemon running inside the Java Virtual Machine that keeps track of
 * whether or not any references to an object remain in scope, and when
 * this ceases to be the case the memory for that object will be freed.
 * That is to say, the garbage collector picks up "trash" (memory that has
 * been allocated but can no longer be used).
 *
 * Why does no similar feature exist built in to C?  The basic answer is that
 * C does not try to be as high level of a language as Java.  In particular,
 * recall that in C you are allowed to do arbitrary pointer arithmetic (ie,
 * you are allowed to interpret pointers directly as addresses in memory,
 * and you can do computation on these addresses to access the
 * contents of other addresses in memory).  Is the intution here starting
 * to become clear?  In Java, the JVM is basically able to do bookkeeping
 * about what "can" and "can't" "ever" be accessed again.  In C, though,
 * you could always access any address in memory with the right pointer
 * arithmetic, so how could the compiler or runtime environment ever know
 * what you might try to access?  The short answer is that without imposing
 * some sort of constraints, it really can't.
 *
 * There are however, attempts to add garbage collection to C "after the
 * fact."  If you're interested in this, Google around or check out
 * http://www.hpl.hp.com/personal/Hans_Boehm/gc/
 *
 * Note that we wouldn't test you on any of the details here, particularly
 * concepts that relate to C.  However, you should understand Java's garbage
 * collection from A Programmer's Perspective (tm), and be able to reason
 * about when the garbage collector will become able to free the memory
 * associated with a given object.
 */




// Question 3.4:
//
// The 'contains' method is inefficient. Why?
// How might we rewrite it to be more efficient?
// Would this duplicate code with another method?
// Suggest a way we could factor this code out if we were writing in ML
// (it's not so easy to do in Java...).

/* contains invokes toArray().
 * toArray() creates a new Array that has all of the values in the
 * queue, and then contains steps through these values individually.
 * In particular, toArray() itself steps through all of the values in
 * the queue individually in order to create the array of these values.
 * 
 * So, if an the element that we're looking for is at the end of our queue
 * or does not exist, by the time we're done with contains we will have
 * stepped through the entire queue twice.
 *
 * What if we re-implemented the logic in toArray() inside contains, instead
 * of invoking toArray()?
 * That is, what if we wrote our own code in contains to step through the
 * values of the q array that are actually values in our queue?  Well,
 * then we would step through the array at most once, and we might actually
 * terminate more quickly if the value that we're looking for is near
 * the beginning of our queue.
 * 
 * In asymptotic terms, either implementation is still O(n) in the worst
 * case, but significantly we have now allowed for the best case to be O(1).
 * That is, if the value that we're looking for is at the beginning of 
 * our queue, this hypothetical version of contains will simply return
 * true immediately.
 *
 * You should, however, feel uneasy about anything that relates to
 * duplicating code, or even duplicating the logic implemented by a piece
 * of code.  How could we factor out this logic?  In Java, it is particularly
 * difficult, and as a result it may be ambiguous which of th two choices
 * (invoke toArray or partially re-implement it) is a better design decision.
 *
 * In ML land, we can take advantage of functional programming.
 * That is, we could define a way to do a fold over our queue, thereby
 * factoring out the basic process of stepping through each value in the
 * queue.  And then what?
 * -> For toArray, our accumulator function would take the current element
 *    of the queue, and a pair of (index in the array * array built up so
 *    far).  Then, the "base" value would 0 paired with be a blank array
 *    initialized with size "size".  At each step, the function would 
 *    simply put the current value at the current index, and increment
 *    this current index.
 * -> For contains, our accumlator function would take the current element
 *    of the queue and a boolean, and simply compute the or of
 *    (x == current) and this boolean.^
 * 
 * ^: What's the problem here?  It ruins the point made above about the
 *    best case run time being O(1) rather than O(n).  How could we fix
 *    this?  Well, you'd have to have some way for the fold to decide
 *    to stop if and when it finds the value that it's looking for, but
 *    a normal fold cannot do this.  We'd have to implement what's called
 *    a "lazy fold" where, rather than taking a current value and an
 *    an accumulated value, we take a current value and a function
 *    that takes a unit argument and returns the accumulated value.
 *    That way, if our accumulator function ever encounters true,
 *    it simply evaluates to true rather than evaluating the next
 *    accumulated value.
 *    Don't worry too much about this if it's not making sense, since we
 *    didn't cover lazy folds in the functional part of the course and
 *    wouldn't test you on them.  You should, however, be able to convince
 *    yourself that the code-duplicating way of writing contains
 *    that we described above _does_ get best case runtime down to O(1),
 *    and a standard fold would not allow for this.
 *   
 *    For extra awesome points, can a lazy fold be either a foldr or a foldl,
 *    or does it only make sense for one of those two?
 */




// Question 3.5:
//
// Why is it somewhat unappealing to have a 'IntCollection' interface?
/*
 * It seems to imply that we'd have to rewrite our code for any type of
 * collection that we wanted, such as StringCollection, BooleanCollection,
 * and whatnot.
 */

// What is ML's solution to this problem?
/* ML allows for parametric polymorphism.  That is, do you remember
 * how we build things like abstract data types by parametrizing algebraic
 * types?
 * Look back at Moogle and the solution to question 3 on the midterm
 * if that doesn't sound familiar.
 * 
 * How would you write a parametrized collection type in ML?
 */

// Java has a similar solution ('generics'), although the implementation is
// somewhat different and the type system doesn't allow quite as much
// flexibility.

// In fact, the actual Java standard library has an interface called Collection
// implemented in terms of generics:
//
// public interface Collection<E> { 
//   public boolean add(E e) { ... }
//   ...
// }
//
// (read "collection of E")
//
// (ArrayList, LinkedList, TreeSet and other classes implement this interface,
// and their use is highly encouraged (we've already seen how painful it can be
// to reinvent the wheel, particularly when the wheel is a balanced tree!).)
//
// We might want Collection to have a method:
//
//   public E[] toArray() { ... }
//
// But there's no clear way to write this
// (and indeed the Java developers didn't, opting instead to have the method
// take the destination array as an argument).
// What goes wrong?
/*
 * This will not be on the exam.
 * The short answer to this question is that Java generics were added
 * after the original version of Java, and as a result essentially only
 * effect things at compile-time, rather than at run-time.  This carries
 * important implications, but none that you need to be worrying about
 * right now.  If you're interested, you can read about the issues of
 * generics and covariance at
 * http://www.ibm.com/developerworks/java/library/j-jtp01255.html
 */
