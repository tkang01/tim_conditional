(* This is a set of exercises meant to resemble the sorts of things
   we'll ask on final.  *)

(* For each of the following Ocaml expressions, explain what value we
get when we evaluate the expression, or why the expression makes no
sense.  Be careful – some of these expressions do not type-check. *)

let f = fun x -> x * x in f 4

let f = fun x -> x+1 in let g = 41 in f g

let f = fun x -> fun y -> x + y + 2 in f

let x = fun f x -> x f in x 41 (fun f -> f+1);;

let c f g x = f g x in c ((+) 1) (( * ) 2) 20;;

List.filter ((=) 3) [1;2;3;4;5];;

let f x = 
  match x with
  | None -> None
  | Some x -> if (x mod 2 == 0) then Some (x+1) else Some (x-1)
in
f (Some 41) ;;


(* For each of the following functions, state whether it could be used
   as an argument to reduce, map, filter or none of these. *)

fun (n: int) -> 2 * n
(* map *)

fun (n: int) -> (2 * n = 4)
(* map, filter *)

fun (x: string) (y: string) -> (String.length x) + (String.length y)
(* map (could map strings to functions of 1 argument that add the
 * argument's length to those strings' respective lengths *)

( * )
(* reduce, map (could map integers to functions of 1 argument that
 * multiply their argument by those respective integers *)

( * ) 4
(* map *)

(* Recall the definition of reduce as defined in class (similar to Ocaml’s List.fold_right): *)

let rec reduce f u xs = 
  match xs with 
  | [] -> u
  | h::t -> f h (reduce f u t)

(* For each of the following, rewrite the function using reduce.  You should not need to define or use auxiliary functions (i.e., you should be able to do this with just anonymous functions.) *)

let rec g xs = 
     match xs with 
     | [] -> ""
     | (n,s)::t -> n ^ ": " ^ string_of_int s ^ "\n" ^ (g t);;

let g = reduce (fun (n,s) r -> n ^ ": " ^ string_of_int s ^ "\n" ^ r) "";;

let rec g xs = 
    match xs with 
    | [] -> None
    | h::t -> (match g t with 
               | None -> Some h
               | Some x -> if h > x then Some (h - 1) else Some x);;

let g = reduce (fun x r -> match r with
		  | None -> Some x
		  | Some y -> if x > y then Some (x-1) else Some y) None;;

let rec g xs = 
    match xs with
    | [] -> (fun yarr -> yarr)
    | h::t -> fun x -> (g t) (h x);;

let g = reduce (fun f r -> fun x -> r (f x)) (fun x -> x);;


(* Recall trinary trees from the midterm:

   An ‘a trinary tree is either empty or a node.  If it’s a node, it
   can either be (1) a binary node with one ‘a value x and two sub-trees
   left and right such that all of the ‘a values in left are less than x,
   and all of the ‘a values in right are greater than x, or else (2) a
   trinary node with two values x and y and three sub-trees left, middle,
   and right with the properties that x < y, all of the ‘a values in left
   are less than x, all of the ‘a values in right are greater than y, and
   all of the ‘a values in middle are between x and y.

   Here is a ML type definition for trinary trees:
*)

type 'a tree = Empty | BNode of ('a tree * 'a * 'a tree) 
			| TNode of ('a tree * 'a * 'a tree * 'a * 'a tree);;


(* Write an instantiation of the iterator pattern for trinary trees. *)

let rec fold (f: 'a -> 'b -> 'b) (u: 'b) (t: 'a tree) = 
  match t with
    | Empty -> u
    | BNode (l, v, r) -> fold f (f v (fold f u l)) r
    | TNode (l, v, m, w, r) -> 
	fold f (f w (fold f (f v (fold f u l)) m)) r ;;

(* Use your iterator implementation to write a function that would add
   all the values in a trinary tree of ints.  *)

let add_all (t: int tree) : int =
  fold (+) 0 t
;;


(* Now write an instantiation of the visitor pattern for trinary
   trees. *)

type ('a, 'b) visitor = {
  empty_visit : 'b;
  bnode_visit : 'b -> 'a -> 'b -> 'b;
  tnode_visit : 'b -> 'a -> 'b -> 'a -> 'b -> 'b;
} ;;

let rec visit (v : ('a, 'b) visitor) (t : 'a tree) : 'b =
  match t with
    | Empty -> v.empty_visit
    | BNode (l, v1, r) -> 
	let (lv, rv) = (visit v l, visit v r) in
	  v.bnode_visit lv v1 rv
    | TNode (l, v1, m, v2, r) ->
	let (lv, mv, rv) = (visit v l, visit v m, visit v r) in
	  v.tnode_visit lv v1 mv v2 rv
;;


(* Use your visitor implementation to implement a function that will
   count the number of BNodes and the number of TNodes in the tree,
   returning a tuple of the two counts *)

let count_nodes_by_type (t: 'a tree) : (int * int) =
  let count_visitor : ('a, int*int) visitor = {
    empty_visit = (0,0);
    bnode_visit = (fun (lb, lt) v1 (rb, rt) -> (lb+rb+1, lt+rt));
    tnode_visit = (fun (lb, lt) v1 (mb, mt) v2 (rb, rt) ->
	(lb+mb+rb, lt+mt+rt+1));
  } in
    visit count_visitor t
;;

(* Now use the visitor implementation to implement a function that
   will print out an 'a tree to the screen.  Of course, you'll need to
   allow the user to pass in a way to print out the actual node values
   (of type 'a).
*)

let print_tree (t: 'a tree) (to_string: 'a -> string) : unit = 
  let print_visitor : ('a, string) visitor = {
    empty_visit = "Empty";
    bnode_visit = (fun lv v1 rv ->
		     "(" ^ lv ^ "," ^ to_string v1 ^ "," ^ rv ^ ")");
    tnode_visit = (fun lv v1 mv v2 rv ->
		     "(" ^ lv ^ "," ^ to_string v1 ^ "," ^ mv ^ "," ^
		       to_string v2 ^ "," ^ rv ^ ")");
  } in
    print_string (visit print_visitor t)
;;


(* Here is the definition of streams from lecture, along with some
   useful functions: *)

type 'a str = Cons of 'a * 'a stream
and 'a stream = unit -> ('a str);;

let rec ones : int stream = fun () -> (Cons (1,ones));;

let head (s:'a stream) : 'a = 
  match s() with 
    | Cons (h,_) -> h
;;

let tail (s:'a stream) : 'a stream = 
  match s() with 
    | Cons (_,t) -> t
;;

let rec take(n:int) (s:'a stream) : 'a = 
  if n <= 0 then head s else take (n-1) (tail s)
;;

let rec first(n:int) (s:'a stream) : 'a list = 
  if n <= 0 then [] else (head s)::(first (n-1) (tail s))
;;

let rec map(f:'a -> 'b) (s:'a stream) : 'b stream = 
  fun () -> (Cons (f (head s), map f (tail s)))
;;

let inc x = 1 + x ;;

let twos = map inc ones ;;

let rec nats = fun () -> (Cons (0, map inc nats)) ;;

let rec zip (f:'a -> 'b -> 'c)  
    (s1:'a stream) (s2:'b stream) : 'c stream = 
  fun () -> (Cons (f (head s1) (head s2), 
                   zip f (tail s1) (tail s2))) ;;

let threes = zip (+) ones twos ;;

(* Write a function to find the maximum of a float stream, assuming
   that the stream will have a single maximum--the values will go up and
   then start going down. *)

let maximum (s: float stream) : float = 
  let rec max_helper (str: float stream) (prev: float) : float = 
    let curr = head str in
      if curr < prev then prev else max_helper (tail str) curr
  in
    max_helper (tail s) (head s)
;;

(* Write a function to compute successive approximation of e^x using the
formula:

  e^x = sum x^i / (i!)
*)

let exp_stream (x: float) : (float stream) = 
  let e_seq = 
    let rec fact n = 
      if n <= 0 then 1 else n * (fact (n-1)) in
    let numer = map (fun i -> x ** (float i)) nats in
    let denom = map (fun i -> float (fact i)) nats in
      zip (/.) numer denom in
  let rec sum_stream_to_n str n =
    if n = 0 then 0. else (head str) +. (sum_stream_to_n (tail str) (n-1)) in
    map (sum_stream_to_n e_seq) nats
;;
(* this is extremely inefficient; we could avoid recomputing values using  
 * memoization *)

(*********************************************************************)


(* Challenge: write reverse using just reduce and lambdas *)

let reverse (xs: 'a list) : 'a list = 
  let id = fun x -> x in     (* you'll find this useful *) 
  let f = fun x r -> fun y -> r (x :: y)
  in
    (* Use a single call to reduce here *)
    (reduce f id xs) [];;
