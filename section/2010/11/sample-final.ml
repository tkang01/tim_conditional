(* This is a set of exercises meant to resemble the sorts of things
   we'll ask on final.  *)

(* For each of the following Ocaml expressions, explain what value we
get when we evaluate the expression, or why the expression makes no
sense.  Be careful – some of these expressions do not type-check. *)

let f = fun x -> x * x in f 4

let f = fun x -> x+1 in let g = 41 in f g

let f = fun x -> fun y -> x + y + 2 in f

let x = fun f x -> x f in x 41 (fun f -> f+1)

let c f g x = f g x in c ((+) 1) (( * ) 2) 20

List.filter ((=) 3) [1;2;3;4;5]

let f x = 
  match x with
  | None -> None
  | Some x -> if (x mod 2 == 0) then (x+1) else Some (x-1)
in
f (Some 41) 


(* For each of the following functions, state whether it could be used
   as an argument to reduce, map, filter or none of these. *)

fun (n: int) -> 2 * n

fun (n: int) -> (2 * n = 4)

fun (x: string) (y: string) -> (String.length x) + (String.length y)

( * )

( * ) 4



(* Recall the definition of reduce as defined in class (similar to Ocaml’s List.fold_right): *)

let rec reduce f u xs = 
  match xs with 
  | [] -> u
  | h::t -> f h (reduce f u t)

(* For each of the following, rewrite the function using reduce.  You should not need to define or use auxiliary functions (i.e., you should be able to do this with just anonymous functions.) *)

let rec g xs = 
     match xs with 
     | [] -> ""
     | (n,s)::t -> n ^ ": " ^ string_of_int s ^ "\n" ^ (g t)

let rec g xs = 
    match xs with 
    | [] -> None
    | h::t -> (match g t with 
               | None -> Some h
               | Some x -> if h > x then Some (h - 1) else Some x)

let rec g xs = 
    match xs with
    | [] -> (fun yarr -> yarr)
    | h::t -> fun x -> (g t) (h x)


(* Recall trinary trees from the midterm:

   An ‘a trinary tree is either empty or a node.  If it’s a node, it
   can either be (1) a binary node with one ‘a value x and two sub-trees
   left and right such that all of the ‘a values in left are less than x,
   and all of the ‘a values in right are greater than x, or else (2) a
   trinary node with two values x and y and three sub-trees left, middle,
   and right with the properties that x < y, all of the ‘a values in left
   are less than x, all of the ‘a values in right are greater than y, and
   all of the ‘a values in middle are between x and y.

   Here is a ML type definition for trinary trees:
*)

type 'a tree = Empty | BNode of ('a tree * 'a * 'a tree) 
			| TNode of ('a tree * 'a * 'a tree * 'a * 'a tree);;


(* Write an instantiation of the iterator pattern for trinary trees. *)






(* Use your iterator implementation to write a function that would add
   all the values in a trinary tree of ints.  *)

let add_all (t: int tree) : int =
  

;;


(* Now write an instantiation of the visitor pattern for trinary
   trees. *)













(* Use your visitor implementation to implement a function that will
   count the number of BNodes and the number of TNodes in the tree,
   returning a tuple of the two counts *)

let count_nodes_by_type (t: 'a tree) : (int * int) =



;;

(* Now use the visitor implementation to implement a function that
   will print out an 'a tree to the screen.  Of course, you'll need to
   allow the user to pass in a way to print out the actual node values
   (of type 'a).
*)

let print_tree


;;




(* Here is the definition of streams from lecture, along with some
   useful functions: *)

type 'a str = Cons of 'a * 'a stream
and 'a stream = unit -> ('a str);;


let rec ones : int stream = fun () -> (Cons (1,ones));;

let head (s:'a stream) : 'a = 
(*  let _ = Printf.printf "head\n" in *)
  match s() with 
    | Cons (h,_) -> h
;;

let tail (s:'a stream) : 'a stream = 
(*  let _ = Printf.printf "tail\n" in *)
  match s() with 
    | Cons (_,t) -> t
;;

let rec take(n:int) (s:'a stream) : 'a = 
  if n <= 0 then head s else take (n-1) (tail s)
;;

let rec first(n:int) (s:'a stream) : 'a list = 
  if n <= 0 then [] else (head s)::(first (n-1) (tail s))
;;

let rec map(f:'a -> 'b) (s:'a stream) : 'b stream = 
  fun () -> (Cons (f (head s), map f (tail s)))
;;

let inc x = 
(*  let _ = Printf.printf "inc\n" in *)
    1 + x ;;

let twos = map inc ones ;;

let rec nats = fun () -> (Cons (0, map inc nats)) ;;

let rec nats = fun () -> (Cons (0, fun () -> 
                                  (Cons (inc (head nats),   
                                         fun () -> (Cons (inc (head (tail nats)), 
                                                          map inc (tail (tail nats))))

                                           inc (head (tail (tail (tail (tail ... nats)))))

map inc (tail nats))))) ;;



let rec zip (f:'a -> 'b -> 'c)  
    (s1:'a stream) (s2:'b stream) : 'c stream = 
  fun () -> (Cons (f (head s1) (head s2), 
                   zip f (tail s1) (tail s2))) ;;

let threes = zip (+) ones twos ;;

(* Write a function to find the maximum of a float stream, assuming
   that the stream will have a single maximum--the values will go up and
   then start going down. *)

let maximum (s: float stream) : float = 



;;

(* Write a function to compute successive approximation of e^x using the
formula:

  e^x = sum x^i / (i!)
*)

let exp_stream (x: float) : (float stream) = 



;;

let rec factorial n = if n <= 1. then 1. else n *. (factorial (n-.1.));;

(* Given stream 1, 1, 1, 1, ..., will return 1, -1, 1, -1, ... *)
let rec alternate (s: float stream) : float stream = 
  fun () -> Cons (head s, map (fun x -> -1. *. x) (alternate (tail s)))
;;

let rec alternate' (s: float stream) : float stream = 
  let t = tail s in
    fun () -> Cons (head s, fun () -> Cons (-1. *. (head t), alternate' (tail t)));;

(* Given 1, 1, 1, 1, ..., will return 1, 2, 3, 4, 5, ... *)
let sum (s : float stream) : float stream = 
  map (fun n -> List.fold_right (+.) (first n s) 0. ) (tail nats)
;;

(* sin(x) = ? Sum (x^(2n+1)/(2n+1)! * (-1)^n *))
let sin_stream (x:float) : float stream =
  let float_nats = map float_of_int nats in
  let top = map (fun n -> x ** (2. *. n +. 1.)) float_nats in
  let bottom = map (fun n -> factorial (2.*.n +. 1.)) float_nats in
    sum (alternate' (zip (/.) top bottom ))

;;

let all_trees : 'a tree stream = ???

(*********************************************************************)


(* Challenge: write reverse using just reduce and lambdas *)

let reverse (xs: 'a list) : 'a list = 
  let id = fun x -> x in     (* you'll find this useful *) 
  let f = 



  in
    (* Use a single call to reduce here *)


type 'a tree = Empty | Branch of 'a * 'a tree * 'a tree

let add x y = x + y 

let add = fun x -> (fun y -> x + y)

add 3

(fun y -> 3 + y)

module type SET =
 sig
   type t
   val default_element : t
   val member: t -> bool
   ... 
 end 

module MyFavoriteSet : SET =
struct
  type t = (int * string)
  let default_element = (3, "hi!")
  let member x = true
end

... SET_ARGS  =
  sig
    type elt
    val compare : elt -> elt -> int
  end

module MySetFunctor(M : SET_ARGS) : SET =
  struct
    type t = M.elt
    let member x = fold (fun a b -> M.compare a x or b) ...
  end
