/* This is a set of exercises meant to resemble the sorts of things
   we'll ask on final.  */

/*

  Define the following terms:
  class
  object
  overriding
  inheritance
  subtyping
  interfaces
  abstract base class
  pattern
  generics/polymorphism
  garbage collection


  - What's the difference between inheritance and subtyping?
 




   
  - What is the difference between an interface and an abstract base
  class?  Why might you want to use one or the other?  Does it make
  sense to use both at the same time?
     




  *** These next two could be (and have been) the subject of books,
  so we wouldn't ask them quite like this on a real final, but we do
  want you to understand some of the pros and cons
  ****
  - Why is OO a great idea?

   
     

 
  - Why is OO a terrible idea?  
     



 
*/


/* What does the following java code print/do */

// The answer is not always 42, but don't panic.
// Work through the following code to find out how many different colors can
// be displayed by a Commodore 64, the best-selling single personal computer
// model of all time (30 million sold!).
class Func {
    private int a;
    protected int b;

    public Func(int m, int n) { a = m; b = n; }

    public int f(int a, int b) { return a - b; }
    public int g(int x, int y) { return a + b; }
    public void h() {
        a = f(a,b) + g(3,4);
        b = g(a,b) + f(3,4);
    }
}

class Grunc extends Func {
    private int a;

    public Grunc(int a, int b) { super(a,b); this.a = b; }

    public int f(int f, int g) { return b - a; }
    public int g(int a, int b) { return (a = super.g(a,b)); }
}

public class Main {
    public static void main(String[] args) {
        Func f = new Func(3,4);
        f.h();
        Func g = new Grunc(f.f(4,3),f.g(2,1));
        g.h();
        System.out.println("It can display a whopping " + g.f(2,3) + " colors!"); // 16
    }
}




/* You want to SOMETHING.  Declare the interface(s) you want to use.  Make
 * sure to name/document your methods so that it is obvious how it
 * will actually work.  */






/* Here's some concurrent code: */
class Foo {
    public int x = 0;
    public void m() {
        x += 1;
    }
}

class FooThread extends Thread {
    Foo f;
    public FooThread(Foo f) {
        this.f = f;
    }

    public void run() {
        f.m();
    }
}

class FooProblem {
    public static void main() throws InterruptedException {
        Foo f = new Foo();
        (new FooThread(f)).start();
        (new FooThread(f)).start();
        /* SECRET LOCATION */
    }
}


/* Explain how f.x could end up being 1.  */




/* Explain how f.x could end up being 2.  */



/* If we put "System.out.println(f.x)" at the SECRET LOCATION in the
 * code, what are possible values that could be printed */


/* Recurrence relations: consider the following functions, from the Checkers
 * assignment. */

private ArrayList<Move> possibleJumps(Piece p, Move soFar, Location l) {
    boolean fresh = (soFar == null);
    if(soFar == null) {
        soFar = new Move(l);
    }
    	
    ArrayList<Move> possible = new ArrayList<Move>();
    	
    Taken t = soFar.getTaken();
    Move afterStep;
    	
    Location proposedVictimLoc;
    ArrayList<Location> jumpsAvail = filterAvailable(jumpsWithinRange(p, l), t);
    for (Location nextStep : jumpsAvail) {
        proposedVictimLoc = getJumpedLocation(p, t, l, nextStep);
        if(proposedVictimLoc != null) {
            // If and only if we're actually going to jump something,
            // we clone our current Move before modifying it.
            afterStep = (Move)soFar.clone();
            afterStep.addLocation(nextStep);
            afterStep.recordTaken(proposedVictimLoc);
            possible.addAll(possibleJumps(p, afterStep, nextStep));
        }
    }
    if(!fresh && possible.isEmpty()) {
        if(soFar != null) {
            ArrayList<Move> justSoFar = new ArrayList<Move>();
            justSoFar.add(soFar);
            return justSoFar;
        }
        else {
            return new ArrayList<Move>();
        }
    }
    else {
        return possible;
    }
}

private ArrayList<Location> filterAvailable(ArrayList<Location> list,
                                            Taken t) {
    ArrayList<Location> avail = new ArrayList<Location>();
    for(Location poss : list) {
        if(isAvailable(t, poss)) {
            avail.add(poss);
        }
    }
    return avail;
}


/* Write a set of recurrences that describes the running time of the 
 * possibleJumps function. You may assume that any function not part of
 * the standard Java libraries for which you are not given the definition
 * runs in constant time. */











/* Using Big-O notation, describe the running time for the possibleJumps
 * function. In particular, use the _simplest_ function possible to
 * characterize the worst-case running time. */

class MySortedList<T : Comparable> {
    T x;
    x.compare()
}

