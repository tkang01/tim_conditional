(* CS51 Section 5 *)
(* Week of 3/8/10 *)

(* The purpose of this section is to help you understand state in ML,
 * as well as to help you prepare for the upcoming midterm by providing
 * more practice.
 *)

(**********************  Part 0: Recap  **** **********************)
(* Hand back, questions, comments, concerns? *)

(******************  Part 1: Mutation and State  ******************)
(* So far in this course, we have worked almost exclusively with the
 * subset of ML that is functional (with the exceptions of exceptions
 * (sorry) and printing).  We have traded iteration counters for
 * recursion, among other things.
 *
 * Recall from lecture last Tuesday that, while there are plenty of
 * reasons to sing the praises of functional programming, eventually
 * we need side-effects.  After all, we write programs in order to
 * change the world, and any change to the world is necessarily a side-effect
 * of functional computation.
 *
 * The basic way of allowing for mutable state in ML is to use references.
 * To recap from lecture:
 *
 * New type: t ref
 * ->Think of it as a pointer to a box that holds a t value.
 * -> The pointer can be shared.
 * -> The contents of the box can be read or written.
 *
 * To create a fresh box: ref 42
 * -> allocates a new box, initializes its contents to 42, and returns a
 *    pointer to that box.
 * To read the contents: !r
 * -> f r points to a box containing 42, then returns 42.
 * -> similar to *r in C/C++
 *
 * To write the contents: r := 42
 * -> updates the box that r points to so that it contains 42.
 * -> similar to *r = 42 in C/C++
 *
 * Note that:
 * Now that we are updating state, sometimes we will want to have a sequence
 * of expressions, but we will only care about the value of the last
 * expression.  We accomplish this with semicolons.
 * For example, (a := 5 + b; !a) would update the "box" that a points to
 * to have a value of 5 more than the value of b, and then evaluate
 * to this same value.
 *
 *)

(* 1.1: Vocabulary Check
 * 
 * What is the name for something that becomes a problem in the presense of 
 * references and mutation, defined by Wikipedia as "situation in which
 * a data location in memory can be accessed through different symbolic
 * names in the program"?  Answer: aliasing
 *)

(* 1.2: 42 Exercises *)
(* Replace ??? so that the expression evaluates to 42. *)
(* 1.2.1 *)
let f = (fun () -> [ref (fun x -> 2 * x)]) in             
  match f () with          
    | [] -> 12
    | a::b -> !a 21
;;

(* 1.3.1 *)
let f = 
  (let called = ref false in 
     (fun () -> if !called then 42 else (called := true; 0))) in
  if f () = 42 then 21 else f ()
;;

(* 1.3: Call Counter
 * Write call_counter : ('a -> 'b) -> (('a->'b)*int ref)
 * The second component of the returned pair should
 * contain the number of times the first component has
 * been called. *)
let call_counter (f:'a -> 'b) : (('a->'b)*int ref) =
  let counter = ref 0 in
    ((fun x -> (counter := !counter + 1; f x)), counter)
;;

(* Using call_counter: *)
let (square, square_cnt) = call_counter (fun x -> x*x);;
let (cube, cube_cnt) = call_counter (fun x -> x*x*x);;

!square_cnt;;  (* evals to 0 *)
cube 3;;
!cube_cnt;;     (* evals to 1 *)
cube (square 2);;  
!square_cnt;;   (* evals to ?? *)
!cube_cnt;;     (* evals to ?? *)



(*** Part 2: Midterm Review ***)

(* 2.1 Datatype Design *)
(* Design a datatype to describe bibliography entries for publications.  Some
 * publications are journal articles, others are books, and others are
 * conference papers.  Journals have a name, number and issue; books have an 
 * ISBN number; All of these entries should have a title and author. *)
(** Answer: **)


type journal =  {name : string ; number: int ; issue : int} 
type book = {isbn : int}

type publication = Journal of journal
                   | Book of book | Paper
type entry = {title: string ; author: string; pub: publication}















(* 2.2: Higher Order Functions *)
(* 2.2.1: Using map, write a function that takes a list of pairs of integers,
 * and produces a list of the sums of the pairs.
 * e.g., list_add [(1,3); (4,2); (3,0)] = [4; 6; 3] *)

let list_add xs = map (fun (x,y) -> x+y) xs



(* 2.2.2: Try to write list_add directly using reduce (fold_right). *)

let list_add xs = reduce (fun (x,y) r -> (x+y)::r) [] xs





(* 2.2.3: Using map, write a function that takes a list of pairs of integers,
 * and produces their quotient if it exists.
 * e.g., list_div [(1,3); (4,2); (3,0)] = [Some 0; Some 2; None] *)


let list_div xs = map (fun (x,y) -> if y = 0 then None else Some x/y) xs


(* 2.2.4:  Try to write list_div directly using reduce (fold_right). *)


let list_div xs = 
  reduce (fun (x,y) r -> (if y = 0 then None else Some x/y)::r) [] xs








(* 2.3: The Substitution Model  *)
(* BY HAND, evaluate the following expresssions.
 * OR, if the expressions do not parse/type check,
 * explain why they do not.  *)

(* 2.3.1 *)
let rec mystery x = if x < 1 then 0 else 1 + (mystery (x - 1))
  in mystery 5;;

(** Answer: **)
5;;


(* 2.3.2 *)
let sum x = List.fold_right (+) x 0 in sum [1.5; 5.3; 6.2];;

(** Answer: Floats in list, but + expects ints **)


(* 2.3.3 *)
let mystery' x =
  List.fold_right
    (fun x y -> let ((a, b), (c, d)) = y in
       ((if x mod 2 == 0 then (x::a, b) else (a, x::b)),
	(if x > 0 then (x::c, d) else (c, x::d))))
    x (([], []), ([], [])) in
  mystery' [-3; -2; -1; 0; 1; 2; 3];;

(** Answer: **)
(([-2; 0; 2], [-3; -1; 1; 3]), ([1; 2; 3], [-3; -2; -1; 0])) ;;


	   

(* 2.3.4 *)
let x = 3 in
let x = 2 * x in
let (x, y) = (x + 1, x + 2) in
  x * y ;;


(** Answer: **)
56;;




(* 2.3.5 *)
let mystery'' x =
  List.fold_right
    (fun x y -> (x (match y with [] -> 0 | hd::tl -> hd))::y)
    x [] in
  mystery'' [( * ) 3; (/) 8; (+) 4]
;;

(** Answer: **)
[6; 2; 4];;

(* 2.3.6 *)
(* What about if we used List.fold_left? *)
let mystery'' x =
  List.fold_left
    (fun y x -> (x (match y with [] -> 0 | hd::tl -> hd))::y)
    [] x in
  mystery'' [( * ) 3; (/) 8; (+) 4]
;;


(* You may notice that we switched the order of x "x y" to be "y x"
 * in the anonymous function, and we switched the order of the last
 * two arguments to be "[] x" instead of "x []".  Don't worry too
 * much about this -- it's just an artifact of the way List.fold_left
 * is implemented in Ocaml.  We won't be testing you on such
 * technicalities; if we require you to make use of a higher order
 * functions, we will provide definitions of them for you. *)

(** Answer: Exception: Division by 0
    ** Because we multiply 0 by 3, and then try to divide 8 by that **)


(* 2.3.7 *)
let foo bar baz = baz bar bar in foo 4 (+);;

(** Answer: 8 **)


(* 2.3.8 *)
let foo bar baz = bar (fun x -> x * 3) baz in
  foo (fun x y -> x y) 4;;

(** Answer: 12 **)


(* 2.3.9 *)
type 'a silt = Empty | Member of 'a * 'a silt ;;

let rec mystery_9 f u gravel =
  (match gravel with
     | Empty -> u
     | Member(a, b) -> f a (mystery_9 f u b))
in mystery_9 (+) 0 (Member(5, Member(6, Member(7, Empty))));;

(** Answer: 18 **)
(** "silt" = "list", get it? **)


(* 2.3.10 *)
let mystery_10 x =
  let rec mystery_10' x y =
    if x == 0 then [x]
    else if y then x::(mystery_10' x false)
    else (- x)::(mystery_10' (x - 1) true)
  in mystery_10' x true
in mystery_10 4;;


(** Dampened oscillation! **)
(** Answer: [4; -4; 3; -3; 2; -2; 1; -1; 0] **)


(* 2.4: Abstract Data Types and Modules *)

(* 2.4.1.  Terminology: Define and describe the relationships between
   "Abstract Data Type", "Implementation", "Signature", "Module", and "Interface" *)

(** 

   Note: all these terms are used with widely varying meanings in Real
   Life.  These are the interpretations we want for CS51.  Don't be
   surprised if you see the same words used in slightly different
   ways.

   Abstract data type: an abstract description of a data structure, in
   terms of the operations that may be performed on it, and the
   invariants that must hold.  Some examples are stacks, queues, priority
   queues, trees, etc. 
   
   Interface: the set of operations that can be invoked by a client of
   some abstraction.  We can refer to the interface of a module, the
   interface of a function, the interface of an operating system, and
   so on.

   Implementation: a concrete set of instructions for how to perform
   the operations in some interface.  Again, this is an abstract term
   that can refer to the implementation of a function, of some
   algorithm, of a module, etc.

   Signature: the ocaml mechanism for defining Abstract Data Types--they
   specify the types and values that must be provided by any
   implementation of that data type.

   Module: the ocaml mechanism for implementing abstract data types.
   Actually specifies the concrete representations of the types, and
   provides implementations of the values (often functions) provided.
**)









(* 2.4.2.  What is a functor? *)


(** A functor is a warm fuzzy thing.  It provides a function from
   modules to modules.  For example, in moogle you wrote a functor
   that, when given a module that implemented a dictionary interface,
   returned a new module that implemented a set interface.  **)







(* 2.4.3.  What's the major problem with this signature for undirected graphs? *)

module type UNDIRECTED_GRAPH =
  sig
    type node
    type graph
    val nodes : graph -> node list
    val edges : graph -> (node * node) list
    val neighbors : graph -> node -> node list
    val add_node : graph -> node -> graph
    val add_edge : graph -> node -> node -> graph
    val delete_node : graph -> node -> graph
    val delete_edge : graph -> node -> node -> graph
  end


(** There's no way to actually create a graph! **)


(* Interfaces, Implementations, and how they're different *)

(* 2.4.4.  Briefly explain in english how you implemented dictionaries using
   red-black trees for moogle.  Make sure you clearly delineate what was part of the
   interface, and what was implementation. *)



(** Dictionaries provide an abstraction mapping keys to values.  In
   moogle, we required that keys were comparable, and used that in our
   implementation.  We stored our key-value pairs in a red-black tree,
   ordering by key.  

   To lookup a key in the dictionary, we performed a lookup for that
   key in the tree, and returned the corresponding value.

   To insert into the dictionary, we inserted the key-value pair into the RB tree at the
   appropriate place.  If there was already an entry for that key, it
   was replaced with the new one.
**)








(* 2.4.5.  Briefly explain in english how you implemented sets using
   dictionaries in moogle.  Make sure you clearly delineate what was part of the
   interface, and what was implementation. *)


(** We implemented a set as a dictionary mapping elements of the set to
   unit.  Inserting X into the set corresponded to inserting the pair
   (X,()) into the dictionary.  Lookup of X looked up X in the
   dictionary and ignored the mapped value.  The other operations were
   implemented accordingly.

   Note: bad answers to this question would include any mention of
   red-black trees.  That's part of a particular implementation of
   dictionaries, and isn't relevant to the implementation of sets
   (except for the performance you get out).  Other bad answers would
   involve, for example, confusing the _set_ insert with the
   _dictionary_ insert.  One is implemented in terms of the other, but
   they are conceptually very different beasts.
**)









(* 2.5 Complexity Analysis of Programs *)

(* 2.5.1 What does "Big-O of f(n)" mean? *)

(** O(f(n)) is the class of functions g(n) such that g(n) is
   asymptotically no larger than f(n).  More formally, it would
   include all g(n) such that the exist c,m such that for n>m, g(n) <=
   c*f(n)
**)





(* 2.5.2 The broad time-complexity classes we're interested in are:
 *
 *   constant time      O(c) = O(1)
 *   logarithmic time   O(log n)
 *   polynomial time    O(n^c)
 *   exponential time   O(c^n)
 *
 * Within each class, which functions are equivalent in terms of Big-0?  Which aren't?
 * (e.g. within constant time, O(1) = O(10) = O(10000) -- all constants are the same.  
 * For polynomials, O(n) = O(2n), but O(n^2) != O(n).) 
 *)


(** For polynomials, any two with the same highest degree are equivalent: 
   O(16n^3 + 100000n^2 +7) = O(n^3)

   For logarithms, any two bases are equivalent, as are the logarithms
   of any polynomials:
   O(log_2 n) = O(log_10 n) = O(log_17 n^74)
   
   For exponential functions, the base matters:
   
   O(2^n) != O(3^n).  In particular, the one with the higher base grows faster:
      2^n = O(3^n), but 3^n != O(2^n)

**)
    









(* 2.5.3 Fill in this table of the solutions to some common recurrences

  T(n) = c                  O(1)

  T(n) = c + T(n-1)         O(n)

  T(n) = kn + T(n-1)       O(n^2)

  T(n) = c + T(n/2)         O(log n)

  T(n) = kn + T(n/2)       O(n)

  T(n) = kn + 2T(n/2)      O(n log n)
*)


(* Answer these questions for the following examples:
 *   1) What is the recurrence relation for this function's running time?
 *   2) What is the asymptotic running time?
 *   3) Does the function utilize tail-calls?
 *   4) If not, could we easily rewrite it so it does? *)

(* 2.5.4 *)

let sum (lst:int list) = foldr (+) 0 lst ;;

(* T(n) = c + T(n-1)
 * O(n)
 * No
 * foldl (+) 0 lst *)

(* 2.5.5 *)

module RBTreeDict(D:DICT_ARG) : (DICT with type key = D.key
                                      with type value = D.value) = 
  struct
     ...  (* imagine a correct implementation of RB trees here *)

    let rec lookup (d:dict) (k:key) : value option = 
      match d with
        | Empty -> None
        | Node n -> (match D.compare k n.key with
                       | Eq -> Some (n.value)
                       | Less -> lookup n.lnode k
                       | _ -> lookup n.rnode k)
    ;;

  end

(** 
 * 1) T(n) = c + T(n_subtree).  Because we're using Red-black trees,
 * we know that the tree is close to balanced, so n_subtree is roughly
 * n/2, so we can write T(n) = c + T(n/2).  (This is a bit handwavy,
 * but it's fine for cs51).
 * 
 * 2) O(log(n)) 
 * 
 * 3) Yes
 **)

(* Pointer: There are some more exercises like this at the bottom of
   timing.ml, linked from the lectures page (complexity lecture) *)
