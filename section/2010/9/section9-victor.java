import java.util.*;

/*
 * CS 51 Section 9
 * Week of 4/11/10
 *
 * - A brief note on generics
 * - Design patterns: general concepts, specific examples, and exercises.
 * - Final project logistical questions?
 */

/***************/
/* 0. Generics */
/***************/

/* Generics in Java are essentially the same as parametric types in ML (the
 * things with the 'a in front of a type declaration). They enable you to
 * specify, for example, the type of objects that your Collection holds in a
 * way that can be checked at compile-time.
 */

/* Example (taken from the Sun language documentation--for link, see below):
 */
//Removes 4-letter words from c. Elements must be strings.
static void expurgate(Collection c) {
   for (Iterator i = c.iterator(); i.hasNext(); )
      if (((String) i.next()).length() == 4)
        i.remove();
}
 /*
 * Here, the fact that elements must be Strings is mentioned only in the
 * comment, and cannot be checked at compile-time. Thus, it is possible that
 * the cast in the if statement might fail and throw an exception. Consider:
 */

// Removes the 4-letter words from c
static void expurgate(Collection<String> c) {
    for (Iterator i = c.iterator(); i.hasNext(); )
        if (((String)i.next()).length() == 4)
        i.remove();
}

/*
 * Now, having parametrized the Collection with the <String> type, the compiler
 * can ensure that c contains only Strings.
 *
 * A more complete explanation can be found at:
 * http://java.sun.com/j2se/1.5.0/docs/guide/language/generics.html.
 */


/**********************/
/* 1. Design patterns */
/**********************/

/*
 * What is a design pattern? According to Christopher Alexander:
 *
 * " A pattern describes a problem which occurs over and over again, and then
 *   describes the core of the solution to that problem, in such a way that you
 *   can use this solution a million times over."
 *
 * Note that Christopher Alexander was an architect, not a computer scientist.
 * The concept of a design pattern is powerful and general, applicable to a
 * variety of fields. It can essentially be thought of as a description of a
 * common problem, a way to go about solving the problem, and the overall
 * impact of applying the pattern, along with a name for the entire package.
 */

/*
 * Elements of a design pattern:
 * - Name: allows us to talk at a high level about the pattern.
 * - Problem: type of situation the pattern is intended to resolve.
 * - Solution: template for resolving the issue. Not a particular example of a
 *             solution, but rather a strategy for creating solutions.
 * - Consequences: analysis of the tradeoffs involved in the solution given
 *                 (e.g., space vs. time, flexibility vs. precision,
 *                  extensibility vs. testing required, etc.).
 */

/*
 * 1.1. The Iterator design pattern
 */

/*
 * Problem: we have an object with multiple elements that we wish to access
 *   sequentially, but without exposing the underlying representation.
 */

/*
 * 1.1a) What are some reasons we might wish not to expose the underlying
 *     representation?
 *
 *
 *
 *
 *
 */

/*
 * Solution: create an interface, Iterable, and make all iterable objects
 *   implement this interface. Then we can write code that operates over an
 *   Iteratable object that will work for any class that implements the
 *   interface.
 */

/* From lecture (named Iterator because Java already has an Iterable): */
interface Iterator {
    /* Returns true if the Iterator has more elements. */
    boolean hasNext();

    /*
     * Returns the next element in the Iterator. Calling this method
     * repeatedly until the hasNext() method returns false will return each
     * element in the underlying collection exactly once.
     * @exception NoSuchElementException Iterator has no more elements.
     */
    Object next();

    /*
     * Removes from the underlying collection the last element returned by the
     * iterator. Optional operation.
     * @exception UnsupportedOperationException if the remove operation is not
     *   supported by this Iterator.
     */
    void remove();
}

/* 1.1b) Where else have we seen the iterator pattern in ML? What were the
 *       differences between that implementation and this one? What are the
 *       tradeoffs involved?
 *
 *
 *
 *
 *
 *
 *
 *
 */

/*
 * See the lecture notes for an implementation of Stack, which is a concrete
 * example of the Iterator pattern, and a discussion of some of the
 * consequences of employing it that way.
 */

/* 1.2 The Strategy design pattern */

/* In this section, we will describe the Strategy pattern in English and ask
 * you to implement it in Java for a particular situation. (Credit for much of
 * the following belongs to Wikipedia; for a more complete exposition, see
 * http://en.wikipedia.org/wiki/Strategy_pattern.)
 */

/*
 * The Strategy pattern aims to allow algorithms to be selected at runtime. The
 * Strategy pattern is useful in situations where it is desirable to be able to
 * switch which algorithm is being used by a particular function at runtime,
 * based on some runtime information such as the type of object passed to the
 * function. It is intended to provide a means to define a family of
 * algorithms, encapsulate each one as an object, and make them
 * interchangeable.
 */

/* 1.2a) How would we enable a function to employ the Strategy pattern in ML?
 *       What are some examples of functions we've used throughout the semester
 *       that make use of this pattern?
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

/*
 * The way that this works in terms of code is that an interface - here named
 * IntArrayStrategy - defines some functions that all usable Strategies must
 * implement. Here, we wish for all Strategies to implement a 'combining'
 * operation (given two ints, return an int), and an no-argument method
 * returning an integer value we might whimsically call the 'base case'
 * of the IntArrayStrategy.
 *
 * Classes implementing this interface are then defined, corresponding to
 * particular Strategies. Here, we would like you to define two classes
 * implementing IntArrayStrategy. The StrategyAdd class should define its
 * execute function to add up intermediate values, with a base case of zero.
 * The StrategyMult class should define its execute function to multiply
 * intermediate values together, with a base case of one.
 *
 * These Strategies are then wrapped within an IntArrayCollapser object,
 * which is instantiated with a particular strategy. The implementation of the
 * IntArrayCollapser class is given for you.
 */

/* 1.2.b)
 * Your goal is to fill in the code skeleton given below in order to make
 *   UseStrategyPattern.main() print out the following:
 * 
 * resultA = 55
 * resultB = 362880
 */

interface IntArrayStrategy {
    // declare the combine and baseCase methods here
    int baseCase();
    int combine(int a, int b);
}

interface IterableStrategy<T, R> {
    // declare the combine and baseCase methods here
    R baseCase();
    R combine(T a, R b);
}

//fold: ('a -> 'b -> 'b) -> 'b -> 'a list -> 'b

class StrategyAdd implements IntArrayStrategy {
    // implement the interface (strategically)
    public int baseCase() {
        return 0;
    }

    public int combine(int a, int b) {
        return a + b;
    }
}

fold (+) 0 xs
fold (*) 1 xs

class StrategyMult implements IntArrayStrategy {
    // implement the interface (strategically)
    public int baseCase() {
        return 1;
    }

    public int combine(int a, int b) {
        return a * b;
    }

}

class IntArrayCollapser {
    private IntArrayStrategy strategy;

    IntArrayCollapser(IntArrayStrategy strategy) {
        this.strategy = strategy;
    }

    public int collapse(ArrayList<Integer> a) {
	    int result = strategy.baseCase();
    	for (Integer i : a) {
	        result = strategy.combine(result, i);
	    }
	    return result;
    }
}

class UseStrategyPattern {
    public static void main() {
        IntArrayCollapser collapser;

        // arr will be an ArrayList of the Integers 1-10
        ArrayList<Integer> arr = new ArrayList<Integer>();
        for (int i = 1; i <= 10; i++) {
            arr.add(new Integer(i));
        }

        // initialize collapser with a new StrategyAdd object
        collapser = new IntArrayCollapser(new StrategyAdd());
        int resultA = collapser.collapse(arr);


        // declare an int resultA to be the return value of invoking
        //  collapser's collapse method on arr



        System.out.println("resultA = " + resultA);

        // initialize context with a new StrategyMult object



        // declare an int resultB to be the return value of invoking
        //  collapser's collapse method on arr



        System.out.println("resultB = " + resultB);
    }
}

/* 1.2c) What functionality, in a clunky and verbose manner, have we
 *       laboriously duplicated from ML? Which pieces correspond to which?
 *
 *
 *
 */

/* 1.2d) How (and to what extent) could we generalize this functionality
 *       (in Java)?
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

/* 1.2e) Let's imagine we wanted to implement a StrategyDiv class that would
 *       do division instead of multiplication. What might be a problem with
 *       this? Name two solutions.
 *
 *
 *
 *
 *
 *
 */

/* 1.2f) How else could we use the Strategy pattern in Java?
 *
 *
 *
 *
 */

/* 1.3 The Visitor design pattern */

/*
 * The design pattern that we have spent the most time discussing in class is
 * the Visitor design pattern. The Visitor pattern deals with the problem of
 * needing to access each node of some ADT (Abstract Data Type) that can be
 * thought of as a tree or graph, and performing some operation at that node
 * based on the type of node that it is. So, for example, when we consider the
 * problem of writing a generalized fold over RB trees, we are encountering one
 * particular instance of the Visitor pattern.
 *
 * We will consider instead the Expression datatype that we have been working
 * with all semester. In ML, we would write:
 *
 * type exp = Int of int | Float of float | Plus of exp * exp |
 *   Times of exp * exp
 * ;;
 */

/* 1.3a) ML review and Visitor pattern exercise bundled into one!
 *
 * Write a function that evaluates an exp to a floating point value.
 *
 let rec eval (e:exp) : float =
   match e with
   | Int i -> float_of_int i
   | Float f -> f
   | Plus (e1,e2) -> (eval e1) +. (eval e2)
   | Times (e1,e2) -> (eval e1) *. (eval e2)

 *
 *
 * ;;
 *
 * Write a function that calculates the size of an exp (i.e., the total number
 * of nodes).
 *
 * let rec size (e:exp) : int =
 *
 let rec size (e:exp) : int =
   match e with
   | Int i -> 1
   | Float f -> 1 
   | Plus (e1,e2) -> 1 + (size e1) + (size e2)
   | Times (e1,e2) -> 1 + (size e1) + (size e2)
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 * ;;
 */

/*
 * These two functions look quite similar. That is because they both satisfy
 * the Visitor pattern: they both visit each "node" of the Expression in
 * question, performing some operation at each node. Can we factor out this
 * structure?
 * */
 
/* 1.3b) Fill in the visit function in the following code.

/*
 * type 'a visitor = {
 *   int_visit : int -> 'a;
 *   float_visit : float -> 'a;
 *   plus_visit : 'a -> 'a -> 'a;
 *   times_visit : 'a -> 'a -> 'a;
 * }
 *
 * let visit (v : 'a visitor) (e : exp) : 'a =
    let rec inner e =
        match e with
        | Int i -> v.int_visit i
        | Float f -> v.float_visit f
        | Plus e1,e2 -> v.plus_visit (inner e1) (inner e2)
        | Times e1,e2 -> v.times_visit (inner e1) (inner e2)
 
 * ;;
 */

/*
 * We have thus factored out the common structure inherent in any operation
 * that "consumes" an Expression. Writing any particular such function is now
 * simple:
 */

/* 1.3c) Fill in the blanks below in the eval_visitor record.
 *
 * let eval_visitor : float visitor = {
 *
 *   int_visit =
 *
 *   float_visit =
 *
 *   plus_visit =
 *
 *   times_visit =
 *
 * }
 *
 * let eval = visit eval_visitor;;
 */

/*
 * How easy is it to extend this framework? Adding new functions operating over
 * Expressions is as easy as implementing eval was just now. However, adding
 * new datatype constructors to the exp type is quite hard. Let's say, for
 * example, that we wanted to add a Minus constructor. We would then need to
 * change the 'a visitor type declaration to include a minus_visit function,
 * and modify each of the visitors that we had constructed accordingly. Thus,
 * as you found out on PS6, in ML, it's easy to add new functions for a given
 * datatype, but hard to add new constructors. As you'll recall from that
 * problem set, the class inheritance structure of Java leaves us with the
 * opposite problem: adding new types of Expressions is easy, but implementing
 * new operations over Expressions is hard.
 */

/*
 * What does this have to do with Java, you might reasonably ask? Well, since
 * the Visitor pattern is, like any pattern, language-independent, we can also
 * consider its implementation in Java. Can we alleviate the difficulty in
 * adding new operations over Expressions without sacrificing the ease of
 * creating new types of Expressions?
 */

/*
 * In order to visit a data structure, we will construct a new visitor object
 * with a method for each type of node (Expression) we have. We will then call
 * the accept() method on each one, which invokes the method in the visitor
 * object appropriate to the type of node in question.
 */


/* 1.4 The Visitor pattern in Java */

/* The following code is the final version of the Visitor pattern for 
 * Expressions presented in lecture. 

/* We define an interface that all visitors should implement. Recall that, 
 * to allow easy extension of the ExpVisitor interface, we define an empty
 * ExpVisitor interface and, for each node, an interface containing the 
 * visit operation on that node.
 */ 

interface ExpVisitor<T> { /* empty */ }

interface IntExpVisitor<T> extends ExpVisitor<T> {
    T visitInt(Int e);
}

interface PlusExpVisitor<T> extends ExpVisitor<T> {
    T visitPlus(Plus e, T o1, T o2);
}

/* All nodes (e.g. Int, Plus) extend the Exp abstract base class, so all nodes 
 * implement the accept method. The accept method takes a visitor, downcasts it
 * to the visitor with the operation appropriate for this node, and calls the 
 * operation with itself as an argument. Note that accept is a generic method;
 * the return type is the same as the type parameter T of ExpVisitor.
 */ 

abstract class Exp { abstract <T> T accept(ExpVisitor<T> v); }

class Int extends Exp {
    final int i;
    Int(int i) { this.i = i; }
    <T> T accept(ExpVisitor<T> v) {
        IntExpVisitor<T> iv = (IntExpVisitor<T>)v;
        return iv.visitInt(this);
    }
}

class Plus extends Exp {
    final Exp e1; final Exp e2;
    Plus(Exp e1, Exp e2) { this.e1 = e1; this.e2 = e2; }
    <T> T accept(ExpVisitor<T> v) {
        T o1 = e1.accept(v);
        T o2 = e2.accept(v);
        PlusExpVisitor<T> pv = (PlusExpVisitor<T>)v;
        return pv.visitPlus(this, o1, o2);
    }
}

/* Now we define a visitor, SizeVisitor, that finds the size of an expression. 
 * For every node an Exp can contain, SizeVisitor should implement the
 * ExpVisitor interface specific to that node. Note that the type parameter T
 * above has been replaced with Integer. 
 */ 

class SizeVisitor implements IntExpVisitor<Integer>, PlusExpVisitor<Integer> {
    public Integer visitInt(Int e) { return 1; }
    public Integer visitPlus(Plus e, Integer o1, Integer o2) {
        return 1 + o1 + o2;
    }

    // Find the size of e by passing a new SizeVisitor to the root node
    static int size(Exp e) {
        SizeVisitor v = new SizeVisitor();
        return e.accept(v);
    }
}

/* Exercise 1.4.a. Implement a visitor that evaluates an Expression. Hint: it
 * looks a lot like SizeVisitor.
 */

class EvalVisitor implements IntExpVisitor<Integer>, PlusExpVisitor<Integer> {
    // Implement the necessary interfaces.








    // Evaluate an Expression to an int.
    static int eval(Exp e) {
        EvalVisitor v = new EvalVisitor();
        return e.accept(v);
    }
}

/* Exercise 1.4.b. Suppose you were implementing a visitor that prints out an 
 * Expression. How would your method declarations change?
 *
 *
 *
 */


/* How do we add Minus to our Expressions? We add a new Minus class in the style
 * of Int and Plus. We need a visit operation for Minus nodes; thus we add
 * the MinusExpVisitor interface.  */

interface MinusExpVisitor<T> extends ExpVisitor<T> {
    T visitMinus(Minus e, T o1, T o2);
}

class Minus extends Exp {
    private Exp e1; private Exp e2;
    Minus(Exp e1, Exp e2) { this.e1 = e1; this.e2 = e2; }
    <T> T accept(ExpVisitor<T> v) {
        T o1 = e1.accept(v);
        T o2 = e2.accept(v);
        MinusExpVisitor<T> pv = (MinusExpVisitor<T>)v;
        return pv.visitMinus(this, o1, o2);
    }
}

/* Now we need a visitor that can handle Minus nodes. We define 
 * SizeMinusVisitor, which inherits operations on Int and Plus nodes from 
 * SizeVisitor. 
 */ 

class SizeMinusVisitor extends SizeVisitor implements MinusExpVisitor<Integer> {
    public Integer visitMinus(Minus e, Integer o1, Integer o2) {
        return 1 + o1 + o2;
    }

    static int size(Exp e) {
        /* Your code here */



    }
}

/* Exercise 1.4.c. Why can't SizeMinusVisitor simply use the size method it 
 * inherited from SizeVisitor?
 *
 *
 *
 */


/* Exercise 1.4.d. Implement the size method of SizeMinusVisitor.
 */

/* Let's see our code in action! */

class UseVisitorPattern {
    public static void main() {
        Exp e1 = new Plus(new Int(13), new Plus(new Int(14), new Int(15)));
        System.out.println("e1 has size: " + SizeVisitor.size(e1));
        System.out.println("e1 has value: " + EvalVisitor.eval(e1));

	Exp e2 = new Minus(e1, new Minus(e1, new Int(42)));
	System.out.println("e2 has size: " + SizeMinusVisitor.size(e2));
        // System.out.println("e2 has value: " + EvalVisitor.eval(e2));
        // What would happen if we uncommented the above line?
    }
}

/* To extend Expressions to contain Minus nodes, we had to add:
 *   - the Minus class definition
 *   - an interface that requires the visit method for Minus nodes
 *   - a visitor that implements the visit method for Minus nodes
 * 
 * This may seem cumbersome, but it's an extensibility win. We can now
 * add new types of Expressions without modifying existing code. Earlier we
 * considered adding new operations over Expressions (evaluation, printing);
 * writing visitors to handle these operations didn't require us to touch
 * existing code either. The Visitor pattern in Java has freed us from the 
 * tradeoff between extending types and extending operations. 
 */ 


/*******************************/
/* 2. Final project questions? */
/*******************************/


public class section9 {
    public static void main(String[] args) {
        System.out.println("Invoking the Strategy pattern example.");
        UseStrategyPattern.main();
        System.out.println("Invoking the Visitor pattern example.");
        UseVisitorPattern.main();
    }
}
