/*
 * CS 51 Section 8 SOLUTIONS
 * Week of 4/4/10
 *
 * Triple feature!
 *
 * Return of the Java (ROTJ): Classes, Inheritance, The Evaluation Model
 *   - and -
 * Design Exercises: Free Practice for Your Google Interview
 *   - and -
 * Minimax: Start Losing Games to Your Computer in Two Easy Steps
 */





/* 1. Return of the Java: Classes, Inheritance, The Evaluation Model */

class Util {
    public static String quote(String s) {
	return "\"" + s + "\"";
    }
}

// - What does the method 'quote' do?

// Returns a new copy of a given string, in double quotes.
// Note that it doesn't destructively update the string in-place.
// Java Strings are immutable.

// - Why is it static? (Review: what's a static method?)

// It's not associated with any data corresponding to particular objects
// of a class. (Rather, it's associated with the class itself.)

// - Why do we have a separate Util class instead of just
//     defining the function at top-level?

// Trick question. Java doesn't let you define functions at top level.
// (Why not? One theory: the unit of compilation is the class
//  (e.g., class foo in foo.java --> foo.class).
//  Permitting function definitions at top level would complicate this model.
//  (Though one could argue that this might not be the right model
//   to begin with.))


// 1.1. ROTJ: Initialization.

class Foo {
    public String s;
}

// Note: all (non-primitive) objects in Java are actually references to data
// on the heap.

// *****
// In other words, where 'String' is written, we should read 'string option'
// (it might be null!).
// *****

class Part1_1 {
    public static void main() {
	Foo f = new Foo();
	// - What would happen if we uncommented the following line?

	// We would get a NullPointerException.
	// We can't call length() on s; s is initialized to null.
	// (Why isn't the default constructor called on member variables?
	//  Not all classes have a default constructor.
	//  If any constructors are defined explicitly,
	//  a default no-argument constructor is not inferred.)

	// System.out.println(f.s.length());
	// - What will the following line print?

	// It will print "null" (with the quotes).
	// Isn't that weird?
	// Lots of errors can come from regarding null as a bona fide element
	//  of a type.
	// (That sort of thing also explains websites with Undefined visitors.)

	System.out.println(Util.quote(f.s));
    }
}


// 1.2. ROTJ: Member data.

// Warning: the following code has intentionally poor design.

class Person {
    private String name;
    private boolean alive;
    public Person(String name, boolean alive) {
	this.name = name;
	this.alive = alive;
    }
    public String getName() {
	return name;
    }
    public boolean isAlive() {
	return alive;
    }
    public void setAlive(boolean b) {
	alive = b;
    }
};

class Dragon {
    private static int people_eaten = 0;
    public void eat(Person p) {
	if (p == null || !p.isAlive()) {
	    return;
	}
	if (p.getName() != "Pinocchio") {
	    p.setAlive(false);
	}
	++people_eaten;  // Om nom nom.
    }
    public int getPeopleEaten() {
	return people_eaten;
    }
};

// - Why won't the eat method call isAlive on a null Person reference?

// 'Short-circuit evaluation': if the first argument to an or (||)
// evaluates to true, Java (and many languages) don't bother to
// evaluate the second argument. Same for and (&&) if the first
// argument is false.

// - Why is it OK for a non-static method ('eat') to use a static
//   member variable ('people_eaten'), but not the other way around?

// One can think of static data as a single copy shared among all
// instances of a class. Each instance can get at the shared data,
// but a communal function doesn't know which instance's data to use
// (and, indeed, it may be the case that no instances exist).

class Part1_2 {
    public static void main() {
	Dragon fred = new Dragon();
	System.out.println(fred.getPeopleEaten());  // 0
	Dragon bill = new Dragon();
	Person bob = new Person("Bob", true);
	System.out.println(bob.isAlive());  // true
	bill.eat(bob);
	bill.eat(bob);
	System.out.println(bob.isAlive());  // false
	System.out.println(bill.getPeopleEaten());  // 1
	System.out.println(fred.getPeopleEaten());  // 1
    }
}

// - What are some design flaws in our Dragon/Person code?

// It's misleading to name getPeopleEaten the way it is,
// because it actually represents people eaten by /all/ dragons.

// If Pinocchio is eaten N times, it will count as N people eaten,
// even though he is only one person. (Again, a problem that could
// be solved by good naming and documentation: numDinnerEvents?)


// 1.3. ROTJ: Obfuscated code!

interface Frob {
    public void frob();
    public int frob(Frob f);
}

abstract class Bar implements Frob {
    protected int bar;
    private int baz;

    public Bar(int bar, int baz) {
	// - Why don't we need to call super here?

	// The superclass (Object) has a default no-argument constructor.
	// (Frob is not the superclass; it's just an interface.)

	this.bar = bar;
	this.baz = baz;
    }

    public void frob() {
	++baz;
    }

    public int frob(Frob f) {
	f.frob();
	return bar * baz + qux(f);
    }

    abstract public int qux(Frob f);
}

class Whatsit extends Bar {
    private int baz;

    public Whatsit(int baz) {
	super(3, baz);
    }

    public void frob() {
	super.frob();
	super.frob();
    }

    public int qux(Frob f) {
	return 9;
    }

    public int quux(Frob f) {
	f.frob();
	frob();
	return baz + f.frob(f);
    }
}

class Part1_3 {
    public static void main() {
	Whatsit w = new Whatsit(5);
	System.out.println(w.quux(w));

	// It evaluates to 42.
	//
	// Key points:
	//
	//   - When x is an instance of Whatsit, and we call x.frob(),
	//       Whatsit's frob() gets called, even if x in context
	//       is declared as a Bar. It's the runtime type that matters
	//       in determining what method gets called (i.e., what type
	//       it actually has, not what type it formally has).
	//
	//  - Whatsit.baz and Bar.baz are distinct fields of a Whatsit.
	//      When baz is accessed from a Whatsit method, it accesses
	//      the former; a Bar method, the latter.
	//      So, for instance, quux explicitly accesses Whatsit's baz,
	//      which has remained 0 (its default value) the whole time.
    }
}

public class section8 {
    public static void main(String[] args) {
	System.out.println("=== Part 1.1 ===");
	Part1_1.main();
	System.out.println("=== Part 1.2 ===");
	Part1_2.main();
	System.out.println("=== Part 1.3 ===");
	Part1_3.main();
    }
}





/* 2. Design Exercises: Free Practice for Your Google interview! */

// The front-end is the part of the software that talks to the user.
// The back-end is the part that talks to the data.
// (This is different from the client/server distinction;
//  e.g., parts of the front-end might run on the client.)

// Example:
//   - GMail front-end: Javascript code in your browser.
//     - Alternate GMail front-end: a commandline interface.
//   - GMail back-end: C++/Java/Python code on servers at The Google.
//     - Alternate GMail back-end: a flat text file on your machine.

// Ideally, your front-end and back-end are decoupled,
// and the protocol they communicate through is standardized,
// so that you can swap out different front-ends with the same
// back-end and vice versa.

// *****
// When designing a large system, it's almost always better to design
// the back-end first. Even a simple command-line front-end will
// do something useful given a good back-end.
// If you design a pretty front-end with bells and whistles first,
// you may find there are serious technical limitations that prevent
// you from implementing the corresponding back-end in the desired manner.
// *****





/* 3. Minimax: Start Losing games to Your Computer in Two Easy Steps */

// In what follows, we're going to consider two-player turn-based games.
// (Similar ideas extend to other games.)


// 3.1. What is a strategy?

// For each game state in which you must move, your strategy specifies a legal
// move that you will take.
// There are good strategies and bad strategies.


// 3.2. What is a greedy strategy? Give an example of when a greedy strategy
//      might fail (e.g. in chess).

// In a greedy strategy, we consider only what position looks best immediately
// following our move. (We might determine 'best' for these purposes by
// a variety of heuristics: in which scenario do you have the most pieces;
// in which scenario are you closer to queening a pawn; in which scenario
// do your pieces make a pretty picture on the board; and so on.)


// 3.3. Let's step back to tic-tac-toe. Can we do better than a greedy strategy?
//      Can we 'play perfectly' in some sense?

// http://en.wikipedia.org/wiki/Minimax

// We start at the bottom of the game tree:
// the 'value' of a completed game is fully determined --
// say, +1 if we win, -1 if we lose, 0 for a draw.

// Then, at each node where /we/ have a choice,
// we take the max of its children as its value
// (we're always going to pick the best outcome for us)
// and where our opponent has a choice,
// we take the min (our opponent is going to pick the best for them).

// The value at the root is the outcome if both parties play perfectly.

// To actually play, given a position (a node),
// take any of its children that realizes the max value.


// 3.4. What does it mean to say 'tic-tac-toe is a draw'?

// The value at the root is 0. (With perfect play, it's a draw.)


// 3.5. How about checkers or chess -- can we solve them the same way?
//      Why [not]?

// No. We would have to reason about 17 kajillion board states.

// Instead, we can cut off our minimax search at some finite depth d
// into the tree and just judge the value of the depth-d nodes by a heuristic
// rather than continuing the search all the way to the leaves
// (i.e. the end of the game).

// (N.B. cutting off at depth d = 1 corresponds to a greedy strategy.)


// 3.6. What are some reasonable heuristics that might help us in checkers?

// E.g., number of pieces we have minus the number our opponent has.
// We'll think more about this on the problem set.
