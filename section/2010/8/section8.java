/*
 * CS 51 Section 8
 * Week of 4/4/10
 *
 * Triple feature!
 *
 * Return of the Java (ROTJ): Classes, Inheritance, The Evaluation Model
 *   - and -
 * Design Exercises: Free Practice for Your Google Interview
 *   - and -
 * Minimax: Start Losing Games to Your Computer in Two Easy Steps
 */





/* 1. Return of the Java: Classes, Inheritance, The Evaluation Model */

class Util {
    public static String quote(String s) {
        return "\"" + s + "\"";
    }
}

// - What does the method 'quote' do?
// - Why is it static? (Review: what's a static method?)
// - Why do we have a separate Util class instead of just
//     defining the function at top-level?


// 1.1. ROTJ: Initialization.

class Foo {
    public String s;
}

class Part1_1 {
    public static void main() {
        Foo f = new Foo();
        // - What would happen if we uncommented the following line?
        // System.out.println(f.s.length());
        // - What will the following line print?
        System.out.println(Util.quote(f.s));
    }
}


// 1.2. ROTJ: Member data.

// Warning: the following code has intentionally poor design.

class Person {
    private String name;
    private boolean alive;
    public Person(String name, boolean alive) {
        this.name = name;
        this.alive = alive;
    }
    public String getName() {
        return name;
    }
    public boolean isAlive() {
        return alive;
    }
    public void setAlive(boolean b) {
        alive = b;
    }
};

class Dragon {
    private static int people_eaten = 0;
    public void eat(Person p) {
        if (p == null || !p.isAlive()) {
            return;
        }
        if (p.getName() != "Pinocchio") {
            p.setAlive(false);
        }
        ++people_eaten;  // Om nom nom.
    }
    public int getPeopleEaten() {
        return people_eaten;
    }
};

// - Why won't the eat method call isAlive on a null Person reference?

// - Why is it OK for a non-static method ('eat') to use a static
//   member variable ('people_eaten'), but not the other way around?

class Part1_2 {
    public static void main() {
        Dragon fred = new Dragon();
        System.out.println(fred.getPeopleEaten());  // - ?
        Dragon bill = new Dragon();
        Person bob = new Person("Bob", true);
        System.out.println(bob.isAlive());  // - ?
        bill.eat(bob);
        bill.eat(bob);
        System.out.println(bob.isAlive());  // - ?
        System.out.println(bill.getPeopleEaten());  // - ?
        System.out.println(fred.getPeopleEaten());  // - ?
    }
}

// - What are some design flaws in our Dragon/Person code?


// 1.3. ROTJ: Obfuscated code!

interface Frob {
    public void frob();
    public int frob(Frob f);
}

abstract class Bar implements Frob {
    protected int bar;
    private int baz;

    public Bar(int bar, int baz) {
        // - Why don't we need to call super here?
        this.bar = bar;
        this.baz = baz;
    }

    public void frob() {
        ++baz;
    }

    public int frob(Frob f) {
        f.frob();
        return bar * baz + qux(f);
    }

    abstract public int qux(Frob f);
}

class Whatsit extends Bar {
    private int baz;

    public Whatsit(int baz) {
        super(3, baz);
    }

    public void frob() {
        super.frob();
        super.frob();
    }

    public int qux(Frob f) {
        return 9;
    }

    public int quux(Frob f) {
        f.frob();
        frob();
        return baz + f.frob(f);
    }
}

class Part1_3 {
    public static void main() {
        Whatsit w = new Whatsit(5);
        System.out.println(w.quux(w));
    }
}

public class section8 {
    public static void main(String[] args) {
        System.out.println("=== Part 1.1 ===");
        Part1_1.main();
        System.out.println("=== Part 1.2 ===");
        Part1_2.main();
        System.out.println("=== Part 1.3 ===");
        Part1_3.main();
    }
}





/* 2. Design Exercises: Free Practice for Your Google interview! */


// 2.0. Explain the terms 'front-end' and 'back-end'.






// 2.1. Design a calculator tool.











// 2.2. Design a chess game.











// 2.3. Design a social networking website.











// 2.4. Person A names a project. Person B designs it. Person C critiques the design.















/* 3. Minimax: Start Losing games to Your Computer in Two Easy Steps */

// In what follows, we're going to consider two-player turn-based games.
// (Similar ideas extend to other games.)


// 3.1. What is a strategy?





// 3.2. What is a greedy strategy? Give an example of when a greedy strategy
//      might fail (e.g. in chess).






// 3.3. Let's step back to tic-tac-toe. Can we do better than a greedy strategy?
//      Can we 'play perfectly' in some sense?
















// 3.4. What does it mean to say 'tic-tac-toe is a draw'?





// 3.5. How about checkers or chess -- can we solve them the same way?
//      Why [not]?









// 3.6. What are some reasonable heuristics that might help us in checkers?
