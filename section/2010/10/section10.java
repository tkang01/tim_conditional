							/*
 * CS 51 Section 10
 * Week of 4/18/10
 * Intro to Concurrency, or Broken Programs and How to Fix Them
 */


/* Exercise 0.0  Define the following terms:

    - Concurrency


   
    - Thread


   
    - Critical section

    

    - lock


    
    - deadlock


    

*/

/* Exercise 0.1
 *
 * Threads make life complicated!  Why do we bother?
 */






/* In Java, a concurrent program uses threads to interleave execution
 * of different parts of the program. Threads are created by extending
 * the Thread class and implementing the run() method. In the programs
 * you've written so far, all your code has run in a main thread that
 * was created by default. If a Thread object is created, it begins
 * executing the code in its run() method after its start() method is
 * called. Calling a thread's join() method causes the main thread to
 * wait until the other thread finishes.
 *
 * We may reason about order of execution more easily by defining
 * "happens-before" relationships. Given a Thread t,
 *   - A call to t.start() happens before any statement executed by t
 *   - If t.join() is called, all statements executed by t happen before
 *     t.join() finishes
 */



/* Exercise 1. Thread interleaving in a final exam generator */


// Utility code for simulating scheduling delays.
class Rand {
    public static int rand(int n) {
	return (int)(Math.random() * n);
    }
}

class Delay {
    public static void delay(Thread t) {
	try { t.sleep(Rand.rand(10)); } catch (InterruptedException e) { }
    }
}

// A Proofreader makes the exam more reasonable by halving the number
// of questions
class Proofreader extends Thread {
    private FinalExamGenerator generator;
    
    public Proofreader(FinalExamGenerator f) {
        this.generator = f;
    }
    
    public void run() {
        int t = generator.getNumQs();
        generator.setNumQs(t/2);
    }
}

class FinalExamGenerator {
    private int numQuestions;

    public int getNumQs() {
        return this.numQuestions;
    }

    public void setNumQs(int n) {
        this.numQuestions = n;
    }

    
    // Create 42 exam questions
    void generateQuestions() {
        this.numQuestions = 42;
    }

    // Create an exam by running a Proofreader thread simultaneously with
    // generateQuestions
    void generateExam() throws InterruptedException {
        Thread tf = new Proofreader(this);
        tf.start();
        generateQuestions();
        tf.join();
    }

/* Exercise 1.1 What are the possible outcomes for numQuestions? How does each
 * outcome happen?
 *
 * 
 *
 *
 */

    // Generate many exams and count how many contain an unexpected number of
    // questions
    public static void main() {
        int totalExamCount = 100;
        int evilExamCount = 0;
        int vacuousExamCount = 0;
        for (int i = 0; i < totalExamCount; i++) {
            FinalExamGenerator g = new FinalExamGenerator();
            try {
                g.generateExam();
                if (g.numQuestions == 42)
                    evilExamCount++;
                if (g.numQuestions == 0)
                    vacuousExamCount++;
            }
            catch (InterruptedException e) {
                System.out.println("A thread was interrupted");
            }
        }
        System.out.println(evilExamCount + " of " + totalExamCount + " exams generated were evil");
        System.out.println(vacuousExamCount + " of " + totalExamCount + " exams generated were vacuous");
    }
}

/* Exercise 1.2 Propose a small change in the above code that would eliminate
 * the concurrency problem.
 */








/* Exercise 2. More subtle thread interleaving
 * The job of collecting census forms from people who didn't complete theirs the
 * first time is unpleasant, so we have parallelized it across two threads
 * ("census takers"). Unfortunately, the following program doesn't always
 * keep an accurate count of how many people have been surveyed.
 */

class CensusData {
    protected int numPeopleSurveyed;
    public void addPersonSurveyed() {
        numPeopleSurveyed++;
    }
}



class CensusTakerThread extends Thread {
    private int numPeopleAssigned;
    private CensusData data;

    public CensusTakerThread(int n, CensusData d) {
        numPeopleAssigned = n;
        data = d;
    }

    public void run() {
        // A census taker needs to collect surveys one by one
        for (int i = 0; i < numPeopleAssigned; i++) {
            data.addPersonSurveyed();
	}
}


class CensusTaker {
    protected static void collectData(CensusData sharedData) {
        Thread anna = new CensusTakerThread(5000, sharedData);
        Thread bill = new CensusTakerThread(5000, sharedData);
        anna.start();
        bill.start();
        try {
            anna.join();
            bill.join();
        }
        catch (InterruptedException e) {
            System.out.println("A thread was interrupted");
        }
    }

    public static void main() {
        CensusData sharedData = new CensusData();
        collectData(sharedData);
        System.out.println("Number of surveys collected: " + sharedData.numPeopleSurveyed);
    }
}

/* 
 * Run a few times.
 *
 * Probably, it will behave just fine.
 * Delays at the precision we're looking for are hard to induce.
 * Still, if you operate at cloud-scale, code like this will fail sometime...
 * (And the kind of bugs you can't reproduce are the worst!)
 *
 * Exercise 2.1. What's going on? Where's the critical section?
 *
 */









/* Exercise 2.2. Fix the problem. */

class BetterCensusData {
    protected int numPeopleSurveyed;
    
    // (Exercise 2.2: something something something addPersonSurveyed?)
}



/*
// This code can be uncommented when you complete exercise 2.2.
class BetterCensusTaker extends CensusTaker {
    public static void main() {
        CensusData sharedData = new BetterCensusData();
        collectData(sharedData);
        System.out.println("Number of surveys collected: " + sharedData.numPeopleSurveyed);
    }
}
*/


/* Exercise 3. Spot the solutions */

class Foo {
    private int a = 1;
    private int b = 1;
    public void m1() {
        if (a > 0)
            b *= -1;
    }
    public void m2() {
        if (b > 0)
            a *= -1;
    }
}

class FooThread extends Thread {
    Foo f;
    boolean b;
    public FooThread(Foo f, boolean b) {
        this.f = f;
        this.b = b;
    }

    public void run() {
        if (b)
            f.m1();
        else
            f.m2();
    }
}

class FooProblem {
    public static void main() throws InterruptedException {
        Foo f = new Foo();
		Thread t1 = new FooThread(f, true);
		Thread t2 = new FooThread(f, false);
		t1.start();
		t2.start();
		t1.join();
		t2.join();
    }
}

/* Exercise 3.1. What values (f.a, f.b) are possible after main() executes?
 * Which interleavings produce them?
 *
 */








/* Exercise 3.2. Which of the following replacements for Foo will rule out
   the possibility (a = -1, b = -1)? */

/* Exercise 3.2.1
 */

class Foo1 {
    int a = 1;
    int b = 1;
    public synchronized void m1() {
        if (a > 0)
            b *= -1;
    }
    public synchronized void m2() {
        if (b > 0)
            a *= -1;
    }
}

/* Exercise 3.2.2
 */

class Foo2 {
    int a = 1;
    int b = 1;
    final Object lock1 = new Object();
    final Object lock2 = new Object();

    public void m1() {
        synchronized(lock1) {
            if (a > 0)
                b *= -1;
        }
    }
    public void m2() {
        synchronized(lock2) {
            if (b > 0)
                a *= -1;
        }
    }
}

/* Exercise 3.2.3
 */

class Foo3 {
    int a = 1;
    int b = 1;
    final Object lock1 = new Object();
    final Object lock2 = new Object();

    public void m1() {
        synchronized(lock1) {
            if (a > 0) {
                synchronized(lock2) {
                    b *= -1;
                }
            }
        }
    }
    public void m2() {
        synchronized(lock2) {
            if (b > 0) {
                synchronized(lock1) {
                    a *= -1;
                }
            }
        }
    }
}

/* Exercise 3.2.4
 */

class Foo4 {
    int a = 1;
    int b = 1;
    final Object lock1 = new Object();
    final Object lock2 = new Object();

    public void m1() {
        synchronized(lock1) {
            synchronized(lock2) {
                if (a > 0) {
                    b *= -1;
                }
            }
        }
    }
    public void m2() {
        synchronized(lock1) {
            synchronized(lock2) {
                if (b > 0) {
                    a *= -1;
                }
            }
        }
    }
}

public class section10 {
    public static void main(String[] args) {
        System.out.println("\nRunning FinalExamGenerator");
        FinalExamGenerator.main();
        System.out.println("\nRunning CensusTaker");
        CensusTaker.main();
    }
}

/* Summary:
 *
 * Reasoning about concurrent programming is hard.  You should now
 * have a sense of why, and should be able to see concurrency problems
 * in toy examples.  If you want to learn more, take CS61.  If you
 * want to really understand concurrency, take CS161.
 */
