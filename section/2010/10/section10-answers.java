/*
 * CS 51 Section 10
 * Week of 4/18/10
 * Intro to Concurrency, or Broken Programs and How to Fix Them
 */


/* Exercise 0.0  Define the following terms:

    - Concurrency


   
    - Thread


   
    - Critical section

    

    - lock


    
    - deadlock


    

*/

/* Exercise 0.1
 *
 * Threads make life complicated!  Why do we bother?
 */

/*
  For one, Multi-core is the Future (tm). We want to add more processors and
  have our code run faster as a result.
  Also, even if we have only one core, we want preemptive multitasking.
*/




/* In Java, a concurrent program uses threads to interleave execution
 * of different parts of the program. Threads are created by extending
 * the Thread class and implementing the run() method. In the programs
 * you've written so far, all your code has run in a main thread that
 * was created by default. If a Thread object is created, it begins
 * executing the code in its run() method after its start() method is
 * called. Calling a thread's join() method causes the main thread to
 * wait until the other thread finishes.
 *
 * We may reason about order of execution more easily by defining
 * "happens-before" relationships. Given a Thread t,
 *   - A call to t.start() happens before any statement executed by t
 *   - If t.join() is called, all statements executed by t happen before
 *     t.join() finishes
 */



/* Exercise 1. Thread interleaving in a final exam generator */


// A Proofreader makes the exam more reasonable by halving the number
// of questions
class Proofreader extends Thread {
    private FinalExamGenerator generator;
    
    public Proofreader(FinalExamGenerator f) {
        this.generator = f;
    }
    
    public void run() {
        int t = generator.getNumQs();
        generator.setNumQs(t/2);
    }
}

class FinalExamGenerator {
    private int numQuestions;

    public int getNumQs() {
        return this.numQuestions;
    }

    public void setNumQs(int n) {
        this.numQuestions = n;
    }

    
    // Create 42 exam questions
    void generateQuestions() {
        this.numQuestions = 42;
    }

    // Create an exam by running a Proofreader thread simultaneously with
    // generateQuestions
    void generateExam() throws InterruptedException {
        Thread tf = new Proofreader(this);
        tf.start();
        generateQuestions();
        tf.join();
    }

/* Exercise 1.1 What are the possible outcomes for numQuestions? How does each
 * outcome happen?
 *
 * Answer: 0, 21, 42.
 */

    // Generate many exams and count how many contain an unexpected number of
    // questions
    public static void main() {
        int totalExamCount = 10000;
        int evilExamCount = 0;
		int vacuousExamCount = 0;
        for (int i = 0; i < totalExamCount; i++) {
            FinalExamGenerator g = new FinalExamGenerator();
            try {
                g.generateExam();
                if (g.numQuestions == 42)
                    evilExamCount++;
                if (g.numQuestions == 0)
                    vacuousExamCount++;
            }
            catch (InterruptedException e) {
                System.out.println("A thread was interrupted");
            }
        }
        System.out.println(evilExamCount + " of " + totalExamCount + " exams generated were evil");
        System.out.println(vacuousExamCount + " of " + totalExamCount + " exams generated were vacuous");
    }
}

/* Exercise 1.2 Propose a small change in the above code that would eliminate
 * the concurrency problem.
 *
 * Answer: call generateQuestions() before tf.start(). This would enforce the
 * happens-before relationship we want:
 *   numQuestions = 42
 * happens before
 *   numQuestions /= 2
 */


/* Exercise 2. More subtle thread interleaving
 * The job of collecting census forms from people who didn't complete theirs the
 * first time is unpleasant, so we have parallelized it across two threads
 * ("census takers"). Unfortunately, the following program doesn't always
 * keep an accurate count of how many people have been surveyed.
 */

class CensusData {
    protected int numPeopleSurveyed;
    public void addPersonSurveyed() {
        numPeopleSurveyed++;
    }
}



class CensusTakerThread extends Thread {
    private int numPeopleAssigned;
    private CensusData data;

    public CensusTakerThread(int n, CensusData d) {
        numPeopleAssigned = n;
        data = d;
    }

    public void run() {
        // A census taker needs to collect surveys one by one
        for (int i = 0; i < numPeopleAssigned; i++)
            data.addPersonSurveyed();
    }
}


class CensusTaker {
    
    protected static void collectData(CensusData sharedData) {
        Thread anna = new CensusTakerThread(5000, sharedData);
        Thread bill = new CensusTakerThread(5000, sharedData);
        anna.start();
        bill.start();
        try {
            anna.join();
            bill.join();
        }
        catch (InterruptedException e) {
            System.out.println("A thread was interrupted");
        }
    }

    public static void main() {
        CensusData sharedData = new CensusData();
        collectData(sharedData);
        System.out.println("Number of surveys collected: " + sharedData.numPeopleSurveyed);
    }
}

/* Run a few times.
 *
 * Exercise 2.1. What's going on? Where's the critical section?
 *
 *
 * Answer: The line "numPeopleSurveyed++" is not atomic; it works like the
 * following:
 *   int t = numPeopleSurveyed;
 *   int s = t+1;
 *   numPeopleSurveyed = s;
 *
 * One thread may trample another thread's increment. Thus, the final value of
 * numPeopleSurveyed may be less than the correct value.
 */

/* Exercise 2.2. Fix the problem in the BetterCensusTaker class, which
 * extends the CensusData class, by overriding an appropriate method.
 */
class BetterCensusData extends CensusData {
    /* Your code here */





}



class BetterCensusTaker extends CensusTaker {
    public static void main() {
        CensusData sharedData = new BetterCensusData();
        collectData(sharedData);
        System.out.println("Number of surveys collected: " + sharedData.numPeopleSurveyed);
    }
}

/* Answer:
   public synchronized void addPersonSurveyed() {
   numPeopleSurveyed++;
   }

   * which is shorthand for
   public void addPersonSurveyed() {
   synchronize(this) {
   numPeopleSurveyed++;
   }
   }
   * Here, the CensusData object whose addPersonSurveyed() method is called
   * acts as an intrinsic lock.
   */


/* Exercise 3. Spot the solutions */

class Foo {
    private int a = 1;
    private int b = 1;
    public void m1() {
        if (a > 0)
            b *= -1;
    }
    public void m2() {
        if (b > 0)
            a *= -1;
    }
}

class FooThread extends Thread {
    Foo f;
    boolean b;
    public FooThread(Foo f, boolean b) {
        this.f = f;
        this.b = b;
    }

    public void run() {
        if (b)
            f.m1();
        else
            f.m2();
    }
}

class FooProblem {
    public static void main() throws InterruptedException {
        Foo f = new Foo();
        (new FooThread(f, true)).start();
        (new FooThread(f, false)).start();
    }
}

/* Exercise 3.1. What values (f.a, f.b) are possible after main() executes?
 * Which interleavings produce them?
 *
 *
 * Answer: (1, -1), (-1, 1), (-1, -1)
 */

/* Exercise 3.2. Which of the following replacements for Foo will fix the problem? */

/* Exercise 3.2.1
 * Answer: yes. Either thread needs to hold the Foo1 object to proceed.
 */

class Foo1 {
    int a = 1;
    int b = 1;
    public synchronized void m1() {
        if (a > 0)
            b *= -1;
    }
    public synchronized void m2() {
        if (b > 0)
            a *= -1;
    }
}

/* Exercise 3.2.2
 * Answer: no. The lock held by a thread never causes the other thread to block.
 */

class Foo2 {
    int a = 1;
    int b = 1;
    final Object lock1 = new Object();
    final Object lock2 = new Object();

    public void m1() {
        synchronized(lock1) {
            if (a > 0)
                b *= -1;
        }
    }
    public void m2() {
        synchronized(lock2) {
            if (b > 0)
                a *= -1;
        }
    }
}

/* Exercise 3.2.3
 * Answer: no. Deadlock!
 */

class Foo3 {
    int a = 1;
    int b = 1;
    final Object lock1 = new Object();
    final Object lock2 = new Object();

    public void m1() {
        synchronized(lock1) {
            if (a > 0) {
                synchronized(lock2) {
                    b *= -1;
                }
            }
        }
    }
    public void m2() {
        synchronized(lock2) {
            if (b > 0) {
                synchronized(lock1) {
                    a *= -1;
                }
            }
        }
    }
}

/* Exercise 3.2.4
 * Answer: yes. This follows the paradigm for preventing deadlock: always
 * request locks in a predefined order.
 */

class Foo4 {
    int a = 1;
    int b = 1;
    final Object lock1 = new Object();
    final Object lock2 = new Object();

    public void m1() {
        synchronized(lock1) {
            synchronized(lock2) {
                if (a > 0) {
                    b *= -1;
                }
            }
        }
    }
    public void m2() {
        synchronized(lock1) {
            synchronized(lock2) {
                if (b > 0) {
                    a *= -1;
                }
            }
        }
    }
}

public class section10 {
    public static void main(String[] args) {
        System.out.println("\nRunning FinalExamGenerator");
        FinalExamGenerator.main();
        System.out.println("\nRunning CensusTaker");
        CensusTaker.main();
        System.out.println("\nRunning BetterCensusTaker");
        BetterCensusTaker.main();
    }
}

/* Summary:
 *
 * Reasoning about concurrent programming is hard.  You should now
 * have a sense of why, and should be able to see concurrency problems
 * in toy examples.  If you want to learn more, take CS61.  If you
 * want to really understand concurrency, take CS161.
 */
