(* CS51 Section 3 *)
(* Week of 2/21/10 *)

(* ******************** Part 1 - Modules ************************* *)
(* The purpose of this section is to get you comfortable with the
 * syntax and concepts behind signatures and modules. You will gain
 * new respect for the abstraction barrier and how different
 * implementation choices can matter - but shouldn't affect the way
 * the module is used.
 *)

(* As a warmup, fill in the signature and module definitions
 * below. The signature should require a definition for a type "foo"
 * and a type signature for functions "bar" and "baz". The type
 * signatures for bar and baz should both involve type foo in some
 * way.
 *)

let reduce f u xs = List.fold_right f xs u;;

module type QUX =
  sig
    type foo
    val bar : foo -> foo
    val baz : int -> foo
  end
;;

module Gibberish : QUX =
  struct
    type foo = string
    let bar (f:foo) = "BAR" ^ f
    let baz (i:int) = string_of_int(i)
  end
;;


(* Now let's turn to a slightly more serious example: undirected
 * graphs.
 * 
 * Undirected graphs are a type of data structure. They have nodes and
 * edges. For our purposes, the nodes can hold data of any type (so
 * you can have, e.g., undirected graphs of integers, of strings,
 * etc.), although node values are unique (so you can't have two nodes
 * with the vaue 5 or "CS51"). We want to hide our underlying
 * representation of graphs using the module system, so that people
 * can use our graphs without knowing how they're implemented. Thus,
 * we need to write an implementation-independent signature that
 * allows people to query the graph for the various types of
 * information that a user might need, as well as allowing the user to
 * modify the graph structure.
 * 
 * This description is purposefully left somewhat open-ended; we want
 * you to think and brainstorm about what the interface for the type
 * of structure defined above might look like.
 *)

module type UNDIRECTED_GRAPH =
  sig
    type node
    type graph
    val empty : graph
    val nodes : graph -> node list
    val edges : graph -> (node * node) list
    val neighbors : graph -> node -> node list
    val add_node : graph -> node -> graph
    val add_edge : graph -> node -> node -> graph
    val delete_node : graph -> node -> graph
    val delete_edge : graph -> node -> node -> graph
  end
;;

(* Below there are a bunch of function implementations. They
 * correspond to two complete and valid defintions of the below
 * functor, which takes a node type and node comparison function and
 * returns a module of signature UNDIRECTED_GRAPH. Label each function 
 * with 1 or 2, such that when you're done, if you grouped together
 * all of the parts labeled 1 and all of the parts labeled 2, you
 * would have reconstructed the two complete and working functors. *)

module type NODE =
  sig
    type t
    val compare : t -> t -> int
  end
;;

module UndirectedGraph (Node : NODE) : (UNDIRECTED_GRAPH with type
  node = Node.t) =
  struct
    type node = Node.t
    let remove_dupls l =
      reduce (fun x r -> if List.mem x r then r else x::r) [] l

    (* The methods and type definitions below go here! *)
    let empty () = []
    (* ... *)


  end
;;

(* Helper function - would be used in both versions of the functor *)
let remove_dupls l =
  reduce (fun x r -> if List.mem x r then r else x::r) [] l
(************)

(* Label: 1 *)
type graph = (node * node) list
(************)

(* Label: 2 *)
type graph = (node * (node list)) list
(************)

(* Label: 1 & 2 (same for both) *)
let empty () = []
(****************)

(**** YOUR ANSWERS BELOW ****)

(* Label: 2 *)
let nodes (g:graph) =
  map fst g
(****************)

(* Label: 1 *)
let nodes (g:graph) =
  remove_dupls (reduce (fun x r -> let (f,s) = x in f::s::r) [] g)
(****************)

(* Label: 1 *)
let edges (g:graph) =
  g
(****************)

(* Label: 2 *)
let edges (g:graph) =
  remove_dupls
    (List.concat 
       (List.map 
	  (fun x -> let (s,ns) = x in
	     (List.map
		(* Only want to include each edge once! *)
		(fun y -> if (compare s y) < 0
		 then (s,y) else (y,s))
		ns))
	  g))
(****************)

(* Label: 2 *)
let neighbors (g:graph) (n:node) =
  snd (List.hd (List.filter (fun x -> (fst x) = n) g))
(****************)

(* Label: 1 *)
let neighbors (g:graph) (n:node) =
  reduce
    (fun x r ->
      if (fst x) = n then (snd x)::r
      else if (snd x) = n then (fst x)::r
      else r)
    []
    g
(****************)

(* Other implementations omitted. But just to check:
 * 
 * Q: Which label would have a relatively easy implementation of
 * add_edge and delete_edge? (The other one would be comparatively
 * much more diffcult.)
 * 
 * A: Label 1
 * 
 * Q: Which implementation, as given, would have a hard time writing a
 * sensible version of add_node? Why?
 * 
 * A: Label 1, because there is no good way to add island nodes.
 * 
 * 
 * 
 * 
 * 
 *)


(* ************ Part 2 - Left-Leaning Red/Black Trees ************ *)

(* You can find the original slides describing this data structure at:
 * http://www.cs.princeton.edu/~rs/talks/LLRB/RedBlack.pdf
 *  
 * The most important thing to understand about left-leaning red/black
 * trees is what the invariants are:
 * 
 * 1. All paths from the root node to a leaf node must touch the same
 *    number of black nodes.
 * 2. Any red node must not have a red node as a child.
 * 3. The root node is black.
 * 4. Links to red nodes must "lean left" except during the
 *    intermediate steps of an insert or a delete. This means that if a
 *    node has only one red child, it must be the left one.  Nodes can
 *    still have two red children.
 * 
 * As long as the functions you write ensure that these invariants
 * are preserved through the function call, then your red/black trees
 * will stay balanced. Keep in mind, of course, the invariant of all
 * binary search trees, which is that left children of a node have a value
 * less than the node's value and right children have a value greater
 * than the node's value.  (For now we'll assume that there are no
 * duplicated values).
 *)


(* Question: why does any tree that satisfies these invariants have to
   be "roughly" balanced? *)

(*
 * A: How do these invariants guarantee balance? Well, the shortest
 * possible path in a given tree is all black nodes. If that path has
 * length n, then since no red node can have red children, the longest
 * possible path has length at most 2n (alternating black and red
 * nodes). Thus, the tree can never get unbalanced by more than a
 * constant factor.
 *)

(* 
 * In the exercises below, the notation R(n) means a red node with the
 * value n; similarly, B(m) means a black node with the value m.
 * 
 * For each tree given below, state whether it is a valid R/B tree. If
 * it is not, state the invariant that is violated and redraw it as a
 * valid tree.
 *)



(* 2.1 *)

(*     R(2)
 *    /   \
 *  B(1) B(3)
 *)

(* Valid? If not, why not? *)
(* 
 * No; root is not black. Color it black to fix.
 *
 *)


(* 2.2 *)

(*     B(2)
 *    /   \
 *  R(1)  R(3)
 *)

(* Valid? If not, why not? *)
(* 
 * Yes.
 *
 *)


(* 2.3 *)

(*     B(2)
 *    /   \
 *  R(1)  B(3)
 *)

(* Valid? If not, why not? *)
(* 
 * No; paths to leaves don't have the same number of black nodes. Make
 * 3 red.
 *
 *)


(* 2.4 *)

(*    B(5)
 *   /   \
 * B(1)  B(9)
 *         \
 *         R(11)
 *)

(* Valid? If not, why not? *)
(* 
 * No; not left-leaning. Switch it with 9, recolor.
 *
 *)

(* 2.5 *)

(*    B(3)
 *   /   \
 * R(0)  R(10)
 *        /
 *      R(6)  
 *)

(* Valid? If not, why not? *)
(* 
 * No, red child of red node. Color 0 and 10 black.
 *
 *)

(* At some point during this assignment, you may (read: should) find
 * yourself wondering how to test your implementation. The way to do
 * it is to write functions that take a tree as input and check
 * whether all of the invariants hold on that tree. Then you can just
 * create a tree, perform a series of insertions, deletions and
 * lookups on it, and make sure that the invariants hold after every
 * call. If they do, then your implementation is very likely to be
 * correct!
 *)

(* For inserting new elements, we have three helper functions that let
 *  us transform the tree, as described in lecture. 
 *
 * Rotate left: 
 *    |               |
 *    A               B
 *   / \      =>     / \
 *      B           A
 *     / \         / \
 * 
 * Rotate right is just the mirror of the above (i.e., the arrow
 * points the other way).
 * 
 * Color flip takes a node and flips the colors of it and its
 * children.
 * 
 * Code for all of these is given in the slides linked above.
 *)

(* The Java code for insert on LL-R/B trees is given
 * below. If you don't quite understand why the code works correctly,
 * don't worry too much about it; as long as you understand what each
 * piece of the code is doing well enough to translate it into OCaml,
 * then you'll gain more intuition about what's going on as you write
 * up the code. The important part for now is being able to follow
 * each function through. To that end, take a look at the code below
 * and the sample calls given afterward. Try to follow each call
 * through the relevant portions of the code and figure out what the
 * tree will look like after each call.
 *)

(* public void insert(Key key, Value val)
 * {
 *    root = insert_partial(root, key, val);
 *    root.color = BLACK;
 * }
 *)

(* private Node insert_partial(Node h, Key key, Value val)
 * {
 *    if (h == null)
 *       return new Node(key, val, RED); // insert at the bottom
 * 
 *    if (isRed(h.left) && isRed(h.right))
 *       colorFlip(h); // consolidate red nodes on the way down
 * 
 *    int cmp = key.compareTo(h.key);
 *    // standard BST insert code
 *    if (cmp == 0) h.val = val;
 *    else if (cmp < 0)
 *       h.left = insert_partial(h.left, key, val);
 *    else 
 *       h.right = insert_partial(h.right, key, val);
 * 
 *    if (isRed(h.right))
 *       h = rotateLeft(h); // fix right-leaning reds on the way up
 * 
 *    if (isRed(h.left) && isRed(h.left.left))
 *       h = rotateRight(h); // fix two reds in a row on the way up
 * 
 *    return h;
 * }
 *)

(***** SAMPLE CALLS - pseudocode only! *****)
(*
 * As a section, follow these calls through on the board. Try to
 * understand what tree transforms are applied in each call and write
 * down the end result in the space below each call.
 * 
 * t = new RBTree();
 * 
 * t.insert(1);
 * 
 * R(1)
-> B(1)    (color root black)
 * 
 * 
 * t.insert(2);
 * 
 * B(1)
 *   \
 *   R(2)
->
 *   B(2)    (rotateLeft)
 *   /
 * R(1)
 * 
 * 
 * t.insert(3);
 * 
 *   B(2)
 *   /  \ 
 * R(1) R(3)
-> 
 *      B(3)    (rotateLeft)
 *      /
 *    R(2)
 *    /
 * R(1)
->
 *   B(2)    (rotateRight)
 *   /  \ 
 * R(1) R(3)
 * 
 * 
 * t.insert(4);
 * 
 *   R(2)    (colorFlip)
 *   /  \ 
 * B(1) B(3)
 *        \
 *        R(4)
-> 
 *   R(2)    (rotateLeft)
 *   /  \ 
 * B(1) B(4)
 *       /
 *    R(3)
-> 
 *   B(2)    (color root black)
 *   /  \ 
 * B(1) B(4)
 *       /
 *    R(3)
 * 
 * 
 * t.insert(5);
 * 
 *   B(2)    (color root black)
 *   /  \ 
 * B(1) B(4)
 *       / \ 
 *    R(3)  R(5)
 * 
 * (same after rotateLeft and rotateRight)
 * 
 * 
 * Now, on your own or in small pairs, try to do the same for the next
 * three:
 * 
 * t.insert(6);
 * 
 *       B(2)    (colorFlip)
 *     /      \
 *  B(1)       R(4)
 *             /  \ 
 *          B(3)  B(5)
 *                  \
 *                  R(6)
 ->
 *       B(2)    (rotateLeft)
 *     /      \
 *  B(1)       R(4)
 *             /  \ 
 *          B(3)  B(6)
 *                 /
 *               R(5)
 ->
 *        B(4)    (rotateLeft)
 *      /      \
 *   R(2)      B(6)
 *   /  \       /
 * B(1) B(3) R(5)
 * 
 * 
 * t.insert(7);
 * 
 *        B(4)    (rotateLeft)
 *      /      \
 *   R(2)      B(6)
 *   /  \       / \
 * B(1) B(3) R(5) R(7)
 * 
 * (same after rotateLeft and rotateRight)
 * 
 * 
 * t.insert(8);
 * 
 *        B(4)    (colorFlip)
 *      /      \
 *   R(2)      R(6)
 *   /  \       / \
 * B(1) B(3) B(5) B(7)
 *                  \
 *                  R(8)
 ->
 *        B(4)    (rotateLeft)
 *      /      \
 *   R(2)      R(6)
 *   /  \       / \
 * B(1) B(3) B(5) B(8)
 *                 /
 *               R(7)
 * 
 * (same after rotateLeft and rotateRight)
 * 
 *)

(* For an illustration of how R/B Trees work, check out the applet at:
 * 
 * http://webpages.ull.es/users/jriera/Docencia/AVL/AVL%20tree%20applet.htm
 * 
 * Be warned! The R/B Trees in the applet are not
 * left-leaning, so they will not correspond perfectly to yours. It
 * should provide some illustration of what's going on, though.
 *)
