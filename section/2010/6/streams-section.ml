(* CS51 Spring 2010
Section 6 Notes
Week of 3/21/10 *)

(************************* Part 1: Streams ***************************)
(* Recall our definition of the stream datatype, and some
 * basic operations we defined on streams. *)
type 'a str = Cons of 'a * ('a stream)
and 'a stream = unit -> 'a str

let head(s:'a stream):'a =
  let Cons(h,_) = s () in h
;;

let tail(s:'a stream):'a stream =
  let Cons(_,t) = s () in t
;;


let rec filter p s : 'a stream =
  if p (head s) then
    (fun () -> (Cons (head s, filter p (tail s))))
  else (filter p (tail s))
;;


(* A stream of all ones will be useful: *)
let rec ones = fun () -> Cons(1, ones);;
  
;;


(* Exercise 1.1 *)
(* Define a stream, mult3, that contains all integer multiples of 3.
 * You may use the nats stream defined in lecture. *)

let rec map(f:'a->'b)(s:'a stream):'b stream =
  fun () -> Cons(f (head s), map f (tail s))
;;

let rec nats =
  (fun () -> Cons(0, map (fun x -> x + 1) nats))
;;

nats = [0, 1, 2, 3, .]

head nats = 0
head (tail nats) = head (fun () -> Cons(f (head nats), map f (tail nats))) = 1
head (tail (tail nats)) = 
  head (map f ((fun () -> Cons(f (head nats), map f (tail nats))))))

  head (fun () -> 
          let s = ((fun () -> Cons(f (head nats), map f (tail nats)))) in
            Cons (f (head s), (map f (tail s)))

 --->  f (f 0)



let mult3 = map (fun x -> x * 3) nats;;

let mult3' = filter (fun x -> (x mod 3) = 0) nats;;  	

(* Exercise 1.4 *)
(* NOTE: Out of order with printed section notes *)
(* Write a function first that takes an integer n and a stream
 * and returns a list containing the first n elements of the stream. *)
let rec first (n: int) (s: 'a stream) : 'a list =
  if n = 0 then [] 
  else (head s)::(first (n-1) (tail s));;
  
;;



(* Exercise 1.2.1 *)
(* Define a function alternate that takes the negative of every other
 * item in a stream, starting with the second. e.g., 1,1,1,1... would
 * become 1,-1,1,-1,... *)
let rec alternate (s: float stream) : float stream =
  fun () -> Cons (head s, map (fun x -> -.x) (tail s));;
  
  
	
;;

(* Exercise 1.2.2 *)
(* Define a stream that contains the alternating harmonic sequence, whose
 * nth term is (-1)^(n+1)(1/n), e.g., 1, -1/2, 1/3, -1/4... 
 * You may use nats. *)

let altharm : float stream =
  
  
;;

(* Exercise 1.3 *)
(* Write a function streammax that takes two streams and returns 
a new stream of the maximum of the first elements of s1 and s2, followed 
by the maximum of the second element of each, and so on. (For example, 
s1 and s2 might represent simultaneous rolls of two dice, and we want a 
stream representing the maximum in each pair of rolls.) *)

let rec streammax (s1: int stream) (s2: int stream) : int stream =







;;

(* Exercise 1.5 - Collatz Conjecture *)
(* Consider the following procedure:
 * Take an integer. If the integer is odd, multiply by 3 and add 1.
 * If it is even, divide it by 2. Repeat this procedure.
 * The Collatz Conjecture states that if this procedure is repeated, starting
 * with any positive integer, it will eventually reach 1.
 *
 * For more information, see this scholarly article:
 * http://en.wikipedia.org/wiki/Collatz_conjecture, or this scholarly
 * webcomic: http://www.xkcd.com/710/. *)

(* Exercise 1.5.1
 * Write a function, collatz, that takes an integer and returns an int
 * stream with the integers that result from performing the above process.
 * Since streams must be infinite according to our definition, once (or if)
 * 1 is reached, the rest of the stream should be ones. *)

let rec collatz (n: int) : int stream =








;;

(* We can define a stream of streams with the collatz streams starting
 * with each natural number. *)

let collatzes = map (fun n -> collatz n) nats;;

(* And a predicate that determines if a stream contains 1. *)

let rec hasone s =
  if head s = 1 then true else hasone (tail s)
;;

(* Now consider the following definition: 
 * let collatzproof = filter (fun s -> not (hasone s)) collatzes;;
 * collatzproof is then a stream of streams which contains any collatz stream
 * that does not contain the number 1. If the stream collatzproof has an 
 * element, the Collatz conjecture is false. If it is empty, the conjecture
 * is true. *)

(* Exercise 1.5.2 *)
(* Why can't we use this to prove or disprove the Collatz conjecture? *) 


(************************* Part 2: Memoization ***************************)
(* Every time we take elements of a stream, they must be recomputed. This
 * results in extra processing time. A solution to this is memoization. *)

(* Exercise 2.1 *)
(* Consider the following stream. *)
let rec randstream = 
  (fun () -> let random = Random.int(100) in Cons(random, randstream));;
let a = head randstream;;
let b = head randstream;;
(* Are a and b necessarily equal? Why or why not? What if we use
 * memoized streams? *)






(* Exercise 2.2 *)
(* What's the tradeoff? When might we might not want to memoize? *)
 
