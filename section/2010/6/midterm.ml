let rec reduce f u xs = 
  match xs with 
  | [] -> u
  | h::t -> f h (reduce f u t)

let rec g xs = 
     match xs with 
     | [] -> 0
     | (h1,h2)::t -> (h1 h2) + (g t)
;;
('b->int,'b) list

let g xs = reduce (fun x r -> let (h1,h2) =x in (h1 h2) + r) 0 xs;;

let rec g xs = 
  match xs with 
    | [] -> None
    | h::t -> (match g t with 
                 | None -> Some h
                 | Some x -> Some x)

let rec g xs = 
  match xs with
    | [] -> (fun x -> x)
    | h::t -> fun x -> h((g t) x)


type 'a trinary_tree = Empty | Binary of 'a * 'a trinary_tree * 'a trinary_tree
                       | Ternary of ...

type a = int;;
type mylist = Empty | MyCons of a * mylist;;




