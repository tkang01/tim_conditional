(* CS51 Section 4 *)
(* Week of 2/28/10 *)

(* The purpose of this section is to give you the tools to understand and
 * compare the time and space requirements for different algorithms. Having a
 * deeper understanding of these trade-offs will help you make better design
 * choices.
 *)

(******************  Part 1: Tail-calls  ******************)
(* In languages like ML, tail calls allow us to allocate stack space more
 * efficiently. Normally, space is allocated for every function that is called
 * and is not deallocated until that function has fully evaluated. These
 * function calls are very expensive in terms of both time and space. In the
 * particular case where the function returns another function call, there is
 * no need to keep the information from original function around; we've already
 * finished using all of the information we need. Thus, if we plan ahead we can
 * write recursive functions that use constant space and spend much less time
 * switching between functions (shuffling frame pointers) on the stack! *)

(* Here we see two versions of "reduce": foldl, which utilizes this tail call
 * optimization, and foldr, which does not. *)
let rec foldr f u xs =
  match xs with
  | [] -> u
  | x::rest -> f x (foldr f u rest) ;;

let rec foldl f u xs =
  match xs with
  | [] -> u
  | x::rest -> foldl f (f x u) rest ;;

(* Which one is our usual reduce? *)

(* In the following examples, what would expressions 1 and 2 evaluate to? *)
let nums = [1;2;3;4] ;;

(* Expression 1 *)
foldr (+) 0 nums ;;

(* Expression 2 *)
foldl (+) 0 nums ;;

(* In that example the result was the same in both cases, but this is not
 * always the case. Determine the result of the following expressions! What 
 * are the functions doing and is the result the same in both cases? *)

(* 1.1 *)
foldr ( * ) 1 nums ;;
foldl ( * ) 1 nums ;;

(* 1.2 *)
let cons hd tl = hd::tl ;;

foldr cons [] nums ;;
foldl cons [] nums ;;

(* 1.3 *)
let more_nums = [[5;8;13];[2;3];[];[1;1]] ;;

foldr (@) [] more_nums ;;
foldl (@) [] more_nums ;;

(* What property should functions have for the result to be the same for both
 * foldr and foldl? *)






(**************  Part 2: Complexity and Big-O notation  **************)
(* The runtime of algorithms is often complicated and hard to
 * express. Big-O notation provides a way for us to simplify the way we
 * think about the complexity of algorithms and allows us to easily
 * compare across different algorithms. We can group algorithms with
 * the same Big-O complexity into time-complexity classes, which are
 * broad categorizations of the time it takes an algorithm to run.  *)

(* For example, take this simple algorithm for sorting a list. Each
 * step is annotated with the amount of time (number of ops) the step
 * takes:
 * 1) Count the number of items in the unsorted list. (n)
 * 2) Create a new list to hold sorted items. (c)
 * 3) Starting from the first item and going through the entire
 * unsorted list:
 *    a. Take the item at the beginning of the unsorted list. (c)
 *    b. Find the place where it should go in the sorted list. (n)
 * 
 * Thus, the runtime of this algorithm is f(n) = n^2 + n + c. (Why?)
 * What does "Big-O of f(n)" mean? *)






(* The broad time-complexity classes we're interested in are:
 *
 *   constant time      O(c) = O(1)
 *   logarithmic time   O(log n)
 *   polynomial time    O(n^c)
 *   exponential time   O(c^n)
 *
 * These are ordered from most time-efficient to least time-efficient. Why is
 * this so? Imagine graphing equations of these types, and you can see which
 * ones grow at a faster rate and eventually surpass the other equations. *)

(* Which of the following is the most efficient? Can you think of any examples
 * of algorithms that run in these times?
 *
 *   2^n
 *   log(n)
 *   n^3
 *   4n^3
 * 
 *)

(* In a binary search, we take as input a sorted array of ints and seek
 * a certain number within it. The strategy is intuitive: first you
 * check the middle element (arbitrarily choosing one of the middle
 * two elements if the array is of even length). If the middle element
 * is lower than the element you seek, you throw away the bottom half
 * of the array and recurse on the top half until you find the element
 * (or vice versa if the middle element is higher).
 *)

(* In how many steps would the binary search algorithm halt if it were to
 * search for the value 17, in the array
 * L={2,3,5,7,11,13,17,19,23,29,31}? *)


(* What is the big-O of the algorithm mentioned in the previous
 * question? Why? *)





(******************  Part 3: Recurrence Relations  ******************)
(* To extract a recurrence relation from a function that describes its runtime,
 * we often break it down according to its cases. Each case can typically be
 * represented with its own equation, where we will map constant-time operations
 * to constant values and function calls (recursive or otherwise) to applying 
 * mathematical functions. 
 *)

(* Useful recurrence relations and their solutions:

  T(n) = c                  _____________

  T(n) = c + T(n-1)         _____________

  T(n) = kn + T(n-1)       _____________

  T(n) = c + T(n/2)         _____________

  T(n) = kn + T(n/2)       _____________

  T(n) = kn + 2T(n/2)      _____________

*)

(* Make sure that you have at least an intuitive sense of why these solutions
 * are correct. You will need to be comfortable using these facts in analyzing
 * both programs that you write and those that you might be presented as part
 * of an exam or problem set question. *)

(* Answer these questions for the following examples:
 *   1) What is the recurrence relation for this function's running time?
 *   2) What is the asymptotic running time?
 *   3) Does the function utilize tail-calls?
 *   4) If not, could we easily rewrite it so it does? *)

(* 3.1 *)
let sum (lst:int list) = foldr (+) 0 lst ;;

(*
 * 1)
 * 
 * 
 * 2)
 * 
 * 
 * 3)
 * 
 * 
 * 4)
 * 
 * 
 * 
 * 
 * 
 *)

(* Does rewriting for tail recursion help the asymptotic running time? *)

(* 3.2 *)
let rec merge xlst ylst =
  match (xlst,ylst) with
    | (x::xtl,y::ytl) ->
	if x < y then x :: merge xtl ylst else y :: merge xlst ytl
    | (_,[]) -> xlst
    | ([],_) -> ylst ;;

(*
 * 1)
 * 
 * 
 * 2)
 * 
 * 
 * 3)
 * 
 * 
 * 4)
 * 
 * 
 * 
 * 
 * 
 *)

(* 3.3 *)
let split lst =
  foldl (fun x (a,b) -> (x::b,a)) ([],[]) lst ;;

(*
 * 1)
 * 
 * 
 * 2)
 * 
 * 
 * 3)
 * 
 * 
 * 4)
 * 
 * 
 * 
 * 
 * 
 *)

(* 3.4 *)
let rec mergesort (lst:int list) =
  match lst with
    | x::y::rest -> let (a,b) = split lst in merge (mergesort a) (mergesort b)
    | _ -> lst ;;

(*
 * 1)
 * 
 * 
 * 2)
 * 
 * 
 * 3)
 * 
 * 
 * 4)
 * 
 * 
 * 
 * 
 * 
 *)

(* 3.5 *)
let partition (pivot:int) (lst:int list) =
  foldl 
    (fun x (less,more) -> if x < pivot then (x::less,more) else (less,x::more)) 
    ([],[])
    lst ;;

(*
 * 1)
 * 
 * 
 * 2)
 * 
 * 
 * 3)
 * 
 * 
 * 4)
 * 
 * 
 * 
 * 
 * 
 *)

(* 3.6 *)
let rec quicksort (lst:int list) =
  match lst with
    | [] -> []
    | x::rest -> let (lows,highs) = partition x rest in
	           quicksort lows @ (x :: quicksort highs) ;;

(*
 * 1)
 * 
 * 
 * 2)
 * 
 * 
 * 3)
 * 
 * 
 * 4)
 * 
 * 
 * 
 * 
 * 
 *)

(* If you still feel uncomfortable with tail-calls, try rewriting the functions
 * above that were not tail-recursive. *)

(****************************** Part 4: Graphs ******************************)

(* Below is part of our UNDIRECTED_GRAPH signature from last week, as well
 * as one implementation of the provided functions.
 *
 * (This partial signature would not be very useful in Real Life -- why not?)
 *
 * This module represents a graph as a list of tuples,
 * each of which contains a node and a list of its neighbors
 * (the "adjacency list" representation). *)

type node = int;;

module type UNDIRECTED_GRAPH =
  sig
    type graph
    type node
    val empty : graph
    val isempty : graph -> bool
    val neighbors : graph -> node -> node list
    val choose : graph -> node
    val remove : graph -> node -> graph
  end
;;

module UndirectedIntGraph : UNDIRECTED_GRAPH =
  struct
    type graph = (node * node list) list
	(* N.B.: parsed as (node * (node list)) list. *)
    type node = int

    exception EmptyGraph
    exception IllFormedGraph

    let remove_dupls (l:'a list) : 'a list =
      reduce (fun x r -> if List.mem x r then r else x::r) [] l ;;

    let empty : graph = [] ;;
      
    let isempty (g:graph) : bool =
      match g with
	| [] -> true
	| _ -> false ;;
    
    let neighbors (g:graph) (n:node) : node list =
      match List.filter (fun (n',_) -> n' = n) g with
	| [] -> raise EmptyGraph
	| [(_,nbrs)] -> nbrs
	| _ :: _ :: _ -> raise IllFormedGraph ;;

    let choose (g:graph) : node =
      match g with
	| [] -> raise EmptyGraph
	| (n,_) :: _ -> n ;;

    let rec remove (g:graph) (n:node) : graph =
      match g with
	| [] -> []
	| (n',nbrs)::g' -> if n' = n then remove g' n
	    else (n', List.filter (fun x -> x != n) nbrs) :: remove g' n ;;
  end
;;

(* Exercise 4.1 *)
(* Write the recurrence relations for the remove function implemented above.
 * What is its Big-O running time ?*)






(* Now, we're going to write some searches on undirected graphs.
 * We want a way of representing the (ordered) set of nodes
 * we're scheduled to visit next in the search;
 * we should be able to "queue up" ("put") a node into the schedule,
 * (though it's not necessarily a queue),
 * and "get" the next scheduled node.
 *
 * We'll assume two implementations of this signature,
 * one which keeps the schedule as a stack (i.e., "put" is just cons,
 * and "get" is hd), and one as a queue (the two-list queue,
 * as shown in lecture).
 * As discussed in lecture, get is constant time for both of these
 * implementations (although this is an amortized running time for the
 * queue implementation). For both implementations, put first checks if
 * the node is already in the set to avoid duplicates. This makes the
 * put function run in linear time in the number of nodes in the set.
 *)

module type NODE_SCHEDULE = sig
  type schedule
  val empty : schedule
  val isempty : schedule -> bool
  exception EmptySet
  val get : schedule -> node * schedule
  val put : node -> schedule -> schedule
end
;;

module NodeSchedule = (* StackSchedule or QueueSchedule *);;


(* Below is a general search function that takes a graph,
 * a node to search for, and a nodeset (initially containing only one 
 * element, the starting point of the search). Take a look
 * at this code, and reason through what it does. *)

let rec search (g: UndirectedIntGraph.graph) (goal: node) 
        (to_visit: NodeSchedule.schedule) : bool =
  if NodeSchedule.isempty to_visit then false
  else
    let (curr, remaining) = NodeSchedule.get to_visit in
      curr = goal ||
        (let to_visit' = reduce NodeSchedule.put remaining (neighbors g curr) in
          search (UndirectedIntGraph.remove g curr) goal to_visit')
;;

(* Exercise 4.2
 * In what order does search visit the nodes in the graph below if it
 * is starting at node 1 and looking for node 4, if we use the StackSchedule
 * module as our schedule implementation? What if we use the QueueSchedule
 * implementation? What general search strategies do these represent?
 *
 *          1--2--10--11
 *         /    \    /
 *        3      4--5
 *       / \    /   |
 *       6--7--8    9--14
 *         / \
 *        12  13
 *)


(* Using StackSchedule, we find...
 *
 * 
 * 
 * 
 * 
 *
 *)

(* Using QueueSchedule, we find...
 *
 * 
 * 
 * 
 * 
 * 
 *)

(* These correspond in general to...
 * 
 * 
 * 
 * 
 * 
 * 
 *)



(* Exercise 4.3
 * Based on what you know about the search patterns of both search
 * implementations and the running times of the functions used, which
 * implementation seems more efficient in most cases, if either? What
 * about in the worst case? Which implementation would you use and why? *)




(* Exercise 4.4
 * What's the bottleneck in this code that's most affecting its
 * asymptotic performance? Can you find a way to rewrite this code
 * to make it asymptotically faster?  *)
