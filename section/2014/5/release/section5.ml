(*
Computer Science 51, Section 5
Tail Calls, Big-O, and PageRank

Tail Calls
----------

In languages like ML, tail calls allow us to allocate stack space more
efficiently. A function that returns the value of its recursive call is said to
be tail recursive.  Essentially, when the "last" thing a function does is call
itself recursively, we say the function is tail recursive. When a function calls
itself recursively and then does something to that result other than just
returning it, it is not tail recursive.

Normally, space is allocated for every function that is called and is not
deallocated until that function has fully evaluated. These function calls are
very expensive in terms of both time and space. In the particular case where the
function returns another function call, there is no need to keep the information
from original function around; we've already finished using all of the
information we need. Thus, if we plan ahead we can write recursive functions
that use constant space and spend much less time switching between functions
(shuffling frame pointers) on the stack!

Here we see two versions of folding over a list: foldl, which utilizes this tail
call optimization, and foldr, which does not.

*)

let rec foldr f u xs =
  match xs with
  | [] -> u
  | x::rest -> f x (foldr f u rest)

let rec foldl f u xs =
  match xs with
  | [] -> u
  | x::rest -> foldl f (f x u) rest

(*
What do the following expressions evaluate to, using foldr? What if we replace
foldr with foldl?
*)
let nums = [1;2;3;4]
let cons hd tl = hd::tl
let more_nums = [[5;8;13];[2;3];[];[1;1]];;

foldr (+) 0 nums;;
foldr ( * ) 1 nums;;
foldr cons [] nums;;
foldr (@) [] more_nums;;

(*
What property should functions have for the result to be the same for both foldr
and foldl?
*)

(*
Complexity and Big-O notation
-----------------------------

The runtime of algorithms is often complicated and hard to express. Big-O
notation provides a way for us to simplify the way we think about the complexity
of algorithms and allows us to easily compare across different algorithms.

We can group algorithms with the same Big-O complexity into time-complexity
classes, which are broad categorizations of the time it takes an algorithm to
run. The broad time-complexity classes we're interested in are:

constant: O(1)
logarithmic: O(log n)
linearithmic: O(n log n)
polynomial: O(n^c)
exponential: O(c^n)

These are ordered from most time-efficient to least time-efficient (though
linear is more efficient than linearithmic). Why is this so? Imagine graphing
equations of these types, and you can see which ones grow at a faster rate and
eventually surpass the other equations.

How would you express the following time complexities in Big-O notation? Which
is the most efficient? Which is the least efficient? Can you think of any
examples of algorithms that run in these times?

n^3
2^n
n log n
n^2 + n
5n^3+83n^2+1567
5^n

Recurrence relations
--------------------

To extract a recurrence relation from a function that describes its runtime,
we often break it down according to its cases. Each case can typically be
represented with its own equation, where we will map constant-time operations
to constant values and function calls (recursive or otherwise) to applying
mathematical functions.

These are some useful recurrence relations and their solutions.
Make sure that you have at least an intuitive sense of why these solutions
are correct.

    T(n)     | Big-O
-------------------------
c            | O(c)
c + T(n-1)   | O(n)
kn + T(n-1)  | O(n^2)
c + T(n/2)   | O(log n)
kn + T(n/2)  | O(n)
kn + 2T(n/2) | O(n log n)


Analyzing functions
-------------------

For the following functions:
1) What is the asymptotic running time?
2) Write the recurrence relation for its running time.
3) Does the function utilize tail-calls? If not, could we easily rewrite
   it so it does?
*)

let split lst = foldl (fun x (a,b) -> (x::b,a)) ([],[]) lst


(*
Remember merge from problem set 1? It takes two ordered lists and combines them
into one ordered list. It's runtime is O(n+m), where n and m are the lengths of
the lists being merged. Here, we use merge to implement mergesort.
*)

let rec mergesort (lst:int list) =
  match lst with
    | [] | [_] -> lst
    | _::_::_ -> let (a,b) = split lst in merge (mergesort a) (mergesort b)


(*
Partition takes one list and splits it into two. The first list contains only
elements smaller than some number x, and the second list contains only elements
greater than or equal to x. Its runtime is O(n), where n is the length of the
input list. Here, we use partition to implement quicksort.
*)

let rec quicksort (lst:int list) =
  match lst with
    | [] -> []
    | x::rest -> let (lows,highs) = partition x rest in
	           quicksort lows @ (x :: quicksort highs)

(*
PageRank
--------

This section describes the QuantumRanker version of the PageRank algorithm
you're implementing in Moogle. The key insight is that the links to a page can
indicate the importance of that page. Furthermore, links from important pages
are better indicators than links from less important pages. This algorithm
captures all of this information.

PageRank can be thought of as a model of user behavior. We assume there is a
"random surfer" who is given a web page at random and keeps clicking on links,
never hitting "back" but eventually gets bored and starts on another random
page.  The probability that the random surfer visits a page is its PageRank.

To clarify, each page is associated with the probability that it will be
accessed by the random surfer. This can serve as a measure of the page's
relative importance because the random surfer would be more likely to stumble
upon a page that has lots of links from other important sites.

Overview of the algorithm:

1) Every page starts with equal probability of being reached. At this point we
have not looked at any links! If there are ten pages on the internet then each
one will start with the initial probability of 1/10.

2) The random surfer follows a link with probability (1-alpha) and jumps to a
random new page with probability alpha. Thus, the probability of arriving at a
particular page is:
alpha * P(arrive randomly) + (1-alpha) * P(arrive through a link)

3) To handle the case of nodes that have no outgoing edges, assume that each
node has an implicit edge to itself.

The iterative algorithm in equations:

P(p_i; 0) = 1/N
P(p_i; t+1) = alpha/N + (1-alpha) *
    (sum of P(p_j;t)/L(p_j) for all pages p_j that link to p_i)

Keep repeating step 2 until all of the probabilities converge to something close
to their true value. It shouldn't take too many iterations for the probabilities
to become extremely stable.

Definition of terms:

p_i and p_j are pages on the internet.

P(p_i; t) is the probibility of stumbling upon page p_i after t iterations
of this algorithm.

N is the number of pages on the internet (i.e. the number of pages you have
crawled).

L(p_j) is the number of outgoing links from page p_j, including implicit link to
itself.

alpha is the damping factor that makes this algorithm converge. It is generally
agreed upon that alpha should be around 0.15.

*)
