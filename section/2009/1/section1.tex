% CS51 Section Notes, Spring 2009
% Professors Greg Morrisett and Ramin Zabeh

\documentclass{article}

\usepackage{fullpage}

\title{Section Notes 1} \author{CS51---Spring 2009} \date{Week of
  February 2, 2009}

\begin{document}
\setlength{\pdfpagewidth}{8.5 in} % invert these comments
\setlength{\pdfpageheight}{11 in} % for normal LaTeX
%\special{papersize=8.5in,11in}     %

\maketitle

\section*{Outline}

\begin{enumerate}
\item Intro to Scheme syntax
\item Evaluating expressions
\item Conditionals and special forms
\item Useful definitions
\item Lists
\item Recursion
\end{enumerate}

At the end of this section, you should be able to understand how Scheme
evaluates expressions, predict what simple functions will evaluate to
given certain arguments, and write functions that include control flow.

\section{Welcome to Scheme}

The programming language you use affects the way you think about
programming.  Consequently, learning different kinds of programming
languages is a good way to deepen your understanding. More
practically, programming languages are tools, and like any good
craftsman, a good programmer must have a well-developed set of tools
from which to choose.

The most frequently used programming languages these days have complex
syntax, taking many pages or even whole books to describe. Not so with
Scheme.  In Scheme, programs consist of \emph{expressions}. And a
Scheme expression is one of two things:

\begin{itemize}
\item An \emph{atom}, which is a simple thing like an identifier, string,
  or number.
\item A parenthesized list of Scheme expressions
\end{itemize}

%We should define a scheme list  (i.e. in the process explain cons cells and nil (empty).
%then we can easily segue into car and cdr.

Notice that this definition is recursive, allowing lists to contain
other lists as well as atoms.  Also, we should note that Scheme is a
lot more lenient than most languages in terms of which identifier names; 
they can contain almost any character, like {\tt !}, {\tt ?}, etc.  
You can give identifiers a definition with  {\tt define}. For instance,

\begin{verbatim}
> (define x 2)
\end{verbatim}
defines the identifier {\tt x} to be {\tt 2}. We can define functions as well:

\begin{verbatim}
> (define (square x) (* x x)) 
\end{verbatim}

Revel in the elegance of this syntax: we always tell {\tt define} which identifier
 we want to set, and then what we want to set it
to. In this case it's like we're saying that, in general, the
expression {\tt (square x)} should have the value {\tt (* x x)}, which
is just what we mean.

Don't be fooled by this simplicity: Scheme is not a toy language
(although it can be fun).  It is incredibly powerful and has been used
to write very cool ``real programs''.  In computer science, as with other fields,
the most sophisticated systems are built on the simplest of primitives.  
We will see examples of this throughout this class.

\section{Evaluation}

We run a Scheme program by \emph{evaluating} an expression using a
Scheme interpreter.  We've said that the Scheme interpreter is
conceptually a simple program, since all it does is apply the same
simple rule to expressions, over and over again for each expression
you give it. This rule is called the \emph{evaluation rule}, and can
be summarized as follows:

\begin{enumerate}
\item If the expression is a constant (e.g. a number), return its
  value.
\item If the expression is an identifier, return its definition (or error
  if none exists).
\item If the expression is a list, first evaluate everything in the
  list.  Then \emph{apply} the function represented by the first
  element in the list to its arguments, which are the rest of the
  list.
\end{enumerate}

For instance, to evaluate
\begin{verbatim}
> (square 2)
\end{verbatim}

Scheme firsts sees that the entire expression is a list, and applies rule 3, which evaluates each item in the list.  The first item in the list is the identifier {\tt square}.  Hence by rule 2, Scheme returns the definition and applies it to the rest of the list, which consists of the number {\tt 2}, yielding a final result of {\tt 4}.  

Make sure you understand these evaluation rules.  The {\tt square} function is intuitively obvious, but what do you think would happen if we try to evaluate a list where the first element is not a function, as in trying to evaluate {\tt (1 2 3)} ?

\section{Conditionals and special forms}

Like any truly great rule, the Scheme evaluation rule has exceptions.
Before stating what these are, let's try to see why we might need
them.  Consider the conditional expression

\begin{verbatim}
> (if (zero? denom) 0 (/ num denom))
\end{verbatim}

If we treat {\tt if} as a normal identifier and try to evaluate this expression, 
rule 3 says that we must first evaluate everything in the list.  Why is this behavior problematic here?

{\tt if} does not always evaluate all of its arguments, and because of this, 
is called a {\it special form}.  We call {\tt zero?} a {\it predicate}, and Scheme provides
other predicates for checking other conditions, like if a value is a number ({\tt number?}) 
or an integer ({\tt integer?}).  A predicate is just a function that returns true or false,
so you can easily write your own predicates -- can you give an example?

Actually, {\tt if} is defined for all values, not just true and false.  
Values are considered to be true if they aren't false.  For instance, 

\begin{verbatim}
    (if x (display "yes") (display "no"))
\end{verbatim}

\noindent prints yes for {\it every} value except for false. That is, it
prints yes when applied to {\tt "hi"}.  

{\tt and} and {\tt or} are also available, and like their counterparts
in most programming languages, perform ``short circuit'' evaluation,
only evaluating arguments as far as necessary to determine the truth
value of the expression. For this reason, {\tt and} and {\tt or} are
frequently used for control flow as well, meaning, respectively,
``while the arguments evaluate to true'' and ``until an argument evaluates to true''.

\section{Useful Definitions}

\subsection{Equality}
Scheme has several different notions of equality, which can take a
little while to get used to. For now we will focus on {\tt equal?} and
{\tt =}.

\begin{itemize}
\item {\tt equal?} is able to compare any two values. It is true if and only
if its arguments are equal in the sense that they would look the same
when printed.

\begin{verbatim}
    > (equal? "hello" "hello") 
    #t 
    > (equal? (+ 1 2) (+ 2 1)) 
    #t 
    > (equal? 2 "two")
    #f
\end{verbatim}

Note that {\tt equal?} does not care whether it \emph{makes sense} to
compare its two arguments; every pair of values is either equal or
not.

\item {\tt =} is for numerical equality. How is it different from {\tt
  equal?}?  If you only compare numbers, you would not notice a
difference. However, {\tt =} requires that its arguments be numbers:

\begin{verbatim}
> (= 2 "hi") =: expects type <number> as 2nd argument, given: "hi";
 other arguments were: 2
\end{verbatim}

What are the implications of this behavior? When should you use {\tt
  =}?
\end{itemize}

\subsection{display and begin}

The function {\tt display} prints its arguments. You should notice
that something feels different about this function: we use it not for
the value it returns, but for something it \emph{does}. Such extra
actions are called {\it side effects}.

When using a function like {\tt display}, you often need to explicitly
sequence evaluations, for example to print something then return some
other value. Scheme provides {\tt begin} for this purpose. {\tt begin}
evaluates its argument expressions in order and returns the value of
the last one.  For instance,

\begin{verbatim}
(begin (display ``foo'') 2)
\end{verbatim}

\noindent prints ``foo'' and then returns {\tt 2}.

\section{Lists}

Scheme provides the {\tt list} function for constructing lists from arguments.  For example:

\begin{verbatim}
> (define my-courses (list "CS51" "EC10" "Expos" "Life Sciences 1a"))
\end{verbatim}

\noindent This statement sets {\tt my-courses} to a list containing the four strings.  The functions {\tt car} and {\tt cdr} allow us to access parts of a list.  When we apply {\tt car} to a list, we get back the first element of that list.    When we apply {\tt cdr}, we get the rest of the list.  For example, using {\tt my-courses} from above:

\begin{verbatim}
> (car my-courses)"CS51"> (cdr my-courses)(list "EC10" "Expos" "Life Sciences 1a")
\end{verbatim}

\noindent A very important function in Scheme is {\tt cons}.  You will learn much more about {\tt cons} later in the course, but for now, think of {\tt cons} as a function that adds an element to the front of a list.  For example, again using {\tt my-courses} from above:

\begin{verbatim}
> (cons "Justice" my-courses)(list "Justice" "CS51" "EC10" "Expos" "Life Sciences 1a")
\end{verbatim}

\noindent We can represent a list with no elements in two ways: {\tt (list )} or {\tt empty}.
Scheme also provides predicates to test if a list is empty and to test if something is a list:

\begin{verbatim}
> (empty? (list ) )
true
> (empty? (list 1 2))
false
> (list? "hi")
false
> (list? (list 1))
true
\end{verbatim}

\noindent We can store anything in a list, including other lists, as in:

\begin{verbatim}
(list "hi" (list 1 2))
\end{verbatim}

\noindent Make sure you understand that this list has only two elements, "hi" and the list consisting of 1 and 2.  

Lists are simple and elegant, especially when combined with recursion.  Suppose we want to create a list with $n$ llamas. Our base case is a list with 0 llamas - the empty list.  The recursive case adds a llama to a list of $n-1$ llamas.  In Scheme: 

\begin{verbatim}
> (define (list-of-llamas n) (if (= n 0) empty (cons "llama" (list-of-llamas (- n 1)))))
> (list-of-llamas  0)
empty
> (list-of-llamas  1)
(list "llama")
> (list-of-llamas 4)
(list "llama" "llama" "llama" "llama")
\end{verbatim}

\section{Recursion}

Make no mistake about it, you will be writing a lot of recursive
functions in this course. It is always worth remembering that a proper
recursive function must have:

\begin{itemize}
\item A base case that deals with the simplest (smallest) input the
  function can take.
\item A recursive case that breaks the problem down, calls the
  function on the subproblems, and combines the results.
\end{itemize}

For instance, we can define odd as

\begin{verbatim}
> (define (odd? x)
        (cond   ((= x 0) #f)
                ((= x 1) #t)
                (else (odd? (- x 2)))))
\end{verbatim}

Actually, there's a mistake in this definition.  It will evaluate forever when
given a negative input (can you see why?).  Insufficient testing of inputs is actually one of the most
common bugs, so we will encourage and require thorough testing for ``corner cases.''
  
Besides {\tt =}, what functions have we seen might be useful for detecting a base
case?

\subsection{The Towers of Hanoi}

The classic Towers of Hanoi puzzle lends itself nicely to a recursive
solution. In this puzzle, you are given a stack of different-sized
disks (piled in size order with the largest on the bottom) on one of
three pegs. The object of the game is to move the disks from one peg
to another, observing the rules that 1) you may only move one disk at
a time, and 2) a larger disk may never be on top of a smaller disk.

It turns out that this puzzle can be solved for any number of disks,
though doing so may require a prohibitive number of moves! Let's write
a Scheme function that simulates an easier version of the game, 
where we just want to know how many moves
are required for $n$ disks.  \\

First of all, what is the base case?  \\

Since we know we can handle the base case, we're allowed to assume
that we know many moves it takes to move $n-1$ disks.  The recursive solution to
Towers of Hanoi breaks the problem down by moving the top $n-1$ disks
first, then moving the bottom disk, then moving the top $n-1$ disks
onto the bottom disk in its new location.

The Scheme function {\tt towers} is listed below. 
It's input is the number of disks to move.

\begin{verbatim}
> (define (towers n) 
    (if (<= n 1) 
      1 
     (+ (towers (- n 1)) (towers 1) (towers (- n 1)))))
\end{verbatim}

Points to observe and discuss:

\begin{itemize}
\item Pegs are represented simply as integers. If the simple solution
  works, go for it.
\item The conditional test, true, and false branches of the {\tt if} are written
  on separate lines for readability.  
\item Why did we use {\tt <=} instead of {\tt =}?
\item What numerical function does {\tt towers} compute? Is this a
  good way to compute that function?
\end{itemize}

How would this solution change if we wanted to print out a human readable description
of the moves required, rather than just compute how many steps the solution takes?  Hint: 
we will need to add additional arguments.
%, the number of the peg to move
%them from, the number of the peg to move them to, and the number of
%the auxillary peg to use in intermediate steps

%     (+ (towers (- n 1) from aux to) (towers 1 from to aux) (towers (- n 1) aux to from))))

Next, imagine we want to display the solution graphically. Will our
data representation need to change? Review top-down design by thinking
about what functions you would write to draw the puzzle solution
process.

\end{document}
