% CS51 Section Notes, Spring 2009
% Professors Greg Morrisett and Ramin Zabih

\documentclass{article}

\usepackage{fullpage}

\title{Section Notes 9} \author{CS51---Spring 2009} \date{Week of
  April 12, 2009}

\begin{document}
\setlength{\pdfpagewidth}{8.5 in} % invert these comments
\setlength{\pdfpageheight}{11 in} % for normal LaTeX
%\special{papersize=8.5in,11in}     %

\maketitle

\section{Outline}

\begin{enumerate}
\item Macros
\item Lazy Lists
\item Least squares fitting
\item Gradient descent
\item Mutation and aliasing
\end{enumerate}

At the end of this section, you should be able to write simple
macros, understand streams, and understand some of the concepts behind
fitting 2D data to the line of best fit.

\section{Macros}

Recall John Scheme, and his attempt to rewrite {\tt or} in the
following way:

\begin{verbatim}
 (define (my-or val1 val2) (if val1 #t val2))
\end{verbatim}

Why didn't his version work? Because the Scheme interpreter will
evaluate val1 and val2 as they get passed into {\tt my-or}, as opposed
to {\tt or}, which properly delays evaluation.

{\tt or} is a special-form in Scheme, as are many of the other
built-in functions we've worked with, such as {\tt and} and {\tt cond}
and {\tt if}, which all have special evaluation rules that are
different from the typical order in which Scheme evaluates functions.

If we want to delay evaluation of certain functions, we can define a
macro in Scheme. Defining a macro is like defining a function, but
instead of immediately evaluating the arguments, as with regular
functions, it will replace the syntax around the arguments with
whatever is specified in the definition, then evaluate it. Macros are
defined as follows:

\begin{verbatim}
 (define-syntax-rule (NAME [ARG...])
  (DEFINITION incorporating the ARGS provided)
\end{verbatim}
Use {\tt ...} in place of ARGS to specify an unknown number of arguments.

Macros do {\em syntactic} replacement of the parameters with the
structure in the definition.  A fixed definition of my-or:

\begin{verbatim}
(define-syntax-rule (my-or-fixed val1 val2) (if val1 #t val2))
\end{verbatim}

Here's a mental model for thinking about how Scheme handles macros:
\begin{enumerate}
\item Find all the calls to macros in the program.  
\item Replace each call with the body of
  the macro, replacing the arguments with the {\em expressions} that
  are being passed as parameters.\footnote{There is a bit more magic
    about replacing duplicated variable names, but this is pretty
    close.  See the lecture notes for an example.}
\item Now that there aren't any macros, run the program using the
  usual Scheme evaluation rules that we know and love.
\end{enumerate}

Thus, a call to {\tt (my-or-fixed (= a 0) (/ a (- 2 2)))} will take the arguments and
replace the val1 and val2 arguments to the macro with the expressions
passed: {\tt (if (= a 0) \#t (/ a (- 2 2)))}.  Then it'll use the
rules for {\tt if}, and thus avoid evaluating the division by 0.

\subsection{Macro examples}

Define a macro to increment a variable by a certain value.

\begin{verbatim}
; Here is an example in use:
(define x 5)
  (+= x 2)
; x => 7

(define-syntax-rule (+= variable value)
    (
\end{verbatim}
\vspace{1.5cm}

Define a macro to add one or more values to a variable.
\begin{verbatim}
; Here is an example in use:
(define x 2)
(+= x 7)
; x  => 9
(+= x 3 4)
;  x  => 16

(define-syntax-rule (+= variable value ...)
    (
	
\end{verbatim}
\vspace{1.5cm}

Define a macro for swapping the contents of two variables.
\begin{verbatim}
(define-syntax-rule (swap x y)
	(
\end{verbatim}
\vspace{1.5cm}

Define a macro for the while-loop.
\begin{verbatim}
(define-syntax-rule (while e1 e2 ...)
  (letrec 
\end{verbatim}
\vspace{1.5cm}

\section{Lazy Lists or Streams}

A stream consists of 1) a current item, and 2) a ``promise'' to produce the
rest of the stream on request. In Scheme, a stream is a cons cell whose
cdr is a function that returns a stream. Note the similarity
to the definition of a list. The critical difference is that the cdr is not
itself a stream, but an unevaluated \emph{lambda}. This is what allows a
stream to be ``infinite'': we don't insist on keeping the whole thing around
at once; the rest of the stream is always a function call away.

\subsection{Lazy-List Macros}

For convenience, we use {\tt lcons} to create a stream and {\tt lcar}/{\tt lcdr} to 
retrieve elements of the stream, instead of using an explicit lambda function.

For your reference, we have pasted the lazy-list macros below:

\begin{verbatim}
; Wrapping e2 in a lambda causes its evaluation to be delayed.
(define-syntax-rule (lcons e1 e2))
	(mcons e1 (lambda () e2)))

; Lazy-car works the same as car.
define (lcar lcell) 
	(mcar lcell))

; Lazy-cdr needs to evaluate the lambda that lcons put in.
(define (lcdr lcell)
	(let* ([v ((mcdr lcell))])
		  ([f (lambda () v)])   ; memoize the result
		(begin (set-mcdr! lcell f) v)))
\end{verbatim}

Note that we used mutable functions (mcons, etc) to define the lazy-list macros, so we can
take advantage of {\em memoization}, in which we remember the result of a computation instead
of re-computing it each time.

Finally, we also have lazy-list functions, such as {\tt lmap} and {\tt
  lfilter}, that use the lazy-list macros, which work like their
corresponding non-lazy-list functions.

{\tt zip} combines 2 infinite lists, and builds off of lazy-list macros as well:
\begin{verbatim}
(define (lzip f xs ys)
  (lcons (f (lcar xs) (lcar ys))
         (lzip f (lcdr xs) (lcdr ys))))
\end{verbatim}



\subsection{Examples}

Write a lazy list of multiples of 3.  You can assume the {\tt nats}
stream of the natural numbers is defined. 

\begin{verbatim}
(define 3-mult
  (
\end{verbatim}
\vspace{1.5cm}
 
Now let's think about how to actually define {\tt nats}: Without
looking at the answers, try to define the list of all natural numbers
starting from 0.

\begin{verbatim}
(define nats 
  (
\end{verbatim}
\vspace{1.5cm}
  
Write a function {\tt (stream-max s1 s2)} that takes two streams and
returns a new stream of the maximum of the first elements of s1 and
s2, followed by the maximum of the second element of each, and so
on. (For example, s1 and s2 might represent simultaneous rolls of two
dice, and we want a stream representing the maximum in each pair of
rolls.) You may assume that both streams are infinite.

\begin{verbatim}
(stream-max s1 s2)
  (
\end{verbatim}
\vspace{1.5cm}

Write a function {\tt (stream->list stream n)} that returns the first
n elements of the stream as a list. {\tt stream->list} must run in O(n).

\begin{verbatim}
(define (stream->list stream n)
  (
\end{verbatim}
\vspace{1.5cm}

The function $e^x$ may be computed by the power series
\[\sum_{i = 0} \frac{x^i}{i!}.\]

Write a function {\tt (e-stream x)} that
returns a stream of approximations for $e^{|x|}$ like
$1, 1 + x, 1 + x + \frac{x^2}{2},\ldots$,
in which each successive approximation contains one more term of the
power series.  

\begin{verbatim}
(define (e-stream x)
  (
\end{verbatim}

\vspace{5cm}


\section{Least squares}

We often want to use computers to process data that came from the
``real world'' somehow--measured, guessed, typed in by users, etc.
Such data invariably has imperfections of various sorts, and we'll be
talking about various ways to deal with them over the next few weeks.
We're starting with the simplest data model: fitting a line to a set
of 2D points.

The model we're using for least squares is straight lines.  Note that
in general, other models are possible.  Remember plotting and
estimating runtimes in PS4?  We used higher degree polynomials and
exponential functions there.  However, it turns out that things are
easier in the linear case, so we'll stick to that for now.

So, the problem we're trying to solve: given a set of data points
$(x_i,y_i)$, we want to find a ``good'' line $y=mx+b$\footnote{Recall
  from lecture that this way of representing lines is actually pretty
  bad.  We'll use it anyway because it's simple} that fits those
points.  Least squared defines an error function that specifies how
``good'' a particular line is.  Not surprisingly given the name, the
error function is the sum of squared residuals:

\[Err(m,b) = \sum_i (y_i - (mx_i+b))^2\]

To find the best line, we need to find the parameters that minimize
the error.  For the least squares error function, it turns out
that there's actually a closed form solution that can be derived.
However, we'll use gradient descent instead, because it's a very
useful general method that applies when there is no known closed form
solution.


\section{Gradient descent}

One very convenient property of the least squares error function
defined above is that it is {\em convex}--it has a single global
minimum, and smoothly approaches it from all sides, with no other
``dips''.  This makes it possible to ``walk downhill'' to find the
minimum, and be sure that we won't get stuck.  We can start at some
initial values for the parameters, and then try adjusting them a bit
in each direction to see which direction makes the function smaller.
If neither direction makes it smaller, we're done. (Why?)  Otherwise,
we move in the direction of decreasing function value and keep going.

Here is one way to implement gradient descent for a 1D function:

\begin{verbatim}
; Do gradient descent
(define (minimize f delta)
  (letrec ([start 1.0]
           [loop (lambda (cur)
                   (cond [(< (f (+ cur delta)) (f cur))
                          (loop (+ cur delta))]
                         [(< (f (- cur delta)) (f cur))
                          (loop (- cur delta))]
                         [else cur]))])
    (loop start)))
\end{verbatim}

What if the function we want to minimize is not convex?  How can we
change the algorithm a bit so it works on a wider class of functions?
(Hint: we may need to give up on having a guarantee that it will
always find the global minimum.)

\vspace{1.5cm}

There are some functions that require exhaustive search to find the
minimum: For example, imagine that you had a 2D function that was
totally flat over the entire domain\footnote{Say, Kansas}, with a
single low dip \footnote{Water well?} somewhere.  Unless you have some
extra info about the structure of the function, there is no better way
to find the lowest point except to search the entire space\footnote{An
  excuse for an exploratory bike trip?}.

\paragraph*{Exercise:}

Given the {\tt miminize} definition above, write {\tt (maximize f
  delta)} that maximizes a convex function.

\vspace{1.5cm}

\paragraph*{Exercise:}

Write a {\tt my-sqrt} function that uses {\tt minimize} to find the
square root of a number to a given precision:

\begin{verbatim}
(define (my-sqrt n delta)
   (
\end{verbatim}
\vspace{1.5cm}

\section{Mutation and Aliasing}

We're now switching gears to talk about mutation and aliasing, since
this will come up in the project. As we've already mentioned, mutation
often makes reasoning about programs harder.  Here, we'd like to
briefly talk about the problems caused by {\em aliasing}.  Aliasing
happens when the same mutable data structure is referenced from
multiple locations.  This means that changes in one part of your
program can affect other parts in non-obvious ways.

A simple example:

\begin{verbatim}
; Make lst a mutable cons cell
(define lst (mcons 1 2))

; lst2 points to the _same_ cell
(define lst2 lst)

(mcar lst)  ; returns 1
(mcar lst2)  ; returns 1 (*)

(set-mcar! lst 42) ; change the car of the first list

(mcar lst) ; returns 42, which totally makes sense
(mcar lst2) ; also returns 42!  A bit more surprising, since we
            ; haven't directly changed lst2 since the call above in (*)
  
\end{verbatim}

Why would someone write such silly code, you may ask.  Well, here's a
slightly more realistic example from Project 2:

Inside the game engine, we have something more or less like this:
\begin{verbatim}
; Called whenever a powerup reappears on the graph
(define (appear powerup node)
  ; Change the powerup state to reflect its new status and location
  (set-powerup-visible! powerup \#t)
  (set-powerup-location! powerup node)

  ; also change its size
  (set-powerup-size! powerup (+ alpha (powerup-size powerup)))

  ; Add it to the list we'll pass to collect-data later
  (set! new-powerups (cons (cons node powerup) new-powerups)))

...

(define (step)
   (if (not (empty? new-powerups))
     (begin 
       ; tell the agent about the newly appeared powerups
       (send student-agent collect-data new-powerups)
       (set! new-powerups empty))  ; reset the list
     (void)  ; otherwise just don't do anything
))
\end{verbatim}

The powerup agents periodically appear and disappear, and change
internal state such as location and size over time.  The collect-data
function that you have to write periodically gets called with a list
of newly appeared powerups.  Imagine that you tried writing it like
this:

\begin{verbatim}
; ****** NOTE: BROKEN CODE ******

(define powerup-history empty)

(define (collect-data powerups)
  ; add each (node . powerup) pair to our history list
  (map (lambda (x)
         (set! powerup-history (cons x (powerup-history))))
       powerups)
  
; and then try to use the history later on:

; return the list of powerup sizes at a particular node.
(define (get-sizes node)
  (map powerup-size 
    (filter (lambda (x) (equal? (car x) node)) powerup-history)))
\end{verbatim}

This all seems perfectly reasonable, right?  We store all the powerups
we've seen, and then get a list of sizes at a node by extracting just
the ones that appeared at that node and getting their size.  What goes
wrong?  Do you see how this is similar to the simplified example above?

%\section{Summary}

%%  Is there a summary?  What _did_ we learn?  Again, how is it
%%  connected to the larger picture?

%%   If the students only learn 3 things from this section, what should
%%   they be? 

% Can we actually do what we promised we'd be able to at this point?

%\section{Further reading}
%%  What are some resources for looking up more advanced material
%%  related to these topics?

%% SICP 

%% There are lots and lots of resources out there about fitting models to
%% data.  The wikipedia page for least squares is a reasonable place to
%% start, though it quickly gets complicated :)

\section{Answers}

%% Are there answers to all the problems? (In the back)  Are they
%% correct? :)

\begin{itemize}

\item
\begin{verbatim} (define-syntax-rule (+= variable value)
  (set! variable (+= variable value)))
\end{verbatim} 

\item
\begin{verbatim} (define-syntax-rule (+= variable value ...)
  (set! variable (+ variable value ...)))
\end{verbatim}

\item
\begin{verbatim}(define-syntax-rule (swap x y)
  (let ([tmp x])
    (set! x y)
    (set! y tmp)))
\end{verbatim}

\item
\begin{verbatim}(define-syntax-rule (while e1 e2 ...)
  (letrec ([loop (lambda ()
                   (if e1 (begin e2 ... (loop)) (void)))])
    (loop)))
\end{verbatim}

\item
\begin{verbatim} (define 3-mult 
	(lfilter (lambda (x) (= (modulo 3 x) 0)) nats))
\end{verbatim}

\item
\begin{verbatim} (define nats 
	(lcons 0 (lmap (lambda (z) (+ 1 z)) nats)))
\end{verbatim}

\item
\begin{verbatim} 
(define (stream-max s1 s2)
   (if (> (lcar s1) (lcar s2))
       (lcons (lcar s1)
              (stream-max (lcdr s1) (lcdr s2)))
       (lcons (lcar s2)
              (stream-max (lcdr s1) (lcdr s2)))))
\end{verbatim}

\item
\begin{verbatim}
(define (stream->list stream n)
   (if (= n 0) '()
       (cons (lcar stream)
             (stream->list (lcdr stream) (- n 1)))))
\end{verbatim}

\item
\begin{verbatim}
;; e-stream: Number -> Stream of Number
;; To find successive approximations of e^x.
;;
;; e-stream wraps to e-stream-helper, providing the initial values of
;; 1 (the first exponent to be calculated) and 1 (the previous term,
;; with exponent 0).
(define (e-stream x)
   (e-stream-helper x 1 1))

;; e-stream-helper: Number x Integer x Number -> Stream of Number
;;
;; e-stream-helper does the work of constructing the stream of
;; approximations for e^x.  It takes the value of x as an argument,
;; and it keeps track of the value of i in the formula (x^i)/(i!) and
;; the value of the previous term, both of which are needed for the
;; mathematical calculation of the term's value.
(define (e-stream-helper x i prev-term)
   (lcons prev-term
         (e-stream-helper x (+ 1 i) (e-stream-calc x i prev-term))))

;; e-stream-calc: Number x Integer x Number -> Number
;;
;; e-stream-calc actually calculates the value of a gtiven term based
;; on the formula for the power series of e^x.
(define (e-stream-calc x i prev-term)
   (+ prev-term (/ (expt x i) (factorial i))))

;; factorial: Number -> Number
;;
;; factorial returns (unsurprisingly) the factorial of its argument.
;; This is called by e-stream-calc to calculate the factorial of i.
(define (factorial n)
   (if (= 1 n) n
       (* n (factorial (- n 1)))))
\end{verbatim}	   

\item
\begin{verbatim}
  (define (maximize f delta) (minimize (lambda (x) (- (f x))) delta))
\end{verbatim}

\item 
\begin{verbatim}
(define (my-sqrt n delta)
    (minimize (lambda (x) (square (- n (square x)))) delta))
\end{verbatim}

\item {\bf Gradient descent modifications}: If the function you're
  minimizing isn't convex, but is not too crazy, gradient descent may
  still do something reasonable.  Now the answer will depend on where
  you start your search: if you have some info about where the minimum
  is likely to be, starting there can help.  Another approach is to do
  random restarts: run the algorithm with multiple starting points and
  choosing the best one.  Of course, this isn't guaranteed to find the
  global optimum, but it often does well in practice.

\item {\bf Aliasing}: the problem with the code is that the powerup-history
will have lots of references to the same powerup objects that appear
at different places over the course of the game.  This means that when
the engine code does something like {\tt (set-powerup-size! powerup...)}, 
it will change the size of the powerups referenced in your history!
So your {\tt get-sizes} function will return a list that has lots of
repeats of the same value.  This is tricky to debug, because the
changes aren't happening anywhere in your code.

\end{itemize}

\end{document}
