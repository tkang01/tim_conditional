#lang s-exp CS51

; Based on a version from http://community.schemewiki.org/?memoization
(define (memoize function) 
   (let ((table (make-hash))) 
     ; Funny lambda syntax: takes args as a list
     (lambda args 
       (dict-ref table 
                 args 
                 ;; If the entry isn't there, call the function.    
                 (lambda () 
                   (let (; Call the function
                         [result (apply function args)])
                     (dict-set! table args result) 
                     result))))))
(define (memoize2 f) f)

(define (square x) (* x x))

; Return a list of numbers from 0 to n-1
(define (up-to-n n)
  (letrec ([f (λ (x lst) (if (< x 0) lst (f (- x 1) (cons x lst))))])
    (f (- n 1) empty)))

(test (up-to-n 3) => '(0 1 2))
(test (up-to-n 0) => '())

; Do hill(down)climbing over the function f, starting at the given
; initial values, using steps of delta.  Returns a pair (x . y) for the 
; best arguments
(define (minimize2d f init-x init-y delta)
  (letrec ([loop (λ (x y) 
                (let ([fxy (f x y)])
                  (cond [(< (f (+ x delta) y) fxy) (loop (+ x delta) y)]
                        [(< (f (- x delta) y) fxy) (loop (- x delta) y)]
                        [(< (f x (+ y delta)) fxy) (loop x (+ y delta))]
                        [(< (f x (- y delta)) fxy) (loop x (- y delta))]
                        [else (cons x y)])))])
    (loop init-x init-y)))
                  

                  
; Returns the sum of the squared errors of the points from the line
; y = m*x + b.  Points represented as pairs (x . y)
(define (sum-square-error m b points)
  (foldr (λ (p err) (+ err (square
                           (- (cdr p) (+ b (* m (car p)))))))
         0 points))
  
(define points1 '((1 . 1) (2 . 2) (3 . 3)))
(test (sum-square-error 0 0 points1) => (+ 1 4 9))
(test (sum-square-error 1 0 points1) => 0)

(define-struct line-model (m b) #:transparent)

(define least-squares
  (memoize (λ (points delta)
             (let* ([init-m 1]
                    [init-b 1]
                    ; The function to optimize
                    [f (lambda (m b) (sum-square-error m b points))]
                    [best-args (minimize2d f init-m init-b delta)])
               (make-line-model (car best-args) (cdr best-args))))))

  
; zip two lists together.  If they have different length, stops 
; once either is empty (NOTE: This is different than the version
; on the midterm)
(define (zip f x y)
  (if (or (empty? x) (empty? y)) empty
      (cons (f (car x) (car y)) (zip f (cdr x) (cdr y)))))

(test (zip + '(1 2 3) '(0 1 2 3)) => '(1 3 5))

; Return a list of consecutive pairs from lst:
; (pairs '(1 2 3 4)) => '((1 . 2) (2 . 3) (3 . 4))
(define (pairs lst) 
  (if (empty? lst) empty
      (zip cons lst (cdr lst))))

(test (pairs '(1 2)) => '((1 . 2)))
(test (pairs '(1 2 3 4)) => '((1 . 2) (2 . 3) (3 . 4)))
  
  
; Return elements start through end-1 of lst
(define (slice lst start end)
  (let ([n (- end start)])
  (take (drop lst start) n)))

(test (slice '(0 1 2 3 4 5) 0 3) => '(0 1 2))
(test (slice '(0 1 2 3 4 5) 2 3) => '(2))
(test (slice '(0 1 2 3 4 5) 3 3) => '())

; Takes:
; - a list of points, sorted in increasing order by x
; - the cost for adding an extra line to our fit
; - a parameter for how accurately to compute the inner least squares
;   estimates
; Finds the set of lines that minimize (n * line-cost + square-error),
; where ranges of points are mapped to lines and there are n lines.
; Returns a list of segments, where each segment is of the form 
; (i j line), which means the points from the ith (inclusive) to jth (exclusive)
; are being fit with the given line.
(define (segmented-ls points line-cost delta)
   (letrec (; How much error we get from fitting the segment from i to j with a 
          ; single line
          [err (memoize 
                (λ (i j)
                  (let* ([segment (slice points i j)]
                         [line (least-squares segment delta)])
                    (sum-square-error (line-model-m line) (line-model-b line) segment))))]
          ; the function that figures out the lowest error cost for fitting
          ; points 0 through n.  Returns a pair (min-cost . split-index)
          [opt (memoize
                (λ (n) 
                 (if (= n 0) (cons 0 0)
                     (let* ([split-costs (map 
                                         (λ (i) (+ (err i n) line-cost 
                                                   (car (opt i))))
                                         (up-to-n n))]
                           ; [dummy (printf "split-costs= ~a\n" split-costs)]
                            [split-index
                             (argmin (λ (x) 
                                             (list-ref split-costs x))
                                    (up-to-n n))]
                            [min-cost (apply min split-costs)])
                       (printf "n=~a min-cost=~a split-point=~a\n" 
                               n min-cost split-index)
                       (cons min-cost split-index)))))]
          ; Use opts to find the optimal split, then generate the actual list of
          ; split points.
          [find-splits (λ (n)
                         (let ([s (cdr (opt n))])
                           (printf "s = ~a\n" s)
                           (if (= 0 s) (list 0 n)
                               (append (find-splits s) (list n)))))])

     (map (λ (pair) 
             (let* ([segment (slice points (car pair) (cdr pair))]
                    [line (least-squares segment delta)])
               (list (car pair) (cdr pair) line)))
          (pairs (find-splits (length points))))))


(define points2 '((1 . 1) (2 . 2) (3 . 3) (4 . 10) (5 . 10) (6 . 10)))
   
; Returns 2 segments:
;(segmented-ls points2 1 0.001)
; Returns 1 segments:
;(segmented-ls points2 20 0.001)

(require plot)

; Helper function: given a list of vectors, returns
; the max value of a given component
(define (component-max k v)
  (apply max (map (lambda (x) (vector-ref x k)) v)))

(define (segment-line segment)
  (let* ([l (caddr segment)]
         [m (line-model-m l)]
         [b (line-model-b l)])
  (line (λ (x) (+ b (* m x))))))

; Takes a list of #(n time) vectors, as produced by time-function,
; for example, and plots them.
; Optionally also takes a curve fitting function to fit to the data
; and a title for the graph
(define plot-sls 
  (lambda (pts segments 
           [title ""])
    (let ([vects (map (λ (x) (vector (car x) (cdr x))) pts)])
      (plot (mix (points vects #:color 'blue)
                 (foldr (λ (x r) (mix (segment-line x) r))
                        (segment-line (car segments))
                        (cdr segments)))
          ; this is the syntax for passing keyword arguments to functions
          #:x-min 0      
          #:y-min 0
          #:x-max (* 1.1 (component-max 0 vects))
          #:y-max (* 1.1 (component-max 1 vects))
          #:x-label "x"
          #:y-label "y"
          #:title title))))

;(plot-sls points2 (segmented-ls points2 1 0.001))
;(plot-sls points2 (segmented-ls points2 20 0.001))
;(plot-sls points2 (segmented-ls points2 0 0.001))

(define points3 '((1 . 1) (2 . 2) (3 . 3) (4 . 5.5) (5 . 7.5) (6 . 9.5) (7 . 8) (8 . 9) (9 . 10) (10 . 11) (11 . 12)))