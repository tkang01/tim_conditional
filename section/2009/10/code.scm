#lang s-exp CS51

(define (fib n)
  (cond [(= n 0) 1]
        [(= n 1) 1]
        [else (+ (fib (- n 1)) (fib (- n 2)))]))


; INSERT COMMENT HERE
;
;
;
;
(define (hash-ref! ht k c)
  (hash-ref ht k (lambda () (let ([v (if (procedure? c) (c) c)])
                              (begin (hash-set! ht k v) v)))))

(define fibht (make-hash))



(define (fib2 n)
  (cond [(= n 0) (hash-ref! fibht 0 (lambda () 1))]
        [(= n 1) (hash-ref! fibht 1 (lambda () 1))]
        [else (hash-ref! fibht n (lambda () (+ (fib2 (- n 1)) (fib2 (- n 2)))))]
        ))


(define (memoize function) 
   (let ((table (make-hash))) 
     ; Funny lambda syntax: takes args as a list
     (lambda args 
       (dict-ref table 
                 args 
                 ;; If the entry isn't there, call the function.    
                 (lambda () 
                   (let (; Call the function
                         [result (apply function args)])
                     (dict-set! table args result) 
                     result))))))



(define (add1 x)
  (begin
    (printf "Adding 1 to ~a\n" x)
    (+ 1 x)))

;> (add1 1)
;Adding 1 to 1
;2
;> (add1 1)
;Adding 1 to 1
;2

(define add1-memo (memoize add1))

; Now will only print the messages once per argument value
;> (add1-memo 1)
;Adding 1 to 1
;2
;> (add1-memo 1)
;2




; Tail recursion examples:

; sum-to-n adds up the natural number from 0 to n, inclusive, and returns the
; result.
(define (sum-to-n n)
  (if (= n 0)
      0
      (+ n (sum-to-n (- n 1)))))


(define (sum-to-n2 n)
; Why did we make the helper local?
  (letrec ([helper
            (lambda (n ac)
              (if (= n 0)
                  ac
                  (helper (- n 1) (+ n ac))))])
    (helper n 0)))


;(time (sum-to-n 1e5))
;(time (sum-to-n2 1e5))

;###########  Answers below

(define (fact n)
  (letrec ([helper
            (lambda (n ac)
              (if (= n 1)
                  ac
                  (helper (- n 1) (* n ac))))])
  (helper n 1)))


(define (fold_left f base lst)
  (if (null? lst)
      base
      (fold_left f (f (car lst) base) (cdr lst))))

(define (fold_right f base lst)
  (fold_left f base (reverse lst)))

(define (rev lst)
  (letrec ([helper
            (lambda (lst ac)
              (if (null? lst)
                  ac
                  (helper (cdr lst) (cons (car lst) ac))))])
    (helper lst '())))
