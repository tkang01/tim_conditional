#!/usr/bin/env python

usage = """make-template.py WEEK

WEEK = 1, 2, 3, etc...
"""

import sys
import os

if len(sys.argv) != 2:
    print usage
    sys.exit()

week = sys.argv[1]

cmd = """mkdir -p %s;
sed 's/WEEK=X/WEEK=%s/' < template/Makefile > %s/Makefile ;
cp template/section-template.tex %s/section%s.tex
""" % (week, week, week, week, week)

print cmd
os.system(cmd)
