% CS51 Section Notes, Spring 2009
% Professors Greg Morrisett and Ramin Zabeh

\documentclass{article}
\usepackage{graphicx}
\usepackage{fullpage}

\title{Section Notes 11} \author{CS51---Spring 2009} \date{Week of
  April 27, 2009}

\begin{document}
\setlength{\pdfpagewidth}{8.5 in} % invert these comments
\setlength{\pdfpageheight}{11 in} % for normal LaTeX
%\special{papersize=8.5in,11in}     %

\maketitle


% What material is being covered this week?

\section{Outline}

\begin{enumerate}
\item Clustering
\item Impossible Programs
\item What have we done this semester?
\end{enumerate}

By the end of this section, you should be familiar with clustering and
recognizing impossible programs.

\section{Clustering}

Clustering is the problem of dividing unlabeled data into
groups.  Because the data is usually coming from a source that doesn't
give us its ``true'' classification, this problem is somewhat
ill-specified--there isn't an obvious metric for what the ``right
answer'' is.  However, it is still a problem that we often face in the
real world, so we have to solve it anyway.  There are many approaches:
two that we're covering in CS51 are the K-means and K-centers algorithms.

\subsection{The K-means algorithm}

The K-means algorithm is based on the model that the error of our
clustering is the sum of the squared distances to the cluster centers.

For our purposes here, we'll assume that we know $k$--the number of
clusters we want\footnote{There do exist ways of picking $k$ if it's
  not known, but we won't cover them here.}.  Then, the algorithm is
extremely simple:

\begin{enumerate}
\item Pick $k$ points to be initial cluster centers.
\item Assign each point to the cluster whose center it's closest to.
\item Choose new cluster centers to be the centroid (average) of all
  the points in the cluster.
\item Repeat until no points change their cluster assignment.
\end{enumerate}

\noindent Some properties of K-means:
\begin{itemize}
\item Each repetition improves the clusters--the new ones have smaller
  error.  (Why this is true is not obvious, but it is true).
\item Will always terminate.
\item Tends to be very fast.
\item Answer depends on starting points.
\item Returned clustering may not be very good--the error function is
  not convex, so it may find a local minimum.
\end{itemize}
%\includegraphics[width=2in]{kmeans-faces.pdf}

\subsection{The K-centers algorithm}

We talked about the fact that it doesn't always make sense to use the
same error metrics when fitting lines to data.  A similar story arises
with clustering.  So, let's consider the case where we don't want to
use the sum of square errors from the cluster centers as our error metric.
Instead, we want to say that the quality of a cluster is the distance
of the {\em furthest} point from the cluster center.  This leads to
the K-centers algorithm:

\begin{enumerate}
\item Pick a point to be the center of the first cluster.
\item Pick the point that is furthest away from that first point to be
  the center of the second cluster.
\item Pick the next point that is furthest away from all the existing
  clusters to be the next center.
\item Repeat until you have $k$ clusters.
\end{enumerate}

This is also very simple, and turns out to get an error within a
factor of 2 of the optimal choice of cluster centers when using this
error metric.  

\noindent Properties of K-centers:

\begin{itemize}
\item Also fairly fast.
\item May also find local minima.  The result mentioned above bounds
  how bad it can be.
\end{itemize}

\subsection{Clustering discussion}

\begin{itemize}
\item Random restart can help find better clusters--use multiple
  starting points.
\item Use the error function for your algorithm to evaluate how good
  the result was--pick the best one.
\item There is a lot of work on trying to pick starting points more
  intelligently than just random.  It's unclear how often they end up
  being better than random restart.  (Depends on dimensionality,
  amount of data, number of clusters, etc...)
\end{itemize}



\section{Impossible Programs}

In computer science (and in the world!), there are a number of
problems that are unsolvable, no matter what kind of clever or
algorithm you write to try to solve it. The example given in lecture
was lossless compression - it is impossible to write a lossless
compression program that always works, and always produces an output
that's smaller than the input.  Because it reduces the size of a file, since you
can keep applying it to its output to shrink the file size to 0.  It
turns out that even if you allow the output size to be less than or
equal to the size of the input, it's still impossible (assuming the
program actually does compress at least one input).  This can be shown
using an argument based on the  pigeon-hole principle.

\vspace{.5cm}

The quintessential impossible program is the halting problem. The
halting problem is to write a program which determines, given another
program and an input to that program, whether the program will
eventually halt when run with that input. In this abstract framework,
there are no resource limitations of memory or time on the program's
execution; it can take arbitrarily long, and use arbitrarily amounts
of storage space before halting. The question is simply whether the
given program will ever halt on a particular input. (Adapted from
Wikipedia.)  Using a technique called diagonalization, it can be
proven that it is impossible to write a program that solves the
halting problem--details in the Lecture 21 notes.

\vspace{.5cm}

Remember reduction from Lecture 13?  There, we proved that a problem X
is as solvable as another problem Y by showing that a solution to X can be used to solve Y 
(ie, reducing Y to X).  If Y is already proven
to have a known characteristic (impossible to solve), and Y can be reduced
to X, that would mean that X has the same characteristic (impossible to
solve). Consider this "real life" example: you know that the problem
of staying awake forever (A) is impossible.  You want to figure out
the problem of how to make a pill that eliminates the need for sleep (S).  
John Scheme has told you that he has created a pill that indeed fits the criteria for S.
Wow, so in figuring out how to solve S, he has managed to solve A! 
But, like always, you should be cautious of John Scheme's schemes - 
you know that A is unsolvable,
so S must actually be impossible. 

\vspace{.5cm}

Reduction is a very useful way to prove impossibility: that is, if the
halting problem can be reduced to P, where we solve the halting
problem by solving P, then we know that P shares the proven
characterisitic of the halting program (impossible to solve). There
is, in fact, a large class of problems that the halting problem can be
reduced to; they are all impossible to solve.

\vspace{.5cm}

Knowing how to recognize provable impossible problems is
important. For example, if someone writes a program and it claims to
be able to solve one of the impossible problems, you know that there
must be something wrong with their code. Here are some problems that
we know are impossible:

\begin{itemize}
\item Whether a program halts on empty input. 

\item Whether a program returns true or false on an empty input.

\item Whether two programs are exactly equal to each other. 

\item Whether a stream will ever terminate

\item Whether a number appears in a stream

\item Whether a program will ever produce any output / some particular output

\item Whether a program computes some specific function (this is
  basically the "exactly equal" case above)

\item Whether a scheme function will have a "divide by zero" error.

\item Whether this program will make a connection to the network

\item Whether this program will ever (try to) write outside some region in memory

\item Whether this program has any bugs (not well specified as stated, but
  impossible in many instances) 
\end{itemize}

\vspace{.5cm}

In general, just about anything of the form "does this arbitrary
program do X" is impossible.
\vspace{.5cm}

\subsection{So what do we do?}

Your boss comes to you with a problem, and you recognize that it's not
possible to solve.  What to do?  All is not lost!  If a program turns
out to be impossible to write, that just means you have to relax some
assumptions. For example, you can't write a lossless compression
program that always works and never returns an output larger than its
input.  This tells you that something has to give--either make it
lossy, make it not always work, or accept that some inputs will end up
with an output that's larger (could be by just one bit!) than the
input.

There are two common approaches to the ``Does this arbitrary program
do X'' problem: one is to restrict the set of allowed programs, so
they are no longer arbitrary.  For example, it's easy to tell whether
a program that doesn't have loops or recursion halts on some input.
Another, used when you want to make sure that the program doesn't do
something ``bad'', is to modify the program to check that it doesn't
do that at runtime.  For example, while we can't tell whether an
arbitrary program will have a divide-by-zero error, we can insert some
code just before every division to do a check.

\section{Things we've learned this semester!}

This is not an exhaustive list; for an exhaustive list of topics
covered in CS51, revist all lecture slides, section notes, problem
sets, and projects - anything we have learned this semester is fair
game!


Data Structures: Graphs, Trees, Queues, PQueues 
\vspace{.5cm}

Abstraction and Contracts 
\vspace{.5cm}

Higher-Order Functions (lambda, fold, map, filter) 
\vspace{.5cm}

Time Complexity, Big-O, and Induction 
\vspace{.5cm}

Dijkstra's Algorithm 
\vspace{.5cm}

Parsing and Parse combinators 
\vspace{.5cm}

Reductions 
\vspace{.5cm}

Side Effects and Mutation 
\vspace{.5cm}

Object-Oriented Design 
\vspace{.5cm}

Lazy-List/Streams 
\vspace{.5cm}

Macros 
\vspace{.5cm}

Line-fitting (Linear Fitting, Least Squares, Least Mediam Squares) 
\vspace{.5cm}

Clustering 
\vspace{.5cm}

Impossible Programs 
\vspace{.5cm}


\end{document}
