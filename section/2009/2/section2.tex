% CS51 Section Notes, Spring 2009
% Professors Greg Morrisett and Ramin Zabeh

\documentclass{article}

\usepackage{fullpage}
\usepackage{graphicx}

\title{Section Notes 2}
\author{CS51---Spring, 2009}
\date{Week of February 8, 2009}  
  
\begin{document}
\setlength{\pdfpagewidth}{8.5 in} % invert these comments
\setlength{\pdfpageheight}{11 in} % for normal LaTeX
%\special{papersize=8.5in,11in}     %

\maketitle


\section*{Outline}

\begin{enumerate}
\item Review
\item Abstraction
\item Trees
\item Reading and writing Scheme
\item Style
\item Answers
\end{enumerate}
% What material is being covered this week?

\section{Review}
By now you should be familiar with the following concepts:
\begin{itemize}
\item The Scheme evaluation rule and exceptions to it (special forms)
\item Side effects, including variable assignment, and the fact that
  we are programming without them
\item How to write a simple recursive function
\end{itemize}

\section{Abstraction}

We hope that you will take a great deal away from CS51. However,
{\em abstraction} is a central concept that stands out among all the others we
will study this semester.

% Why is there a picture of a segway here? Well, a segway is a form of
% transportation - like a car or a bike, but it has a very different interface.
% You may find it helpful to discuss with your section a couple of
% possibilities:
% 1) Could we us the segway interface on a car? Same underlying implementation, different interface
% 2) Could a different interface have been provided to the segway? Note that
% automatic cars have 2 pedals and a steering wheel. This is their abstract interface that hides the
% details of the car, truck, front-wheel, rear-wheel, or whatever else.
\includegraphics{segway.pdf}


An {\em abstraction} in programming is a tool used to generalize code to make it
more broadly applicable. One of the first forms of abstraction that you
encountered as programmers was the ``function''. A function parameterizes a
section of code so that the same code can be applied to different inputs. While
a parameterized function is abstract in that it can be applied to different
arguments, it is also abstract in that the details of the function's
implementation are hidden.

As an example, if you took CS50, you will remember the Spell Checker problem
set where you were asked to design and implement functions that would be used
to check for spelling errors in an input file.  Each student's ``check''
function conformed to the same contract - take a string as an argument and
return true if the word appeared in the dictionary file and false otherwise.
The competition to produce the fastest implementation of these functions was
possible because the function interface effectively abstracted away the
particular implementation.

Why do we care about abstraction? The simple answer is that it makes our jobs
as programmers easier. It's much harder to reason about complex systems than
about individual units of programs that have been broken down into pieces with
well-defined boundaries. Functional interfaces and contracts are one such
boundary, but they are not the only kind that come up in software design.

\subsection{Data Abstraction}

Besides functional or control abstraction, as described above, CS51 introduces
{\em data abstraction}. Data abstraction separates the conceptual properties of
a datatype from its implementation. An example of an {\em Abstract DataType} or
{\em ADT} is a {\em Set}. Conceptually, a Set is a collection of unique values
and nothing more. This abstract definition provides no guidance as to how a Set
should be represented on a computer.  In Scheme, one might consider storing the
contents of a given Set in a list, however this is certainly not the only
possible concrete implementation of this type, and is probably a rather poor
choice of representation. Why might a list be a poor choice?  Later in this
section we will discuss binary trees, which is one of the many possible
concrete datatypes that can be used to represent the Set ADT in Scheme. 

\subsection{Representing Data Types in Scheme}

Scheme provides us with primitive types including numbers, strings, and lists,
but it also provides us with a mechanism for defining our own structured types.
Much like the struct that you have seen in C, Scheme provides a mechanism for
defining a type with named components. For example, if you wanted to write a
program that would store and manipulate student information, you might want the
following structure definition in your program:

\begin{verbatim}
(define-struct student (name gradyear house concentration gpa))
\end{verbatim}

Once you have introduced this new concrete type in Scheme, you automatically
get some help in the form of functions that create, check, and interrogate
students\footnote{Note: no students were harmed in the making of these notes.}.
For our example type, these generated functions are:

\begin{verbatim}
make-student
student?
student-name
student-gradyear
student-house
student-concentration
student-gpa
\end{verbatim}

From this representation of a student record, you could design a whole program
that allows TFs and Professors to store and retrieve information about their
students. Note, however, that this structure is still somewhat abstract in that
it does not tie us to a specific representation of the fields. For an example,
think about the various ways that you might choose to store the gpa for a
student. Over the next few months we will learn more about how PLT Scheme helps
us enforce data {\em encapsulation}, or the strict separation of a datatype's
interface from its implementation. 

For more information on data abstraction and how to work with structures in
Scheme, check out Chapter 6 of ``How To Design Programs''
(http://www.htdp.org/2003-09-26/Book/curriculum-Z-H-9.html\#node\_chap\_6) or
Chapter 5 of ``Guide: PLT Scheme''
(http://docs.plt-scheme.org/guide/define-struct.html).

\subsection{Variables as Abstractions}

The Scheme special form {\tt let} allows you to introduce
local variables. Creating local names for values lets you avoid typing a
common subexpression multiple times, and lets the interpreter avoid
evaluating it multiple times.

The \emph{bindings} you create with {\tt let} remain active within the
{\tt let} block, but nowhere else. Only the {\tt expression} below would
have access to these particular values for {\tt sym1} and {\tt sym2}:

\begin{verbatim}
    (let  (  [sym1  expr1]
             [sym2  expr2]
             ...             )
          expression)
\end{verbatim}

{\tt let} introduces a variety of interesting technicalities. For instance,
the semantics of {\tt let} say that the symbols get their values
simultaneously---in other words, you can't
express the value of one in terms of another.

Before moving on to talk about some concrete data types, it is worth noting
that Scheme's {\tt let} expression provides a form of abstraction. Recall that
{\tt let} can be used to associate identifiers with the result of evaluting
expressions. What is the result of evaluating the following expression?

\begin{verbatim}
(let ([size (if (< 1 0) "Small" "X-Large")]
      [color "Chartreuse"]
      [material "Wool"])
   (list size color material))
\end{verbatim}

While the example above is trivial, it demonstrates that the final list
expression can be written without any knowledge of how size, color or material
are computed. A more complicated example might replace the list expression
with an expression that computes the price of a sweater based on these three
parameters. What might that code look like? Is there any limit to the
complexity that we can put in our {\tt let} identifier definitions?

\section{Trees}

\subsection{Definition}
A tree is a connected graph that does not contain a cycle, and a binary tree is a tree data structure in which each node contains at most 2 children.

A tree is a data structure in-and-of-itself; however, trees can also be used to
represent other Abstract Data Types.  Dictionaries are often represented as
trees, since trees naturally allow for ordering data. Trees can also be used to
represent other undirected graphs (by picking a root node in a graph) in order
to accomplish Breadth-First Search or Depth-First Search (explained in a
following section). 

\subsection{Trees in Scheme}

\begin{figure}[htp]
\centering
\includegraphics{binarytree}
\caption{A properly-formed binary search tree}\label{fig:binarytree}
\end{figure}

How can we depict the preceding binary tree as a list in Scheme?

\vspace{1in}

How can we depict the same graph as a recursively-defined binary tree in Scheme? You can think of it in terms of list, or a Scheme structure: 
\begin{verbatim} 
(define-struct tree (value left right))
\end{verbatim}. 

\vspace{1in}

\subsection{Inserting Elements in Trees}

The tree provided in figure 1 is called a binary search tree because its nodes obey a set of simple rules. What would the binary search tree look like with the element 9 inserted? What would the Scheme data structure look like?

\vspace{1in}

Try inserting the elements 3, 4, and 14. 

\vspace{1in}

\subsection{Searching Trees}
Breadth-first search is a graph-searching algorithm that starts at the root and searches all of the root's child nodes. Then it explores all the child nodes of those nodes, and repeats, until the item is found, or the graph has been exhaustively searched. 

Depth-first search is a graph-searching algorithm that starts at the root and explores all the way down a branch before backtracking. In backtracking, we return to the most recently visited node that has other children and explore all the way down this next branch; we repeat until the item is found, or the graph has been exhaustively searched. 

For some graphs, such as binary serach trees (see Figure 1), the search process should be simpler. In what way can we use the proper ordering of the tree to find the element we are searching for?

\vspace{1in}

% The TF's can demonstrate an example of each on the board.


\section{Reading and writing scheme functions}

\subsection{A mystery function}

Try to figure out what this function does. You may want to consider the
following:
\begin{itemize}
\item What type of object does the function evaluate to?
\item What do the various cases mean?
\item How are subproblem results being combined, and what does this imply?
\end{itemize}

\begin{verbatim}
(define (enigma baz qux)
  (cond [(null? qux) 0]
        [(equal? baz (car qux)) (+ 1 (enigma baz (cdr qux)))]
        [(pair? (car qux)) (+ (enigma baz (car qux))
                              (enigma baz (cdr qux)))]
        [else (enigma baz (cdr qux))]))
\end{verbatim}

%%%% solution
%\begin{verbatim}
%enigma counts occurrences of baz in tree structure qux.
%\end{verbatim}
%%%%

%\vspace{0.4in}
\vspace{1.5in}



\subsection{Mystery function challenge}

This one's a little tricky. Try applying the following strategy:
\begin{itemize}
\item Don't get bogged down in details. What does the function do in the
most general case?
\item There appear to be two recursive cases. Is this essential to the
function or perhaps somewhat misleading?
\end{itemize}

\begin{verbatim}
(define (mystery foo bar)
  (cond [(and (null? foo) (null? bar)) '()]
        [(null? foo) (append bar foo)]
        [(null? bar) (cons (car foo)
                           (mystery bar (cdr foo)))]
        [else (cons (car foo)
                    (cons (car bar)
                          (mystery (cdr foo)
                                   (cdr bar))))]))
\end{verbatim}
%;;;; solution
%this mingles lists
%(mystery '(1 2 3) '(a b c)) => (1 a 2 b 3 c)
%the two null? cases are bad style and cruelly misleading
%;;;;

\section{Style notes}

Starting now, we will be watching your coding style like hawks, and so should
you. Prudent and consistent style is a big help when you read over code,
debug it or try to remember how it works.

A few important points to remember:

\begin{itemize}
\item Use {\tt cond} instead of nested {\tt if}

\item Remember that a function you use might already return the value you
need, eliminating the need to check its value:
\begin{verbatim}
    (if (= (+ (square a) (square b)) (square c)) #t #f)
\end{verbatim}
becomes simply:
\begin{verbatim}
    (= (+ (square a) (square b)) (square c))
\end{verbatim}
Think functionally. Every expression already yields a value, so there is rarely
a need to explicitly {\tt return} one object or another.

\item Learn to use {\tt and} and {\tt or}

\item Be sure to use {\tt cons} when all you need to do is add an item to
the front of a list. Never use the construct
{\tt (append (list x) ...)}. Append works to join two lists together;
however if the item being added is not originally a list, it shouldn't be 
made into a list, then appended. 

\item Use line breaks and indentation well. Line up the arguments to a
function so each one starts at the same horizontal position.
Avoid very long and very short lines in general. Be sure to use an
editor like Dr. Scheme, emacs, or vim that can intelligently indent Scheme for you.

\end{itemize}


\section{Answers}

\subsection{Trees}

\begin{itemize}
\item  From 3.2: (list 1 2 7 10 11 12 15)
\item From 3.2: (list (10 (2 (1 empty empty) (7 empty empty)) (12 (11 empty empty) (15 empty empty))))
\item From 3.3: (list (10 (2 (1 empty empty) (7 empty (9 empty empty))) (12 (11 empty empty) (15 empty empty))))
\end{itemize}

\end{document}
