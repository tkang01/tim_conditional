#! /usr/bin/env python

import csv
import os
import sys

import sendgrid

from jinja2 import Template

APIKEY = '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'
sg = sendgrid.SendGridClient(APIKEY)

def email(student):
	msg = sendgrid.Mail()
	msg.add_to(student['email'])
	msg.set_subject('[Computer Science 51] Lab Section Assignment')
	template = Template('<!DOCTYPE html>\
					<html>\
					<head>\
					<title>CS51 Attendance Assignment </title>\
					</head>\
					<body>\
					<p>Hello {{student.first_name | title}} {{student.last_name | title}},</p>\
\
					<p>You have been assigned to the following Lab meeting: </p>\
\
					<p><b>{{student.assignment}}</b></p>\
					\
					<p>This is an automated email. Please do not reply to this email.</p>\
\
					<p>If you need to be re-assigned, please fill out the following form.</p>\
\
					<a href=\'https://docs.google.com/forms/d/1hRP_VjgMnjPfam_X229T-QXzOZzioYojZDX7eYlyKAU/viewform?usp=send_form\'>https://docs.google.com/forms/d/1hRP_VjgMnjPfam_X229T-QXzOZzioYojZDX7eYlyKAU/viewform?usp=send_form</a>\
\
					<p>Thanks,</p>\
					CS51 Attendance')
	msg.set_html(template.render(student=student))
	msg.set_from('Computer Science 51 Attendance <cs51attendance@gmail.com>')

	try:
		status, msg = sg.send(msg)
	except Exception as e:
		print "FAILED: {0}".format(student['email'])


def parse(file):
	with open(file, 'rU') as f:
		reader = csv.DictReader(f)
		for student in reader:
			email(student)


if __name__ == "__main__":


	if len(sys.argv) != 2:
		print "Usage: ./mailer.py /path/to/csv"
		exit(1)

	if not 'EMAILPW' in os.environ:
		print "Config: set password in environment variable"
		exit(1)
		
	parse(sys.argv[1])

