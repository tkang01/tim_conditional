#!/usr/bin/env python

import sys
import csv 

def find_duplicates(reference, check):
	with open('out.csv', 'w') as outfile:

		ref_reader = csv.DictReader(reference)

		reference_emails = {}
		for row in ref_reader:
			reference_emails[str(row['email'])] = True

		check_reader = csv.DictReader(check)

		writer = csv.DictWriter(outfile, fieldnames=check_reader.fieldnames)
		writer.writeheader()

		for row in check_reader:
			if not row['email'] in reference_emails:
				writer.writerow(row)

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "Usage: ./find_missing.py /path/to/reference /path/to/check"
		exit(1)

	with open(sys.argv[1], 'rU') as reference:
		with open(sys.argv[2], 'rU') as check:
			find_duplicates(reference, check)
