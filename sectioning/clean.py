#!/usr/bin/env python

import numpy as np
import pandas as pd
import os
import sys

def clean(fname):
	data = pd.read_csv(fname)

	if 'ResponseID' in data:
		del data['ResponseID']

	if 'ResponseSet' in data:	
		del data['ResponseSet']

	if 'StartDate' in data:
		del data['StartDate']

	if 'EndDate' in data:
		del data['EndDate']
	
	if 'Finished' in data:
		del data['Finished']

	cols = data.columns.values

	for i,col in enumerate(cols):
		if 'HUID' in col:
			cols[i] = 'HUID'
		elif 'first name' in col:
			cols[i] = 'fn'
		elif 'last name' in col:
			cols[i] = 'ln'
		elif 'email address' in col:
			cols[i] = 'email'
		else:
			col = col.split('.-')
			if 'Lab' in col[0]:
				cols[i] = 'Lab {0}'.format(col[1])
			elif 'Section' in col[0]:
				cols[i] = 'Section {0}'.format(col[1])

	data.columns = cols

	data.to_csv("cleaned_"+fname)

if __name__ == "__main__":
	if len(sys.argv) !=2:
		print "Usage: ./clean.py /path/to/csv/file"
		exit(1)
	else:
		clean(sys.argv[1])

