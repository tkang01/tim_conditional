#! /usr/bin/env python
import csv
import os
import sys

import sendgrid

from jinja2 import Environment, PackageLoader
env = Environment(loader=PackageLoader('codereviewmailer', 'templates'))

APIKEY = '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'
sg = sendgrid.SendGridClient(APIKEY)

def email(student):
	msg = sendgrid.Mail()
	msg.add_to(student['email'])
	msg.set_subject('[Computer Science 51] Code Review Assignment')
	template = env.get_template('assignment.html')
	msg.set_html(template.render(student=student))
	msg.set_from('Computer Science 51 Attendance <cs51attendance@gmail.com>')

	try:
		status, msg = sg.send(msg)
	except Exception as e:
		print "FAILED: {0}".format(student['email'])

def re_assign(student):
	msg = sendgrid.Mail()
	msg.add_to(student['email'])
	msg.set_subject('[Computer Science 51] New Code Review Assignment')
	template = env.get_template('re-assignment.html')
	msg.set_html(template.render(student=student))
	msg.set_from('Computer Science 51 Attendance <cs51attendance@gmail.com>')

	try:
		status, msg = sg.send(msg)
	except Exception as e:
		print "FAILED: {0}".format(student['email'])

def parse(file):
	with open(file, 'rU') as f:
		reader = csv.DictReader(f)
		count = 0
		for student in reader:
			re_assign(student)
			count += 1 
		print count


if __name__ == "__main__":

	if len(sys.argv) != 2:
		print "Usage: ./mailer.py /path/to/csv"
		exit(1)
		
	parse(sys.argv[1])

