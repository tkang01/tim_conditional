#Midterm Draft for CS51 2014

##Section 1 - True or False
Higher-order functions are functions that change behavior based upon the type of the their arguments. - True or **False**

The following defines an algebraic data type - True or **False**?

	type person = {name : string; age : int; major : string }

Anything that you can write with a loop, you can write with recursion - **True** or False?

Ocaml is a statically typed language - **True** or False?

The type of the following function is `int list option -> int option list` - True or **False**?


	let rec func data =
	match data with
	| [] -> None
	| None :: tl -> func tl
	| (Some hd) :: tl ->
	(match func tl with
	| None -> (if hd > 0 then Some [hd] else None)
	| Some lst -> (if hd > 0 then Some (hd :: lst) else Some lst))
	;;


Will the following function compile without error - True or **False**;

	let func (d : int list) : int option =
		match d with
		| [] -> Some 4
		| hd::[] -> if hd > 0 then Some hd else None
		| hd::snd:tl ->
			(match func tl with
				| None -> Some (hd - snd)
				| Some v -> if v > hd then hd else Some snd)


The function signature of the following function is `('a -> 'b -> 'b) -> 'a list -> 'b -> 'b list` - True or **False**

	let func f xs u =
		match xs with
			| [] -> f u u
			| hd::tl -> (f hd u) :: (func tl)


##Section 2 - Code Fill-ins

Fill in the `???` in each of the following blocks of code such that they evaluate to the stated value. You may have to use some combination of `fold_right` and `map`.

Question 1 (Using High Level Functions) - This should evaluate to 51.

    let inc = (fun x -> x + 1) in
	let mul2 = (fun x -> x * 2) in
	let lst = [7;8;9] in ???


Answer :

    let inc = (fun x -> x + 1) in
	let mul2 = (fun x -> x * 2) in
	let lst = [7;8;9] in List.fold_right (+)
		(List.map add (List.map mul2 lst)) 0;;

Question 2 (Geared at making sure they understand let binding) - This should evaluate to 51.

	let f = (fun x -> x + 1) in
	let f = (fun x -> (f x) + 1) in
	???

Answer:

	let f = (fun x -> x + 1) in
	let f = (fun x -> (f x) + 1) in
	f 49;;

Question 3 (Using Records and Higher Order Functions) - This should evaluate out to "AliceCathy"

	type person = {age : int; name : string; major : string};;


	let people = [{age = 81; name = "Alice"; major="CS"};
		{age=51;name="Bob";major="Econ"};
		{age=27;name="Cathy";major="Physics"}] in
	???;;


Answer :

	type person = {age : int; name : string; major : string};;


	let people = [{age = 81; name = "Alice"; major="CS"};
		{age=51;name="Bob";major="Econ"};
		{age=27;name="Cathy";major="Physics"}] in
		let targets = List.filter (fun x -> x.age <> 51) people in
		List.fold_right (fun x y -> x.name ^ y) targets ""
	;;




