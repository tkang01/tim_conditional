#!/bin/bash
echo -e "\n\n"
if [ ! -f "ps0.ml" ]; then
    echo "ps0.ml isn't in this directory!";
else
    read -p "Enter your nice.fas.harvard.edu username: " -e user
    echo -e "\n\n\n!!!!!!!!!!!!!!!!\n\nYou will be prompted twice (or more, if you enter it incorrectly) for your password. If you are asked \n'Are you sure you want to continue connecting (yes/no)?',\n type 'yes'\n\n!!!!!!!!!!!!!!!!\n\n\n" 
    scp ps0.ml $user@nice.fas.harvard.edu:.CS51_ps0.ml
    ssh $user@nice.fas.harvard.edu "mkdir .CS51_PS0_tmp || true; mv -f .CS51_ps0.ml .CS51_PS0_tmp/ps0.ml; cd .CS51_PS0_tmp; submit lib51 0 \`pwd\`; cd ..; rm -rf .CS51_PS0_tmp"
fi
