#!/usr/bin/python

import os
import time
import datetime
import math
import sys
import re

SECONDS_PER_DAY = 24. * 60. * 60.
GRACE_PERIOD = 15. * 60.

DUE_PTRN = re.compile('([0-9]+)/([0-9]+)@([0-9]+)')

def latedays(path, due):
    submit_time = os.stat(path).st_mtime
    late = submit_time - due

    if late >= GRACE_PERIOD:
        return int(math.ceil(late / SECONDS_PER_DAY))
    else:
        return 0
        

def usage():
    print """
  Given the month day and hour that a problem set is due and a list of
  files, this program checks the timestamps of the files and prints a
  list with the file name and the number of late days.

  Usage:

    ./latedays.py <month>/<day>@<hour (24-hour format)> [files ...]"""



if __name__ == '__main__':
    due = time.time()
    try:
        (m,d,h) = DUE_PTRN.match(sys.argv[1]).groups()
        due = time.localtime()
        due = datetime.datetime(due.tm_year, int(m), int(d), int(h))
        due = time.mktime(due.utctimetuple())
    except:
        usage()
        sys.exit(1)

    for f in sys.argv[2:]:
        print "%s : %d" % (f, latedays(f, due))
    
