#!/usr/bin/env bash
# CS 51 Install Script
# Stefan Rajkovic, Sam Green, Dan Armendariz 
# 2015-2016

if [ "$(whoami)" = "root" ]; then
    echo "Please do not run with sudo or as root!"
    exit 1
fi
# -e = exit on errors
# -u = any reference to undeclared variable is an error and quits (rather than silently fails)
# -o pipefail = if, during a piped command (eg line 15) the initial command fails, the return code is passed down the chain
set -euo pipefail

os=`uname`
ocaml_version='4.02.2'
ocaml_base="$HOME/.ocamlbrew"
LOG="$HOME/cs51_install.log"

# remove log file if it exists
test -f $LOG && rm $LOG

function cs51_note () {
    echo "CS 51 --- $1" | tee -a $LOG
}

function try () {
    # temporarily disable exit status checking
    set +e
    
    # run command and check for non-0 exit status
    "$@" | tee -a $LOG
    if [ $? -ne 0 ]; then
        # uh oh
        cs51_note "ERROR: The Ocaml install did not complete!"
        cs51_note "Take a look at $LOG, and email it to staff if necessary"
        exit 2
    fi

    # back to safety
    set -e
}

function universal_install () {
    cs51_note "Attempting to install OCaml and OPAM from ocamlbrew"
    try curl -ksL https://raw.github.com/hcarty/ocamlbrew/master/ocamlbrew-install 2>>$LOG | try env OCAMLBREW_BASE="${ocaml_base}" OCAMLBREW_FLAGS="-v ${ocaml_version} -r" bash
}

function linux_shell () {
    src="${ocaml_base}/ocaml-${ocaml_version}/etc/ocamlbrew.bashrc"
    if ! grep -qs "source \"$src\"" ~/.bashrc; then
        echo "source \"$src\"" >> ~/.bashrc
    fi
    source $src
}

function mac_shell () {
    src="${ocaml_base}/ocaml-${ocaml_version}/etc/ocamlbrew.bashrc"
    if ! grep -qs "source \"$src\"" ~/.bash_profile; then
        echo "source \"$src\"" >> ~/.bash_profile
    fi
    source $src
}

function configuration () {
    cs51_note "Setting up OPAM and necessary packages"
    try opam init -a
    eval `opam config env`

    # Opam installations
    try opam install -y tuareg merlin utop

    cs51_note "Setting up emacs config files."
    EMACS_CONFIG_FILE="$HOME/.emacs"
    if [ ! -f ~/.emacs ]; then
        if [ -f ~/.emacs.d/init.el ]; then
            EMACS_CONFIG_FILE="$HOME/.emacs.d/init.el"
        else
            touch ~/.emacs
        fi
    fi    
    
    EMACS_FILE_MARKER_TUAREG=';; Added by CS 51 setup script -- Tuareg'
    if ! grep -qs "$EMACS_FILE_MARKER_TUAREG" "$EMACS_CONFIG_FILE"; then
        echo $EMACS_FILE_MARKER_TUAREG >> "$EMACS_CONFIG_FILE"
        cat <<-TUAREG >> "$EMACS_CONFIG_FILE"
(load "~/.opam/system/share/emacs/site-lisp/tuareg-site-file")
(add-to-list 'load-path "~/.opam/system/share/emacs/site-lisp/")

TUAREG
    fi
    
    EMACS_FILE_MARKER_MERLIN=';; Added by CS 51 setup script -- Merlin'
    if ! grep -qs "$EMACS_FILE_MARKER_MERLIN" "$EMACS_CONFIG_FILE"; then
        echo $EMACS_FILE_MARKER_MERLIN >> "$EMACS_CONFIG_FILE"
        cat <<-MERLIN >> "$EMACS_CONFIG_FILE"
(setq opam-share (substring (shell-command-to-string "opam config var share 2> /dev/null") 0 -1))
(add-to-list 'load-path (concat opam-share "/emacs/site-lisp"))
;; Load merlin-mode
(require 'merlin)
;; Start merlin on ocaml files
(add-hook 'tuareg-mode-hook 'merlin-mode t)
(add-hook 'caml-mode-hook 'merlin-mode t)
;; Enable auto-complete. This only enables 'M-x auto-complete'. For live autocomplete as you type, replace "'easy"with "t"
(setq merlin-ac-setup 'easy)
;; Use opam switch to lookup ocamlmerlin binary
(setq merlin-command 'opam)

(add-hook 'tuareg-mode-hook #'merlin-mode)
MERLIN
    fi
    
    EMACS_FILE_MARKER_UTOP=';; Added by CS 51 setup script -- UTOP'
    if ! grep -qs "$EMACS_FILE_MARKER_UTOP" "$EMACS_CONFIG_FILE"; then
        echo $EMACS_FILE_MARKER_UTOP >> "$EMACS_CONFIG_FILE"
        cat <<-UTOP >> "$EMACS_CONFIG_FILE"
;; Setup environment variables using opam
(dolist (var (car (read-from-string (shell-command-to-string "opam config env --sexp"))))
  (setenv (car var) (cadr var)))

;; Update the emacs path
(setq exec-path (append (parse-colon-path (getenv "PATH"))
                        (list exec-directory)))

;; Update the emacs load path
(add-to-list 'load-path (expand-file-name "../../share/emacs/site-lisp"
                                          (getenv "OCAML_TOPLEVEL_PATH")))

;; Automatically load utop.el
(autoload 'utop "utop" "Toplevel for OCaml"t)

(autoload 'utop-minor-mode "utop" "Minor mode for utop"t)
(add-hook 'tuareg-mode-hook 'utop-minor-mode)

;; Rebinding some of Tuareg's bindings to use utop instead of the ocaml toplevel
(defun tuareg-mode-keybindings  ()
  "Shadow a few of Tuareg mode's keybindings to use utop instead of the default toplevel."
  (interactive)
  (local-unset-key (kbd "C-c C-e"))
  (local-set-key (kbd "C-c C-e") 'utop-eval-phrase)
  (local-unset-key (kbd "C-c C-r"))
  (local-set-key (kbd "C-c C-r") 'utop-eval-region))
(add-hook 'tuareg-mode-hook 'tuareg-mode-keybindings)

UTOP
    fi

    $SHELL -l
}

function osx_install () {
    if [ 'xcode-select -p' == 2 ]; then
        cs51_note "Installing XCode command line tools. You should see a dialog box prompting you to install."
    fi
    
    if hash brew; then
        cs51_note "Updating brew recipes; you may be asked for your password."
        try sudo chown -R $(whoami):admin /usr/local
        try brew update
    else
        cs51_note "Attempting to install Homebrew, follow all of the directions, using Homebrew's defaults."
        try ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    fi
    
    if brew list | grep emacs; then
        cs51_note "emacs previously installed"
    else
        cs51_note "intstalling emacs"
        try brew install emacs
    fi

    if brew cask list | grep xquartz; then
        cs51_note "xquartz already installed"
    else
        try brew cask install xquartz
    fi

    universal_install

    mac_shell

    configuration 
}

function linux_install () {
    source /etc/os-release
    case "$ID" in
        ubuntu | debian | mint) manager="apt-get";;
        centos | rhel) manager="yum";;
        fedora) if (( $VERSION_ID < 23 )); then
                    manager="yum"
                else
                    manager="dnf"
                fi ;;
        *) cs51_note "Sorry, we only support automatic installation for Ubuntu, Debian, Fedora, CentOS, and RHEL"
           exit 1;;
    esac

    cs51_note "Installing emacs and opam prerequisites."

    install_array=(gcc make patch unzip m4 emacs git xorg)
    
    if ! hash which; then
        install_array[9]=which
    fi

    if ! hash curl; then
        install_array[10]=curl
    fi

    try sudo $manager install -y ${install_array[@]}

    universal_install

    linux_shell

    configuration 
}

 case "$os" in
     Darwin) osx_install;;
     Linux) linux_install;;
     *) cs51_note "Sorry, we only support automatic installation on OS X and Linux"
        exit 1;;
 esac

