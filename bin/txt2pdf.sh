#!/bin/sh -e
# Call this script with name of text file
# ./PATH/txt2pdf.sh input.txt

# Print text file to postscript
enscript -p temp-postscript-file.ps $1
# Convert postscript to pdf
ps2pdf temp-postscript-file.ps "${1%%.*}.pdf"
# Clean up
rm temp-postscript-file.ps