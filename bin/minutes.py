###
# Minutes spent data is available for PS2 (mapfold.ml, expression.ml), PS3
# (ps3.ml), PS4 (ps4.ml), and PS6 (streams.ml, refs.ml, music.ml).
#
###

import os
import json
import sys, getopt

def getInfo(directory):
    dir = directory.split('_')
    huid = dir.pop()
    name = ' '. join(dir)
    return name, huid

def minutes_from_line(line):

    # Removes semicolons from line and returns list of all numbers in line.
    numbers = [int(s) for s in line.replace(';', '').split() if s.isdigit()]
    if len(numbers) != 1:
        return
    return numbers[0]

if __name__ == "__main__":

    debug = False

    try:
        opts, args = getopt.getopt(sys.argv[1:], "d", ["debug"])
    except getopt.GetoptError:
        print 'Failure(CLA)' # todo(better error message)
        sys.exit(1)

    if len(args) != 3:
        print 'Failure(requires 3 arguments)' # todo (better error message)
        sys.exit(2)

    pset = args[1]
    dir_path = args[0] + pset
    filename = args[2]

    for opt, val in opts:
        if opt in ("-d", "--debug"):
            debug = True

    all_minutes = []
    data = []
    id = 0

    for student_dir in os.listdir(dir_path):

        name, huid = getInfo(student_dir)

        full_path = dir_path + '/' + student_dir + '/' + filename

        # This error occurs if the student didn't submit the file.
        if not (os.path.isfile(full_path)):
            if debug:
                print 'Failure (path doesn\'t exist): ' + full_path
            continue

        with open(full_path) as in_file:
            for line in in_file:
                if line.find('minutes_spent') != -1:
                    minutes = minutes_from_line(line)

                    # This error most commonly occurs if:
                    #  - Student commented out the minutes_spent line
                    #  - Student multiplied (i.e., 4 * 60 instead of 240)
                    #  - Student put value on new line
                    if not(minutes):
                        if debug:
                            print 'Failure (malformed minutes_spent line): ' + full_path
                        break

                    data_object = {
                        'minutes': minutes,
                        'pset': pset,
                        'file': filename,
                        'id': id
                    }
                    data.append(data_object)
                    id += 1
                    break

    if debug:
        print 'There are ' + str(len(data)) + ' data points.'

    with open('output' + '/' + filename.split('.')[0] + '.json', 'w') as out_file:
        json.dump(data, out_file)
