#!/bin/sh

# Usage:
# tally_date_days.sh ps_number due_date 

deadline=`date -j "$2" "+%s"`

cd ps$1

for username in *; do
	latest_submission=`git log --pretty=format:'%ct' -n 1 $username`
	latest_submission_human_readable=`git log --pretty=format:'%cd' -n 1 $username`
	difference_full=$(($latest_submission - $deadline))
	difference=$(( ($latest_submission - $deadline) / 86400))
	echo $difference,$difference_full,$latest_submission_human_readable,$username,$deadline,$latest_submission,$difference
done
