#!/usr/bin/python

import csv
import os
import sys
import re
import getopt

def usage():
    print """USAGE: mass-mailer.py --template=my-template [--send] grades.csv

Read grades.csv, and send a template email to each student.
Template format is {{format-string:column-name}}.
Examples: {{%s:Email Address}}, {{%2d:Midterm Q1}}.
"""

def send(text, addr="NOT GIVEN"):
    p = os.popen('/usr/sbin/sendmail -t', 'w')
    p.write(text)
    exitcode = p.close()
    if exitcode:
        print "Failed to send mail to '%s'" % addr

PTRN_HOLE = re.compile(r'\{\{([^:]+):([^}]+)\}\}')

def generate(template, row):
    t = template
    m = PTRN_HOLE.search(t)

    while m:
        fill = m.group(1)
        if 'd' in fill:
            try:
                fill = fill % int(row[m.group(2)])
            except:
                fill = "??" # missing
                print "missing %s" % row[m.group(2)]
        else:
            fill = fill % row[m.group(2)]
            
        t = t[:m.start()] + fill + t[m.end():]
        m = PTRN_HOLE.search(t)

    t = t.replace('\r', '')

    return t

if __name__ == '__main__':
    try:
        opts, args = getopt.getopt(sys.argv[1:],
                                   "",
                                   ["help",
                                    "template=",
                                    "send"])
    except getopt.GetoptError, err:
        print str(err)
        usage()
        sys.exit(2)

    should_send = False
        
    for o, a in opts:
        if o == '--help':
            usage()
            sys.exit(0)
        elif o == '--template':
            template = open(a, 'rb').read()
        elif o == '--send':
            should_send = True
        else:
            print o
            assert False

    if len(args) < 1:
        usage()
        sys.exit(2)

    d = csv.DictReader(open(args[0],'rb'))

    for r in d:
        if not r.has_key('First Name') or r['First Name'] == '':
            continue
        
        report = generate(template, r)

        if False: # should_send:
            send(report, r['Email Address'])
            print "Sent to %s" % r['Email Address']
        else:
            print report
                          
