#!/usr/bin/python
# CS 51 Tools
# Script for sending out graded code submissions.
#

import os
import csv
import sys
import re

staff_email = "cs51@seas.harvard.edu"
course = "CS51"
prof = "Stuart"
headtf = "RoBen"

# Just print the emails by default.  Require an explicit -send flag to
# actually send
DEBUG = True

# returns dictionary mapping fas usernames to email addresses
email_fieldname = "Email Address"
username_fieldname = "Submission Directory"
score_fieldname = "PS1 Grade" # hack hack hack
total = '54'

email_field_index = -1
username_field_index = -1
score_field_index = -1
def parse_emails_and_scores(filename):
    student_emails = {}
    student_scores = {}
    reader = csv.reader(open(filename,'rU'))
    first = True
    for fields in reader:
        if first:
            fieldNames = fields
            if (None in fieldNames) or ('' in fieldNames):
                print "All fields must have names!"
                print "field names are %s" % fieldNames
                sys.exit(1)

            email_field_index = fieldNames.index(email_fieldname)
            username_field_index = fieldNames.index(username_fieldname)
            score_field_index = fieldNames.index(score_fieldname)
            first = False
        else:
            if len(fields)-1 < min(email_field_index, username_field_index):
                continue
            try:
                username = fields[username_field_index]
            except IndexError:
                username = ''
            if len(username) > 0:
                student_emails[username] = fields[email_field_index]
                try:
                    student_scores[username] = fields[score_field_index]
                # If there's no score, remove the student email (we're not going
                # to be shipping a grade)
                except IndexError:
                    del(student_emails[username])
    return (student_emails, student_scores)

def load(filename):
    """Load the contents of a file into a list of lines, stripping newlines."""
    f = open(filename)
    ls = []
    for l in f:
        ls.append(''.join(c for c in l[:-1] if c not in '\r'))
          # Strip trailing newlines and silly DOS line endings.
    f.close()
    return ls

def headers(to_address, ps_number):
    all = []
    all.append("From: %s" % staff_email)
    all.append('To: %s' % to_address)
    all.append('Subject: %s: Your graded PS%s' % (course, ps_number))
    all.append('\n') # make sure there's the blank line at the end of the header
    return '\n'.join(all)

def send_email(text, address="NOT GIVEN"):
    """Send the given email, which must include all the proper headers already.
    The address should be passed in for better error messages.
    """
    p = os.popen('/usr/sbin/sendmail -t', 'w')
    p.write(text)
    exitcode = p.close()
    if exitcode:
        print ("Mail didn't send successfully to '%s'.  Exit code: %s"
               % (address, exitcode))

def send_all(parts, submissions_dir, student_emails, student_scores, ps_number):
    warnings = ""
    for username in student_emails.keys():
        if not username in os.listdir(submissions_dir):
            warnings += "\nWarning: no graded files for user %s" % username
            continue

        if (student_scores[username] == ''):
            warnings += "\nWarning: no e-mail sent to user %s" % username
            continue

        email_str = headers(student_emails[username], ps_number)
        score = student_scores[username]
        email_str += 'Your score on this problem set was ' + score + '/' + total + '.'
        email_str += '\n\nPlease let us know if you have any questions.\n\n'
        email_str += 'UnBowden, UnBent, Unbroken,\n' + headtf
        for part in parts:
            email_str += '\n\n' + '*'*60 + '\n\n'


            email_str += '\n\nYour graded %s.ml follows:\n' % part
            part_path = os.path.join(submissions_dir, username,
                                     '%s_gc.ml' % part)
            if os.path.exists(part_path):
                email_str += '\n'.join(load(part_path))
            else:
                warnings += "\nWarning: graded %s.ml is missing for user %s" % \
                    (part, username)

        if DEBUG:
            print "Email for user %s:" % username
            print email_str
        else:
            print "Sending email to %s..." % username
            send_email(email_str, student_emails[username])

    print warnings

def usernames_set_from_file(filename):
    return set(load(filename))

def usage():
    print '''Usage: sendcode.py ps_number grades.csv [usernames.csv] [-send]

Optionally provide usernames.csv to email grades only to specified usernames
   (provide 1 per line)
If -send isn't given, will print emails instead of actually sending.'''
    sys.exit(255)

if __name__ == '__main__':
    my_path = os.path.dirname(os.path.realpath(__file__))
    if len(sys.argv) < 3:
        usage()
    ps_number = sys.argv[1]
    spreadsheet = sys.argv[2]

    send_loc = 3
    usernames = None
    if len(sys.argv) > 3 and sys.argv[3] != "-send":
        usernames_file = sys.argv[3]
        send_loc = 4
        usernames = usernames_set_from_file(usernames_file)

    if len(sys.argv) > send_loc and sys.argv[send_loc] == "-send":
        DEBUG = False

    ps_name = 'ps' + ps_number
    ps_submissions_path = \
        os.path.join(my_path, '..', '..', 'grading', ps_number)
    ps_part_names_path = os.path.join(my_path, '..', 'hw', '2014', ps_number, 'tests',
                                      'part_names')
    if os.path.exists(ps_part_names_path):
        parts = load(ps_part_names_path)
    else:
        # If the part_names file does not exist, assume there is only one part,
        # with the same name as the pset.
        parts = [ps_name]

    # If we provided an explicit list of usernames, sanitize out any
    # student_emails and student_scores for students that aren't in that list.
    (student_emails, student_scores) = parse_emails_and_scores(spreadsheet)
    if usernames is not None:
        for username in student_emails.keys():
            if username not in usernames:
                del(student_emails[username])
                del(student_scores[username])

    send_all(parts, ps_submissions_path, student_emails, student_scores, ps_number)

