#!/usr/bin/python

import os
import csv
import sys
import re

staff_email = "cs51-staff@eecs.harvard.edu"
course = "CS51"
prof = "Greg"
headtf = "David and Anna"

# Just print the emails by default.  Require an explicit -send flag to 
# actually send
DEBUG = True

def usage():
    print """Usage: sendgrades.py grades.csv [-send]

If -send isn't given, will print emails instead of actually sending."""
    sys.exit(1)

# Try to convert a string to a float.  If it works, return the float.  If not, the string
def try_float(s):
    try:
        if s == None:
            return s
        f = float(s)
        return round(f, 2)
    except ValueError:
        return s
#    except TypeError:
#        print "Type error: %s" % type(s)


class Student:
    def __init__(self):
        self.fieldnames = []
        self.vals = {}
    
    def add_field(self, name, val):
#        print "adding field %s" % name
        self.vals[name] = val
#        print name + ("='%s'" % self.vals[name])
        self.fieldnames.append(name)

    def __repr__(self):
        return ",".join([f + "=" + str(self.vals[f]) for f in self.fieldnames]) + "\n"

    def pset_string(self):
        s = ""
        fields = []
        for f in self.fieldnames:
            if re.match("^PS[0-7]$", f, re.IGNORECASE):
                s = s + "%s: %s/%s\n" % (f, self.vals[f], totals[f])
#             elif re.match("^PS[0-4] late days$", f, re.IGNORECASE):
#                 # Add an extra newline to separate psets
#                 s = s + "%s: %s\n\n" % (f, self.vals[f])
                
        return s

    def quiz_string(self):
        fields = []
        for f in self.fieldnames:
            if re.match("^Quiz *[0-9]$", f, re.IGNORECASE):
                fields.append(f)

        s= "\n".join(["%s: %s/%s" % (f, self.vals[f], totals[f]) for f in fields])
        if s != "":
            s += "\n"
        return s
        

    def midterm_string(self):
        fields = []
        for f in self.fieldnames:
            if re.match("^Midterm Q[0-9]$", f, re.IGNORECASE):
                fields.append(f)

        s= "\n".join(["%s: %s/%s" % (f, self.vals[f], totals[f]) for f in fields])
        if s != "":
            s += "\n"

        for f in self.fieldnames:
            if re.match("^Midterm Total$", f, re.IGNORECASE):
                return s + ("Midterm: %s/%s\n" % (self.vals[f], totals[f]))
        return s

    def project_string(self):
        for f in self.fieldnames:
            if re.match("^Project total$", f, re.IGNORECASE):
                return "Final Project: %s/%s\n" % (self.vals[f], totals[f])
        return ""

    def moogle_string(self):
        s=""
        for f in self.fieldnames:
            if re.match("^Moogle Total$", f, re.IGNORECASE):
                s += "Moogle: %s/%s\n" % (self.vals[f], totals[f])
            #if re.match("^Moogle Part 1 Late Days$", f, re.IGNORECASE):
             #   s += "Moogle Part 1 Late Days: %s\n" % (self.vals[f])
           # if re.match("^Moogle part 2 late days$", f, re.IGNORECASE):
            #    s += "Moogle Part 2 Late Days: %s\n" % (self.vals[f])
        return s


    def final_string(self):
        for f in self.fieldnames:
            if re.match("^Final Final$", f, re.IGNORECASE):
                return "Final: %s/%s\n" % (self.vals[f], totals[f])
        return ""

    def overall_string(self):
        return ""
#         for f in self.fieldnames:
#             if re.match("^Overall$", f, re.IGNORECASE):
#                 return "Overall score: %s\n" % self.vals[f]
#         return ""

    def letter_grade_string(self):
        return ""
#         for f in self.fieldnames:
#             if re.match("^Letter grade$", f, re.IGNORECASE):
#                 return "Unofficial letter grade: %s\n" % self.vals[f]
#         return ""
        

    def headers(self):
        all = []
        all.append("From: %s" % staff_email)
        all.append('To: %s' % self.vals["email-address"])
        all.append('Subject: Your %s grades' % course)
        all.append('\n') # make sure there's the blank line at the end of the header
        return '\n'.join(all)

    def email(self):
# A note on problem set feedback: instead of writing a ton of individual
# comments, we've been posting solutions along with a detailed list of
# mistakes we saw.  We strongly encourage you to look at them and
# compare them to your code, so you can see where you made mistakes, and
# where there might be a simpler/clear way to do something.

        return """%s
Hi %s,

These are your grades for the semester:

Problem sets:
%s

%s%s
If there is a wrong grade recorded, please
email %s as soon as possible.

-%s Staff
""" % (self.headers(),
       self.vals["first-name"].split()[0],
       self.pset_string(),

       #self.quiz_string(), 
       self.midterm_string(),
       #self.project_string(),
       self.moogle_string(),
       #self.final_string(),
       #self.overall_string(),
       #self.letter_grade_string(),

       headtf,
       course
      )
        

# return a list of entries
def readFile(filename):
    students = []
    reader = csv.reader(open(filename,'rU')) 
    first = True
    for fields in reader:
        if first:
            fieldNames = fields
            if (None in fieldNames) or ('' in fieldNames):
                print "All fields must have names!"
                print "field names are %s" % fieldNames
                sys.exit(1)

            first = False
        else:
            student = Student()
            for i in range(len(fieldNames)):
                if i < len(fields):
                    f = fields[i]
                else:
                    f = ''
#                print "adding field %s: '%s'='%s'" % (i, fieldNames[i], f)
                student.add_field(fieldNames[i], f)

            if student.vals.has_key("last-name"):
                # Ignore blank lines
                students.append(student)
    return students

# only want the real students
def realStudent(s):
    first = s.vals["first-name"]
    return (len(first) > 1)

def send_mail(text, address="NOT GIVEN"):
    """Send the given email, which must include all the proper headers already.
    The address should be passed in for better error messages.
    """
    p = os.popen('/usr/sbin/sendmail -t', 'w')
    p.write(text)
    exitcode = p.close()
    if exitcode:
        print ("Mail didn't send successfully to '%s'.  Exit code: %s" 
               % (address, exitcode))
    

def send_grades():
    print "Reading grades from %s" % filename

    students = readFile(filename)

    global totals
    totals = [s.vals for s in students if s.vals["seas-username"] == "Out of"][0]
    #avg = [s for s in students if s.vals["last-name"] == "Average"][0]
    #stddev = [s for s in students if s.vals["last-name"] == "StdDev"][0]

    students =  filter(realStudent , students)

    if DEBUG:
         print "DEBUG mode is on.  Won't actually send emails.  Will print them instead.  Use the -send flag after the filename to send."
         import time
         time.sleep(3)
         for s in students:
             print s.email()

    #     print "Averages:"
     #    print avg.pset_string()
      #   print avg.quiz_string()
       #  print avg.midterm_string()
    else:
        for s in students:
            print 'Emailing grades to %s' % s.vals["email-address"]
            send_mail(s.email(), s.vals["email-address"])

if __name__ == '__main__':                
    if len(sys.argv) < 2:
        usage()
    filename = sys.argv[1]
    if len(sys.argv) > 2 and sys.argv[2] == "-send":
        DEBUG = False


    send_grades()
