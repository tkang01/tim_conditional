import os
import json

output_file = 'all.json'

combined = []
for file in os.listdir('output'):
    if file == output_file:
        continue
    with open('output/' + file) as in_file:
        data = json.load(in_file)
        combined = combined + data

with open('output/' + output_file, 'w') as out_file:
    json.dump(combined, out_file)
