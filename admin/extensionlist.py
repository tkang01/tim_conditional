import pandas as pd

def get_data(filename):
	return pd.read_csv(filename)	

def delete_college_columns(df):
	if 'Notify' in df:
		del df['Notify']

	if 'Photo' in df:
		del df['Photo']

	if 'Grade Basis' in df:
		del df['Grade Basis']

	if 'Program and Plan' in df:
		del df['Program and Plan']

	if 'Level' in df:
		del df['Level']

	if 'Units' in df:
		del df['Units']

	return df

def clean(df):
	return df.rename(columns = {
		'Email Address':'email',
		'ID':'studentid',
		'Name':'name'
		})

if __name__ == "__main__":
	import sys
	if len(sys.argv) != 2:
		print "Usage ./list-prep.py filename"
		exit(1)

	data = get_data(sys.argv[1])

	data = delete_college_columns(data)

	data = clean(data)

	data.to_csv('Vocareumized'+sys.argv[1], index=False)


